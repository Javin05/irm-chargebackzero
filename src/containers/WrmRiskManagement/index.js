import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import WrmRiskManagement from '../../components/wrmManagement/analyzeRiskManangement'
import SDKCBForm from '../../components/wrmManagement/SDK/sdkForm'

const issuerBankBreadCrumbs = [
  {
    title: 'Risk Management',
    path: '/wrm-riskmmanagement',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]
function IssuerBank() {
  return (
    <Switch>
      <Route path='/wrm-riskmmanagement'>
        <PageTitle breadcrumbs={{}}>Wrm Queue</PageTitle>
        <WrmRiskManagement />
      </Route>
      <Route path='/wrm-sdk'>
        <PageTitle breadcrumbs={{}}>WRM SDK</PageTitle>
        <SDKCBForm />
      </Route>
    </Switch>
  )
}

export default IssuerBank
