import React from 'react'
import { Switch, Route } from 'react-router-dom'
import NicSearch from '../../components/nicSearch/nicSearch'
import { PageTitle } from '../../theme/layout/core'


function NicSearchList(props) {
  return (
    <Switch>
      <Route path='/nic-search'>
        <PageTitle breadcrumbs={{}}>NIC Search</PageTitle>
        <NicSearch />
      </Route>
    </Switch>
  )
}

export default NicSearchList
