import { Switch, Route } from 'react-router-dom'
import WebReport from '../../components/webReportSetting/index'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/appstore-report-settings'>
        <PageTitle breadcrumbs={{}}>App Store Report</PageTitle>
        <WebReport reportValue={"Appstore Report"} />
      </Route>
    </Switch>
  )
}

export default Home
