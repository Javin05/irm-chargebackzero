import { Switch, Route } from 'react-router-dom'
import WrmRtWebReport from '../../components/wrmRtWebReport/index'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/wrm-realtime-web-report-settings'>
        <PageTitle breadcrumbs={{}}>Wrm Realtime Web Report</PageTitle>
        <WrmRtWebReport reportValue={"Realtime Web Report"}/>
      </Route>
    </Switch>
  )
}

export default Home
