import React from 'react'
import { Switch, Route } from 'react-router-dom'
import RiskSummary from '../../components/eddRiskSummary/riskSummary'

function EddRiskSummaryPage (props) {
  
  return (
    <Switch>
      <Route path='/edd-risk-summary/update/:id'>
        <RiskSummary />
      </Route>
    </Switch>
  )
}

export default EddRiskSummaryPage