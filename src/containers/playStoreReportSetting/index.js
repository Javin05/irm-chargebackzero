import { Switch, Route } from 'react-router-dom'
import PlayStoreReport from '../../components/playStoreReportSetting/index'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/playstore-report-settings'>
        <PageTitle breadcrumbs={{}}>Play Store </PageTitle>
        <PlayStoreReport />
      </Route>
    </Switch>
  )
}

export default Home
