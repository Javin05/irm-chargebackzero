import React, { useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import { Registration } from '../../components/auth/Registration'
import ForgotPassword from '../../components/auth/ForgotPassword'
import Login from '../../components/auth/Login'
import { toAbsoluteUrl } from '../../theme/helpers'
import routeConfig from '../../routing/routeConfig'
import { colors } from '../../utils/constants'
import ResetPassword from '../../components/auth/ResetPassword'

export function LoginPage(props) {
  const query = useLocation().search
  const pathName = useLocation().pathname

  useEffect(() => {
    document.body.classList.add('bg-white')
    return () => {
      document.body.classList.remove('bg-white')
    }
  }, [])

  return (
    <>
      <div className="d-flex flex-column flex-lg-row flex-column-fluid"
      >
        <div className="col-5 d-grid place-content-center bg-radiant"
        >
          <div className="d-flex flex-center flex-column">
            <div
              className='w-lg-500px bg-white rounded shadow-sm p-10 p-lg-15 mt-10'
            >
              <Login />
            </div>
          </div>
        </div>
        <div className="col-7 bg-cover d-flex flex-column flex-lg-row-fluid bgi-no-repeat bgi-position-x-center bgi-size-contain min-h-100px min-h-lg-350px"
          style={{
            backgroundImage: `url(${toAbsoluteUrl(
              '/media/loginImage/jumio-go-hero.webp'
            )})`
          }}
        >
        </div>
      </div>

    </>
  )
}
