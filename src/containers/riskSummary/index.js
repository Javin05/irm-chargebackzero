import React from 'react'
import { Switch, Route } from 'react-router-dom'
import RiskSummary from '../../components/riskSummary/riskSummary'
import RiskSummaryNew from '../../components/riskSummary/riskSummaryNew'

function RiskSummaryPage (props) {
  
  return (
    <Switch>
      <Route path='/risk-summary/update/:id'>
        <RiskSummary />
      </Route>
      <Route path='/risk-summary/new-update/:id'>
        <RiskSummaryNew />
      </Route>
    </Switch>
  )
}

export default RiskSummaryPage