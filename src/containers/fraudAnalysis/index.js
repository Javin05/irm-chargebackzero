import React from 'react'
import { Switch, Route } from 'react-router-dom'
import FraudAnalysis from '../../components/fraudAnalysis/index'
import FraudAnalysisShopLens from '../../components/fraudAnalysisDetails/shopLens'
import { PageTitle } from '../../theme/layout/core'
import FraudAnalysisDetails from '../../components/fraudAnalysisDetails'

function FraudAnalyzer () {
  return (
    <Switch>
      <Route path="/fraudanalysis-tool/update/:id">
        <PageTitle breadcrumbs={[]}>Fraud Analysis Details</PageTitle>
        <FraudAnalysisDetails/>
      </Route>
      <Route path='/fraudanalysis-tool'>
       <PageTitle breadcrumbs={[]}>Fraud Analysis</PageTitle>
        <FraudAnalysis />
      </Route>
      <Route path='/fraudanalysis-tool/shoplens'>
       <PageTitle breadcrumbs={[]}>Fraud Analysis Details</PageTitle>
        <FraudAnalysisShopLens />
      </Route>

    </Switch>
  )
}

export default FraudAnalyzer
