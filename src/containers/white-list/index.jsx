import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom'
import Whitelist from '../../components/white-list'

function WhitelistContainer (props) {

    return (
        <Switch>
            <Route path='/white-list'>
                <Whitelist />
            </Route>
        </Switch>
    )
}

export default WhitelistContainer