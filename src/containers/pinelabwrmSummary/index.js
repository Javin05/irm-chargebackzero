import { Route, Switch } from "react-router-dom";
import PinelabwrmSummary from "../../components/dashboard/PinelabwrmSummary";
import { PageTitle } from "../../theme/layout/core";

function Dashboard(props) {
  return (
    <Switch>
      <Route path="/pinelabs-wrmdashboard">
        <PageTitle breadcrumbs={{}}>PineLab WRM Summary</PageTitle>
        <PinelabwrmSummary />
      </Route>
    </Switch>
  );
}

export default Dashboard;
