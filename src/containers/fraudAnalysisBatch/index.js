import React from 'react'
import { Switch, Route } from 'react-router-dom'
import FraudAnalysisBatchList from '../../components/fraudAnalysisBatch/index'
import { PageTitle } from '../../theme/layout/core'
import FraudAnalysisBatchDetails from '../../components/fraudAnalysisBatchDetails'

function FraudAnalyzerBatch () {
  return (
    <Switch>
      <Route path="/fraudanalysis-batchupload/update/:id">
        <PageTitle breadcrumbs={[]}>Fraud Analysis Batch Upload Details</PageTitle>
        <FraudAnalysisBatchDetails />
      </Route>
      <Route path='/fraudanalysis-batchupload'>
       <PageTitle breadcrumbs={[]}>Fraud Analysis Batch Upload</PageTitle>
        <FraudAnalysisBatchList />
      </Route>
    </Switch>
  )
}

export default FraudAnalyzerBatch
