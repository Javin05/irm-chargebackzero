import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import { 
    queuesActions, 
    dashboardFilterActions,
    dashboardTransactionActions
} from '../../store/actions'
import { connect } from 'react-redux'
import Select from 'react-select'
import MainDashboard from '../../components/main-dashboard'

function MainDashboardRoute (props) {
    const { 
        getQueueslistDispatch, 
        queuesLists, 
        getFilterDispatch, 
        dashboardFilter, 
        dashboardLoading,
        getTransactionDispatch,
        transactionData, 
        transactionLoading
    } = props
    const [filter, setFilter] = useState(localStorage.getItem('dashboardType')!==null? localStorage.getItem('dashboardType') : "today");
    const [dashboard, setDashboard] = useState("dashboard");
    const [queueid, setQueueid] = useState(localStorage.getItem('dashboardKey') !==null? localStorage.getItem('dashboardKey') : "624fc67fae69dc1e03f47ebd" ); 
    const [chartdata, setChartdata] = useState(null);
    const [transdata, setTransdata] = useState(null);
    let options = [];

    const filterHandler = (filter) => {
        setFilter(filter)
        localStorage.setItem('dashboardType', filter);

        let queryParams= `summary?type=${filter}&queue_id=${queueid}`;
        getFilterDispatch(queryParams)

        let queryParams1= `?type=${filter}&queue_id=${queueid}`;
        getTransactionDispatch(queryParams1)
    }
    const chooseDashboardHandler = (e) => {
        setDashboard(e.label)
        setQueueid(e.value)
        localStorage.setItem('dashboardKey', e.value);

        let queryParams= `summary?type=${filter}&queue_id=${e.value}`;
        getFilterDispatch(queryParams)

        let queryParams1= `?type=${filter}&queue_id=${queueid}`;
        getTransactionDispatch(queryParams1)
    }

    if( queuesLists && queuesLists?.data ){
        queuesLists.data.forEach(element => {
            const obj = {
                'value':element._id, 
                'label':element.queueName
            }
            options.push(obj);
        });
    }

    useEffect(() => {
        getQueueslistDispatch()

        let queryParams= `summary?type=${filter}&queue_id=${queueid}`;
        getFilterDispatch(queryParams)

        let queryParams1= `?type=${filter}&queue_id=${queueid}`;
        getTransactionDispatch(queryParams1)

    }, [])

    useEffect(() => {
        if( dashboardFilter && dashboardFilter?.data ){   
            setChartdata(dashboardFilter.data[0]) 
        }
    }, [dashboardFilter])   

    useEffect(() => {
        if( transactionData && transactionData?.data ){ 
            let mFilter=filter;
            if(filter==="today"){
                mFilter="sevenDays"
            }
            else if(filter==="monthly"){
                mFilter="monthly"
            } 
            else if(filter==="year"){
                mFilter="yearly"
            }
            setTransdata(transactionData.data[mFilter]) 
        }
    }, [transactionData])

    return (
        <Switch>
            <Route path='/main-dashboard'>
                <section className="custom-toolbar mb-4">
                    <div className="row">
                        <div className="col-md-6">
                            
                        </div>
                        <div className="col-md-6">
                            <ul className="filter-ul">
                                <li className={filter==='today'? "active" : ""}>
                                    <span onClick={()=>filterHandler("today")} className="btn btn-filter btn-sm mr-2">Today</span>
                                </li>
                                <li className={filter==='monthly'? "active" : ""}>
                                    <span onClick={()=>filterHandler("monthly")} className="btn btn-filter btn-sm mr-2">Month</span>
                                </li>
                                <li className={filter==='year'? "active" : ""}>
                                    <span onClick={()=>filterHandler("year")} className="btn btn-filter btn-sm mr-2">Year</span>
                                </li>
                                <li>
                                    { queueid !== null? (
                                        <Select 
                                            options={options}
                                            className="react-c-select" 
                                            onChange={chooseDashboardHandler}
                                            value={options.find(op => {
                                                return op.value === queueid
                                            })}/>
                                        ) : (
                                            <Select 
                                                options={options}
                                                className="react-c-select" 
                                                onChange={chooseDashboardHandler}/>
                                        ) 
                                    }
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
                <MainDashboard loading={dashboardLoading} chartdata={chartdata} transdata={transdata} filterType={filter}/>
            </Route>
        </Switch>
    )
}

const mapStateToProps = (state) => {
    const { queueslistStore, dashboardFilterStore, dashboardTransactionStore } = state;
    return {
        loading: queueslistStore && queueslistStore.loading ? queueslistStore.loading : false,
        queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
        dashboardLoading: dashboardFilterStore && dashboardFilterStore.dashboard_loading ? dashboardFilterStore.dashboard_loading : false,
        dashboardFilter: dashboardFilterStore && dashboardFilterStore.dashboard_data ? dashboardFilterStore.dashboard_data : {},
        transactionLoading: dashboardTransactionStore && dashboardTransactionStore.transaction_loading ? dashboardTransactionStore.transaction_loading : false,
        transactionData: dashboardTransactionStore && dashboardTransactionStore.transaction_data ? dashboardTransactionStore.transaction_data : {},
    }
}

const mapDispatchToProps = (dispatch) => ({
    getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
    getFilterDispatch: (params) => dispatch(dashboardFilterActions.dashboardFilterInit(params)),
    getTransactionDispatch: (params) => dispatch(dashboardTransactionActions.dashboardTransactionInit(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(MainDashboardRoute);