import { Switch, Route } from 'react-router-dom'
import WrmRtWebReport from '../../components/wrmRtWebReport/index'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/wrm-realtime-appstore-report-settings'>
        <PageTitle breadcrumbs={{}}>Wrm Realtime App Store Report</PageTitle>
        <WrmRtWebReport reportValue={"Realtime Appstore Report"}/>
      </Route>
    </Switch>
  )
}

export default Home
