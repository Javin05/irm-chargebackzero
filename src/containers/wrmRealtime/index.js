import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import WrmRealtime from '../../components/wrmRealtime/analyzeRTRiskManangement'
import SDKCBForm from '../../components/wrmManagement/SDK/sdkForm'

const issuerBankBreadCrumbs = [
  {
    title: 'Risk Management',
    path: '/wrm-riskmmanagement',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]
function IssuerBank() {
  return (
    <Switch>
      <Route path='/wrm-realtime'>
        <PageTitle breadcrumbs={{}}>Wrm Realtime Queue</PageTitle>
        <WrmRealtime />
      </Route>
      <Route path='/wrm-sdk'>
        <PageTitle breadcrumbs={{}}>WRM SDK</PageTitle>
        <SDKCBForm />
      </Route>
    </Switch>
  )
}

export default IssuerBank
