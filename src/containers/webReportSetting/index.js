import { Switch, Route } from 'react-router-dom'
import WebReport from '../../components/webReportSetting/index'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/web-report-settings'>
        <PageTitle breadcrumbs={{}}>Web Report</PageTitle>
        <WebReport reportValue={"Web Report"}/>
      </Route>
    </Switch>
  )
}

export default Home
