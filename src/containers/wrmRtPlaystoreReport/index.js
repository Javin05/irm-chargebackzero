import { Switch, Route } from 'react-router-dom'
import WrmRtWebReport from '../../components/wrmRtWebReport'
import { PageTitle } from '../../theme/layout/core'


function Home(props) {
  return (
    <Switch>
      <Route path='/wrm-realtime-playstore-report-settings'>
        <PageTitle breadcrumbs={{}}>Wrm Reatime Play Store Report</PageTitle>
        <WrmRtWebReport reportValue={"Realtime Playstore Report"}/>
      </Route>
    </Switch>
  )
}

export default Home