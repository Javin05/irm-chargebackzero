import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import RiskManagementList from '../../components/riskManagement/analyzeRiskManangement'
import AccountRiskSummary from '../../components/accountRiskSummary/riskSummary'

function IssuerBank () {
  return (
    <Switch>
      <Route path='/accounts-riskmanagement'>
        <PageTitle breadcrumbs={{}}>Account Risk Management</PageTitle>
        <RiskManagementList />
      </Route>
      <Route path='/account-risk-summary/update/:id'>
        <PageTitle breadcrumbs={{}}>Account Risk Management</PageTitle>
        <AccountRiskSummary />
      </Route>
    </Switch>
  )
}

export default IssuerBank
