import React from 'react'
import { Switch, Route } from 'react-router-dom'
import APIUsageSummaryAWS from '../../components/dashboard/APIUsageSummaryAWS'

function APIUsageSummary(props) {
    return (
        <Switch>
            <Route path='/apiusage-dashboard'>
                <APIUsageSummaryAWS />
            </Route>
        </Switch>
    )
}

export default APIUsageSummary