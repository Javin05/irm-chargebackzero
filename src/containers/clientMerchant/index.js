import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Clients from '../../components/clientManagement/clients'
import ClientOnboarding from '../../components/clientManagement/onboarding/clientOnboarding'
import AddMerchant from '../../components/clientManagement/merchant/addMerchant'
import { PageTitle } from '../../theme/layout/core'


const profileBreadCrumbs = [
  {
    title: 'Client Management',
    path: '/client-management',
    isSeparator: false,
    isActive: false
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false
  }
]

function RulesSelect() {
  return (
    <Switch>
      <Route path='/client-management'>
        <PageTitle breadcrumbs={profileBreadCrumbs}>Clients</PageTitle>
        <Clients />
      </Route>
      <Route path='/client-onboarding'>
        <PageTitle breadcrumbs={profileBreadCrumbs}>Client Onboarding</PageTitle>
        <ClientOnboarding />
      </Route>
      <Route path='/client-onboarding/update-merchant/:id'>
        <PageTitle breadcrumbs={profileBreadCrumbs}>Update Merchant</PageTitle>
        <AddMerchant />
      </Route>
    </Switch>
  )
}

export default RulesSelect
