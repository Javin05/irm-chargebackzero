import React from 'react'
import { Switch, Route } from 'react-router-dom'
import EDDRiskManagement from '../../components/EDDRiskManagement/eddRiskManagement'
import { PageTitle } from '../../theme/layout/core'


function EDDRiskManagementList(props) {
  return (
    <Switch>
      <Route path='/edd-riskmanagement'>
        <PageTitle breadcrumbs={{}}>EDD RiskManagement</PageTitle>
        <EDDRiskManagement />
      </Route>
    </Switch>
  )
}

export default EDDRiskManagementList