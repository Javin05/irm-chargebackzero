import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WrmRealtimeSummary from '../../components/wrmRealtimeSummary'
import { PageTitle } from '../../theme/layout/core'


function Dashboard(props) {
  return (
    <Switch>
      <Route path='/wrm-realtime-summary'>
        <PageTitle breadcrumbs={{}}>Wrm Realtime Summary</PageTitle>
        <WrmRealtimeSummary />
      </Route>
    </Switch>
  )
}

export default Dashboard
