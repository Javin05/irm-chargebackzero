import { Switch, Route } from 'react-router-dom'
import { PageTitle } from '../../theme/layout/core'
import WrmOperationManagement from '../../components/wrmOperationManagmement'
import WrmOperationDetails from '../../components/wrmOperationDetails/index'

function LimitedCheck() {
  return (
    <Switch>
       <Route path='/limited-check'>
        <PageTitle breadcrumbs={{}}>Wrm Queue</PageTitle>
        <WrmOperationManagement />
      </Route>
      
    </Switch>
  )
}

export default LimitedCheck