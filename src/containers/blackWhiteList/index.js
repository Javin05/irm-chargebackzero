import { Route, Switch } from "react-router-dom";
import BlackWhiteList from "../../components/BlackWhiteList/BlackWhiteList";
import BlackWhiteListForm from "../../components/BlackWhiteList/BlackWhiteListForm";
import { PageTitle } from "../../theme/layout/core";


function BlackWhiteListSelect() {
  return (
    <Switch>
      <Route path="/whitelist-blacklist">
        <PageTitle breadcrumbs={[]}>Black List & White List</PageTitle>
        <BlackWhiteList />
      </Route>
      <Route path="/whitelist-blacklist-form/add">
        <PageTitle breadcrumbs={[]}>Add  Black List & White List</PageTitle>
        <BlackWhiteListForm />
      </Route>
      <Route path="/whitelist-blacklist-forms/update/:id">
        <PageTitle breadcrumbs={[]}>Update Black List & White List</PageTitle>
        <BlackWhiteListForm />
      </Route>
    </Switch>
  );
}

export default BlackWhiteListSelect;
