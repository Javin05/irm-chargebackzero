import React from 'react'
import { Switch, Route } from 'react-router-dom'
import WRMAuditReportSummaryAWS from '../../components/dashboard/WRMAuditReportSummaryAWS'
function WRMAuditReportSummary(props) {
    return (
        <Switch>
            <Route path='/wrm-audit'>
                <WRMAuditReportSummaryAWS  />
            </Route>
        </Switch>
    )
}

export default WRMAuditReportSummary