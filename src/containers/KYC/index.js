import React from 'react'
import { Switch, Route } from 'react-router-dom'
import KYCList from '../../components/KYC/KYCPage'
import { PageTitle } from '../../theme/layout/core'


function Homes () {
  return (
    <>
    <Switch>
      <Route path='/KYC'>
        <PageTitle>KYC</PageTitle>
        <KYCList />
      </Route>
    </Switch>
    </>
  )
}

export default Homes
