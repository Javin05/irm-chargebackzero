import React from 'react'
import { Switch, Route } from 'react-router-dom'
import RealtimeSummary from '../../components/realtimeSummary/realtimeSummary'

function RealtimeSummaryPage (props) {
  return (
    <Switch>
      <Route path='/realtime-summary/update/:id'>
        <RealtimeSummary />
      </Route>
    </Switch>
  )
}

export default RealtimeSummaryPage