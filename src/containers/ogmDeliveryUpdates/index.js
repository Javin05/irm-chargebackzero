import React from 'react'
import { Switch, Route } from 'react-router-dom'
import OgmDeliveryUpdates from '../../components/ogmDeliveryUpdates'
import { PageTitle } from '../../theme/layout/core'


function OGMDeliveryUpdatesDashboard () {
  return (
    <Switch>
      <Route path='/ogm-delivery-updates'>
        <PageTitle breadcrumbs={{}}>OGM Delivery Updates</PageTitle>
        <OgmDeliveryUpdates />
      </Route>
    </Switch>
  )
}

export default OGMDeliveryUpdatesDashboard