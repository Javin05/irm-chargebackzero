import React from 'react'
import { Switch, Route } from 'react-router-dom'
import OgmForceUploadSummary from '../../components/ogmForceUploadSummary'
import { PageTitle } from '../../theme/layout/core'


function Dashboard(props) {
  return (
    <Switch>
      <Route path='/ogmForceUpload-summary'>
        <PageTitle breadcrumbs={{}}>Ogm Force Upload Summary</PageTitle>
        <OgmForceUploadSummary />
      </Route>
    </Switch>
  )
}

export default Dashboard
