import React from 'react'
import MCCCodeList from '../../components/mccCode';
import { Switch, Route } from 'react-router-dom'


const MCCCode = (props) => {
    return ( 
        <Switch>
        <Route path='/mcccode'>
            <MCCCodeList/>
        </Route>
    </Switch>
     );
}
 
export default MCCCode;