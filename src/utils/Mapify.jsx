import { Component } from 'react'
import GoogleMap from 'google-map-react'
import Marker from './map/Marker'
import Polyline from './map/Polyline'

class Mapify extends Component {
    constructor(props) {
        super(props)

        this.state = {
            mapsLoaded: false,
            map: null,
            maps: null
        }
    }
    onMapLoaded(map, maps) {
        this.fitBounds(map, maps)

        this.setState({
            ...this.state,
            mapsLoaded: true,
            map: map,
            maps: maps
        })
    }
    fitBounds(map, maps) {
        var bounds = new maps.LatLngBounds()
        for (let marker of this.props.AllMarkers) {
            bounds.extend(
                new maps.LatLng(marker.lat, marker.lng)
            )
        }
        map.fitBounds(bounds)
    }
    afterMapLoadChanges() {
        return (
            <div style={{display: 'none'}}>
            <Polyline
                map={this.state.map}
                maps={this.state.maps}
                markers={this.props.AllMarkers} />
            </div>
        )
    }

    render() {
        const {AllMarkers} = this.props;
        return (
            <GoogleMap
                bootstrapURLKeys={{key: 'PUT GOOGLE MAPS KEY HERE'}}
                style={{height: '100vh', width: '100%'}}
                defaultCenter={this.props.AllMarkers[0]}
                defaultZoom={this.props.zoom}
                onGoogleApiLoaded={({map, maps}) => this.onMapLoaded(map, maps)}>

                {AllMarkers && AllMarkers.map((mark, i) => (
                    <Marker key={i} text={mark.area} lat={mark.lat} lng={mark.lng} />
                ))}   
                {this.state.mapsLoaded ? this.afterMapLoadChanges() : ''}
            </GoogleMap>
        )
    }
}

export default Mapify