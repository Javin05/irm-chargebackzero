import { toAbsoluteUrl } from '../theme/helpers'

export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount'
export const DAEMON = '@@saga-injector/daemon'
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount'
export const colors = {
  oxfordBlue: '#181c32'
}

export const NEWDATA = {
  NEW: 'blink'
}

export const SECRETKYEY = {
  FINGERPRINT: 'XbbRNl6Nrbbs1ickBwcLj1BPUCkVrl8XdJ0j6n9J6M34nW7DtKJAVDojo2ohj63dd81PbhRaalQlOCfAKTFYwi0TFwkCSJXocgYSmmZcH4ZDpR8FBr8Hfqrpb9IElMqmLNR8dlOI23dfDdrIAipfzYdfbwD7b9wXtIBrTP6nOMuukt7soPzuhjS2t2bsYuaUIQSsXglIpzXzzYk4GZfXaADD1z9vac9ay7tCQ10vyzAEqbMlU5zJf5HmTKJpuJkf'
}

export const API = {
  API_KEY: 'API_KEY'
}

export const ERROR = {
  INVALID_RESPONSE: 'Invalid response',
  MSG: 'error'
}

export const EMPTY_DROPZONE = {
  MSG: 'Drag and drop here.',
  FILE_MSG: 'Upload file here. Only Json file are allowed.Allowed Size 1MB'
}

export const STATUS_BADGES = {
  ACTIVE: 'badge-success',
  INACTIVE: 'badge-warning text-black',
  PENDING: 'badge-info',
  DELETED: 'badge-danger',
  undefined: 'text-dark'
}

export const CHANGE_ICON = {
  green: 'bi bi-check-circle-fill text-success',
  orange: 'bi bi-exclamation-triangle-fill text-warning',
  red: 'bi bi-exclamation-triangle-fill text-danger',
}


export const FILE_FORMAT_TYPE_VIDEO = [
  'video/MP4',
  'video/mp4',
  'video/MOV',
  'video/mov',
  'video/WMV',
  'video/wmv',
  'video/AVI',
  'video/avi',
  'video/AVCHD',
  'video/avchd',
  'video/FLV',
  'video/flv',
  'video/F4V',
  'video/f4v',
  'video/SWF',
  'video/swf',
  'video/WEBM',
  'video/webm',
  'video/HTML5',
  'video/html5',
  'video/MPEG-2',
  'video/mpeg-2'
]

export const FILE_FORMAT_TYPE = [
  'image/jpeg',
  'image/png',
  'image/jpg',
  'image/svg+xml',
  'image/JPEG',
  'image/PNG',
  'image/JPG',
  'video/MP4',
  'video/mp4',
  'video/MOV',
  'video/mov',
  'video/WMV',
  'video/wmv',
  'video/AVI',
  'video/avi',
  'video/AVCHD',
  'video/avchd',
  'video/FLV',
  'video/flv',
  'video/F4V',
  'video/f4v',
  'video/SWF',
  'video/swf',
  'video/WEBM',
  'video/webm',
  'video/HTML5',
  'video/html5',
  'video/MPEG-2',
  'video/mpeg-2'
]

export const FIND_FORMATE = [
  'jpeg',
  'png',
  'jpg',
  'svg+xml',
  'JPEG',
  'PNG',
  'JPG',
]

export const FILE_UPLOAD_TYPE = [
  'image/jpeg',
  'image/png',
  'image/jpg'
]

export const RISKMANAGEMENT_EMAIL_DASHBOARD = {
  "Disposable Email": 'Temporary and throwaway mail servers commonly used by fraudsters and abusive users.',
  "First Seen": 'First time  this email address was analyzed',
  "Domain Age": 'Newly registered domains are often used by fraudsters & bots.',
  "Common Email Provider": 'Popular personal email services like gmail, yahoo, hotmail, etc.',
  "Spam Trap Score": 'Estimation of the email as a spam trap. Scrub values with "high" from your marketing lists to avoid deliverability issues.',
  "Frequent Complainer": 'Detects if this email frequently unsubscribes from marketing lists or reports email as SPAM.',
  "Connection Test Time Out": 'Indicate if the emails mail server timed out during the connection test due to not replying in a timely manner. Please retry using our API which allows for a longer timeout threshold.',
  "Recent Abuse": 'Has this email recently engaged in fraudulent activity across our network?',
  "Fraud Score": 'Overall risk analysis score (0 to 100) based on recent behavior, reputation, and machine learning. Values = 100 indicate confirmed fraud. Values less than 100 indicate the confidence level using machine learning techniques.',
  "Catch All": 'Indication if an email is likely to be a catch all.',
  "Data Leak": 'Signals if the email address has been exposed in a data breach.',
  "SMTP Score": '    -1 = invalid | 0 = mail server rejecting mail | 1 = temporary rejection error | 2 = catch all server | 3 = verified email.',
  "Overall Score": '0 = invalid | 1 = DNS valid, unreachable server | 2 = temporary rejection error | 3 = catch all server | 4 = verified email.',
}

export const VALID_EMAIL = [
  "Fields",
  "Email Analysis",
  "Disposable Email",
  "First Name",
  "First Seen",
  "Domain Age",
  "Generic/Role Based Account",
  "Common Email Provider",
  "Spam Trap Score",
  "Frequent Complainer",
  "Connection Test Time Out",
  "Recent Abuse",
  "Fraud Score",
  "Catch All",
  "Data Leak",
  "SMTP Score",
  "Overall Score"
]

export const RISKMANAGEMENT_WEBSITE_DASHBOARD = {
  "Safe": 'Can this URL be trusted?',
  "Suspicious": 'Indicates reputation issues and potentially malicious activity.',
  "Domain": 'Domain name of the final URL after all redirections.',
  "IP Address": 'The corresponding IP address for the URLs web server.',
  "Malware": 'Indicates if this domain has recently hosted malware, viruses, or C2 activity.',
  "Phishing": 'Indicates if this site has been recently associated with known phishing techniques.',
  "Risk Score": 'Overall threat score from 0 (clean) to 100 (high risk).',
  "Parked Domain": 'Is this domain currently parked?',
  "Spamming": 'Is this domain recently sending SPAM?',
  "HTTP Status Code": 'Status code of the HTTP request. 200 is most common.',
  "Page Size": 'Page size of URLs content in bytes.',
}

export const WEBSIT_VALIDATE = [
  "Safe",
  "Suspicious",
  "Domain",
  "IP Address",
  "Malware",
  "Phishing",
  "Risk Score",
  "Parked Domain",
  "Spamming",
  "HTTP Status Code",
  "Page Size",
]

export const DROPZONE_IMAGE_NAME_TYPES = {
  IMAGE: 'IMAGE',
  VIDEO: 'VIDEO',
  AUDIO: 'AUDIO',
  APPUSER: 'appuser',
  DOCUMENTS: 'FILES',
  USER_PROFILE: 'userprofile',
}

export const DROPZONE_IMAGE_FLAG = {
  PROFILES: 'PROFILES',
  UPLOAD_DAILIES: 'UPLOAD_DAILIES',
  APPUSER: 'appuser',
  STILS: 'STILS',
  AUDIO: 'AUDIO',
  DOCUMENTS: 'FILES'
}

export const MAP = {
  "BUSINESS ADDRESS": '/media/icons/duotune/maps/map020.png',
  INDIVIDUALADDRESS: '/media/icons/duotune/maps/map011.png',
  IP: '/media/icons/duotune/maps/map013.png',
  BillingAdderess: '/media/icons/duotune/maps/map016.png',
  ShippingAdderess: '/media/icons/duotune/maps/map019.png',
  PHONE: '/media/icons/duotune/maps/map011.png',
}

export const RISKSTATUSCOLOR = {
  APPROVED: 'sucess-color',
  HOLD: 'holding-color',
  PENDING: 'pending-color',
  REJECTED: 'danger-color',
}

export const KYC_STATUS = {
  APPROVED: 'bg-success',
  HOLD: 'bg-orange',
  PENDING: 'bg-warning',
  REJECTED: 'bg-danger',
}

export const QUEUE_ROUTING = {
  SUSPECT_ACCOUNTS_Q: '/risk-management',
  SUSPECT_TXN_Q: '/transactions',
  CHARGEBACK_Q: '/upcoming',
  KYC_Q: '/KYC',
  SUSPECT_KYC_Q: '/KYC',
  AML_Q: '/aml-queue',
  ATO_Q: '/upcoming',
  ONGOING_MONITORING_Q: '/ongoing-monitoring',
  WEB_RISK_MONITORING_Q: '/wrm-riskmmanagement',
  MERCHANT_ONBOARDING_Q: '/accounts-riskmanagement',
  TRANSACTION_LAUNDERING_Q: '/upcoming',
  SUSPECT_MERCHANT_ONBOARDING_Q: '/accounts-riskmanagement',
  SUSPECT_WEB_RISK_MONITORING_Q: '/wrm-riskmmanagement',
  SUSPECT_ONGOING_MONITORING_Q: '/ongoing-monitoring',
}

export const KYC_SCORE_STATUS = {
  NO: 'bi bi-x-circle-fill text-danger',
  YES: 'bi bi-check-circle-fill text-success'
}

export const MONITOR_STATUS = {
  NO: 'bi bi-circle-fill text-success fs-3',
  YES: 'bi bi-circle-fill text-danger fs-3'
}
export const CONDITON_DETECT = {
  APPROVED: 'bi bi-check-circle-fill text-success fs-3',
  REJECTED: 'bi bi-x-circle-fill text-danger fs-3',
  PENDING: 'bi bi-circle-fill text-orange fs-3'
}

export const AADHAR_MATCH = {
  MisMatch: 'bi bi-x-circle-fill text-danger',
  Match: 'bi bi-check-circle-fill text-success'
}

export const TXNSTATUS = {
  Success: 'badge-primary',
  Dispute: 'badge-orange text-black',
  Refund: 'badge-warning text-black',
  Chargeback: 'badge-danger'
}

export const DASHBOARD_SCORE_COLOR = {
  Orange: 'alert-warning',
  Green: 'alert-success',
  Red: 'alert-danger'
}

export const BUTTON_COLOR = {
  HOLD: 'btn-light-warning pull-right w-100px',
  PROCESSING: 'btn-light-dark pull-right w-130px',
  "COMPLETED": 'btn-light-success pull-right w-120px',
  "UPDATE STATUS": 'btn-light-success pull-right w-120px'
}

export const TAGSTATUSNAME = {
  HOLD: 'Resume',
  PROCESSING: 'Update Status',
  "UPDATE STATUS": 'Send Email'
}

export const RISKSTATUS = {
  QUEUED: 'badge-warning text-black',
  APPROVED: 'badge-success text-black',
  HOLD: 'badge-orange text-black',
  PENDING: 'badge-warning text-black',
  PROCESSING: 'badge-warning',
  REJECTED: 'badge-danger text-black',
  "MANUAL REVIEW": 'badge-orange text-black',
  "DATA CAPTURED": 'badge-secondary text-black',
  COMPLETED: 'badge-success text-white',
  "DATA PROCESSING": 'badge-warning text-black',
  "TAG PROCESSING": 'badge-secondary text-black',
  "TAG CAPTURED": 'badge-secondary text-black',
  "WAITING FOR REPORT": 'badge-secondary text-black',
  "PREPARING REPORT": 'badge-secondary text-black',
  "MINI KYC": 'badge-warning text-black',
  Completed: 'badge-success text-black',
  "In progress": 'badge-orange text-black',
  positive: "badge-success text-white",
  negative: "badge-danger text-white",
  accessible: "badge-success text-white",
  inaccessible: "badge-danger text-white",
  "UPDATE STATUS": 'badge-orange text-black',
  ACTIVE: 'badge-success text-black',
  INACTIVE: 'badge-warning text-black',
  VALIDATED: 'badge-primary text-black',
  "VALIDATE PROCESSING": 'badge-warning text-black',
  DELETED: 'badge-danger text-black',
  STARTED: 'badge-orange text-black',
  STOPPED: 'badge-danger text-black',
  FRAUD: "badge-secondary text-black",
  STARTED: 'badge-warning text-black',
  PENDING: 'badge-primary text-black',
  CLOSED: 'badge-success text-black',
  DUPLICATE: 'badge-secondary text-black',
  "IN-PROGRESS": 'badge-warning text-white',
  Approved: 'badge-success text-black',
  Rejected: 'badge-danger text-black',
  High: 'badge-orange text-black',
  Medium: "badge-info text-white",
  Low: 'badge-warning text-white',
  "No Data": 'badge-light text-black'
}

export const RiskPdfStatus = {
  Approved: {
    color: 'green',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  Rejected: {
    color: 'red',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  APPROVED: {
    color: 'green',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  REJECTED: {
    color: 'red',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  LOW: {
    color: 'green',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  MEDIUM: {
    color: 'orange',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  High: {
    color: 'red',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  HIGH: {
    color: 'red',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  "High Risk": {
    color: 'red',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  "Low Risk": {
    color: 'green',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  "Medium Risk": {
    color: 'orange',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  "--":{
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
  accessible: {
    marginRight: 5,
    padding: '2px 6px',
    borderRadius: 5,
    color: 'green',
    fontSize: 10,
    textAlign: 'end',
  },
  Inaccessible: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    color: 'red',
    fontSize: 10,
    textAlign: 'end',
  },
  inaccessible: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    color: 'red',
    fontSize: 10,
    textAlign: 'end',
  },
  positive: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    backgroundColor: 'green',
    color: 'white',
    fontSize: 10,
    width: '50%',
    textAlign: 'center'
  },
  "No Data": {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    color: 'black',
    fontSize: 10,
    textAlign: 'center'
  },
  HOLD: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    backgroundColor: 'orange',
    color: 'white',
    fontSize: 10,
  },
  PENDING: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    backgroundColor: 'yellow',
    color: 'black',
    fontSize: 10,
  },
  PROCESSING: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    backgroundColor: 'yellow',
    color: 'black',
    fontSize: 10,
  },
  negative: {
    marginRight: 5,
    padding: 3,
    borderRadius: 5,
    backgroundColor: 'red',
    color: 'white',
    fontSize: 10,
    textAlign: 'center',
  },
  "Refer": {
    color: 'blue',
    flex: 1,
    padding: 5,
    fontSize: "10px",
    fontWeight: 'bolder',
  },
};

export const PMASTATUS = {
  Yes: 'bi-x-circle-fill text-danger fs-3 d-flex',
}

export const KYC_TYPE_STATUS = {
  LLP: 'badge-success',
  Proprietorship: 'badge-orange text-black',
  Partnership: 'badge-warning text-black',
  "Public Limited": 'badge-danger',
  "Private Limited": 'badge-secondary text-black',
  Trust: 'badge-success text-black',
  Society: 'badge-warning text-black',
  NGO: 'badge-orange text-black',
  HUF: 'badge-success',
  SoleProp: 'badge-success',
  "CONTINUE WITH FULL KYC": 'btn-light-primary',
  "FULL KYC": 'btn-light-success'
}

export const REVIEWTYPE = {
  Auto: 'badge-success text-black',
  Manual: 'badge-orange text-black',
  "SUSPECTED ACCOUNT": 'badge-success text-black',
  "NOT SUSPECTED": 'badge-orange text-black'
}

export const AVSSTATUS = {
  M: 'badge-success text-black',
  N: 'badge-danger text-black',
  S: 'badge-danger text-black',
  P: 'badge-danger text-black',
  U: 'badge-danger text-black'
}

export const CVSSTATUS = {
  X: 'badge-success text-black',
  Y: 'badge-success text-black',
  Z: 'badge-danger text-black',
  A: 'badge-danger text-black',
  N: 'badge-danger text-black',
  U: 'badge-danger text-black'
}

export const STATUS_BADGE = {
  ACTIVE: 'badge-success',
  INACTIVE: 'badge-warning text-black',
  PENDING: 'badge-danger'
}

export const USER_ERROR = {
  COMPANYNAME: 'Company Name is required.',
  PRODUCTNAME: 'Product name is required.',
  MERCHANTNAME: 'Merchant Name is required.',
  DBA: 'DBA is required.',
  MERCHANT_ID: 'Merchant Id is required.',
  DISPUTE_METHOD: 'Dispute method is required.',
  DISPUTE_AMOUNT: 'Dispute amount is required.',
  DISPUTE_DATE: 'Dispute date is required.',
  CB_CASE: 'Chargeback case is required.',
  CB_DATE: 'Chargeback Date is required.',
  CB_DEADLINE: 'Chargeback Deadline is required.',
  CB_AMOUNT: 'Chargeback amount is required.',
  CB_STATUS: 'Chargeback status is required.',
  CB_TYPE: 'Chargeback Type is required.',
  CARD_SIX: 'Card first six number is required.',
  CARD_FOUR: 'Card last four digit is required.',
  CARD_TYPE: 'Card Type is required.',
  CB_REASON: 'Chargeback reason is required.',
  TD_DATE: 'Transaction Date is required.',
  TD_AMT: 'Transaction amount is required.',
  TD_NO: 'Transaction number is required.',
  SA_TY: 'Sale Type is required.',
  CUS_IP: 'Cutomer IP is required.',
  CUS_PH: 'Customer Phone is required.',
  AFF_ID: 'Affiliate ID is required.',
  AVS_MATCH: 'AVS is required.',
  CVV: 'CVV is required.',
  CUS_NA: 'Customer Name is required.',
  AUTH_CODE: 'Authentication code is required.',
  COUNTRY: 'Country is required. ',
  NON_US: 'Non US country is required.',
  MERCHANT_DESCRIPTOR: 'Merchant Descriptor is required.',
  ARN: 'ARN is required.',
  TRANSACTION_CODE: 'Transaction Code is required.',
  TRANSACTION_ID: 'Transaction ID is required.',
  TRANSACTION_AMOUNT: 'Transaction Amount is required.',
  TRANSACTION_CURRENCY: 'Transaction Currency is required.',
  AVS_CODE: 'AVS Code is required.',
  CVV_CODE: 'CVV Code is required.',
  SECURE_CODE: 'Secure Code is required.',
  RE_FUNDED: 'Refunded is required.',
  ISSUE_BANK: 'IssueBank is required.',
  CARD_COUNTRY: 'Card Country is required.',
  BILLING_COUNTRY: 'Billing Country is required.',
  BILLING_STATE: 'Billing State is required.',
  BILLING_CITY: 'Billing City is required.',
  BILLING_ZIPCODE: 'Billing ZipCode is required.',
  ACC_USERNAME: 'Account UserName is required.',
  EMAIL: 'Email is required.',
  PHONE_NUMBER: 'Phone Number is required.',
  ACC_ACTIVEDATE: 'Account Active Date is required.',
  PURCHASE_DATE: 'First Purchase Date is required.',
  LAST_LOGIN: 'Last Login is required.',
  IP_ADDRESS: 'IP Address is required.',
  IP_LOCATION: 'IP Location is required.',
  DEVICE_NAME: 'Device Name is required.',
  LOGIN_HISTORY: 'Login History is required.',
  TRANSACTION_HISTORY: 'Transaction History is required.',
  PRODUCT_TYPE: 'Product Type is required.',
  PRODUCT_NAME: 'Product Name is required.',
  PRO_ACQ_DATE: 'Product Acquisation Date is required.',
  DELIVERY_DATE: 'Delivery Date is required.',
  TRACKING_NO: 'Tracking Number is required.',
  SHIPPING_COUNTRY: 'Shipping Country is required.',
  SHIPPING_STATE: 'Shipping State is required.',
  SHIPPING_CITY: 'Shipping City is required.',
  SHIPPING_ADDRESS: 'Shipping Address is required.',
  SHIPPING_ZIPCODE: 'Shipping ZipCode is required.',
  WEBSITE: 'Website is required.',
  DESCRIPTION: 'Description is required.',
  BENIFITS: 'Benifits is required.',
  SPECIFICINFO: 'Specific Info is required.',
  CONTACT_URL: 'Contact URL is required.',
  LIVE_CHAT: 'Support Live Chat is required.',
  TERMS_URL: 'Terms and Condition URL is required.',
  REFUND_URL: 'Refund Policy URL is required.',
  FILE: 'File is required. ',
  EMAIL_REQUIRED: 'Email address is required.',
  EMAIL_INVALID: 'Email address is invalid.',
  PASSWORD_REQUIRED: 'Password is required.',
  PASSWORD_MIN_MAX_LENGTH: 'Should be greater than 6 or less than 14 digits.',
  PASSWORD_INVALID: 'Please enter valid password.',
  CONFIRM_PASSWORD_REQUIRED: 'Confirm password is required.',
  PASSWORD_SAME: 'Password and Confirm password should be same.',
  ALERT_ID: 'Alert Id is required.',
  ALERT_DATE: 'Alert Date is required.',
  ALERT_TYPE: 'Alert Type is required.',
  TRNS_TYPE: 'Transaction Type is required.',
  UPI_ID: 'UPI Transaction ID is required.',
  CUS_FNAME: 'Customer First Name is required.',
  CARD_BIN: 'Card Bin is required. ',
  EXP_MONTH: 'Expiration Month is required.',
  EXP_YEAR: 'Expiration Year is required.',
  CUS_LNAME: 'Customer Last Name is required.',
  DES: 'Descriptor is required.',
  DES_CONTACT: 'Descriptor Contact is required.',
  DES_CONTACT_INVALID: 'Descriptor Contact is invalid.',
  NOT_TYPE: 'Notification Type is requirted.',
  BANK: 'Bank Selection is required.',
  CARD_SIX_INVALID: 'Card First Six digits is invalid.',
  CARD_FOUR_INVALID: 'Card Last Four digits is invalid.',
  PROVIDER: 'Provider is required. ',
  RES_DATE: 'Resolution Date is required.',
  DURATION: 'Duration is required.',
  MERCHANT: 'Merchant is required.',
  REFUND_STATUS: 'Refund Status is required.',
  REFUND_AMOUNT: 'Refund Amount is required.',
  REFUND_DATE: 'Refund Date is required.',
  REFUND_DATE: 'Refund Date is required.',
  ADDRESS: 'Address is required.',
  PHONE: 'Phone is required.',
  DEVICE_ID: 'Device ID is required.',
  F_NAME: 'First Name is required.',
  L_NAME: 'Last Name is required.',
  COMPANY_NAME: 'Company Name is required.',
  P_EMAIL: 'Personal email is required.',
  B_EMAIL: 'Buisness email is required.',
  BUSINESS_PHONE: 'Business Phone is required.',
  BUSINESS_ADDRESS: 'Business Address is required.',
  REASON: 'Reason is required.',
  UPDATE_QUEUE: 'Update queue is required.',
  WEBSITE: 'Website is required.',
  QUEUE_NAME: 'Queue Name is required.',
  SLA_HIGH: 'SlaHigh is required.',
  SLA_MEDIUM: 'SlaMedium is required.',
  SLA_LOW: 'SlaLow is required.',
  PHONE_NUMBER_INVALID: 'Phone Number is invalid. (ex: 555-555-1234)',
  ADDRESS_REQUIRED: 'Address is required.',
  ZIPCODE_REQUIRED: 'Zip Code is required.',
  ZIPCODE_INVALID: 'Zip Code is should be in 6 digits.',
  INDUSTRY_REQUIRED: 'Industry is required.',
  ROLE_REQUIRED: 'Role is required.',
  STATE_REQUIRED: 'State is required.',
  CITY_REQUIRED: 'City is required.',
  GSTIN_REQUIRED: 'GSTIN is required.',
  CLIENT_ID: 'Client is required.',
}

export const DATE = {
  TIME: 'Time',
  TIME_INTERVAL: 15,
  TWELVE_HRS_FORMAT: 'h:mm A',
  DATE_TIME_FORMAT: ' D/MM/YYYY, h:mm A',
  DATE_ONLY: 'dd/MM/yyyy',
  DATE_FOR_PICKER: 'yyyy/MM/dd',
  DATE_MONTH_ONLY: 'dd/MM',
  DATE_MONTH_ONLY__: 'DD/MM',
  MONTH_ONLY: 'MM',
  YEAR_ONLY: 'yyyy',
  EXAMPLE_DATE: '2017-03-13',
  DATE_FORMAT: 'YYYY-MM-DD',
  DATE_FORMAT_MINS: 'MMM-DD-YYYY, hh:mm',
  DATE_FOR_PICKER_MONTH: 'MM',
  DATE_FORMAT_MINS_SECS: 'yyyy-mm-dd, hh:mm:ss',
}

export const RISKAPPROVED = ["APPROVED"]

export const REGEX = {
  PHONEPLUS: /^\+\d{1,3}\d{10}$/,
  DECIMAL: /^[0-9]+(\.[0-9]+)?$/,
  EMAIL: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
  DOB: /^\d{2}[-/.]\d{2}[-/.]\d{4}$/,
  PHONE: /^[0][1-9]\d{9}$|^[1-9]\d{9}$/,
  NUMERIC: /^[0-9.]+$/,
  ACCOUNT_NUMBER: /^\d{9,18}$/,
  IFSC_NUMBER: /^[A-Z]{4}[0][A-Z0-9]{6}$/,
  PASSWORD: /^[a-zA-Z0-9@#^\-_$]*$/,
  PASSWORD_MIN_MAX_LENGTH: /^[a-zA-Z0-9@#^\-_$]{6,14}$/,
  TEXT: /^[a-zA-Z_ ]*$/,
  ALPHA_NUMERIC: /^[a-zA-Z0-9 /]*$/,
  ALPHA_NUMERIC_CHARS: /^[a-zA-Z0-9@#^\-_$/]*$/,
  ALPHA_NUMERIC_CHARS_SPACE: /^[a-zA-Z0-9@^\-_.,àâçéèêëîïôûùüÿñæœ +g]*$/,
  ALPHA_CHARS_SPACE: /^[a-zA-Z^-_$.,àâçéèêëîïôûùüÿñæœ +g]*$/,
  ZIP_CODE: /^([0-9]){5,6}$/,
  NUMERIC_SIX_DIGITS: /^([0-9]){1,6}$/,
  NUMERIC_FOUR_DIGITS: /^([0-9]){4}$/,
  SPACE: / +/g,
  WEBSITE_URL: /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
  NUMERIC_DIGITS: /^[6-9]\d{0,9}$/,
  WEBSITE_URLS: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
  PHONE_NUMBER_1: /^[0-9]+(-[0-9]+)+$/,
  PHONE_NUMBER: /^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$/,
  NUMERIC_CHARS: /^[0-9-_]+$/,
  ALPHA_UPPER_CASE: /^[A-Z]*$/,
  ALPHA_NUMERIC_SPECIAL_CHARS: /^[a-zA-Z0-9@^\-_:/'"{}|.,àâçéèêëîïôûùüÿñæœ +g]*$/,
  CAID_NUMBER: /^([0-9]){6,8}$/,
  BIN_NUMBER: /^([0-9]){4,6}$/,
  URL: /^(ftp|http|https):\/\/[^ "]+\.[a-zA-Z]{2,4}$/,
  URL_ONE: /^(ftp|http|https):\/\/[^ "]+$/,
  LENGTH: /^(?!.*  )(?=.*[\w-])[\w -]{50,10000}$/,
  PAN: /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/,
  CIN: /^([L|U]{1})([0-9]{5})([A-Za-z]{2})([0-9]{4})([A-Za-z]{3})([0-9]{6})$/,
  GSTIN_REGEX: /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/,
  IP_ADDRESS: /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$/
}

export const RESPONSE_STATUS = {
  SUCCESS: 'ok',
  ERROR: 'error'
}

export const SESSION = {
  TOKEN: 'token',
  EXPIRED: 'session_expired',
  RESET_TOKEN: 'resetToken',
  AUTH_FAILED: 'Auth failed',
  LAST_INTERACTION_TIME: 'lastInteractionTime',
  IDLE_TIME: 30
}

export const ORDER_INSIGHT_LAYOUT = {
  EXPORT: 'exportPIselectedCols',
  SEARCH: 'searchPIselectedCols',
  TABLE_LAYOUT: 'tableLayoutPIselectedCols'
}

export const CHARGEBACK_LAYOUT = {
  EXPORT: 'exportCBselectedCols',
  SEARCH: 'searchCBselectedCols',
  TABLE_LAYOUT: 'tableLayoutCBselectedCols'
}

export const INQUIRY_ALERT_LAYOUT = {
  EXPORT: 'export',
  SEARCH: 'search',
  TABLE_LAYOUT: 'tableLayout'
}

export const LAYOUT_DETAILS = {
  EXPORT_IBA: 'exportIBAselectedCols',
  SEARCH_IBA: 'searchIBAselectedCols',
  TABLE_LAYOUT_IBA: 'tableLayoutIBAselectedCols'
}

export const PREVENTION_LAYOUT = {
  EXPORT: 'exportPAselectedCols',
  SEARCH: 'searchPAselectedCols',
  TABLE_LAYOUT: 'tableLayoutPAselectedCols'
}

export const ENV = {
  SDK_URL: process.env.REACT_APP_SDK_URL
}

export const GOOGLE_MAP = {
  KEY: process.env.REACT_APP_SDK_URL
}

export const AADHAAR_FRONT = {
  Aadhaar: 'aadharFront',
  Passport: 'passportFront',
  VoterId: 'voterIdFront'
}

export const ORGANIZATIONS_TITLE = {
  LLP: 'Add your LLP and gstNumber',
  Proprietorship: 'Add your GST details',
  Partnership: 'Add your GST details',
  "Public Limited": 'Add your CIN and GSTIN',
  "Private Limited": 'Add your CIN and GSTIN',
  Trust: 'Add your GST details',
  Society: 'Add your GST details',
  NGO: 'Add your GST details',
  HUF: 'Add your GST details',
  SoleProp: 'Add your GST details',
}

export const FRONT_TYPE_VERIFY = [
  'aadharFront',
  'passportFront',
  'voterIdFront'
]

export const KYC_URL = {
  IMAGE_PREVIEW_URL: process.env.REACT_APP_IMAGE_PREVIEW_URL
}


export const AADHAAR_BACK = {
  Aadhaar: 'aadharBack',
  Passport: 'passportBack',
  VoterId: 'voterIdBack'
}

export const IDPROFF = {
  Aadhaar: 'Aadhaar',
  Passport: 'Passport',
  VoterId: 'Voter Id'
}

export const STATUS_RESPONSE = {
  SUCCESS_MSG: 'ok',
  ERROR_MSG: 'error'
}

export const ALERT_TYPE = {
  SELECT: '',
  INQUIERY: 'Inquiry',
  NOTIFICATION: 'Notification'
}

export const MONTH = {
  SELECT: '',
  INQUIERY: 'Inquiry',
  NOTIFICATION: 'Notification'
}

export const NOTIFICATION_TYPE = {
  SELECT: '',
  BLANK_SPACE: 'Blank Space',
  CANCEL_REQURRING: 'Cancel Recurring',
  CHARGEBACK: 'Chargeback',
  EXCEPTION_UNKOWN: 'Exception or Unknown',
  FRAUD_ALERT: 'Fraud Alert'
}


export const BLACKLIST = {
  "not blacklisted": 'bi bi-check-circle-fill text-success',
  blacklisted: 'bi bi-x-circle-fill text-danger'
}

export const FINDBLACKLIST = {
  "not blacklisted": 'No',
  blacklisted: 'Yes'
}

export const SWEET_ALERT_MSG = {
  DELETE: 'Deleted Successfully',
  CREATE: 'Created Successfully',
  UPDATE: 'Updated Successfully',
  CONFIRMATION_TEXT: 'Are you sure!',
  UPDATE_QUEUE: 'Want To Update This Queues !',
  REJECT: 'Want To Reject !',
  HOLD: 'Want To Hold !',
  APPROVE: 'Want To Approve !',
  DELETE_RULES: 'Want to delete this Rules !',
  DELETE_LIST: 'Want to delete this Data !',
  DELETE_BLOCKLIST: 'Want to delete this Block List !',
  DELETE_WHITELIST: 'Want to delete this Whitelist !',
  DELETE_WATCHLIST: 'Want to delete this Watchlist !',
  DELETE_CITY: 'Want to delete this city !',
  DELETE_STATE: 'Want to delete this state !',
  DELETE_AREA: 'Want to delete this area !',
  DELETE_ADVERTISER: 'Want to delete this Advertiser !',
  DELETE_OFFERS: 'Want to delete this Offers !',
  DELETE_MEMBERS: 'Want to delete this Members !',
  DELETE_MANAGER: 'Want to delete this State Manager !',
  DELETE_EXCUTIVE: 'Want to delete this Executive !',
  DELETE_FRANCHISE: 'Want to delete this Franchise',
  DELETE_WEB: 'Want to delete this Web Risk Analysis !',
  UPDATE_WEB: 'Want To Update This Web Risk Analysis !',
  UPDATE_ASSIGN: 'Want To Assign This Members !',
  DELETE_CLIENT: 'Want To Delete The Client Category?',
  DELETE_REPORT: 'Want To Delete The Report params?',
}

export const HEADER = {
  TOKEN: 'Authorization',
  CONTENT_TYPE: 'application/json',
  MULTIPART_CONTENT_TYPE: 'multipart/form-data,boundary=----WebKitFormBoundaryyrV7KO0BoCBuDbTL',
  TIMEOUT: 46000000,
  BEARER: 'Bearer'
}

export const DROPZONE_MESSAGES = {
  JSON_INVALID: 'Uploaded file is not a valid json file.',
  IMAGE_INVALID: 'Uploaded file is not a valid image. Only JPG, PNG and JPEG files are allowed.',
  VIDEO_INVALID: 'Uploaded file is not a valid video format.',
  AUDIO_INVALID: 'Uploaded file is not a valid audio format.',
  DOCUMENT_INVALID: 'Uploaded file is not a valid document format.',
  CSV_INVALID: 'Uploaded Document is not a valid document.Only CSV files are allowed',
  EXCEL_INVALID: 'Uploaded Document is not a valid document.Only Excel files are allowed',
  PDF_INVALID: 'Uploaded file is not a valid Pdf format.',
  ZIP_INVALID: 'Uploaded file is not a valid zip file.',
}

export const FILE_FORMAT_CB_DOCUMENT = [
  'text/csv',
  'application/pdf',
  'application/msword',
  'application/vnd.ms-excel',
  'text/plain',
  'image/gif',
  'image/vnd.adobe.photoshop',
]

export const FILE_EXCEL_DOCUMENT = [
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
]

export const FILE_CSV_DOCUMENT = [
  'text/csv',
]

//onboarding
export const VIEW_PERMISSION = [
  "view"
]
export const CREATE_PERMISSION = [
  "create"
]
export const UPDATE_PERMISSION = [
  "update"
]
export const DELETE_PERMISSION = [
  "delete"
]
export const IMPORT_PERMISSION = [
  "import"
]
export const EXPORT_PERMISSION = [
  "export"
]
export const UPDATE_DELETE_PERMISSION = [
  "update", "delete"
]
export const API_URL = process.env.REACT_APP_API_URL
export const CB_URL = `${process.env.REACT_APP_CHARGEBACK_URL}/uploads`
export const DOCUMENT_URL = process.env.REACT_APP_UPLOAD_URL

export const KYC_VERIFY = {
  NO: 'badge-danger',
  YES: 'badge-success'
}

export const KYC_VERIFY_DATA = {
  NO: 'Unverified',
  YES: 'Verified'
}

export const refundVerifiOptions = [
  { value: "Partially", label: "Partially" },
  { value: "Fully", label: "Fully" },
]

export const currencyOptions = [
  { value: "AED", label: "United Arab Emirates Dirham" },
  { value: "AFN", label: "Afghan Afghani" },
  { value: "ALL", label: "Albanian Lek" },
  { value: "AMD", label: "Armenian Dram" },
  { value: "ANG", label: "Netherlands Antillean Guilder" },
  { value: "AOA", label: "Angolan Kwanza" },
  { value: "ARS", label: "Argentine Peso" },
  { value: "AUD", label: "Australian Dollar" },
  { value: "AWG", label: "Aruban Florin" },
  { value: "AZN", label: "Azerbaijani Manat" },
  { value: "BAM", label: "Bosnia-Herzegovina Convertible Mark" },
  { value: "BBD", label: "Barbadian Dollar" },
  { value: "BDT", label: "Bangladeshi Taka" },
  { value: "BGN", label: "Bulgarian Lev" },
  { value: "BHD", label: "Bahraini Dinar" },
  { value: "BIF", label: "Burundian Franc" },
  { value: "BMD", label: "Bermudan Dollar" },
  { value: "BND", label: "Brunei Dollar" },
  { value: "BOB", label: "Bolivian Boliviano" },
  { value: "BRL", label: "Brazilian Real" },
  { value: "BSD", label: "Bahamian Dollar" },
  { value: "BTC", label: "Bitcoin" },
  { value: "BTN", label: "Bhutanese Ngultrum" },
  { value: "BWP", label: "Botswanan Pula" },
  { value: "BYN", label: "Belarusian Ruble" },
  { value: "BZD", label: "Belize Dollar" },
  { value: "CAD", label: "Canadian Dollar" },
  { value: "CDF", label: "Congolese Franc" },
  { value: "CHF", label: "Swiss Franc" },
  { value: "CLF", label: "Chilean Unit of Account (UF)" },
  { value: "CLP", label: "Chilean Peso" },
  { value: "CNH", label: "Chinese Yuan (Offshore)" },
  { value: "CNY", label: "Chinese Yuan" },
  { value: "COP", label: "Colombian Peso" },
  { value: "CRC", label: "Costa Rican Colón" },
  { value: "CUC", label: "Cuban Convertible Peso" },
  { value: "CUP", label: "Cuban Peso" },
  { value: "CVE", label: "Cape Verdean Escudo" },
  { value: "CZK", label: "Czech Republic Koruna" },
  { value: "DJF", label: "Djiboutian Franc" },
  { value: "DKK", label: "Danish Krone" },
  { value: "DOP", label: "Dominican Peso" },
  { value: "DZD", label: "Algerian Dinar" },
  { value: "EGP", label: "Egyptian Pound" },
  { value: "ERN", label: "Eritrean Nakfa" },
  { value: "ETB", label: "Ethiopian Birr" },
  { value: "EUR", label: "Euro" },
  { value: "FJD", label: "Fijian Dollar" },
  { value: "FKP", label: "Falkland Islands Pound" },
  { value: "GBP", label: "British Pound Sterling" },
  { value: "GEL", label: "Georgian Lari" },
  { value: "GGP", label: "Guernsey Pound" },
  { value: "GHS", label: "Ghanaian Cedi" },
  { value: "GIP", label: "Gibraltar Pound" },
  { value: "GMD", label: "Gambian Dalasi" },
  { value: "GNF", label: "Guinean Franc" },
  { value: "GTQ", label: "Guatemalan Quetzal" },
  { value: "GYD", label: "Guyanaese Dollar" },
  { value: "HKD", label: "Hong Kong Dollar" },
  { value: "HNL", label: "Honduran Lempira" },
  { value: "HRK", label: "Croatian Kuna" },
  { value: "HTG", label: "Haitian Gourde" },
  { value: "HUF", label: "Hungarian Forint" },
  { value: "IDR", label: "Indonesian Rupiah" },
  { value: "ILS", label: "Israeli New Sheqel" },
  { value: "IMP", label: "Manx pound" },
  { value: "INR", label: "Indian Rupee" },
  { value: "IQD", label: "Iraqi Dinar" },
  { value: "IRR", label: "Iranian Rial" },
  { value: "ISK", label: "Icelandic Króna" },
  { value: "JEP", label: "Jersey Pound" },
  { value: "JMD", label: "Jamaican Dollar" },
  { value: "JOD", label: "Jordanian Dinar" },
  { value: "JPY", label: "Japanese Yen" },
  { value: "KES", label: "Kenyan Shilling" },
  { value: "KGS", label: "Kyrgystani Som" },
  { value: "KHR", label: "Cambodian Riel" },
  { value: "KMF", label: "Comorian Franc" },
  { value: "KPW", label: "North Korean Won" },
  { value: "KRW", label: "South Korean Won" },
  { value: "KWD", label: "Kuwaiti Dinar" },
  { value: "KYD", label: "Cayman Islands Dollar" },
  { value: "KZT", label: "Kazakhstani Tenge" },
  { value: "LAK", label: "Laotian Kip" },
  { value: "LBP", label: "Lebanese Pound" },
  { value: "LKR", label: "Sri Lankan Rupee" },
  { value: "LRD", label: "Liberian Dollar" },
  { value: "LSL", label: "Lesotho Loti" },
  { value: "LYD", label: "Libyan Dinar" },
  { value: "MAD", label: "Moroccan Dirham" },
  { value: "MDL", label: "Moldovan Leu" },
  { value: "MGA", label: "Malagasy Ariary" },
  { value: "MKD", label: "Macedonian Denar" },
  { value: "MMK", label: "Myanma Kyat" },
  { value: "MNT", label: "Mongolian Tugrik" },
  { value: "MOP", label: "Macanese Pataca" },
  { value: "MRU", label: "Mauritanian Ouguiya" },
  { value: "MUR", label: "Mauritian Rupee" },
  { value: "MVR", label: "Maldivian Rufiyaa" },
  { value: "MWK", label: "Malawian Kwacha" },
  { value: "MXN", label: "Mexican Peso" },
  { value: "MYR", label: "Malaysian Ringgit" },
  { value: "MZN", label: "Mozambican Metical" },
  { value: "NAD", label: "Namibian Dollar" },
  { value: "NGN", label: "Nigerian Naira" },
  { value: "NIO", label: "Nicaraguan Córdoba" },
  { value: "NOK", label: "Norwegian Krone" },
  { value: "NPR", label: "Nepalese Rupee" },
  { value: "NZD", label: "New Zealand Dollar" },
  { value: "OMR", label: "Omani Rial" },
  { value: "PAB", label: "Panamanian Balboa" },
  { value: "PEN", label: "Peruvian Nuevo Sol" },
  { value: "PGK", label: "Papua New Guinean Kina" },
  { value: "PHP", label: "Philippine Peso" },
  { value: "PKR", label: "Pakistani Rupee" },
  { value: "PLN", label: "Polish Zloty" },
  { value: "PYG", label: "Paraguayan Guarani" },
  { value: "QAR", label: "Qatari Rial" },
  { value: "RON", label: "Romanian Leu" },
  { value: "RSD", label: "Serbian Dinar" },
  { value: "RUB", label: "Russian Ruble" },
  { value: "RWF", label: "Rwandan Franc" },
  { value: "SAR", label: "Saudi Riyal" },
  { value: "SBD", label: "Solomon Islands Dollar" },
  { value: "SCR", label: "Seychellois Rupee" },
  { value: "SDG", label: "Sudanese Pound" },
  { value: "SEK", label: "Swedish Krona" },
  { value: "SGD", label: "Singapore Dollar" },
  { value: "SHP", label: "Saint Helena Pound" },
  { value: "SLL", label: "Sierra Leonean Leone" },
  { value: "SOS", label: "Somali Shilling" },
  { value: "SRD", label: "Surinamese Dollar" },
  { value: "SSP", label: "South Sudanese Pound" },
  { value: "STD", label: "São Tomé and Príncipe Dobra (pre-2018)" },
  { value: "STN", label: "São Tomé and Príncipe Dobra" },
  { value: "SVC", label: "Salvadoran Colón" },
  { value: "SYP", label: "Syrian Pound" },
  { value: "SZL", label: "Swazi Lilangeni" },
  { value: "THB", label: "Thai Baht" },
  { value: "TJS", label: "Tajikistani Somoni" },
  { value: "TMT", label: "Turkmenistani Manat" },
  { value: "TND", label: "Tunisian Dinar" },
  { value: "TOP", label: "Tongan Pa'anga" },
  { value: "TRY", label: "Turkish Lira" },
  { value: "TTD", label: "Trinidad and Tobago Dollar" },
  { value: "TWD", label: "New Taiwan Dollar" },
  { value: "TZS", label: "Tanzanian Shilling" },
  { value: "UAH", label: "Ukrainian Hryvnia" },
  { value: "UGX", label: "Ugandan Shilling" },
  { value: "USD", label: "United States Dollar" },
  { value: "UYU", label: "Uruguayan Peso" },
  { value: "UZS", label: "Uzbekistan Som" },
  { value: "VEF", label: "Venezuelan Bolívar Fuerte (Old)" },
  { value: "VES", label: "Venezuelan Bolívar Soberano" },
  { value: "VND", label: "Vietnamese Dong" },
  { value: "VUV", label: "Vanuatu Vatu" },
  { value: "WST", label: "Samoan Tala" },
  { value: "XAF", label: "CFA Franc BEAC" },
  { value: "XAG", label: "Silver Ounce" },
  { value: "XAU", label: "Gold Ounce" },
  { value: "XCD", label: "East Caribbean Dollar" },
  { value: "XDR", label: "Special Drawing Rights" },
  { value: "XOF", label: "CFA Franc BCEAO" },
  { value: "XPD", label: "Palladium Ounce" },
  { value: "XPF", label: "CFP Franc" },
  { value: "XPT", label: "Platinum Ounce" },
  { value: "YER", label: "Yemeni Rial" },
  { value: "ZAR", label: "South African Rand" },
  { value: "ZMW", label: "Zambian Kwacha" },
  { value: "ZWL", label: "Zambian Dollar" },
]

export const API_MESSAGES = {
  SOMETHING_WRONG: 'Something went wrong. Please try again later!',
}

export const CRM_ERROR = {
  API_KEY_REQUIRED: 'API Key is required.',
  API_KEY_INVALID: 'API Key is Invalid.',
  API_SECRET_REQUIRED: 'API Secret is required.',
  METHOD_REQUIRED: 'Method is required.',
  API_SECRET_INVALID: 'API Secret is Invalid.',
  API_END_POINT_REQUIRED: 'API End Point is required.',
  API_END_POINT_INVALID: 'API End Point is Invalid.',
  PROCESSOR_TYPE_REQUIRED: 'Processor Type is required.',
  PAYMENT_GATE_REQUIRED: 'Payment Gateway Type is required.',
  GATEWAY: 'Gateway Type is required.',
  PORT_NUMBER_REQUIRED: 'Port Number is required.',
  FAX_NUMBER_REQUIRED: 'Fax Number is required.',
  EMAIL_REQUIRED: 'Email is required.',
  API_REQUIRED: 'API is required.',
  CRM_ALREADY: 'CRM is Already Exist!',
  PROCESSOR_ALREADY: 'Processor Type is Already Exist!',
  PAYMENT_ALREADY: 'Payment Gateway Type is Already Exist!',
  DISPUTE_MODE_REQUIRED: 'Submission Mode is required.',
  PORTNUMBER_REQUIRED: 'PortNumber is required.',
  USER_NAME_REQUIRED: 'Username is required.',
  HOST_NAME_REQUIRED: 'Host name is required.',
  OAUTH_URL_REQUIRED: 'OAuth Url is required.',
  PASSWORD_REQUIRED: 'Password is required.',
  FAX_REQUIRED: 'Fax Number is required.',
  REQUEST_API_KEY_REQUIRED: 'Request ApiKey is required.',
  REQUEST_API_SECRET_REQUIRED: 'Request Api secret key is required.',
  REQUEST_API_END_POINT_REQUIRED: 'Request Api end point is required.',
  OAUTH_URL: 'OAuth Url is required.',
  OAUTH_USERNAME: 'User Name is required.',
  OAUTH_PASSWORD: 'OAuth Password is required.'
}

export const USER_MANAGEMENT_ERROR = {
  USER_ROLE_REQUIRED: 'User Role is required.',
  USER_TYPE_REQUIRED: 'User Type is required.',
  PERMISSIONS_REQUIRED: 'Please select the permissions.',
  USER_FNAME: 'First Name is required.',
  USER_LNAME: 'Last Name is required.',
  USER_EMAIL: 'Email is required.',
  USER_PHONE: 'Phone Number is required.',
  USER_PASSWORD: 'Password is required.',
}

export const KYC_FORM = {
  ACTIVE_STEP: 'ACTIVE_STEP',
  CONTACT_DETAILS: 'CONTACT_DETAILS',
  BUSINESS: 'BUSINESS',
  BUSINESS_DETAILS: 'BUSINESS_DETAILS',
  BANK_ACCOUNT: 'BANK_ACCOUNT',
  OTP: 'OTP_DATA',
  ORGANIZATIONS: 'ORGANIZATIONS',
  SING_WIBMA: 'SING_WIBMA'
}

export const CRM_FORM = {
  COMPANY_DETAILS: 'COMPANY_DETAILS',
  MERCHANT_DETAILS: 'MERCHANT_DETAILS',
  USER_DETAILS: 'USER_DETAILS',
  CRM_DETAILS: 'CRM_DETAILS',
  CRM_DETAILS_TABLE: 'CRM_DETAILS_TABLE',
  PROCESSOR_DETAILS: 'PROCESSOR_DETAILS',
  PROCESSOR_DETAILS_TABLE: 'PROCESSOR_DETAILS_TABLE',
  GATEWAY_DETAILS: 'GATEWAY_DETAILS',
  GATEWAY_DETAILS_TABLE: 'GATEWAY_DETAILS_TABLE',
  PACKAGE_DETAILS: 'PACKAGE_DETAILS',
  ACTIVE_STEP: 'ACTIVE_STEP',
  EDIT_CRM: 'EDIT_CRM',
  EDIT_CRM_TABLE: 'EDIT_CRM_TABLE',
  EDIT_PROCESSOR: 'EDIT_PROCESSOR',
  EDIT_PROCESSOR_TABLE: 'EDIT_PROCESSOR_TABLE',
  EDIT_GATEWAY: 'EDIT_GATEWAY',
  EDIT_GATEWAY_TABLE: 'EDIT_GATEWAY_TABLE'
}

export const SAVE_CURRENT = {
  CLIENT_ID: 'SAVE_CURRENT_CLIENT_ID',
  MID_ID: 'SAVE_CURRENT_MID_ID',
  ADD_MID_ID: 'SAVE_CURRENT_ADD_MID_ID',
  PARTNER_ID: 'SAVE_CURRENT_PARTNER_ID'
}

export const SET_STORAGE = {
  PARTNER_DETAILS: 'PARTNER_DETAILS',
  PARTNER_USER_DETAILS: 'PARTNER_USER_DETAILS',
  USER_MENU_DETAILS: 'USER_MENU_DETAILS',
  IS_FIRST_LOGIN: 'IS_FIRST_LOGIN',
  LAST_LOCATION: 'LAST_LOCATION'
}

export const SET_FILTER = {
  CLIENT_ID_FILTER: 'CLIENT_ID_FILTER'
}

export const ADMINVALIDATE = [
  "Analyst",
  "Admin"
]

export const GET_CLIENT_FILTER = {
  GET_CLIENT_ID: 'GET_CLIENT_ID'
}

export const SITE_CONFIG_ERROR = {
  PASS_MAX_REQUIRED: 'Password Max Character is required.',
  PASS_MIN_REQUIRED: 'Password Min Character is required.',
  PASS_LEAST_REQUIRED: 'Password Least Digits is required.',
  PASS_REPEATED_REQUIRED: 'Previous Password Repeated Times is required.',
  PASS_EXPIRED_REQUIRED: 'Password Expired Days is required.',
  PASS_MAX_FAIL_REQUIRED: 'Password Maximum Failure Attempts is required.',
  LOGIN_DURA_REQUIRED: 'Login Duration After Failure Attempts is required.',
  SESSION_TIME: 'Session Timeout is required.'
}

export const BACKEND_DASHBOARD_ERROR = {
  BACKEND_DELAY_REQUIRED: 'Delay Time is required.',
  NEXT_EXECUTION_DATE_REQUIRED: 'Next Execution Date is required.',
  LAST_EXECUTION_DATE_REQUIRED: 'Last Execution Date is required.',
  LEGAL_NAME_REQUIRED: 'Legal Name is required.',
}

export const KYC_COMMENT_ERROR = {
  KYC_COMMENT_REQUIRED: 'Comment is required.',
  KYC_STATUS: 'Status is required.'
}

export const SLICE_CHARACTERS = {
  UP_TO_70: 70,
  UP_TO_35: 35,
  UP_TO_50: 50,
  UP_TO_40: 40,
  UP_TO_25: 25,
  UP_TO_15: 15
}

export const FILE_FORMAT_TYPE_DOCUMENT = [
  'text/csv',
  'application/pdf',
  'application/msword',
  'application/vnd.ms-excel',
  'text/plain',
  'image/gif',
  'image/vnd.adobe.photoshop'
]

export const FILE_FORMAT_CB_EVIDENCE = [
  'text/csv',
  'application/vnd.ms-excel'
]

export const FILE_FORMAT_IMAGES = [
  'image/jpeg',
  'image/png',
  'image/jpg',
  'image/svg+xml',
  'image/JPEG',
  'image/PNG',
  'image/JPG',
]

export const FILE_FORMAT_MID = [
  'image/jpeg',
  'image/png',
  'image/jpg',
  'image/svg+xml',
  'image/JPEG',
  'image/PNG',
  'image/JPG',
  'application/pdf',
]

export const iconOptions = [
  { value: "/media/icons/duotune/technology/teh002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/technology/teh002.svg')} alt='Technology' />, },
  { value: "/media/icons/duotune/communication/com005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com005.svg')} alt='Communication' />, },
  { value: "/media/icons/duotune/communication/com013.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com013.svg')} alt='Communication' />, },
  { value: "/media/icons/duotune/communication/com010.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com010.svg')} alt='Communication' />, },
  { value: "/media/icons/duotune/communication/com004.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com004.svg')} alt='Communication' />, },
  { value: "/media/icons/duotune/communication/com006.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com006.svg')} alt='Communication' />, },
  { value: "/media/icons/duotune/communication/com002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com002.svg')} alt='Communication' />, },
  { value: "/media/icons/duotune/general/gen008.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen008.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen001.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen001.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen028.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen028.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen007.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen007.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen032.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen032.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen035.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen035.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen049.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen049.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen019.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen019.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen022.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen022.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen014.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen014.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen017.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen017.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen054.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen054.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen011.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen011.svg')} alt='General' />, },
  { value: "/media/icons/duotune/general/gen026.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen026.svg')} alt='General' />, },
  { value: "/media/icons/duotune/social/soc001.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc001.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc003.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc003.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc006.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc006.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc002.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc004.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc004.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc008.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc008.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc005.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc007.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc007.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/social/soc010.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/social/soc010.svg')} alt='Social' />, },
  { value: "/media/icons/duotune/graphs/gra003.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra003.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra006.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra006.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra007.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra007.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra011.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra011.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra009.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra009.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra004.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra004.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra001.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra001.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra010.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra010.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra002.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra005.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra008.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra008.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/graphs/gra012.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/graphs/gra012.svg')} alt='Graphs' />, },
  { value: "/media/icons/duotune/finance/fin006.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin006.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin008.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin008.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin003.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin003.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin002.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin001.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin001.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin004.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin004.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin010.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin010.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/finance/fin009.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/finance/fin009.svg')} alt='Finance' />, },
  { value: "/media/icons/duotune/coding/cod002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod002.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod008.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod008.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod004.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod004.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod001.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod001.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod009.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod009.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod010.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod010.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod007.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod007.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/coding/cod005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod005.svg')} alt='Coding' />, },
  { value: "/media/icons/duotune/art/art002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/art/art002.svg')} alt='Art' />, },
  { value: "/media/icons/duotune/maps/map010.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/maps/map010.svg')} alt='Map' />, },
  { value: "/media/icons/duotune/abstract/abs048.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/abstract/abs048.svg')} alt='abstract' />, },
  { value: "/media/icons/duotune/general/gen008.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen008.svg')} alt='general' />, },
  { value: "/media/icons/duotune/communication/com006.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com006.svg')} alt='communication' />, },
  { value: "/media/icons/duotune/general/gen015.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen015.svg')} alt='general' />, },
  { value: "/media/icons/duotune/abstract/abs021.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/abstract/abs021.svg')} alt='abstract' />, },
  { value: "/media/icons/duotune/abstract/abs022.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/abstract/abs022.svg')} alt='abstract' />, },
  { value: "/media/icons/duotune/abstract/abs023.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/abstract/abs023.svg')} alt='abstract' />, },
  { value: "/media/icons/duotune/general/gen005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen005.svg')} alt='general' />, },
  { value: "/media/icons/duotune/communication/com003.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com003.svg')} alt='communication' />, },
  { value: "/media/icons/duotune/general/gen035.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen035.svg')} alt='general' />, },
  { value: "/media/icons/duotune/general/gen027.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen027.svg')} alt='general' />, },
  { value: "/media/icons/duotune/general/gen009.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen009.svg')} alt='general' />, },
  { value: "/media/icons/duotune/general/gen039.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen039.svg')} alt='general' />, },
  { value: "/media/icons/duotune/arrows/arr086.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/arrows/arr086.svg')} alt='arrows' />, },
  { value: "/media/icons/duotune/abstract/abs029.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/abstract/abs029.svg')} alt='abstract' />, },
  { value: "/media/icons/duotune/general/gen030.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen030.svg')} alt='general' />, },
  { value: "/media/icons/duotune/general/gen011.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen011.svg')} alt='general' />, },
  { value: "/media/icons/duotune/general/gen031.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen031.svg')} alt='general' />, },
  { value: "/media/icons/duotune/general/gen012.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen012.svg')} alt='general' />, },
  { value: "/media/icons/duotune/communication/com005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com005.svg')} alt='communication' />, },
  { value: "/media/icons/duotune/communication/com013.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com013.svg')} alt='communication' />, },
  { value: "/media/icons/duotune/general/gen019.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen019.svg')} alt='general' />, },
  { value: "/media/icons/duotune/coding/cod001.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/coding/cod001.svg')} alt='coding' />, },
  { value: "/media/icons/duotune/general/gen049.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen049.svg')} alt='general' />, },
  { value: "/media/icons/duotune/communication/com005.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/communication/com005.svg')} alt='communication' />, },
  { value: "/media/icons/duotune/general/gen022.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/general/gen022.svg')} alt='general' />, },
  { value: "/media/icons/duotune/technology/teh002.svg", label: <img src={toAbsoluteUrl('/media/icons/duotune/technology/teh002.svg')} alt='technology' />, },
]