export const FeedBackActionsTypes = {
    GET_FEEDBACK_LIST: 'GET_FEEDBACK_LIST',
    SAVE_FEEDBACK_LIST_RESPONSE: 'SAVE_FEEDBACK_LIST_RESPONSE',
    CLEAR_FEEDBACK_LIST: 'CLEAR_FEEDBACK_LIST'
}

export const FeedBackActions = {
    getFeedBackList: (params) => ({
        type: FeedBackActionsTypes.GET_FEEDBACK_LIST,
        params
    }),
    saveFeedBackListResponse: (data) => ({
        type: FeedBackActionsTypes.SAVE_FEEDBACK_LIST_RESPONSE,
        data
    }),
    clearFeedBackList: () => ({
        type: FeedBackActionsTypes.CLEAR_FEEDBACK_LIST
    })
}

export const SendFeedBackactionsTypes = {
    SEND_FEEDBACK_POST: 'SEND_FEEDBACK_POST',
    SEND_FEEDBACK_RESPONSE: 'SEND_FEEDBACK_RESPONSE',
    CLEAR_SEND_FEEDBACK: 'CLEAR_SEND_FEEDBACK'
}

export const SendFeedBackactions = {
    sendFeedBackPost: (params) => ({
        type: SendFeedBackactionsTypes.SEND_FEEDBACK_POST,
        params
    }),
    saveSendFeedBackPostResponse: (data) => ({
        type: SendFeedBackactionsTypes.SEND_FEEDBACK_RESPONSE,
        data
    }),
    clearSendFeedBackPost: () => ({
        type: SendFeedBackactionsTypes.CLEAR_SEND_FEEDBACK
    })
}

export const NotificationFeedBackActionsTypes = {
    GET_NOTIFICATION_FEEDBACK_LIST: 'GET_NOTIFICATION_FEEDBACK_LIST',
    SAVE_NOTIFICATION_FEEDBACK_LIST_RESPONSE: 'SAVE_NOTIFICATION_FEEDBACK_LIST_RESPONSE',
    CLEAR_NOTIFICATION_FEEDBACK_LIST: 'CLEAR_NOTIFICATION_FEEDBACK_LIST'
}

export const NotificationFeedBackActions = {
    getNotificationFeedBackList: (params) => ({
        type: NotificationFeedBackActionsTypes.GET_NOTIFICATION_FEEDBACK_LIST,
        params
    }),
    saveNotificationFeedBackListResponse: (data) => ({
        type: NotificationFeedBackActionsTypes.SAVE_NOTIFICATION_FEEDBACK_LIST_RESPONSE,
        data
    }),
    clearNotificationFeedBackList: () => ({
        type: NotificationFeedBackActionsTypes.CLEAR_NOTIFICATION_FEEDBACK_LIST
    })
}

export const FeedBackViewActionsTypes = {
    GET_FEEDBACK_VIEW_LIST: 'GET_FEEDBACK_VIEW_LIST',
    SAVE_FEEDBACK_VIEW_LIST_RESPONSE: 'SAVE_FEEDBACK_VIEW_LIST_RESPONSE',
    CLEAR_FEEDBACK_VIEW_LIST: 'CLEAR_FEEDBACK_VIEW_LIST'
}

export const FeedBackViewActions = {
    getFeedBackViewList: (params) => ({
        type: FeedBackViewActionsTypes.GET_FEEDBACK_VIEW_LIST,
        params
    }),
    saveFeedBackViewListResponse: (data) => ({
        type: FeedBackViewActionsTypes.SAVE_FEEDBACK_LIST_VIEW_RESPONSE,
        data
    }),
    clearFeedBackViewList: () => ({
        type: FeedBackViewActionsTypes.CLEAR_FEEDBACK_VIEW_LIST
    })
}

export const FeedBackClosectionsTypes = {
    GET_CLOSE_FEEDBACK_VIEW_LIST: 'GET_CLOSE_FEEDBACK_VIEW_LIST',
    SAVE_CLOSE_FEEDBACK_VIEW_LIST_RESPONSE: 'SAVE_CLOSE_FEEDBACK_VIEW_LIST_RESPONSE',
    CLEAR_CLOSE_FEEDBACK_VIEW_LIST: 'CLEAR_CLOSE_FEEDBACK_VIEW_LIST'
}

export const FeedBackCloseActions = {
    getCloseFeedBackViewList: (params) => ({
        type: FeedBackClosectionsTypes.GET_CLOSE_FEEDBACK_VIEW_LIST,
        params
    }),
    saveCloseFeedBackViewListResponse: (data) => ({
        type: FeedBackClosectionsTypes.SAVE_CLOSE_FEEDBACK_VIEW_LIST_RESPONSE,
        data
    }),
    clearCloseFeedBackViewList: () => ({
        type: FeedBackClosectionsTypes.CLEAR_CLOSE_FEEDBACK_VIEW_LIST
    })
}

export const FeedBackCountActionsTypes = {
    GET_FEEDBACK_COUNT: 'GET_FEEDBACK_COUNT',
    SAVE_FEEDBACK_COUNT_RESPONSE: 'SAVE_FEEDBACK_COUNT_RESPONSE',
    CLEAR_FEEDBACK_COUNT: 'CLEAR_FEEDBACK_COUNT'
}

export const FeedBackCountActions = {
    getFeedBackCount: (params) => ({
        type: FeedBackCountActionsTypes.GET_FEEDBACK_COUNT,
        params
    }),
    saveFeedBackCountResponse: (data) => ({
        type: FeedBackCountActionsTypes.SAVE_FEEDBACK_COUNT_RESPONSE,
        data
    }),
    clearFeedBackCount: () => ({
        type: FeedBackCountActionsTypes.CLEAR_FEEDBACK_COUNT
    })
}