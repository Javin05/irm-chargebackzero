export const ClientWebActionsTypes = {
  GET_CLIENTWEB_LIST: 'GET_CLIENTWEB_LIST',
  SAVE_CLIENTWEB_LIST_RESPONSE: 'SAVE_CLIENTWEB_LIST_RESPONSE',
  CLEAR_CLIENTWEB_LIST: 'CLEAR_CLIENTWEB_LIST'
}

export const ClientWebActions = {
  getClientWeblist: (params) => ({
    type: ClientWebActionsTypes.GET_CLIENTWEB_LIST,
    params
  }),
  saveClientWeblistResponse: (data) => ({
    type: ClientWebActionsTypes.SAVE_CLIENTWEB_LIST_RESPONSE,
    data
  }),
  clearClientWeblist: () => ({
    type: ClientWebActionsTypes.CLEAR_CLIENTWEB_LIST
  })
}

export const PostClientWebActionsTypes = {
  POST_CLIENTWEB_LIST: 'POST_CLIENTWEB_LIST',
  POST_SAVE_CLIENTWEB_LIST_RESPONSE: 'POST_SAVE_CLIENTWEB_LIST_RESPONSE',
  POST_CLEAR_CLIENTWEB_LIST: 'POST_CLEAR_CLIENTWEB_LIST'
}

export const PostClientWebActions = {
  PostClientWeb: (id, params) => ({
    type: PostClientWebActionsTypes.POST_CLIENTWEB_LIST,
    payload: { id, params }
  }),
  postsaveClientWeblistResponse: (data) => ({
    type: PostClientWebActionsTypes.POST_SAVE_CLIENTWEB_LIST_RESPONSE,
    data
  }),
  postclearClientWeblist: () => ({
    type: PostClientWebActionsTypes.POST_CLEAR_CLIENTWEB_LIST
  })
}

export const PostClientPlaStoreActionsTypes = {
  POST_CLIENT_PLAYSTORE_LIST: 'POST_CLIENT_PLAYSTORE_LIST',
  POST_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE: 'POST_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE',
  POST_CLEAR_CLIENT_PLAYSTORE_LIST: 'POST_CLEAR_CLIENT_PLAYSTORE_LIST'
}

export const PostClientPlayStoreActions = {
  PostClientPlayStore: (id, params) => ({
    type: PostClientPlaStoreActionsTypes.POST_CLIENT_PLAYSTORE_LIST,
    payload: { id, params }
  }),
  postsaveClientPlayStorlistResponse: (data) => ({
    type: PostClientPlaStoreActionsTypes.POST_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE,
    data
  }),
  postclearClientPlayStorlist: () => ({
    type: PostClientPlaStoreActionsTypes.POST_CLEAR_CLIENT_PLAYSTORE_LIST
  })
}

export const PostClientAppStoreActionsTypes = {
  POST_CLIENT_APPSTORE_LIST: 'POST_CLIENT_APPSTORE_LIST',
  POST_SAVE_CLIENT_APPSTORE_LIST_RESPONSE: 'POST_SAVE_CLIENT_APPSTORE_LIST_RESPONSE',
  POST_CLEAR_CLIENT_APPSTORE_LIST: 'POST_CLEAR_CLIENT_APPSTORE_LIST'
}

export const PostClientAppStoreActions = {
  PostClientAppStore: (id, params) => ({
    type: PostClientAppStoreActionsTypes.POST_CLIENT_APPSTORE_LIST,
    payload: { id, params }
  }),
  postsaveClientAppStorlistResponse: (data) => ({
    type: PostClientAppStoreActionsTypes.POST_SAVE_CLIENT_APPSTORE_LIST_RESPONSE,
    data
  }),
  postclearClientAppStorlist: () => ({
    type: PostClientAppStoreActionsTypes.POST_CLEAR_CLIENT_APPSTORE_LIST
  })
}

export const ClientPlayStoreActionsTypes = {
  GET_CLIENTPLAYSTORE_LIST: 'GET_CLIENTPLAYSTORE_LIST',
  SAVE_CLIENTPLAYSTORE_LIST_RESPONSE: 'SAVE_CLIENTPLAYSTORE_LIST_RESPONSE',
  CLEAR_CLIENTPLAYSTORE_LIST: 'CLEAR_CLIENTPLAYSTORE_LIST'
}

export const ClientPlayStoreActions = {
  getClientPlayStorelist: (params) => ({
    type: ClientPlayStoreActionsTypes.GET_CLIENTPLAYSTORE_LIST,
    params
  }),
  saveClientPlayStorelistResponse: (data) => ({
    type: ClientPlayStoreActionsTypes.SAVE_CLIENTPLAYSTORE_LIST_RESPONSE,
    data
  }),
  clearClientPlayStorelist: () => ({
    type: ClientPlayStoreActionsTypes.CLEAR_CLIENTPLAYSTORE_LIST
  })
}

export const ClientAppStoreActionsTypes = {
  GET_CLIENTAPPSTORE_LIST: 'GET_CLIENTAPPSTORE_LIST',
  SAVE_CLIENTAPPSTORE_LIST_RESPONSE: 'SAVE_CLIENTAPPSTORE_LIST_RESPONSE',
  CLEAR_CLIENTAPPSTORE_LIST: 'CLEAR_CLIENTAPPSTORE_LIST'
}

export const ClientAppStoreActions = {
  getClientAppStorelist: (params) => ({
    type: ClientAppStoreActionsTypes.GET_CLIENTAPPSTORE_LIST,
    params
  }),
  saveClientAppStorelistResponse: (data) => ({
    type: ClientAppStoreActionsTypes.SAVE_CLIENTAPPSTORE_LIST_RESPONSE,
    data
  }),
  clearClientAppStorelist: () => ({
    type: ClientAppStoreActionsTypes.CLEAR_CLIENTAPPSTORE_LIST
  })
}

export const exportClientWebActionsTypes = {
  EXPORT_CLIENTWEB_LIST: 'EXPORT_CLIENTWEB_LIST',
  EXPORT_SAVE_CLIENTWEB_LIST_RESPONSE: 'EXPORT_SAVE_CLIENTWEB_LIST_RESPONSE',
  EXPORT_CLEAR_CLIENTWEB_LIST: 'EXPORT_CLEAR_CLIENTWEB_LIST'
}

export const exportClientWebActions = {
  exportClientWeb: (id, params) => ({
    type: exportClientWebActionsTypes.EXPORT_CLIENTWEB_LIST,
   payload:{id, params} 
    }),
  exportsaveClientWeblistResponse: (data) => ({
    type: exportClientWebActionsTypes.EXPORT_SAVE_CLIENTWEB_LIST_RESPONSE,
    data
  }),
  exportclearClientWeblist: () => ({
    type: exportClientWebActionsTypes.EXPORT_CLEAR_CLIENTWEB_LIST
  })
}

export const exportClientPlaStoreActionsTypes = {
  EXPORT_CLIENT_PLAYSTORE_LIST: 'EXPORT_CLIENT_PLAYSTORE_LIST',
  EXPORT_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE: 'EXPORT_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE',
  EXPORT_CLEAR_CLIENT_PLAYSTORE_LIST: 'EXPORT_CLEAR_CLIENT_PLAYSTORE_LIST'
}

export const exportClientPlayStoreActions = {
  exportClientPlayStore: (id, params) => ({
    type: exportClientPlaStoreActionsTypes.EXPORT_CLIENT_PLAYSTORE_LIST,
    payload:{id, params} 
  }),
  exportsaveClientPlayStorlistResponse: (data) => ({
    type: exportClientPlaStoreActionsTypes.EXPORT_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE,
    data
  }),
  exportclearClientPlayStorlist: () => ({
    type: exportClientPlaStoreActionsTypes.EXPORT_CLEAR_CLIENT_PLAYSTORE_LIST
  })
}

export const exportClientAppStoreActionsTypes = {
  EXPORT_CLIENT_APPSTORE_LIST: 'EXPORT_CLIENT_APPSTORE_LIST',
  EXPORT_SAVE_CLIENT_APPSTORE_LIST_RESPONSE: 'EXPORT_SAVE_CLIENT_APPSTORE_LIST_RESPONSE',
  EXPORT_CLEAR_CLIENT_APPSTORE_LIST: 'EXPORT_CLEAR_CLIENT_APPSTORE_LIST'
}

export const exportClientAppStoreActions = {
  exportClientAppStore: (id, params) => ({
    type: exportClientAppStoreActionsTypes.EXPORT_CLIENT_APPSTORE_LIST,
    payload:{id, params} 
  }),
  exportsaveClientAppStorlistResponse: (data) => ({
    type: exportClientAppStoreActionsTypes.EXPORT_SAVE_CLIENT_APPSTORE_LIST_RESPONSE,
    data
  }),
  exportclearClientAppStorlist: () => ({
    type: exportClientAppStoreActionsTypes.EXPORT_CLEAR_CLIENT_APPSTORE_LIST
  })
}