export const ClientManagementActionsTypes = {
    GET_CLIENTMANAGEMENT_LIST: 'GET_CLIENTMANAGEMENT_LIST',
    SAVE_CLIENTMANAGEMENT_LIST_RESPONSE: 'SAVE_CLIENTMANAGEMENT_LIST_RESPONSE',
    CLEAR_CLIENTMANAGEMENT_LIST: 'CLEAR_CLIENTMANAGEMENT_LIST'
  }
  
  export const ClientManagementActions = {
    getClientManagementlist: (params) => ({
      type: ClientManagementActionsTypes.GET_CLIENTMANAGEMENT_LIST,
      params
    }),
    saveClientManagementlistResponse: (data) => ({
      type: ClientManagementActionsTypes.SAVE_CLIENTMANAGEMENT_LIST_RESPONSE,
      data
    }),
    clearClientManagementlist: () => ({
      type: ClientManagementActionsTypes.CLEAR_CLIENTMANAGEMENT_LIST
    })
  }