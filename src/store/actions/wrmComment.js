export const WRMCommentTypes = {
    GET_WRM_COMMENT_LIST: 'GET_WRM_COMMENT_LIST',
    SAVE_WRM_COMMENT_LIST_RESPONSE: 'SAVE_WRM_COMMENT_LIST_RESPONSE',
    CLEAR_WRM_COMMENT_LIST: 'CLEAR_WRM_COMMENT_LIST'
  }
  
  export const WRMCommentActions = {
    getWRMCommentlist: (params) => ({
      type: WRMCommentTypes.GET_WRM_COMMENT_LIST,
      params
    }),
    saveWRMlistResponse: (data) => ({
      type: WRMCommentTypes.SAVE_WRM_COMMENT_LIST_RESPONSE,
      data
    }),
    clearWRMCommentlist: () => ({
      type: WRMCommentTypes.CLEAR_WRM_COMMENT_LIST
    })
  }

  export const updateWRMCommentActionsTypes = {
    UPDATE_WRM_COMMENT: 'UPDATE_WRM_COMMENT',
    SAVE_UPDATE_WRM_COMMENT_RESPONSE: 'SAVE_UPDATE_WRM_COMMENT_RESPONSE',
    CLEAR_UPDATE_WRM_COMMENT: 'CLEAR_UPDATE_WRM_COMMENT'
  }
  
  export const updateWRMCommentActions = {
    updateWRMComment: (id, params) => {
      return {
        type: updateWRMCommentActionsTypes.UPDATE_WRM_COMMENT,
        payload: { id, params }
      }
    },
    saveupdateWRMCommentResponse: data => ({
      type: updateWRMCommentActionsTypes.SAVE_UPDATE_WRM_COMMENT_RESPONSE,
      data
    }),
    clearupdateWRMComment: () => ({
      type: updateWRMCommentActionsTypes.CLEAR_UPDATE_WRM_COMMENT
    })
  }

  export const WRMCommentdeleteActionsTypes = {
    REQUEST: 'WRMCOMMENT_DELETE_REQUEST',
    RESPONSE: 'WRMCOMMENT_DELETE_RESPONSE',
    ERROR: 'WRMCOMMENT_DELETE_ERROR',
    CLEAR: 'WRMCOMMENT_DELETE_CLEAR'
  }
  
  export const WRMCommentdeleteActions = {
    delete: (params) => ({
      type: WRMCommentdeleteActionsTypes.REQUEST,
      params
    }),
    saveResponse: (data) => ({
      type: WRMCommentdeleteActionsTypes.RESPONSE,
      data
    }),
    clear: () => ({
      type: WRMCommentdeleteActionsTypes.CLEAR
    })
  }

  export const WrmStatusType = {
    WRM_STATUS: "WRM_STATUS",
    WRM_STATUS_RESPONSE: "WRM_STATUS_RESPONSE",
    WRM_STATUS_CLEAR: "WRM_STATUS_CLEAR"
  };
  
  export const WrmStatusAction = {
    WrmStatus: (params) => {
      return {
        type: WrmStatusType.WRM_STATUS,
        payload:{params}
      }
    },
    WrmStatusResponse: (data) => ({
      type: WrmStatusType.WRM_STATUS_RESPONSE,
      data,
    }),
    clearWrmStatus: () => ({
      type: WrmStatusType.WRM_STATUS_CLEAR,
    })
  }