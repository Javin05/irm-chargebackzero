

export const FraudAnalysisActionType = {
    REQUEST: 'REQUEST_FRAUD_ANALYSYS',
    RESPONSE: 'RESPONSE_FRAUD_ANALYSIS',
    CLEAR: 'CLEAR_FRAUD_ANALSYSIS'
  }
  
  export const FraudAnalysisActions = {
    request: (data) => ({
      type: FraudAnalysisActionType.REQUEST,
      payload: data
    }),
    response: (data) => ({
      type: FraudAnalysisActionType.RESPONSE,
      data
    }),
    clear: () => ({
      type: FraudAnalysisActionType.CLEAR
    })
  }
export const FraudToolFileUploadActionType  ={
  REQUEST:"REQUEST_FILE_UPLOAD",
  RESPONSE:"RESPNOSE_FILE_UPLOAD",
  CLEAR:"CLEAR_FILE_UPLOAD"
}
export const FraudToolfileUploadActions = {
  request: (params) => ({
    type: FraudToolFileUploadActionType.REQUEST,
    payload: params
  }),
  response: (data) => ({
    type: FraudToolFileUploadActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudToolFileUploadActionType.CLEAR
  })
}

export const FraudToolListActionType = {
  REQUEST:'REQUEST_FRAUD_TOOL_LIST',
  RESPONSE:'RESPONSE_FRAUD_TOOL_LIST',
  CLEAR: 'CLEAR_FRAUD_TOOL_LIST'
}

export const FraudToolListActions = {
  request: (params) => ({
    type: FraudToolListActionType.REQUEST,
    params
  }),
  response: (data) => ({
    type: FraudToolListActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudToolListActionType.CLEAR
  })
}
export const FraudToolDetailsActionType = {
  REQUEST:'REQUEST_FRAUD_TOOL_DETAILS',
  RESPONSE:'RESPONSE_FRAUD_TOOL_DETAILS',
  CLEAR: 'CLEAR_FRAUD_TOOL_DETAILS'
}

export const FraudToolDetailActions = {
  request: (params) => ({
    type: FraudToolDetailsActionType.REQUEST,
    id  : params.id
  }),
  response: (data) => ({
    type: FraudToolDetailsActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudToolDetailsActionType.CLEAR
  })
}

export const FraudAnalysisBulkUploadActionType = {
  REQUEST: 'REQUEST_FRAUD_ANALYSYS_BULK_UPLOAD',
  RESPONSE: 'RESPONSE_FRAUD_ANALYSIS_BULK_UPLOAD',
  CLEAR: 'CLEAR_FRAUD_ANALSYSIS_BULK_UPLOAD'
}

export const FraudAnalysisBulkUploadActions = {
  request: (data) => ({
    type: FraudAnalysisBulkUploadActionType.REQUEST,
    payload: data
  }),
  response: (data) => ({
    type: FraudAnalysisBulkUploadActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudAnalysisBulkUploadActionType.CLEAR
  })
}

export const FraudAnalysisSummaryListActionType = {
  REQUEST:'REQUEST_FRAUD_ANALYSIS_SUMMARY_LIST',
  RESPONSE:'RESPONSE_FRAUD_ANALYSIS_SUMMARY_LIST',
  CLEAR: 'CLEAR_FRAUD_ANALYSIS_SUMMARY_LIST'
}

export const FraudAnalysisSummaryListActions = {
  request: (params) => ({
    type: FraudAnalysisSummaryListActionType.REQUEST,
    params
  }),
  response: (data) => ({
    type: FraudAnalysisSummaryListActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudAnalysisSummaryListActionType.CLEAR
  })
}

export const FraudAnalysisExportActionType = {
  REQUEST:'REQUEST_FRAUD_ANALYSIS_EXPORT_DETAILS',
  RESPONSE:'RESPONSE_FRAUD_ANALYSIS_EXPORT_DETAILS',
  CLEAR: 'CLEAR_FRAUD_ANALYSIS_EXPORT_DETAILS'
}

export const FraudAnalysisExportActions = {
  request: (params) => ({
    type: FraudAnalysisExportActionType.REQUEST,
    id  : params
  }),
  response: (data) => ({
    type: FraudAnalysisExportActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudAnalysisExportActionType.CLEAR
  })
}

export const FraudAnalysisTagListActionType = {
  REQUEST:'REQUEST_FRAUD_ANALYSIS_TAG_LIST',
  RESPONSE:'RESPONSE_FRAUD_ANALYSIS_TAG_LIST',
  CLEAR: 'CLEAR_FRAUD_ANALYSIS_TAG_LIST'
}

export const FraudAnalysisTagListActions = {
  request: (params) => ({
    type: FraudAnalysisTagListActionType.REQUEST,
    params
  }),
  response: (data) => ({
    type: FraudAnalysisTagListActionType.RESPONSE,
    data
  }),
  clear: () => ({
    type: FraudAnalysisTagListActionType.CLEAR
  })
}