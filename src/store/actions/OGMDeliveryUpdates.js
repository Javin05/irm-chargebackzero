export const getOGMDeliveryUpdateTypes = {
    GET_OGM_DELIVERY_UPDATES_LIST: 'GET_OGM_DELIVERY_UPDATES_LIST',
    SAVE_OGM_DELIVERY_UPDATES_LIST_RESPONSE: 'SAVE_OGM_DELIVERY_UPDATES_LIST_RESPONSE',
    CLEAR_OGM_DELIVERY_UPDATES_LIST: 'CLEAR_OGM_DELIVERY_UPDATES_LIST'
}
export const OGMDeliveryUpdateActions = {
    getOGMDeliveryUpdatelist: (params) => ({
        type: getOGMDeliveryUpdateTypes.GET_OGM_DELIVERY_UPDATES_LIST,
        params
    }),
    saveOGMDeliveryUpdatelistResponse: (data) => ({
        type: getOGMDeliveryUpdateTypes.SAVE_OGM_DELIVERY_UPDATES_LIST_RESPONSE,
        data
    }),
    clearOGMDeliveryUpdatelist: () => ({
        type: getOGMDeliveryUpdateTypes.CLEAR_OGM_DELIVERY_UPDATES_LIST
    })
}

export const addOGMDeliveryUpdateTypes = {
    REQUEST: 'ADD_OGM_DELIVERY_UPDATES',
    RESPONSE: 'ADD_OGM_DELIVERY_UPDATES_RESPONSE',
    CLEAR: 'ADD_OGM_DELIVERY_UPDATES_CLEAR'
  }
export const addOGMDeliveryUpdateActions = {
    addOGMDeliveryUpdate: (data) => {
      return {
        type: addOGMDeliveryUpdateTypes.REQUEST,
        payload: data
      }
    },
    saveaddOGMDeliveryUpdateResponse: (data) => {
      return {
        type: addOGMDeliveryUpdateTypes.RESPONSE,
        data
      }
    },
    clearaddOGMDeliveryUpdate: () => ({
      type: addOGMDeliveryUpdateTypes.CLEAR
    })
  }

export const OGMDeliveryUpdateIdTypes = {
    OGM_DELIVERY_UPDATES_GET_DETAILS: "OGM_DELIVERY_UPDATES_GET_DETAILS",
    OGM_DELIVERY_UPDATES_GET_DETAILS_RESPONSE: "OGM_DELIVERY_UPDATES_GET_DETAILS_RESPONSE",
    OGM_DELIVERY_UPDATES_GET_DETAILS_CLEAR: "OGM_DELIVERY_UPDATES_GET_DETAILS_CLEAR",
  };
  
export const OGMDeliveryUpdateIdAction = {
    getOGMDeliveryUpdateDetail: (id) => ({
      type: OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS,
      id,
    }),
    saveOGMDeliveryUpdateDetailResponse: (data) => ({
      type: OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS_RESPONSE,
      data,
    }),
    OGMDeliveryUpdateDetailClear: () => ({
      type: OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS_CLEAR,
    })
  }

export const updateOGMDeliveryUpdateActionsTypes = {
    UPDATE_OGM_DELIVERY_UPDATES_SUMMARY: 'UPDATE_OGM_DELIVERY_UPDATES_SUMMARY',
    SAVE_UPDATE_OGM_DELIVERY_UPDATES_RESPONSE: 'SAVE_UPDATE_OGM_DELIVERY_UPDATES_RESPONSE',
    CLEAR_UPDATE_OGM_DELIVERY_UPDATES: 'CLEAR_UPDATE_OGM_DELIVERY_UPDATES'
  }

export const updateOGMDeliveryUpdateActions = {
    updateOGMDeliveryUpdate: (id, params) => {
      return {
        type: updateOGMDeliveryUpdateActionsTypes.UPDATE_OGM_DELIVERY_UPDATES_SUMMARY,
        payload: { id, params }
      }
    },
    saveupdateOGMDeliveryUpdateResponse: data => ({
      type: updateOGMDeliveryUpdateActionsTypes.SAVE_UPDATE_OGM_DELIVERY_UPDATES_RESPONSE,
      data
    }),
    clearupdateOGMDeliveryUpdate: () => ({
      type: updateOGMDeliveryUpdateActionsTypes.CLEAR_UPDATE_OGM_DELIVERY_UPDATES
    })
  }

export const OGMDeliveryUpdatedeleteActionsTypes = {
    REQUEST: 'OGM_DELIVERY_UPDATE_DELETE_REQUEST',
    RESPONSE: 'OGM_DELIVERY_UPDATE_DELETE_RESPONSE',
    CLEAR: 'OGM_DELIVERY_UPDATE_DELETE_CLEAR'
  }
  
export const OGMDeliveryUpdatedeleteActions = {
    delete: (params) => ({
      type: OGMDeliveryUpdatedeleteActionsTypes.REQUEST,
      params
    }),
    saveResponse: (data) => ({
      type: OGMDeliveryUpdatedeleteActionsTypes.RESPONSE,
      data
    }),
    clear: () => ({
      type: OGMDeliveryUpdatedeleteActionsTypes.CLEAR
    })
  }