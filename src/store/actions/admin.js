export const AdminActionsTypes = {
  GET_ADMIN_LIST: 'GET_ADMIN_LIST',
  SAVE_ADMIN_LIST_RESPONSE: 'SAVE_ADMIN_LIST_RESPONSE',
  CLEAR_ADMIN_LIST: 'CLEAR_ADMIN_LIST'
}

export const AdminActions = {
  getAdminlist: (params) => ({
    type: AdminActionsTypes.GET_ADMIN_LIST,
    params
  }),
  saveAdminlistResponse: (data) => ({
    type: AdminActionsTypes.SAVE_ADMIN_LIST_RESPONSE,
    data
  }),
  clearAdminlist: () => ({
    type: AdminActionsTypes.CLEAR_ADMIN_LIST
  })
}

export const AdminAddTypes = {
  ADMIN_POST: 'ADMIN_POST',
  ADMIN_POST_RESPONSE: 'ADMIN_POST_RESPONSE',
  ADMIN_POST_CLEAR: 'ADMIN_POST_CLEAR'
}

export const AdminAddAction = {
  AdminAdd: (data) => {
    return {
      type: AdminAddTypes.ADMIN_POST,
      payload: data
    }
  },
  saveAdminResponse: data => ({
    type: AdminAddTypes.ADMIN_POST_RESPONSE,
    data
  }),
  clearAdmin: () => ({
    type: AdminAddTypes.ADMIN_POST_CLEAR
  })
}

export const AdminGetId = {
  GET_ADMIN_DETAILS: 'GET_ADMIN_DETAILS',
  ADMIN_DETAILS_RESPONSE: 'ADMIN_DETAILS_RESPONSE',
  CLEAR_ADMIN_DETAILS: 'CLEAR_ADMIN_DETAILS'
}

export const AdminGetIdActions = {
  getAdminIdDetails: (id) => ({
    type: AdminGetId.GET_ADMIN_DETAILS,
    id
  }),
  saveAdminIdDetailsResponse: data => ({
    type: AdminGetId.ADMIN_DETAILS_RESPONSE,
    data
  }),
  clearAdminIdDetails: () => ({
    type: AdminGetId.CLEAR_ADMIN_DETAILS
  })
}

export const updateAdminActionsTypes = {
  UPDATE_ADMIN: 'UPDATE_ADMIN',
  SAVE_UPDATE_ADMIN_RESPONSE: 'SAVE_UPDATE_ADMIN_RESPONSE',
  CLEAR_UPDATE_ADMIN: 'CLEAR_UPDATE_ADMIN'
}

export const updateAdminActions = {
  updateAdmin: (id, params) => {
    return {
      type: updateAdminActionsTypes.UPDATE_ADMIN,
      payload:  { id, params }
    }
  },
  saveupdateAdminResponse: data => ({
    type: updateAdminActionsTypes.SAVE_UPDATE_ADMIN_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearupdateAdmin: () => ({
    type: updateAdminActionsTypes.CLEAR_UPDATE_ADMIN
  })
}
