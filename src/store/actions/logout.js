export const LogoutActionsTypes = {
  LOGOUT: 'LOGOUT',
  SAVE_LOGOUT_RESPONSE: 'SAVE_LOGOUT_RESPONSE',
  CLEAR_LOGOUT: 'CLEAR_LOGOUT'
}

export const LogoutActions = {
  logout: () => {
    return {
      type: LogoutActionsTypes.LOGOUT
    }
  },
  saveLogoutResponse: data => ({
    type: LogoutActionsTypes.SAVE_LOGOUT_RESPONSE,
    data
  }),
  clearLogout: () => ({
    type: LogoutActionsTypes.CLEAR_LOGOUT
  })
}