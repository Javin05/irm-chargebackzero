
export const BusinessUploadDocType = {
  BUSINESSUPLOAD_DOC_UPLOAD: "BUSINESSUPLOAD_DOC_UPLOAD",
  BUSINESSUPLOAD_DOC_UPLOAD_RESPONSE: "BUSINESSUPLOAD_DOC_UPLOAD_RESPONSE",
  BUSINESSUPLOAD_DOC_UPLOAD_CLEAR: "BUSINESSUPLOAD_DOC_UPLOAD_CLEAR"
};

export const BusinessUploadDocAction = {
  BusinessUploadDoc: (params) => {
    return {
      type: BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD,
      payload: params
    }
  },
  BusinessUploadDocResponse: (data) => ({
    type: BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearBusinessUploadDoc: () => ({
    type: BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD_CLEAR,
  })
}


export const chequeDetailsDocType = {
  CHEQUE_DOC_UPLOAD: "CHEQUE_DOC_UPLOAD",
  CHEQUE_DOC_UPLOAD_RESPONSE: "CHEQUE_DOC_UPLOAD_RESPONSE",
  CHEQUE_DOC_UPLOAD_CLEAR: "CHEQUE_DOC_UPLOAD_CLEAR"
};

export const chequeDetailsDocAction = {
  chequeDetailsDoc: (id, params) => {
    return {
      type: chequeDetailsDocType.CHEQUE_DOC_UPLOAD,
      payload: { id, params }
    }
  },
  chequeDetailsDocResponse: (data) => ({
    type: chequeDetailsDocType.CHEQUE_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearchequeDetailsDoc: () => ({
    type: chequeDetailsDocType.CHEQUE_DOC_UPLOAD_CLEAR,
  })
}

export const categoryEntityType = {
  CATEGORY_ENTITY: "CATEGORY_ENTITY",
  CATEGORY_ENTITY_RESPONSE: "CATEGORY_ENTITY_RESPONSE",
  CATEGORY_ENTITY_CLEAR: "CATEGORY_ENTITY_CLEAR"
};

export const categoryEntityAction = {
  categoryEntity: (params) => {
    return {
      type: categoryEntityType.CATEGORY_ENTITY,
      params
    }
  },
  categoryEntityResponse: (data) => ({
    type: categoryEntityType.CATEGORY_ENTITY_RESPONSE,
    data,
  }),
  clearcategoryEntity: () => ({
    type: categoryEntityType.CATEGORY_ENTITY_CLEAR,
  })
}

export const BusinessUpload4DocType = {
  BUSINESSUPLOAD4_DOC_UPLOAD: "BUSINESSUPLOAD4_DOC_UPLOAD",
  BUSINESSUPLOAD4_DOC_UPLOAD_RESPONSE: "BUSINESSUPLOAD4_DOC_UPLOAD_RESPONSE",
  BUSINESSUPLOAD4_DOC_UPLOAD_CLEAR: "BUSINESSUPLOAD4_DOC_UPLOAD_CLEAR"
};

export const BusinessUpload4DocAction = {
  BusinessUpload4Doc: (params) => {
    return {
      type: BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD,
      payload: params
    }
  },
  BusinessUpload4DocResponse: (data) => ({
    type: BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearBusinessUpload4Doc: () => ({
    type: BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD_CLEAR,
  })
}

export const AccountUploadDocType = {
  ACCOUNT_DOC_UPLOAD: "ACCOUNT_DOC_UPLOAD_DOC_UPLOAD",
  ACCOUNT_DOC_UPLOAD_RESPONSE: "ACCOUNT_DOC_UPLOAD_DOC_UPLOAD_RESPONSE",
  ACCOUNT_DOC_UPLOAD_CLEAR: "ACCOUNT_DOC_UPLOAD_DOC_UPLOAD_CLEAR"
}

export const AccountUploadAction = {
  AccountUploadDoc: (params) => {
    return {
      type: AccountUploadDocType.ACCOUNT_DOC_UPLOAD,
      payload: params
    }
  },
  AccountUploadDocResponse: (data) => ({
    type: AccountUploadDocType.ACCOUNT_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearAccountUploadDoc: () => ({
    type: AccountUploadDocType.ACCOUNT_DOC_UPLOAD_CLEAR,
  })
}

export const HufDeedUploadDocType = {
  HUFDEED_DOC_UPLOAD: "HUFDEED_DOC_UPLOAD_DOC_UPLOAD",
  HUFDEED_DOC_UPLOAD_RESPONSE: "HUFDEED_DOC_UPLOAD_DOC_UPLOAD_RESPONSE",
  HUFDEED_DOC_UPLOAD_CLEAR: "HUFDEED_DOC_UPLOAD_DOC_UPLOAD_CLEAR"
}

export const HufDeedUploadAction = {
  HufDeedUploadDoc: (params) => {
    return {
      type: HufDeedUploadDocType.HUFDEED_DOC_UPLOAD,
      payload: params
    }
  },
  HufDeedUploadDocResponse: (data) => ({
    type: HufDeedUploadDocType.HUFDEED_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearHufDeedUploadDoc: () => ({
    type: HufDeedUploadDocType.HUFDEED_DOC_UPLOAD_CLEAR,
  })
}

export const SolePropUploadDocType = {
  SOLEPROP_DOC_UPLOAD: "SOLEPROP_DOC_UPLOAD_DOC_UPLOAD",
  SOLEPROP_DOC_UPLOAD_RESPONSE: "SOLEPROP_DOC_UPLOAD_DOC_UPLOAD_RESPONSE",
  SOLEPROP_DOC_UPLOAD_CLEAR: "SOLEPROP_DOC_UPLOAD_DOC_UPLOAD_CLEAR"
}

export const SolePropUploadAction = {
  SolePropUploadDoc: (params) => {
    return {
      type: SolePropUploadDocType.SOLEPROP_DOC_UPLOAD,
      payload: params
    }
  },
  SolePropUploadDocResponse: (data) => ({
    type: SolePropUploadDocType.SOLEPROP_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearSolePropUploadDoc: () => ({
    type: SolePropUploadDocType.SOLEPROP_DOC_UPLOAD_CLEAR,
  })
}

export const DashComUploadDocType = {
  DASHCOMMON_DOC_UPLOAD: "DASHCOMMON_DOC_UPLOAD_DOC_UPLOAD",
  DASHCOMMON_DOC_UPLOAD_RESPONSE: "DASHCOMMON_DOC_UPLOAD_DOC_UPLOAD_RESPONSE",
  DASHCOMMON_DOC_UPLOAD_CLEAR: "DASHCOMMON_DOC_UPLOAD_DOC_UPLOAD_CLEAR"
}

export const DashComUploadAction = {
  DashComUploadDoc: (id, params) => {
    return {
      type: DashComUploadDocType.DASHCOMMON_DOC_UPLOAD,
      payload: { id, params }
    }
  },
  DashComUploadDocResponse: (data) => ({
    type: DashComUploadDocType.DASHCOMMON_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearDashComUploadDoc: () => ({
    type: DashComUploadDocType.DASHCOMMON_DOC_UPLOAD_CLEAR,
  })
}

export const DashReUploadDocType = {
  DASHRE_DOC_UPLOAD: "DASHRE_DOC_UPLOAD_DOC_UPLOAD",
  DASHRE_DOC_UPLOAD_RESPONSE: "DASHRE_DOC_UPLOAD_DOC_UPLOAD_RESPONSE",
  DASHRE_DOC_UPLOAD_CLEAR: "DASHRE_DOC_UPLOAD_DOC_UPLOAD_CLEAR"
}

export const DashReUploadAction = {
  DashReUploadDoc: (id, params) => {
    return {
      type: DashReUploadDocType.DASHRE_DOC_UPLOAD,
      payload: { id, params }
    }
  },
  DashReUploadDocResponse: (data) => ({
    type: DashReUploadDocType.DASHRE_DOC_UPLOAD_RESPONSE,
    data,
  }),
  clearDashReUploadDoc: () => ({
    type: DashReUploadDocType.DASHRE_DOC_UPLOAD_CLEAR,
  })
}

export const DashVideoApproveType = {
  DASHRE_VIDEO_APPROVE: "DASHRE_VIDEO_APPROVE",
  DASHRE_VIDEO_APPROVE_RESPONSE: "DASHRE_VIDEO_APPROVE_RESPONSE",
  DASHRE_VIDEO_APPROVE_CLEAR: "DASHRE_VIDEO_APPROVE_CLEAR"
}

export const DashVideoApproveAction = {
  DashVideoApproveDoc: (id, params) => {
    return {
      type: DashVideoApproveType.DASHRE_VIDEO_APPROVE,
      payload: { id, params }
    }
  },
  DashVideoApproveResponse: (data) => ({
    type: DashVideoApproveType.DASHRE_VIDEO_APPROVE_RESPONSE,
    data,
  }),
  clearVideoApproveDoc: () => ({
    type: DashVideoApproveType.DASHRE_VIDEO_APPROVE_CLEAR,
  })
}

export const uploadViewType = {
  VIEW_UPLOAD: "VIEW_UPLOAD_DOC_UPLOAD",
  VIEW_UPLOAD_RESPONSE: "VIEW_UPLOAD_DOC_UPLOAD_RESPONSE",
  VIEW_UPLOAD_CLEAR: "VIEW_UPLOAD_DOC_UPLOAD_CLEAR"
}

export const uploadViewAction = {
  viewUploadDoc: (params) => {
    return {
      type: uploadViewType.VIEW_UPLOAD,
      params
    }
  },
  viewUploadDocResponse: (data) => ({
    type: uploadViewType.VIEW_UPLOAD_RESPONSE,
    data,
  }),
  clearviewUploadDoc: () => ({
    type: uploadViewType.VIEW_UPLOAD_CLEAR,
  })
}

export const addKycFvType = {
  ADD_KYC_FV: "ADD_KYC_FV_DOC_UPLOAD",
  ADD_KYC_FV_RESPONSE: "ADD_KYC_FV_DOC_UPLOAD_RESPONSE",
  ADD_KYC_FV_CLEAR: "ADD_KYC_FV_DOC_UPLOAD_CLEAR"
}

export const addKycFvAction = {
  addKycFv: (params) => {
    return {
      type: addKycFvType.ADD_KYC_FV,
      params
    }
  },
  addKycFvResponse: (data) => ({
    type: addKycFvType.ADD_KYC_FV_RESPONSE,
    data,
  }),
  clearaddKycFv: () => ({
    type: addKycFvType.ADD_KYC_FV_CLEAR,
  })
}

export const UpdateKycFvType = {
  UPDATE_KYC_FV: "UPDATE_KYC_FV_DOC_UPLOAD",
  UPDATE_KYC_FV_RESPONSE: "UPDATE_KYC_FV_DOC_UPLOAD_RESPONSE",
  UPDATE_KYC_FV_CLEAR: "UPDATE_KYC_FV_DOC_UPLOAD_CLEAR"
}

export const UpdateKycFvAction = {
  UpdateKycFv: (id, params) => {
    return {
      type: UpdateKycFvType.UPDATE_KYC_FV,
      payload: { id, params }
    }
  },
  UpdateKycFvResponse: (data) => ({
    type: UpdateKycFvType.UPDATE_KYC_FV_RESPONSE,
    data,
  }),
  clearUpdateKycFv: () => ({
    type: UpdateKycFvType.UPDATE_KYC_FV_CLEAR,
  })
}


export const kycStatusType = {
  KYC_STATUS_GET: "KYC_STATUS_GET",
  KYC_STATUS_GET_RESPONSE: "KYC_STATUS_GET_RESPONSE",
  KYC_STATUS_GET_CLEAR: "KYC_STATUS_GET_CLEAR"
}

export const kycStatusAction = {
  kycStatus: (params) => {
    return {
      type: kycStatusType.KYC_STATUS_GET,
      params
    }
  },
  kycStatusResponse: (data) => ({
    type: kycStatusType.KYC_STATUS_GET_RESPONSE,
    data,
  }),
  clearkycStatus: () => ({
    type: kycStatusType.KYC_STATUS_GET_CLEAR,
  })
}

export const KycStatusChangeActionsTypes = {
  KYC_STATUS_CHANGE: 'KYC_STATUS_CHANGE',
  SAVE_KYC_STATUS_CHANGE_RESPONSE: 'SAVE_KYC_STATUS_CHANGE_RESPONSE',
  CLEAR_KYC_STATUS_CHANGE: 'CLEAR_KYC_STATUS_CHANGE'
}

export const KycStatusChangeActions = {
  KycStatusChange: (params) => {
    return {
      type: KycStatusChangeActionsTypes.KYC_STATUS_CHANGE,
      params
    }
  },
  saveKycStatusChangeResponse: data => ({
    type: KycStatusChangeActionsTypes.SAVE_KYC_STATUS_CHANGE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearKycStatusChange: () => ({
    type: KycStatusChangeActionsTypes.CLEAR_KYC_STATUS_CHANGE
  })
}

export const KycUpdateQueueType = {
  KYC_UPDATE_QUEUE: "KYC_UPDATE_QUEUE",
  KYC_UPDATE_QUEUE_RESPONSE: "KYC_UPDATE_QUEUE_RESPONSE",
  KYC_UPDATE_QUEUE_CLEAR: "KYC_UPDATE_QUEUE_CLEAR"
}

export const KycUpdateQueueAction = {
  KycUpdateQueueDoc: (params) => {
    return {
      type: KycUpdateQueueType.KYC_UPDATE_QUEUE,
      params
    }
  },
  KycUpdateQueueResponse: data => ({
    type: KycUpdateQueueType.KYC_UPDATE_QUEUE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearKycUpdateQueueDoc: () => ({
    type: KycUpdateQueueType.KYC_UPDATE_QUEUE_CLEAR,
  })
}