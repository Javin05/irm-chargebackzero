export const MonitorActionsTypes = {
  GET_MONITOR_LIST: 'GET_MONITOR_LIST',
  SAVE_MONITOR_LIST_RESPONSE: 'SAVE_MONITOR_LIST_RESPONSE',
  CLEAR_MONITOR_LIST: 'CLEAR_MONITOR_LIST'
}

export const MonitorActions = {
  getMonitorlist: (params) => ({
    type: MonitorActionsTypes.GET_MONITOR_LIST,
    params
  }),
  saveMonitorlistResponse: (data) => ({
    type: MonitorActionsTypes.SAVE_MONITOR_LIST_RESPONSE,
    data
  }),
  clearMonitorlist: () => ({
    type: MonitorActionsTypes.CLEAR_MONITOR_LIST
  })
}

export const OGMactionsTypes = {
  OGM_POST: 'OGM_POST',
  OGM_POST_SAVE_RESPONSE: 'OGM_POST_SAVE_RESPONSE',
  CLEAR_OGM_LIST: 'CLEAR_OGM_LIST'
}

export const OGMactions = {
  OGMpost: (params) => ({
    type: OGMactionsTypes.OGM_POST,
    params
  }),
  saveOGMpostResponse: (data) => ({
    type: OGMactionsTypes.OGM_POST_SAVE_RESPONSE,
    data
  }),
  clearOGMpost: () => ({
    type: OGMactionsTypes.CLEAR_OGM_LIST
  })
}

export const MonitorDashboardActionsTypes = {
  GET_MONITORDASHBOARD: 'GET_MONITORDASHBOARD',
  SAVE_MONITORDASHBOARD_RESPONSE: 'SAVE_MONITORDASHBOARD_RESPONSE',
  CLEAR_MONITORDASHBOARD: 'CLEAR_MONITORDASHBOARD'
}

export const MonitorDashboardActions = {
  getMonitorDashboard: (id, date) => ({
    type: MonitorDashboardActionsTypes.GET_MONITORDASHBOARD,
    payload: {id, date}
  }),
  saveMonitorDashboardResponse: (data) => ({
    type: MonitorDashboardActionsTypes.SAVE_MONITORDASHBOARD_RESPONSE,
    data
  }),
  clearMonitorDashboard: () => ({
    type: MonitorDashboardActionsTypes.CLEAR_MONITORDASHBOARD
  })
}

export const OGMintervalActionsTypes = {
  GET_OGM_INTERVAL_LIST: 'GET_OGM_INTERVAL_LIST',
  SAVE_OGM_INTERVAL_LIST_RESPONSE: 'SAVE_OGM_INTERVAL_LIST_RESPONSE',
  CLEAR_OGM_INTERVAL_LIST: 'CLEAR_OGM_INTERVAL_LIST'
}

export const OGMintervalActions = {
  getOGMinterval: (params) => ({
    type: OGMintervalActionsTypes.GET_OGM_INTERVAL_LIST,
    params
  }),
  saveOGMintervalResponse: (data) => ({
    type: OGMintervalActionsTypes.SAVE_OGM_INTERVAL_LIST_RESPONSE,
    data
  }),
  clearOGMinterval: () => ({
    type: OGMintervalActionsTypes.CLEAR_OGM_INTERVAL_LIST
  })
}

export const MonitorDashboardIdTypes = {
  GET_MONITORDASHBOARDID: 'GET_MONITORDASHBOARDID',
  SAVE_MONITORDASHBOARDID_RESPONSE: 'SAVE_MONITORDASHBOARDID_RESPONSE',
  CLEAR_MONITORDASHBOARDID: 'CLEAR_MONITORDASHBOARDID'
}

export const MonitorDashboardIdActions = {
  getMonitorDashboardId: (params) => ({
    type: MonitorDashboardIdTypes.GET_MONITORDASHBOARDID,
    id: params
  }),
  saveMonitorDashboardIdResponse: (data) => ({
    type: MonitorDashboardIdTypes.SAVE_MONITORDASHBOARDID_RESPONSE,
    data
  }),
  clearMonitorDashboardId: () => ({
    type: MonitorDashboardIdTypes.CLEAR_MONITORDASHBOARDID
  })
}

export const MonitorDashboardDateTypes = {
  GET_MONITORDASHBOARD_DATE: 'GET_MONITORDASHBOARD_DATE',
  SAVE_MONITORDASHBOARD_DATE_RESPONSE: 'SAVE_MONITORDASHBOARD_DATE_RESPONSE',
  CLEAR_MONITORDASHBOARD_DATE: 'CLEAR_MONITORDASHBOARD_DATE'
}

export const MonitorDashboardDateActions = {
  getMonitorDashboardDate: (params) => ({
    type: MonitorDashboardDateTypes.GET_MONITORDASHBOARD_DATE,
    id: params
  }),
  saveMonitorDashboardDateResponse: (data) => ({
    type: MonitorDashboardDateTypes.SAVE_MONITORDASHBOARD_DATE_RESPONSE,
    data
  }),
  clearMonitorDashboardDate: () => ({
    type: MonitorDashboardDateTypes.CLEAR_MONITORDASHBOARD_DATE
  })
}

export const MonitorDashboardStatusTypes = {
  GET_MONITORDASHBOARD_STATUS: 'GET_MONITORDASHBOARD_STATUS',
  SAVE_MONITORDASHBOARD_STATUS_RESPONSE: 'SAVE_MONITORDASHBOARD_STATUS_RESPONSE',
  CLEAR_MONITORDASHBOARD_STATUS: 'CLEAR_MONITORDASHBOARD_STATUS'
}

export const MonitorDashboardStatusActions = {
  getMonitorDashboardStatus: (params) => ({
    type: MonitorDashboardStatusTypes.GET_MONITORDASHBOARD_STATUS,
    params
  }),
  saveMonitorDashboardStatusResponse: (data) => ({
    type: MonitorDashboardStatusTypes.SAVE_MONITORDASHBOARD_STATUS_RESPONSE,
    data
  }),
  clearMonitorDashboardStatus: () => ({
    type: MonitorDashboardStatusTypes.CLEAR_MONITORDASHBOARD_STATUS
  })
}

export const MonitorCountsTypes = {
  GET_MONITOR_COUNTS: 'GET_MONITOR_COUNTS',
  SAVE_MONITOR_COUNTS_RESPONSE: 'SAVE_MONITOR_COUNTS_RESPONSE',
  CLEAR_MONITOR_COUNTS: 'CLEAR_MONITOR_COUNTS'
}

export const MonitorCountsActions = {
  getMonitorCounts: (params) => ({
    type: MonitorCountsTypes.GET_MONITOR_COUNTS,
    params
  }),
  saveMonitorCountsResponse: (data) => ({
    type: MonitorCountsTypes.SAVE_MONITOR_COUNTS_RESPONSE,
    data
  }),
  clearMonitorCounts: () => ({
    type: MonitorCountsTypes.CLEAR_MONITOR_COUNTS
  })
}

export const MonitorActionUpdateTypes = {
  POST_MONITOR_ACTIONUPDATE: 'GET_MONITOR_ACTIONUPDATE',
  SAVE_MONITOR_ACTIONUPDATE_RESPONSE: 'SAVE_MONITOR_ACTIONUPDATE_RESPONSE',
  CLEAR_MONITOR_ACTIONUPDATE: 'CLEAR_MONITOR_ACTIONUPDATE'
}

export const MonitorActionUpdateActions = {
  PostMonitorActionUpdate: (params) => ({
    type: MonitorActionUpdateTypes.POST_MONITOR_ACTIONUPDATE,
    params
  }),
  saveMonitorActionUpdateResponse: (data) => ({
    type: MonitorActionUpdateTypes.SAVE_MONITOR_ACTIONUPDATE_RESPONSE,
    data
  }),
  clearMonitorActionUpdate: () => ({
    type: MonitorActionUpdateTypes.CLEAR_MONITOR_ACTIONUPDATE
  })
}

export const MonitorViewsTypes = {
  GET_MONITOR_VIEWS: 'GET_MONITOR_VIEWS',
  SAVE_MONITOR_VIEWS_RESPONSE: 'SAVE_MONITOR_VIEWS_RESPONSE',
  CLEAR_MONITOR_VIEWS: 'CLEAR_MONITOR_VIEWS'
}

export const MonitorViewsActions = {
  getMonitorViews: (params) => ({
    type: MonitorViewsTypes.GET_MONITOR_VIEWS,
    params
  }),
  saveMonitorViewsResponse: (data) => ({
    type: MonitorViewsTypes.SAVE_MONITOR_VIEWS_RESPONSE,
    data
  }),
  clearMonitorViews: () => ({
    type: MonitorViewsTypes.CLEAR_MONITOR_VIEWS
  })
}

export const UpdateCurrentReportTypes = {
  GET_UPDATE_CURRENT_REPORT: 'GET_UPDATE_CURRENT_REPORT',
  SAVE_UPDATE_CURRENT_REPORT_RESPONSE: 'SAVE_UPDATE_CURRENT_REPORT_RESPONSE',
  CLEAR_UPDATE_CURRENT_REPORT: 'CLEAR_UPDATE_CURRENT_REPORT'
}

export const UpdateCurrentReportActions = {
  getUpdateCurrentReport: (id, params) => ({
    type: UpdateCurrentReportTypes.GET_UPDATE_CURRENT_REPORT,
    payload: { id, params }
  }),
  saveUpdateCurrentReportResponse: (data) => ({
    type: UpdateCurrentReportTypes.SAVE_UPDATE_CURRENT_REPORT_RESPONSE,
    data
  }),
  clearUpdateCurrentReport: () => ({
    type: UpdateCurrentReportTypes.CLEAR_UPDATE_CURRENT_REPORT
  })
}

export const monitorExportReportTypes = {
  GET_MONITOR_EXPORT_REPORT: 'GET_MONITOR_EXPORT_REPORT',
  SAVE_MONITOR_EXPORT_REPORT_RESPONSE: 'SAVE_MONITOR_EXPORT_REPORT_RESPONSE',
  CLEAR_MONITOR_EXPORT_REPORT: 'CLEAR_MONITOR_EXPORT_REPORT'
}

export const monitorExportReportActions = {
  getmonitorExportReport: (params) => ({
    type: monitorExportReportTypes.GET_MONITOR_EXPORT_REPORT,
    params
  }),
  savemonitorExportReportResponse: (data) => ({
    type: monitorExportReportTypes.SAVE_MONITOR_EXPORT_REPORT_RESPONSE,
    data
  }),
  clearmonitorExportReport: () => ({
    type: monitorExportReportTypes.CLEAR_MONITOR_EXPORT_REPORT
  })
}

export const monitorDeleteTypes = {
  DELETE_MONITOR_REPORT: 'DELETE_MONITOR_REPORT',
  SAVE_DELETE_MONITOR_REPORT_RESPONSE: 'SAVE_DELETE_MONITOR_REPORT_RESPONSE',
  CLEAR_DELETE_MONITOR_REPORT: 'CLEAR_DELETE_MONITOR_REPORT'
}

export const deleteMonitorReportActions = {
  deletemonitorReport: (params) => ({
    type: monitorDeleteTypes.DELETE_MONITOR_REPORT,
    params
  }),
  saveDeletemonitorReportResponse: (data) => ({
    type: monitorDeleteTypes.SAVE_DELETE_MONITOR_REPORT_RESPONSE,
    data
  }),
  clearDeletemonitorReport: () => ({
    type: monitorDeleteTypes.CLEAR_DELETE_MONITOR_REPORT
  })
}

export const viewBackendDashboardTypes = {
  GET_BACKEND_DASHBOARD: 'GET_BACKEND_DASHBOARD',
  SAVE_GET_BACKEND_DASHBOARD_RESPONSE: 'SAVE_GET_BACKEND_DASHBOARD_RESPONSE',
  CLEAR_GET_BACKEND_DASHBOARD: 'CLEAR_GET_BACKEND_DASHBOARD'
}

export const viewBackendDashboardActions = {
  backendDashboardView: (params) => ({
    type: viewBackendDashboardTypes.GET_BACKEND_DASHBOARD,
    params
  }),
  savebackendDashboardViewResponse: (data) => ({
    type: viewBackendDashboardTypes.SAVE_GET_BACKEND_DASHBOARD_RESPONSE,
    data
  }),
  clearBackendDashboardView: () => ({
    type: viewBackendDashboardTypes.CLEAR_GET_BACKEND_DASHBOARD
  })
}

export const UpdateBackendDashboardTypes = {
  GET_UPDATE_BACKEND_DASHBOARD: 'GET_UPDATE_BACKEND_DASHBOARD',
  SAVE_UPDATE_BACKEND_DASHBOARD_RESPONSE: 'SAVE_UPDATE_BACKEND_DASHBOARD_RESPONSE',
  CLEAR_UPDATE_BACKEND_DASHBOARD: 'CLEAR_UPDATE_BACKEND_DASHBOARD'
}

export const UpdateBackendDashboardActions = {
  getUpdateBackendDashboard: (id, params) => ({
    type: UpdateBackendDashboardTypes.GET_UPDATE_BACKEND_DASHBOARD,
    payload: { id, params }
  }),
  saveUpdateBackendDashboardtResponse: (data) => ({
    type: UpdateBackendDashboardTypes.SAVE_UPDATE_BACKEND_DASHBOARD_RESPONSE,
    data
  }),
  clearUpdateBackendDashboard: () => ({
    type: UpdateBackendDashboardTypes.CLEAR_UPDATE_BACKEND_DASHBOARD
  })
}

export const MonitarStatusChangeActionsTypes = {
  MONITAR_STATUS_CHANGE: 'MONITAR_STATUS_CHANGE',
  SAVE_MONITAR_STATUS_CHANGE_RESPONSE: 'SAVE_MONITAR_STATUS_CHANGE_RESPONSE',
  CLEAR_MONITAR_STATUS_CHANGE: 'CLEAR_MONITAR_STATUS_CHANGE'
}

export const MonitarStatusChangeActions = {
  MonitarStatusChange: (params) => {
    return {
      type: MonitarStatusChangeActionsTypes.MONITAR_STATUS_CHANGE,
      params
    }
  },
  saveMonitarStatusChangeResponse: data => ({
    type: MonitarStatusChangeActionsTypes.SAVE_MONITAR_STATUS_CHANGE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearMonitarStatusChange: () => ({
    type: MonitarStatusChangeActionsTypes.CLEAR_MONITAR_STATUS_CHANGE
  })
}

export const MonitorUpdateQueueActionsTypes = {
  MONITOR_UPDATE_QUEUE: 'MONITOR_UPDATE_QUEUE',
  SAVE_MONITOR_UPDATE_QUEUE_RESPONSE: 'SAVE_MONITOR_UPDATE_QUEUE_RESPONSE',
  CLEAR_MONITOR_UPDATE_QUEUE: 'CLEAR_MONITOR_UPDATE_QUEUE'
}

export const MonitorUpdateQueueActions = {
  MonitorUpdateQueue: (params) => {
    return {
      type: MonitorUpdateQueueActionsTypes.MONITOR_UPDATE_QUEUE,
      params
    }
  },
  saveMonitorUpdateQueueResponse: data => ({
    type: MonitorUpdateQueueActionsTypes.SAVE_MONITOR_UPDATE_QUEUE_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearMonitorUpdateQueue: () => ({
    type: MonitorUpdateQueueActionsTypes.CLEAR_MONITOR_UPDATE_QUEUE
  })
}

export const UpdateViewNewCallbackTypes = {
  GET_UPDATE_VIEW_NEW_CALLBACK: 'GET_UPDATE_VIEW_NEW_CALLBACK',
  SAVE_UPDATE_VIEW_NEW_CALLBACK_RESPONSE: 'SAVE_UPDATE_VIEW_NEW_CALLBACK_RESPONSE',
  CLEAR_UPDATE_VIEW_NEW_CALLBACK: 'CLEAR_UPDATE_VIEW_NEW_CALLBACK'
}

export const UpdateViewNewCallbackActions = {
  getUpdateViewNewCallback: (id, params) => ({
    type: UpdateViewNewCallbackTypes.GET_UPDATE_VIEW_NEW_CALLBACK,
    payload: { id, params }
  }),
  saveUpdateViewNewCallbacktResponse: (data) => ({
    type: UpdateViewNewCallbackTypes.SAVE_UPDATE_VIEW_NEW_CALLBACK_RESPONSE,
    data
  }),
  clearUpdateViewNewCallback: () => ({
    type: UpdateViewNewCallbackTypes.CLEAR_UPDATE_VIEW_NEW_CALLBACK
  })
}

export const OGMEmailTemplateTypes = {
  OGM_EMAIL_TEMPLATE_POST: 'OGM_EMAIL_TEMPLATE_POST',
  OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE: 'OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE',
  CLEAR_OGM_EMAIL_TEMPLATE_LIST: 'CLEAR_OGM_EMAIL_TEMPLATE_LIST'
}

export const OGMEmailTemplateactions = {
  OGMEmailTemplatepost: (params) => ({
    type: OGMEmailTemplateTypes.OGM_EMAIL_TEMPLATE_POST,
    params
  }),
  saveOGMEmailTemplatepostResponse: (data) => ({
    type: OGMEmailTemplateTypes.OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE,
    data
  }),
  clearOGMEmailTemplatepost: () => ({
    type: OGMEmailTemplateTypes.CLEAR_OGM_EMAIL_TEMPLATE_LIST
  })
}

export const UpdateOGMEmailTemplateTypes = {
  GET_UPDATE_OGM_EMAIL_TEMPLATE: 'GET_UPDATE_OGM_EMAIL_TEMPLATE',
  SAVE_UPDATE_OGM_EMAIL_TEMPLATE_RESPONSE: 'SAVE_UPDATE_OGM_EMAIL_TEMPLATE_RESPONSE',
  CLEAR_UPDATE_OGM_EMAIL_TEMPLATE: 'CLEAR_UPDATE_OGM_EMAIL_TEMPLATE'
}

export const UpdateOGMEmailTemplateActions = {
  getUpdateOGMEmailTemplate: ( params) => ({
    type: UpdateOGMEmailTemplateTypes.GET_UPDATE_OGM_EMAIL_TEMPLATE,
    payload: { params }
  }),
  saveUpdateOGMEmailTemplateResponse: (data) => ({
    type: UpdateOGMEmailTemplateTypes.SAVE_UPDATE_OGM_EMAIL_TEMPLATE_RESPONSE,
    data
  }),
  clearUpdateOGMEmailTemplate: () => ({
    type: UpdateOGMEmailTemplateTypes.CLEAR_UPDATE_OGM_EMAIL_TEMPLATE
  })
}

export const DashbordOGMEmailTemplateTypes = {
  DASHBOARD_OGM_EMAIL_TEMPLATE_POST: 'DASHBOARD_OGM_EMAIL_TEMPLATE_POST',
  DASHBOARD_OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE: 'DASHBOARD_OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE',
  CLEAR_DASHBOARD_OGM_EMAIL_TEMPLATE_LIST: 'CLEAR_DASHBOARD_OGM_EMAIL_TEMPLATE_LIST'
}

export const DashbordOGMEmailTemplateactions = {
  DashbordOGMEmailTemplatepost: (params) => ({
    type: DashbordOGMEmailTemplateTypes.DASHBOARD_OGM_EMAIL_TEMPLATE_POST,
    params
  }),
  saveDashbordOGMEmailTemplatepostResponse: (data) => ({
    type: DashbordOGMEmailTemplateTypes.DASHBOARD_OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE,
    data
  }),
  clearDashbordOGMEmailTemplatepost: () => ({
    type: DashbordOGMEmailTemplateTypes.CLEAR_DASHBOARD_OGM_EMAIL_TEMPLATE_LIST
  })
}

export const EmailHistoryIdTypes = {
  GET_EMAIL_HISTORY_ID: 'GET_EMAIL_HISTORY_ID',
  SAVE_EMAIL_HISTORY_ID_RESPONSE: 'SAVE_EMAIL_HISTORY_ID_RESPONSE',
  CLEAR_EMAIL_HISTORY_ID: 'CLEAR_EMAIL_HISTORY_ID'
}

export const EmailHistoryIdActions = {
  getEmailHistoryId: (params) => ({
    type: EmailHistoryIdTypes.GET_EMAIL_HISTORY_ID,
    id: params
  }),
  saveEmailHistoryIdResponse: (data) => ({
    type: EmailHistoryIdTypes.SAVE_EMAIL_HISTORY_ID_RESPONSE,
    data
  }),
  clearEmailHistoryId: () => ({
    type: EmailHistoryIdTypes.CLEAR_EMAIL_HISTORY_ID
  })
}

export const OgmForceUploadTypes = {
  OGM_FORCE_UPLOAD_POST: 'OGM_FORCE_UPLOAD_POST',
  OGM_FORCE_UPLOAD_POST_SAVE_RESPONSE: 'OGM_FORCE_UPLOAD_POST_SAVE_RESPONSE',
  CLEAR_OGM_FORCE_UPLOAD_LIST: 'CLEAR_OGM_FORCE_UPLOAD_LIST'
}

export const OgmForceUploadAction = {
  OgmForceUpload: (params) => ({
    type: OgmForceUploadTypes.OGM_FORCE_UPLOAD_POST,
    params
  }),
  saveOgmForceUploadResponse: (data) => ({
    type: OgmForceUploadTypes.OGM_FORCE_UPLOAD_POST_SAVE_RESPONSE,
    data
  }),
  clearOgmForceUpload: () => ({
    type: OgmForceUploadTypes.CLEAR_OGM_FORCE_UPLOAD_LIST
  })
}