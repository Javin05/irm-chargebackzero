export const EddRiskManagementActionsTypes = {
  GET_EDDRISKMANAGEMENT_LIST: 'GET_EDDRISKMANAGEMENT_LIST',
  SAVE_EDDRISKMANAGEMENT_LIST_RESPONSE: 'SAVE_EDDRISKMANAGEMENT_LIST_RESPONSE',
  CLEAR_EDDRISKMANAGEMENT_LIST: 'CLEAR_EDDRISKMANAGEMENT_LIST'
}

export const EddRiskManagementActions = {
  getEddRiskMangemnt: (params) => ({
    type: EddRiskManagementActionsTypes.GET_EDDRISKMANAGEMENT_LIST,
    params
  }),
  saveEddRiskMangemntResponse: (data) => ({
    type: EddRiskManagementActionsTypes.SAVE_EDDRISKMANAGEMENT_LIST_RESPONSE,
    data
  }),
  clearEddRiskMangemnt: () => ({
    type: EddRiskManagementActionsTypes.CLEAR_EDDRISKMANAGEMENT_LIST
  })
}

export const AddEddRiskManagementTypes = {
  GET_ADD_EDD_RISKMANAGEMENT: 'GET_ADD_EDD_RISKMANAGEMENT',
  SAVE_ADD_EDD_RISKMANAGEMENT_RESPONSE: 'SAVE_ADD_EDD_RISKMANAGEMENT_RESPONSE',
  CLEAR_ADD_EDD_RISKMANAGEMENT: 'CLEAR_ADD_EDD_RISKMANAGEMENT'
}

export const AddEddRiskManagementActions = {
  getUpdateWebReport: (params) => ({
    type: AddEddRiskManagementTypes.GET_ADD_EDD_RISKMANAGEMENT,
    params
  }),
  saveUpdateWebReportResponse: (data) => ({
    type: AddEddRiskManagementTypes.SAVE_ADD_EDD_RISKMANAGEMENT_RESPONSE,
    data
  }),
  clearUpdateWebReport: () => ({
    type: AddEddRiskManagementTypes.CLEAR_ADD_EDD_RISKMANAGEMENT
  })
}

export const eddMerchantIdDetailsTypes = {
  GET_EDD_MERCHANT_ID: 'GET_EDD_MERCHANT_ID',
  EDD_MERCHANT_ID_RESPONSE: 'EDD_MERCHANT_ID_RESPONSE',
  CLEAR_EDD_MERCHANT_ID: 'CLEAR_EDD_MERCHANT_ID'
}

export const eddMerchantIdDetailsActions = {
  getEddMerchantIdDetailsData: (id) => ({
    type: eddMerchantIdDetailsTypes.GET_EDD_MERCHANT_ID,
    id
  }),
  saveEddMerchantIdDetailsResponse: data => ({
    type: eddMerchantIdDetailsTypes.EDD_MERCHANT_ID_RESPONSE,
    data
  }),
  clearEddMerchantIdData: () => ({
    type: eddMerchantIdDetailsTypes.CLEAR_EDD_MERCHANT_ID
  })
}

export const BusinessTypeActionsTypes = {
  GET_BUSINESS_TYPE_LIST: 'GET_BUSINESS_TYPE_LIST',
  SAVE_BUSINESS_TYPE_LIST_RESPONSE: 'SAVE_BUSINESS_TYPE_LIST_RESPONSE',
  CLEAR_BUSINESS_TYPE_LIST: 'CLEAR_BUSINESS_TYPE_LIST'
}

export const BusinessTypeActions = {
  getBusinessType: (params) => ({
    type: BusinessTypeActionsTypes.GET_BUSINESS_TYPE_LIST,
    params
  }),
  saveBusinessTypeResponse: (data) => ({
    type: BusinessTypeActionsTypes.SAVE_BUSINESS_TYPE_LIST_RESPONSE,
    data
  }),
  clearBusinessType: () => ({
    type: BusinessTypeActionsTypes.CLEAR_BUSINESS_TYPE_LIST
  })
}

export const BusinessAgeActionsTypes = {
  GET_BUSINESS_AGE_LIST: 'GET_BUSINESS_AGE_LIST',
  SAVE_BUSINESS_AGE_LIST_RESPONSE: 'SAVE_BUSINESS_AGE_LIST_RESPONSE',
  CLEAR_BUSINESS_AGE_LIST: 'CLEAR_BUSINESS_AGE_LIST'
}

export const BusinessAgeActions = {
  getBusinessAge: (params) => ({
    type: BusinessAgeActionsTypes.GET_BUSINESS_AGE_LIST,
    params
  }),
  saveBusinessAgeResponse: (data) => ({
    type: BusinessAgeActionsTypes.SAVE_BUSINESS_AGE_LIST_RESPONSE,
    data
  }),
  clearBusinessAge: () => ({
    type: BusinessAgeActionsTypes.CLEAR_BUSINESS_AGE_LIST
  })
}

export const BusinessNdxActionsTypes = {
  GET_BUSINESS_NDX_LIST: 'GET_BUSINESS_NDX_LIST',
  SAVE_BUSINESS_NDX_LIST_RESPONSE: 'SAVE_BUSINESS_NDX_LIST_RESPONSE',
  CLEAR_BUSINESS_NDX_LIST: 'CLEAR_BUSINESS_NDX_LIST'
}

export const BusinessNdxActions = {
  getBusinessNdx: (params) => ({
    type: BusinessNdxActionsTypes.GET_BUSINESS_NDX_LIST,
    params
  }),
  saveBusinessNdxResponse: (data) => ({
    type: BusinessNdxActionsTypes.SAVE_BUSINESS_NDX_LIST_RESPONSE,
    data
  }),
  clearBusinessNdx: () => ({
    type: BusinessNdxActionsTypes.CLEAR_BUSINESS_NDX_LIST
  })
}

export const PaymentInstrumentActionsTypes = {
  GET_PAYMENT_INSTRUMENT_LIST: 'GET_PAYMENT_INSTRUMENT_LIST',
  SAVE_PAYMENT_INSTRUMENT_LIST_RESPONSE: 'SAVE_PAYMENT_INSTRUMENT_LIST_RESPONSE',
  CLEAR_PAYMENT_INSTRUMENT_LIST: 'CLEAR_PAYMENT_INSTRUMENT_LIST'
}

export const PaymentInstrumentActions = {
  getPaymentInstrument: (params) => ({
    type: PaymentInstrumentActionsTypes.GET_PAYMENT_INSTRUMENT_LIST,
    params
  }),
  savePaymentInstrumentResponse: (data) => ({
    type: PaymentInstrumentActionsTypes.SAVE_PAYMENT_INSTRUMENT_LIST_RESPONSE,
    data
  }),
  clearPaymentInstrument: () => ({
    type: PaymentInstrumentActionsTypes.CLEAR_PAYMENT_INSTRUMENT_LIST
  })
}

export const EddExportActionsTypes = {
  GET_EDD_EXPORT_LIST: 'GET_EDD_EXPORT_LIST',
  SAVE_EDD_EXPORT_LIST_RESPONSE: 'SAVE_EDD_EXPORT_LIST_RESPONSE',
  CLEAR_EDD_EXPORT_LIST: 'CLEAR_EDD_EXPORT_LIST'
}

export const EddExportActions = {
  getEddExport: (params) => ({
    type: EddExportActionsTypes.GET_EDD_EXPORT_LIST,
    params
  }),
  saveEddExportResponse: (data) => ({
    type: EddExportActionsTypes.SAVE_EDD_EXPORT_LIST_RESPONSE,
    data
  }),
  clearEddExport: () => ({
    type: EddExportActionsTypes.CLEAR_EDD_EXPORT_LIST
  })
}

export const EddDashboardActionsTypes = {
  GET_EDD_DASHBOARD_DETAILS: 'GET_EDD_DASHBOARD_DETAILS',
  EDD_DASHBOARD_RESPONSE: 'EDD_DASHBOARD_RESPONSE',
  CLEAR_EDD_DASHBOARD_DETAILS: 'CLEAR_EDD_DASHBOARD_DETAILS'
}

export const EddDashboardActions = {
  getEddDashboardDetails: (id) => ({
    type: EddDashboardActionsTypes.GET_EDD_DASHBOARD_DETAILS,
    id
  }),
  saveEddDashboardResponse: data => ({
    type: EddDashboardActionsTypes.EDD_DASHBOARD_RESPONSE,
    data
  }),
  clearEddDashboardDetails: () => ({
    type: EddDashboardActionsTypes.CLEAR_EDD_DASHBOARD_DETAILS
  })
}

export const EddApiDetailsActionsTypes = {
  GET_API_DETAILS: 'GET_API_DETAILS',
  EDD_API_DETAILS_RESPONSE: 'EDD_API_DETAILS_RESPONSE',
  CLEAR_API_DETAILS_DETAILS: 'CLEAR_API_DETAILS_DETAILS'
}

export const EddApiDetailsActions = {
  getEddApiDetails: (id) => ({
    type: EddApiDetailsActionsTypes.GET_API_DETAILS,
    id
  }),
  saveEddApiDetailsResponse: data => ({
    type: EddApiDetailsActionsTypes.EDD_API_DETAILS_RESPONSE,
    data
  }),
  clearEddApiDetails: () => ({
    type: EddApiDetailsActionsTypes.CLEAR_API_DETAILS_DETAILS
  })
}

export const EddRiskManagementApiFinalListTypes = {
  GET_EDD_API_FINAL_LIST: 'GET_EDD_API_FINAL_LIST',
  SAVE_EDD_API_FINAL_LIST_RESPONSE: 'SAVE_EDD_API_FINAL_LIST_RESPONSE',
  CLEAR_EDD_API_FINAL_LIST: 'CLEAR_EDD_API_FINAL_LIST'
}

export const EddRiskManagementApiFinalListActions = {
  getEddApiFinalList: (params) => ({
    type: EddRiskManagementApiFinalListTypes.GET_EDD_API_FINAL_LIST,
    params
  }),
  saveEddApiFinalListResponse: (data) => ({
    type: EddRiskManagementApiFinalListTypes.SAVE_EDD_API_FINAL_LIST_RESPONSE,
    data
  }),
  clearEddApiFinalList: () => ({
    type: EddRiskManagementApiFinalListTypes.CLEAR_EDD_API_FINAL_LIST
  })
}

export const FinalDecisionUpdateActionsTypes = {
  GET_FINAL_DECISION_UPDATE: 'GET_FINAL_DECISION_UPDATE',
  SAVE_FINAL_DECISION_UPDATE_RESPONSE: 'SAVE_FINAL_DECISION_UPDATE_RESPONSE',
  CLEAR_FINAL_DECISION_UPDATE: 'CLEAR_FINAL_DECISION_UPDATE'
}

export const FinalDecisionUpdateActions = {
  getFinalDecisionUpdate: (params) => ({
      type: FinalDecisionUpdateActionsTypes.GET_FINAL_DECISION_UPDATE,
      params
  }),
  saveFinalDecisionUpdateResponse: (data) => ({
      type: FinalDecisionUpdateActionsTypes.SAVE_FINAL_DECISION_UPDATE_RESPONSE,
      data
  }),
  clearFinalDecisionUpdate: () => ({
      type: FinalDecisionUpdateActionsTypes.CLEAR_FINAL_DECISION_UPDATE
  })
}

export const eddApiScheduledListTypes = {
  GET_EDD_API_SCHEDULED_LIST_ID: 'GET_EDD_API_SCHEDULED_LIST_ID',
  EDD_API_SCHEDULED_LIST_ID_RESPONSE: 'EDD_API_SCHEDULED_LIST_ID_RESPONSE',
  CLEAR_EDD_API_SCHEDULED_LIST_ID: 'CLEAR_EDD_API_SCHEDULED_LIST_ID'
}

export const eddApiScheduledListActions = {
  getEddApiScheduledListData: (id) => ({
    type: eddApiScheduledListTypes.GET_EDD_API_SCHEDULED_LIST_ID,
    id
  }),
  saveEddApiScheduledListResponse: data => ({
    type: eddApiScheduledListTypes.EDD_API_SCHEDULED_LIST_ID_RESPONSE,
    data
  }),
  clearEddApiScheduledListData: () => ({
    type: eddApiScheduledListTypes.CLEAR_EDD_API_SCHEDULED_LIST_ID
  })
}

export const eddApiScheduledListReportTypes = {
  GET_EDD_API_SCHEDULED_LIST_REPORT: 'GET_EDD_API_SCHEDULED_LIST_REPORT',
  EDD_API_SCHEDULED_LIST_REPORT_RESPONSE: 'EDD_API_SCHEDULED_LIST_REPORT_RESPONSE',
  CLEAR_EDD_API_SCHEDULED_LIST_REPORT: 'CLEAR_EDD_API_SCHEDULED_LIST_REPORT'
}

export const eddApiScheduledListReportActions = {
  getEddApiScheduledListReportData: (id) => ({
    type: eddApiScheduledListReportTypes.GET_EDD_API_SCHEDULED_LIST_REPORT,
    id
  }),
  saveEddApiScheduledListReportResponse: data => ({
    type: eddApiScheduledListReportTypes.EDD_API_SCHEDULED_LIST_REPORT_RESPONSE,
    data
  }),
  clearEddApiScheduledListReportData: () => ({
    type: eddApiScheduledListReportTypes.CLEAR_EDD_API_SCHEDULED_LIST_REPORT
  })
}

export const ReTriggerFinalApiUpdateActionsTypes = {
  GET_RE_TRIGGER_FINAL_API_UPDATE: 'GET_RE_TRIGGER_FINAL_API_UPDATE',
  SAVE_RE_TRIGGER_FINAL_API_UPDATE_RESPONSE: 'SAVE_RE_TRIGGER_FINAL_API_UPDATE_RESPONSE',
  CLEAR_RE_TRIGGER_FINAL_API_UPDATE: 'CLEAR_RE_TRIGGER_FINAL_API_UPDATE'
}

export const ReTriggerFinalApiUpdateActions = {
  getReTriggerFinalApiUpdate: (params) => ({
      type: ReTriggerFinalApiUpdateActionsTypes.GET_RE_TRIGGER_FINAL_API_UPDATE,
      params
  }),
  saveReTriggerFinalApiUpdateResponse: (data) => ({
      type: ReTriggerFinalApiUpdateActionsTypes.SAVE_RE_TRIGGER_FINAL_API_UPDATE_RESPONSE,
      data
  }),
  clearReTriggerFinalApiUpdate: () => ({
      type: ReTriggerFinalApiUpdateActionsTypes.CLEAR_RE_TRIGGER_FINAL_API_UPDATE
  })
}

export const EDDUpdateActionsTypes = {
  GET_EDD_UPDATE: 'GET_EDD_UPDATE',
  SAVE_EDD_UPDATE_RESPONSE: 'SAVE_EDD_UPDATE_RESPONSE',
  CLEAR_EDD_UPDATE: 'CLEAR_EDD_UPDATE'
}

export const EDDUpdateActions = {
  getEDDUpdate: (id, params) => ({
      type: EDDUpdateActionsTypes.GET_EDD_UPDATE,
      id,
      params
  }),
  saveEDDUpdateResponse: (data) => ({
      type: EDDUpdateActionsTypes.SAVE_EDD_UPDATE_RESPONSE,
      data
  }),
  clearEDDUpdate: () => ({
      type: EDDUpdateActionsTypes.CLEAR_EDD_UPDATE
  })
}

export const eddHistoryListActionTypes = {
  GET_EDD_HISTORY_LIST: 'GET_EDD_HISTORY_LIST',
  EDD_HISTORY_LIST_RESPONSE: 'EDD_HISTORY_LIST_RESPONSE',
  CLEAR_EDD_HISTORY_LIST: 'CLEAR_EDD_HISTORY_LIST'
}

export const eddHistoryListActions = {
  getEddHistoryList: (id) => ({
    type: eddHistoryListActionTypes.GET_EDD_HISTORY_LIST,
    id
  }),
  saveEddHistoryListResponse: data => ({
    type: eddHistoryListActionTypes.EDD_HISTORY_LIST_RESPONSE,
    data
  }),
  clearEddHistoryList: () => ({
    type: eddHistoryListActionTypes.CLEAR_EDD_HISTORY_LIST
  })
}