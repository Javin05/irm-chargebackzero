export const WatchListTypes = {
    FETCH_WATCHLIST_INIT: 'FETCH_WATCHLIST_INIT',
    FETCH_WATCHLIST_SUCCESS: 'FETCH_WATCHLIST_SUCCESS',
    FETCH_WATCHLIST_ERROR: 'FETCH_WATCHLIST_ERROR',

    FETCH_WATCHLISTTYPE_INIT: 'FETCH_WATCHLISTTYPE_INIT',
    FETCH_WATCHLISTTYPE_SUCCESS: 'FETCH_WATCHLISTTYPE_SUCCESS',
    FETCH_WATCHLISTTYPE_ERROR: 'FETCH_WATCHLISTTYPE_ERROR',

    CREATE_WATCHLISTTYPE_INIT: 'CREATE_WATCHLISTTYPE_INIT',
    CREATE_WATCHLISTTYPE_SUCCESS: 'CREATE_WATCHLISTTYPE_SUCCESS',
    CREATE_WATCHLISTTYPE_ERROR: 'CREATE_WATCHLISTTYPE_ERROR',

    UPDATE_WATCHLISTTYPE_INIT: 'UPDATE_WATCHLISTTYPE_INIT',
    UPDATE_WATCHLISTTYPE_SUCCESS: 'UPDATE_WATCHLISTTYPE_SUCCESS',
    UPDATE_WATCHLISTTYPE_ERROR: 'UPDATE_WATCHLISTTYPE_ERROR',

    DELETE_WATCHLISTTYPE_INIT: 'DELETE_WATCHLISTTYPE_INIT',
    DELETE_WATCHLISTTYPE_SUCCESS: 'DELETE_WATCHLISTTYPE_SUCCESS',
    DELETE_WATCHLISTTYPE_ERROR: 'DELETE_WATCHLISTTYPE_ERROR',
}
export const WatchListActions = {

    fetchWatchList: (params) => ({
        type: WatchListTypes.FETCH_WATCHLIST_INIT,
        params
    }),
    fetchWatchListSuccess: (data) => ({
        type: WatchListTypes.FETCH_WATCHLIST_SUCCESS,
        data
    }),
    fetchWatchListError: () => ({
        type: WatchListTypes.FETCH_WATCHLIST_ERROR
    }),

    fetchWatchListType: (params) => ({
        type: WatchListTypes.FETCH_WATCHLISTTYPE_INIT,
        params
    }),
    fetchWatchListTypeSuccess: (data) => ({
        type: WatchListTypes.FETCH_WATCHLISTTYPE_SUCCESS,
        data
    }),
    fetchWatchListTypeError: () => ({
        type: WatchListTypes.FETCH_WATCHLISTTYPE_ERROR
    }),

    createWatchListType: (formData) => ({
        type: WatchListTypes.CREATE_WATCHLISTTYPE_INIT,
        formData
    }),
    createWatchListTypeSuccess: (data) => ({
        type: WatchListTypes.CREATE_WATCHLISTTYPE_SUCCESS,
        data
    }),
    createWatchListTypeError: () => ({
        type: WatchListTypes.CREATE_WATCHLISTTYPE_ERROR
    }),

    updateWatchListType: (id, formData) => ({
        type: WatchListTypes.UPDATE_WATCHLISTTYPE_INIT,
        id,
        formData
    }),
    updateWatchListTypeSuccess: (data) => ({
        type: WatchListTypes.UPDATE_WATCHLISTTYPE_SUCCESS,
        data
    }),
    updateWatchListTypeError: () => ({
        type: WatchListTypes.UPDATE_WATCHLISTTYPE_ERROR
    }),

    deleteWatchListType: (id, params) => ({
        type: WatchListTypes.DELETE_WATCHLISTTYPE_INIT,
        id,
        params
    }),
    deleteWatchListTypeSuccess: (data) => ({
        type: WatchListTypes.DELETE_WATCHLISTTYPE_SUCCESS,
        data
    }),
    deleteWatchListTypeError: () => ({
        type: WatchListTypes.DELETE_WATCHLISTTYPE_ERROR
    }),
}