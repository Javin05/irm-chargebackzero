export const OgmForceUploadSummaryTypes = {
    OGM_FORCE_UPLOAD_SUMMARYLIST: "OGM_FORCE_UPLOAD_SUMMARYLIST",
    OGM_FORCE_UPLOAD_SUMMARYLIST_SUCCESS: "OGM_FORCE_UPLOAD_SUMMARYLIST_SUCCESS",
    OGM_FORCE_UPLOAD_SUMMARYLIST_CLEAR: "OGM_FORCE_UPLOAD_SUMMARYLIST_CLEAR",
  }
  
  export const OgmForceUploadSummaryAction = {
    getOgmForceUpload: (params) => ({
      type: OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST,
      params,
    }),
    saveOgmForceUpload: (data) => ({
      type: OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST_SUCCESS,
      data,
    }),
    clearOgmForceUpload: () => ({
      type: OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST_CLEAR,
    })
  }