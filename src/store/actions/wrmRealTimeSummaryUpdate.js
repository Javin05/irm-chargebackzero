export const updateWrmRealtimeupdateimagesActionsTypes = {
    UPDATE_WRM_REALTIME_IMAGE: 'UPDATE_WRM_REALTIME_IMAGE',
    SAVE_UPDATE_WRM_REALTIME_IMAGE_RESPONSE: 'SAVE_UPDATE_WRM_REALTIME_IMAGE_RESPONSE',
    CLEAR_UPDATE_WRM_REALTIME_IMAGE: 'CLEAR_UPDATE_WRM_REALTIME_IMAGE'
  }

export const updateWrmRealtimeupdateimagesActions = {
    updateWrmRealtimeupdateimages: (id, params) => {
      return {
        type: updateWrmRealtimeupdateimagesActionsTypes.UPDATE_WRM_REALTIME_IMAGE,
        payload:  { id, params }
      }
    },
    saveupdateWrmRealtimeupdateimagesResponse: data => ({
      type: updateWrmRealtimeupdateimagesActionsTypes.SAVE_UPDATE_WRM_REALTIME_IMAGE_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearupdateWrmRealtimeupdateimages: () => ({
      type: updateWrmRealtimeupdateimagesActionsTypes.CLEAR_UPDATE_WRM_REALTIME_IMAGE
    })
  }

  export const updateWebsiteStatusChangeActionsTypes = {
    UPDATE_WRM_WEBSITE_STATUS_CHANGE: 'UPDATE_WRM_WEBSITE_STATUS_CHANGE',
    SAVE_UPDATE_WRM_WEBSITE_STATUS_CHANGE_RESPONSE: 'SAVE_UPDATE_WRM_WEBSITE_STATUS_CHANGE_RESPONSE',
    CLEAR_UPDATE_WRM_WEBSITE_STATUS_CHANGE: 'CLEAR_UPDATE_WRM_WEBSITE_STATUS_CHANGE'
  }

  export const updateWebsiteStatusChangeActions = {
    updateWebsiteStatusChange: (id, params) => {
      return {
        type: updateWebsiteStatusChangeActionsTypes.UPDATE_WRM_WEBSITE_STATUS_CHANGE,
        payload:  { id, params }
      }
    },
    saveupdateWebsiteStatusChangeResponse: data => ({
      type: updateWebsiteStatusChangeActionsTypes.SAVE_UPDATE_WRM_WEBSITE_STATUS_CHANGE_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearupdateWebsiteStatusChange: () => ({
      type: updateWebsiteStatusChangeActionsTypes.CLEAR_UPDATE_WRM_WEBSITE_STATUS_CHANGE
    })
  }

  export const WRMExportWebreportTypes = {
    GET_WRM_EXPORT_WEBREPORT_LIST: 'GET_WRM_EXPORT_WEBREPORT_LIST',
    SAVE_WRM_EXPORT_WEBREPORT_LIST_RESPONSE: 'SAVE_WRM_EXPORT_WEBREPORT_LIST_RESPONSE',
    CLEAR_WRM_EXPORT_WEBREPORT_LIST: 'CLEAR_WRM_EXPORT_WEBREPORT_LIST'
  }
  
  export const WRMExportWebreportTypesActions = {
    getWRMExportWebReportlist: (params) => ({
      type: WRMExportWebreportTypes.GET_WRM_EXPORT_WEBREPORT_LIST,
      params
    }),
    saveWRMExportWebReportlistResponse: (data) => ({
      type: WRMExportWebreportTypes.SAVE_WRM_EXPORT_WEBREPORT_LIST_RESPONSE,
      data
    }),
    clearWRMExportWebReportlist: () => ({
      type: WRMExportWebreportTypes.CLEAR_WRM_EXPORT_WEBREPORT_LIST
    })
  }

  export const WrmRealtimeUpdateGetByIdTypes = {
    GET_WRM_REALTIME_UPDATE_BY_ID: 'GET_WRM_REALTIME_UPDATE_BY_ID',
    SAVE_WRM_REALTIME_UPDATE_BY_ID_RESPONSE: 'SAVE_WRM_REALTIME_UPDATE_BY_ID_RESPONSE',
    CLEAR_WRM_REALTIME_UPDATE_BY_ID: 'CLEAR_WRM_REALTIME_UPDATE_BY_ID'
  }
  
  export const WrmRealtimeUpdateGetByIdActions = {
    getWrmRealtimeUpdateGetById: (id) => ({
      type: WrmRealtimeUpdateGetByIdTypes.GET_WRM_REALTIME_UPDATE_BY_ID,
      id
    }),
    saveWrmRealtimeUpdateGetByIdResponse: (data) => ({
      type: WrmRealtimeUpdateGetByIdTypes.SAVE_WRM_REALTIME_UPDATE_BY_ID_RESPONSE,
      data
    }),
    clearWrmRealtimeUpdateGetById: () => ({
      type: WrmRealtimeUpdateGetByIdTypes.CLEAR_WRM_REALTIME_UPDATE_BY_ID
    })
  }

  export const WrmRealtimeUpdateDashboardGetByIdTypes = {
    GET_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID: 'GET_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID',
    SAVE_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID_RESPONSE: 'SAVE_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID_RESPONSE',
    CLEAR_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID: 'CLEAR_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID'
  }
  
  export const WrmRealtimeUpdateDashboardGetByIdActions = {
    getWrmRealtimeUpdateDashboardGetById: (id) => ({
      type: WrmRealtimeUpdateDashboardGetByIdTypes.GET_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID,
      id
    }),
    saveWrmRealtimeUpdateDashboardGetByIdResponse: (data) => ({
      type: WrmRealtimeUpdateDashboardGetByIdTypes.SAVE_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID_RESPONSE,
      data
    }),
    clearWrmRealtimeUpdateDashboardGetById: () => ({
      type: WrmRealtimeUpdateDashboardGetByIdTypes.CLEAR_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID
    })
  }

  export const WrmRealtimeUpdateDistancedataGetByIdTypes = {
    GET_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID: 'GET_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID',
    SAVE_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID_RESPONSE: 'SAVE_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID_RESPONSE',
    CLEAR_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID: 'CLEAR_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID'
  }
  
  export const WrmRealtimeUpdateDistancedataGetByIdActions = {
    getWrmRealtimeUpdateDistanceDataGetById: (id) => ({
      type: WrmRealtimeUpdateDistancedataGetByIdTypes.GET_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID,
      id
    }),
    saveWrmRealtimeUpdateDistanceDataGetByIdResponse: (data) => ({
      type: WrmRealtimeUpdateDistancedataGetByIdTypes.SAVE_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID_RESPONSE,
      data
    }),
    clearWrmRealtimeUpdateDistanceDataGetById: () => ({
      type: WrmRealtimeUpdateDistancedataGetByIdTypes.CLEAR_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID
    })
  }

  export const WrmRTUpdateRedirectionURLActionsTypes = {
    WRMRT_UPDATE_REDIRECTION_URL: 'WRMRT_UPDATE_REDIRECTION_URL',
    SAVE_WRMRT_UPDATE_REDIRECTION_URL_RESPONSE: 'SAVE_WRMRT_UPDATE_REDIRECTION_URL_RESPONSE',
    CLEAR_WRMRT_UPDATE_REDIRECTION_URL: 'CLEAR_WRMRT_UPDATE_REDIRECTION_URL'
  }
  
  export const WrmRTUpdateRedirectionURLActions = {
    WrmRTUpdateRedirectionURL: (id, params) => {
      return {
        type: WrmRTUpdateRedirectionURLActionsTypes.WRMRT_UPDATE_REDIRECTION_URL,
        payload:  { id, params }
      }
    },
    saveWrmRTUpdateRedirectionURLResponse: data => ({
      type: WrmRTUpdateRedirectionURLActionsTypes.SAVE_WRMRT_UPDATE_REDIRECTION_URL_RESPONSE,
      data,
      status: data.status,
      message: data.message
    }),
    clearWrmRTUpdateRedirectionURL: () => ({
      type: WrmRTUpdateRedirectionURLActionsTypes.CLEAR_WRMRT_UPDATE_REDIRECTION_URL
    })
  }

  export const WRMExportPlayStoreReportTypes = {
    GET_WRM_EXPORT_PLAYSTORE_REPORT_LIST: 'GET_WRM_EXPORT_PLAYSTORE_REPORT_LIST',
    SAVE_WRM_EXPORT_PLAYSTORE_REPORT_LIST_RESPONSE: 'SAVE_WRM_EXPORT_PLAYSTORE_REPORT_LIST_RESPONSE',
    CLEAR_WRM_EXPORT_PLAYSTORE_REPORT_LIST: 'CLEAR_WRM_EXPORT_PLAYSTORE_REPORT_LIST'
  }
  
  export const WRMExportPlayStoreReportActions = {
    getWRMExportPlayStoreReportlist: (params) => ({
      type: WRMExportPlayStoreReportTypes.GET_WRM_EXPORT_PLAYSTORE_REPORT_LIST,
      params
    }),
    saveWRMExportPlayStoreReportlistResponse: (data) => ({
      type: WRMExportPlayStoreReportTypes.SAVE_WRM_EXPORT_PLAYSTORE_REPORT_LIST_RESPONSE,
      data
    }),
    clearWRMExportPlayStoreReportlist: () => ({
      type: WRMExportPlayStoreReportTypes.CLEAR_WRM_EXPORT_PLAYSTORE_REPORT_LIST
    })
  }

  export const WRMExportAppStoreReportTypes = {
    GET_WRM_EXPORT_APPSTORE_REPORT_LIST: 'GET_WRM_EXPORT_APPSTORE_REPORT_LIST',
    SAVE_WRM_EXPORT_APPSTORE_REPORT_LIST_RESPONSE: 'SAVE_WRM_EXPORT_APPSTORE_REPORT_LIST_RESPONSE',
    CLEAR_WRM_EXPORT_APPSTORE_REPORT_LIST: 'CLEAR_WRM_EXPORT_APPSTORE_REPORT_LIST'
  }
  
  export const WRMExportAppStoreReportActions = {
    getWRMExportAppStoreReportlist: (params) => ({
      type: WRMExportAppStoreReportTypes.GET_WRM_EXPORT_APPSTORE_REPORT_LIST,
      params
    }),
    saveWRMExportAppStoreReportlistResponse: (data) => ({
      type: WRMExportAppStoreReportTypes.SAVE_WRM_EXPORT_APPSTORE_REPORT_LIST_RESPONSE,
      data
    }),
    clearWRMExportAppStoreReportlist: () => ({
      type: WRMExportAppStoreReportTypes.CLEAR_WRM_EXPORT_APPSTORE_REPORT_LIST
    })
  }

  export const WRMRealTimeCategoriesStatusTypes = {
    GET_WRM_REALTIME_CATEGORIES_STATUS_LIST: 'GET_WRM_REALTIME_CATEGORIES_STATUS_LIST',
    SAVE_WRM_REALTIME_CATEGORIES_STATUS_LIST_RESPONSE: 'SAVE_WRM_REALTIME_CATEGORIES_STATUS_LIST_RESPONSE',
    CLEAR_WRM_REALTIME_CATEGORIES_STATUS_LIST: 'CLEAR_WRM_REALTIME_CATEGORIES_STATUS_LIST'
  }
  
  export const WRMRealTimeCategoriesStatusActions = {
    getWRMRealTimeCategoriesStatuslist: (params) => ({
      type: WRMRealTimeCategoriesStatusTypes.GET_WRM_REALTIME_CATEGORIES_STATUS_LIST,
      params
    }),
    saveWRMRealTimeCategoriesStatuslistResponse: (data) => ({
      type: WRMRealTimeCategoriesStatusTypes.SAVE_WRM_REALTIME_CATEGORIES_STATUS_LIST_RESPONSE,
      data
    }),
    clearWRMRealTimeCategoriesStatuslist: () => ({
      type: WRMRealTimeCategoriesStatusTypes.CLEAR_WRM_REALTIME_CATEGORIES_STATUS_LIST
    })
  }

  export const WRMRealtimePMAListTypes = {
    GET_WRM_REALTIME_PMA_LIST: 'GET_WRM_REALTIME_PMA_LIST',
    SAVE_WRM_REALTIME_PMA_LIST_RESPONSE: 'SAVE_WRM_REALTIME_PMA_LIST_RESPONSE',
    CLEAR_WRM_REALTIME_PMA_LIST: 'CLEAR_WRM_REALTIME_PMA_LIST'
  }
  
  export const WRMRealtimePMAListActions = {
    getWRMRealtimePMAlist: (params) => ({
      type: WRMRealtimePMAListTypes.GET_WRM_REALTIME_PMA_LIST,
      params
    }),
    saveWRMRealtimePMAlistResponse: (data) => ({
      type: WRMRealtimePMAListTypes.SAVE_WRM_REALTIME_PMA_LIST_RESPONSE,
      data
    }),
    clearWRMRealtimePMAlist: () => ({
      type: WRMRealtimePMAListTypes.CLEAR_WRM_REALTIME_PMA_LIST
    })
  }

  export const WrmRealtimeGetWebRiskScoreGetByIdTypes = {
    GET_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID: 'GET_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID',
    SAVE_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID_RESPONSE: 'SAVE_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID_RESPONSE',
    CLEAR_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID: 'CLEAR_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID'
  }
  
  export const WrmRealtimeGetWebRiskScoreGetByIdActions = {
    getWrmRealtimeGetWebRiskScoreGetById: (id) => ({
      type: WrmRealtimeGetWebRiskScoreGetByIdTypes.GET_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID,
      id
    }),
    saveWrmRealtimeGetWebRiskScoreGetByIdResponse: (data) => ({
      type: WrmRealtimeGetWebRiskScoreGetByIdTypes.SAVE_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID_RESPONSE,
      data
    }),
    clearWrmRealtimeGetWebRiskScoreGetById: () => ({
      type: WrmRealtimeGetWebRiskScoreGetByIdTypes.CLEAR_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID
    })
  }

  export const WRMRealtimeBlackWhiteListTypes = {
    GET_WRM_REALTIME_BLACK_WHITE_LIST_TYPE: 'GET_WRM_REALTIME_BLACK_WHITE_LIST_TYPE',
    SAVE_WRM_REALTIME_BLACK_WHITE_LIST_TYPE_RESPONSE: 'SAVE_WRM_REALTIME_BLACK_WHITE_LIST_TYPE_RESPONSE',
    CLEAR_WRM_REALTIME_BLACK_WHITE_LIST_TYPE: 'CLEAR_WRM_REALTIME_BLACK_WHITE_LIST_TYPE'
  }
  
  export const WRMRealtimeBlackWhiteListTypesActions = {
    getWRMRealtimeBlackWhiteListTypes: (params) => ({
      type: WRMRealtimeBlackWhiteListTypes.GET_WRM_REALTIME_BLACK_WHITE_LIST_TYPE,
      params
    }),
    saveWRMRealtimeBlackWhiteListTypesResponse: (data) => ({
      type: WRMRealtimeBlackWhiteListTypes.SAVE_WRM_REALTIME_BLACK_WHITE_LIST_TYPE_RESPONSE,
      data
    }),
    clearWRMRealtimeBlackWhiteListTypes: () => ({
      type: WRMRealtimeBlackWhiteListTypes.CLEAR_WRM_REALTIME_BLACK_WHITE_LIST_TYPE
    })
  }

  export const WrmRTStatusType = {
    WRM_RT_STATUS: "WRM_RT_STATUS",
    WRM_RT_STATUS_RESPONSE: "WRM_RT_STATUS_RESPONSE",
    WRM_RT_STATUS_CLEAR: "WRM_RT_STATUS_CLEAR"
  };
  
  export const WrmRTStatusAction = {
    WrmRTStatus: (params) => {
      return {
        type: WrmRTStatusType.WRM_RT_STATUS,
        payload:{params}
      }
    },
    WrmRTStatusResponse: (data) => ({
      type: WrmRTStatusType.WRM_RT_STATUS_RESPONSE,
      data,
    }),
    clearWrmRTStatus: () => ({
      type: WrmRTStatusType.WRM_RT_STATUS_CLEAR,
    })
  }

  export const WRMRealtimeGetClientListTypes = {
    GET_WRM_REALTIME_GET_CLIENT_LIST: 'GET_WRM_REALTIME_GET_CLIENT_LIST',
    SAVE_WRM_REALTIME_GET_CLIENT_LIST_RESPONSE: 'SAVE_WRM_REALTIME_GET_CLIENT_LIST_RESPONSE',
    CLEAR_WRM_REALTIME_GET_CLIENT_LIST: 'CLEAR_WRM_REALTIME_GET_CLIENT_LIST'
  }
  
  export const WRMRealtimeGetClientListActions = {
    getWRMRealtimeGetClientList: (params) => ({
      type: WRMRealtimeGetClientListTypes.GET_WRM_REALTIME_GET_CLIENT_LIST,
      params
    }),
    saveWRMRealtimeGetClientListResponse: (data) => ({
      type: WRMRealtimeGetClientListTypes.SAVE_WRM_REALTIME_GET_CLIENT_LIST_RESPONSE,
      data
    }),
    clearWRMRealtimeGetClientList: () => ({
      type: WRMRealtimeGetClientListTypes.CLEAR_WRM_REALTIME_GET_CLIENT_LIST
    })
  }