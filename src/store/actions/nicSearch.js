export const NicSearchactionsTypes = {
    NIC_SEARCH_POST: 'NIC_SEARCH_POST',
    NIC_SEARCH_RESPONSE: 'NIC_SEARCH_RESPONSE',
    CLEAR_NIC_SEARCH: 'CLEAR_NIC_SEARCH'
}

export const NicSearchactions = {
    nicSearchPost: (params) => ({
        type: NicSearchactionsTypes.NIC_SEARCH_POST,
        params
    }),
    saveNicSearchPostResponse: (data) => ({
        type: NicSearchactionsTypes.NIC_SEARCH_RESPONSE,
        data
    }),
    clearNicSearchPost: () => ({
        type: NicSearchactionsTypes.CLEAR_NIC_SEARCH
    })
}