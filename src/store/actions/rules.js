export const rulesActionTypes = {
  GET_RULES: 'GET_RULES',
  POST_RULES: 'POST_RULES',
  POST_RULES_RESPONSE: 'POST_RULES_RESPONSE',
  POST_CLEAR_RULES: 'POST_CLEAR_RULES',
  SAVE_RULES_RESPONSE: 'SAVE_RULES_RESPONSE',
  CLEAR_RULES: 'CLEAR_RULES',
  DELETE_RULES_RESPONSE: 'DELETE_SUCCESS_RULES_RESPONSE',
  DELETE_RULES: 'DELETE_RULES',
}

export const rulesActions = {
  getRules: (params) => ({
    type: rulesActionTypes.GET_RULES,
    params
  }),
  postRules: (params) => ({
    type: rulesActionTypes.POST_RULES,
    payload: params
  }),
  PostRulesResponse: (data) => ({
    type: rulesActionTypes.POST_RULES_RESPONSE,
    data
  }),
  PostclearRules: data => ({
    type: rulesActionTypes.POST_CLEAR_RULES,
    data
  }),
  deleteRules: (id) => ({
    type: rulesActionTypes.DELETE_RULES,
    id: id
  }),
  DeleteRulesResponse: (data) => ({
    type: rulesActionTypes.DELETE_RULES_RESPONSE,
    data
  }),
  saveRulesResponse: (data) => ({
    type: rulesActionTypes.SAVE_RULES_RESPONSE,
    data
  }),
  clearRules: data => ({
    type: rulesActionTypes.CLEAR_RULES,
    data
  }),
}
export const RulesGetId = {
  GET_RULES_DETAILS: 'GET_RULES_DETAILS',
  RULES_DETAILS_RESPONSE: 'RULES_DETAILS_RESPONSE',
  CLEAR_RULES_DETAILS: 'CLEAR_RULES_DETAILS'
}

export const RulesGetIdActions = {
  getRulesIdDetails: (id) => ({
    type: RulesGetId.GET_RULES_DETAILS,
    id
  }),
  saveRulesIdDetailsResponse: data => ({
    type: RulesGetId.RULES_DETAILS_RESPONSE,
    data
  }),
  clearRulesIdDetails: () => ({
    type: RulesGetId.CLEAR_RULES_DETAILS
  })
}

export const updateRulesActionsTypes = {
  UPDATE_RULES: 'UPDATE_RULES',
  SAVE_UPDATE_RULES_RESPONSE: 'SAVE_UPDATE_RULES_RESPONSE',
  CLEAR_UPDATE_RULES: 'CLEAR_UPDATE_RULES'
}

export const updateRulesActions = {
  updateRules: (id, params) => {
    return {
      type: updateRulesActionsTypes.UPDATE_RULES,
      payload: { id, params }
    }
  },
  saveupdateRulesResponse: data => ({
    type: updateRulesActionsTypes.SAVE_UPDATE_RULES_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearupdateRules: () => ({
    type: updateRulesActionsTypes.CLEAR_UPDATE_RULES
  })
}

