export const backWhiteListActionTypes = {
  GET_BlackWhiteList: 'GET_BlackWhiteList',
  POST_BlackWhiteList: 'POST_BlackWhiteList',
  POST_BlackWhiteList_RESPONSE: 'POST_BlackWhiteList_RESPONSE',
  POST_CLEAR_BlackWhiteList: 'POST_CLEAR_BlackWhiteList',
  SAVE_BlackWhiteList_RESPONSE: 'SAVE_BlackWhiteList_RESPONSE',
  CLEAR_BlackWhiteList: 'CLEAR_BlackWhiteList',
  DELETE_BlackWhiteList_RESPONSE: 'DELETE_SUCCESS_BlackWhiteList_RESPONSE',
  DELETE_BlackWhiteList: 'DELETE_BlackWhiteList',
}

export const backWhiteListActions = {
  getBlackWhiteList: (params) => ({
    type: backWhiteListActionTypes.GET_BlackWhiteList,
    params
  }),
  postBlackWhiteList: (params) => ({
    type: backWhiteListActionTypes.POST_BlackWhiteList,
    payload: params
  }),
  PostBlackWhiteListResponse: (data) => ({
    type: backWhiteListActionTypes.POST_BlackWhiteList_RESPONSE,
    data
  }),
  PostclearBlackWhiteList: data => ({
    type: backWhiteListActionTypes.POST_CLEAR_BlackWhiteList,
    data
  }),
  deleteBlackWhiteList: (id) => ({
    type: backWhiteListActionTypes.DELETE_BlackWhiteList,
    id: id
  }),
  DeleteBlackWhiteListResponse: (data) => ({
    type: backWhiteListActionTypes.DELETE_BlackWhiteList_RESPONSE,
    data
  }),
  saveBlackWhiteListResponse: (data) => ({
    type: backWhiteListActionTypes.SAVE_BlackWhiteList_RESPONSE,
    data
  }),
  clearBlackWhiteList: data => ({
    type: backWhiteListActionTypes.CLEAR_BlackWhiteList,
    data
  }),
}
export const BlackWhiteListGetId = {
  GET_BlackWhiteList_DETAILS: 'GET_BlackWhiteList_DETAILS',
  BlackWhiteList_DETAILS_RESPONSE: 'BlackWhiteList_DETAILS_RESPONSE',
  CLEAR_BlackWhiteList_DETAILS: 'CLEAR_BlackWhiteList_DETAILS'
}

export const BlackWhiteListGetIdActions = {
  getBlackWhiteListIdDetails: (id) => ({
    type: BlackWhiteListGetId.GET_BlackWhiteList_DETAILS,
    id
  }),
  saveBlackWhiteListIdDetailsResponse: data => ({
    type: BlackWhiteListGetId.BlackWhiteList_DETAILS_RESPONSE,
    data
  }),
  clearBlackWhiteListIdDetails: () => ({
    type: BlackWhiteListGetId.CLEAR_BlackWhiteList_DETAILS
  })
}

export const updateBlackWhiteListActionsTypes = {
  UPDATE_BlackWhiteList: 'UPDATE_BlackWhiteList',
  SAVE_UPDATE_BlackWhiteList_RESPONSE: 'SAVE_UPDATE_BlackWhiteList_RESPONSE',
  CLEAR_UPDATE_BlackWhiteList: 'CLEAR_UPDATE_BlackWhiteList'
}

export const updateBlackWhiteListActions = {
  updateBlackWhiteList: (id, params) => {
    return {
      type: updateBlackWhiteListActionsTypes.UPDATE_BlackWhiteList,
      payload: { id, params }
    }
  },
  saveupdateBlackWhiteListResponse: data => ({
    type: updateBlackWhiteListActionsTypes.SAVE_UPDATE_BlackWhiteList_RESPONSE,
    data,
    status: data.status,
    message: data.message
  }),
  clearupdateBlackWhiteList: () => ({
    type: updateBlackWhiteListActionsTypes.CLEAR_UPDATE_BlackWhiteList
  })
}

