export const WrmRealtimeSummaryTypes = {
    WRM_REALTIME_SUMMARYLIST: "WRM_REALTIME_SUMMARYLIST",
    WRM_REALTIME_SUMMARYLIST_SUCCESS: "WRM_REALTIME_SUMMARYLIST_SUCCESS",
    WRM_REALTIME_SUMMARYLIST_CLEAR: "WRM_REALTIME_SUMMARYLIST_CLEAR",
    WRM_REALTIME_SUMMARYLIST_ERROR: "WRM_REALTIME_SUMMARYLIST_ERROR"
}

export const WrmRealtimeSummaryAction = {
    WrmRealtimeSummary: (params) => ({
        type: WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST,
        params,
    }),
    WrmRealtimeSummary_SUCCESS: (data) => ({
        type: WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST_SUCCESS,
        data,
    }),
    WrmRealtimeSummary_CLEAR: () => ({
        type: WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST_CLEAR,
    }),
    WrmRealtimeSummary_ERROR: () => ({
        type: WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST_ERROR,
    })
}

export const WrmRealtimeSummaryExportTypes = {
    WRM_REALTIME_SUMMARY_EXPORT: "WRM_REALTIME_SUMMARY_EXPORT",
    WRM_REALTIME_SUMMARY_EXPORT_SUCCESS: "WRM_REALTIME_SUMMARY_EXPORT_SUCCESS",
    WRM_REALTIME_SUMMARY_EXPORT_CLEAR: "WRM_REALTIME_SUMMARY_EXPORT_CLEAR",
    WRM_REALTIME_SUMMARY_EXPORT_ERROR: "WRM_REALTIME_SUMMARY_EXPORT_ERROR"
}

export const WrmRealtimeSummaryExportAction = {
    WrmRealtimeSummaryExport: (params) => ({
        type: WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT,
        params,
    }),
    WrmRealtimeSummaryExportSuccess: (data) => ({
        type: WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT_SUCCESS,
        data,
    }),
    WrmRealtimeSummaryExportClear: () => ({
        type: WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT_CLEAR,
    }),
    WrmRealtimeSummaryExportError: () => ({
        type: WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT_ERROR,
    })
}

export const WrmRealtimeActionsTypes = {
    GET_WRM_REALTIME_LIST: 'GET_WRM_REALTIME_LIST',
    SAVE_WRM_REALTIME_LIST_RESPONSE: 'SAVE_WRM_REALTIME_LIST_RESPONSE',
    CLEAR_WRM_REALTIME_LIST: 'CLEAR_WRM_REALTIME_LIST'
}

export const WrmRealtimeActions = {
    getWrmRealtime: (params) => ({
        type: WrmRealtimeActionsTypes.GET_WRM_REALTIME_LIST,
        params
    }),
    saveWrmRealtimeResponse: (data) => ({
        type: WrmRealtimeActionsTypes.SAVE_WRM_REALTIME_LIST_RESPONSE,
        data
    }),
    clearWrmRealtime: () => ({
        type: WrmRealtimeActionsTypes.CLEAR_WRM_REALTIME_LIST
    })
}

export const WrmrtBulkTypes = {
    POST_WRMRTBULK_LIST: 'POST_WRMRTBULK_LIST',
    SAVE_WRMRTBULK_LIST_RESPONSE: 'SAVE_WRMRTBULK_LIST_RESPONSE',
    CLEAR_WRMRTBULK_LIST: 'CLEAR_WRMRTBULK_LIST'
}

export const WrmrtBulkActions = {
    getWrmrtBulk: (data) => ({
        type: WrmrtBulkTypes.POST_WRMRTBULK_LIST,
        payload: data
    }),
    saveWrmrtBulkResponse: (data) => ({
        type: WrmrtBulkTypes.SAVE_WRMRTBULK_LIST_RESPONSE,
        data
    }),
    clearWrmrtBulk: () => ({
        type: WrmrtBulkTypes.CLEAR_WRMRTBULK_LIST
    })
}

export const WrmrtIndividualTypes = {
    POST_WRMRT_INDIVIDUAL_LIST: 'POST_WRMRT_INDIVIDUAL_LIST',
    SAVE_WRMRT_INDIVIDUAL_LIST_RESPONSE: 'SAVE_WRMRT_INDIVIDUAL_LIST_RESPONSE',
    CLEAR_WRMRT_INDIVIDUAL_LIST: 'CLEAR_WRMRT_INDIVIDUAL_LIST'
}

export const WrmrtIndividualActions = {
    postWrmrtIndividual: (data) => ({
        type: WrmrtIndividualTypes.POST_WRMRT_INDIVIDUAL_LIST,
        payload: data
    }),
    saveWrmrtIndividualResponse: (data) => ({
        type: WrmrtIndividualTypes.SAVE_WRMRT_INDIVIDUAL_LIST_RESPONSE,
        data
    }),
    clearWrmrtIndividual: () => ({
        type: WrmrtIndividualTypes.CLEAR_WRMRT_INDIVIDUAL_LIST
    })
}

export const wrmrtStatusTypes = {
    GET_WRMRT_STATUS: 'GET_WRMRT_STATUS',
    SAVE_WRMRT_STATUS_RESPONSE: 'SAVE_WRMRT_STATUS_RESPONSE',
    CLEAR_WRMRT_STATUS: 'CLEAR_WRMRT_STATUS'
}

export const wrmrtStatusActions = {
    getWrmrtStatus: (params) => ({
        type: wrmrtStatusTypes.GET_WRMRT_STATUS,
        params
    }),
    saveWrmrtStatusResponse: (data) => ({
        type: wrmrtStatusTypes.SAVE_WRMRT_STATUS_RESPONSE,
        data
    }),
    clearWrmrtStatus: () => ({
        type: wrmrtStatusTypes.CLEAR_WRMRT_STATUS
    })
}

export const WRMRTCommentTypes = {
    GET_WRMRT_COMMENT_LIST: 'GET_WRMRT_COMMENT_LIST',
    SAVE_WRMRT_COMMENT_LIST_RESPONSE: 'SAVE_WRMRT_COMMENT_LIST_RESPONSE',
    CLEAR_WRMRT_COMMENT_LIST: 'CLEAR_WRMRT_COMMENT_LIST'
}

export const WRMRTCommentActions = {
    getWRMRTCommentlist: (params) => ({
        type: WRMRTCommentTypes.GET_WRMRT_COMMENT_LIST,
        params
    }),
    saveWRMRTlistResponse: (data) => ({
        type: WRMRTCommentTypes.SAVE_WRMRT_COMMENT_LIST_RESPONSE,
        data
    }),
    clearWRMRTCommentlist: () => ({
        type: WRMRTCommentTypes.CLEAR_WRMRT_COMMENT_LIST
    })
}

export const dashboardRTGetDetailsPdfTypes = {
    GET_DASHBOARDRT_DETAILSPDF: 'GET_DASHBOARDRT_DETAILSPDF',
    DASHBOARDRT_DETAILSPDF_RESPONSE: 'DASHBOARDRT_DETAILSPDF_RESPONSE',
    CLEAR_DASHBOARDRT_DETAILSPDF: 'CLEAR_DASHBOARDRT_DETAILSPDF'
}

export const dashboardRTDetailsPdfActions = {
    getdashboardRTDetailsPdf: (id) => ({
        type: dashboardRTGetDetailsPdfTypes.GET_DASHBOARDRT_DETAILSPDF,
        id
    }),
    savedashboardRTDetailsPdfResponse: data => ({
        type: dashboardRTGetDetailsPdfTypes.DASHBOARDRT_DETAILSPDF_RESPONSE,
        data
    }),
    cleardashboardRTDetailsPdf: () => ({
        type: dashboardRTGetDetailsPdfTypes.CLEAR_DASHBOARDRT_DETAILSPDF
    })
}

export const PlayStoreDashboardRTTypes = {
    GET_PLAYSTORE_DASHBOARDRT: 'GET_PLAYSTORE_DASHBOARDRT',
    SAVE_PLAYSTORE_DASHBOARDRT_RESPONSE: 'SAVE_PLAYSTORE_DASHBOARDRT_RESPONSE',
    CLEAR_PLAYSTORE_DASHBOARDRT: 'CLEAR_PLAYSTORE_DASHBOARDRT'
}

export const PlayStoreDashboardRTActions = {
    getPlayStoreDashboardRT: (params) => ({
        type: PlayStoreDashboardRTTypes.GET_PLAYSTORE_DASHBOARDRT,
        params
    }),
    savePlayStoreDashboardRTResponse: (data) => ({
        type: PlayStoreDashboardRTTypes.SAVE_PLAYSTORE_DASHBOARDRT_RESPONSE,
        data
    }),
    clearPlayStoreDashboardRT: () => ({
        type: PlayStoreDashboardRTTypes.CLEAR_PLAYSTORE_DASHBOARDRT
    })
}

export const PostCategoryRTTypes = {
    POST_CATEGORYRT: 'POST_CATEGORYRT',
    SAVE_CATEGORYRT_RESPONSE: 'SAV_CATEGORYRT_RESPONSE',
    CLEAR_CATEGORYRT: 'CLEAR_CATEGORYRT_SUMMARY'
}

export const PostCategoryRTActions = {
    PostCategoryRT: (params) => ({
        type: PostCategoryRTTypes.POST_CATEGORYRT,
        params
    }),
    PostCategoryRTResponse: data => ({
        type: PostCategoryRTTypes.SAVE_CATEGORYRT_RESPONSE,
        data
    }),
    ClearPostCategoryRT: () => ({
        type: PostCategoryRTTypes.CLEAR_CATEGORYRT
    })
}

export const PostPmaRTTypes = {
    POST_PMART: 'POST_PMART',
    SAVE_PMART_RESPONSE: 'SAVE_PMART_RESPONSE',
    CLEAR_PMART: 'CLEAR_PMART'
}

export const PostPmaRTActions = {
    PostPmaRT: (params) => ({
        type: PostPmaRTTypes.POST_PMART,
        params
    }),
    PostPmaRTResponse: data => ({
        type: PostPmaRTTypes.SAVE_PMART_RESPONSE,
        data
    }),
    ClearPostPmaRT: () => ({
        type: PostPmaRTTypes.CLEAR_PMART
    })
}

export const PostProposedCategoryRTTypes = {
    POST_PROPOSEDCATEGORYRT: 'POST_PROPOSEDCATEGORYRT',
    SAVE_PROPOSEDCATEGORYRT_RESPONSE: 'SAVE_PROPOSEDCATEGORYRT_RESPONSE',
    CLEAR_PROPOSEDCATEGORYRT: 'CLEAR_PROPOSEDCATEGORYRT'
}

export const PostProposedCategoryRTActions = {
    PostProposedCategoryRT: (params) => ({
        type: PostProposedCategoryRTTypes.POST_PROPOSEDCATEGORYRT,
        params
    }),
    PostProposedCategoryRTResponse: data => ({
        type: PostProposedCategoryRTTypes.SAVE_PROPOSEDCATEGORYRT_RESPONSE,
        data
    }),
    ClearPostProposedCategoryRT: () => ({
        type: PostProposedCategoryRTTypes.CLEAR_PROPOSEDCATEGORYRT
    })
}

export const PriceRTTypes = {
    FETCH_PRICERT_POPUP_INIT: 'FETCH_PRICERT_POPUP_INIT',
    FETCH_PRICERT_POPUP_SUCCESS: 'FETCH_PRICERT_POPUP_SUCCESS',
    FETCH_PRICERT_POPUP_ERROR: 'FETCH_PRICERT_POPUP_ERROR',

    PRICERT_POPUP_INIT: 'PRICERT_POPUP_INIT',
    PRICERT_POPUP_SUCCESS: 'PRICERT_POPUP_SUCCESS',
    PRICERT_POPUP_ERROR: 'PRICERT_POPUP_ERROR',
    PRICERT_POPUP_CLEAR: 'PRICERT_POPUP_CLEAR'
}
export const PriceRTActions = {

    fetchPopupInit: (id) => ({
        type: PriceRTTypes.FETCH_PRICERT_POPUP_INIT,
        id
    }),
    fetchPopupSuccess: (data) => ({
        type: PriceRTTypes.FETCH_PRICERT_POPUP_SUCCESS,
        data
    }),
    fetchPopupError: () => ({
        type: PriceRTTypes.FETCH_PRICERT_POPUP_ERROR
    }),

    postPopupInit: (id, formData) => ({
        type: PriceRTTypes.PRICERT_POPUP_INIT,
        id,
        formData
    }),
    postPopupSuccess: (data) => ({
        type: PriceRTTypes.PRICERT_POPUP_SUCCESS,
        data
    }),
    postPopupError: () => ({
        type: PriceRTTypes.PRICERT_POPUP_ERROR
    }),
    postPopupClear: () => ({
        type: PriceRTTypes.PRICERT_POPUP_CLEAR
    })
}

export const PostBlockCategoryRTTypes = {
    POST_PROPOSEDCATEGORYRT: 'POST_PROPOSEDCATEGORYRT',
    SAVE_PROPOSEDCATEGORYRT_RESPONSE: 'SAVE_PROPOSEDCATEGORYRT_RESPONSE',
    CLEAR_PROPOSEDCATEGORYRT: 'CLEAR_PROPOSEDCATEGORYRT'
}

export const PostBlockCategoryRTActions = {
    PostProposedCategoryRT: (params) => ({
        type: PostProposedCategoryRTTypes.POST_PROPOSEDCATEGORYRT,
        params
    }),
    PostProposedCategoryRTResponse: data => ({
        type: PostProposedCategoryRTTypes.SAVE_PROPOSEDCATEGORYRT_RESPONSE,
        data
    }),
    ClearPostProposedCategoryRT: () => ({
        type: PostProposedCategoryRTTypes.CLEAR_PROPOSEDCATEGORYRT
    })
}