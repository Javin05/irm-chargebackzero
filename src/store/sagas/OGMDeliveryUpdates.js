import { put, takeLatest, all } from "redux-saga/effects"
import axiosInstance from "../../services"
import { API_MESSAGES } from '../../utils/constants'
import {
  OGMDeliveryUpdateActions,
  getOGMDeliveryUpdateTypes,
  addOGMDeliveryUpdateActions,
  addOGMDeliveryUpdateTypes,
  OGMDeliveryUpdateIdAction,
  OGMDeliveryUpdateIdTypes,
  updateOGMDeliveryUpdateActions,
  updateOGMDeliveryUpdateActionsTypes,
  OGMDeliveryUpdatedeleteActions,
  OGMDeliveryUpdatedeleteActionsTypes
} from "../actions"
import serviceList from "../../services/serviceList"

function* fetchgetOGMDeliveryUpdatelist(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.get(serviceList.ogmDeliveryUpdate, { params })
        if (json.status === 200) {
            yield put(OGMDeliveryUpdateActions.saveOGMDeliveryUpdatelistResponse(json.data))
        } else {
            yield put(OGMDeliveryUpdateActions.saveOGMDeliveryUpdatelistResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: getOGMDeliveryUpdateTypes.SAVE_OGM_DELIVERY_UPDATES_LIST_RESPONSE, data })
    }
}

function* addOGMDeliveryUpdate(action) {
    const { payload } = action
    try {
      const json = yield axiosInstance.post(serviceList.ogmDeliveryUpdate, payload)
      if (json.status === 200) {
        yield put(addOGMDeliveryUpdateActions.saveaddOGMDeliveryUpdateResponse(json.data))
      } else {
        yield put(addOGMDeliveryUpdateActions.saveaddOGMDeliveryUpdateResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: addOGMDeliveryUpdateTypes.RESPONSE, data })
    }
  }

function* OGMDeliveryUpdateDetails(actions) {
    const { id } = actions
    const endPointUrl = `${serviceList.ogmDeliveryUpdate}/${id}`
    try {
      const json = yield axiosInstance.get(endPointUrl)
      if (json.status === 200) {
        yield put(OGMDeliveryUpdateIdAction.saveOGMDeliveryUpdateDetailResponse(json.data))
      } else {
        yield put(OGMDeliveryUpdateIdAction.saveOGMDeliveryUpdateDetailResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS_RESPONSE, data })
    }
  }

function* fetchOGMDeliveryUpdateEditUpdate(actions) {
    const { payload } = actions
    const { id, params } = payload && payload
    const endPointUrl = `${serviceList.ogmDeliveryUpdate}/${id}`
    try {
      const json = yield axiosInstance.put(endPointUrl, params)
      if (json.status === 200) {
        yield put(updateOGMDeliveryUpdateActions.saveupdateOGMDeliveryUpdateResponse(json.data))
      } else {
        yield put(updateOGMDeliveryUpdateActions.saveupdateOGMDeliveryUpdateResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: updateOGMDeliveryUpdateActionsTypes.SAVE_UPDATE_OGM_DELIVERY_UPDATES_RESPONSE, data })
    }
  }

function* deleteOGMDeliveryUpdate(actions) {
    const { params } = actions
    const deleteUrl = `${serviceList.ogmDeliveryUpdate}/${params.id}`
    try {
      const json = yield axiosInstance.delete(deleteUrl)
      if (json.status === 200) {
        yield put(OGMDeliveryUpdatedeleteActions.saveResponse(json.data))
      } else {
        yield put(OGMDeliveryUpdatedeleteActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: OGMDeliveryUpdatedeleteActionsTypes.RESPONSE, data })
    }
  }

export function* fetchOGMDeliveryUpdateWatcher() {
  yield all([
    yield takeLatest(getOGMDeliveryUpdateTypes.GET_OGM_DELIVERY_UPDATES_LIST, fetchgetOGMDeliveryUpdatelist),
    yield takeLatest(addOGMDeliveryUpdateTypes.REQUEST, addOGMDeliveryUpdate),
    yield takeLatest(OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS, OGMDeliveryUpdateDetails),
    yield takeLatest(updateOGMDeliveryUpdateActionsTypes.UPDATE_OGM_DELIVERY_UPDATES_SUMMARY, fetchOGMDeliveryUpdateEditUpdate),
    yield takeLatest(OGMDeliveryUpdatedeleteActionsTypes.REQUEST, deleteOGMDeliveryUpdate),
  ])
}