import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
    AMLqueueTypes, AMLqueueActions, AMLPostActions, AMLPostTypes, ApproveAMLActions, ApproveAMLActionsTypes, AMLGetIdTypes, AMLGetIdActions,
    TransactionAMLActions, TransactionAMLActionsTypes, AMLCountActions, AMLCountActionsTypes, AMLPercentageActions, AMLPercentageActionsTypes,
    AMLWMYActions, AMLWMYActionsTypes, AMLAnnatationActions, AMLAnnatationActionsTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchAMLget(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.AML, { params })
  if (json.status === 200) {
    yield put(AMLqueueActions.saveAMLqueuelistResponse(json.data))
  } else {
    yield put(AMLqueueActions.saveAMLqueuelistResponse([]))
  }
}

function* fetchAMLpost(actions) {
    const { payload } = actions
    try {
      const data = yield axiosInstance.post(serviceList.AmlPost, payload)
      if (data && data.data) {
        yield put(AMLPostActions.saveAMLPostResponse(data.data))
      }
    } catch (error) {
      yield put(AMLPostActions.saveAMLPostResponse(error))
    }
}

function * fetchApproveAML (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.AMLApprove}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(ApproveAMLActions.saveApproveAMLResponse(json.data))
  } else {
    yield put(ApproveAMLActions.saveApproveAMLResponse([]))
  }
}

function * fetchGetIdAML (actions) {
  const { id, params } = actions
  const endPointUrl = `${serviceList.suspectAccounts}/${id}`
  const json = yield axiosInstance.get(endPointUrl, params)
  if (json.status === 200) {
    yield put(AMLGetIdActions.saveAMLIdDetailsResponse(json.data))
  } else {
    yield put(AMLGetIdActions.saveAMLIdDetailsResponse([]))
  }
}

function* fetchTransactionAML (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.AMLDispute}/${id}`
  const json = yield axiosInstance.post(endPointUrl, params)
  if (json.status === 200) {
    yield put(TransactionAMLActions.saveTransactionAMLResponse(json.data))
  } else {
    yield put(TransactionAMLActions.saveTransactionAMLResponse([]))
  }
}

function* fetchCountAML (actions) {
  const { params } = actions
  try {
    const data = yield axiosInstance.post(serviceList.AMLApprove, params)
    if (data && data.data) {
      yield put(AMLCountActions.saveCountAMLResponse(data.data))
    }
  } catch (error) {
    yield put(AMLCountActions.saveCountAMLResponse(error))
  }
}

function* fetchPercentageAML (actions) {
  const { id, params } = actions
  const endPointUrl = `${serviceList.percentageAML}/${id}`
  const json = yield axiosInstance.get(endPointUrl, params)
    if (json.status === 200) {
      yield put(AMLPercentageActions.savePercentageAMLResponse(json.data))
    }else {
      yield put(AMLPercentageActions.savePercentageAMLResponse([]))
    }
}

function* fetchWMYAML (actions) {
  const { id, params } = actions
  const endPointUrl = `${serviceList.AmlMetricsData}/${id}`
  const json = yield axiosInstance.get(endPointUrl, params)
    if (json.status === 200) {
      yield put(AMLWMYActions.saveWMYAMLResponse(json.data))
    }else {
      yield put(AMLWMYActions.saveWMYAMLResponse([]))
    }
}

function* fetchAnnatationAML (actions) {
  const { id, params } = actions
  const endPointUrl = `${serviceList.AmlAnnatation}/${id}`
  const json = yield axiosInstance.get(endPointUrl, params)
    if (json.status === 200) {
      yield put(AMLAnnatationActions.saveAnnatationAMLResponse(json.data))
    }else {
      yield put(AMLAnnatationActions.saveAnnatationAMLResponse([]))
    }
}

export function* fetchAMLWatcher() {
  yield all([
    yield takeLatest(AMLGetIdTypes.GET_AML_DETAILS, fetchGetIdAML),
    yield takeLatest(ApproveAMLActionsTypes.APPROVE_AML, fetchApproveAML),
    yield takeLatest(AMLqueueTypes.GET_AML_LIST, fetchAMLget),
    yield takeLatest(AMLPostTypes.AMLPOST_LIST, fetchAMLpost),
    yield takeLatest(TransactionAMLActionsTypes.TRANSACTION_AML, fetchTransactionAML),
    yield takeLatest(AMLCountActionsTypes.COUNT_AML, fetchCountAML),
    yield takeLatest(AMLPercentageActionsTypes.PERCENTAGE_AML, fetchPercentageAML),
    yield takeLatest(AMLWMYActionsTypes.WMY_AML, fetchWMYAML),
    yield takeLatest(AMLAnnatationActionsTypes.ANNATATION_AML, fetchAnnatationAML)
  ])
}