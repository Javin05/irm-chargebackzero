import { put, takeLatest, all } from 'redux-saga/effects'
import { API_MESSAGES } from '../../utils/constants'
import axiosInstance from '../../services'
import {
  clientActions, clientActionsTypes,
  userRoleActions, usersRoleTypes,
  industryActions, industryTypes,
  RiskweightageActions,
  RiskweightageTypes,
  RiskweightagePostTypes,
  RiskweightagePostActions,
  RiskweightageEditActions,
  RiskweightageEditTypes,
  RiskweightageUpdateActions,
  RiskweightageUpdateTypes,
  RiskweightageDeleteActions,
  RiskweightageDeleteTypes,
  RiskyDomainActions,
  RiskyDomainTypes,
  RiskyDomainPostActions,
  RiskyDomainPostTypes,
  RiskyDomainEditActions,
  RiskyDomainEditTypes,
  RiskyDomainUpdateTypes,
  RiskyDomainUpdateActions,
  RiskyDomainDeleteTypes,
  RiskyDomainDeleteActions,
  GroupScoreWeightAgeTypes,
  GroupScoreWeightAgeActions,
  GroupScoreWeightAgePostTypes,
  GroupScoreWeightAgePostActions,
  GroupScoreWeightAgeEditTypes,
  GroupScoreWeightAgeEditActions,
  GroupScoreWeightAgeUpdateTypes,
  GroupScoreWeightAgeUpdateActions,
  GroupScoreWeightAgeDeleteActions,
  GroupScoreWeightAgeDeleteTypes,
  RiskScoreWeightAgeTypes,
  RiskScoreWeightAgeActions,
  RiskScoreWeightAgePostTypes,
  RiskScoreWeightAgePostActions,
  RiskScoreWeightAgeEditTypes,
  RiskScoreWeightAgeEditActions,
  RiskScoreWeightAgeUpdateTypes,
  RiskScoreWeightAgeUpdateActions,
  RiskScoreWeightAgeDeleteActions,
  RiskScoreWeightAgeDeleteTypes,  
  GroupIdDrpdnTypes,
  GroupIdDrpdnActions,
  ReportDrpdnTypes,
  ReportDrpdnActions,
  ExportWRMEditActions,
  ExportWRMEditTypes,
  ExportWRMActions,
  ExportWRMTypes,
  ExportPlaystoreEditActions,
  ExportPlaystoreActions,
  ExportAppstoreActions,
  ExportAppstoreTypes,
  ExportPlaystoreEditTypes,
  ExportPlaystoreTypes,
  CloneScoreTypes,
  CloneScoreActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchClient(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.getClients, { params })
    if (json.status === 200) {
      yield put(clientActions.saveclientResponse(json.data))
    } else {
      yield put(clientActions.saveclientResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: clientActionsTypes.SAVE_CLIENT_RESPONSE, data })
  }
}

function* fetchUserRole(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.userRole, { params })
    if (json.status === 200) {
      yield put(userRoleActions.saveUserRoleResponse(json.data))
    } else {
      yield put(userRoleActions.saveUserRoleResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: usersRoleTypes.RESPONSE, data })
  }
}

function* fetchIndustry(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.Industry, { params })
    if (json.status === 200) {
      yield put(industryActions.saveIndustryResponse(json.data))
    } else {
      yield put(industryActions.saveIndustryResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: industryTypes.RESPONSE, data })
  }
}

function* fetchRiskweightage(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.get(`${module==="WRM" ? serviceList.Riskweightage: serviceList.WrmRTRiskweightage}`, { params })
    if (json.status === 200) {
      yield put(RiskweightageActions.saveRiskweightageResponse(json.data))
    } else {
      yield put(RiskweightageActions.saveRiskweightageResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageTypes.RESPONSE, data })
  }
}

function* fetchRiskweightagePost(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.post(`${module==="WRM" ? serviceList.Riskweightage: serviceList.WrmRTRiskweightage}`, params)
    if (json.status === 200) {
      yield put(RiskweightagePostActions.saveRiskweightagePostResponse(json.data))
    } else {
      yield put(RiskweightagePostActions.saveRiskweightagePostResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightagePostTypes.RESPONSE, data })
  }
}

function* fetchRiskweightageEdit(actions) {
  const { params, module } = actions
  const id = `${module==="WRM" ? serviceList.Riskweightage: serviceList.WrmRTRiskweightage}/${params}`
  try {
    const json = yield axiosInstance.get(id)
    if (json.status === 200) {
      yield put(RiskweightageEditActions.saveRiskweightageEditResponse(json.data))
    } else {
      yield put(RiskweightageEditActions.saveRiskweightageEditResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageEditTypes.RESPONSE, data })
  }
}

function* fetchRiskweightageUpdate(actions) {
  const { payload, module } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${module==="WRM" ? serviceList.Riskweightage: serviceList.WrmRTRiskweightage}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(RiskweightageUpdateActions.saveRiskweightageUpdateResponse(json.data))
    } else {
      yield put(RiskweightageUpdateActions.saveRiskweightageUpdateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageUpdateTypes.RESPONSE, data })
  }
}

function* fetchRiskweightageDelete(actions) {
  const { params, module } = actions
  const endPointUrl = `${module==="WRM" ? serviceList.Riskweightage: serviceList.WrmRTRiskweightage}/${params}`
  try {
    const json = yield axiosInstance.delete(endPointUrl)
    if (json.status === 200) {
      yield put(RiskweightageDeleteActions.saveRiskweightageDeleteResponse(json.data))
    } else {
      yield put(RiskweightageDeleteActions.saveRiskweightageDeleteResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageDeleteTypes.RESPONSE, data })
  }
}

function* fetchRiskyDomain(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.get(`${module==="WRM" ? serviceList.riskyDomain: serviceList.WrmRTRiskyDomain}` , {params})
    if (json.status === 200) {
      yield put(RiskyDomainActions.saveRiskyDomainResponse(json.data))
    } else {
      yield put(RiskyDomainActions.saveRiskyDomainResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskyDomainTypes.RESPONSE, data })
  }
}

function* fetchRiskyDomainPost(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.post(`${module==="WRM" ? serviceList.riskyDomain: serviceList.WrmRTRiskyDomain}` , params)
    if (json.status === 200) {
      yield put(RiskyDomainPostActions.saveRiskyDomainPostResponse(json.data))
    } else {
      yield put(RiskyDomainPostActions.saveRiskyDomainPostResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskyDomainPostTypes.RESPONSE, data })
  }
}

function* fetchRiskyDomainEdit(actions) {
  const { params, module } = actions
  const id = `${module==="WRM" ? serviceList.riskyDomain: serviceList.WrmRTRiskyDomain}/${params}`
  try {
    const json = yield axiosInstance.get(id)
    if (json.status === 200) {
      yield put(RiskyDomainEditActions.saveRiskyDomainEditResponse(json.data))
    } else {
      yield put(RiskyDomainEditActions.saveRiskyDomainEditResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskyDomainEditTypes.RESPONSE, data })
  }
}

function* fetchRiskyDomainUpdate(actions) {
  const { payload, module } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${module==="WRM" ? serviceList.riskyDomain: serviceList.WrmRTRiskyDomain}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(RiskyDomainUpdateActions.saveRiskyDomainUpdateResponse(json.data))
    } else {
      yield put(RiskyDomainUpdateActions.saveRiskyDomainUpdateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskyDomainUpdateTypes.RESPONSE, data })
  }
}

function* fetchRiskyDomainDelete(actions) {
  const { params, module } = actions
  const endPointUrl = `${module==="WRM" ? serviceList.riskyDomain: serviceList.WrmRTRiskyDomain}/${params}`
  try {
    const json = yield axiosInstance.delete(endPointUrl)
    if (json.status === 200) {
      yield put(RiskyDomainDeleteActions.saveRiskyDomainDeleteResponse(json.data))
    } else {
      yield put(RiskyDomainDeleteActions.saveRiskyDomainDeleteResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskyDomainDeleteTypes.RESPONSE, data })
  }
}

function* fetchGroupScoreWeightAge(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.get(`${module==="WRM" ?serviceList.GroupScoreWeightAge: serviceList.WrmRTRiskScoreWeightAge}` , {params})
    if (json.status === 200) {
      yield put(GroupScoreWeightAgeActions.saveGroupScoreWeightAgeResponse(json.data))
    } else {
      yield put(GroupScoreWeightAgeActions.saveGroupScoreWeightAgeResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: GroupScoreWeightAgeTypes.RESPONSE, data })
  }
}

function* fetchGroupScoreWeightAgePost(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.post(`${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}` , params)
    if (json.status === 200) {
      yield put(GroupScoreWeightAgePostActions.saveGroupScoreWeightAgePostResponse(json.data))
    } else {
      yield put(GroupScoreWeightAgePostActions.saveGroupScoreWeightAgePostResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: GroupScoreWeightAgePostTypes.RESPONSE, data })
  }
}

function* fetchGroupScoreWeightAgeEdit(actions) {
  const { params, module } = actions
  const id = `${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}/${params}`
  try {
    const json = yield axiosInstance.get(id)
    if (json.status === 200) {
      yield put(GroupScoreWeightAgeEditActions.saveGroupScoreWeightAgeEditResponse(json.data))
    } else {
      yield put(GroupScoreWeightAgeEditActions.saveGroupScoreWeightAgeEditResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: GroupScoreWeightAgeEditTypes.RESPONSE, data })
  }
}

function* fetchGroupScoreWeightAgeUpdate(actions) {
  const { payload, module } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(GroupScoreWeightAgeUpdateActions.saveGroupScoreWeightAgeUpdateResponse(json.data))
    } else {
      yield put(GroupScoreWeightAgeUpdateActions.saveGroupScoreWeightAgeUpdateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: GroupScoreWeightAgeUpdateTypes.RESPONSE, data })
  }
}

function* fetchGroupScoreWeightAgeDelete(actions) {
  const { params, module } = actions
  const endPointUrl = `${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}/${params}`
  try {
    const json = yield axiosInstance.delete(endPointUrl)
    if (json.status === 200) {
      yield put(GroupScoreWeightAgeDeleteActions.saveGroupScoreWeightAgeDeleteResponse(json.data))
    } else {
      yield put(GroupScoreWeightAgeDeleteActions.saveGroupScoreWeightAgeDeleteResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: GroupScoreWeightAgeDeleteTypes.RESPONSE, data })
  }
}

function* fetchRiskScoreWeightAge(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.get(`${module === "WRM" ? serviceList.RiskScoreWeightAge: serviceList.WrmRTGroupScoreWeightAge}` , {params})
    if (json.status === 200) {
      yield put(RiskScoreWeightAgeActions.saveRiskScoreWeightAgeResponse(json.data))
    } else {
      yield put(RiskScoreWeightAgeActions.saveRiskScoreWeightAgeResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskScoreWeightAgeTypes.RESPONSE, data })
  }
}

function* fetchRiskScoreWeightAgePost(actions) {
  const { params, module } = actions
  try {
    const json = yield axiosInstance.post(`${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}` , params)
    if (json.status === 200) {
      yield put(RiskScoreWeightAgePostActions.saveRiskScoreWeightAgePostResponse(json.data))
    } else {
      yield put(RiskScoreWeightAgePostActions.saveRiskScoreWeightAgePostResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskScoreWeightAgePostTypes.RESPONSE, data })
  }
}

function* fetchRiskScoreWeightAgeEdit(actions) {
  const { params, module } = actions
  const id = `${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}/${params}`
  try {
    const json = yield axiosInstance.get(id)
    if (json.status === 200) {
      yield put(RiskScoreWeightAgeEditActions.saveRiskScoreWeightAgeEditResponse(json.data))
    } else {
      yield put(RiskScoreWeightAgeEditActions.saveRiskScoreWeightAgeEditResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskScoreWeightAgeEditTypes.RESPONSE, data })
  }
}

function* fetchRiskScoreWeightAgeUpdate(actions) {
  const { payload, module } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(RiskScoreWeightAgeUpdateActions.saveRiskScoreWeightAgeUpdateResponse(json.data))
    } else {
      yield put(RiskScoreWeightAgeUpdateActions.saveRiskScoreWeightAgeUpdateResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskScoreWeightAgeUpdateTypes.RESPONSE, data })
  }
}

function* fetchRiskScoreWeightAgeDelete(actions) {
  const { params, module } = actions
  const endPointUrl = `${module==="WRM" ?serviceList.WeightAge: serviceList.WrmRTWeightAge}/${params}`
  try {
    const json = yield axiosInstance.delete(endPointUrl)
    if (json.status === 200) {
      yield put(RiskScoreWeightAgeDeleteActions.saveRiskScoreWeightAgeDeleteResponse(json.data))
    } else {
      yield put(RiskScoreWeightAgeDeleteActions.saveRiskScoreWeightAgeDeleteResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskScoreWeightAgeDeleteTypes.RESPONSE, data })
  }
}

function* fetchGroupIdDrpdn(actions) {
  const { params,module } = actions
  try {
    const json = yield axiosInstance.get(`${module==="WRM" ?serviceList.GroupScoreWeightAge: serviceList.WrmRTRiskScoreWeightAge}` , {params})
    if (json.status === 200) {
      yield put(GroupIdDrpdnActions.saveGroupIdDrpdnResponse(json.data))
    } else {
      yield put(GroupIdDrpdnActions.saveGroupIdDrpdnResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: GroupIdDrpdnTypes.RESPONSE, data })
  }
}

function* fetchReportDrpdn(actions) {
  const { params } = actions
  try {
    const json = yield axiosInstance.get(serviceList.ReportDrpdn , {params})
    if (json.status === 200) {
      yield put(ReportDrpdnActions.saveReportDrpdnResponse(json.data))
    } else {
      yield put(ReportDrpdnActions.saveReportDrpdnResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: ReportDrpdnTypes.RESPONSE, data })
  }
}
function* fetchExportWRMEdit(action) {
 let {id,data} = action
 console.log(id,data)
  try {
    const json = yield axiosInstance.put(`${serviceList.exportWRMEdit}/${id}`,data)
    if (json.status === 200) {
      yield put(ExportWRMEditActions.saveExportWRMEditResponse(json.data))
    } else {
      yield put(ExportWRMEditActions.saveExportWRMEditResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageEditTypes.RESPONSE, data })
  }
}

function* fetchExportWRM(action) {
 let {id,query} = action
  try {
    const json = yield axiosInstance.get(`${serviceList.exportWRMList}?${query}=${id}`)
    if (json.status === 200) {
      yield put(ExportWRMActions.saveExportWRMResponse(json.data))
    } else {
      yield put(ExportWRMActions.saveExportWRMResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageEditTypes.RESPONSE, data })
  }
}

function* fetchExportPlaystoreEdit(action) {
 let {id,data} = action
  try {
    const json = yield axiosInstance.put(`${serviceList.exportPlaystoreEdit}/${id}`,data)
    if (json.status === 200) {
      yield put(ExportPlaystoreEditActions.saveExportPlaystoreEditResponse(json.data))
    } else {
      yield put(ExportPlaystoreEditActions.saveExportPlaystoreEditResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageEditTypes.RESPONSE, data })
  }
}

function* fetchExportPlaystore(action) {
 let {id,query} = action
  try {
    const json = yield axiosInstance.get(`${serviceList.exportPlaystoreList}?${query}=${id}`)
    if (json.status === 200) {
      yield put(ExportPlaystoreActions.saveExportPlaystoreResponse(json.data))
    } else {
      yield put(ExportPlaystoreActions.saveExportPlaystoreResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: RiskweightageEditTypes.RESPONSE, data })
  }
}

function* fetchExportAppstore(action) {
  let {id,query} = action
   try {
     const json = yield axiosInstance.get(`${serviceList.exportAppstoreList}?${query}=${id}`)
     if (json.status === 200) {
       yield put(ExportAppstoreActions.saveExportAppstoreResponse(json.data))
     } else {
       yield put(ExportAppstoreActions.saveExportAppstoreResponse([]))
     }
   }
   catch (error) {
     const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
     yield put({ type: RiskweightageEditTypes.RESPONSE, data })
   }
 }


function* fetchCloneScore(action) {
  let {params} = action
   try {
    const json = yield axiosInstance.post(serviceList.cloneScopeRisk, params)
     if (json.status === 200) {
       yield put(CloneScoreActions.saveCloneScoreResponse(json.data))
     } else {
       yield put(CloneScoreActions.saveCloneScoreResponse([]))
     }
   }
   catch (error) {
     const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
     yield put({ type: CloneScoreTypes.RESPONSE, data })
   }
 }

export function* fetchClientWatcher() {
  yield all([
    yield takeLatest(clientActionsTypes.GET_CLIENT, fetchClient),
    yield takeLatest(usersRoleTypes.REQUEST, fetchUserRole),
    yield takeLatest(industryTypes.REQUEST, fetchIndustry),
    yield takeLatest(RiskweightageTypes.REQUEST, fetchRiskweightage),
    yield takeLatest(RiskweightagePostTypes.REQUEST, fetchRiskweightagePost),
    yield takeLatest(RiskweightageEditTypes.REQUEST, fetchRiskweightageEdit),
    yield takeLatest(RiskweightageUpdateTypes.REQUEST, fetchRiskweightageUpdate),
    yield takeLatest(RiskweightageDeleteTypes.REQUEST, fetchRiskweightageDelete),
    yield takeLatest(RiskyDomainTypes.REQUEST, fetchRiskyDomain),
    yield takeLatest(RiskyDomainPostTypes.REQUEST, fetchRiskyDomainPost),
    yield takeLatest(RiskyDomainEditTypes.REQUEST, fetchRiskyDomainEdit),
    yield takeLatest(RiskyDomainUpdateTypes.REQUEST, fetchRiskyDomainUpdate),
    yield takeLatest(RiskyDomainDeleteTypes.REQUEST, fetchRiskyDomainDelete),
    yield takeLatest(GroupScoreWeightAgeTypes.REQUEST, fetchGroupScoreWeightAge),
    yield takeLatest(GroupScoreWeightAgePostTypes.REQUEST, fetchGroupScoreWeightAgePost),
    yield takeLatest(GroupScoreWeightAgeEditTypes.REQUEST, fetchGroupScoreWeightAgeEdit),
    yield takeLatest(GroupScoreWeightAgeUpdateTypes.REQUEST, fetchGroupScoreWeightAgeUpdate),
    yield takeLatest(GroupScoreWeightAgeDeleteTypes.REQUEST, fetchGroupScoreWeightAgeDelete),
    yield takeLatest(RiskScoreWeightAgeTypes.REQUEST, fetchRiskScoreWeightAge),
    yield takeLatest(RiskScoreWeightAgePostTypes.REQUEST, fetchRiskScoreWeightAgePost),
    yield takeLatest(RiskScoreWeightAgeEditTypes.REQUEST, fetchRiskScoreWeightAgeEdit),
    yield takeLatest(RiskScoreWeightAgeUpdateTypes.REQUEST, fetchRiskScoreWeightAgeUpdate),
    yield takeLatest(RiskScoreWeightAgeDeleteTypes.REQUEST, fetchRiskScoreWeightAgeDelete),
    yield takeLatest(GroupIdDrpdnTypes.REQUEST, fetchGroupIdDrpdn),
    yield takeLatest(ReportDrpdnTypes.REQUEST, fetchReportDrpdn),
    yield takeLatest(ExportWRMEditTypes.REQUEST,fetchExportWRMEdit),
    yield takeLatest(ExportWRMTypes.REQUEST, fetchExportWRM),
    yield takeLatest(ExportPlaystoreEditTypes.REQUEST,fetchExportPlaystoreEdit),
    yield takeLatest(ExportPlaystoreTypes.REQUEST, fetchExportPlaystore),
    yield takeLatest(ExportAppstoreTypes.REQUEST, fetchExportAppstore),
    yield takeLatest(CloneScoreTypes.REQUEST, fetchCloneScore) 
  ])
}