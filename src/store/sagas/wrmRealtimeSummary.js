import { put, takeLatest, all } from "redux-saga/effects"
import axiosInstance from "../../services"
import {
    WrmRealtimeSummaryTypes,
    WrmRealtimeSummaryAction,
    WrmRealtimeSummaryExportAction,
    WrmRealtimeSummaryExportTypes,
    WrmRealtimeActionsTypes,
    WrmRealtimeActions,
    WrmrtBulkTypes,
    WrmrtBulkActions,
    WrmrtIndividualTypes,
    WrmrtIndividualActions,
    wrmrtStatusActions,
    wrmrtStatusTypes,
    WRMRTCommentTypes,
    WRMRTCommentActions,
    dashboardRTGetDetailsPdfTypes,
    dashboardRTDetailsPdfActions,
    PlayStoreDashboardRTActions,
    PlayStoreDashboardRTTypes,
    PostCategoryRTTypes,
    PostCategoryRTActions,
    PostPmaRTTypes,
    PostPmaRTActions,
    PostProposedCategoryRTTypes,
    PostProposedCategoryRTActions,
    PriceRTActions,
    PriceRTTypes,
    PostBlockCategoryRTTypes,
    PostBlockCategoryRTActions
} from "../actions"
import serviceList from "../../services/serviceList"
import { API_MESSAGES } from "../../utils/constants"

function* fetchWrmRealtimeSummaryList(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmRealtimeSummary, { params })
    if (json.status === 200) {
        yield put(WrmRealtimeSummaryAction.WrmRealtimeSummary_SUCCESS(json.data))
    } else {
        yield put(WrmRealtimeSummaryAction.WrmRealtimeSummary_SUCCESS([]))
    }
}

function* fetchWrmRealtimeSummaryExport(actions) {
    const { params } = actions
    const endPointUrl = `${serviceList.wrmRealtimeSummaryExport}`
    const json = yield axiosInstance.get(endPointUrl, { params })
    if (json.status === 200) {
        yield put(WrmRealtimeSummaryExportAction.WrmRealtimeSummaryExportSuccess(json.data))
    } else {
        yield put(WrmRealtimeSummaryExportAction.WrmRealtimeSummaryExportSuccess([]))
    }
}

function* fetchWrmRealtime(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.WrmRealtime, { params })
    if (json.status === 200) {
        yield put(WrmRealtimeActions.saveWrmRealtimeResponse(json.data))
    } else {
        yield put(WrmRealtimeActions.saveWrmRealtimeResponse([]))
    }
}

function* fetchWrmrtBulk(action) {
    const { payload } = action
    try {
        const data = yield axiosInstance.post(serviceList.WrmrtBulk, payload)
        if (data && data.data) {
            yield put(WrmrtBulkActions.saveWrmrtBulkResponse(data.data))
        }
    } catch (error) {
        yield put(WrmrtBulkActions.saveWrmrtBulkResponse(error))
    }
}

function* fetchWrmrtIndividual(action) {
    const { payload } = action
    try {
        const data = yield axiosInstance.post(serviceList.WrmrtIndividual, payload)
        if (data && data.data) {
            yield put(WrmrtIndividualActions.saveWrmrtIndividualResponse(data.data))
        }
    } catch (error) {
        yield put(WrmrtIndividualActions.saveWrmrtIndividualResponse(error))
    }
}

function* fetchWrmrtStatus(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.WrmrtStatus, { params })
    if (json.status === 200) {
        yield put(wrmrtStatusActions.saveWrmrtStatusResponse(json.data))
    } else {
        yield put(wrmrtStatusActions.saveWrmrtStatusResponse([]))
    }
}

function* fetchWRMRTCommentlist(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.get(serviceList.WRMRTComments, { params })
        if (json.status === 200) {
            yield put(WRMRTCommentActions.saveWRMRTlistResponse(json.data))
        } else {
            yield put(WRMRTCommentActions.saveWRMRTlistResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: WRMRTCommentTypes.SAVE_WRMRT_COMMENT_LIST_RESPONSE, data })
    }
}

function* getdashboardRTPdf(actions) {
    const { id } = actions && actions.id
    const endPointUrl = `${serviceList.risktimeList}/${id}?type=pdf`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
        yield put(dashboardRTDetailsPdfActions.savedashboardRTDetailsPdfResponse(json.data))
    } else {
        yield put(dashboardRTDetailsPdfActions.savedashboardRTDetailsPdfResponse([]))
    }
}

function* fetchPlayStoreDashboardRT(actions) {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.pdfRTStoreExport, { params })
    if (json.status === 200) {
        yield put(PlayStoreDashboardRTActions.savePlayStoreDashboardRTResponse(json.data))
    } else {
        yield put(PlayStoreDashboardRTActions.savePlayStoreDashboardRTResponse([]))
    }
}

function* fetchCategoryRT(actions) {
    const params = actions.params
    const endPointUrl = `${serviceList.riskCategoryRtStatus}`
    const json = yield axiosInstance.post(endPointUrl, params)
    if (json.status === 200) {
        yield put(PostCategoryRTActions.PostCategoryRTResponse(json.data))
    } else {
        yield put(PostCategoryRTActions.PostCategoryRTResponse([]))
    }
}

function* fetchPmaRT(actions) {
    const params = actions.params
    const endPointUrl = `${serviceList.PMARTupdated}`
    const json = yield axiosInstance.post(endPointUrl, params)
    if (json.status === 200) {
        yield put(PostPmaRTActions.PostPmaRTResponse(json.data))
    } else {
        yield put(PostPmaRTActions.PostPmaRTResponse([]))
    }
}

function* fetchProposedCategoryRT(actions) {
    const params = actions.params
    const endPointUrl = `${serviceList.proposedCategoryRTupdated}`
    const json = yield axiosInstance.post(endPointUrl, params)
    if (json.status === 200) {
        yield put(PostProposedCategoryRTActions.PostProposedCategoryRTResponse(json.data))
    } else {
        yield put(PostProposedCategoryRTActions.PostProposedCategoryRTResponse([]))
    }
}

function * fetchPriceRT (actions) {
    const { id } = actions
    const endPointUrl = `${serviceList.fetchPriceRTCheck}/${id}`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
        yield put(PriceRTActions.fetchPopupSuccess(json.data))
    } else {
        yield put(PriceRTActions.fetchPopupError([]))
    }
}

function * createPriceRT (actions) {
    const { id, formData } = actions
    const endPointUrl = `${serviceList.postPriceRTCheck}/${id}`
    const json = yield axiosInstance.post(endPointUrl, formData)
    if (json.status === 200) {
        yield put(PriceRTActions.postPopupSuccess(json.data))
    } else {
        yield put(PriceRTActions.postPopupError([]))
    }
}

function* fetchBlockCategoryRT(actions) {
    const params = actions.params
    const endPointUrl = `${serviceList.riskCategoryRTStatus}`
    const json = yield axiosInstance.post(endPointUrl, params)
    if (json.status === 200) {
        yield put(PostBlockCategoryRTActions.PostProposedCategoryRTResponse(json.data))
    } else {
        yield put(PostBlockCategoryRTActions.PostProposedCategoryRTResponse([]))
    }
}

export function* fetchWrmRealtimeSummaryWatcher() {
    yield all([
        yield takeLatest(WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST, fetchWrmRealtimeSummaryList),
        yield takeLatest(WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT, fetchWrmRealtimeSummaryExport),
        yield takeLatest(WrmRealtimeActionsTypes.GET_WRM_REALTIME_LIST, fetchWrmRealtime),
        yield takeLatest(WrmrtBulkTypes.POST_WRMRTBULK_LIST, fetchWrmrtBulk),
        yield takeLatest(WrmrtIndividualTypes.POST_WRMRT_INDIVIDUAL_LIST, fetchWrmrtIndividual),
        yield takeLatest(wrmrtStatusTypes.GET_WRMRT_STATUS, fetchWrmrtStatus),
        yield takeLatest(WRMRTCommentTypes.GET_WRMRT_COMMENT_LIST, fetchWRMRTCommentlist),
        yield takeLatest(dashboardRTGetDetailsPdfTypes.GET_DASHBOARDRT_DETAILSPDF, getdashboardRTPdf),
        yield takeLatest(PlayStoreDashboardRTTypes.GET_PLAYSTORE_DASHBOARDRT, fetchPlayStoreDashboardRT),
        yield takeLatest(PostCategoryRTTypes.POST_CATEGORYRT, fetchCategoryRT),
        yield takeLatest(PostPmaRTTypes.POST_PMART, fetchPmaRT),
        yield takeLatest(PostProposedCategoryRTTypes.POST_PROPOSEDCATEGORYRT, fetchProposedCategoryRT),
        yield takeLatest(PriceRTTypes.FETCH_PRICERT_POPUP_INIT, fetchPriceRT),
        yield takeLatest(PriceRTTypes.PRICERT_POPUP_INIT, createPriceRT),
        yield takeLatest(PostBlockCategoryRTTypes.POST_PROPOSEDCATEGORYRT, fetchBlockCategoryRT),
    ])
}