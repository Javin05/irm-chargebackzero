import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  dashboardGetDetailsTypes,
  dashboardDetailsActions,
  dashboardFilterTypes,
  dashboardFilterActions,
  dashboardTransactionTypes,
  dashboardTransactionActions,
  getWebsiteIdActions,
  getWebsiteIdTypes,
  dashboardDetailsPdfActions,
  dashboardGetDetailsPdfTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function * getdashboard (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.riskManagementList}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(dashboardDetailsActions.savedashboardDetailsResponse(json.data))
  } else {
    yield put(dashboardDetailsActions.savedashboardDetailsResponse([]))
  }
}

function * getdashboardPdf (actions) {
    const { id } = actions && actions.id
    const endPointUrl = `${serviceList.riskManagementList}/${id}?type=pdf`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
      yield put(dashboardDetailsPdfActions.savedashboardDetailsPdfResponse(json.data))
    } else {
      yield put(dashboardDetailsPdfActions.savedashboardDetailsPdfResponse([]))
    }
  }

function * dashboardFilter (actions) {
    const { params } = actions
    const endPointUrl = `${serviceList.DashboardFilter}/${params}`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
        yield put(dashboardFilterActions.dashboardFilterSuccess(json.data))
    } else {
        yield put(dashboardFilterActions.dashboardFilterError([]))
    }
}
function * dashboardTransactions (actions) {
    const { params } = actions
    const endPointUrl = `${serviceList.dashboardTransactionStatus}/${params}`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
        yield put(dashboardTransactionActions.getWebsiteIdSuccess(json.data))
    } else {
        yield put(dashboardTransactionActions.dashboardTransactionError([]))
    }
}

function * getWebsiteId (actions) {
    const { params } = actions
    const endPointUrl = `${serviceList.getWebsiteId}/${params}`
    const json = yield axiosInstance.get(endPointUrl)
    if (json.status === 200) {
        yield put(getWebsiteIdActions.getWebsiteIdSuccess(json.data))
    } else {
        yield put(getWebsiteIdActions.getWebsiteIdSuccess([]))
    }
}

export function * fetchdashboardWatcher () {
    yield all([
        yield takeLatest(dashboardGetDetailsTypes.GET_DASHBOARD_DETAILS, getdashboard),
        yield takeLatest(dashboardGetDetailsPdfTypes.GET_DASHBOARD_DETAILSPDF, getdashboardPdf),
        yield takeLatest(dashboardFilterTypes.DASHBOARD_FILTER_INIT, dashboardFilter),
        yield takeLatest(dashboardTransactionTypes.DASHBOARD_TRANSACTION_INIT, dashboardTransactions),
        yield takeLatest(getWebsiteIdTypes.GET_WEBSITEID, getWebsiteId),
    ])
}