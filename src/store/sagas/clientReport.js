import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  ClientWebActions,
  ClientWebActionsTypes,
  PostClientWebActions,
  PostClientWebActionsTypes,
  ClientPlayStoreActionsTypes,
  ClientPlayStoreActions,
  PostClientPlaStoreActionsTypes,
  PostClientPlayStoreActions,
  exportClientWebActionsTypes,
  exportClientWebActions,
  exportClientPlaStoreActionsTypes,
  exportClientPlayStoreActions,
  exportClientAppStoreActionsTypes,
  exportClientAppStoreActions,
  ClientAppStoreActions,
  ClientAppStoreActionsTypes,
  PostClientAppStoreActionsTypes,
  PostClientAppStoreActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchClientWebReport(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.clientWeb}/${params}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(ClientWebActions.saveClientWeblistResponse(json.data))
  } else {
    yield put(ClientWebActions.saveClientWeblistResponse([]))
  }
}

function* fetchPostClientWebReport(actions) {
  const { payload } = actions
  const {params, id} = payload && payload
  const endPointUrl = `${serviceList.postClientWeb}/${id}`
  const json = yield axiosInstance.post(endPointUrl, params)  
  if (json.status === 200) {
    yield put(PostClientWebActions.postsaveClientWeblistResponse(json.data))
  } else {
    yield put(PostClientWebActions.postsaveClientWeblistResponse([]))
  }
}

function* fetchClientPlayStoreReport(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.clientplayStore}/${params}`
  const json = yield axiosInstance.get(endPointUrl) 
  if (json.status === 200) {
    yield put(ClientPlayStoreActions.saveClientPlayStorelistResponse(json.data))
  } else {
    yield put(ClientPlayStoreActions.saveClientPlayStorelistResponse([]))
  }
}

function* fetchClientAppStoreReport(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.clientappStore}/${params}`
  const json = yield axiosInstance.get(endPointUrl) 
  if (json.status === 200) {
    yield put(ClientAppStoreActions.saveClientAppStorelistResponse(json.data))
  } else {
    yield put(ClientAppStoreActions.saveClientAppStorelistResponse([]))
  }
}

function* fetchPostClientPlayStoreReport(actions) {
  const { payload } = actions
  const {params, id} = payload && payload
  const endPointUrl = `${serviceList.postClientPlay}/${id}`
  const json = yield axiosInstance.post(endPointUrl, params)  
  if (json.status === 200) {
    yield put(PostClientPlayStoreActions.postsaveClientPlayStorlistResponse(json.data))
  } else {
    yield put(PostClientPlayStoreActions.postsaveClientPlayStorlistResponse([]))
  }
}

function* fetchPostClientAppStoreReport(actions) {
  const { payload } = actions
  const {params, id} = payload && payload
  const endPointUrl = `${serviceList.postClientApp}/${id}`
  const json = yield axiosInstance.post(endPointUrl, params)  
  if (json.status === 200) {
    yield put(PostClientAppStoreActions.postsaveClientAppStorlistResponse(json.data))
  } else {
    yield put(PostClientAppStoreActions.postsaveClientAppStorlistResponse([]))
  }
}

function* fetchexportClientwebReport(actions) {
  const { id, params } = actions && actions.payload
  const endPointUrl = `${serviceList.exportclientReport}/${id}`
  const json = yield axiosInstance.get(endPointUrl, { params})  
  if (json.status === 200) {
    yield put(exportClientWebActions.exportsaveClientWeblistResponse(json.data))
  } else {
    yield put(exportClientWebActions.exportsaveClientWeblistResponse([]))
  }
}

function* fetchexportClientPlayStoreReport(actions) {
  const { id, params } = actions && actions.payload
  const endPointUrl = `${serviceList.exportPlayStoreReport}/${id}`
  const json = yield axiosInstance.get(endPointUrl, { params})  
  if (json.status === 200) {
    yield put(exportClientPlayStoreActions.exportsaveClientPlayStorlistResponse(json.data))
  } else {
    yield put(exportClientPlayStoreActions.exportsaveClientPlayStorlistResponse([]))
  }
}

function* fetchexportClientAppStoreReport(actions) {
  const { id, params } = actions && actions.payload
  const endPointUrl = `${serviceList.exportAppStoreReport}/${id}`
  const json = yield axiosInstance.get(endPointUrl, { params})  
  if (json.status === 200) {
    yield put(exportClientAppStoreActions.exportsaveClientAppStorlistResponse(json.data))
  } else {
    yield put(exportClientAppStoreActions.exportsaveClientAppStorlistResponse([]))
  }
}


export function* fetchClientWebReportWatcher() {
  yield all([
    yield takeLatest(ClientWebActionsTypes.GET_CLIENTWEB_LIST, fetchClientWebReport),
    yield takeLatest(PostClientWebActionsTypes.POST_CLIENTWEB_LIST, fetchPostClientWebReport),
    yield takeLatest(ClientPlayStoreActionsTypes.GET_CLIENTPLAYSTORE_LIST, fetchClientPlayStoreReport),
    yield takeLatest(ClientAppStoreActionsTypes.GET_CLIENTAPPSTORE_LIST, fetchClientAppStoreReport),
    yield takeLatest(PostClientPlaStoreActionsTypes.POST_CLIENT_PLAYSTORE_LIST, fetchPostClientPlayStoreReport),
    yield takeLatest(PostClientAppStoreActionsTypes.POST_CLIENT_APPSTORE_LIST, fetchPostClientAppStoreReport),
    yield takeLatest(exportClientWebActionsTypes.EXPORT_CLIENTWEB_LIST, fetchexportClientwebReport),
    yield takeLatest(exportClientPlaStoreActionsTypes.EXPORT_CLIENT_PLAYSTORE_LIST, fetchexportClientPlayStoreReport),
    yield takeLatest(exportClientAppStoreActionsTypes.EXPORT_CLIENT_APPSTORE_LIST, fetchexportClientAppStoreReport),
  ])
}