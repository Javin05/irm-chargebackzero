import { all, put, takeLatest } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import {
  BlackWhiteListGetId, BlackWhiteListGetIdActions,
  backWhiteListActionTypes,
  backWhiteListActions,
  updateBlackWhiteListActions,
  updateBlackWhiteListActionsTypes
} from '../actions'

function* fetchBlackWhiteListlist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.balckWhiteListGroup, { params })
  if (json.status === 200) {
    yield put(backWhiteListActions.saveBlackWhiteListResponse(json.data))
  } else {
    yield put(backWhiteListActions.saveBlackWhiteListResponse([]))
  }
}

function* postBlackWhiteListList(actions) {
  const { payload } = actions
  try {
    const data = yield axiosInstance.post(serviceList.balckWhiteListGroup, payload)
    if (data && data.data) {
      yield put(backWhiteListActions.PostBlackWhiteListResponse(data.data))
    }
  } catch (error) {
    yield put(backWhiteListActions.PostBlackWhiteListResponse(error))
  }
}

function* deleteBlackWhiteListList(actions) {
  const { id } = actions
  console.log(id, "delete ")
  const endPointUrl = `${serviceList.balckWhiteListGroup}/delete/${id}`
  const json = yield axiosInstance.delete(endPointUrl)
  if (json.status === 200) {
    yield put(backWhiteListActions.DeleteBlackWhiteListResponse(json.data))
  } else {
    yield put(backWhiteListActions.DeleteBlackWhiteListResponse([]))
  }
}

function* getEditBlackWhiteList(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.balckWhiteListGroup}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(BlackWhiteListGetIdActions.saveBlackWhiteListIdDetailsResponse(json.data))
  } else {
    yield put(BlackWhiteListGetIdActions.saveBlackWhiteListIdDetailsResponse([]))
  }
}

function* fetchupdateBlackWhiteList(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.balckWhiteListGroup}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateBlackWhiteListActions.saveupdateBlackWhiteListResponse(json.data))
  } else {
    yield put(updateBlackWhiteListActions.saveupdateBlackWhiteListResponse([]))
  }
}


export function* fetchBlackWhiteListWatcher() {
  yield all([
    yield takeLatest(backWhiteListActionTypes.GET_BlackWhiteList, fetchBlackWhiteListlist),
    yield takeLatest(backWhiteListActionTypes.POST_BlackWhiteList, postBlackWhiteListList),
    yield takeLatest(backWhiteListActionTypes.DELETE_BlackWhiteList, deleteBlackWhiteListList),
    yield takeLatest(BlackWhiteListGetId.GET_BlackWhiteList_DETAILS, getEditBlackWhiteList),
    yield takeLatest(updateBlackWhiteListActionsTypes.UPDATE_BlackWhiteList, fetchupdateBlackWhiteList),
  ])
}