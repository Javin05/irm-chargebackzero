import { put, takeLatest, all } from "redux-saga/effects";
import axiosInstance from "../../services";
import { API_MESSAGES } from '../../utils/constants'
import { 
    KYCCommentTypes,
    KYCCommentActions,
    updateKYCCommentActionsTypes,
    updateKYCCommentActions,
    KYCCommentdeleteActionsTypes,
    KYCCommentdeleteActions,
    KYCDocumentdeleteActionsTypes,
    KYCDocumentdeleteActions,
    KYCDocumentListActions,
    KYCDocumentListActionsTypes,
    KycUpdateLogsActions,
    KycUpdateLogsActionsTypes,
    KycDocumentUpdateHistoryActionsTypes,
    KycDocumentUpdateHistoryActions
  } from '../actions'
import serviceList from "../../services/serviceList"

  function* fetchKYCCommentlist(actions) {
    try {
      const { params } = actions
      const json = yield axiosInstance.get(serviceList.kycComments, { params })
      if (json.status === 200) {
        yield put(KYCCommentActions.saveKYCCommentlistResponse(json.data))
      } else {
        yield put(KYCCommentActions.saveKYCCommentlistResponse([]))
      }
    } catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: KYCCommentTypes.SAVE_KYC_COMMENT_LIST_RESPONSE, data })
    }
  }

  function* fetchKYCCommentUpdate(actions) {
    const { payload } = actions
    const { id, params } = payload && payload
    const endPointUrl = `${serviceList.kycCommentsUpdate}/${id}`
    try {
      const json = yield axiosInstance.put(endPointUrl, params)
      if (json.status === 200) {
        yield put(updateKYCCommentActions.saveupdateKYCCommentResponse(json.data))
      } else {
        yield put(updateKYCCommentActions.saveupdateKYCCommentResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: updateKYCCommentActionsTypes.SAVE_UPDATE_KYC_COMMENT_RESPONSE, data })
    }
  }

  function* deleteKYCComment(actions) {
    const { params } = actions
    const deleteUrl = `${serviceList.kycCommentsDelete}/${params} `
    try {
      const json = yield axiosInstance.delete(deleteUrl)
      if (json.status === 200) {
        yield put(KYCCommentdeleteActions.saveResponse(json.data))
      } else {
        yield put(KYCCommentdeleteActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: KYCCommentdeleteActionsTypes.RESPONSE, data })
    }
  }

  function* deleteKYCDocument(actions) {
    const { params } = actions
    const deleteUrl = `${serviceList.kycDocumentDelete}/${params} `
    try {
      const json = yield axiosInstance.delete(deleteUrl)
      if (json.status === 200) {
        yield put(KYCDocumentdeleteActions.saveResponse(json.data))
      } else {
        yield put(KYCDocumentdeleteActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: KYCDocumentdeleteActionsTypes.RESPONSE, data })
    }
  }

  function* KYCDocumentList(actions) {
    const { params } = actions
    const getUrl = `${serviceList.kycDocumentList}`
    try {
      const json = yield axiosInstance.get(getUrl, {params})
      if (json.status === 200) {
        yield put(KYCDocumentListActions.saveResponse(json.data))
      } else {
        yield put(KYCDocumentListActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: KYCDocumentListActionsTypes.RESPONSE, data })
    }
  }
  
  function* KYCUpdateLogs(actions) {
    const { params } = actions
    const getUrl = `${serviceList.kycLogs}`
    try {
      const json = yield axiosInstance.get(getUrl, {params})
      if (json.status === 200) {
        yield put(KycUpdateLogsActions.saveResponse(json.data))
      } else {
        yield put(KycUpdateLogsActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: KycUpdateLogsActionsTypes.RESPONSE, data })
    }
  }

  
  function* KYCDocumentUpdateHistory(actions) {
    const { params } = actions
    const getUrl = `${serviceList.kycUpdateLogs}/${params}`
    try {
      const json = yield axiosInstance.get(getUrl)
      if (json.status === 200) {
        yield put(KycDocumentUpdateHistoryActions.saveResponse(json.data))
      } else {
        yield put(KycDocumentUpdateHistoryActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: KycDocumentUpdateHistoryActionsTypes.RESPONSE, data })
    }
  }

  export function* fetchKYCComment() {
    yield all([
      yield takeLatest(KYCCommentTypes.GET_KYC_COMMENT_LIST, fetchKYCCommentlist),
      yield takeLatest(updateKYCCommentActionsTypes.UPDATE_KYC_COMMENT, fetchKYCCommentUpdate),
      yield takeLatest(KYCCommentdeleteActionsTypes.REQUEST, deleteKYCComment),
      yield takeLatest(KYCDocumentdeleteActionsTypes.REQUEST, deleteKYCDocument),
      yield takeLatest(KYCDocumentListActionsTypes.REQUEST, KYCDocumentList),
      yield takeLatest(KycUpdateLogsActionsTypes.REQUEST, KYCUpdateLogs),
      yield takeLatest(KycDocumentUpdateHistoryActionsTypes.REQUEST, KYCDocumentUpdateHistory)
    ])
  }