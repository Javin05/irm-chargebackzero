import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  EddRiskManagementActionsTypes,
  EddRiskManagementActions,
  AddEddRiskManagementActions,
  AddEddRiskManagementTypes,
  eddMerchantIdDetailsActions,
  eddMerchantIdDetailsTypes,
  BusinessTypeActionsTypes,
  BusinessTypeActions,
  BusinessAgeActionsTypes,
  BusinessAgeActions,
  BusinessNdxActionsTypes,
  BusinessNdxActions,
  PaymentInstrumentActionsTypes,
  PaymentInstrumentActions,
  EddExportActionsTypes,
  EddExportActions,
  EddDashboardActionsTypes,
  EddDashboardActions,
  EddApiDetailsActionsTypes,
  EddApiDetailsActions,
  EddRiskManagementApiFinalListTypes,
  EddRiskManagementApiFinalListActions,
  FinalDecisionUpdateActionsTypes,
  FinalDecisionUpdateActions,
  eddApiScheduledListTypes,
  eddApiScheduledListActions,
  eddApiScheduledListReportTypes,
  eddApiScheduledListReportActions,
  ReTriggerFinalApiUpdateActionsTypes,
  ReTriggerFinalApiUpdateActions,
  EDDUpdateActions,
  EDDUpdateActionsTypes,
  eddHistoryListActions,
  eddHistoryListActionTypes
} from '../actions'
import serviceList from '../../services/serviceList'
import { API_MESSAGES } from '../../utils/constants'

function* fetchEddRiskManagement(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.eddRiskManagement, { params })
  if (json.status === 200) {
    yield put(EddRiskManagementActions.saveEddRiskMangemntResponse(json.data))
  } else {
    yield put(EddRiskManagementActions.saveEddRiskMangemntResponse([]))
  }
}

function* fetchAddEddRiskManagement(actions) {
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.AddEddRiskManagement, params)
  if (json.status === 200) {
    yield put(AddEddRiskManagementActions.saveUpdateWebReportResponse(json.data))
  } else {
    yield put(AddEddRiskManagementActions.saveUpdateWebReportResponse([]))
  }
}

function * getEditEddMerchant (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.eddRiskManagement}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(eddMerchantIdDetailsActions.saveEddMerchantIdDetailsResponse(json.data))
  } else {
    yield put(eddMerchantIdDetailsActions.saveEddMerchantIdDetailsResponse([]))
  }
}

function* fetchBusinessType(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.businessType, { params })
  if (json.status === 200) {
    yield put(BusinessTypeActions.saveBusinessTypeResponse(json.data))
  } else {
    yield put(BusinessTypeActions.saveBusinessTypeResponse([]))
  }
}

function* fetchBusinessAge(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.businessAge, { params })
  if (json.status === 200) {
    yield put(BusinessAgeActions.saveBusinessAgeResponse(json.data))
  } else {
    yield put(BusinessAgeActions.saveBusinessAgeResponse([]))
  }
}

function* fetchBusinessNdx(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.businessNdx, { params })
  if (json.status === 200) {
    yield put(BusinessNdxActions.saveBusinessNdxResponse(json.data))
  } else {
    yield put(BusinessNdxActions.saveBusinessNdxResponse([]))
  }
}

function* fetchPaymentInstrument(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.paymentInstrument, { params })
  if (json.status === 200) {
    yield put(PaymentInstrumentActions.savePaymentInstrumentResponse(json.data))
  } else {
    yield put(PaymentInstrumentActions.savePaymentInstrumentResponse([]))
  }
}

function* fetchEddExport(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.eddExport, { params })
  if (json.status === 200) {
    yield put(EddExportActions.saveEddExportResponse(json.data))
  } else {
    yield put(EddExportActions.saveEddExportResponse([]))
  }
}

function* fetchDashboardlist(actions) {
	const { id } = actions
	const endPointUrl = `${serviceList.eddDashboard}/${id}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(EddDashboardActions.saveEddDashboardResponse(json.data))
	} else {
		yield put(EddDashboardActions.saveEddDashboardResponse([]))
	}
}

function* fetchApiDetailslist(actions) {
	const { params } = actions
	const endPointUrl = `${serviceList.eddApiList}`
	const json = yield axiosInstance.get(endPointUrl)
	if (json.status === 200) {
		yield put(EddApiDetailsActions.saveEddApiDetailsResponse(json.data))
	} else {
		yield put(EddApiDetailsActions.saveEddApiDetailsResponse([]))
	}
}

function* fetchEddApiFinalList(actions) {
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.eddApiFinalList, params)
  if (json.status === 200) {
    yield put(EddRiskManagementApiFinalListActions.saveEddApiFinalListResponse(json.data))
  } else {
    yield put(EddRiskManagementApiFinalListActions.saveEddApiFinalListResponse([]))
  }
}

function* fetchFinalDecisionUpdate(actions) {
  try {
      const { params } = actions
      const json = yield axiosInstance.put(`${serviceList.eddFinalDecision}/${params.id}`, params)
      if (json.status === 200) {
          yield put(FinalDecisionUpdateActions.saveFinalDecisionUpdateResponse(json.data))
      } else {
          yield put(FinalDecisionUpdateActions.saveFinalDecisionUpdateResponse([]))
      }
  } catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FinalDecisionUpdateActionsTypes.SAVE_FINAL_DECISION_UPDATE_RESPONSE, data })
  }
}

function * getEddApiScheduledList (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.eddScheduledApiList}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(eddApiScheduledListActions.saveEddApiScheduledListResponse(json.data))
  } else {
    yield put(eddApiScheduledListActions.saveEddApiScheduledListResponse([]))
  }
}

function * getEddApiScheduledListReport (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.eddScheduledApiListReport}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(eddApiScheduledListReportActions.saveEddApiScheduledListReportResponse(json.data))
  } else {
    yield put(eddApiScheduledListReportActions.saveEddApiScheduledListReportResponse([]))
  }
}

function* fetchReTriggerFinalApiUpdate(actions) {
  try {
      const { params } = actions
      const json = yield axiosInstance.put(`${serviceList.eddReTriggerFinalApiUpdate}/${params.id}`, params)
      if (json.status === 200) {
          yield put(ReTriggerFinalApiUpdateActions.saveReTriggerFinalApiUpdateResponse(json.data))
      } else {
          yield put(ReTriggerFinalApiUpdateActions.saveReTriggerFinalApiUpdateResponse([]))
      }
  } catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: ReTriggerFinalApiUpdateActionsTypes.SAVE_RE_TRIGGER_FINAL_API_UPDATE_RESPONSE, data })
  }
}

function* fetchEDDUpdate(actions) {
  try {
      const { id, params } = actions
      const json = yield axiosInstance.put(`${serviceList.eddUpdate}/${id}`, params)
      if (json.status === 200) {
          yield put(EDDUpdateActions.saveEDDUpdateResponse(json.data))
      } else {
          yield put(EDDUpdateActions.saveEDDUpdateResponse([]))
      }
  } catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: EDDUpdateActionsTypes.SAVE_EDD_UPDATE_RESPONSE, data })
  }
}

function * getEddHistoryList (actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.eddLogHistory}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(eddHistoryListActions.saveEddHistoryListResponse(json.data))
  } else {
    yield put(eddHistoryListActions.saveEddHistoryListResponse([]))
  }
}

export function* fetchEddRiskManagementWatcher() {
  yield all([yield takeLatest(EddRiskManagementActionsTypes.GET_EDDRISKMANAGEMENT_LIST, fetchEddRiskManagement)])
  yield all([yield takeLatest(AddEddRiskManagementTypes.GET_ADD_EDD_RISKMANAGEMENT, fetchAddEddRiskManagement)])
  yield all([yield takeLatest(eddMerchantIdDetailsTypes.GET_EDD_MERCHANT_ID, getEditEddMerchant)])
  yield all([yield takeLatest(BusinessTypeActionsTypes.GET_BUSINESS_TYPE_LIST, fetchBusinessType)])
  yield all([yield takeLatest(BusinessAgeActionsTypes.GET_BUSINESS_AGE_LIST, fetchBusinessAge)])
  yield all([yield takeLatest(BusinessNdxActionsTypes.GET_BUSINESS_NDX_LIST, fetchBusinessNdx)])
  yield all([yield takeLatest(PaymentInstrumentActionsTypes.GET_PAYMENT_INSTRUMENT_LIST, fetchPaymentInstrument)])
  yield all([yield takeLatest(EddExportActionsTypes.GET_EDD_EXPORT_LIST, fetchEddExport)])
  yield all([yield takeLatest(EddDashboardActionsTypes.GET_EDD_DASHBOARD_DETAILS, fetchDashboardlist)])
  yield all([yield takeLatest(EddApiDetailsActionsTypes.GET_API_DETAILS, fetchApiDetailslist)])
  yield all([yield takeLatest(EddRiskManagementApiFinalListTypes.GET_EDD_API_FINAL_LIST, fetchEddApiFinalList)])
  yield all([yield takeLatest(FinalDecisionUpdateActionsTypes.GET_FINAL_DECISION_UPDATE, fetchFinalDecisionUpdate)])
  yield all([yield takeLatest(eddApiScheduledListTypes.GET_EDD_API_SCHEDULED_LIST_ID, getEddApiScheduledList)])
  yield all([yield takeLatest(eddApiScheduledListReportTypes.GET_EDD_API_SCHEDULED_LIST_REPORT, getEddApiScheduledListReport)])
  yield all([yield takeLatest(ReTriggerFinalApiUpdateActionsTypes.GET_RE_TRIGGER_FINAL_API_UPDATE, fetchReTriggerFinalApiUpdate)])
  yield all([yield takeLatest(EDDUpdateActionsTypes.GET_EDD_UPDATE, fetchEDDUpdate)])
  yield all([yield takeLatest(eddHistoryListActionTypes.GET_EDD_HISTORY_LIST, getEddHistoryList)])
}