import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  queuesActions, queuesActionsTypes, queuesTypes,
  queuesAction, queuesGetId, queuesGetIdActions,
  updateQueuesActionsTypes, updateQueuesActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchqueueslist(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.Queues, { params })
  if (json.status === 200) {
    yield put(queuesActions.savequeueslistResponse(json.data))
  } else {
    yield put(queuesActions.savequeueslistResponse([]))
  }
}

function* fetchqueuesLogin(action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.Queues, payload)
    if (data && data.data) {
      yield put(queuesAction.savequeuesResponse(data.data))
    }
  } catch (error) {
    yield put(queuesAction.savequeuesResponse(error))
  }
}

function* getEditQueues(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.Queues}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(queuesGetIdActions.savequeuesIdDetailsResponse(json.data))
  } else {  
    yield put(queuesGetIdActions.savequeuesIdDetailsResponse([]))
  }
}

function * fetchupdateQueue (actions) {
  // const { id, payload } = action
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.Queues}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(updateQueuesActions.saveupdateQueuesResponse(json.data))
  } else {
    yield put(updateQueuesActions.saveupdateQueuesResponse([]))
  }
}

export function* fetchqueueslistWatcher() {
  yield all([
    yield takeLatest(queuesActionsTypes.GET_QUEUES_LIST, fetchqueueslist),
    yield all([yield takeLatest(queuesTypes.QUEUES_POST, fetchqueuesLogin)]),
    yield takeLatest(queuesGetId.GET_QUEUES_DETAILS, getEditQueues),
    yield takeLatest(updateQueuesActionsTypes.UPDATE_QUEUES, fetchupdateQueue)
  ])
}