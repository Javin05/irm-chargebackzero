import { put, takeLatest, all } from 'redux-saga/effects'
import { API_MESSAGES } from '../../utils/constants'
import axiosInstance from '../../services'
import {
    FeedBackActionsTypes,
    FeedBackActions,
    SendFeedBackactionsTypes,
    SendFeedBackactions,
    NotificationFeedBackActions,
    NotificationFeedBackActionsTypes,
    FeedBackViewActionsTypes,
    FeedBackViewActions,
    FeedBackClosectionsTypes,
    FeedBackCloseActions,
    FeedBackCountActionsTypes,
    FeedBackCountActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchFeedBack(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.get(`${serviceList.feedBackList}/${params}`)
        if (json.status === 200) {
            yield put(FeedBackActions.saveFeedBackListResponse(json.data))
        } else {
            yield put(FeedBackActions.saveFeedBackListResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: FeedBackActionsTypes.SAVE_FEEDBACK_LIST_RESPONSE, data })
    }
}

function* FeedBackPost(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.post(serviceList.feedBackPost, params)
        if (json.status === 200) {
            yield put(SendFeedBackactions.saveSendFeedBackPostResponse(json.data))
        } else {
            yield put(SendFeedBackactions.saveSendFeedBackPostResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: SendFeedBackactionsTypes.SEND_FEEDBACK_RESPONSE, data })
    }
}

function* notificationFetchFeedBack(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.get(serviceList.notificationFeedBackList, {params})
        if (json.status === 200) {
            yield put(NotificationFeedBackActions.saveNotificationFeedBackListResponse(json.data))
        } else {
            yield put(NotificationFeedBackActions.saveNotificationFeedBackListResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: NotificationFeedBackActionsTypes.SAVE_NOTIFICATION_FEEDBACK_LIST_RESPONSE, data })
    }
}

function* fetchFeedViewBack(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.put(`${serviceList.feedBackViewList}/${params}`)
        if (json.status === 200) {
            yield put(FeedBackViewActions.saveFeedBackViewListResponse(json.data))
        } else {
            yield put(FeedBackViewActions.saveFeedBackViewListResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: FeedBackViewActionsTypes.SAVE_FEEDBACK_VIEW_LIST_RESPONSE, data })
    }
}

function* fetchCloseFeedViewBack(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.put(`${serviceList.closeFeedBackViewList}/${params}`)
        if (json.status === 200) {
            yield put(FeedBackCloseActions.saveCloseFeedBackViewListResponse(json.data))
        } else {
            yield put(FeedBackCloseActions.saveCloseFeedBackViewListResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: FeedBackClosectionsTypes.SAVE_CLOSE_FEEDBACK_VIEW_LIST_RESPONSE, data })
    }
}

function* fetchFeedBackCount(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.get(`${serviceList.feedBackCount}`)
        if (json.status === 200) {
            yield put(FeedBackCountActions.saveFeedBackCountResponse(json.data))
        } else {
            yield put(FeedBackCountActions.saveFeedBackCountResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: FeedBackCountActionsTypes.SAVE_FEEDBACK_COUNT_RESPONSE, data })
    }
}

export function* fetchFeedBackWatcher() {
    yield all([
        yield takeLatest(FeedBackActionsTypes.GET_FEEDBACK_LIST, fetchFeedBack),
        yield takeLatest(SendFeedBackactionsTypes.SEND_FEEDBACK_POST, FeedBackPost),
        yield takeLatest(NotificationFeedBackActionsTypes.GET_NOTIFICATION_FEEDBACK_LIST, notificationFetchFeedBack),
        yield takeLatest(FeedBackViewActionsTypes.GET_FEEDBACK_VIEW_LIST, fetchFeedViewBack),
        yield takeLatest(FeedBackClosectionsTypes.GET_CLOSE_FEEDBACK_VIEW_LIST, fetchCloseFeedViewBack),
        yield takeLatest(FeedBackCountActionsTypes.GET_FEEDBACK_COUNT, fetchFeedBackCount)
    ])
}