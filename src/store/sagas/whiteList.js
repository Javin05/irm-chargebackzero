import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import {
  WhiteListTypes,
  WhiteListActions,
  whiteListUploadActions,
  WhiteListUploadTypes,
  WhiteListDeleteClientActions,
  WhiteListDeleteClientTypes
} from '../actions'

function* fetchWhiteListSaga(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.fetchWhitelist, { params })
  if (json.status === 200) {
    yield put(WhiteListActions.fetchWhitelistSuccess(json.data))
  } else {
    yield put(WhiteListActions.fetchWhitelistError([]))
  }
}
function* fetchWhiteListTypeSaga(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.fetchWhitelistType, { params })
  if (json.status === 200) {
    yield put(WhiteListActions.fetchWhitelistTypeSuccess(json.data))
  } else {
    yield put(WhiteListActions.fetchWhitelisttypeError([]))
  }
}
function* createWhiteListTypeSaga(actions) {
  const { formData } = actions
  const json = yield axiosInstance.post(serviceList.createWhitelistType, formData)
  if (json.status === 200) {
    yield put(WhiteListActions.createWhitelistTypeSuccess(json.data))
  } else {
    yield put(WhiteListActions.createWhitelistTypeError([]))
  }
}
function* updateWhiteListTypeSaga(actions) {
  const { id, formData } = actions
  const endPointUrl = `${serviceList.updateWhitelistType}/${id}`
  const json = yield axiosInstance.put(endPointUrl, formData)
  if (json.status === 200) {
    yield put(WhiteListActions.updateWhitelistTypeSuccess(json.data))
  } else {
    yield put(WhiteListActions.updateWhitelistTypeError([]))
  }
}
function* deleteWhiteListTypeSaga(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.deleteWhitelistType}/${id}`
  const json = yield axiosInstance.delete(endPointUrl)
  if (json.status === 200) {
    yield put(WhiteListActions.deleteWhitelistTypeSuccess(json.data))
  } else {
    yield put(WhiteListActions.deleteWhitelistTypeError([]))
  }
}
function* fetchWhiteListExportSaga(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.fetchWhitelistType, { params })
  if (json.status === 200) {
    yield put(WhiteListActions.fetchWhitelistExportSuccess(json.data))
  } else {
    yield put(WhiteListActions.fetchWhitelistExportError([]))
  }
}
function* csvPostSaga(actions) {
  const { payload } = actions
  const json = yield axiosInstance.post(serviceList.csvWhiteLiist, payload)
  if (json.status === 200) {
    yield put(whiteListUploadActions.savewhiteListUploadResponse(json.data))
  } else {
    yield put(whiteListUploadActions.savewhiteListUploadResponse([]))
  }
}

function* DeleteWhiteListClient(actions) {
  const { payload } = actions
  try {
    const data = yield axiosInstance.post(serviceList.WhiteListDeleteall, payload)
    if (data && data.data) {
      yield put(WhiteListDeleteClientActions.WhiteListDeleteClientSuccess(data.data))
    }
  } catch (error) {
    yield put(WhiteListDeleteClientActions.WhiteListDeleteClientError(error))
  }
}

export function* whitelistWatcher() {
  yield all([yield takeLatest(WhiteListTypes.FETCH_WHITELIST_INIT, fetchWhiteListSaga)])
  yield all([yield takeLatest(WhiteListTypes.FETCH_WHITELISTTYPE_INIT, fetchWhiteListTypeSaga)])
  yield all([yield takeLatest(WhiteListTypes.CREATE_WHITELISTTYPE_INIT, createWhiteListTypeSaga)])
  yield all([yield takeLatest(WhiteListTypes.UPDATE_WHITELISTTYPE_INIT, updateWhiteListTypeSaga)])
  yield all([yield takeLatest(WhiteListUploadTypes.POST_WHITELISTUPLOAD_LIST, csvPostSaga)])
  yield all([yield takeLatest(WhiteListTypes.DELETE_WHITELISTTYPE_INIT, deleteWhiteListTypeSaga)])
  yield all([yield takeLatest(WhiteListDeleteClientTypes.WHITE_LIST_DELETE_CLIENT, DeleteWhiteListClient)])
  yield all([yield takeLatest(WhiteListTypes.FETCH_WHITELISTEXPORT_INIT, fetchWhiteListExportSaga)])
}