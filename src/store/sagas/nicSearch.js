import { put, takeLatest, all } from 'redux-saga/effects'
import { API_MESSAGES } from '../../utils/constants'
import axiosInstance from '../../services'
import {
    NicSearchactionsTypes,
    NicSearchactions
} from '../actions'
import serviceList from '../../services/serviceList'

function* nicSearchPost(actions) {
    try {
        const { params } = actions
        const json = yield axiosInstance.post(serviceList.nicSearch, params)
        if (json.status === 200) {
            yield put(NicSearchactions.saveNicSearchPostResponse(json.data))
        } else {
            yield put(NicSearchactions.saveNicSearchPostResponse([]))
        }
    } catch (error) {
        const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
        yield put({ type: NicSearchactionsTypes.NIC_SEARCH_RESPONSE, data })
    }
}

export function* fetchNicSearchWatcher() {
    yield all([
        yield takeLatest(NicSearchactionsTypes.NIC_SEARCH_POST, nicSearchPost),
    ])
}