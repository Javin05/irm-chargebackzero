import { put, takeLatest, all } from "redux-saga/effects"
import axiosInstance from "../../services"
import {
    OgmForceUploadSummaryAction,
    OgmForceUploadSummaryTypes
} from "../actions"
import serviceList from "../../services/serviceList"

function* fetchOgmForceUploadSummaryList(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.OgmForceUploadSummary, { params })
  if (json.status === 200) {
    yield put(OgmForceUploadSummaryAction.saveOgmForceUpload(json.data))
  } else {
    yield put(OgmForceUploadSummaryAction.saveOgmForceUpload([]))
  }
}

export function* fetchOgmForceUploadSummaryWatcher() {
    yield all([
      yield takeLatest(OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST, fetchOgmForceUploadSummaryList), 
    ])
  }