import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  preventionAlertActionsTypes,
  preventionAlertActions,
  dashboardGetDetailsTypes,
  dashboardDetailsActions,
  editAlertsTypes,
  editAlertsActions
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchPrevention (actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.preventionAlert, { params })
  if (json.status === 200) {
    yield put(preventionAlertActions.savePreventionAlertResponse(json.data))
  } else {
    yield put(preventionAlertActions.savePreventionAlertResponse([]))
  }
}

function * getdashboard (actions) {

  const { id } = actions
  const endPointUrl = `${serviceList.riskManagementList}/${id}`
  const json = yield axiosInstance.get(endPointUrl)
  if (json.status === 200) {
    yield put(dashboardDetailsActions.savedashboardDetailsResponse(json.data))
  } else {
    yield put(dashboardDetailsActions.savedashboardDetailsResponse([]))
  }
}

function * editAlerts (actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.editAlerts}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(editAlertsActions.saveeditAlertsResponse(json.data))
  } else {
    yield put(editAlertsActions.saveeditAlertsResponse([]))
  }
}

export function * fetchPreventionWatcher () {
  yield all([
    yield takeLatest(preventionAlertActionsTypes.GET_PREVENTIONALERT, fetchPrevention),
    // yield takeLatest(dashboardGetDetailsTypes.GET_PREVENTION_ALERT_DETAILS, getdashboard),
    yield takeLatest(editAlertsTypes.REQUEST, editAlerts)
  ])
}
