import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  MerchantLoginAction,
  MerchantLoginTypes
} from '../actions'
import serviceList from '../../services/serviceList'

function * fetchMerchantLogin (action) {
  const { payload } = action
  try {
    const data = yield axiosInstance.post(serviceList.merchantLogin, payload)
    if (data && data.data) {
      yield put(MerchantLoginAction.saveMerchantLoginResponse(data.data))
    }
  } catch (error) {
    yield put(MerchantLoginAction.saveMerchantLoginResponse(error))
  }
}

export function * fetchMerchantLoginWatcher () {
  yield all([yield takeLatest(MerchantLoginTypes.MERCHANT_LOGIN, fetchMerchantLogin)])
}
