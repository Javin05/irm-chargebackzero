import { put, takeLatest, all } from "redux-saga/effects";
import axiosInstance from "../../services";
import { API_MESSAGES } from '../../utils/constants'
import {
  updateWrmRealtimeupdateimagesActionsTypes,
  updateWrmRealtimeupdateimagesActions,
  updateWebsiteStatusChangeActions,
  updateWebsiteStatusChangeActionsTypes,
  WRMExportWebreportTypes,
  WRMExportWebreportTypesActions,
  WrmRealtimeUpdateGetByIdTypes,
  WrmRealtimeUpdateGetByIdActions,
  WrmRealtimeUpdateDashboardGetByIdTypes,
  WrmRealtimeUpdateDashboardGetByIdActions,
  WrmRealtimeUpdateDistancedataGetByIdTypes,
  WrmRealtimeUpdateDistancedataGetByIdActions,
  WrmRTUpdateRedirectionURLActionsTypes,
  WrmRTUpdateRedirectionURLActions,
  WRMExportPlayStoreReportTypes,
  WRMExportPlayStoreReportActions,
  WRMExportAppStoreReportActions,
  WRMExportAppStoreReportTypes,
  WRMRealTimeCategoriesStatusActions,
  WRMRealTimeCategoriesStatusTypes,
  WRMRealtimePMAListActions,
  WRMRealtimePMAListTypes,
  WrmRealtimeGetWebRiskScoreGetByIdActions,
  WrmRealtimeGetWebRiskScoreGetByIdTypes,
  WRMRealtimeBlackWhiteListTypesActions,
  WRMRealtimeBlackWhiteListTypes,
  WrmRTStatusAction,
  WrmRTStatusType,
  WRMRealtimeGetClientListTypes,
  WRMRealtimeGetClientListActions
} from '../actions'

import serviceList from "../../services/serviceList"

function* fetchWRMRealtimeupdateimages(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.wrmRealtimeUpdateImage}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(updateWrmRealtimeupdateimagesActions.saveupdateWrmRealtimeupdateimagesResponse(json.data))
    } else {
      yield put(updateWrmRealtimeupdateimagesActions.saveupdateWrmRealtimeupdateimagesResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: updateWrmRealtimeupdateimagesActionsTypes.SAVE_UPDATE_WRM_REALTIME_IMAGE_RESPONSE, data })
  }
}

function* fetchWRMWebsiteStatusChange(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.wrmRealtimeUpdateWebStatusChange}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(updateWebsiteStatusChangeActions.saveupdateWebsiteStatusChangeResponse(json.data))
    } else {
      yield put(updateWebsiteStatusChangeActions.saveupdateWebsiteStatusChangeResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: updateWebsiteStatusChangeActionsTypes.SAVE_UPDATE_WRM_WEBSITE_STATUS_CHANGE_RESPONSE, data })
  }
}

function* fetchWRMRealtimeUpdateExportWebreportlist(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmRealtimeUpdateExportWebreport, { params })
    if (json.status === 200) {
      yield put(WRMExportWebreportTypesActions.saveWRMExportWebReportlistResponse(json.data))
    } else {
      yield put(WRMExportWebreportTypesActions.saveWRMExportWebReportlistResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMExportWebreportTypes.SAVE_WRM_EXPORT_WEBREPORT_LIST_RESPONSE, data })
  }
}

function* fetchWrmRealtimeUpdateGetById(actions) {
  const { id } = actions
  const json = yield axiosInstance.get(`${serviceList.wrmRealtimeUpdateGetById}/${id}`,)
  if (json.status === 200) {
    yield put(WrmRealtimeUpdateGetByIdActions.saveWrmRealtimeUpdateGetByIdResponse(json.data))
  } else {
    yield put(WrmRealtimeUpdateGetByIdActions.saveWrmRealtimeUpdateGetByIdResponse([]))
  }
}

function* fetchWrmRealtimeUpdateDashboardGetById(actions) {
  const { id } = actions
  const json = yield axiosInstance.get(`${serviceList.wrmRealtimeUpdateGetByIdDashboard}/${id}`,)
  if (json.status === 200) {
    yield put(WrmRealtimeUpdateDashboardGetByIdActions.saveWrmRealtimeUpdateDashboardGetByIdResponse(json.data))
  } else {
    yield put(WrmRealtimeUpdateDashboardGetByIdActions.saveWrmRealtimeUpdateDashboardGetByIdResponse([]))
  }
}

function* fetchWrmRealtimeUpdateDistanceDataGetById(actions) {
  const { id } = actions
  const json = yield axiosInstance.get(`${serviceList.wrmRealtimeUpdateGetByIdDistanceData}/${id}`,)
  if (json.status === 200) {
    yield put(WrmRealtimeUpdateDistancedataGetByIdActions.saveWrmRealtimeUpdateDistanceDataGetByIdResponse(json.data))
  } else {
    yield put(WrmRealtimeUpdateDistancedataGetByIdActions.saveWrmRealtimeUpdateDistanceDataGetByIdResponse([]))
  }
}

function* fetchWrmRTRedirectionURLUpdate(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.WRMRealtimeUpdateRedirectionURL}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(WrmRTUpdateRedirectionURLActions.saveWrmRTUpdateRedirectionURLResponse(json.data))
  } else {
    yield put(WrmRTUpdateRedirectionURLActions.saveWrmRTUpdateRedirectionURLResponse([]))
  }
}

function* fetchWRMRealtimeExportPlayStoreReportlist(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmExportPlayStoreReport, { params })
    if (json.status === 200) {
      yield put(WRMExportPlayStoreReportActions.saveWRMExportPlayStoreReportlistResponse(json.data))
    } else {
      yield put(WRMExportPlayStoreReportActions.saveWRMExportPlayStoreReportlistResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMExportPlayStoreReportTypes.SAVE_WRM_EXPORT_PLAYSTORE_REPORT_LIST_RESPONSE, data })
  }
}

function* fetchWRMRealtimeExportAppStoreReportlist(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmExportAppStoreReport, { params })
    if (json.status === 200) {
      yield put(WRMExportAppStoreReportActions.saveWRMExportAppStoreReportlistResponse(json.data))
    } else {
      yield put(WRMExportAppStoreReportActions.saveWRMExportAppStoreReportlistResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMExportAppStoreReportTypes.SAVE_WRM_EXPORT_APPSTORE_REPORT_LIST_RESPONSE, data })
  }
}

function* fetchWRMRealtimeCategoriesStatuslist(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmRiskRealtimeCategorieStatus, { params })
    if (json.status === 200) {
      yield put(WRMRealTimeCategoriesStatusActions.saveWRMRealTimeCategoriesStatuslistResponse(json.data))
    } else {
      yield put(WRMRealTimeCategoriesStatusActions.saveWRMRealTimeCategoriesStatuslistResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMRealTimeCategoriesStatusTypes.SAVE_WRM_REALTIME_CATEGORIES_STATUS_LIST_RESPONSE, data })
  }
}

function* fetchWRMRealtimePMAlist(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmRiskRealtimePMA, { params })
    if (json.status === 200) {
      yield put(WRMRealtimePMAListActions.saveWRMRealtimePMAlistResponse(json.data))
    } else {
      yield put(WRMRealtimePMAListActions.saveWRMRealtimePMAlistResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMRealtimePMAListTypes.SAVE_WRM_REALTIME_PMA_LIST_RESPONSE, data })
  }
}

function* fetchWrmRealtimeGetWebRiskScoreGetById(actions) {
  const { id } = actions
  const json = yield axiosInstance.get(`${serviceList.wrmRiskRTMGetWebRiskScore}/${id}`,)
  if (json.status === 200) {
    yield put(WrmRealtimeGetWebRiskScoreGetByIdActions.saveWrmRealtimeGetWebRiskScoreGetByIdResponse(json.data))
  } else {
    yield put(WrmRealtimeGetWebRiskScoreGetByIdActions.saveWrmRealtimeGetWebRiskScoreGetByIdResponse([]))
  }
}

function* fetchWRMRealtimeBlackWhitelistType(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmRiskRTMBlackWhite, { params })
    if (json.status === 200) {
      yield put(WRMRealtimeBlackWhiteListTypesActions.saveWRMRealtimeBlackWhiteListTypesResponse(json.data))
    } else {
      yield put(WRMRealtimeBlackWhiteListTypesActions.saveWRMRealtimeBlackWhiteListTypesResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMRealtimeBlackWhiteListTypes.SAVE_WRM_REALTIME_BLACK_WHITE_LIST_TYPE_RESPONSE, data })
  }
}

function* WrmRTStatusSaga(action) {
  const {payload} = action
  const { params } = payload && payload
  const endPointUrl = `${serviceList.wrmStatus}`
  try {const data = yield axiosInstance.post(endPointUrl, params)
    if (data && data.data) {
      yield put(WrmRTStatusAction.WrmRTStatusResponse(data.data))
    }
  } catch (error) {
    yield put(WrmRTStatusAction.WrmRTStatusResponse(error))
  }
}

function* fetchWRMRealtimeGetClientlist(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.wrmRiskRTMGetClient, { params })
    if (json.status === 200) {
      yield put(WRMRealtimeGetClientListActions.saveWRMRealtimeGetClientListResponse(json.data))
    } else {
      yield put(WRMRealtimeGetClientListActions.saveWRMRealtimeGetClientListResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: WRMRealtimeGetClientListTypes.SAVE_WRM_REALTIME_GET_CLIENT_LIST_RESPONSE, data })
  }
}

export function* fetchWRMRealtimeSummaryupdateWatcher() {
  yield all([
    yield takeLatest(updateWrmRealtimeupdateimagesActionsTypes.UPDATE_WRM_REALTIME_IMAGE, fetchWRMRealtimeupdateimages),
    yield takeLatest(updateWebsiteStatusChangeActionsTypes.UPDATE_WRM_WEBSITE_STATUS_CHANGE, fetchWRMWebsiteStatusChange),
    yield takeLatest(WRMExportWebreportTypes.GET_WRM_EXPORT_WEBREPORT_LIST, fetchWRMRealtimeUpdateExportWebreportlist),
    yield takeLatest(WrmRealtimeUpdateGetByIdTypes.GET_WRM_REALTIME_UPDATE_BY_ID, fetchWrmRealtimeUpdateGetById),
    yield takeLatest(WrmRealtimeUpdateDashboardGetByIdTypes.GET_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID, fetchWrmRealtimeUpdateDashboardGetById),
    yield takeLatest(WrmRealtimeUpdateDistancedataGetByIdTypes.GET_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID, fetchWrmRealtimeUpdateDistanceDataGetById),
    yield takeLatest(WrmRTUpdateRedirectionURLActionsTypes.WRMRT_UPDATE_REDIRECTION_URL, fetchWrmRTRedirectionURLUpdate),
    yield takeLatest(WRMExportPlayStoreReportTypes.GET_WRM_EXPORT_PLAYSTORE_REPORT_LIST, fetchWRMRealtimeExportPlayStoreReportlist),
    yield takeLatest(WRMExportAppStoreReportTypes.GET_WRM_EXPORT_APPSTORE_REPORT_LIST, fetchWRMRealtimeExportAppStoreReportlist),
    yield takeLatest(WRMRealTimeCategoriesStatusTypes.GET_WRM_REALTIME_CATEGORIES_STATUS_LIST, fetchWRMRealtimeCategoriesStatuslist),
    yield takeLatest(WRMRealtimePMAListTypes.GET_WRM_REALTIME_PMA_LIST, fetchWRMRealtimePMAlist),
    yield takeLatest(WrmRealtimeGetWebRiskScoreGetByIdTypes.GET_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID, fetchWrmRealtimeGetWebRiskScoreGetById),
    yield takeLatest(WRMRealtimeBlackWhiteListTypes.GET_WRM_REALTIME_BLACK_WHITE_LIST_TYPE, fetchWRMRealtimeBlackWhitelistType),
    yield takeLatest(WrmRTStatusType.WRM_RT_STATUS,WrmRTStatusSaga),
    yield takeLatest(WRMRealtimeGetClientListTypes.GET_WRM_REALTIME_GET_CLIENT_LIST, fetchWRMRealtimeGetClientlist),
  ])
}