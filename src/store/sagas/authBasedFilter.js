import { put, takeLatest, all } from 'redux-saga/effects'
import { API_MESSAGES } from '../../utils/constants'
import axiosInstance from '../../services'
import {
  getClientActionTypes,
  clientCredFilterActions
} from '../actions'
import serviceList from '../../services/serviceList'

function* fetchFilterClient(actions) {
  try {
    const { params } = actions
    const json = yield axiosInstance.get(serviceList.getClients, { params })
    if (json.status === 200) {
      yield put(clientCredFilterActions.saveAuthClientResponse(json.data))
    } else {
      yield put(clientCredFilterActions.saveAuthClientResponse([]))
    }
  } catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: getClientActionTypes.SAVE_FILTERS_CLIENT_RESPONSE, data })
  }
}

export function* fetchFilterClientWatcher() {
  yield all([
    yield takeLatest(getClientActionTypes.GET_FILTERS_CLIENT, fetchFilterClient)
  ])
}
