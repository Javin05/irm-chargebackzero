import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import {
  BusinessUploadDocType,
  BusinessUploadDocAction,
  chequeDetailsDocAction,
  chequeDetailsDocType,
  categoryEntityAction,
  categoryEntityType,
  BusinessUpload4DocAction,
  BusinessUpload4DocType,
  AccountUploadAction,
  AccountUploadDocType,
  DashComUploadDocType,
  DashComUploadAction,
  HufDeedUploadAction,
  HufDeedUploadDocType,
  SolePropUploadDocType,
  SolePropUploadAction,
  DashReUploadAction,
  DashReUploadDocType,
  DashVideoApproveAction,
  DashVideoApproveType,
  uploadViewAction,
  uploadViewType,
  UpdateKycFvAction,
  UpdateKycFvType,
  addKycFvType,
  addKycFvAction,
  kycStatusType,
  kycStatusAction,
  KycStatusChangeActionsTypes,
  KycStatusChangeActions,
  KycUpdateQueueType,
  KycUpdateQueueAction
} from '../actions'
import serviceList from '../../services/serviceList'

function* BusinessSaga(action) {
  const { payload } = action
  const endPointUrl = `${serviceList.kycUpload}`
  try {
    const data = yield axiosInstance.post(endPointUrl, payload)
    if (data && data.data) {
      yield put(BusinessUploadDocAction.BusinessUploadDocResponse(data.data))
    }
  } catch (error) {
    yield put(BusinessUploadDocAction.BusinessUploadDocResponse(error))
  }
}

function* ChequeUpload(action) {
  const { payload } = action
  const {id, params} = payload && payload;
  const endPointUrl = `${serviceList.CheckDetails}/${id}`
  try {
    const data = yield axiosInstance.post(endPointUrl, params)
    if (data && data.data) {
      yield put(chequeDetailsDocAction.chequeDetailsDocResponse(data.data))
    }
  } catch (error) {
    yield put(chequeDetailsDocAction.chequeDetailsDocResponse(error))
  }
}
function* categoryEntitySaga(action) {
  const { params } = action
  const endPointUrl = `${serviceList.categoryEntity}`
  try {
    const data = yield axiosInstance.get(endPointUrl, {params})
    if (data && data.data) {
      yield put(categoryEntityAction.categoryEntityResponse(data.data))
    }
  } catch (error) {
    yield put(categoryEntityAction.categoryEntityResponse(error))
  }
}

function* BusinessUp4Saga(action) {
  const { payload } = action
  const endPointUrl = `${serviceList.kycUpload}`
  try {
    const data = yield axiosInstance.post(endPointUrl, payload)
    if (data && data.data) {
      yield put(BusinessUpload4DocAction.BusinessUpload4DocResponse(data.data))
    }
  } catch (error) {
    yield put(BusinessUpload4DocAction.BusinessUpload4DocResponse(error))
  }
}

function* AccountUploaSaga(action) {
  const { payload } = action
  const endPointUrl = `${serviceList.kycUpload}`
  try {
    const data = yield axiosInstance.post(endPointUrl, payload)
    if (data && data.data) {
      yield put(AccountUploadAction.AccountUploadDocResponse(data.data))
    }
  } catch (error) {
    yield put(AccountUploadAction.AccountUploadDocResponse(error))
  }
}

function* DashComUploaSaga(action) {
  const { payload } = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.kycStatus}/${id}`
  try {
    const data = yield axiosInstance.put(endPointUrl, params)
    if (data && data.data) {
      yield put(DashComUploadAction.DashComUploadDocResponse(data.data))
    }
  } catch (error) {
    yield put(DashComUploadAction.DashComUploadDocResponse(error))
  }
}

function* HufDeedUploaSaga(action) {
  const { payload } = action
  const endPointUrl = `${serviceList.kycUpload}`
  try {
    const data = yield axiosInstance.post(endPointUrl, payload)
    if (data && data.data) {
      yield put(HufDeedUploadAction.HufDeedUploadDocResponse(data.data))
    }
  } catch (error) {
    yield put(HufDeedUploadAction.HufDeedUploadDocResponse(error))
  }
}

function* SolePropUploaSaga(action) {
  const { payload } = action
  const endPointUrl = `${serviceList.kycUpload}`
  try {
    const data = yield axiosInstance.post(endPointUrl, payload)
    if (data && data.data) {
      yield put(SolePropUploadAction.SolePropUploadDocResponse(data.data))
    }
  } catch (error) {
    yield put(SolePropUploadAction.SolePropUploadDocResponse(error))
  }
}

function* DashReUploaSaga(action) {
  const { payload } = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.kycReUpload}/${id}`
  try {
    const data = yield axiosInstance.put(endPointUrl, params)
    if (data && data.data) {
      yield put(DashReUploadAction.DashReUploadDocResponse(data.data))
    }
  } catch (error) {
    yield put(DashReUploadAction.DashReUploadDocResponse(error))
  }
}

function* DashVideoApproveSaga(action) {
  const { payload } = action
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.kycVideoApprove}/${id}`
  try {
    const data = yield axiosInstance.put(endPointUrl, params)
    if (data && data.data) {
      yield put(DashVideoApproveAction.DashVideoApproveResponse(data.data))
    }
  } catch (error) {
    yield put(DashVideoApproveAction.DashVideoApproveResponse(error))
  }
}

function* fetchuploadView(actions) {
  const { params } = actions;
  const endPointUrl = `${serviceList.kycUploadView}/${params}`
  const json = yield axiosInstance.get(endPointUrl);
  if (json.status === 200) {
    yield put(uploadViewAction.viewUploadDocResponse(json.data));
  } else {
    yield put(uploadViewAction.viewUploadDocResponse([]));
  }
}

function* fetchaddKycFv(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.addKyc}`
  const json = yield axiosInstance.post(endPointUrl, params);
  if (json.status === 200) {
    yield put(addKycFvAction.addKycFvResponse(json.data));
  } else {
    yield put(addKycFvAction.addKycFvResponse([]));
  }
}

function* fetchupdateKycFv(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.addKyc}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params);
  if (json.status === 200) {
    yield put(UpdateKycFvAction.UpdateKycFvResponse(json.data));
  } else {
    yield put(UpdateKycFvAction.UpdateKycFvResponse([]));
  }
}

function* fetchkycStatus(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.kycliststatus, {params});
  if (json.status === 200) {
    yield put(kycStatusAction.kycStatusResponse(json.data));
  } else {
    yield put(kycStatusAction.kycStatusResponse([]));
  }
}

function* fetchkycStatusAR(actions) {
  const { params } = actions
  const json = yield axiosInstance.post(serviceList.statusupdate, params);
  if (json.status === 200) {
    yield put(KycStatusChangeActions.saveKycStatusChangeResponse(json.data));
  } else {
    yield put(KycStatusChangeActions.saveKycStatusChangeResponse([]));
  }
}

function* KycUpdateQueueSaga(action) {
  const { params } = action
  const json = yield axiosInstance.post(serviceList.updateQueue, params);
  if (json.status === 200) {
    yield put(KycUpdateQueueAction.KycUpdateQueueResponse(json.data));
  } else {
    yield put(KycUpdateQueueAction.KycUpdateQueueResponse([]));
  }
}

export function* fetchBusinessUploadWatcher() {
  yield all([yield takeLatest(KycUpdateQueueType.KYC_UPDATE_QUEUE, KycUpdateQueueSaga)])
  yield all([yield takeLatest(BusinessUploadDocType.BUSINESSUPLOAD_DOC_UPLOAD, BusinessSaga)])
  yield all([yield takeLatest(chequeDetailsDocType.CHEQUE_DOC_UPLOAD, ChequeUpload)])
  yield all([yield takeLatest(categoryEntityType.CATEGORY_ENTITY, categoryEntitySaga)])
  yield all([yield takeLatest(BusinessUpload4DocType.BUSINESSUPLOAD4_DOC_UPLOAD, BusinessUp4Saga)])
  yield all([yield takeLatest(AccountUploadDocType.ACCOUNT_DOC_UPLOAD, AccountUploaSaga)])
  yield all([yield takeLatest(DashComUploadDocType.DASHCOMMON_DOC_UPLOAD, DashComUploaSaga)])
  yield all([yield takeLatest(HufDeedUploadDocType.HUFDEED_DOC_UPLOAD, HufDeedUploaSaga)])
  yield all([yield takeLatest(SolePropUploadDocType.SOLEPROP_DOC_UPLOAD, SolePropUploaSaga)])
  yield all([yield takeLatest(DashReUploadDocType.DASHRE_DOC_UPLOAD, DashReUploaSaga)])
  yield all([yield takeLatest(DashVideoApproveType.DASHRE_VIDEO_APPROVE, DashVideoApproveSaga)])
  yield all([yield takeLatest(uploadViewType.VIEW_UPLOAD, fetchuploadView)])
  yield all([yield takeLatest(addKycFvType.ADD_KYC_FV, fetchaddKycFv)])
  yield all([yield takeLatest(UpdateKycFvType.UPDATE_KYC_FV, fetchupdateKycFv)])
  yield all([yield takeLatest(kycStatusType.KYC_STATUS_GET, fetchkycStatus)])
  yield all([yield takeLatest(KycStatusChangeActionsTypes.KYC_STATUS_CHANGE, fetchkycStatusAR)])
}