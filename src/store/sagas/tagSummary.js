import { put, takeLatest, all } from "redux-saga/effects"
import axiosInstance from "../../services"
import { API_MESSAGES } from '../../utils/constants'
import {
  TagSummaryAction,
  TagSummaryTypes,
  UpdatetagstatusAction,
  UpdatetagstatusTypes,
  TagSummaryIdAction,
  TagSummaryIdTypes,
  TagSummaryUpdateTypes,
  TagSummaryUpdateAction,
  PostSendEmailAction,
  PostSendEmailTypes,
  BatchSummaryExportAction,
  BatchSummaryExportTypes,
  SummaryWebExportTypes,
  SummaryWebExportAction,
  SummaryPlaytoreExportTypes,
  SummaryPlaytoreExportAction,
  updateTagSummaryActionsTypes,
  updateTagSummaryActions
} from "../actions"
import serviceList from "../../services/serviceList"

function* fetchTagSummaryList(actions) {
  const { params } = actions
  const json = yield axiosInstance.get(serviceList.TagSummary, { params })
  if (json.status === 200) {
    yield put(TagSummaryAction.TagSummary_SUCCESS(json.data))
  } else {
    yield put(TagSummaryAction.TagSummary_SUCCESS([]))
  }
}

function* UpdatetagstatusList(actions) {
  const { payload } = actions
  try {
    const data = yield axiosInstance.post(serviceList.Updatetagstatus, payload)
    if (data) {
      yield put(UpdatetagstatusAction.UpdatetagstatusSuccess(data))
    }
  } catch (error) {
    yield put(UpdatetagstatusAction.UpdatetagstatusSuccess(error))
  }
}

function* fetchTagSummaryID(actions) {
  const { id } = actions
  const endPointUrl = `${serviceList.TagSummary}/${id}`
  const json = yield axiosInstance.get(endPointUrl,)
  if (json.status === 200) {
    yield put(TagSummaryIdAction.TagSummaryIdSuccess(json.data))
  } else {
    yield put(TagSummaryIdAction.TagSummaryIdSuccess([]))
  }
}

function* fetchTagSummaryUpdate(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.Updatetagcomplete}/${id}`
  const json = yield axiosInstance.put(endPointUrl, params)
  if (json.status === 200) {
    yield put(TagSummaryUpdateAction.TagSummaryUpdateSuccess(json.data))
  } else {
    yield put(TagSummaryUpdateAction.TagSummaryUpdateSuccess([]))
  }
}

function* fetchPostSendEmail(actions) {
  const { payload } = actions
  const endPointUrl = `${serviceList.SendEmail}`
  const json = yield axiosInstance.post(endPointUrl, payload)
  if (json.status === 200) {
    yield put(PostSendEmailAction.PostSendEmailSuccess(json.data))
  } else {
    yield put(PostSendEmailAction.PostSendEmailSuccess([]))
  }
}

function* fetchBatchSummaryExport(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.TagSummaryExport}`
  const json = yield axiosInstance.get(endPointUrl, {params})
  if (json.status === 200) {
    yield put(BatchSummaryExportAction.BatchSummaryExportSuccess(json.data))
  } else {
    yield put(BatchSummaryExportAction.BatchSummaryExportSuccess([]))
  }
}

function* fetchSummaryWebExport(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.TagSummaryWeb}`
  const json = yield axiosInstance.post(endPointUrl, params)
  if (json.status === 200) {
    yield put(SummaryWebExportAction.SummaryWebExportSuccess(json.data))
  } else {
    yield put(SummaryWebExportAction.SummaryWebExportSuccess([]))
  }
}

function* fetchSummaryPlayStoreExport(actions) {
  const { params } = actions
  const endPointUrl = `${serviceList.TagSummaryPlay}`
  const json = yield axiosInstance.post(endPointUrl, params)
  if (json.status === 200) {
    yield put(SummaryPlaytoreExportAction.SummaryPlaytoreExportSuccess(json.data))
  } else {
    yield put(SummaryPlaytoreExportAction.SummaryPlaytoreExportSuccess([]))
  }
}

function* fetchTagSummaryEditUpdate(actions) {
  const { payload } = actions
  const { id, params } = payload && payload
  const endPointUrl = `${serviceList.TagSummary}/${id}`
  try {
    const json = yield axiosInstance.put(endPointUrl, params)
    if (json.status === 200) {
      yield put(updateTagSummaryActions.saveupdateTagSummaryResponse(json.data))
    } else {
      yield put(updateTagSummaryActions.saveupdateTagSummaryResponse([]))
    }
  }
  catch (error) {
    const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
    yield put({ type: updateTagSummaryActionsTypes.SAVE_UPDATE_TAG_SUMMARY_RESPONSE, data })
  }
}


export function* fetchTagSummaryWatcher() {
  yield all([
    yield takeLatest(TagSummaryTypes.TAG_SUMMARYLIST, fetchTagSummaryList),
    yield takeLatest(UpdatetagstatusTypes.UPDATE_TAGSTATUS, UpdatetagstatusList),
    yield takeLatest(TagSummaryIdTypes.TAG_SUMMARY_ID, fetchTagSummaryID),
    yield takeLatest(TagSummaryUpdateTypes.TAG_SUMMARY_UPDATES, fetchTagSummaryUpdate),
    yield takeLatest(PostSendEmailTypes.POSE_SEND_EMAIL, fetchPostSendEmail),
    yield takeLatest(BatchSummaryExportTypes.BATCH_SUMMARY_EXPORT, fetchBatchSummaryExport),
    yield takeLatest(SummaryWebExportTypes.SUMMARY_WEB_EXPORT, fetchSummaryWebExport),
    yield takeLatest(SummaryPlaytoreExportTypes.SUMMARY_PLAYSTORE_EXPORT, fetchSummaryPlayStoreExport),
    yield takeLatest(updateTagSummaryActionsTypes.UPDATE_TAG_SUMMARY, fetchTagSummaryEditUpdate)

  ])
}