import { put, takeLatest, all } from "redux-saga/effects";
import axiosInstance from "../../services";
import { API_MESSAGES } from '../../utils/constants'
import { 
    WRMCommentTypes,
    WRMCommentActions,
    updateWRMCommentActionsTypes,
    updateWRMCommentActions,
    WRMCommentdeleteActionsTypes,
    WRMCommentdeleteActions,
    WrmStatusType,
    WrmStatusAction
  } from '../actions'
import serviceList from "../../services/serviceList"

  function* fetchWRMCommentlist(actions) {
    try {
      const { params } = actions
      const json = yield axiosInstance.get(serviceList.WRMComments, { params })
      if (json.status === 200) {
        yield put(WRMCommentActions.saveWRMlistResponse(json.data))
      } else {
        yield put(WRMCommentActions.saveWRMlistResponse([]))
      }
    } catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: WRMCommentTypes.SAVE_WRM_COMMENT_LIST_RESPONSE, data })
    }
  }

  function* fetchWRMCommentUpdate(actions) {
    const { payload } = actions
    const { id, params } = payload && payload
    const endPointUrl = `${serviceList.WRMCommentsUpdate}/${id}`
    try {
      const json = yield axiosInstance.put(endPointUrl, params)
      if (json.status === 200) {
        yield put(updateWRMCommentActions.saveupdateWRMCommentResponse(json.data))
      } else {
        yield put(updateWRMCommentActions.saveupdateWRMCommentResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: updateWRMCommentActionsTypes.SAVE_UPDATE_WRM_COMMENT_RESPONSE, data })
    }
  }

  function* deleteWRMComment(actions) {
    const { params } = actions
    const deleteUrl = `${serviceList.WRMCommentsDelete}/${params} `
    try {
      const json = yield axiosInstance.delete(deleteUrl)
      if (json.status === 200) {
        yield put(WRMCommentdeleteActions.saveResponse(json.data))
      } else {
        yield put(WRMCommentdeleteActions.saveResponse([]))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: WRMCommentdeleteActionsTypes.RESPONSE, data })
    }
  }

  function* WrmStatusSaga(action) {
    const {payload} = action
    const { params } = payload && payload
    const endPointUrl = `${serviceList.webriskRealtimeStatus}`
    try {const data = yield axiosInstance.post(endPointUrl, params)
      if (data && data.data) {
        yield put(WrmStatusAction.WrmStatusResponse(data.data))
      }
    } catch (error) {
      yield put(WrmStatusAction.WrmStatusResponse(error))
    }
  }

  export function* fetchWRMComment() {
    yield all([
      yield takeLatest(WRMCommentTypes.GET_WRM_COMMENT_LIST, fetchWRMCommentlist),
      yield takeLatest(updateWRMCommentActionsTypes.UPDATE_WRM_COMMENT, fetchWRMCommentUpdate),
      yield takeLatest(WRMCommentdeleteActionsTypes.REQUEST, deleteWRMComment),
      yield takeLatest(WrmStatusType.WRM_STATUS,WrmStatusSaga),
    ])
  }