import { put, takeLatest, all } from 'redux-saga/effects'
import axiosInstance from '../../services'
import serviceList from '../../services/serviceList'
import { 
  FileUploadActionType, 
  FraudAnalysisActionType, 
  FraudAnalysisActions, 
  FraudToolDetailActions, 
  FraudToolDetailsActionType, 
  FraudToolFileUploadActionType, 
  FraudToolListActionType, 
  FraudToolListActions, 
  FraudToolfileUploadActions, 
  fileUploadActions,
  FraudAnalysisBulkUploadActionType,
  FraudAnalysisBulkUploadActions,
  FraudAnalysisSummaryListActionType,
  FraudAnalysisSummaryListActions,
  FraudAnalysisExportActionType,
  FraudAnalysisExportActions,
  FraudAnalysisTagListActionType,
  FraudAnalysisTagListActions
} from '../actions/index'
import { API_MESSAGES } from '../../utils/constants'

function* fraudAnlysisData(action) {
    const { payload } = action
    try {
      const json = yield axiosInstance.post(serviceList.fraudAnalysisTool, payload)
      if (json.status === 200) {
        yield put(FraudAnalysisActions.response(json.data))
      } else {
        yield put(FraudAnalysisActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudAnalysisActionType.RESPONSE, data })
    }
  }

  function* fileUpload(action) {
    const { payload } = action;
    try {const data = yield axiosInstance.post(serviceList.kycUpload,payload)
      if (data && data.data) {
        yield put(FraudToolfileUploadActions.response(data.data));
      }
    } catch (error) {
      yield put(FraudToolfileUploadActions.response(error));
    }
  }

  function* fraudAnlysisList(action) {
    const  { params } = action
    try {
      const json = yield axiosInstance.get(serviceList.fraudAnalysisTool, { params })
      if (json.status === 200) {
        yield put(FraudToolListActions.response(json.data))
      } else {
        yield put(FraudToolListActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudToolListActionType.RESPONSE, data })
    }
  }
  function* fraudAnlysisDetails(action) {
    const {id} = action
    try {
      const json = yield axiosInstance.get(`${serviceList.fraudAnalysisTool}/${id}`)
      if (json.status === 200) {
        yield put(FraudToolDetailActions.response(json.data))
      } else {
        yield put(FraudToolDetailActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudToolListActionType.RESPONSE, data })
    }
  }
  function* fraudAnlysisBulkUploadData(action) {
    const { payload } = action
    try {
      const json = yield axiosInstance.post(serviceList.fraudAnalysisToolZipUpload, payload)
      if (json.status === 200) {
        yield put(FraudAnalysisBulkUploadActions.response(json.data))
      } else {
        yield put(FraudAnalysisBulkUploadActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudAnalysisBulkUploadActionType.RESPONSE, data })
    }
  }
  function* fraudAnlysisSummaryList(action) {
    const  { params } = action
    try {
      const json = yield axiosInstance.get(serviceList.fraudAnalysisToolSummaryList, { params })
      if (json.status === 200) {
        yield put(FraudAnalysisSummaryListActions.response(json.data))
      } else {
        yield put(FraudAnalysisSummaryListActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudAnalysisSummaryListActionType.RESPONSE, data })
    }
  }
  function* fraudAnlysisExport(action) {
    const { id } = action
    try {
      const json = yield axiosInstance.get(`${serviceList.fraudAnalysisToolExport}/${id}`)
      if (json.status === 200) {
        yield put(FraudAnalysisExportActions.response(json.data))
      } else {
        yield put(FraudAnalysisExportActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudAnalysisExportActionType.RESPONSE, data })
    }
  }
  function* fraudAnlysisTagList(action) {
    const  { params } = action
    try {
      const json = yield axiosInstance.get(serviceList.fraudAnalysisToolTagList, { params })
      if (json.status === 200) {
        yield put(FraudAnalysisTagListActions.response(json.data))
      } else {
        yield put(FraudAnalysisTagListActions.response(json.data))
      }
    }
    catch (error) {
      const data = { status: 'error', message: API_MESSAGES.SOMETHING_WRONG }
      yield put({ type: FraudAnalysisTagListActionType.RESPONSE, data })
    }
  }
  export function* fetchFraudAnlysisWatcher() {
    yield all([
      yield takeLatest(FraudAnalysisActionType.REQUEST, fraudAnlysisData),
      yield takeLatest(FraudToolFileUploadActionType.REQUEST, fileUpload),
      yield takeLatest(FraudToolListActionType.REQUEST, fraudAnlysisList),
      yield takeLatest(FraudToolDetailsActionType.REQUEST, fraudAnlysisDetails),
      yield takeLatest(FraudAnalysisBulkUploadActionType.REQUEST, fraudAnlysisBulkUploadData),
      yield takeLatest(FraudAnalysisSummaryListActionType.REQUEST, fraudAnlysisSummaryList),
      yield takeLatest(FraudAnalysisExportActionType.REQUEST, fraudAnlysisExport),
      yield takeLatest(FraudAnalysisTagListActionType.REQUEST, fraudAnlysisTagList),

    ])
}
