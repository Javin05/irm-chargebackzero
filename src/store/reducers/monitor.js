import {
  MonitorActionsTypes,
  OGMactionsTypes,
  MonitorDashboardActionsTypes,
  OGMintervalActionsTypes,
  MonitorDashboardIdTypes,
  MonitorDashboardDateTypes,
  MonitorDashboardStatusTypes,
  MonitorCountsTypes,
  MonitorActionUpdateTypes,
  MonitorViewsTypes,
  UpdateCurrentReportTypes,
  monitorExportReportTypes,
  monitorDeleteTypes,
  viewBackendDashboardTypes,
  UpdateBackendDashboardTypes,
  MonitarStatusChangeActionsTypes,
  MonitorUpdateQueueActionsTypes,
  UpdateViewNewCallbackTypes,
  OGMEmailTemplateTypes,
  UpdateOGMEmailTemplateTypes,
  DashbordOGMEmailTemplateTypes,
  EmailHistoryIdTypes,
  OgmForceUploadTypes,
} from '../actions'

export const MonitorInitialState = {
  Monitor: null
}

export const MonitorStoreKey = 'MonitorStore'

export const MonitorReducer = (state = MonitorInitialState, action) => {
  switch (action.type) {
    case MonitorActionsTypes.GET_MONITOR_LIST:
      return { ...state, loading: true }
    case MonitorActionsTypes.SAVE_MONITOR_LIST_RESPONSE:
      return { ...state, MonitorData: action.data, loading: false }
    case MonitorActionsTypes.CLEAR_MONITOR_LIST:
      return { ...state, MonitorData: null }
    default:
      return state
  }
}

export const OGMInitialState = {
  OGM: null
}

export const OGMStoreKey = 'OGMStore'

export const OGMReducer = (state = OGMInitialState, action) => {
  switch (action.type) {
    case OGMactionsTypes.OGM_POST:
      return { ...state, loading: true }
    case OGMactionsTypes.OGM_POST_SAVE_RESPONSE:
      return { ...state, OGMpostDAta: action.data, loading: false }
    case OGMactionsTypes.CLEAR_OGM_LIST:
      return { ...state, OGMpostDAta: null }
    default:
      return state
  }
}

export const MonitorDashboardInitialState = {
  MonitorDashboard: null
}

export const MonitorDashboardStoreKey = 'MonitorDashboardStore'

export const MonitorDashboardReducer = (state = MonitorDashboardInitialState, action) => {
  switch (action.type) {
    case MonitorDashboardActionsTypes.GET_MONITORDASHBOARD:
      return { ...state, loading: true }
    case MonitorDashboardActionsTypes.SAVE_MONITORDASHBOARD_RESPONSE:
      return { ...state, MonitorDashboardData: action.data, loading: false }
    case MonitorDashboardActionsTypes.CLEAR_MONITORDASHBOARD:
      return { ...state, MonitorDashboardData: null }
    default:
      return state
  }
}

export const OGMintervalInitialState = {
  OGMinterval: null
}

export const OGMintervalStoreKey = 'OGMintervalStore'

export const OGMintervalReducer = (state = OGMintervalInitialState, action) => {
  switch (action.type) {
    case OGMintervalActionsTypes.GET_OGM_INTERVAL_LIST:
      return { ...state, loading: true }
    case OGMintervalActionsTypes.SAVE_OGM_INTERVAL_LIST_RESPONSE:
      return { ...state, OGMintervalData: action.data, loading: false }
    case OGMintervalActionsTypes.CLEAR_OGM_INTERVAL_LIST:
      return { ...state, OGMintervalData: null }
    default:
      return state
  }
}

export const MonitorDashboardIdInitialState = {
  MonitorDashboardIData: null
}
export const MonitorDashboardIdStoreKey = 'MonitorDashboardIdStore'

export const MonitorDashboardIdReducer = (state = MonitorDashboardIdInitialState, action) => {
  switch (action.type) {
    case MonitorDashboardIdTypes.GET_MONITORDASHBOARDID:
      return { ...state, loading: true }
    case MonitorDashboardIdTypes.SAVE_MONITORDASHBOARDID_RESPONSE:
      return { ...state, MonitorDashboardIData: action.data, loading: false }
    case MonitorDashboardIdTypes.CLEAR_MONITORDASHBOARDID:
      return { ...state, MonitorDashboardIData: null }
    default:
      return state
  }
}

export const MonitorDashboardDateInitialState = {
  MonitorDashboardDateData: null
}
export const MonitorDashboardDateStoreKey = 'MonitorDashboardDateStore'

export const MonitorDashboardDateReducer = (state = MonitorDashboardDateInitialState, action) => {
  switch (action.type) {
    case MonitorDashboardDateTypes.GET_MONITORDASHBOARD_DATE:
      return { ...state, loading: true }
    case MonitorDashboardDateTypes.SAVE_MONITORDASHBOARD_DATE_RESPONSE:
      return { ...state, MonitorDashboardDateData: action.data, loading: false }
    case MonitorDashboardDateTypes.CLEAR_MONITORDASHBOARD_DATE:
      return { ...state, MonitorDashboardDateData: null }
    default:
      return state
  }
}

export const MonitorDashboardStatusInitialState = {
  MonitorDashboardStatusData: null
}
export const MonitorDashboardStatusStoreKey = 'MonitorDashboardStatusStore'

export const MonitorDashboardStatusReducer = (state = MonitorDashboardStatusInitialState, action) => {
  switch (action.type) {
    case MonitorDashboardStatusTypes.GET_MONITORDASHBOARD_STATUS:
      return { ...state, loading: true }
    case MonitorDashboardStatusTypes.SAVE_MONITORDASHBOARD_STATUS_RESPONSE:
      return { ...state, MonitorDashboardStatusData: action.data, loading: false }
    case MonitorDashboardStatusTypes.CLEAR_MONITORDASHBOARD_STATUS:
      return { ...state, MonitorDashboardStatusData: null }
    default:
      return state
  }
}

export const MonitorCountsInitialState = {
  MonitorCountsData: null
}
export const MonitorCountsStoreKey = 'MonitorCountsStore'

export const MonitorCountsReducer = (state = MonitorCountsInitialState, action) => {
  switch (action.type) {
    case MonitorCountsTypes.GET_MONITOR_COUNTS:
      return { ...state, loading: true }
    case MonitorCountsTypes.SAVE_MONITOR_COUNTS_RESPONSE:
      return { ...state, MonitorCountsData: action.data, loading: false }
    case MonitorCountsTypes.CLEAR_MONITOR_COUNTS:
      return { ...state, MonitorCountsData: null }
    default:
      return state
  }
}

export const MonitorActionUpdateInitialState = {
  MonitorActionUpdateData: null
}
export const MonitorActionUpdateStoreKey = 'MonitorActionUpdateStore'

export const MonitorActionUpdateReducer = (state = MonitorActionUpdateInitialState, action) => {
  switch (action.type) {
    case MonitorActionUpdateTypes.POST_MONITOR_ACTIONUPDATE:
      return { ...state, loading: true }
    case MonitorActionUpdateTypes.SAVE_MONITOR_ACTIONUPDATE_RESPONSE:
      return { ...state, MonitorActionUpdateData: action.data, loading: false }
    case MonitorActionUpdateTypes.CLEAR_MONITOR_ACTIONUPDATE:
      return { ...state, MonitorActionUpdateData: null }
    default:
      return state
  }
}

export const MonitorViewsInitialState = {
  MonitorViewsData: null
}
export const MonitorViewsStoreKey = 'MonitorViewsStore'

export const MonitorViewsReducer = (state = MonitorViewsInitialState, action) => {
  switch (action.type) {
    case MonitorViewsTypes.GET_MONITOR_VIEWS:
      return { ...state, loading: true }
    case MonitorViewsTypes.SAVE_MONITOR_VIEWS_RESPONSE:
      return { ...state, MonitorViewsData: action.data, loading: false }
    case MonitorViewsTypes.CLEAR_MONITOR_VIEWS:
      return { ...state, MonitorViewsData: null }
    default:
      return state
  }
}

export const UpdateCurrentReportInitialState = {
  UpdateCurrentReportData: null
}
export const UpdateCurrentReportStoreKey = 'UpdateCurrentReportStore'

export const UpdateCurrentReportReducer = (state = UpdateCurrentReportInitialState, action) => {
  switch (action.type) {
    case UpdateCurrentReportTypes.GET_UPDATE_CURRENT_REPORT:
      return { ...state, loading: true }
    case UpdateCurrentReportTypes.SAVE_UPDATE_CURRENT_REPORT_RESPONSE:
      return { ...state, UpdateCurrentReportData: action.data, loading: false }
    case UpdateCurrentReportTypes.CLEAR_UPDATE_CURRENT_REPORT:
      return { ...state, UpdateCurrentReportData: null }
    default:
      return state
  }
}

export const monitorExportReportInitialState = {
  monitorExportReportData: null
}
export const monitorExportReportStoreKey = 'monitorExportReportStore'

export const monitorExportReportReducer = (state = monitorExportReportInitialState, action) => {
  switch (action.type) {
    case monitorExportReportTypes.GET_MONITOR_EXPORT_REPORT:
      return { ...state, loading: true }
    case monitorExportReportTypes.SAVE_MONITOR_EXPORT_REPORT_RESPONSE:
      return { ...state, monitorExportReportData: action.data, loading: false }
    case monitorExportReportTypes.CLEAR_MONITOR_EXPORT_REPORT:
      return { ...state, monitorExportReportData: null }
    default:
      return state
  }
}

export const DeletemonitorReportInitialState = {
  DeletemonitorReportData: null
}
export const DeletemonitorReportStoreKey = 'DeletemonitorReportStore'

export const DeletemonitorReportReducer = (state = DeletemonitorReportInitialState, action) => {
  switch (action.type) {
    case monitorDeleteTypes.DELETE_MONITOR_REPORT:
      return { ...state, loading: true }
    case monitorDeleteTypes.SAVE_DELETE_MONITOR_REPORT_RESPONSE:
      return { ...state, DeletemonitorReportData: action.data, loading: false }
    case monitorDeleteTypes.CLEAR_DELETE_MONITOR_REPORT:
      return { ...state, DeletemonitorReportData: null }
    default:
      return state
  }
}

export const BackendDashboadReportInitialState = {
  BackendDashboadReportData: null
}
export const BackendDashboadReportStoreKey = 'BackendDashboadReportStore'

export const BackendDashboadReportReducer = (state = BackendDashboadReportInitialState, action) => {
  switch (action.type) {
    case viewBackendDashboardTypes.GET_BACKEND_DASHBOARD:
      return { ...state, loading: true }
    case viewBackendDashboardTypes.SAVE_GET_BACKEND_DASHBOARD_RESPONSE:
      return { ...state, BackendDashboadReportData: action.data, loading: false }
    case viewBackendDashboardTypes.CLEAR_GET_BACKEND_DASHBOARD:
      return { ...state, BackendDashboadReportData: null }
    default:
      return state
  }
}

export const UpdateBackendDashboardReportInitialState = {
  UpdateBackendDashboardReportData: null
}
export const UpdateBackendDashboardReportStoreKey = 'UpdateBackendDashboardReportStore'

export const UpdateBackendDashboardReportReducer = (state = UpdateBackendDashboardReportInitialState, action) => {
  switch (action.type) {
    case UpdateBackendDashboardTypes.GET_UPDATE_BACKEND_DASHBOARD:
      return { ...state, loading: true }
    case UpdateBackendDashboardTypes.SAVE_UPDATE_BACKEND_DASHBOARD_RESPONSE:
      return { ...state, UpdateBackendDashboardReportData: action.data && action.data, statusEBD: action.data.status, messageEBD: action.data.message, loading: false }
    case UpdateBackendDashboardTypes.CLEAR_UPDATE_BACKEND_DASHBOARD:
      return { ...state, UpdateBackendDashboardReportData: null, messageEBD: '', statusEBD: '' }
    default:
      return state
  }
}

export const monitarStatusARInitialState = {
  data: null,
}
export const monitarStatusARStoredKey = "monitarStatusARStore"

export const monitarStatusARReducer = (state = monitarStatusARInitialState, action) => {
  switch (action.type) {
    case MonitarStatusChangeActionsTypes.MONITAR_STATUS_CHANGE:
      return { ...state, loading: true }
    case MonitarStatusChangeActionsTypes.SAVE_MONITAR_STATUS_CHANGE_RESPONSE:
      return { ...state, MonitarStatusChangeResponce: action.data, loading: false };
    case MonitarStatusChangeActionsTypes.CLEAR_MONITAR_STATUS_CHANGE:
      return { ...state, MonitarStatusChangeResponce: null }
    default:
      return state
  }
}

export const monitorUpdateQueueInitialState = {
  data: null,
}
export const monitorUpdateQueueStoredKey = "monitorUpdateQueueStore"

export const monitorUpdateQueueReducer = (state = monitorUpdateQueueInitialState, action) => {
  switch (action.type) {
    case MonitorUpdateQueueActionsTypes.MONITOR_UPDATE_QUEUE:
      return { ...state, loading: true }
    case MonitorUpdateQueueActionsTypes.SAVE_MONITOR_UPDATE_QUEUE_RESPONSE:
      return { ...state, MonitorUpdateQueueResponce: action.data, loading: false };
    case MonitorUpdateQueueActionsTypes.CLEAR_MONITOR_UPDATE_QUEUE:
      return { ...state, MonitorUpdateQueueResponce: null }
    default:
      return state
  }
}

export const UpdateViewNewCallbackInitialState = {
  UpdateViewNewCallbackData: null
}
export const UpdateViewNewCallbackStoreKey = 'UpdateViewNewCallbackStore'

export const UpdateViewNewCallbackReducer = (state = UpdateViewNewCallbackInitialState, action) => {
  switch (action.type) {
    case UpdateViewNewCallbackTypes.GET_UPDATE_VIEW_NEW_CALLBACK:
      return { ...state, loading: true }
    case UpdateViewNewCallbackTypes.SAVE_UPDATE_VIEW_NEW_CALLBACK_RESPONSE:
      return { ...state, UpdateViewNewCallbackData: action && action.data, statusEBD: action.data.status, messageEBD: action.data.message, loading: false }
    case UpdateViewNewCallbackTypes.CLEAR_UPDATE_VIEW_NEW_CALLBACK:
      return { ...state, UpdateViewNewCallbackData: null, messageEBD: '', statusEBD: '' }
    default:
      return state
  }
}

export const OGMEmailTemplateInitialState = {
  OGMEmailTemplate: null
}

export const OGMEmailTemplateStoreKey = 'OGMEmailTemplateStore'

export const OGMEmailTemplateReducer = (state = OGMEmailTemplateInitialState, action) => {
  switch (action.type) {
    case OGMEmailTemplateTypes.OGM_EMAIL_TEMPLATE_POST:
      return { ...state, loading: true }
    case OGMEmailTemplateTypes.OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE:
      return { ...state, OGMEmailTemplatepostData: action && action.data, loading: false }
    case OGMEmailTemplateTypes.CLEAR_OGM_EMAIL_TEMPLATE_LIST:
      return { ...state, OGMEmailTemplatepostData: null }
    default:
      return state
  }
}

export const updateEmailTemplateInitialState = {
  data: null
}

export const updateEmailTemplateStoreKey = 'updateEmailTemplateStore'

export const updateEmailTemplateReducer = (state = updateEmailTemplateInitialState, action) => {
  switch (action.type) {
    case UpdateOGMEmailTemplateTypes.GET_UPDATE_OGM_EMAIL_TEMPLATE:
      return { ...state, loading: true }
    case UpdateOGMEmailTemplateTypes.SAVE_UPDATE_OGM_EMAIL_TEMPLATE_RESPONSE:
      return {
        ...state,
        saveupdateEmailTemplateResponse: action && action.data,
        loading: false
      }
    case UpdateOGMEmailTemplateTypes.CLEAR_UPDATE_OGM_EMAIL_TEMPLATE:
      return { ...state, saveupdateEmailTemplateResponse: null }
    default:
      return state
  }
}

export const DashbordOGMEmailTemplateInitialState = {
  DashbordOGMEmailTemplate: null
}

export const DashbordOGMEmailTemplateStoreKey = 'DashbordOGMEmailTemplateStore'

export const DashbordOGMEmailTemplateReducer = (state = DashbordOGMEmailTemplateInitialState, action) => {
  switch (action.type) {
    case DashbordOGMEmailTemplateTypes.DASHBOARD_OGM_EMAIL_TEMPLATE_POST:
      return { ...state, loading: true }
    case DashbordOGMEmailTemplateTypes.DASHBOARD_OGM_EMAIL_TEMPLATE_POST_SAVE_RESPONSE:
      return { ...state, DashboardOGMEmailTemplate: action && action.data, loading: false }
    case DashbordOGMEmailTemplateTypes.CLEAR_DASHBOARD_OGM_EMAIL_TEMPLATE_LIST:
      return { ...state, DashboardOGMEmailTemplate: null }
    default:
      return state
  }
}

export const EmailHistoryIdInitialState = {
  EmailHistoryIdata: null
}
export const EmailHistoryIdStoreKey = 'EmailHistoryIdStore'

export const EmailHistoryIdReducer = (state = EmailHistoryIdInitialState, action) => {
  switch (action.type) {
    case EmailHistoryIdTypes.GET_EMAIL_HISTORY_ID:
      return { ...state, loading: true }
    case EmailHistoryIdTypes.SAVE_EMAIL_HISTORY_ID_RESPONSE:
      return { ...state, EmailHistoryIdData: action.data, loading: false }
    case EmailHistoryIdTypes.CLEAR_EMAIL_HISTORY_ID:
      return { ...state, EmailHistoryIdData: null }
    default:
      return state
  }
}

export const OgmForceUploadInitialState = {
  OgmForceUpload: null
}

export const OgmForceUploadStoreKey = 'OgmForceUploadStore'

export const OgmForceUploadReducer = (state = OgmForceUploadInitialState, action) => {
  switch (action.type) {
    case OgmForceUploadTypes.OGM_FORCE_UPLOAD_POST:
      return { ...state, loading: true }
    case OgmForceUploadTypes.OGM_FORCE_UPLOAD_POST_SAVE_RESPONSE:
      return { ...state, OgmForceUpload: action && action.data, loading: false }
    case OgmForceUploadTypes.CLEAR_OGM_FORCE_UPLOAD_LIST:
      return { ...state, OgmForceUpload: null }
    default:
      return state
  }
}