import { getOGMsummaryTypes, OGMlinkAnalyticsTypes } from '../actions'
  
export const OGMsummaryInitialState = {
  list: null
}

export const OGMsummaryStoreKey = 'OGMsummaryStore'

export const OGMsummaryReducer = (state = OGMsummaryInitialState, action) => {
  switch (action.type) {
    case getOGMsummaryTypes.GET_OGM_SUMMARY_LIST:
      return { ...state, loading: true }
    case getOGMsummaryTypes.SAVE_OGM_SUMMARY_LIST_RESPONSE:
      return { ...state, OGMsummarylists: action.data, loading: false }
    case getOGMsummaryTypes.CLEAR_OGM_SUMMARY_LIST:
      return { ...state, OGMsummarylists: null }
    default:
      return state
  }
}

export const OGMlinkAnalyticsInitialState = {
  list: null
}

export const OGMlinkAnalyticsStoreKey = 'OGMlinkAnalyticsStore'

export const OGMlinkAnalyticsReducer = (state = OGMlinkAnalyticsInitialState, action) => {
  switch (action.type) {
    case OGMlinkAnalyticsTypes.GET_OGM_LINKANALYTICS_LIST:
      return { ...state, loading: true }
    case OGMlinkAnalyticsTypes.SAVE_OGM_LINKANALYTICS_LIST_RESPONSE:
      return { ...state, OGMlinkAnalyticslists: action.data, loading: false }
    case OGMlinkAnalyticsTypes.CLEAR_OGM_LINKANALYTICS_LIST:
      return { ...state, OGMlinkAnalyticslists: null }
    default:
      return state
  }
}