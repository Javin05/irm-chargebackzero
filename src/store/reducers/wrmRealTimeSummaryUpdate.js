import {
  updateWrmRealtimeupdateimagesActionsTypes,
  updateWebsiteStatusChangeActionsTypes,
  WRMExportWebreportTypes,
  WrmRealtimeUpdateGetByIdTypes,
  WrmRealtimeUpdateDashboardGetByIdTypes,
  WrmRealtimeUpdateDistancedataGetByIdTypes,
  WrmRTUpdateRedirectionURLActionsTypes,
  WRMExportPlayStoreReportTypes,
  WRMExportAppStoreReportTypes,
  WRMRealTimeCategoriesStatusTypes,
  WRMRealtimePMAListTypes,
  WrmRealtimeGetWebRiskScoreGetByIdTypes,
  WRMRealtimeBlackWhiteListTypes,
  WrmRTStatusType,
  WRMRealtimeGetClientListTypes
} from '../actions'

export const updateWrmRealtimeupdateimagesInitialState = {
  data: null
}

export const updateWrmRealtimeupdateimagesStoreKey = 'updateWrmRealtimeupdateimagesStore'

export const updateWrmRealtimeupdateimagesReducer = (state = updateWrmRealtimeupdateimagesInitialState, action) => {
  switch (action.type) {
    case updateWrmRealtimeupdateimagesActionsTypes.UPDATE_WRM_REALTIME_IMAGE:
      return { ...state, loading: true }
    case updateWrmRealtimeupdateimagesActionsTypes.SAVE_UPDATE_WRM_REALTIME_IMAGE_RESPONSE:
      return {
        ...state,
        saveupdateWrmRealtimeupdateimagesResponse: action.data,
        loading: false
      }
    case updateWrmRealtimeupdateimagesActionsTypes.CLEAR_UPDATE_WRM_REALTIME_IMAGE:
      return { ...state, saveupdateWrmRealtimeupdateimagesResponse: null }
    default:
      return state
  }
}

export const updateWRMWebsiteStatusChangeInitialState = {
  data: null
}

export const updateWRMWebsiteStatusChangeStoreKey = 'updateWRMWebsiteStatusChangeStore'

export const updateWRMWebsiteStatusChangeReducer = (state = updateWRMWebsiteStatusChangeInitialState, action) => {
  switch (action.type) {
    case updateWebsiteStatusChangeActionsTypes.UPDATE_WRM_WEBSITE_STATUS_CHANGE:
      return { ...state, loading: true }
    case updateWebsiteStatusChangeActionsTypes.SAVE_UPDATE_WRM_WEBSITE_STATUS_CHANGE_RESPONSE:
      return {
        ...state,
        saveupdateWRMWebsiteStatusChangeResponse: action.data,
        loading: false
      }
    case updateWebsiteStatusChangeActionsTypes.CLEAR_UPDATE_WRM_WEBSITE_STATUS_CHANGE:
      return { ...state, saveupdateWRMWebsiteStatusChangeResponse: null }
    default:
      return state
  }
}

export const WRMExportWebreportlistInitialState = {
  list: null
}

export const WRMExportWebreportlistStoreKeys = 'WRMExportWebreportlistStore'

export const WRMExportWebreportlistReducers = (state = WRMExportWebreportlistInitialState, action) => {
  switch (action.type) {
    case WRMExportWebreportTypes.GET_WRM_EXPORT_WEBREPORT_LIST:
      return { ...state, loading: true }
    case WRMExportWebreportTypes.SAVE_WRM_EXPORT_WEBREPORT_LIST_RESPONSE:
      return { ...state, WRMExportWebReportlists: action.data, loading: false }
    case WRMExportWebreportTypes.CLEAR_WRM_EXPORT_WEBREPORT_LIST:
      return { ...state, WRMExportWebReportlists: null }
    default:
      return state
  }
}

export const WrmRealtimeUpdateGetByIdInitialState = {
  data: null
}

export const WrmRealtimeUpdateGetByIdStoreKey = 'WrmRealtimeUpdateGetByIdStore'

export const WrmRealtimeUpdateGetByIdReducer = (state = WrmRealtimeUpdateGetByIdInitialState, action) => {
  switch (action.type) {
    case WrmRealtimeUpdateGetByIdTypes.GET_WRM_REALTIME_UPDATE_BY_ID:
      return { ...state, loading: true }
    case WrmRealtimeUpdateGetByIdTypes.SAVE_WRM_REALTIME_UPDATE_BY_ID_RESPONSE:
      return {
        ...state,
        WrmRealtimeUpdateGetById: action.data,
        loading: false
      }
    case WrmRealtimeUpdateGetByIdTypes.CLEAR_WRM_REALTIME_UPDATE_BY_ID:
      return { ...state, WrmRealtimeUpdateGetById: null }
    default:
      return state
  }
}

export const WrmRealtimeUpdateDashboardGetByIdInitialState = {
  data: null
}

export const WrmRealtimeUpdateDashboardGetByIdStoreKey = 'WrmRealtimeUpdateDashboardGetByIdStore'

export const WrmRealtimeUpdateDashboardGetByIdReducer = (state = WrmRealtimeUpdateDashboardGetByIdInitialState, action) => {
  switch (action.type) {
    case WrmRealtimeUpdateDashboardGetByIdTypes.GET_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID:
      return { ...state, loading: true }
    case WrmRealtimeUpdateDashboardGetByIdTypes.SAVE_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID_RESPONSE:
      return {
        ...state,
        WrmRealtimeUpdateDashboardGetById: action.data,
        loading: false
      }
    case WrmRealtimeUpdateDashboardGetByIdTypes.CLEAR_WRM_REALTIME_UPDATE_DASHBOARD_BY_ID:
      return { ...state, WrmRealtimeUpdateDashboardGetById: null }
    default:
      return state
  }
}

export const WrmRealtimeUpdateDistancedataGetByIdInitialState = {
  data: null
}

export const WrmRealtimeUpdateDistancedataGetByIdStoreKey = 'WrmRealtimeUpdateDistancedataGetByIdStore'

export const WrmRealtimeUpdateDistancedataGetByIdReducer = (state = WrmRealtimeUpdateDistancedataGetByIdInitialState, action) => {
  switch (action.type) {
    case WrmRealtimeUpdateDistancedataGetByIdTypes.GET_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID:
      return { ...state, loading: true }
    case WrmRealtimeUpdateDistancedataGetByIdTypes.SAVE_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID_RESPONSE:
      return {
        ...state,
        WrmRealtimeUpdateDistancedataGetById: action.data,
        loading: false
      }
    case WrmRealtimeUpdateDistancedataGetByIdTypes.CLEAR_WRM_REALTIME_UPDATE_DISTANCE_DATA_BY_ID:
      return { ...state, WrmRealtimeUpdateDistancedataGetById: null }
    default:
      return state
  }
}

export const wrmRTUpdateRedirectionURLInitialState = {
  data: null
}
export const wrmRTUpdateRedirectionURLStoreKey = 'wrmRTUpdateRedirectionURLStore'

export const wrmRTUpdateRedirectionURLReducer = (state = wrmRTUpdateRedirectionURLInitialState, action) => {
  switch (action.type) {
    case WrmRTUpdateRedirectionURLActionsTypes.WRMRT_UPDATE_REDIRECTION_URL:
      return { ...state, loading: true }
    case WrmRTUpdateRedirectionURLActionsTypes.SAVE_WRMRT_UPDATE_REDIRECTION_URL_RESPONSE:
      return {
        ...state,
        wrmRTUpdateRedirectionURLResponce: action.data,
        loading: false
      }
    case WrmRTUpdateRedirectionURLActionsTypes.CLEAR_WRMRT_UPDATE_REDIRECTION_URL:
      return { ...state, wrmRTUpdateRedirectionURLResponce: null }
    default:
      return state
  }
}

export const WRMExportPlayStoreReportlistInitialState = {
  list: null
}

export const WRMExportPlayStoreReportlistStoreKeys = 'WRMExportPlayStoreReportlistStore'

export const WRMExportPlayStoreReportlistReducers = (state = WRMExportPlayStoreReportlistInitialState, action) => {
  switch (action.type) {
    case WRMExportPlayStoreReportTypes.GET_WRM_EXPORT_PLAYSTORE_REPORT_LIST:
      return { ...state, loading: true }
    case WRMExportPlayStoreReportTypes.SAVE_WRM_EXPORT_PLAYSTORE_REPORT_LIST_RESPONSE:
      return { ...state, WRMExportPlayStoreReportlists: action.data, loading: false }
    case WRMExportPlayStoreReportTypes.CLEAR_WRM_EXPORT_PLAYSTORE_REPORT_LIST:
      return { ...state, WRMExportPlayStoreReportlists: null }
    default:
      return state
  }
}

export const WRMExportAppStoreReportlistInitialState = {
  list: null
}

export const WRMExportAppStoreReportlistStoreKeys = 'WRMExportAppStoreReportlistStore'

export const WRMExportAppStoreReportlistReducers = (state = WRMExportAppStoreReportlistInitialState, action) => {
  switch (action.type) {
    case WRMExportAppStoreReportTypes.GET_WRM_EXPORT_APPSTORE_REPORT_LIST:
      return { ...state, loading: true }
    case WRMExportAppStoreReportTypes.SAVE_WRM_EXPORT_APPSTORE_REPORT_LIST_RESPONSE:
      return { ...state, WRMExportAppStoreReportlists: action.data, loading: false }
    case WRMExportAppStoreReportTypes.CLEAR_WRM_EXPORT_APPSTORE_REPORT_LIST:
      return { ...state, WRMExportAppStoreReportlists: null }
    default:
      return state
  }
}

export const WRMRealtimeCategorieStatuslistInitialState = {
  list: null
}

export const WRMRealtimeCategorieStatuslistStoreKeys = 'WRMRealtimeCategorieStatuslistStore'

export const WRMRealtimeCategorieStatuslistReducers = (state = WRMRealtimeCategorieStatuslistInitialState, action) => {
  switch (action.type) {
    case WRMRealTimeCategoriesStatusTypes.GET_WRM_REALTIME_CATEGORIES_STATUS_LIST:
      return { ...state, loading: true }
    case WRMRealTimeCategoriesStatusTypes.SAVE_WRM_REALTIME_CATEGORIES_STATUS_LIST_RESPONSE:
      return { ...state, WRMRealtimeCategorieStatuslists: action.data, loading: false }
    case WRMRealTimeCategoriesStatusTypes.CLEAR_WRM_REALTIME_CATEGORIES_STATUS_LIST:
      return { ...state, WRMRealtimeCategorieStatuslists: null }
    default:
      return state
  }
}

export const WRMRealtimePMAlistInitialState = {
  list: null
}

export const WRMRealtimePMAlistStoreKeys = 'WRMRealtimePMAlistStore'

export const WRMRealtimePMAlistReducers = (state = WRMRealtimePMAlistInitialState, action) => {
  switch (action.type) {
    case WRMRealtimePMAListTypes.GET_WRM_REALTIME_PMA_LIST:
      return { ...state, loading: true }
    case WRMRealtimePMAListTypes.SAVE_WRM_REALTIME_PMA_LIST_RESPONSE:
      return { ...state, WRMRealtimePMAlists: action.data, loading: false }
    case WRMRealtimePMAListTypes.CLEAR_WRM_REALTIME_PMA_LIST:
      return { ...state, WRMRealtimePMAlists: null }
    default:
      return state
  }
}

export const WrmRealtimeGetWebriskScoreGetByIdInitialState = {
  data: null
}

export const WrmRealtimeGetWebriskScoreGetByIdStoreKey = 'WrmRealtimeGetWebriskScoreGetByIdStore'

export const WrmRealtimeGetWebriskScoreGetByIdReducer = (state = WrmRealtimeGetWebriskScoreGetByIdInitialState, action) => {
  switch (action.type) {
    case WrmRealtimeGetWebRiskScoreGetByIdTypes.GET_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID:
      return { ...state, loading: true }
    case WrmRealtimeGetWebRiskScoreGetByIdTypes.SAVE_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID_RESPONSE:
      return {
        ...state,
        WrmRealtimeGetWebriskScoreGetById: action.data,
        loading: false
      }
    case WrmRealtimeGetWebRiskScoreGetByIdTypes.CLEAR_WRM_REALTIME_GET_WEBRISKSCORE_BY_ID:
      return { ...state, WrmRealtimeGetWebriskScoreGetById: null }
    default:
      return state
  }
}

export const WRMRealtimeBlackWhiteListTypesInitialState = {
  list: null
}

export const WRMRealtimeBlackWhiteListTypesStoreKeys = 'WRMRealtimeBlackWhiteListTypesStore'

export const WRMRealtimeBlackWhiteListTypesReducers = (state = WRMRealtimeBlackWhiteListTypesInitialState, action) => {
  switch (action.type) {
    case WRMRealtimeBlackWhiteListTypes.GET_WRM_REALTIME_BLACK_WHITE_LIST_TYPE:
      return { ...state, loading: true }
    case WRMRealtimeBlackWhiteListTypes.SAVE_WRM_REALTIME_BLACK_WHITE_LIST_TYPE_RESPONSE:
      return { ...state, WRMRealtimeBlackWhiteList: action.data, loading: false }
    case WRMRealtimeBlackWhiteListTypes.CLEAR_WRM_REALTIME_BLACK_WHITE_LIST_TYPE:
      return { ...state, WRMRealtimeBlackWhiteList: null }
    default:
      return state
  }
}

export const WrmRTStatusInitialState = {
  data: null,
}
export const WrmRTStatusKey = "WrmRTStatusStores"

export const WrmRTStatusReducer = (state = WrmRTStatusInitialState, action) => {
  switch (action.type) {
    case WrmRTStatusType.WRM_RT_STATUS:
      return { ...state, loading: true }
    case WrmRTStatusType.WRM_RT_STATUS_RESPONSE:
      return { ...state, WrmRTStatusres: action.data, loading: false }
    case WrmRTStatusType.WRM_RT_STATUS_CLEAR:
      return { ...state, WrmRTStatusres: null }
    default:
      return state
  }
}

export const WRMRealtimeGetClientlistInitialState = {
  list: null
}

export const WRMRealtimeGetClientlistStoreKeys = 'WRMRealtimeGetClientlistStore'

export const WRMRealtimeGetClientlistReducers = (state = WRMRealtimeGetClientlistInitialState, action) => {
  switch (action.type) {
    case WRMRealtimeGetClientListTypes.GET_WRM_REALTIME_GET_CLIENT_LIST:
      return { ...state, loading: true }
    case WRMRealtimeGetClientListTypes.SAVE_WRM_REALTIME_GET_CLIENT_LIST_RESPONSE:
      return { ...state, WRMRealtimeGetClientlists: action.data, loading: false }
    case WRMRealtimeGetClientListTypes.CLEAR_WRM_REALTIME_GET_CLIENT_LIST:
      return { ...state, WRMRealtimeGetClientlists: null }
    default:
      return state
  }
}