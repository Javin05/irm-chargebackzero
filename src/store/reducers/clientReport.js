import { ClientWebActionsTypes, PostClientWebActionsTypes, ClientPlayStoreActionsTypes, PostClientPlaStoreActionsTypes, exportClientPlaStoreActionsTypes, exportClientWebActionsTypes, exportClientAppStoreActionsTypes, ClientAppStoreActionsTypes, PostClientAppStoreActionsTypes } from '../actions'
  
export const clientReportInitialState = {
  list: null
}

export const clientReportStoreKey = 'clientReportStore'

export const clientReportReducer = (state = clientReportInitialState, action) => {
  switch (action.type) {
    case ClientWebActionsTypes.GET_CLIENTWEB_LIST:
      return { ...state, loading: true }
    case ClientWebActionsTypes.SAVE_CLIENTWEB_LIST_RESPONSE:
      return { ...state, clientReports: action.data, loading: false }
    case ClientWebActionsTypes.CLEAR_CLIENTWEB_LIST:
      return { ...state, clientReports: null }
    default:
      return state
  }
}

export const postclientReportInitialState = {
  list: null
}

export const postclientReportStoreKey = 'postclientReportStore'

export const postclientReportReducer = (state = postclientReportInitialState, action) => {
  switch (action.type) {
    case PostClientWebActionsTypes.POST_CLIENTWEB_LIST:
      return { ...state, loading: true }
    case PostClientWebActionsTypes.POST_SAVE_CLIENTWEB_LIST_RESPONSE:
      return { ...state, postclientReports: action.data, loading: false }
    case PostClientWebActionsTypes.POST_CLEAR_CLIENTWEB_LIST:
      return { ...state, postclientReports: null }
    default:
      return state
  }
}

export const clientPlayStoreReportInitialState = {
  list: null
}

export const clientPlayStoreReportStoreKey = 'clientPlayStoreReportStore'

export const clientPlayStoreReportReducer = (state = clientPlayStoreReportInitialState, action) => {
  switch (action.type) {
    case ClientPlayStoreActionsTypes.GET_CLIENTPLAYSTORE_LIST:
      return { ...state, loading: true }
    case ClientPlayStoreActionsTypes.SAVE_CLIENTPLAYSTORE_LIST_RESPONSE:
      return { ...state, clientPlayStoreReports: action.data, loading: false }
    case ClientPlayStoreActionsTypes.CLEAR_CLIENTPLAYSTORE_LIST:
      return { ...state, clientPlayStoreReports: null }
    default:
      return state
  }
}

export const clientAppStoreReportInitialState = {
  list: null
}

export const clientAppStoreReportStoreKey = 'clientAppStoreReportStore'

export const clientAppStoreReportReducer = (state = clientAppStoreReportInitialState, action) => {
  switch (action.type) {
    case ClientAppStoreActionsTypes.GET_CLIENTAPPSTORE_LIST:
      return { ...state, loading: true }
    case ClientAppStoreActionsTypes.SAVE_CLIENTAPPSTORE_LIST_RESPONSE:
      return { ...state, clientAppStoreReports: action.data, loading: false }
    case ClientAppStoreActionsTypes.CLEAR_CLIENTAPPSTORE_LIST:
      return { ...state, clientAppStoreReports: null }
    default:
      return state
  }
}

export const PostClientPlaStoreReportInitialState = {
  list: null
}

export const PostClientPlaStoreReportStoreKey = 'PostClientPlaStoreReportStore'

export const PostClientPlaStoreReportReducer = (state = PostClientPlaStoreReportInitialState, action) => {
  switch (action.type) {
    case PostClientPlaStoreActionsTypes.POST_CLIENT_PLAYSTORE_LIST:
      return { ...state, loading: true }
    case PostClientPlaStoreActionsTypes.POST_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE:
      return { ...state, PostClientPlaStoreReports: action.data, loading: false }
    case PostClientPlaStoreActionsTypes.POST_CLEAR_CLIENT_PLAYSTORE_LIST:
      return { ...state, PostClientPlaStoreReports: null }
    default:
      return state
  }
}

export const PostClientAppStoreReportInitialState = {
  list: null
}

export const PostClientAppStoreReportStoreKey = 'PostClientAppStoreReportStore'

export const PostClientAppStoreReportReducer = (state = PostClientAppStoreReportInitialState, action) => {
  switch (action.type) {
    case PostClientAppStoreActionsTypes.POST_CLIENT_APPSTORE_LIST:
      return { ...state, loading: true }
    case PostClientAppStoreActionsTypes.POST_SAVE_CLIENT_APPSTORE_LIST_RESPONSE:
      return { ...state, PostClientAppStoreReports: action.data, loading: false }
    case PostClientAppStoreActionsTypes.POST_CLEAR_CLIENT_APPSTORE_LIST:
      return { ...state, PostClientAppStoreReports: null }
    default:
      return state
  }
}

export const exportClientPlaStoreReportInitialState = {
  list: null
}

export const exportClientPlaStoreReportStoreKey = 'exportClientPlayStoreReportStore'

export const exportClientPlaStoreReportReducer = (state = exportClientPlaStoreReportInitialState, action) => {
  switch (action.type) {
    case exportClientPlaStoreActionsTypes.EXPORT_CLIENT_PLAYSTORE_LIST:
      return { ...state, loading: true }
    case exportClientPlaStoreActionsTypes.EXPORT_SAVE_CLIENT_PLAYSTORE_LIST_RESPONSE:
      return { ...state, exportClientPlayStoreReports: action.data, loading: false }
    case exportClientPlaStoreActionsTypes.EXPORT_CLEAR_CLIENT_PLAYSTORE_LIST:
      return { ...state, exportClientPlayStoreReports: null }
    default:
      return state
  }
}

export const exportClientAppStoreReportInitialState = {
  list: null
}

export const exportClientAppStoreReportStoreKey = 'exportClientAppStoreReportStore'

export const exportClientAppStoreReportReducer = (state = exportClientAppStoreReportInitialState, action) => {
  switch (action.type) {
    case exportClientAppStoreActionsTypes.EXPORT_CLIENT_APPSTORE_LIST:
      return { ...state, loading: true }
    case exportClientAppStoreActionsTypes.EXPORT_SAVE_CLIENT_APPSTORE_LIST_RESPONSE:
      return { ...state, exportClientAppStoreReports: action.data, loading: false }
    case exportClientAppStoreActionsTypes.EXPORT_CLEAR_CLIENT_APPSTORE_LIST:
      return { ...state, exportClientAppStoreReports: null }
    default:
      return state
  }
}

export const webclientReportInitialState = {
  list: null
}

export const webclientReportStoreKey = 'webclientReportStore'

export const webclientReportReducer = (state = webclientReportInitialState, action) => {
  switch (action.type) {
    case exportClientWebActionsTypes.EXPORT_CLIENTWEB_LIST:
      return { ...state, loading: true }
    case exportClientWebActionsTypes.EXPORT_SAVE_CLIENTWEB_LIST_RESPONSE:
      return { ...state, exportclientReports: action.data, loading: false }
    case exportClientWebActionsTypes.EXPORT_CLEAR_CLIENTWEB_LIST:
      return { ...state, exportclientReports: null }
    default:
      return state
  }
}