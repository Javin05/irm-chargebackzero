import {
  WrmRealtimeSummaryTypes,
  WrmRealtimeSummaryExportTypes,
  WrmRealtimeActionsTypes,
  WrmrtBulkTypes,
  WrmrtIndividualTypes,
  wrmrtStatusTypes,
  WRMRTCommentTypes,
  dashboardRTGetDetailsPdfTypes,
  PlayStoreDashboardRTTypes,
  PostCategoryRTTypes,
  PostPmaRTTypes,
  PostProposedCategoryRTTypes,
  PriceRTTypes,
  PostBlockCategoryRTTypes
} from "../actions"

export const WrmRealtimeSummaryStoreKey = "WrmRealtimeSummaryStore"

export const WrmRealtimeSummaryInitialState = {
  list: null,
}

export const WrmRealtimeSummaryReducers = (
  state = WrmRealtimeSummaryInitialState,
  action
) => {
  switch (action.type) {
    case WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST:
      return { ...state, loading: true }
    case WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST_SUCCESS:
      return { ...state, WrmRealtimeSummaryData: action.data, loading: false }
    case WrmRealtimeSummaryTypes.WRM_REALTIME_SUMMARYLIST_CLEAR:
      return { ...state, WrmRealtimeSummaryData: null }
    default:
      return state
  }
}

export const WrmRealtimeSummaryExportStoreKey = "WrmRealtimeSummaryExportStore"

export const WrmRealtimeSummaryExportInitialState = {
  list: null,
}

export const WrmRealtimeSummaryExportReducers = (
  state = WrmRealtimeSummaryExportInitialState,
  action
) => {
  switch (action.type) {
    case WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT:
      return { ...state, loading: true }
    case WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT_SUCCESS:
      return { ...state, WrmRealtimeSummaryExportData: action.data, loading: false }
    case WrmRealtimeSummaryExportTypes.WRM_REALTIME_SUMMARY_EXPORT_CLEAR:
      return { ...state, WrmRealtimeSummaryExportData: null }
    default:
      return state
  }
}

export const WrmRealtimeInitialState = {
  data: null
}

export const WrmRealtimeStoreKey = 'WrmRealtimeStore'

export const WrmRealtimeReducer = (state = WrmRealtimeInitialState, action) => {
  switch (action.type) {
    case WrmRealtimeActionsTypes.GET_WRM_REALTIME_LIST:
      return { ...state, loading: true }
    case WrmRealtimeActionsTypes.SAVE_WRM_REALTIME_LIST_RESPONSE:
      return {
        ...state,
        WrmRealtime: action.data,
        loading: false
      }
    case WrmRealtimeActionsTypes.CLEAR_WRM_REALTIME_LIST:
      return { ...state, WrmRealtime: null }
    default:
      return state
  }
}

export const WrmrtBulkInitialState = {
  list: null
}

export const WrmrtBulkStoreKey = 'WrmrtBulkStore'

export const WrmrtBulkReducer = (state = WrmrtBulkInitialState, action) => {
  switch (action.type) {
    case WrmrtBulkTypes.POST_WRMRTBULK_LIST:
      return { ...state, loading: true }
    case WrmrtBulkTypes.SAVE_WRMRTBULK_LIST_RESPONSE:
      return { ...state, postCSVWebAnalysis: action.data, loading: false }
    case WrmrtBulkTypes.CLEAR_WRMRTBULK_LIST:
      return { ...state, postCSVWebAnalysis: null }
    default:
      return state
  }
}

export const WrmrtIndividualInitialState = {
  list: null
}

export const WrmrtIndividualStoreKey = 'WrmrtIndividualStore'

export const WrmrtIndividualReducer = (state = WrmrtIndividualInitialState, action) => {
  switch (action.type) {
    case WrmrtIndividualTypes.POST_WRMRT_INDIVIDUAL_LIST:
      return { ...state, loading: true }
    case WrmrtIndividualTypes.SAVE_WRMRT_INDIVIDUAL_LIST_RESPONSE:
      return { ...state, postManualWebAnalysis: action.data, loading: false }
    case WrmrtIndividualTypes.CLEAR_WRMRT_INDIVIDUAL_LIST:
      return { ...state, postManualWebAnalysis: null }
    default:
      return state
  }
}

export const WrmrtStatusInitialState = {
  data: null
}

export const WrmrtStatusStoreKey = 'WrmrtStatusStore'

export const WrmrtStatusManageReducer = (state = WrmrtStatusInitialState, action) => {
  switch (action.type) {
    case wrmrtStatusTypes.GET_WRMRT_STATUS:
      return { ...state, loading: true }
    case wrmrtStatusTypes.SAVE_WRMRT_STATUS_RESPONSE:
      return {
        ...state,
        WrmrtStatusRes: action.data,
        loading: false
      }
    case wrmrtStatusTypes.CLEAR_WRMRT_STATUS:
      return { ...state, WrmrtStatusRes: null }
    default:
      return state
  }
}

export const WRMRTlistInitialState = {
  list: null
}

export const WRMRTlistStoreKeys = 'WRMRTlistStore'

export const WRMRTlistReducers = (state = WRMRTlistInitialState, action) => {
  switch (action.type) {
    case WRMRTCommentTypes.GET_WRMRT_COMMENT_LIST:
      return { ...state, loading: true }
    case WRMRTCommentTypes.SAVE_WRMRT_COMMENT_LIST_RESPONSE:
      return { ...state, WRMRTlists: action.data, loading: false }
    case WRMRTCommentTypes.CLEAR_WRMRT_COMMENT_LIST:
      return { ...state, WRMRTlists: null }
    default:
      return state
  }
}

export const preventionRTInitialPdfState = {
  list: null
}

export const dashboardRTPdfStoreKey = 'dashboardRTPdfStore'
export const preventionRTPdfReducer = (state = preventionRTInitialPdfState, action) => {
  switch (action.type) {
    case dashboardRTGetDetailsPdfTypes.GET_DASHBOARDRT_DETAILSPDF:
      return { ...state, loadingPAD: true }
    case dashboardRTGetDetailsPdfTypes.DASHBOARDRT_DETAILSPDF_RESPONSE:
      return {
        ...state,
        dashboardRTDetails: action.data,
        statusPAD: action.data && action.data.status,
        messagesPAD: action.data && action.data.message,
        loadingPAD: false
      }
    case dashboardRTGetDetailsPdfTypes.CLEAR_DASHBOARDRT_DETAILSPDF:
      return { ...state, statusPAD: '', messagesPAD: '' }
    default:
      return state
  }
}

export const PlayStoreDashboardRTInitialState = {
  list: null
}

export const PlayStoreDashboardRTStoreKey = 'PlayStoreDashboardRTStore'

export const PlayStoreDashboardRTReducer = (state = PlayStoreDashboardRTInitialState, action) => {
  switch (action.type) {
    case PlayStoreDashboardRTTypes.GET_PLAYSTORE_DASHBOARDRT:
      return { ...state, loading: true }
    case PlayStoreDashboardRTTypes.SAVE_PLAYSTORE_DASHBOARDRT_RESPONSE:
      return { ...state, PlayStoreDashboardRTResponse: action, loading: false }
    case PlayStoreDashboardRTTypes.CLEAR_PLAYSTORE_DASHBOARDRT:
      return { ...state, PlayStoreDashboardRTResponse: null }
    default:
      return state
  }
}

export const PostCategoryRTInitialState = {
  list: null
}

export const PostCategoryRTStoreKey = 'PostCategoryStoreRT'

export const PostCategoryRTReducer = (state = PostCategoryRTInitialState, action) => {
  switch (action.type) {
    case PostCategoryRTTypes.POST_CATEGORYRT:
      return { ...state, loading: true }
    case PostCategoryRTTypes.SAVE_CATEGORYRT_RESPONSE:
      return { ...state, postCategoryRTRes: action, loading: false }
    case PostCategoryRTTypes.CLEAR_CATEGORYRT:
      return { ...state, postCategoryRTRes: null }
    default:
      return state
  }
}

export const PostPmaRTInitialState = {
  list: null
}

export const PostPmaRTStoreKey = 'PostPmaRTStore'

export const PostPmaRTReducer = (state = PostPmaRTInitialState, action) => {
  switch (action.type) {
    case PostPmaRTTypes.POST_PMART:
      return { ...state, loading: true }
    case PostPmaRTTypes.SAVE_PMART_RESPONSE:
      return { ...state, postPmaRTRes: action, loading: false }
    case PostPmaRTTypes.CLEAR_PMART:
      return { ...state, postPmaRTRes: null }
    default:
      return state
  }
}

export const PostProposedCategoryRTInitialState = {
  list: null
}

export const PostProposedCategoryRTStoreKey = 'PostProposedCategoryRTStore'

export const PostProposedCategoryRTReducer = (state = PostProposedCategoryRTInitialState, action) => {
  switch (action.type) {
    case PostProposedCategoryRTTypes.POST_PROPOSEDCATEGORYRT:
      return { ...state, loading: true }
    case PostProposedCategoryRTTypes.SAVE_PROPOSEDCATEGORYRT_RESPONSE:
      return { ...state, postProposedCategoryRTRes: action, loading: false }
    case PostProposedCategoryRTTypes.CLEAR_PROPOSEDCATEGORYRT:
      return { ...state, postProposedCategoryRTRes: null }
    default:
      return state
  }
}

const PopupRTInits = {
  priceSuccess: null,
  priceCheckList: null,
}

export const PriceRTStoreKey = 'PriceRTStore'

export const PriceRTReducer = (state = PopupRTInits, action) => {

  switch (action.type) {

      case PriceRTTypes.FETCH_PRICERT_POPUP_INIT:
      case PriceRTTypes.PRICERT_POPUP_INIT:
          return { 
              ...state, 
              loading: true 
          }

      case PriceRTTypes.PRICERT_POPUP_SUCCESS:
          return { 
              ...state, 
              loading: false, 
              priceSuccess: action
          }

      case PriceRTTypes.FETCH_PRICERT_POPUP_SUCCESS:
          return { 
              ...state, 
              loading: false, 
              priceCheckList: action.data?.data
          }

          case PriceRTTypes.PRICERT_POPUP_CLEAR:
          return { 
              ...state, 
              loading: false, 
              priceSuccess: null
          }
          
      case PriceRTTypes.FETCH_PRICERT_POPUP_ERROR:
      case PriceRTTypes.PRICERT_POPUP_ERROR:
          return { 
              ...state, 
              loading: false, 
              error: action 
          }

      default:
          return state
  }
}

export const PostBlockCategoryRTInitialState = {
  list: null
}

export const PostBlockCategoryRTStoreKey = 'PostBlockCategoryRTStore'

export const PostBlockCategoryRTReducer = (state = PostBlockCategoryRTInitialState, action) => {
  switch (action.type) {
    case PostBlockCategoryRTTypes.POST_PROPOSEDCATEGORYRT:
      return { ...state, loading: true }
    case PostBlockCategoryRTTypes.SAVE_PROPOSEDCATEGORYRT_RESPONSE:
      return { ...state, postProposedCategoryRTRes: action, loading: false }
    case PostBlockCategoryRTTypes.CLEAR_PROPOSEDCATEGORYRT:
      return { ...state, postProposedCategoryRTRes: null }
    default:
      return state
  }
}