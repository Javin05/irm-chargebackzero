import { LogoutActionsTypes } from '../actions'

export const logoutInitialState = {
  data: null
}

export const logoutStoreKey = 'logoutStore'

const logoutReducer = (state = logoutInitialState, action) => {
  switch (action.type) {
    case LogoutActionsTypes.LOGOUT:
      return { ...state, loading: true }
    case LogoutActionsTypes.SAVE_LOGOUT_RESPONSE:
      return { ...state, logout: action.data, loading: false }
    case LogoutActionsTypes.CLEAR_LOGOUT:
      return { ...state, logout: null }
    default:
      return state
  }
}

export default logoutReducer