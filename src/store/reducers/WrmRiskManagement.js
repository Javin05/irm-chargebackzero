import { 
  WrmRiskManagementActionsTypes, 
  wrmSdkActionsTypes,
  WrmStatusTypes,
  UpdateWebReportTypes,
  DashboardListPmaTypes,
  GetClientsTypes,
  riskLevelActionTypes,
  CategoryStatusTypes,
  updateImageUploadActionsTypes,
  updateStatusChangeActionsTypes,
  wrmStatusChangeActionsTypes,
  WrmUpdateQueueActionsTypes,
  WrmUpdateRedirectionURLActionsTypes
} from '../actions'

export const WrmRiskManagementInitialState = {
  data: null
}

export const WrmRiskManagementStoreKey = 'WrmRiskManagementStore'

export const WrmRiskManagementReducer = (state = WrmRiskManagementInitialState, action) => {
  switch (action.type) {
    case WrmRiskManagementActionsTypes.GET_WRMRISKMANAGEMENT_LIST:
      return { ...state, loading: true }
    case WrmRiskManagementActionsTypes.SAVE_WRMRISKMANAGEMENT_LIST_RESPONSE:
      return {
        ...state,
        WrmRiskManagement: action.data,
        loading: false
      }
    case WrmRiskManagementActionsTypes.CLEAR_WRMRISKMANAGEMENT_LIST:
      return { ...state, WrmRiskManagement: null }
    default:
      return state
  }
}

export const WrmStatusInitialState = {
  data: null
}

export const WrmStatusStoreKey = 'WrmStatusStore'

export const WrmStatusManageReducer = (state = WrmStatusInitialState, action) => {
  switch (action.type) {
    case WrmStatusTypes.GET_WRM_STATUS:
      return { ...state, loading: true }
    case WrmStatusTypes.SAVE_WRM_STATUS_RESPONSE:
      return {
        ...state,
        WrmStatusRes: action.data,
        loading: false
      }
    case WrmStatusTypes.CLEAR_WRM_STATUS:
      return { ...state, WrmStatusRes: null }
    default:
      return state
  }
}

export const WrmSdkInitialState = {
  data: null
}

export const WrmSdkManageStoreKey = 'WrmSdkManageStore'

export const WrmSdkManageReducer = (state = WrmSdkInitialState, action) => {
  switch (action.type) {
    case wrmSdkActionsTypes.GET_WRMSDKMANAGE_LIST:
      return { ...state, loading: true }
    case wrmSdkActionsTypes.SAVE_WRMSDKMANAGE_LIST_RESPONSE:
      return {
        ...state,
        wrmSdkManage: action.data,
        loading: false
      }
    case wrmSdkActionsTypes.CLEAR_WRMSDKMANAGE_LIST:
      return { ...state, wrmSdkManage: null }
    default:
      return state
  }
}

export const UpdateWebReportInitialState = {
  data: null
}

export const UpdateWebReportManageStoreKey = 'UpdateWebReportManageStore'

export const UpdateWebReportManageReducer = (state = UpdateWebReportInitialState, action) => {
  switch (action.type) {
    case UpdateWebReportTypes.GET_UPDATEWEBREPORT:
      return { ...state, loading: true }
    case UpdateWebReportTypes.SAVE_UPDATEWEBREPORT_RESPONSE:
      return {
        ...state,
        UpdateWebReportRes: action.data,
        loading: false
      }
    case UpdateWebReportTypes.CLEAR_UPDATEWEBREPORT:
      return { ...state, UpdateWebReportRes: null }
    default:
      return state
  }
}

export const DashboardListPmaInitialState = {
  data: null
}

export const DashboardListPmaStoreKey = 'DashboardListPmaStore'

export const DashboardListPmaManageReducer = (state = DashboardListPmaInitialState, action) => {
  switch (action.type) {
    case DashboardListPmaTypes.GET_DASHBOARDLIST_PMA:
      return { ...state, loading: true }
    case DashboardListPmaTypes.SAVE_DASHBOARDLIST_PMA_RESPONSE:
      return {
        ...state,
        DashboardListPmaRes: action.data,
        loading: false
      }
    case DashboardListPmaTypes.CLEAR_DASHBOARDLIST_PMA:
      return { ...state, DashboardListPmaRes: null }
    default:
      return state
  }
}

export const GetClientsInitialState = {
  data: null
}

export const GetClientsStoreKey = 'GetClientsStore'

export const GetClientsManageReducer = (state = GetClientsInitialState, action) => {
  switch (action.type) {
    case GetClientsTypes.GET_CLIENTS:
      return { ...state, loading: true }
    case GetClientsTypes.SAVE_GET_CLIENTS_RESPONSE:
      return {
        ...state,
        GetClientsRes: action.data,
        loading: false
      }
    case GetClientsTypes.CLEAR_GET_CLIENTS:
      return { ...state, GetClientsRes: null }
    default:
      return state
  }
}

export const RiskLevelInitialList = {
  data: null,
};
export const RiskLevelStoreKey = "RiskLevelStore";

export const RiskLevelReducer = (state = RiskLevelInitialList, action) => {
  switch (action.type) {
    case riskLevelActionTypes.GET_RISKLEVEL_LIST:
      return { ...state, loadingRL: true };
    case riskLevelActionTypes.SAVE_RISKLEVEL_LIST_RESPONSE:
      return {
        ...state,
        riskLevelList: action.data.data,
        loadingRL: false,
      };
    case riskLevelActionTypes.CLEAR_RISKLEVEL_LIST:
      return { ...state, loadingRL: false, riskLevelList: null };
    default:
      return state;
  }
}

export const CategoryStatusInitialState = {
  data: null
}

export const CategoryStatusManageStoreKey = 'CategoryStatusManageStore'

export const CategoryStatusManageReducer = (state = CategoryStatusInitialState, action) => {
  switch (action.type) {
    case CategoryStatusTypes.GET_CATEGORYSTATUS_LIST:
      return { ...state, loading: true }
    case CategoryStatusTypes.SAVE_CATEGORYSTATUS_LIST_RESPONSE:
      return {
        ...state,
        CategoryStatusData: action.data,
        loading: false
      }
    case CategoryStatusTypes.CLEAR_CATEGORYSTATUS_LIST:
      return { ...state, CategoryStatusData: null }
    default:
      return state
  }
}

export const updateImageUploadInitialState = {
  data: null
}

export const updateImageUploadStoreKey = 'updateImageUploadStore'

export const updateImageUploadReducer = (state = updateImageUploadInitialState, action) => {
  switch (action.type) {
    case updateImageUploadActionsTypes.UPDATE_IMAGE_UPLOAD:
      return { ...state, loading: true }
    case updateImageUploadActionsTypes.SAVE_IMAGE_UPLOAD_RESPONSE:
      return {
        ...state,
        updateImageUploadResponce: action.data,
        loading: false
      }
    case updateImageUploadActionsTypes.CLEAR_IMAGE_UPLOAD:
      return { ...state, updateImageUploadResponce: null }
    default:
      return state
  }
}

export const updateStatusChangeInitialState = {
  data: null
}

export const updateStatusChangeStoreKey = 'updateStatusChangeStore'

export const updateStatusChangeReducer = (state = updateStatusChangeInitialState, action) => {
  switch (action.type) {
    case updateStatusChangeActionsTypes.UPDATE_STATUS_CHANGE:
      return { ...state, loading: true }
    case updateStatusChangeActionsTypes.SAVE_STATUS_CHANGE_RESPONSE:
      return {
        ...state,
        updateStatusChangeResponce: action.data,
        loading: false
      }
    case updateStatusChangeActionsTypes.CLEAR_STATUS_CHANGE:
      return { ...state, updateStatusChangeResponce: null }
    default:
      return state
  }
}

export const wrmStatusChangeInitialState = {
  data: null
}
export const wrmStatusChangeStoreKey = 'wrmStatusChangeStore'

export const wrmStatusChangeReducer = (state = wrmStatusChangeInitialState, action) => {
  switch (action.type) {
    case wrmStatusChangeActionsTypes.WRM_STATUS_CHANGE:
      return { ...state, loading: true }
    case wrmStatusChangeActionsTypes.SAVE_WRM_STATUS_CHANGE_RESPONSE:
      return {
        ...state,
        wrmStatusChangeResponce: action.data,
        loading: false
      }
    case wrmStatusChangeActionsTypes.CLEAR_WRM_STATUS_CHANGE:
      return { ...state, wrmStatusChangeResponce: null }
    default:
      return state
  }
}

export const wrmUpdateQueueInitialState = {
  data: null
}
export const wrmUpdateQueueStoreKey = 'wrmUpdateQueueStore'

export const wrmUpdateQueueReducer = (state = wrmUpdateQueueInitialState, action) => {
  switch (action.type) {
    case WrmUpdateQueueActionsTypes.WRM_UPDATE_QUEUE:
      return { ...state, loading: true }
    case WrmUpdateQueueActionsTypes.SAVE_WRM_UPDATE_QUEUE_RESPONSE:
      return {
        ...state,
        wrmUpdateQueueResponce: action.data,
        loading: false
      }
    case WrmUpdateQueueActionsTypes.CLEAR_WRM_UPDATE_QUEUE:
      return { ...state, wrmUpdateQueueResponce: null }
    default:
      return state
  }
}

export const wrmUpdateRedirectionURLInitialState = {
  data: null
}
export const wrmUpdateRedirectionURLStoreKey = 'wrmUpdateRedirectionURLStore'

export const wrmUpdateRedirectionURLReducer = (state = wrmUpdateRedirectionURLInitialState, action) => {
  switch (action.type) {
    case WrmUpdateRedirectionURLActionsTypes.WRM_UPDATE_REDIRECTION_URL:
      return { ...state, loading: true }
    case WrmUpdateRedirectionURLActionsTypes.SAVE_WRM_UPDATE_REDIRECTION_URL_RESPONSE:
      return {
        ...state,
        wrmUpdateRedirectionURLResponce: action.data,
        loading: false
      }
    case WrmUpdateRedirectionURLActionsTypes.CLEAR_WRM_UPDATE_REDIRECTION_URL:
      return { ...state, wrmUpdateRedirectionURLResponce: null }
    default:
      return state
  }
}