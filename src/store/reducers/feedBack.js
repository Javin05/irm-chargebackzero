import {
  FeedBackActionsTypes,
  SendFeedBackactionsTypes,
  NotificationFeedBackActionsTypes,
  FeedBackViewActionsTypes,
  FeedBackClosectionsTypes,
  FeedBackCountActionsTypes
} from '../actions'

export const FeedBackInitialState = {
  FeedBack: null
}

export const FeedBackStoreKey = 'FeedBackStore'

export const FeedBackReducer = (state = FeedBackInitialState, action) => {
  switch (action.type) {
    case FeedBackActionsTypes.GET_FEEDBACK_LIST:
      return { ...state, loading: true }
    case FeedBackActionsTypes.SAVE_FEEDBACK_LIST_RESPONSE:
      return { ...state, FeedBackData: action.data, loading: false }
    case FeedBackActionsTypes.CLEAR_FEEDBACK_LIST:
      return { ...state, FeedBackData: null }
    default:
      return state
  }
}

export const SendFeedbackInitialState = {
  SendFeedback: null
}

export const SendFeedbackStoreKey = 'SendFeedbackStore'

export const SendFeedbackReducer = (state = SendFeedbackInitialState, action) => {
  switch (action.type) {
    case SendFeedBackactionsTypes.SEND_FEEDBACK_POST:
      return { ...state, loading: true }
    case SendFeedBackactionsTypes.SEND_FEEDBACK_RESPONSE:
      return { ...state, SendFeedBackData: action.data, loading: false }
    case SendFeedBackactionsTypes.CLEAR_SEND_FEEDBACK:
      return { ...state, SendFeedBackData: null }
    default:
      return state
  }
}

export const NotificationFeedBackInitialState = {
  NotificationFeedBack: null
}

export const NotificationFeedBackStoreKey = 'NotificationFeedBackStore'

export const NotificationFeedBackReducer = (state = NotificationFeedBackInitialState, action) => {
  switch (action.type) {
    case NotificationFeedBackActionsTypes.GET_NOTIFICATION_FEEDBACK_LIST:
      return { ...state, loading: true }
    case NotificationFeedBackActionsTypes.SAVE_NOTIFICATION_FEEDBACK_LIST_RESPONSE:
      return { ...state, NotificationFeedBackData: action.data, loading: false }
    case NotificationFeedBackActionsTypes.CLEAR_NOTIFICATION_FEEDBACK_LIST:
      return { ...state, NotificationFeedBackData: null }
    default:
      return state
  }
}

export const FeedBackViewInitialState = {
  FeedBack: null
}

export const FeedBackViewStoreKey = 'FeedBackViewStore'

export const FeedBackViewReducer = (state = FeedBackViewInitialState, action) => {
  switch (action.type) {
    case FeedBackViewActionsTypes.GET_FEEDBACK_VIEW_LIST:
      return { ...state, loading: true }
    case FeedBackViewActionsTypes.SAVE_FEEDBACK_VIEW_LIST_RESPONSE:
      return { ...state, FeedBackViewData: action.data, loading: false }
    case FeedBackViewActionsTypes.CLEAR_FEEDBACK_VIEW_LIST:
      return { ...state, FeedBackViewData: null }
    default:
      return state
  }
}

export const FeedBackCloseViewInitialState = {
  FeedBack: null
}

export const FeedBackCloseViewStoreKey = 'FeedBackCloseViewStore'

export const FeedBackCloseViewReducer = (state = FeedBackCloseViewInitialState, action) => {
  switch (action.type) {
    case FeedBackClosectionsTypes.GET_CLOSE_FEEDBACK_VIEW_LIST:
      return { ...state, loading: true }
    case FeedBackClosectionsTypes.SAVE_CLOSE_FEEDBACK_VIEW_LIST_RESPONSE:
      return { ...state, FeedBackClose: action.data, loading: false }
    case FeedBackClosectionsTypes.CLEAR_CLOSE_FEEDBACK_VIEW_LIST:
      return { ...state, FeedBackClose: null }
    default:
      return state
  }
}

export const FeedBackCountInitialState = {
  FeedBackCount: null
}

export const FeedBackCountStoreKey = 'FeedBackCountStore'

export const FeedBackCountReducer = (state = FeedBackCountInitialState, action) => {
  switch (action.type) {
    case FeedBackCountActionsTypes.GET_FEEDBACK_COUNT:
      return { ...state, loading: true }
    case FeedBackCountActionsTypes.SAVE_FEEDBACK_COUNT_RESPONSE:
      return { ...state, FeedBackCountData: action.data, loading: false }
    case FeedBackCountActionsTypes.CLEAR_FEEDBACK_COUNT:
      return { ...state, FeedBackCountData: null }
    default:
      return state
  }
}