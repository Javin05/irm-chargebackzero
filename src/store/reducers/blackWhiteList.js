import { backWhiteListActionTypes, updateBlackWhiteListActionsTypes, BlackWhiteListGetId } from '../actions'

export const blackWhiteListInitialState = {
  list: null
}

export const blackWhiteListStoreKey = 'blackWhiteListStore'

export const blackWhiteListReducer = (state = blackWhiteListInitialState, action) => {
  switch (action.type) {
    case backWhiteListActionTypes.GET_BlackWhiteList:
      return { ...state, loading: true }
    case backWhiteListActionTypes.POST_BlackWhiteList:
      return { ...state, loadingARG: true, }
    case backWhiteListActionTypes.POST_BlackWhiteList_RESPONSE:
      return { ...state, addBlackWhiteList: action.data, statusAR: action.data.status, messageAR: action.data.message, loadingARG: false }
    case backWhiteListActionTypes.POST_CLEAR_BlackWhiteList:
      return { ...state, addBlackWhiteList: null, statusAR: null, messageAR: null }
    case backWhiteListActionTypes.DELETE_BlackWhiteList:
      return { ...state, loading: true }
    case backWhiteListActionTypes.CLEAR_BlackWhiteList:
      return { ...state, DeleteBlackWhiteList: null }
    case backWhiteListActionTypes.DELETE_BlackWhiteList_RESPONSE:
      return { ...state, DeleteBlackWhiteList: action.data, loading: false }
    case backWhiteListActionTypes.SAVE_BlackWhiteList_RESPONSE:
      return { ...state, blackWhiteList: action.data, loading: false }
    case backWhiteListActionTypes.CLEAR_BlackWhiteList:
      return { ...state, blackWhiteList: null }
    default:
      return state
  }
}

export const editBlackWhiteListInitialState = {
  list: null
}

export const editBlackWhiteListStoreKey = 'editBlackWhiteListStore'

export const editBlackWhiteListReducer = (state = editBlackWhiteListInitialState, action) => {
  switch (action.type) { 
    case BlackWhiteListGetId.GET_BlackWhiteList_DETAILS:
      return { ...state, loadingGR: true }
    case BlackWhiteListGetId.BlackWhiteList_DETAILS_RESPONSE:
      return { ...state, BlackWhiteListIdDetail: action.data && action.data.data, statusEA: action.data.status, messageEA: action.data.message, loadingGR: false }
    case BlackWhiteListGetId.CLEAR_BlackWhiteList_DETAILS:
      return { ...state, BlackWhiteListIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const updateBlackWhiteListInitialState = {
  data: null
}

export const updateBlackWhiteListStoreKey = 'updateBlackWhiteListStore'

export const updateBlackWhiteListReducer = (state = updateBlackWhiteListInitialState, action) => {
  switch (action.type) {
    case updateBlackWhiteListActionsTypes.UPDATE_BlackWhiteList:
      return { ...state, loadingUR: true }
    case updateBlackWhiteListActionsTypes.SAVE_UPDATE_BlackWhiteList_RESPONSE:
      return { ...state, updateBlackWhiteListResponce: action.data, statusUR: action.data.status, messageUR: action.data.message, loadingUR: false }
    case updateBlackWhiteListActionsTypes.CLEAR_UPDATE_BlackWhiteList:
      return { ...state, updateBlackWhiteListResponce: null, statusUR: '', messageUR:'' }
    default:
      return state
  }
}
