import { 
    FraudTypes 
} from '../actions'

const FraudInitials = {
    fraudStatus: null,
    error: null
}

export const FraudActionStore = 'FraudActionStore'

export const fraudReducer = (state = FraudInitials, action) => {

    switch (action.type) {
        case FraudTypes.FRAUD_STATUS_INIT:
            return { 
                ...state, 
                fraudStatus: null, 
                loading: true
            }

        case FraudTypes.FRAUD_STATUS_SUCCESS:
            return { 
                ...state, 
                loading: false, 
                fraudStatus: action.data
            }

        case FraudTypes.FRAUD_STATUS_ERROR:
            return { 
                ...state, 
                error: action,
                fraudStatus: null,  
            }

        case FraudTypes.FRAUD_STATUS_CLEAR:
            return { 
                ...state, 
                error: action,
                fraudStatus: null,  
            }

        default:
            return state
    }
}