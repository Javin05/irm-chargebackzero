import {
    NicSearchactionsTypes,
  } from '../actions'

export const NicSearchInitialState = {
    NicSearch: null
  }
  
  export const NicSearchStoreKey = 'NicSearchStore'
  
  export const NicSearchReducer = (state = NicSearchInitialState, action) => {
    switch (action.type) {
      case NicSearchactionsTypes.NIC_SEARCH_POST:
        return { ...state, loading: true }
      case NicSearchactionsTypes.NIC_SEARCH_RESPONSE:
        return { ...state, nicSearchData: action.data, loading: false }
      case NicSearchactionsTypes.CLEAR_NIC_SEARCH:
        return { ...state, nicSearchData: null }
      default:
        return state
    }
  }