import { 
  webReportSettingActionsTypes, webReportSettingAddTypes, webReportSettingGetId, updatewebReportSettingActionsTypes, webReportSettingDelete
 } from '../actions'
  
export const webReportSettingslistInitialState = {
  list: null
}

export const webReportSettingslistStoreKey = 'webReportSettingslistStore'

export const webReportSettingslistReducer = (state = webReportSettingslistInitialState, action) => {
  switch (action.type) {
    case webReportSettingActionsTypes.GET_WEBREPROT_SEETING_LIST:
      return { ...state, loading: true }
    case webReportSettingActionsTypes.SAVE_WEBREPROT_SEETING_LIST_RESPONSE:
      return { ...state, webReportSettingslists: action.data, loading: false }
    case webReportSettingActionsTypes.CLEAR_WEBREPROT_SEETING_LIST:
      return { ...state, webReportSettingslists: null }
    default:
      return state
  }
} 

export const webReportSettingsAddInitialState = {
  data: null
} 

export const webReportSettingsAddStoreKey = 'webReportSettingsAddStore'

export const webReportSettingsAddReducer = (state = webReportSettingsAddInitialState, action) => {
  switch (action.type) {
    case webReportSettingAddTypes.WEBREPROT_SEETING_POST:
      return { ...state, loading: true }
    case webReportSettingAddTypes.WEBREPROT_SEETING_POST_RESPONSE:
      return { ...state, webReportSettingsAdd: action.data, webReportSettingStatus:  action.data.status, webReportSettingMessage:  action.data.message, loading: false }
    case webReportSettingAddTypes.WEBREPROT_SEETING_POST_CLEAR:
      return { ...state, webReportSettingsAdd: null }
    default:
      return state
  }
}

export default webReportSettingsAddReducer


export const editwebReportSettingsInitialState = {
  list: null
}

export const editwebReportSettingsStoreKey = 'editwebReportSettingsStore'

export const editwebReportSettingsReducer = (state = editwebReportSettingsInitialState, action) => {
  switch (action.type) {
    case webReportSettingGetId.GET_WEBREPROT_SEETING_DETAILS:
      return { ...state, loading: true }
    case webReportSettingGetId.WEBREPROT_SEETING_DETAILS_RESPONSE:
      return { ...state, webReportSettingsIdDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case webReportSettingGetId.CLEAR_WEBREPROT_SEETING_DETAILS:
      return { ...state, webReportSettingsIdDetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const updatewebReportSettingInitialState = {
  data: null
}

export const updatewebReportSettingStoreKey = 'updatewebReportSettingStore'

export const updatewebReportSettingReducer = (state = updatewebReportSettingInitialState, action) => {
  switch (action.type) {
    case updatewebReportSettingActionsTypes.UPDATE_WEBREPROT_SEETING:
      return { ...state, loading: true }
    case updatewebReportSettingActionsTypes.SAVE_UPDATE_WEBREPROT_SEETING_RESPONSE:
      return {
        ...state,
        updatewebReportSettingResponce: action.data,
        loading: false
      }
    case updatewebReportSettingActionsTypes.CLEAR_UPDATE_WEBREPROT_SEETING:
      return { ...state, updatewebReportSettingResponce: null }
    default:
      return state
  }
}

export const DeletewebReportSettingInitialState = {
  data: null
}

export const DeletewebReportSettingStoreKey = 'DeletewebReportSettingStore'

export const DeletewebReportSettingReducer = (state = DeletewebReportSettingInitialState, action) => {
  switch (action.type) {
    case webReportSettingDelete.WEBREPROT_SEETING_DELETE:
      return { ...state, loading: true }
    case webReportSettingDelete.WEBREPROT_SEETING_DELETE_RESPONSE:
      return {
        ...state,
        DeletewebReportSetting: action.data,
        loading: false
      }
    case webReportSettingDelete.CLEAR_WEBREPROT_SEETING_DELETE:
      return { ...state, DeletewebReportSetting: null }
    default:
      return state
  }
}