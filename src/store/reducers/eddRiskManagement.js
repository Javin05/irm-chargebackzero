import {
  EddRiskManagementActionsTypes,
  AddEddRiskManagementTypes,
  eddMerchantIdDetailsTypes,
  BusinessTypeActionsTypes,
  BusinessAgeActionsTypes,
  BusinessNdxActionsTypes,
  PaymentInstrumentActionsTypes,
  EddExportActionsTypes,
  EddDashboardActionsTypes,
  EddApiDetailsActionsTypes,
  EddRiskManagementApiFinalListTypes,
  FinalDecisionUpdateActionsTypes,
  eddApiScheduledListTypes,
  eddApiScheduledListReportTypes,
  ReTriggerFinalApiUpdateActionsTypes,
  EDDUpdateActionsTypes,
  eddHistoryListActionTypes
} from '../actions'

export const EddRiskManagementInitialState = {
  data: null
}

export const EddRiskManagementStoreKey = 'EddRiskManagementStore'

export const EddRiskManagementReducer = (state = EddRiskManagementInitialState, action) => {
  switch (action.type) {
    case EddRiskManagementActionsTypes.GET_EDDRISKMANAGEMENT_LIST:
      return { ...state, loading: true }
    case EddRiskManagementActionsTypes.SAVE_EDDRISKMANAGEMENT_LIST_RESPONSE:
      return {
        ...state,
        EddRiskManagement: action.data,
        loading: false
      }
    case EddRiskManagementActionsTypes.CLEAR_EDDRISKMANAGEMENT_LIST:
      return { ...state, EddRiskManagement: null }
    default:
      return state
  }
}

export const AddEddRiskManagementInitialState = {
  data: null
}

export const AddEddRiskManagementStoreKey = 'AddEddRiskManagementStore'

export const AddEddRiskManagementReducer = (state = AddEddRiskManagementInitialState, action) => {
  switch (action.type) {
    case AddEddRiskManagementTypes.GET_ADD_EDD_RISKMANAGEMENT:
      return { ...state, loading: true }
    case AddEddRiskManagementTypes.SAVE_ADD_EDD_RISKMANAGEMENT_RESPONSE:
      return {
        ...state,
        AddEddRiskManagementRes: action.data,
        loading: false
      }
    case AddEddRiskManagementTypes.CLEAR_ADD_EDD_RISKMANAGEMENT:
      return { ...state, AddEddRiskManagementRes: null }
    default:
      return state
  }
}

export const editEddMerchantInitialState = {
  list: null
}

export const editEddMerchantStoreKey = 'editEddMerchantStore'

export const editEddMerchantReducer = (state = editEddMerchantInitialState, action) => {
  switch (action.type) {
    case eddMerchantIdDetailsTypes.GET_EDD_MERCHANT_ID:
      return { ...state, loadingEA: true }
    case eddMerchantIdDetailsTypes.EDD_MERCHANT_ID_RESPONSE:
      return { ...state, eddMerchantIddetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case eddMerchantIdDetailsTypes.CLEAR_EDD_MERCHANT_ID:
      return { ...state, eddMerchantIddetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const BusinessTypeInitialState = {
  data: null
}

export const BusinessTypeStoreKey = 'BusinessTypeStore'

export const BusinessTypeReducer = (state = BusinessTypeInitialState, action) => {
  switch (action.type) {
    case BusinessTypeActionsTypes.GET_BUSINESS_TYPE_LIST:
      return { ...state, loading: true }
    case BusinessTypeActionsTypes.SAVE_BUSINESS_TYPE_LIST_RESPONSE:
      return {
        ...state,
        BusinessType: action.data,
        loading: false
      }
    case BusinessTypeActionsTypes.CLEAR_BUSINESS_TYPE_LIST:
      return { ...state, BusinessType: null }
    default:
      return state
  }
}

export const BusinessAgeInitialState = {
  data: null
}

export const BusinessAgeStoreKey = 'BusinessAgeStore'

export const BusinessAgeReducer = (state = BusinessAgeInitialState, action) => {
  switch (action.type) {
    case BusinessAgeActionsTypes.GET_BUSINESS_AGE_LIST:
      return { ...state, loading: true }
    case BusinessAgeActionsTypes.SAVE_BUSINESS_AGE_LIST_RESPONSE:
      return {
        ...state,
        BusinessAge: action.data,
        loading: false
      }
    case BusinessAgeActionsTypes.CLEAR_BUSINESS_AGE_LIST:
      return { ...state, BusinessAge: null }
    default:
      return state
  }
}

export const BusinessNdxInitialState = {
  data: null
}

export const BusinessNdxStoreKey = 'BusinessNdxStore'

export const BusinessNdxReducer = (state = BusinessNdxInitialState, action) => {
  switch (action.type) {
    case BusinessNdxActionsTypes.GET_BUSINESS_NDX_LIST:
      return { ...state, loading: true }
    case BusinessNdxActionsTypes.SAVE_BUSINESS_NDX_LIST_RESPONSE:
      return {
        ...state,
        BusinessNdx: action.data,
        loading: false
      }
    case BusinessNdxActionsTypes.CLEAR_BUSINESS_NDX_LIST:
      return { ...state, BusinessNdx: null }
    default:
      return state
  }
}

export const PaymentInstrumentInitialState = {
  data: null
}

export const PaymentInstrumentStoreKey = 'PaymentInstrumentStore'

export const PaymentInstrumentReducer = (state = PaymentInstrumentInitialState, action) => {
  switch (action.type) {
    case PaymentInstrumentActionsTypes.GET_PAYMENT_INSTRUMENT_LIST:
      return { ...state, loading: true }
    case PaymentInstrumentActionsTypes.SAVE_PAYMENT_INSTRUMENT_LIST_RESPONSE:
      return {
        ...state,
        PaymentInstrument: action.data,
        loading: false
      }
    case PaymentInstrumentActionsTypes.CLEAR_PAYMENT_INSTRUMENT_LIST:
      return { ...state, PaymentInstrument: null }
    default:
      return state
  }
}

export const EddExportInitialState = {
  data: null
}

export const EddExportStoreKey = 'EddExportStore'

export const EddExportReducer = (state = EddExportInitialState, action) => {
  switch (action.type) {
    case EddExportActionsTypes.GET_EDD_EXPORT_LIST:
      return { ...state, loading: true }
    case EddExportActionsTypes.SAVE_EDD_EXPORT_LIST_RESPONSE:
      return {
        ...state,
        EddExport: action.data,
        loading: false
      }
    case EddExportActionsTypes.CLEAR_EDD_EXPORT_LIST:
      return { ...state, EddExport: null }
    default:
      return state
  }
}

export const EddDashboardInitialState = {
  list: null
}
export const EddDashboardStoreKey = 'EddDashboardStore'
export const EddDashboardReducer = (state = EddDashboardInitialState, action) => {
  switch (action.type) {
    case EddDashboardActionsTypes.GET_EDD_DASHBOARD_DETAILS:
      return { ...state, loading: true }
    case EddDashboardActionsTypes.EDD_DASHBOARD_RESPONSE:
      return { ...state, eddDashboardData: action.data, loading: false }
    case EddDashboardActionsTypes.CLEAR_EDD_DASHBOARD_DETAILS:
      return { ...state, eddDashboardData: null }
    default:
      return state
  }
}

export const EddApiDetailsActionsInitialState = {
  list: null
}
export const EddApiDetailStoreKey = 'ApiDetailStore'
export const EddApiDetailReducer = (state = EddApiDetailsActionsInitialState, action) => {
  switch (action.type) {
    case EddApiDetailsActionsTypes.GET_API_DETAILS:
      return { ...state, loading: true }
    case EddApiDetailsActionsTypes.EDD_API_DETAILS_RESPONSE:
      return { ...state, eddApiDetailsData: action.data, loading: false }
    case EddApiDetailsActionsTypes.CLEAR_API_DETAILS_DETAILS:
      return { ...state, eddApiDetailsData: null }
    default:
      return state
  }
}

export const EddApiFinalListInitialState = {
  data: null
}

export const EddApiFinalListStoreKey = 'EddApiFinalListStore'

export const EddApiFinalListReducer = (state = EddApiFinalListInitialState, action) => {
  switch (action.type) {
    case EddRiskManagementApiFinalListTypes.GET_EDD_API_FINAL_LIST:
      return { ...state, loading: true }
    case EddRiskManagementApiFinalListTypes.SAVE_EDD_API_FINAL_LIST_RESPONSE:
      return {
        ...state,
        AddEddApiFinalListRes: action.data,
        loading: false
      }
    case EddRiskManagementApiFinalListTypes.CLEAR_EDD_API_FINAL_LIST:
      return { ...state, AddEddApiFinalListRes: null }
    default:
      return state
  }
}

export const EddFinalDecisionUpdateInitialState = {
  data: null
}

export const EddFinalDecisionUpdateStoreKey = 'EddFinalDecisionUpdateStore'

export const EddFinalDecisionUpdateReducer = (state = EddFinalDecisionUpdateInitialState, action) => {
  switch (action.type) {
    case FinalDecisionUpdateActionsTypes.GET_FINAL_DECISION_UPDATE:
      return { ...state, loading: true }
    case FinalDecisionUpdateActionsTypes.SAVE_FINAL_DECISION_UPDATE_RESPONSE:
      return {
        ...state,
        AddEddFinalDecisionRes: action.data,
        loading: false
      }
    case FinalDecisionUpdateActionsTypes.CLEAR_FINAL_DECISION_UPDATE:
      return { ...state, AddEddFinalDecisionRes: null }
    default:
      return state
  }
}

export const editEddApiScheduledListInitialState = {
  list: null
}

export const editEddApiScheduledListStoreKey = 'editEddApiScheduledListStore'

export const editEddApiScheduledListReducer = (state = editEddApiScheduledListInitialState, action) => {
  switch (action.type) {
    case eddApiScheduledListTypes.GET_EDD_API_SCHEDULED_LIST_ID:
      return { ...state, loadingEA: true }
    case eddApiScheduledListTypes.EDD_API_SCHEDULED_LIST_ID_RESPONSE:
      return { ...state, eddApiScheduledListdetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case eddApiScheduledListTypes.CLEAR_EDD_API_SCHEDULED_LIST_ID:
      return { ...state, eddApiScheduledListdetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const editEddApiScheduledListReportInitialState = {
  list: null
}

export const editEddApiScheduledListReportStoreKey = 'editEddApiScheduledListReportStore'

export const editEddApiScheduledListReportReducer = (state = editEddApiScheduledListReportInitialState, action) => {
  switch (action.type) {
    case eddApiScheduledListReportTypes.GET_EDD_API_SCHEDULED_LIST_REPORT:
      return { ...state, loading: true }
    case eddApiScheduledListReportTypes.EDD_API_SCHEDULED_LIST_REPORT_RESPONSE:
      return { ...state, eddApiScheduledListReportdetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loading: false }
    case eddApiScheduledListReportTypes.CLEAR_EDD_API_SCHEDULED_LIST_REPORT:
      return { ...state, eddApiScheduledListReportdetail: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const EddReTriggerFinalApiUpdateInitialState = {
  data: null
}

export const EddReTriggerFinalApiUpdateStoreKey = 'EddReTriggerFinalApiUpdateStore'

export const EddReTriggerFinalApiUpdateReducer = (state = EddReTriggerFinalApiUpdateInitialState, action) => {
  switch (action.type) {
    case ReTriggerFinalApiUpdateActionsTypes.GET_RE_TRIGGER_FINAL_API_UPDATE:
      return { ...state, loading: true }
    case ReTriggerFinalApiUpdateActionsTypes.SAVE_RE_TRIGGER_FINAL_API_UPDATE_RESPONSE:
      return {
        ...state,
        ReTriggerFinalApiRes: action.data,
        loading: false
      }
    case ReTriggerFinalApiUpdateActionsTypes.CLEAR_RE_TRIGGER_FINAL_API_UPDATE:
      return { ...state, ReTriggerFinalApiRes: null }
    default:
      return state
  }
}

export const EddUpdateInitialState = {
  data: null
}

export const EddUpdateStoreKey = 'EddUpdateStore'

export const EddUpdateReducer = (state = EddUpdateInitialState, action) => {
  switch (action.type) {
    case EDDUpdateActionsTypes.GET_EDD_UPDATE:
      return { ...state, loading: true }
    case EDDUpdateActionsTypes.SAVE_EDD_UPDATE_RESPONSE:
      return {
        ...state,
        EddUpdateRes: action.data,
        loading: false
      }
    case EDDUpdateActionsTypes.CLEAR_EDD_UPDATE:
      return { ...state, EddUpdateRes: null }
    default:
      return state
  }
}

export const eddHistoryListInitialState = {
  list: null
}

export const eddHistoryListStoreKey = 'eddHistoryListStore'

export const eddHistoryListReducer = (state = eddHistoryListInitialState, action) => {
  switch (action.type) {
    case eddHistoryListActionTypes.GET_EDD_HISTORY_LIST:
      return { ...state, loading: true }
    case eddHistoryListActionTypes.EDD_HISTORY_LIST_RESPONSE:
      return { ...state, eddHistoryList: action.data, status: action.data.status, message: action.data.message, loading: false }
    case eddHistoryListActionTypes.CLEAR_EDD_HISTORY_LIST:
      return { ...state, eddHistoryList: null, message: '', status: '' }
    default:
      return state
  }
}