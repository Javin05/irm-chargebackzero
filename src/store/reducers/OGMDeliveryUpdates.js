import { getOGMDeliveryUpdateTypes, addOGMDeliveryUpdateTypes, OGMDeliveryUpdateIdTypes, updateOGMDeliveryUpdateActionsTypes, OGMDeliveryUpdatedeleteActionsTypes } from '../actions'
  
export const OGMDeliveryUpdateInitialState = {
  list: null
}

export const OGMDeliveryUpdateStoreKey = 'OGMDeliveryUpdateStore'

export const OGMDeliveryUpdateReducer = (state = OGMDeliveryUpdateInitialState, action) => {
  switch (action.type) {
    case getOGMDeliveryUpdateTypes.GET_OGM_DELIVERY_UPDATES_LIST:
      return { ...state, loading: true }
    case getOGMDeliveryUpdateTypes.SAVE_OGM_DELIVERY_UPDATES_LIST_RESPONSE:
      return { ...state, OGMDeliveryUpdatelists: action.data, loading: false }
    case getOGMDeliveryUpdateTypes.CLEAR_OGM_DELIVERY_UPDATES_LIST:
      return { ...state, OGMDeliveryUpdatelists: null }
    default:
      return state
  }
}

export const addOGMDeliveryUpdateInitialState = {
  list: null
}
export const addOGMDeliveryUpdateStoreKey = 'addOGMDeliveryUpdateStore'

export const addOGMDeliveryUpdateReducer = (state = addOGMDeliveryUpdateInitialState, action) => {
  switch (action.type) {
    case addOGMDeliveryUpdateTypes.REQUEST:
      return { ...state, loading: true }
    case addOGMDeliveryUpdateTypes.RESPONSE:
      return { ...state, ogmDeliveryUpdatePost: action.data, loading: false }
    case addOGMDeliveryUpdateTypes.CLEAR:
      return { ...state, ogmDeliveryUpdatePost: null }
    default:
      return state
  }
}

export const OGMDeliveryUpdateIDInitialState = {
  list: null,
}
export const OGMDeliveryUpdateIDStoreKey = "OGMDeliveryUpdateIDStore"

export const OGMDeliveryUpdateIDReducer = (state = OGMDeliveryUpdateIDInitialState, action) => {
  switch (action.type) {
    case OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS:
      return { ...state, loading: true }
    case OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS_RESPONSE:
      return { ...state, OGMDeliveryUpdateIdData: action.data, loading: false }
    case OGMDeliveryUpdateIdTypes.OGM_DELIVERY_UPDATES_GET_DETAILS_CLEAR:
      return { ...state, OGMDeliveryUpdateIdData: null }
    default:
      return state
  }
}

export const updateOGMDeliveryUpdateInitialState = {
    data: null
  }
  
  export const updateOGMDeliveryUpdateStoreKey = 'updateOGMDeliveryUpdateStore'
  
  export const updateOGMDeliveryUpdateReducer = (state = updateOGMDeliveryUpdateInitialState, action) => {
    switch (action.type) {
      case updateOGMDeliveryUpdateActionsTypes.UPDATE_OGM_DELIVERY_UPDATES_SUMMARY:
        return { ...state, loading: true }
      case updateOGMDeliveryUpdateActionsTypes.SAVE_UPDATE_OGM_DELIVERY_UPDATES_RESPONSE:
        return {
          ...state,
          saveupdateOGMDeliveryUpdateResponse: action.data,
          loading: false
        }
      case updateOGMDeliveryUpdateActionsTypes.CLEAR_UPDATE_OGM_DELIVERY_UPDATES:
        return { ...state, saveupdateOGMDeliveryUpdateResponse: null }
      default:
        return state
    }
  }

export const DeleteOGMDeliveryUpdateInitialState = {
      data: null
    }
export const OGMDeliveryUpdatedeleteStoreKey = 'OGMDeliveryUpdatedeleteStore'
    
export const OGMDeliveryUpdatedeleteReducer = (state = DeleteOGMDeliveryUpdateInitialState, action) => {
      switch (action.type) {
        case OGMDeliveryUpdatedeleteActionsTypes.REQUEST:
          return { ...state, loading: true }
        case OGMDeliveryUpdatedeleteActionsTypes.RESPONSE:
          return { ...state, DeleteODMDeliveryUpdateData: action.data, loading: false }
        case OGMDeliveryUpdatedeleteActionsTypes.CLEAR:
          return { ...state, DeleteODMDeliveryUpdateData: null }
        default:
          return state
      }
    }