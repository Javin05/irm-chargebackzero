import {
  KYCDashboardSummaryTypes,
  KYCDashboardSummaryAction,
} from "../actions";

export const KYCDashboardSummaryStoreKey = "KYCDashboardSummaryStoreKey";

export const KYCDashboardSummaryInitialState = {
  list: null,
};

export const KYCDashboardSummaryReducers = (
  state = KYCDashboardSummaryInitialState,
  action
) => {
  switch (action.type) {
    case KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_INIT:
      return { ...state, loading: true };
    case KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_SUCCESS:
      return { ...state, KYCDashboardSummary: action.data, loading: false };
    case KYCDashboardSummaryTypes.KYC_SUMMARY_DASHBOARD_CLEAR:
      return { ...state, KYCDashboardSummary: null };
    default:
      return state;
  }
};
