import {
  preventionAlertActionsTypes,
  dashboardGetDetailsTypes,
  dashboardGetDetailsPdfTypes,
  editAlertsTypes,
  dashboardFilterTypes,
  dashboardTransactionTypes,
  getWebsiteIdTypes
} from '../actions'

export const preventionInitialState = {
  list: null
}

export const dashboardStoreKey = 'dashboardStore'
export const preventionReducer = (state = preventionInitialState, action) => {
  switch (action.type) {
    case preventionAlertActionsTypes.GET_PREVENTIONALERT:
      return { ...state, loadingPA: true }
    case preventionAlertActionsTypes.SAVE_PREVENTIONALERT_RESPONSE:
      return {
        ...state,
        preventionAlertList: action.data && action.data.data,
        statusPA: action.data && action.data.status,
        messagesPA: action.data && action.data.message,
        count: action.data && action.data.count,
        loadingPA: false
      }
    case preventionAlertActionsTypes.CLEAR_PREVENTIONALERT:
      return { ...state, preventionAlertList: null, statusPA: '', messagesPA: '' }

    case dashboardGetDetailsTypes.GET_DASHBOARD_DETAILS:
      return { ...state, loadingPAD: true }
    case dashboardGetDetailsTypes.DASHBOARD_DETAILS_RESPONSE:
      return {
        ...state,
        dashboardDetails: action.data,
        statusPAD: action.data && action.data.status,
        messagesPAD: action.data && action.data.message,
        loadingPAD: false
      }
    case dashboardGetDetailsTypes.CLEAR_DASHBOARD_DETAILS:
      return { ...state, statusPAD: '', messagesPAD: '' }
    default:
      return state
  }
}

export const preventionInitialPdfState = {
  list: null
}

export const dashboardPdfStoreKey = 'dashboardPdfStore'
export const preventionPdfReducer = (state = preventionInitialPdfState, action) => {
  switch (action.type) {
    case dashboardGetDetailsPdfTypes.GET_DASHBOARD_DETAILSPDF:
      return { ...state, loadingPAD: true }
    case dashboardGetDetailsPdfTypes.DASHBOARD_DETAILSPDF_RESPONSE:
      return {
        ...state,
        dashboardDetails: action.data,
        statusPAD: action.data && action.data.status,
        messagesPAD: action.data && action.data.message,
        loadingPAD: false
      }
    case dashboardGetDetailsPdfTypes.CLEAR_DASHBOARD_DETAILSPDF:
      return { ...state, statusPAD: '', messagesPAD: '' }
    default:
      return state
  }
}

export const editAlertsStoreKey = 'editAlertsStore'
export const editAlertsReducer = (state = {}, action) => {
  switch (action.type) {
    case editAlertsTypes.REQUEST:
      return { ...state, loadingEA: true }
    case editAlertsTypes.RESPONSE:
      return { ...state, dataEA: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
    case editAlertsTypes.CLEAR:
      return { ...state, dataEA: null, messageEA: '', statusEA: '' }
    default:
      return state
  }
}

export const dashboardFilterStore = 'dashboardFilterStore'
export const dashboardFilterReducer = (state = {}, action) => {
  switch (action.type) {
    case dashboardFilterTypes.DASHBOARD_FILTER_INIT:
      return {
        ...state,
        dashboard_loading: true
      }
    case dashboardFilterTypes.DASHBOARD_FILTER_SUCCESS:
      return {
        ...state,
        dashboard_loading: false,
        dashboard_data: action.data
      }
    case dashboardFilterTypes.DASHBOARD_FILTER_ERROR:
      return {
        ...state,
        dashboard_loading: false,
        dashboard_data: null
      }
    case dashboardFilterTypes.DASHBOARD_FILTER_CLEAR:
      return {
        ...state,
        dashboard_loading: false,
        dashboard_data: null
      }
    default:
      return state
  }
}

export const dashboardTransactionStore = 'dashboardTransactionStore'
export const dashboardTransactionReducer = (state = {}, action) => {
  switch (action.type) {
    case dashboardTransactionTypes.DASHBOARD_TRANSACTION_INIT:
      return {
        ...state,
        transaction_loading: true
      }
    case dashboardTransactionTypes.DASHBOARD_TRANSACTION_SUCCESS:
      return {
        ...state,
        transaction_loading: false,
        transaction_data: action.data
      }
    case dashboardTransactionTypes.DASHBOARD_TRANSACTION_ERROR:
      return {
        ...state,
        transaction_loading: false,
        transaction_data: null
      }
    default:
      return state
  }
}

export const getWebsiteIdStoreKey = 'getWebsiteIdStore'
export const getWebsiteIdReducer = (state = {}, action) => {
  switch (action.type) {
    case getWebsiteIdTypes.GET_WEBSITEID:
      return {
        ...state,
        loading: true
      }
    case getWebsiteIdTypes.GET_WEBSITEID_SUCCESS:
      return {
        ...state,
        loading: false,
        getWebsiteIdData: action.data
      }
    case getWebsiteIdTypes.GET_WEBSITEID_ERROR:
      return {
        ...state,
        loading: false,
        getWebsiteIdData: null
    }
    default:
      return state
  }
}