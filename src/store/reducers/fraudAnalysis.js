import { 
  FileUploadActionType, 
  FraudAnalysisActionType, 
  FraudToolDetailsActionType, 
  FraudToolFileUploadActionType, 
  FraudToolListActionType, 
  FraudAnalysisBulkUploadActionType ,
  FraudAnalysisSummaryListActionType,
  FraudAnalysisExportActionType,
  FraudAnalysisTagListActionType
} from "../actions"

export const fraudAnalysisStoreKey = 'fruadAnalysisStore'

export const fraudAnalysisReducer = (state = {}, action) => {
  const res = action.data && action.data
  switch (action.type) {
    case FraudAnalysisActionType.REQUEST:
      return { ...state, loadingFAT: true }
    case FraudAnalysisActionType.RESPONSE:
      return {
        ...state,
        fraudAnalysisStatus: res.status,
        fraudAnalysisMessage: res.message,
        loadingFAT: false
      }
    case FraudAnalysisActionType.CLEAR:
      return { ...state, fraudAnalysis: null, fraudAnalysisStatus: '', fraudAnalysisMessage: '' }
    default:
      return state
  }
}


export const  FraudAnalysisDocumetUploaInitialState = {
  data: null,
}
export const FraudAnalysisDocumetUploadStoreKey = "FraudAnalysisDocumetUploadStore"

export const FraudAnalysisDocumetReducer = (state = FraudAnalysisDocumetUploaInitialState, action) => {
  switch (action.type) {
    case FraudToolFileUploadActionType.REQUEST:
      return { ...state, loading: true }
    case FraudToolFileUploadActionType.RESPONSE:
      return { ...state, fileLink: action.data, loading: false }
    case FraudToolFileUploadActionType.CLEAR:
      return { ...state, fileLink: null }
    default:
      return state
  }
}

export const FraudAnalysisListInitialState = {
  data: null
}

export const FraudAnalysisListStoreKey = 'FraudAnalysisListManagementStore'

export const FraudToolListReducer = (state = FraudAnalysisListInitialState, action) => {
  switch (action.type) {
    case FraudToolListActionType.REQUEST:
      return { ...state, loading: true }
    case FraudToolListActionType.RESPONSE:
      return {
        ...state,
        fraudToolData: action.data,
        loading: false
      }
    case FraudToolListActionType.CLEAR:
      return { ...state, fraudToolData: null }
    default:
      return state
  }
}

export const FraudAnalysisDetailStoreKey = 'FraudAnalysisDetailManagementStore'

export const FraudToolDetailReducer = (state = {}, action) => {
  switch (action.type) {
    case FraudToolDetailsActionType.REQUEST:
      return { ...state, loading: true }
    case FraudToolDetailsActionType.RESPONSE:
      return {
        ...state,
        fraudToolDetailData: action.data,
        loading: false
      }
    case FraudToolDetailsActionType.CLEAR:
      return { ...state, fraudToolDetailData: null }
    default:
      return state
  }
}

export const FraudAnalysisBulkUploadInitialState = {
  data: null
}

export const FraudAnalysisBulkUploadStoreKey = 'FraudAnalysisBulkUploadStore'

export const FraudAnalysisBulkUploadReducer = (state = FraudAnalysisBulkUploadInitialState, action) => {
  switch (action.type) {
    case FraudAnalysisBulkUploadActionType.REQUEST:
      return { ...state, loading: true }
    case FraudAnalysisBulkUploadActionType.RESPONSE:
      return {
        ...state,
        FraudAnalysisBulkUploadRes: action.data,
        loading: false,
      }
    case FraudAnalysisBulkUploadActionType.CLEAR:
      return { ...state, FraudAnalysisBulkUploadRes: null }
    default:
      return state
  }
}

export const FraudAnalysisSummaryListInitialState = {
  data: null
}

export const FraudAnalysisSummaryListStoreKey = 'FraudAnalysisSummaryListStore'

export const FraudAnalysisSummaryListReducer = (state = FraudAnalysisSummaryListInitialState, action) => {
  switch (action.type) {
    case FraudAnalysisSummaryListActionType.REQUEST:
      return { ...state, loading: true }
    case FraudAnalysisSummaryListActionType.RESPONSE:
      return {
        ...state,
        fraudAnalysisSummaryData: action.data,
        loading: false
      }
    case FraudAnalysisSummaryListActionType.CLEAR:
      return { ...state, fraudAnalysisSummaryData: null }
    default:
      return state
  }
}

export const FraudAnalysisExportStoreKey = 'FraudAnalysisExportStore'

export const FraudAnalysisExportReducer = (state = {}, action) => {
  switch (action.type) {
    case FraudAnalysisExportActionType.REQUEST:
      return { ...state, loading: true }
    case FraudAnalysisExportActionType.RESPONSE:
      return {
        ...state,
        fraudAnalysisExportData: action.data,
        loading: false
      }
    case FraudAnalysisExportActionType.CLEAR:
      return { ...state, fraudAnalysisExportData: null }
    default:
      return state
  }
}

export const FraudAnalysisTagListInitialState = {
  data: null
}

export const FraudAnalysisTagListStoreKey = 'FraudAnalysisTagListStore'

export const FraudAnalysisTagListReducer = (state = FraudAnalysisTagListInitialState, action) => {
  switch (action.type) {
    case FraudAnalysisTagListActionType.REQUEST:
      return { ...state, loading: true }
    case FraudAnalysisTagListActionType.RESPONSE:
      return {
        ...state,
        fraudAnalysisTagData: action.data,
        loading: false
      }
    case FraudAnalysisTagListActionType.CLEAR:
      return { ...state, fraudAnalysisTagData: null }
    default:
      return state
  }
}