import {
    OgmForceUploadSummaryTypes
} from "../actions"

export const OgmForceUploadSummaryStoreKey = "OgmForceUploadSummaryStore"

export const OgmForceUploadSummaryInitialState = {
    list: null,
}

export const OgmForceUploadSummaryReducers = (
    state = OgmForceUploadSummaryInitialState,
    action
) => {
    switch (action.type) {
        case OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST:
            return { ...state, loading: true }
        case OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST_SUCCESS:
            return { ...state, OgmForceUploadSummaryData: action.data, loading: false }
        case OgmForceUploadSummaryTypes.OGM_FORCE_UPLOAD_SUMMARYLIST_CLEAR:
            return { ...state, OgmForceUploadSummaryData: null }
        default:
            return state
    }
}