import { ClientExportTypes } from '../actions'
  
export const ClientExportInitialState = {
  list: null
}

export const ClientExportStoreKey = 'ClientExportStore'

export const ClientExportReducer = (state = ClientExportInitialState, action) => {
  switch (action.type) {
    case ClientExportTypes.GET_CLIENT_EXPORT_LIST:
      return { ...state, loading: true }
    case ClientExportTypes.SAVE_CLIENT_EXPORT_LIST_RESPONSE:
      return { ...state, ClientExports: action.data, loading: false }
    case ClientExportTypes.CLEAR_CLIENT_EXPORT_LIST:
      return { ...state, ClientExports: null }
    default:
      return state
  }
}