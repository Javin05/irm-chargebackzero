import {
  riskSummaryTypes,
 GetAsigneeTypes,
 AsiggnActionsTypes,
 riskScoreTypes,
 PostCategoryTypes,
 GetCategoryTypes,
 PostCategory1Types,
 PostPMATypes,
 matchAnalysisTypes,
 PostProposedCategoryTypes
} from '../actions'

export const riskSummaryInitialState = {
  list: null
}

export const riskSummaryStoreKey = 'riskSummaryStore'

export const riskSummaryReducer = (state = riskSummaryInitialState, action) => {
  switch (action.type) {
    case riskSummaryTypes.GET_RISK_SUMMARY:
      return { ...state, loading: true }
    case riskSummaryTypes.SAVE_RISK_SUMMARY_RESPONSE:
      return { ...state, getRiskSummarys: action.data, loading: false }
    case riskSummaryTypes.CLEAR_RISK_SUMMARY:
      return { ...state, getRiskSummarys: null }
    default:
      return state
  }
}

export const matchAnalysisInitialState = {
  list: null
}

export const matchAnalysisStoreKey = 'matchAnalysisStore'

export const matchAnalysisReducer = (state = matchAnalysisInitialState, action) => {
  switch (action.type) {
    case matchAnalysisTypes.GET_MATCH_ANALYSIS:
      return { ...state, loading: true }
    case matchAnalysisTypes.SAVE_MATCH_ANALYSIS_RESPONSE:
      return { ...state, getMatchAnalysis: action.data, loading: false }
    case matchAnalysisTypes.CLEAR_MATCH_ANALYSIS:
      return { ...state, getMatchAnalysis: null }
    default:
      return state
  }
}

export const postAsigneeInitialState = {
  list: null
}

export const postAsigneeStoreKey = 'postAsigneeStore'

export const postAsigneeReducer = (state = postAsigneeInitialState, action) => {
  switch (action.type) {
    case GetAsigneeTypes.GET_ASSINEE:
      return { ...state, loading: true }
    case GetAsigneeTypes.SAVE_ASSINEE_RESPONSE:
      return { ...state, getAssignee: action.data, loading: false }
    case GetAsigneeTypes.CLEAR_ASSINEE:
      return { ...state, getAssignee: null }
    default:
      return state
  }
}

export const updateAssignInitialState = {
  list: null
}

export const updateAssignStoreKey = 'updateAssignStore'

export const updateAssignReducer = (state = updateAssignInitialState, action) => {
  switch (action.type) {
    case AsiggnActionsTypes.ASSIGN:
      return { ...state, loading: true }
    case AsiggnActionsTypes.SAVE_ASSIGN_RESPONSE:
      return { ...state, updateAssign: action.data, loading: false }
    case AsiggnActionsTypes.CLEAR_ASSIGN:
      return { ...state, updateAssign: null }
    default:
      return state
  }
}

export const riskScoreInitialState = {
  list: null
}

export const riskScoreStoreKey = 'riskScoreStore'

export const riskScoreReducer = (state = riskScoreInitialState, action) => {
  switch (action.type) {
    case riskScoreTypes.GET_RISK_SCORE:
      return { ...state, loading: true }
    case riskScoreTypes.SAVE_RISK_SCORE_RESPONSE:
      return { ...state, getriskScores: action.data, loading: false }
    case riskScoreTypes.CLEAR_RISK_SCORE:
      return { ...state, getriskScores: null }
    default:
      return state
  }
}

export const PostCategoryInitialState = {
  list: null
}

export const PostCategoryStoreKey = 'PostCategoryStore'

export const PostCategoryReducer = (state = PostCategoryInitialState, action) => {
  switch (action.type) {
    case PostCategoryTypes.POST_CATEGORY:
      return { ...state, loading: true }
    case PostCategoryTypes.SAVE_CATEGORY_RESPONSE:
      return { ...state, PostCategory: action, loading: false }
    case PostCategoryTypes.CLEAR_CATEGORY:
      return { ...state, PostCategory: null }
    default:
      return state
  }
}

export const PostCategory1InitialState = {
  list: null
}

export const PostCategory1StoreKey = 'PostCategoryStore1'

export const PostCategory1Reducer = (state = PostCategory1InitialState, action) => {
  switch (action.type) {
    case PostCategory1Types.POST_CATEGORY1:
      return { ...state, loading: true }
    case PostCategory1Types.SAVE_CATEGORY1_RESPONSE:
      return { ...state, postCategory1Res: action, loading: false }
    case PostCategory1Types.CLEAR_CATEGORY:
      return { ...state, postCategory1Res: null }
    default:
      return state
  }
}

export const PostProposedCategoryInitialState = {
  list: null
}

export const PostProposedCategoryStoreKey = 'PostProposedCategoryStore'

export const PostProposedCategoryReducer = (state = PostProposedCategoryInitialState, action) => {
  switch (action.type) {
    case PostProposedCategoryTypes.POST_PROPOSED_CATEGORY:
      return { ...state, loading: true }
    case PostProposedCategoryTypes.SAVE_PROPOSED_CATEGORY_RESPONSE:
      return { ...state, postProposedCategoryRes: action, loading: false }
    case PostProposedCategoryTypes.CLEAR_PROPOSED_CATEGORY:
      return { ...state, postProposedCategoryRes: null }
    default:
      return state
  }
}

export const GetCategoryInitialState = {
  list: null
}

export const GetCategoryStoreKey = 'GetCategoryStore'

export const GetCategoryReducer = (state = GetCategoryInitialState, action) => {
  switch (action.type) {
    case GetCategoryTypes.GET_CATEGORY:
      return { ...state, loading: true }
    case GetCategoryTypes.SAVE_GET_CATEGORY_RESPONSE:
      return { ...state, getCategory: action, loading: false }
    case GetCategoryTypes.CLEAR_GET_CATEGORY:
      return { ...state, getCategory: null }
    default:
      return state
  }
}

export const PostPMAInitialState = {
  list: null
}

export const PostPMAStoreKey = 'PostPMAStore'

export const PostPMAReducer = (state = PostPMAInitialState, action) => {
  switch (action.type) {
    case PostPMATypes.POST_PMA:
      return { ...state, loading: true }
    case PostPMATypes.SAVE_PMA_RESPONSE:
      return { ...state, postPMAres: action, loading: false }
    case PostPMATypes.CLEAR_PMA:
      return { ...state, postPMAres: null }
    default:
      return state
  }
}