import { AccountRiskCommentTypes, updateAccountRiskCommentActionsTypes, AccountRiskCommentdeleteActionsTypes } from '../actions'
  
  export const AccountRisklistInitialState = {
    list: null
  }
  
  export const AccountRisklistStoreKeys = 'AccountRisklistStore'
  
  export const AccountRisklistReducers = (state = AccountRisklistInitialState, action) => {
    switch (action.type) {
      case AccountRiskCommentTypes.GET_ACCOUNTRISK_COMMENT_LIST:
        return { ...state, loading: true }
      case AccountRiskCommentTypes.SAVE_ACCOUNTRISK_COMMENT_LIST_RESPONSE:
        return { ...state, AccountRisklists: action.data, loading: false }
      case AccountRiskCommentTypes.CLEAR_ACCOUNTRISK_COMMENT_LIST:
        return { ...state, AccountRisklists: null }
      default:
        return state
    }
  }

  export const updateAccountRiskInitialState = {
    data: null
  }
  
  export const updateAccountRiskStoreKey = 'updateAccountRiskStore'
  
  export const updateAccountRiskReducer = (state = updateAccountRiskInitialState, action) => {
    switch (action.type) {
      case updateAccountRiskCommentActionsTypes.UPDATE_ACCOUNTRISK_COMMENT:
        return { ...state, loading: true }
      case updateAccountRiskCommentActionsTypes.SAVE_UPDATE_ACCOUNTRISK_COMMENT_RESPONSE:
        return {
          ...state,
          saveupdateAccountRiskResponse: action.data,
          loading: false
        }
      case updateAccountRiskCommentActionsTypes.CLEAR_UPDATE_ACCOUNTRISK_COMMENT:
        return { ...state, saveupdateAccountRiskResponse: null }
      default:
        return state
    }
  }

  export const DeleteAccountRiskInitialState = {
    DeleteAccountRiskData: null
  }
  export const AccountRiskdeleteStoreKey = 'AccountRiskdeleteStore'
  
  export const AccountRiskdeleteReducer = (state = DeleteAccountRiskInitialState, action) => {
    switch (action.type) {
      case AccountRiskCommentdeleteActionsTypes.REQUEST:
        return { ...state, loading: true }
      case AccountRiskCommentdeleteActionsTypes.RESPONSE:
        return { ...state, DeleteAccountRiskData: action.data, loading: false }
      case AccountRiskCommentdeleteActionsTypes.CLEAR:
        return { ...state, DeleteAccountRiskData: null }
      default:
        return state
    }
  }