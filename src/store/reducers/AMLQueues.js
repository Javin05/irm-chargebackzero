import { 
    AMLqueueTypes,
    AMLPostTypes,
    ApproveAMLActionsTypes,
    AMLGetIdTypes,
    TransactionAMLActionsTypes,
    AMLCountActionsTypes,
    AMLPercentageActionsTypes,
    AMLWMYActionsTypes,
    AMLAnnatationActionsTypes
} from '../actions'
  
  export const AMLgettInitialState = {
    list: null
  }
  
  export const AMLgetStoreKeys = 'AMLgetStore'
  
  export const AMLgetReducers = (state = AMLgettInitialState, action) => {
    switch (action.type) {
      case AMLqueueTypes.GET_AML_LIST:
        return { ...state, loading: true }
      case AMLqueueTypes.SAVE_AML_RESPONSE:
        return { ...state, AMLgetList: action.data, loading: false }
      case AMLqueueTypes.CLEAR_AML_LIST:
        return { ...state, AMLgetList: null }
      default:
        return state
    }
  }

  export const AMLpostInitialState = {
    list: null
  }
  
  export const AMLpostStoreKeys = 'AMLpostTypeStore'
  
  export const AMLpostReducers = (state = AMLpostInitialState, action) => {
    switch (action.type) {
      case AMLPostTypes.AMLPOST_LIST:
        return { ...state, loading: true }
      case AMLPostTypes.SAVE_AMLPOST_LIST_RESPONSE:
        return { ...state, AMLpost: action.data, loading: false }
      case AMLPostTypes.CLEAR_AMLPOST_LIST:
        return { ...state, AMLpost: null }
      default:
        return state
    }
  }

  export const ApproveAMLInitialState = {
    data: null
  }
  
  export const ApproveAMLStoreKey = 'ApproveAMLStore'
  
  export const ApproveAMLReducer = (state = ApproveAMLInitialState, action) => {
    switch (action.type) {
      case ApproveAMLActionsTypes.APPROVE_AML:
        return { ...state, loading: true }
      case ApproveAMLActionsTypes.SAVE_APPROVE_AML_RESPONSE:
        return {
          ...state,
          ApproveAMLResponce: action.data,
          loading: false
        }
      case ApproveAMLActionsTypes.CLEAR_APPROVE_AML:
        return { ...state, ApproveAMLResponce: null }
      default:
        return state
    }
  }

  export const editAMLInitialState = {
    list: null
  }
  
  export const editAMLStoreKey = 'editAMLStore'
  
  export const editAMLReducer = (state = editAMLInitialState, action) => {
    switch (action.type) {
      case AMLGetIdTypes.GET_AML_DETAILS:
        return { ...state, loadingEA: true }
      case AMLGetIdTypes.AML_DETAILS_RESPONSE:
        return { ...state, AMLIdDetail: action.data, statusEA: action.data.status, messageEA: action.data.message, loadingEA: false }
      case AMLGetIdTypes.CLEAR_AML_DETAILS:
        return { ...state, AMLIdDetail: null, messageEA: '', statusEA: '' }
      default:
        return state
    }
  }

  export const TransactionAMLInitialState = {
    data: null
  }
  
  export const TransactionStatusAMLStoreKey = 'TransactionAMLStore'
  
  export const TransactionStatusAMLReducer = (state = TransactionAMLInitialState, action) => {
    switch (action.type) {
      case TransactionAMLActionsTypes.TRANSACTION_AML:
        return { ...state, loading: true }
      case TransactionAMLActionsTypes.SAVE_TRANSACTION_AML_RESPONSE:
        return {
          ...state,
          TransactionAMLResponce: action.data,
          loading: false
        }
      case TransactionAMLActionsTypes.CLEAR_TRANSACTION_AML:
        return { ...state, TransactionAMLResponce: null }
      default:
        return state
    }
  }

  export const CountAMLInitialState = {
    data: null
  }
  
  export const CountAMLStoreKey = 'CountAMLStore'
  
  export const CountAMLReducer = (state = CountAMLInitialState, action) => {
    switch (action.type) {
      case AMLCountActionsTypes.COUNT_AML:
        return { ...state, loading: true }
      case AMLCountActionsTypes.SAVE_COUNT_AML_RESPONSE:
        return {
          ...state,
          CountAMLResponce: action.data,
          loading: false
        }
      case AMLCountActionsTypes.CLEAR_COUNT_AML:
        return { ...state, CountAMLResponce: null }
      default:
        return state
    }
  }

  export const PercentageAMLInitialState = {
    data: null
  }
  
  export const PercentageAMLStoreKey = 'PercentageAMLStore'
  
  export const PercentageAMLReducer = (state = PercentageAMLInitialState, action) => {
    switch (action.type) {
      case AMLPercentageActionsTypes.PERCENTAGE_AML:
        return { ...state, loading: true }
      case AMLPercentageActionsTypes.SAVE_PERCENTAGE_AML_RESPONSE:
        return {
          ...state,
          PercentageAMLResponce: action.data,
          loading: false
        }
      case AMLPercentageActionsTypes.CLEAR_PERCENTAGE_AML:
        return { ...state, PercentageAMLResponce: null }
      default:
        return state
    }
  }

  export const AMLWMYAInitialState = {
    data: null
  }
  
  export const AMWMYAStoreKey = 'AMLWMYAStore'
  
  export const AMLWMYReducer = (state = AMLWMYAInitialState, action) => {
    switch (action.type) {
      case AMLWMYActionsTypes.WMY_AML:
        return { ...state, loading: true }
      case AMLWMYActionsTypes.SAVE_WMY_AML_RESPONSE:
        return {
          ...state,
          WMYResponce: action.data,
          loading: false
        }
      case AMLWMYActionsTypes.CLEAR_WMY_AML:
        return { ...state, WMYResponce: null }
      default:
        return state
    }
  }

  export const AMLAnnatationAInitialState = {
    data: null
  }
  
  export const AMAnnatationAStoreKey = 'AMLAnnatationAStore'
  
  export const AMLAnnatationReducer = (state = AMLAnnatationAInitialState, action) => {
    switch (action.type) {
      case AMLAnnatationActionsTypes.ANNATATION_AML:
        return { ...state, loading: true }
      case AMLAnnatationActionsTypes.SAVE_ANNATATION_AML_RESPONSE:
        return {
          ...state,
          AnnatationData: action.data,
          loading: false
        }
      case AMLAnnatationActionsTypes.CLEAR_ANNATATION_AML:
        return { ...state, AnnatationData: null }
      default:
        return state
    }
  }