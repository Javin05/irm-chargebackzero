/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC, useEffect } from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { unsetLocalStorage } from '../../../../utils/helper';
import { connect } from 'react-redux'
import { LogoutActions } from '../../../../store/actions';
import { RESPONSE_STATUS } from '../../../../utils/constants';
interface ConfirmationModalProps {
  logoutDispatch: any, 
  clearLogout: any, 
  logoutData: any, 
  loading: any
}
const HeaderUserMenu: FC <ConfirmationModalProps>= (props) => {
  const {logoutDispatch, clearLogout, logoutData, loading} = props
  const dispatch = useDispatch();
  const logoutStatus = logoutData && logoutData.status
  const logout = () => {
    logoutDispatch()
  };

  useEffect(() => {
    if (logoutStatus === RESPONSE_STATUS.SUCCESS) {
      unsetLocalStorage()
      window.location.href = '/';
    }else if(logoutStatus === RESPONSE_STATUS.ERROR){
      unsetLocalStorage()
      window.location.href = '/';
    }
  }, [logoutStatus])

  return (
    <div
      className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
      data-kt-menu="true"
    >
      {/* <div className="menu-item px-3">
        <div className="menu-content d-flex align-items-center px-3">
          <div className="symbol symbol-50px me-5">
            <img alt="Logo" src={''} />
          </div>

          <div className="d-flex flex-column">
            <div className="fw-bolder d-flex align-items-center fs-5">
              David
              <span className="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">
                Pro
              </span>
            </div>
            <a href="#" className="fw-bold text-muted text-hover-primary fs-7">
              abc@gmail.com
            </a>
          </div>
        </div>
      </div>

      <div className="separator my-2" />

      <div className="menu-item px-5">
        <Link to={'/crafted/pages/profile'} className="menu-link px-5">
          My Profile
        </Link>
      </div>

      <div className="menu-item px-5">
        <a href="#" className="menu-link px-5">
          <span className="menu-text">My Projects</span>
          <span className="menu-badge">
            <span className="badge badge-light-danger badge-circle fw-bolder fs-7">
              3
            </span>
          </span>
        </a>
      </div>

      <div
        className="menu-item px-5"
        data-kt-menu-trigger="hover"
        data-kt-menu-placement="left-start"
        data-kt-menu-flip="bottom"
      >
        <a href="#" className="menu-link px-5">
          <span className="menu-title">My Subscription</span>
          <span className="menu-arrow" />
        </a>

        <div className="menu-sub menu-sub-dropdown w-175px py-4">
          <div className="menu-item px-3">
            <a href="#" className="menu-link px-5">
              Referrals
            </a>
          </div>

          <div className="menu-item px-3">
            <a href="#" className="menu-link px-5">
              Billing
            </a>
          </div>

          <div className="menu-item px-3">
            <a href="#" className="menu-link px-5">
              Payments
            </a>
          </div>

          <div className="menu-item px-3">
            <a href="#" className="menu-link d-flex flex-stack px-5">
              Statements
              <i
                className="fas fa-exclamation-circle ms-2 fs-7"
                data-bs-toggle="tooltip"
                title="View your statements"
              />
            </a>
          </div>

          <div className="separator my-2" />

          <div className="menu-item px-3">
            <div className="menu-content px-3">
              <label className="form-check form-switch form-check-custom form-check-solid">
                <input
                  className="form-check-input w-30px h-20px"
                  type="checkbox"
                  value="1"
                  defaultChecked={true}
                  name="notifications"
                />
                <span className="form-check-label text-muted fs-7">
                  Notifications
                </span>
              </label>
            </div>
          </div>
        </div>
      </div>

      <div className="menu-item px-5">
        <a href="#" className="menu-link px-5">
          My Statements
        </a>
      </div>

      <div className="separator my-2" />

      <div className="menu-item px-5 my-1">
        <Link to="/crafted/account/settings" className="menu-link px-5">
          Account Settings
        </Link>
      </div> */}

      <div className="menu-item px-5">
        <a onClick={logout} className="menu-link px-5">
          Sign Out
        </a>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  const { logoutStore } = state
  return {
    logoutData: logoutStore && logoutStore.logout ? logoutStore.logout : {},
    loading: logoutStore && logoutStore.loading ? logoutStore.loading : false
  }
}

const mapDispatchToProps = dispatch => ({
  logoutDispatch: () => dispatch(LogoutActions.logout()),
  clearLogout: () => dispatch(LogoutActions.clearLogout())
})

export default connect(mapStateToProps,mapDispatchToProps)(HeaderUserMenu)