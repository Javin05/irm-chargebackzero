import React, { useEffect } from 'react'
import AsideDefault from './components/aside/AsideDefault'
import { Footer } from './components/Footer'
import HeaderWrapper from './components/header/HeaderWrapper'
import { Toolbar } from './components/toolbar/Toolbar'
import { ScrollTop } from './components/ScrollTop'
import { Content } from './components/Content'
import { PageDataProvider } from './core'
import { useLocation } from 'react-router-dom'
import {
  DrawerMessenger,
  ActivityDrawer,
  Main,
  InviteUsers,
  UpgradePlan,
} from '../partials'
import { MenuComponent } from '../assets/ts/components'
import moment from "moment";
import _ from 'lodash'
import { SESSION, SET_STORAGE } from "../../utils/constants";
import {
  getLocalStorage,
  setLocalStorage,
  getLastInteractionTime,
  setLastInteractionTime,
  addEvents,
  removeEvents,
} from "../../utils/helper";
import { setTimeout } from "timers";
import { left } from "@popperjs/core";
import clsx from 'clsx'

interface MasterLayoutOptions {
  children?: any;
  continueSession?: any;
  setTimerDispatch?: any;
  getSiteconfigDispatch?: any;
  siteConfigs?: any;
  statusGSC?: any;
  loadingGSC?: any;
}

const MasterLayout: React.FC<MasterLayoutOptions> = ({
  children,
  continueSession,
  setTimerDispatch,
  getSiteconfigDispatch,
  siteConfigs,
  statusGSC,
  loadingGSC,
}) => {

  const didMount = React.useRef(false);
  const isAuthorized = getLocalStorage(SESSION.TOKEN);
  const location = useLocation()

  useEffect(() => {
    setTimeout(() => {
      MenuComponent.reinitialization()
    }, 500)
  }, [])

  useEffect(() => {
    setTimeout(() => {
      MenuComponent.reinitialization()
    }, 500)
  }, [location.key])

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentName = url && url[1]
  const [counter, setCounter] = React.useState<any>(0);

  const eventHandler = () => {
    if (isAuthorized) {
      setLastInteractionTime(SESSION.LAST_INTERACTION_TIME, moment());
    }
  };

  const lastInteractionTime = getLastInteractionTime(
    SESSION.LAST_INTERACTION_TIME
  );

  const locate = location.pathname

  useEffect(() => {
    let now = moment(new Date());
    let end = moment(lastInteractionTime);
    let duration = moment.duration(now.diff(end));
    let timeInteractionMins = duration.asMinutes();
    const getIdleTimeOut = siteConfigs && siteConfigs.sessionTimeOut ?
      siteConfigs.sessionTimeOut : SESSION.IDLE_TIME;
    const checkSession = Math.floor(timeInteractionMins) >= getIdleTimeOut;
    // const fifteenMinsInsecs = 900000
    if (checkSession) {
      setLocalStorage(SET_STORAGE.LAST_LOCATION, locate)
      window.location.href = "/session-timeout";
    } else {
      setTimeout(() => {
        setCounter((val: any) => val + 60);
      }, 60000);
    }
  }, [counter]);

  useEffect(() => {
    addEvents(eventHandler);
    return () => {
      removeEvents(eventHandler);
    };
  }, []);

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  });

  const findRoute =
    [
      "?apiKey=",
    ]
  const joindata = `${location.search}`
  let result = joindata.slice(0, 8)
  setLocalStorage('SDKROUTE', result)
  
  return (
    <PageDataProvider>
      {
        !_.includes(findRoute, result) ? (
          <div className="page d-flex flex-row flex-column-fluid">
            <AsideDefault />
            <div
              className="wrapper d-flex flex-column flex-row-fluid"
              id="kt_wrapper"
            >
              <HeaderWrapper />
              <div
                id="kt_content"
                className="content d-flex flex-column flex-column-fluid"
              >
                {
                  currentName === 'home' || currentName === 'queue-reports' ? (
                    null
                  ) :
                    (
                      <div
                        className={pathName ==='/nic-search' ? 'mt-2':'mt-20'}
                      >
                        <Toolbar />
                      </div>
                    )
                }
                <div className="post d-flex flex-column-fluid" id="kt_post">
                  <Content>{children}</Content>
                </div>
              </div>
              <Footer />
            </div>
          </div>
        ) : (
          <div className="container-fixed">
            <div
              id='kt_page_title'
              data-kt-swapper='true'
              data-kt-swapper-mode='prepend'
              data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
              className={clsx('page-title d-flex bg-skyBlue py-8')}
            >
              <h1 className='d-flex align-items-center text-dark fw-bolder my-1 fs-3 ms-10'>
                {/* Wrm Riskmmanagement */}
              </h1>
            </div>
              <div className="post d-flex flex-column-fluid" id="kt_post">
                <Content>{children}</Content>
              </div>
            </div>
        )
      }

      {/* begin:: Drawers */}
      <ActivityDrawer />
      <DrawerMessenger />
      {/* end:: Drawers */}

      {/* begin:: Modals */}
      <Main />
      <InviteUsers />
      <UpgradePlan />
      {/* end:: Modals */}
      <ScrollTop />
    </PageDataProvider>
  )
}

export { MasterLayout }
