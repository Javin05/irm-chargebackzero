/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import { KTSVG } from '../../../helpers';
import { AsideMenuItemWithSub } from './AsideMenuItemWithSub';
import { AsideMenuItem } from './AsideMenuItem';

export function AsideMenuMain() {
  return (
    <>
      <AsideMenuItem
        to="/main-dashboard"
        title="Dashboard"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/abstract/abs048.svg"
      />
      <AsideMenuItem
        to="/home"
        title="Home"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/general/gen008.svg"
      />
      <AsideMenuItemWithSub
        to='/risk-management'
        icon='/media/icons/duotune/communication/com006.svg'
        title='Cases'
        fontIcon='bi-layers'
      >
        <AsideMenuItem
          to="/risk-management"
          title="Accounts"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/general/gen015.svg"
        />
        <AsideMenuItem
          to="/transaction"
          title="Transactions"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/abstract/abs021.svg"
        />
        <AsideMenuItem
          to="/aml-queue"
          title="AML"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/abstract/abs022.svg"
        />
        <AsideMenuItem
          to="/kyc"
          title="KYC"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/abstract/abs023.svg"
        />
      </AsideMenuItemWithSub>
      <AsideMenuItem
        to="/upcoming"
        title="Create"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/general/gen005.svg"
      />
      <AsideMenuItemWithSub
        to='/mamage-queues'
        icon='/media/icons/duotune/communication/com003.svg'
        title='Queues'
        fontIcon='bi-layers'
      >
        {/* <AsideMenuItem
          to="/queues"
          title="Queues"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/general/gen007.svg"
        /> */}
        <AsideMenuItem
          to='/manage-queues'
          icon='/media/icons/duotune/general/gen035.svg'
          title='Manage Queues'
          fontIcon='bi-layers'
        />
      </AsideMenuItemWithSub>
      <AsideMenuItemWithSub
        to='/rules'
        icon='/media/icons/duotune/general/gen027.svg'
        title='Rules'
        fontIcon='bi-layers'
      >
        <AsideMenuItem
          to="/rules"
          title="Rules Basic"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/general/gen009.svg"
        />
        <AsideMenuItem
          to='/blacklist'
          icon='/media/icons/duotune/general/gen039.svg'
          title='BlackList'
          fontIcon='bi-layers'
        />
        <AsideMenuItem
          to="/white-list"
          title="White List"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/arrows/arr086.svg"
        />
        {/* <AsideMenuItem
          to="/watch-list"
          title="Watch List"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/abstract/abs029.svg"
        /> */}
        <AsideMenuItem
          to="/fraud-patterns"
          title="Fraud Patterns"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/layouts/lay002.svg"
        />
      </AsideMenuItemWithSub>
      <AsideMenuItemWithSub
        to='/reports'
        icon='/media/icons/duotune/general/gen030.svg'
        title='Reports'
        fontIcon='bi-layers'
      >
        <AsideMenuItem
          to="/reports"
          title="Reports"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/general/gen011.svg"
        />
        <AsideMenuItem
          to="/queue-reports"
          title="Queue Reports"
          fontIcon="bi-archive"
          icon="/media/icons/duotune/general/gen031.svg"
        />
      </AsideMenuItemWithSub>
      <AsideMenuItem
        to="/upcoming"
        title="Filters"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/general/gen012.svg"
      />
      <AsideMenuItem
        to="/admin"
        title="Admin"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/communication/com005.svg"
      />
      <AsideMenuItem
        to="/client-management"
        title="Clients"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/communication/com013.svg"
      />

      <AsideMenuItem
        to="/new-dashboard"
        title="New Dashboard"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/general/gen019.svg"
      />
      {/* <AsideMenuItem
        to="/web-risk"
        title="Web Risk Analysis"
        fontIcon="bi-archive"
        icon="/media/icons/duotune/general/gen024.svg"
      /> */}
      <AsideMenuItem
        to='/user-management'
        title='Users ManageMent'
        icon='/media/icons/duotune/general/gen023.svg'
      />
      <AsideMenuItemWithSub
        to='/crafted/pages'
        icon="/media/icons/duotune/coding/cod001.svg"
        title='Settings'
      >
        <AsideMenuItem
          to='/user-role'
          title='Users Role'
          icon='/media/icons/duotune/general/gen049.svg'
        />
        <AsideMenuItem
          to='/user-type'
          title='Users Type'
          icon='/media/icons/duotune/communication/com005.svg'
        />
        <AsideMenuItem
          to='/user-components'
          title='Components'
          icon='/media/icons/duotune/general/gen022.svg'
        />
        <AsideMenuItem
          to='/user-privileges'
          title='Users Privileges'
          icon='/media/icons/duotune/technology/teh002.svg'
        />
      </AsideMenuItemWithSub>
    </>
  );
}
