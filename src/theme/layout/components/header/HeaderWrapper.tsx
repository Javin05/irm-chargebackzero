/* eslint-disable react-hooks/exhaustive-deps */
import clsx from 'clsx'
import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import color from "../../../../utils/colors";
import { KTSVG, toAbsoluteUrl } from '../../../helpers'
import { SET_FILTER, GET_CLIENT_FILTER } from '../../../../utils/constants'
import { setLocalStorage, getLocalStorage, removeLocalStorage } from '../../../../utils/helper'
import ReactSelect from "../../../../theme/layout/components/ReactSelect";
import { useLayout } from '../../core'
import Topbar from './Topbar'
import { clientCredFilterActions } from '../../../../store/actions'
import FindRole from '../../../../components/wrmManagement/Role'

function HeaderWrapper(props: any) {
  const {
    getClientDispatch,
    getClient,
    loadingClient,
    setFilterFunctionDispatch,
    setCredFilterParamsDispatch
  } = props

  const didMount = React.useRef(false);
  const pathName = useLocation().pathname;
  setLocalStorage("ROUTE", JSON.stringify(pathName))
  const Role = JSON.parse(getLocalStorage("ROLEDATA") || "")
  const { config, classes, attributes } = useLayout()
  const { header, aside } = config
  const [typingTimeout, setTypingTimeout] = useState<any>(0);
  const [selectedClientOption, setSelectedClientOption] = useState([] as any);
  const [clientOption, setClientOption] = useState([] as any);
  const [formData, setFormData] = useState({
    clientId: ""
  });

  useEffect(() => {
    const params = {
      skipPagination: true
    }
    getClientDispatch(params)
  }, [])

  const customStyles = {
    control: (provided: any) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided: any) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided: any) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state: any) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided: any) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    }),
  };

  const getDefaultOptions = (data: any, name: any) => {
    const defaultOptions = [] as any[];
    if (!_.isEmpty(data)) {
      data.map((item: any) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ""}`,
          value: item._id,
        })
      );
      return _.sortBy(defaultOptions, 'label');
    }
  };

  useEffect(() => {
    if (didMount.current) {
      const data = getDefaultOptions(getClient, "company");
      setClientOption(data);
    }
  }, [getClient]);

  const handleChangeClient = (selectedOption: any) => {
    if (selectedOption !== null) {
      setSelectedClientOption(selectedOption);
      setFormData((values) => ({ ...values, clientId: selectedOption.value }));
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      if (getClient && getClient.length == 0) {
        setTypingTimeout(
          setTimeout(() => {
            const param = {
              clientId: selectedOption.label,
            };
            getClientDispatch(param);
          }, 1000)
        );
      }
      if (getClient && getClient.length > 0) {
        setLocalStorage(SET_FILTER.CLIENT_ID_FILTER, selectedOption.value);
        setLocalStorage(GET_CLIENT_FILTER.GET_CLIENT_ID, JSON.stringify(selectedOption));
      }
    } else {
      setSelectedClientOption([]);
      setFormData({
        clientId: ""
      });
      removeLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      removeLocalStorage(GET_CLIENT_FILTER.GET_CLIENT_ID);
    }
  };

  const handleSearch = () => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER);
    const params = {
      clientId: credBasedClientValue,
    };
    setFilterFunctionDispatch(true);
    setCredFilterParamsDispatch(params);
  };

  const resetOptions = () => {
    if (selectedClientOption && selectedClientOption.label) {
      setSelectedClientOption([]);
      setFilterFunctionDispatch(true);
      setFormData({
        clientId: ""
      });
      setCredFilterParamsDispatch({});
      removeLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
      removeLocalStorage(GET_CLIENT_FILTER.GET_CLIENT_ID);
    }
  }

  const credBasedClientValue = getLocalStorage(
    GET_CLIENT_FILTER.GET_CLIENT_ID
  );

  useEffect(() => {
    setSelectedClientOption(credBasedClientValue ? JSON.parse(credBasedClientValue) : null)
  }, []);

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  });

  const slugs = [
    "/risk-management",
    "/transactions",
    "/aml-queue",
    "/KYC",
    "/manage-queues",
    "/rules",
    "/blacklist",
    "/white-list",
    "/fraud-patterns",
    "/queue-reports",
    "/filters",
    "/tag-summary",
    "/wrm-riskmmanagement",
    "/ongoing-monitoring",
    "/ogmForceUpload-summary",
    "/wrm-realtime-summary",
    "/wrm-realtime"
  ]

  return (
    <div
      id='kt_header'
      className={clsx('header', classes.header.join(' '), 'align-items-stretch')}
      {...attributes.headerMenu}
    >
      <div
        className={clsx(
          classes.headerContainer.join(' '),
          'd-flex align-items-stretch justify-content-between'
        )}
      >
        {aside.display && (
          <div className='d-flex align-items-center d-lg-none ms-n3 me-1' title='Show aside menu'>
            <div
              className='btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px'
              id='kt_aside_mobile_toggle'
            >
              <KTSVG path='/media/icons/duotune/abstract/abs015.svg' className='svg-icon-2x mt-1' />
            </div>
          </div>
        )}
        {!aside.display && (
          <div className='d-flex align-items-center flex-grow-1 flex-lg-grow-0'>
            <Link to='/perform-summary' className='d-lg-none'>
              <img alt='Logo' src={toAbsoluteUrl('/media/logos/logo-2.svg')} className='h-30px' />
            </Link>
          </div>
        )}

        {aside.display && (
          <div className='d-flex align-items-center flex-grow-1 flex-lg-grow-0'>
            <Link to='/' className='d-lg-none'>
              <img alt='Logo' src={toAbsoluteUrl('/media/logos/logo-2.svg')} className='h-30px' />
            </Link>
          </div>
        )}

        <FindRole
          role={Role}
        >
          <>
            {slugs.includes(pathName) ? (
              <>
                <div className="row g-2 col-lg-7 mt-2">
                  {
                    // ['All', 'Client'].includes(checkUserRole()) ? (
                    <div className="col-lg-3">
                      <div className="col-lg-12">
                        <label className="fs-8 fw-bolder">Client</label>
                        <ReactSelect
                          isClearable
                          styles={customStyles}
                          isMulti={false}
                          name="clientId"
                          placeholder="Select..."
                          className="basic-single"
                          classNamePrefix="select"
                          isLoading={loadingClient}
                          handleChangeReactSelect={handleChangeClient}
                          options={clientOption}
                          value={selectedClientOption}
                        />
                      </div>
                    </div>
                    // ) : null
                  }
                  <div className="col-lg-3 mt-5">
                    <button
                      type="button"
                      className="btn btn-sm btn-primary w-40 mx-2"
                      onClick={() => {
                        handleSearch();
                      }}
                    >
                      <span className="indicator-label">Go</span>
                    </button>
                    <button
                      type="button"
                      className="btn btn-sm btn-secondary w-40"
                      onClick={() => {
                        resetOptions();
                      }}
                    >
                      <span className="indicator-label">Reset</span>
                    </button>
                  </div>
                </div>
              </>
            ) : null}
          </>
        </FindRole>
        <div className='d-flex align-items-stretch justify-content-between flex-lg-grow-1'>
          {header.left === 'menu' && (
            <div className='d-flex align-items-stretch' id='kt_header_nav'>
            </div>
          )}

          {header.left === 'page-title' && (
            <div className='d-flex align-items-center' id='kt_header_nav' />
          )}

          <div className='d-flex align-items-stretch flex-shrink-0 w-100 justify-content-end'>
            <Topbar />
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => {
  const { clientCrudFilterStore } = state
  return {
    getClient: clientCrudFilterStore && clientCrudFilterStore.getClient ? clientCrudFilterStore.getClient : [],
    loadingClient: clientCrudFilterStore && clientCrudFilterStore.loadingGetClient ? clientCrudFilterStore.loadingGetClient : false,
    setFilterFunction: clientCrudFilterStore && clientCrudFilterStore.setFilterFunction &&
      clientCrudFilterStore.setFilterFunction ? clientCrudFilterStore.setFilterFunction : false,
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  getClientDispatch: (params: any) =>
    dispatch(clientCredFilterActions.getAuthClient(params)),
  setFilterFunctionDispatch: (data: any) =>
    dispatch(clientCredFilterActions.setFilterFunction(data)),
  setCredFilterParamsDispatch: (data: any) =>
    dispatch(clientCredFilterActions.setCredFilterParams(data)),
  clearClientDispatch: () =>
    dispatch(clientCredFilterActions.clearAuthClient()),
})

export default connect(mapStateToProps, mapDispatchToProps)(HeaderWrapper);