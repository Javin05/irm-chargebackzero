import clsx from "clsx";
import React, { FC, useEffect } from "react";
import { KTSVG, toAbsoluteUrl } from "../../../helpers";
import { HeaderNotificationsMenu, HeaderThemesMenu } from "../../../partials";
import HeaderUserMenu from "../../../partials/layout/header-menus/HeaderUserMenu";
import { useLayout } from "../../core";
import { useHistory } from "react-router-dom";
import { getLocalStorage } from "../../../../utils/helper";
import FindRole from "../../../../components/wrmManagement/Role";
import { connect } from "react-redux";
import {
  FeedBackCountActions,
  NotificationFeedBackActions,
} from "../../../../store/actions";

const toolbarButtonMarginClass = "",
  toolbarButtonHeightClass = "w-30px h-30px w-md-40px h-md-40px",
  toolbarUserAvatarHeightClass = "symbol-30px symbol-md-40px",
  toolbarButtonIconSizeClass = "svg-icon-1";

const Topbar: FC = (props: any) => {
  const {
    getNotificationFeedBackDispatch,
    getFeedBackCount,
    feedBackCountData,
    irmClientResponse,
  } = props;
  const { config } = useLayout();
  const history = useHistory();
  const Role = JSON.parse(getLocalStorage("ROLEDATA") || "");

  const handleFeedback = () => {
    history.push("/feedback");
  };

  useEffect(() => {
    const params = {
      limit: 25,
      page: 1
    }
    getNotificationFeedBackDispatch(params);
    getFeedBackCount();
  }, []);

  return (
    <div className="d-flex align-items-stretch flex-shrink-0 w-50 justify-content-end">
      {/* NOTIFICATIONS */}
      <div
        className={clsx(
          "d-flex align-items-center me-10",
          toolbarButtonMarginClass
        )}
      >
        {/* begin::Menu- wrapper */}
        {Role !== "Admin" &&
        irmClientResponse &&
        irmClientResponse.result[0] &&
        irmClientResponse.result[0].feedBackChat === "YES" ? (
          <button
            onClick={handleFeedback}
            className="btn btn-secondary position-relative p-2 rounded-circle"
          >
            <i className="bi bi-bell fs-1 ms-2"></i>
            <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
              {feedBackCountData && feedBackCountData.count}
            </span>
          </button>
        ) : Role === "Admin" ? (
          <button
            onClick={handleFeedback}
            className="btn btn-secondary position-relative p-2 rounded-circle"
          >
            <i className="bi bi-bell fs-1 ms-2"></i>
            <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
              {feedBackCountData && feedBackCountData.count}
            </span>
          </button>
        ) : null}
        {/* <HeaderNotificationsMenu /> */}
        {/* end::Menu wrapper */}
      </div>

      {/* THEMES */}
      {/*<div className={clsx('d-flex align-items-center', toolbarButtonMarginClass)}>
        {/* begin::Menu- wrapper */}
      {/*<div
          className={clsx(
            'btn btn-icon btn-active-light-primary position-relative',
            toolbarButtonHeightClass
          )}
          data-kt-menu-trigger='click'
          data-kt-menu-attach='parent'
          data-kt-menu-placement='bottom-end'
          data-kt-menu-flip='bottom'
        >
          <KTSVG
            path='/media/icons/duotune/coding/cod001.svg'
            className={toolbarButtonIconSizeClass}
          />
        </div>
        <HeaderThemesMenu />
        {/* end::Menu wrapper */}
      {/*</div>

      {/* begin::User */}
      <div
        className={clsx(
          "d-flex align-items-center justify-content-between",
          toolbarButtonMarginClass
        )}
        id="kt_header_user_menu_toggle"
      >
        {/* begin::Toggle */}
        <div
          className={clsx(
            "cursor-pointer symbol",
            toolbarUserAvatarHeightClass
          )}
          data-kt-menu-trigger="click"
          data-kt-menu-attach="parent"
          data-kt-menu-placement="bottom-end"
          data-kt-menu-flip="bottom"
        >
          <img src={toAbsoluteUrl("/media/avatars/150-2.jpg")} alt="metronic" />
        </div>
        <HeaderUserMenu />
        {/* end::Toggle */}
      </div>
      {/* end::User */}

      {/* begin::Aside Toggler */}
      {config.header.left === "menu" && (
        <div
          className="d-flex align-items-center d-lg-none ms-2 me-n3"
          title="Show header menu"
        >
          <div
            className="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px"
            id="kt_header_menu_mobile_toggle"
          >
            <KTSVG
              path="/media/icons/duotune/text/txt001.svg"
              className="svg-icon-1"
            />
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: any) => {
  const { clinetListStore, FeedBackCountStore } = state;
  return {
    feedBackCountData:
      FeedBackCountStore && FeedBackCountStore.FeedBackCountData
        ? FeedBackCountStore.FeedBackCountData
        : {},
    irmClientResponse:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists.data
        : "",
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  getNotificationFeedBackDispatch: (params: any) =>
    dispatch(NotificationFeedBackActions.getNotificationFeedBackList(params)),
  getFeedBackCount: (params: any) =>
    dispatch(FeedBackCountActions.getFeedBackCount(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Topbar);
