
import React, { FC, useEffect, useRef, useState, useCallback } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  CityActions,
  CommomFileAction,
  IndidualPanUploadAction,
  form80UploadAction,
  form12UploadAction
} from '../../../store/actions'
import { STATUS_RESPONSE, DROPZONE_MESSAGES, KYC_URL } from '../../../utils/constants'
import { useDropzone } from 'react-dropzone'
import PAPverify from './PAPverify'
import DocumentVerify from './DocumentVerify'
import {
  DROPZONE_IMAGE_NAME_TYPES,
  FILE_FORMAT_TYPE,
  FILE_FORMAT_TYPE_DOCUMENT,
  FILE_ORGANIZATION,
  COMMON_PROOF_TYPE
} from "../../../constants/index";
import { warningAlert } from "../../../utils/alerts"
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'

function Documents(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    dropzoneDocument,
    dropzoneImage,
    setFullKycDetails,
    CommonFileDocument,
    ClearCommonFileDispatch,
    IndidualPanUploadDispatch,
    IndidualPanUploadRes,
    IndidualPanUploadLoadig,
    clearIndidualPanUploadDispatch,
    form80UploadDispatch,
    Form80Res,
    Form80ResLoadig,
    clearform80UploadDispatch,
    form12UploadDispatch,
    Form12Res,
    Form12ResLoadig,
    clearform12UploadDispatch,
  } = props
  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const hiddenFileInput = useRef(null)
  const hiddenFilesInput = useRef(null)
  const indidualFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [showForm, setShowForm] = useState(true)
  const [file80GName, setFile80GName] = useState('Upload')
  const [file12GName, setFile12GName] = useState('Upload')
  const [individualPanFileName, setIndividualPanFileName] = useState('Upload')
  const [showIndidualPan, setshowIndidualPan] = useState(false);
  const [showform80g, setShowform80g] = useState(false);
  const [showform12G, setShowform12g] = useState(false);
  const [nameChange, setNameChange] = useState(COMMON_PROOF_TYPE[organization]);
  const [formData, setFormData] = useState({
    businessPanImage: '',
    businessCinImage: '',
    form80G: '',
    trust_form12A: '',
    allFiles: '',
    individualPanImage: ''
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleSubmit = () => {
    const errors = {}
    if (organization === "Proprietorship"){
      if (_.isEmpty(formData.individualPanImage)) {
        errors.individualPanImage = 'Individual PanImage Is Required'
      }
      if (_.isEmpty(formData.businessCinImage)) {
        errors.businessCinImage = 'Business Cin Document Is Required'
      }
      if (_.isEmpty(errors)) {
        onClickNext(5)
        setFullKycDetails((values) => ({ ...values, uploadsData: formData }))
      }
    }else{
      if (_.isEmpty(formData.businessPanImage)) {
        errors.businessPanImage = 'Business PanImage Is Required'
      }
      if (_.isEmpty(formData.businessCinImage)) {
        errors.businessCinImage = 'Business Cin Document Is Required'
      }
      if (_.isEmpty(formData.individualPanImage)) {
        errors.individualPanImage = 'Individual PanImage Is Required'
      }
      if (_.isEmpty(errors)) {
        onClickNext(5)
        setFullKycDetails((values) => ({ ...values, uploadsData: formData }))
      }

    }
    setErrors(errors)
  }

  useEffect(() => {
    if (dropzoneImage && dropzoneImage.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = dropzoneImage && dropzoneImage.data && dropzoneImage.data.path
      setFormData((values) => ({ ...values, businessPanImage: data }))
      setErrors(false)
    } else if (dropzoneImage && dropzoneImage.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData({
        businessPanImage: formData.businessPanImage,
        businessCinImage: formData.businessCinImage
      })
    }
  }, [dropzoneImage])

  useEffect(() => {
    if (dropzoneDocument && dropzoneDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = dropzoneDocument && dropzoneDocument.data && dropzoneDocument.data.path
      setFormData((values) => ({ ...values, businessCinImage: data }))
      setErrors(false)
    } else if (dropzoneDocument && dropzoneDocument.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData({
        businessPanImage: formData.businessPanImage,
        businessCinImage: ''
      })
    }
  }, [dropzoneDocument])

  useEffect(() => {
    if (CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      setFormData((values) => ({ ...values, businessCinImage: data }))
      setErrors(false)
    } else if (CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData({
        businessCinImage: formData.businessCinImage,
        businessPanImage: formData.businessPanImage,
      })
      ClearCommonFileDispatch()
    }
  }, [CommonFileDocument])

  useEffect(() => {
    if (IndidualPanUploadRes && IndidualPanUploadRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = IndidualPanUploadRes && IndidualPanUploadRes.data && IndidualPanUploadRes.data.path
      setFormData((values) => ({ ...values, individualPanImage: data }))
      setIndividualPanFileName('Uploaded')
      setshowIndidualPan(true)
    } else if (IndidualPanUploadRes && IndidualPanUploadRes.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, individualPanImage: '' }))
      clearIndidualPanUploadDispatch()
    }
  }, [IndidualPanUploadRes])

  useEffect(() => {
    if (Form80Res && Form80Res.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = Form80Res && Form80Res.data && Form80Res.data.path
      setFormData((values) => ({ ...values, form80G: data }))
      setFile80GName('Uploaded')
      setShowform80g(true)
    } else if (Form80Res && Form80Res.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, form80G: '' }))
      clearform80UploadDispatch()
      setFile80GName('Upload')
      setShowform80g(false)
    }
  }, [Form80Res])

  useEffect(() => {
    if (Form12Res && Form12Res.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = Form12Res && Form12Res.data && Form12Res.data.path
      setFormData((values) => ({ ...values, form12A: data }))
      setFile12GName('Uploaded')
      setShowform12g(true)
    } else if (Form12Res && Form12Res.status === STATUS_RESPONSE.ERROR_MSG) {
      setFormData((values) => ({ ...values, form12A: '' }))
      clearform80UploadDispatch()
      setFile12GName('Upload')
      setShowform12g(false)
    }
  }, [Form12Res])

  const FileChangeHandler = (e) => {
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        if (name === 'individualPanImage') {
          const data = new FormData()
          data.append('type', 'individualPanImage')
          data.append('file_to_upload', files)
          IndidualPanUploadDispatch(data)
        }else if (name === 'form80G'){
          const data = new FormData()
          data.append('type', 'form80G')
          data.append('file_to_upload', files)
          form80UploadDispatch(data)
        }else{
          const data = new FormData()
          data.append('type', 'form12A')
          data.append('file_to_upload', files)
          form12UploadDispatch(data)
        }    
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  }

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  }
  const handleBackClick = (event) => {
    hiddenFilesInput.current.click(event);
  }

  const indidualPanClick = (event) => {
    indidualFilesInput.current.click(event);
  }

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Upload your business documents</span>
                  </label>
                </div>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>We require these for verifying your business details.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='adhaarNumber'
                    />
                  </label>
                </div>
              </div>
              {
                organization === 'Proprietorship' ? (
                  null
                ):(
                  <>
                  <div className='row mb-4 mt-15'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2'>
                        <span className='required fw-bold fs-6'>Company PAN</span>
                      </label>
                    </div>
                    <div className='col-lg-6'>
                      <PAPverify
                        type={DROPZONE_IMAGE_NAME_TYPES.businessPanImage}
                        multiple={false}
                        thumbnail={false}
                        maxFileSize="5000"
                        formatType={FILE_FORMAT_TYPE}
                        showRemove={true}
                        proofType={`pan`}
                      />
                      {errors && errors.businessPanImage && (
                        <div className='rr mt-1'>
                          <style>{'.rr{color:red;}'}</style>
                          {errors.businessPanImage}
                        </div>
                      )}
                    </div>
                  </div>
                  </>
                )
              }
              <div className="separator border-secondary mt-4 mb-4" />
              <div className='fv-row'>
                <div className='row'>
                  <div className='col-md-6'>
                    <label className='d-flex align-items-center fw-bold mb-2'>
                      <span className='required fw-bold fs-6'>Indiduval PAN</span>
                    </label>
                  </div>
                  <div className='col-md-6'>
                    <input
                      type="file"
                      className="d-none"
                      name="individualPanImage"
                      id="individualPanImage"
                      multiple={false}
                      ref={indidualFilesInput}
                      onChange={FileChangeHandler} />
                    <button type="button"
                      className={`${!showIndidualPan ? 'btn btn-light-primary btn-sm' : 'btn btn-sm btn-success'}`}
                      onClick={indidualPanClick}>
                      {IndidualPanUploadLoadig
                        ? (
                          'Uploading...'
                        )
                        : (
                          <>
                            {/* <i className="bi bi-upload" /> */}
                            {individualPanFileName}
                          </>
                        )}
                    </button>
                    {errors && errors.individualPanImage && (
                      <div className="rr mt-1">
                        <style>
                          {
                            ".rr{color:red}"
                          }
                        </style>
                        {errors.individualPanImage}
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="separator border-secondary mt-4 mb-4" />
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-6'>
                      {FILE_ORGANIZATION[organization]}
                    </span>
                  </label>
                </div>
                <div className='col-lg-6'>
                  <>
                    <DocumentVerify
                      type={DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS}
                      multiple={false}
                      thumbnail={false}
                      maxFileSize="5000"
                      formatType={FILE_FORMAT_TYPE_DOCUMENT}
                      showRemove={true}
                      proofType={COMMON_PROOF_TYPE[organization]}
                    />
                    {errors && errors.businessCinImage && (
                      <div className='rr mt-1'>
                        <style>{'.rr{color:red;}'}</style>
                        {errors.businessCinImage}
                      </div>
                    )}
                  </>
                </div>
              </div>
              <div className="separator border-secondary mt-4 mb-4" />
              <div>
                {
                  (organization === 'Trust' || organization === 'Society' || organization === 'NGO') ? (
                    <>
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-md-6'>
                            <label className='d-flex align-items-center fw-bold mb-2'>
                              <span className='required fw-bold fs-6'>Form 80G Allotmeent Letter</span>
                            </label>
                          </div>
                          <div className='col-md-6'>
                            <input
                              type="file"
                              className="d-none"
                              name="form80G"
                              id="form80G"
                              multiple={false}
                              ref={hiddenFileInput}
                              onChange={FileChangeHandler} />
                            <button type="button"
                              className={`${!showform80g ? 'btn btn-light-primary btn-sm' : 'btn btn-sm btn-success'}`}
                              onClick={handleClick}>
                              {Form80ResLoadig
                              ? (
                                'Uploading...'
                              )
                              : (
                                <>
                                  {file80GName}
                                </>
                              )}
                            </button>
                            {errors && errors.form80G && (
                              <div className="rr mt-1">
                                <style>
                                  {
                                    ".rr{color:red}"
                                  }
                                </style>
                                {errors.form80G}
                              </div>
                            )}
                          </div>
                          <div className='text-muted fs-8'>Optional</div>
                        </div>
                      </div>
                      <div className="separator border-secondary mt-4 mb-4" />
                      <div className='fv-row mt-4'>
                        <div className='row'>
                          <div className='col-md-6'>
                            <label className='d-flex align-items-center fw-bold mb-2'>
                              <span className='required fw-bold fs-6'>Form 12G Allotmeent Letter</span>
                            </label>
                          </div>
                          <div className='col-md-6'>
                            <input
                              type="file"
                              className="d-none"
                              name="form12A"
                              id="form12A"
                              multiple={false}
                              ref={hiddenFilesInput}
                              onChange={FileChangeHandler} />
                            <button type="button"
                              className={`${!showform12G ? 'btn btn-light-primary btn-sm' : 'btn btn-sm btn-success'}`}
                              onClick={handleBackClick}>
                              {Form12ResLoadig
                              ? (
                                'Uploading...'
                              )
                              : (
                                <>
                                  {file12GName}
                                </>
                              )}
                            </button>
                            {errors && errors.form12A && (
                              <div className="rr mt-1">
                                <style>
                                  {
                                    ".rr{color:red}"
                                  }
                                </style>
                                {errors.form12A}
                              </div>
                            )}
                          </div>
                          <div className='text-muted fs-8'>Optional</div>
                        </div>
                      </div>
                    </>
                  ) : (
                    null
                  )
                }
              </div>
            </div>
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2'>
                  <button
                    onClick={() => { goBack(2) }}
                    type='button'
                    className='btn btn-sm btn-light-primary me-3'
                  >
                    <KTSVG
                      path='/media/icons/duotune/arrows/arr063.svg'
                      className='svg-icon-4 me-1'
                    />
                    Back
                  </button>
                </div>
                <div>
                  {
                    showForm ? (
                      <button type='submit' className='btn btn-sm btn-primary me-3'
                        onClick={() => {
                          handleSubmit()
                        }}
                      >
                        <span className='indicator-label'
                        >
                          <i className={`bi bi-person-check-fill`} />
                        </span>
                        {loading
                          ? (
                            <span
                              className='spinner-border spinner-border-sm mx-3'
                              role='status'
                              aria-hidden='true'
                            />
                          )
                          : (
                            'Submit'
                          )}
                      </button>
                    ) :
                      null
                  }
                  <button type='submit' className='btn btn-sm btn-light-danger ms-2'
                    onClick={() => { onClickNext(5) }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Skip
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, dropzoneStore, CommonFileStore, IndidualPanStore, Form80Store, Form12Store } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    dropzoneDocument: dropzoneStore && dropzoneStore.dropzoneDocument ? dropzoneStore.dropzoneDocument : {},
    dropzoneImage: dropzoneStore && dropzoneStore.dropzoneImage ? dropzoneStore.dropzoneImage : {},
    CommonFileDocument: CommonFileStore && CommonFileStore.commonFileRes ? CommonFileStore.commonFileRes : {},
    IndidualPanUploadRes: IndidualPanStore && IndidualPanStore.IndidualPanUploadRes ? IndidualPanStore.IndidualPanUploadRes : {},
    IndidualPanUploadLoadig: IndidualPanStore && IndidualPanStore.loading ? IndidualPanStore.loading : false,
    Form80Res: Form80Store && Form80Store.Form80Res ? Form80Store.Form80Res : {},
    Form80ResLoadig: Form80Store && Form80Store.loading ? Form80Store.loading : false,
    Form12Res: Form12Store && Form12Store.Form12Res ? Form12Store.Form12Res : {},
    Form12ResLoadig: Form12Store && Form12Store.loading ? Form12Store.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  ClearCommonFileDispatch: (params) => dispatch(CommomFileAction.clearCommonFile(params)),
  IndidualPanUploadDispatch: (params) => dispatch(IndidualPanUploadAction.IndidualPanUpload(params)),
  clearIndidualPanUploadDispatch: (params) => dispatch(IndidualPanUploadAction.clearIndidualPanUpload(params)),
  form80UploadDispatch: (params) => dispatch(form80UploadAction.form80Upload(params)),
  clearform80UploadDispatch: (params) => dispatch(form80UploadAction.clearform80Upload(params)),
  form12UploadDispatch: (params) => dispatch(form12UploadAction.form12Upload(params)),
  clearform12UploadDispatch: (params) => dispatch(form12UploadAction.clearform12Upload(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Documents)