import React, { useState, useEffect, useCallback } from "react";
import { connect } from "react-redux";
import Dropzone, { useDropzone } from "react-dropzone";
import _ from "lodash";
import {
  DROPZONE_IMAGE_NAME_TYPES
} from "../../../constants/index";
import { UploadDocumentActions } from '../../../utils/dropzone/redux/action'
import { STATUS_RESPONSE } from '../../../utils/constants'
import { warningAlert } from "../../../utils/alerts"
import { CommomFileAction } from '../../../store/actions'

function DocumentVerify(props) {
  const {
    defaultImages,
    multiple,
    type,
    name,
    flag,
    maxFileSize,
    formatType,
    dropzoneImage,
    postUploadDispatch,
    proofType,
    DocumentUploadDispatch,
    dropzoneDocument,
    CommonUploadDispatch,
    CommonFileDocument,
    ClearCommonFileDispatch,
    ClearDocumentDispatch
  } = props;

  const [totalSize, setTotalSize] = useState(0);
  const [status, setStatus] = useState(
    "Upload"
  );
  const [show, setShow] = useState(false);
  const [error, setError] = useState(false);
  const isJsonFormat = _.includes(formatType, "application/json");

  const setFormatTypeError = (type, setStatus) => {
    switch (type) {
      case DROPZONE_IMAGE_NAME_TYPES.IMAGE:
        return setStatus('Image is InValid Type')
      case DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS:
        return setStatus('Document is Invalid Type')
    }
  };

  const onDrop = useCallback((acceptedFiles) => {
    setError(false);
    setStatus("Uploading...");
    let isValidFileFormat = false;
    const formData = new FormData();
    acceptedFiles.forEach((item) => {
      const fileType = item && item.type;
      const uploadedFileSize = item && item.size;
      isValidFileFormat = _.includes(formatType, fileType);
      const fileSize = Number(maxFileSize) * 1024 * 1024;
      setTotalSize(uploadedFileSize);
      if (isValidFileFormat) {
        if (uploadedFileSize < fileSize) {
          formData.append('type', `${proofType}`)
          formData.append('file_to_upload', item)
          if(proofType === 'cin'){
            DocumentUploadDispatch(formData)
          }else {
            const data = new FormData();
            data.append('type', `${proofType}`)
            data.append('file_to_upload', item)
            CommonUploadDispatch(data)
          }
        } else {
          setError(true);
          setStatus(
            `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`
          );
        }
      } else {
        setError(true);
        setFormatTypeError(type, setStatus);
      }
    })
  }, [])

  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  useEffect(() => {
    if(dropzoneDocument && dropzoneDocument.status === STATUS_RESPONSE.SUCCESS_MSG){
      setShow(true)
      setStatus('Uploaded')
      ClearDocumentDispatch()
    } else if (dropzoneDocument && dropzoneDocument.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        dropzoneDocument && dropzoneDocument.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      setStatus('Upload')
      ClearDocumentDispatch()
    }
  },[dropzoneDocument])

  useEffect(() => {
    if(CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG){
      setShow(true)
      setStatus('Uploaded')
    } else if (CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        CommonFileDocument && CommonFileDocument.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      setStatus('Upload')
      ClearCommonFileDispatch()
    }
  },[CommonFileDocument])

  return (
    <>
      <div
        {...getRootProps({
          className: `dropzone ${error && "dropzone-error"}`,
        })}
      >
        <input {...getInputProps({ multiple })} />
        <button className= {`${!show ? `btn btn-sm btn-light-primary` : 'btn btn-sm btn-success'}`}>
          {status}
        </button>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { dropzoneStore, CommonFileStore } = state
  return {
    dropzoneDocument: dropzoneStore && dropzoneStore.dropzoneDocument ? dropzoneStore.dropzoneDocument : {},
    CommonFileDocument: CommonFileStore && CommonFileStore.commonFileRes ? CommonFileStore.commonFileRes : {}
  }
}

const mapDispatchToProps = (dispatch) => ({
  DocumentUploadDispatch: (params) => dispatch(UploadDocumentActions.uploadDocument(params)),
  ClearDocumentDispatch: (params) => dispatch(UploadDocumentActions.ClearDocument(params)),
  CommonUploadDispatch: (params) => dispatch(CommomFileAction.CommonFile(params)),
  ClearCommonFileDispatch: (params) => dispatch(CommomFileAction.clearCommonFile(params))
})
export default connect(mapStateToProps, mapDispatchToProps)(DocumentVerify)