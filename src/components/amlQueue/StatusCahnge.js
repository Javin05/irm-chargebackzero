import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import { USER_ERROR, REGEX, STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import { useLocation } from 'react-router-dom'
import {
  AMLGetIdActions,
  ApproveAMLActions,
  AMLqueueActions,
  TransactionAMLActions
} from '../../store/actions'
import { KTSVG } from '../../theme/helpers'
import clsx from 'clsx'
import _ from 'lodash'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"

function ChangeStatus(props) {
  const {
    AMLGetById,
    loading,
    ApproveAMLResponceData,
    getAMLqueuelistDispatch,
    clearApproveAML,
    getAMLIdDispatch,
    ApproveAMLDispatch,
  } = props
  const [active, setActive] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [rejectValue, setRejectValue] = useState()
  const [formData, setFormData] = useState({
    message: ''
  })
  const [holdFormData, setHoldFormData] = useState({
    riskStatus: 'HOLD',
    reason: ''
  })
  const [rejectFormData, setRejectFormData] = useState({
    riskStatus: 'REJECTED',
    reason: '',
    rejectType: '',
    rejectMoreValue: ''
  })
  const [errors, setErrors] = useState({
    reason: '',
  })
  const [approveFormData, setApproveFormData] = useState({
    riskStatus: 'APPROVED',
  })
  const [txnStatusFormData, setTxnStatusFormData] = useState({
    status: '',
    amount: '',
    transactionId: '',
    notes: ''
  })
  useEffect(() => {
    if (currentId) {
      getAMLIdDispatch(currentId)
    }
  }, [currentId])

  const clear = () => {
    setHoldFormData({
      riskStatus: 'HOLD',
      reason: ''
    })
    setRejectFormData({
      riskStatus: 'REJECTED',
      reason: '',
      rejectType: '',
      rejectMoreValue: ''
    })
    setApproveFormData({
      riskStatus: 'APPROVED',
    })
  }

  useEffect(() => {
    if (ApproveAMLResponceData && ApproveAMLResponceData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        ApproveAMLResponceData && ApproveAMLResponceData.message,
        'success',
      )
      getAMLqueuelistDispatch()
      clearApproveAML()
    } else if (ApproveAMLResponceData && ApproveAMLResponceData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        ApproveAMLResponceData && ApproveAMLResponceData.message,
        '',
        'Try again',
        '',
        () => { clear() },
        clearApproveAML()
      )
    }
  }, [ApproveAMLResponceData])

  useEffect(() => {
    if (ApproveAMLResponceData && ApproveAMLResponceData.message === 'Record Status Approved Successfully') {
      const approvemodalBtn = document.getElementById('amlapproveModal')
      approvemodalBtn.click()
    }
    if (ApproveAMLResponceData && ApproveAMLResponceData.message === 'Record Status Rejected Successfully') {
      const rejectmodalBtn = document.getElementById('reject-model')
      rejectmodalBtn.click()
    }
    if (ApproveAMLResponceData && ApproveAMLResponceData.message === 'Record Status Changed as Hold') {
      const modalBtn = document.getElementById('hold-model')
      modalBtn.click()
    }
  }, [ApproveAMLResponceData])

  const approveSubmit = () => {
    ApproveAMLDispatch(currentId, approveFormData)
  }

  const onConfirmHold = () => {
    ApproveAMLDispatch(currentId, holdFormData)
  }

  const holdSubmit = () => {
    const errors = {}
    if (_.isEmpty(holdFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.HOLD,
        'warning',
        'Yes',
        'No',
        () => { onConfirmHold() },
        () => { }
      )
    }
  }

  const onConfirmReject = () => {
    ApproveAMLDispatch(currentId, rejectFormData)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        'warning',
        'Yes',
        'No',
        () => { onConfirmReject() },
        () => { }
      )
    }
  }

  const handleChange = (e) => {
    setHoldFormData({ ...holdFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
    setRejectValue(e.target.value)
  }

  const AMLIdDetail = AMLGetById && AMLGetById.data ? AMLGetById.data : '--'

  return (
    <>
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="hold-model"
        data-target='#amlholdModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#amlrejectModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="approve-model"
        data-target='#amlapproveModal'
        onClick={() => { }}
      />
      <div
        className='modal fade'
        id='amlholdModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'rgb(213 191 86 / 30%)' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Hold
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 '>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={holdFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => holdSubmit()}
                              disabled={loading}
                            >
                              <span className='indicator-label'>Submit</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='amlapproveModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Are You Sure Want to Approve This Users Risk Analytics Report ?</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'rgb(213 191 86 / 30%)' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-9 col-md-8 col-sm-8 mb-3'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-warning m-2 fa-pull-right close'
                          data-dismiss='modal'
                        >
                          No
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                          onClick={() => approveSubmit()}
                          data-dismiss='modal'
                        >
                          Yes
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='amlrejectModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              // onClick={() => { setShow(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'rgb(213 191 86 / 30%)' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Reject :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => rejectChange(e)}
                            autoComplete='off'
                            value={rejectFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Type :
                        </label>
                        <div className='col-lg-8 col-md-8 col-sm-8'>
                          <select
                            className='form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => rejectChange(e)}
                            value={rejectFormData.rejectType || ''}
                            name='rejectType'
                          >
                            <option value='Suspect Fraud'>Suspect Fraud</option>
                            <option value=' Fraud'>Fraud</option>
                            <option value='Spam'>Spam</option>
                            <option value='Fake'>Fake</option>
                            <option value='Funneling'>Funneling</option>
                            <option value='Other'>Other</option>
                          </select>
                          {
                            rejectFormData.rejectType === 'More' ? (
                              <>
                                <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label mt-2'>
                                  please enter your message :
                                </label>
                                <textarea
                                  name='rejectMoreValue'
                                  type='text'
                                  className={clsx(
                                    'form-control form-control-lg form-control-solid',
                                    { 'is-invalid': formData.rejectMoreValue && errors.rejectMoreValue },
                                    {
                                      'is-valid': formData.rejectMoreValue && !errors.rejectMoreValue
                                    }
                                  )}
                                  placeholder='Message'
                                  onChange={(e) => rejectChange(e)}
                                  autoComplete='off'
                                  value={rejectFormData.rejectMoreValue || ''}
                                />
                              </>
                            ) : rejectValue === 'Spam' || rejectValue === 'Suspect Fraud' || rejectValue === 'Fraud' || rejectValue === 'Fake' || rejectValue === 'Funneling' ?
                              null
                              : null
                          }
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => rejectSubmit()}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className='col-sm-12 col-md-12 col-lg-12'>
          {
            AMLIdDetail.riskStatus === 'PENDING' ?
              (
                <ul className="nav">
                  <li className="nav-item">
                    <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                      data-toggle='modal'
                      data-target='#amlapproveModal'
                      onClick={() => setActive(true)}
                    >
                      Approve
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                      data-toggle='modal'
                      data-target='#amlrejectModal'
                      onClick={() => setActive(true)}
                    >
                      Reject
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link btn btn-sm fw-bolder px-4 me-2 btn-warning"
                      data-toggle='modal'
                      data-target='#amlholdModal'
                      onClick={() => setActive(true)}
                    >
                      Hold
                    </a>
                  </li>
                </ul>
              )
              : (
                AMLIdDetail.riskStatus === 'HOLD' ? (
                  <ul className="nav">
                    <li className="nav-item">
                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                        data-toggle='modal'
                        data-target='#amlapproveModal'
                        onClick={() => { setActive(true) }}
                      >
                        Approve
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                        data-toggle='modal'
                        data-target='#amlrejectModal'
                        onClick={() => { setActive(true) }}
                      >
                        Reject
                      </a>
                    </li>
                  </ul>
                ) :
                  (
                    AMLIdDetail.riskStatus === 'APPROVED' ? (
                      null
                    ) :
                      (
                        AMLIdDetail.riskStatus === 'REJECTED' ? (
                          null
                        ) : (null)
                      )
                  )
              )
          }
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { editAMLStore, ApproveAMLStore } = state

  return {
    ApproveAMLResponceData: ApproveAMLStore && ApproveAMLStore.ApproveAMLResponce ? ApproveAMLStore.ApproveAMLResponce : {},
    loading: ApproveAMLStore && ApproveAMLStore.loading ? ApproveAMLStore.loading : false,
    AMLGetById: editAMLStore && editAMLStore.AMLIdDetail ? editAMLStore.AMLIdDetail : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getAMLIdDispatch: (id) => dispatch(AMLGetIdActions.getAMLIdDetails(id)),
  ApproveAMLDispatch: (id, params) => dispatch(ApproveAMLActions.ApproveAML(id, params)),
  clearApproveAML: () => dispatch(ApproveAMLActions.clearApproveAML()),
  getAMLqueuelistDispatch: (params) => dispatch(AMLqueueActions.getAMLqueuelist(params)),
  TransactionStatusAmlDispatch: (params) => dispatch(TransactionAMLActions.TransactionAML(params)),

})

export default connect(mapStateToProps, mapDispatchToProps)(ChangeStatus)