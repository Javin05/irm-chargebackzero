import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import { useLocation } from 'react-router-dom'
import {
  ApproveAMLActions,
  AMLqueueActions,
  TransactionAMLActions
} from '../../store/actions'
import { KTSVG } from '../../theme/helpers'
import clsx from 'clsx'
import _ from 'lodash'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"

function ChangeStatus(props) {
  const {
    AMLGetById,
    TxnAMLResponceData,
    getAMLqueuelistDispatch,
    clearTransactionAML,
    TransactionStatusAmlDispatch,
  } = props
  const [active, setActive] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]  
  
  const [holdFormData, setHoldFormData] = useState({
    status: 'Refund',
    amount: '',
    transactionId: currentId,
    notes: ''
  })
  const [rejectFormData, setRejectFormData] = useState({
    status: 'Dispute',
    amount: '',
    transactionId: currentId,
    notes: ''
  })
  const [errors, setErrors] = useState({
    amount: '',
  })
  const [approveFormData, setApproveFormData] = useState({
    status: 'Chargeback',
    amount: '',
    transactionId: currentId,
    notes: ''
  })

  const clear = () => {
    setHoldFormData({
      status: '',
      amount: '',
      transactionId: '',
      notes: ''
    })
    setRejectFormData({
      status: '',
      amount: '',
      transactionId: '',
      notes: ''
    })
    setApproveFormData({
      status: '',
      amount: '',
      transactionId: '',
      notes: ''
    })
  }

  useEffect(() => {
    if (TxnAMLResponceData && TxnAMLResponceData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        TxnAMLResponceData && TxnAMLResponceData.message,
        'success',
      )
      getAMLqueuelistDispatch()
      clearTransactionAML()
    } else if (TxnAMLResponceData && TxnAMLResponceData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        TxnAMLResponceData && TxnAMLResponceData.message,
        '',
        'Try again',
        '',
        () => { clear() },
        clearTransactionAML()
      )
    }
  }, [TxnAMLResponceData])

  useEffect(() => {
    if (TxnAMLResponceData && TxnAMLResponceData.data && TxnAMLResponceData.data.status === 'Chargeback') {
      setApproveFormData({
        status: '',
        amount: '',
        transactionId: '',
        notes: ''
      })
      const approvemodalBtn = document.getElementById('approve-chargeback')
      approvemodalBtn.click()
    }
    if (TxnAMLResponceData && TxnAMLResponceData.data && TxnAMLResponceData.data.status === 'Dispute') {
      setRejectFormData({
        status: '',
        amount: '',
        transactionId: '',
        notes: ''
      })
      const rejectmodalBtn = document.getElementById('dispute-model')
      rejectmodalBtn.click()
    }
    if (TxnAMLResponceData && TxnAMLResponceData.data && TxnAMLResponceData.data.status === 'Refund') {
      setHoldFormData({
        status: '',
        amount: '',
        transactionId: '',
        notes: ''
      })
      const modalBtn = document.getElementById('refund-model')
      modalBtn.click()
    }
  }, [TxnAMLResponceData])

  const onApproveSubmit = () => {
    TransactionStatusAmlDispatch(currentId, approveFormData)
  }

  const approve = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.amount)) {
      errors.amount = 'Amount is Requiered'
    }
    setErrors(errors)

    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        'Want To Change ChargeBack',
        'warning',
        'Yes',
        'No',
        () => { onApproveSubmit() },
        () => { }
      )
    }
  }

  const onConfirmHold = () => {
    TransactionStatusAmlDispatch(currentId, holdFormData)
  }

  const holdSubmit = () => {
    const errors = {}
    if (_.isEmpty(holdFormData.amount)) {
      errors.amount = 'Amount is Requiered'
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        'Want To Change Refund',
        'warning',
        'Yes',
        'No',
        () => { onConfirmHold() },
        () => { }
      )
    }
  }

  const onConfirmReject = () => {
    TransactionStatusAmlDispatch(currentId, rejectFormData)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.amount)) {
      errors.amount = 'Amount is Requiered'
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        'Want To Change Dispute',
        'warning',
        'Yes',
        'No',
        () => { onConfirmReject() },
        () => { }
      )
    }
  }

  const AMLIdDetail = AMLGetById && AMLGetById.data ? AMLGetById.data : '--'

  const handleChange = (e) => {
    if(holdFormData.amount < AMLIdDetail.transactionAmount) {
      errors.amount = 'please enter correct value'
    }else {
    setHoldFormData({ ...holdFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
    }
  }

  const approveOnChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  return (
    <>
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="refund-model"
        data-target='#refundModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="dispute-model"
        data-target='#disputemodal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="approve-chargeback"
        data-target='#chargeback'
        onClick={() => { }}
      />
      <div
        className='modal fade'
        id='refundModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Refund</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'rgb(213 191 86 / 30%)', borderRadius:'0.475rem' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label d-flex justify-content-start mt-3'>
                          Amount :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <input
                            name='amount'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': holdFormData.amount && errors.amount },
                              {
                                'is-valid': holdFormData.amount && !errors.amount
                              }
                            )}
                            placeholder='Amount'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={holdFormData.amount || ''}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors.amount && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.amount}</span>
                            </div>
                          )}
                        </div>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label d-flex justify-content-start'>
                          Notes :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <textarea
                            name='notes'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': holdFormData.notes && errors.notes },
                              {
                                'is-valid': holdFormData.notes && !errors.notes
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={holdFormData.notes || ''}
                          />
                          {errors.notes && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.notes}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => holdSubmit()}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='chargeback'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >

        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Chargeback</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'rgb(213 191 86 / 30%)', borderRadius:'0.475rem' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label d-flex justify-content-start mt-3'>
                          Amount :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <input
                            name='amount'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': approveFormData.amount && errors.amount },
                              {
                                'is-valid': approveFormData.amount && !errors.amount
                              }
                            )}
                            placeholder='Amount'
                            onChange={(e) => approveOnChange(e)}
                            autoComplete='off'
                            value={approveFormData.amount || ''}
                            onKeyPress={(e)=> {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors.amount && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.amount}</span>
                            </div>
                          )}
                        </div>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label d-flex justify-content-start'>
                          Notes :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <textarea
                            name='notes'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': approveFormData.notes && errors.notes },
                              {
                                'is-valid': approveFormData.notes && !errors.notes
                              }
                            )}
                            placeholder='Notes'
                            onChange={(e) => approveOnChange(e)}
                            autoComplete='off'
                            value={approveFormData.notes || ''}
                          />
                          {errors.notes && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.notes}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                             <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => approve()}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='disputemodal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Dispute</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed' style={{ backgroundColor: 'rgb(213 191 86 / 30%)', borderRadius:'0.475rem' }}>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label d-flex justify-content-start mt-3'>
                          Amount :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <input
                            name='amount'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': rejectFormData.amount && errors.amount },
                              {
                                'is-valid': rejectFormData.amount && !errors.amount
                              }
                            )}
                            placeholder='Amount'
                            onChange={(e) => rejectChange(e)}
                            autoComplete='off'
                            value={rejectFormData.amount || ''}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors.amount && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.amount}</span>
                            </div>
                          )}
                        </div>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label d-flex justify-content-start'>
                          Notes :
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 mb-2'>
                          <textarea
                            name='notes'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': rejectFormData.notes && errors.notes },
                              {
                                'is-valid': rejectFormData.notes && !errors.notes
                              }
                            )}
                            placeholder='Notes'
                            onChange={(e) => rejectChange(e)}
                            autoComplete='off'
                            value={rejectFormData.notes || ''}
                          />
                          {errors.notes && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.notes}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => rejectSubmit()}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className='col-sm-12 col-md-12 col-lg-12 mt-4'>
          <ul className="nav">
          <li className="nav-item">
              <a href="#" className="btn btn-primary btn-sm me-1 mb-2"
                data-toggle='modal'
                data-target='#chargeback'
                onClick={() => setActive(true)}
              >
                Chargeback
              </a>
              <a href="#" className="btn btn-primary btn-sm me-1 mb-2"
                data-toggle='modal'
                data-target='#disputemodal'
                onClick={() => setActive(true)}
              >
                Dispute
              </a>
              <a href="#" className="btn btn-primary btn-sm me-1 mb-2" 
                 data-toggle='modal'
                 data-target='#refundModal'
                 onClick={() => setActive(true)}
              >
                Refund
              </a>
            </li>
          </ul>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { editAMLStore, TransactionAMLStore } = state

  return {
    TxnAMLResponceData: TransactionAMLStore && TransactionAMLStore.TransactionAMLResponce ? TransactionAMLStore.TransactionAMLResponce : {},
    loading: TransactionAMLStore && TransactionAMLStore.loading ? TransactionAMLStore.loading : false,
    AMLGetById: editAMLStore && editAMLStore.AMLIdDetail ? editAMLStore.AMLIdDetail : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  ApproveAMLDispatch: (id, params) => dispatch(ApproveAMLActions.ApproveAML(id, params)),
  clearApproveAML: () => dispatch(ApproveAMLActions.clearApproveAML()),
  getAMLqueuelistDispatch: (params) => dispatch(AMLqueueActions.getAMLqueuelist(params)),
  TransactionStatusAmlDispatch: (id, params) => dispatch(TransactionAMLActions.TransactionAML(id, params)),
  clearTransactionAML: (params) => dispatch(TransactionAMLActions.clearTransactionAML(params)),
})


export default connect(mapStateToProps, mapDispatchToProps)(ChangeStatus)