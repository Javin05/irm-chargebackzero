import _ from 'lodash'
import {
  USER_ERROR,
  REGEX
} from '../../utils/constants'

export const AMLValidation = (values, setErrors) => {
  const errors = {};
  if (!values.transactionDate) {
    errors.transactionDate = 'Transaction Date is required.'
  }
  if (!values.transactionAmount) {
    errors.transactionAmount = 'Transaction Amount field is required.'
  }
  if (!values.senderEmail) {
    errors.senderEmail = 'Sender Email field is required.'
  }
   if (values.senderEmail && !REGEX.EMAIL.test(values.senderEmail)) {
    errors.senderEmail = USER_ERROR.EMAIL_INVALID
  }
  if (!values.receiptEmail) {
    errors.receiptEmail = 'Receipt Email field is required.'
  }
  if (values.receiptEmail && !REGEX.EMAIL.test(values.receiptEmail)) {
    errors.receiptEmail = USER_ERROR.EMAIL_INVALID
  }
  if (!values.ipAddress) {
    errors.ipAddress = 'IPaddress field is required.'
  }
  if (!values.deviceAddress) {
    errors.deviceAddress = 'DeviceAddress field is required.'
  }
  if (!values.disputeAmount) {
    errors.disputeAmount = 'Dispute Amount field is required.'
  }
  if (!values.enqueueHrs) {
    errors.enqueueHrs = 'Enqueue Hrs field is required.'
  }
  if (!values.score) {
    errors.score = 'Score field is required.'
  }
  if (!values.accountType) {
    errors.accountType = 'Account Type field is required.'
  }
  if (!values.enqueueReason) {
    errors.enqueueReason = 'Enqueue Reason field is required.'
  }
  setErrors(errors); 
  return errors;
}