import { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { toAbsoluteUrl } from '../../theme/helpers'
import { colors } from '../../utils/constants'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  AMLPostActions,
  riskManagementActions
} from '../../store/actions'
import { STATUS_RESPONSE, DATE } from '../../utils/constants'
import _ from 'lodash'
import 'react-phone-input-2/lib/style.css'
import color from '../../utils/colors'
import { AMLValidation } from './validation'
import { warningAlert, confirmationAlert } from "../../utils/alerts"
import { DateSelector } from '../../theme/layout/components/DateSelector'

function AddAML(props) {
  const {
    loading,
    AMLPostDisatch,
    riskmgmtlists,
    getRiskManagementlistDispatch,
    AMLpostSuccess,
    AMLClearDisatch
  } = props

  const [SelectedTXNidOption, setSelectedTXNidOption] = useState('')
  const [TXNidOption, setTXNidOption] = useState()
  const [formData, setFormData] = useState({
    transactionId: '',
    transactionDate: '',
    transactionAmount: '',
    transactionStatus: 'Success',
    senderEmail: '',
    receiptEmail: '',
    ipAddress: '',
    deviceAddress: '',
    disputeAmount: '',
    enqueueHrs: '',
    score: '',
    accountType: '',
    enqueueReason: ''
  })

  const [errors, setErrors] = useState({
    transactionId: '',
    transactionDate: '',
    transactionAmount: '',
    transactionStatus: 'Success',
    senderEmail: '',
    receiptEmail: '',
    ipAddress: '',
    deviceAddress: '',
    disputeAmount: '',
    enqueueHrs: '',
    score: '',
    accountType: '',
    enqueueReason: ''
  })
  const [show, setShow] = useState(false)

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    getRiskManagementlistDispatch()
  }, [])

  const handleSubmit = (e) => {
    const errors = AMLValidation(formData, setErrors)
    if (_.isEmpty(errors)) {
      AMLPostDisatch(formData)
    }
  }

  const handleChange = (e) => {
    setErrors({ ...errors, [e.target.name]: '' })
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  useEffect(() => {
    if (AMLpostSuccess && AMLpostSuccess.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        AMLpostSuccess && AMLpostSuccess.message,
        'success',
        'Back to AML',
        'Ok',
        () => { onConfirm() },
        () => { clear() 
        },
        AMLClearDisatch()
      )
    } else if (AMLpostSuccess && AMLpostSuccess.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        AMLpostSuccess && AMLpostSuccess.message,
        '',
        'Try again',
        '',
        () => { },
        AMLClearDisatch()
      )
    }
  }, [AMLpostSuccess])

  const onConfirm = () => {
    AMLClearDisatch()
    props.history.push('/aml-queue')
    setFormData({
      transactionId: '',
      transactionDate: '',
      transactionAmount: '',
      transactionStatus: 'Success',
      senderEmail: '',
      receiptEmail: '',
      ipAddress: '',
      deviceAddress: '',
      disputeAmount: '',
      enqueueHrs: '',
      score: '',
      accountType: '',
      enqueueReason: ''
    })
  }

  const clear = () => {
    AMLClearDisatch()
    setFormData({
      transactionId: '',
      transactionDate: '',
      transactionAmount: '',
      transactionStatus: 'Success',
      senderEmail: '',
      receiptEmail: '',
      ipAddress: '',
      deviceAddress: '',
      disputeAmount: '',
      enqueueHrs: '',
      score: '',
      accountType: '',
      enqueueReason: ''
    })
  }

  const TXNidData = riskmgmtlists && riskmgmtlists.data
  useEffect(() => {
    const IdOption = getDefaultOption(TXNidData)
    setTXNidOption(IdOption)
  }, [TXNidData])

  const getDefaultOption = (TXNidData) => {
    const defaultOptions = []
    for (const item in TXNidData) {
      defaultOptions.push({ label: TXNidData[item].companyName, value: TXNidData[item].fieldType })
    }
    return defaultOptions
  }

  return (
    <>

      <div
        className='d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed overflow-hidden'
        style={{
          backgroundImage: `url(${toAbsoluteUrl(
            '/media/illustrations/sketchy-1/14.png'
          )})`,
          backgroundColor: colors.oxfordBlue
        }}
      >
        {/* begin::Wrapper */}
        <div className='row mb-4'>
          {/* begin::Content */}
          <div className='d-flex flex-center flex-column flex-column-fluid p-10'>
            {/* begin::Logo */}
            <a href='#' className='mb-12'>
              <img
                alt='Logo'
                // src={toAbsoluteUrl('/media/loginImage/MicrosoftTeams-image.png')}
                src={toAbsoluteUrl('/media/loginImage/mshield_logo.png')}
                className='h-65px'
              />
            </a>
          </div>
          {/* end::Logo */}
          <div className='card card-xl-stretch'>
            <div className='row mt-10 mb-8'>
              <div className='col-lg-12'>
                <h1 className='text-dark mb-3 d-flex justify-content-center'>Add AML</h1>
              </div>
            </div>
            <div className='card-body pt-0'>
              <div className='row'>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <form className='container-fixed'>
                    <div className='card-header'>
                      <div className='card-body'>
                        <div className='form-group row mb-4'>
                        <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              Transaction ID:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Transaction ID'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.transactionId && errors.transactionId },
                                  {
                                    'is-valid': formData.transactionId && !errors.transactionId
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='transactionId'
                                autoComplete='off'
                              />
                              {errors.transactionId && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.transactionId}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              Transaction Date:
                            </label>
                            <div className='col-lg-11'>
                              <DateSelector
                                name='transactionDate'
                                placeholder='Transaction Date'
                                className='form-control'
                                selected={formData.transactionDate || ''}
                                onChange={(date) => {
                                  setErrors({ ...errors, transactionDate: '' })
                                  setFormData((values) => ({
                                    ...values,
                                    transactionDate: date
                                  }))
                                }}
                                dateFormat={DATE.DATE_FOR_PICKER}
                                peek={true}
                                monthDropdown={true}
                                yearDropdown={true}
                                showYear={true}
                              />
                              {errors && errors.transactionDate && (
                                <div className='fv-plugins-message-container text-danger ms-2'>
                                  <div className='fv-help-block'>
                                    <span role='alert'>{errors && errors.transactionDate}</span>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              Transaction Amount:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Transaction Amount'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.transactionAmount && errors.transactionAmount },
                                  {
                                    'is-valid': formData.transactionAmount && !errors.transactionAmount
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='transactionAmount'
                                autoComplete='off'
                              />
                              {errors.transactionAmount && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.transactionAmount}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              Sender Email:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Sender Email'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.senderEmail && errors.senderEmail },
                                  {
                                    'is-valid': formData.senderEmail && !errors.senderEmail
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='senderEmail'
                                autoComplete='off'
                              />
                              {errors.senderEmail && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.senderEmail}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              Receipt Email:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Receipt Email'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.receiptEmail && errors.receiptEmail },
                                  {
                                    'is-valid': formData.receiptEmail && !errors.receiptEmail
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='receiptEmail'
                                autoComplete='off'
                              />
                              {errors.receiptEmail && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.receiptEmail}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              IP Address:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='IP Address'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.ipAddress && errors.ipAddress },
                                  {
                                    'is-valid': formData.ipAddress && !errors.ipAddress
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='ipAddress'
                                autoComplete='off'
                              />
                              {errors.ipAddress && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.ipAddress}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                              Device Address:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Device Address'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.deviceAddress && errors.deviceAddress },
                                  {
                                    'is-valid': formData.deviceAddress && !errors.deviceAddress
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='deviceAddress'
                                autoComplete='off'
                              />
                              {errors.deviceAddress && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.deviceAddress}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                            Dispute Amount :
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Dispute Amount'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.disputeAmount && errors.disputeAmount },
                                  {
                                    'is-valid': formData.disputeAmount && !errors.disputeAmount
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='disputeAmount'
                                autoComplete='off'
                              />
                              {errors.disputeAmount && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.disputeAmount}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                            Enqueue Hrs :
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Enqueue Hrs'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.enqueueHrs && errors.enqueueHrs },
                                  {
                                    'is-valid': formData.enqueueHrs && !errors.enqueueHrs
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='enqueueHrs'
                                autoComplete='off'
                              />
                              {errors.enqueueHrs && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.enqueueHrs}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                            Score:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Score'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.score && errors.score },
                                  {
                                    'is-valid': formData.score && !errors.score
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='score'
                                autoComplete='off'
                              />
                              {errors.score && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.score}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                            Account Type:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Account Type'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.accountType && errors.accountType },
                                  {
                                    'is-valid': formData.accountType && !errors.accountType
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='accountType'
                                autoComplete='off'
                              />
                              {errors.accountType && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.accountType}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='col-lg-3 mb-3'>
                            <label className='font-size-xs font-weight-bold mb-3  form-label'>
                            Enqueue Reason:
                            </label>
                            <div className='col-lg-11'>
                              <input
                                placeholder='Enqueue Reason'
                                className={clsx(
                                  'form-control form-control-lg form-control-solid',
                                  { 'is-invalid': formData.enqueueReason && errors.enqueueReason },
                                  {
                                    'is-valid': formData.enqueueReason && !errors.enqueueReason
                                  }
                                )}
                                onChange={(e) => handleChange(e)}
                                type='text'
                                name='enqueueReason'
                                autoComplete='off'
                              />
                              {errors.enqueueReason && (
                                <div className='fv-plugins-message-container text-danger'>
                                  <span role='alert text-danger'>{errors.enqueueReason}</span>
                                </div>
                              )}
                            </div>
                          </div>
                          <div className='form-group row mb-4'>
                            <div className='col-lg-6' />
                            <div className='col-lg-6'>
                              <div className='col-lg-11'>
                                <button
                                  type='button'
                                  className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                                  onClick={(e) => handleSubmit(e)}
                                  disabled={loading}
                                >
                                  {!loading && <span className='indicator-label'>Submit</span>}
                                  {loading && (
                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                      Please wait...
                                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                    </span>
                                  )}
                                </button>
                                <Link
                                  to='/aml-queue'
                                  disabled={loading}
                                  className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                                >
                                  Back To AML List
                                </Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  const { riskManagementlistStore, AMLpostTypeStore } = state

  return {
    riskmgmtlists:riskManagementlistStore && riskManagementlistStore.riskmgmtlists ? riskManagementlistStore.riskmgmtlists : '',
    AMLpostSuccess:AMLpostTypeStore && AMLpostTypeStore.AMLpost ? AMLpostTypeStore.AMLpost : '',
  }
}

const mapDispatchToProps = dispatch => ({
  AMLPostDisatch: (data) => dispatch(AMLPostActions.getAMLPost(data)),
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params)),
  AMLClearDisatch: (data) => dispatch(AMLPostActions.clearAMLPost(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddAML)
