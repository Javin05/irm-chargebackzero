import React, { useEffect, useState, useRef } from 'react'
import _, { values } from 'lodash'
import {
  webReportSettingActions,
  webReportSettingAddAction,
  webReportSettingGetIdActions,
  updatewebReportSettingActions,
  webReportSettingDeleteActions
} from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper'
import { KTSVG } from '../../theme/helpers'
import FindRole from '../wrmManagement/Role'
import {
  SWEET_ALERT_MSG,
  STATUS_BADGE,
  STATUS_RESPONSE
} from '../../utils/constants'
import Modal from 'react-bootstrap/Modal'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"

function WrmRtWebReport(props) {
  const {
    getwebReportSettinglistDispatch,
    webReportSettingslists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    webReportSettingAddDispatch,
    webReportSettingsAdd,
    addLoading,
    clearwebReportSettingAddDispatch,
    getwebReportSettingIdDetailsDispatch,
    webReportSettingsIdDetail,
    clearwebReportSettingIdDetailsDispatch,
    updateLoading,
    updatewebReportSettingDispatch,
    updatewebReportSettingResponce,
    clearupdatewebReportSettingDispatch,
    webReportSettingDeleteDispatch,
    clearwebReportSettingDeleteDispatch,
    DeletewebReportSetting,
    reportValue
  } = props

  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [searchParams, setSearchParams] = useState({})
  const [show, setShow] = useState(false)
  const [editshow, seteditshow] = useState(false)
  const [searchShow, setSearchShow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    report_key: "",
    report_value: "",
    report_type: reportValue
  })
  const [searchData, setSearchData] = useState({
    report_key: "",
    report_value: "",
    report_type: reportValue
  })

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
      report_type: reportValue
    }
    getwebReportSettinglistDispatch(params)
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      const pickByParams = _.pickBy(params)
      getwebReportSettinglistDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
    }
  }, [setFilterFunction, setCredFilterParams])


  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      report_key: searchData.report_key ? searchData.report_key : '',
      report_value: searchData.report_value ? searchData.report_value : '',
      report_type: searchData.report_type ? searchData.report_type : '',
    }
    setActivePageNumber(pageNumber)
    getwebReportSettinglistDispatch(params)
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const handelChange = (event) => {
    const { name, value } = event.target
    setFormData(values => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const searchChange = (event) => {
    const { name, value } = event.target
    setSearchData(values => ({ ...values, [name]: value }))
  }

  const Search = () => {
    const params = {
      limit: limit,
      page: activePageNumber,
      report_type: searchData.report_type,
      report_key: searchData.report_key,
      report_value: searchData.report_value
    }
    getwebReportSettinglistDispatch(params)
    setSearchShow(false)
  }

  const Postsubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.report_key)) {
      errors.report_key = "Report Key Is Required"
    }
    if (_.isEmpty(formData.report_value)) {
      errors.report_value = "Report Value Is Required"
    }
    if (editshow) {
      if (_.isEmpty(errors)) {
        const data = webReportSettingsIdDetail && webReportSettingsIdDetail.data
        updatewebReportSettingDispatch(data && data._id, formData)
      }
    } else {
      if (_.isEmpty(errors)) {
        webReportSettingAddDispatch(formData)
      }
    }
    setErrors(errors)
  }

  const onEditItm = (data) => {
    const params = {
      id: data,
      report_type: reportValue,
    }
    getwebReportSettingIdDetailsDispatch(params)
    setShow(true)
  }

  useEffect(() => {
    if (webReportSettingsAdd && webReportSettingsAdd.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        webReportSettingsAdd && webReportSettingsAdd.message,
        'success'
      )
      clearwebReportSettingAddDispatch()
      setShow(false)
      setFormData({
        report_key: '',
        report_value: ''
      })
      const params = {
        limit: limit,
        page: activePageNumber,
        report_type: reportValue
      }
      getwebReportSettinglistDispatch(params)
    } else if (webReportSettingsAdd && webReportSettingsAdd.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        webReportSettingsAdd && webReportSettingsAdd.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearwebReportSettingAddDispatch()
    }
  }, [webReportSettingsAdd])

  useEffect(() => {
    if (webReportSettingsIdDetail && webReportSettingsIdDetail.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = webReportSettingsIdDetail && webReportSettingsIdDetail.data
      setFormData(values => ({
        ...values,
        report_key: data && data.report_key,
        report_value: data && data.report_value
      }))
      seteditshow(true)
    }
  }, [webReportSettingsIdDetail])

  
  useEffect(() => {
    if (updatewebReportSettingResponce && updatewebReportSettingResponce.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        updatewebReportSettingResponce && updatewebReportSettingResponce.message,
        'success'
      )
      clearupdatewebReportSettingDispatch()
      setShow(false)
      seteditshow(false)
      setFormData({
        report_key: '',
        report_value: ''
      })
      const params = {
        limit: limit,
        page: activePageNumber,
        report_type: reportValue
      }
      getwebReportSettinglistDispatch(params)
    } else if (updatewebReportSettingResponce && updatewebReportSettingResponce.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        updatewebReportSettingResponce && updatewebReportSettingResponce.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearupdatewebReportSettingDispatch()
    }
  }, [updatewebReportSettingResponce])

  
  const onConfirmUpdate = (params) => {
    const data ={
      id : params,
      report_type: reportValue
    }
    webReportSettingDeleteDispatch(data)
  }

  const onDeleteItem = (params) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_REPORT,
      'warning',
      'Yes',
      'No',
      () => { onConfirmUpdate(params) },
      () => { }
    )
  }

  useEffect(() => {
    if (DeletewebReportSetting && DeletewebReportSetting.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        DeletewebReportSetting && DeletewebReportSetting.message,
        'success'
      )
      clearwebReportSettingDeleteDispatch()
      setShow(false)
      seteditshow(false)
      setFormData({
        report_key: '',
        report_value: ''
      })
      const params = {
        limit: limit,
        page: activePageNumber,
        report_type: reportValue
      }
      getwebReportSettinglistDispatch(params)
    } else if (DeletewebReportSetting && DeletewebReportSetting.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DeletewebReportSetting && DeletewebReportSetting.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearwebReportSettingDeleteDispatch()
    }
  }, [DeletewebReportSetting])

  const clear = () => {
    setShow(false)
    setFormData(values => ({
      ...values,
      report_key: '',
      report_value: ''
    }))
    clearwebReportSettingIdDetailsDispatch()
    seteditshow(false)
  }

  const totalPages =
    webReportSettingslists && webReportSettingslists.data && webReportSettingslists.data.count
      ? Math.ceil(parseInt(webReportSettingslists && webReportSettingslists.data && webReportSettingslists.data.count) / limit)
      : 1
  return (
    <>
    
    <Modal
        show={searchShow}
        size="lg"
        centered
        onHide={() => {
          setSearchShow(false)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(167 199 230), rgb(188 204 237))", "color": "#ffffff" }}
          closeButton={() => setSearchShow(false)}>
          <Modal.Title>Search</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Key :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => searchChange(e)}
                  name='report_key'
                  value={searchData.report_key}
                  placeholder='Report Key'
                  className="form-control form-control-solid" />
                {errors && errors.report_key && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_key}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Value :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => searchChange(e)}
                  name='report_value'
                  value={searchData.report_value}
                  placeholder='Report Value'
                  className="form-control form-control-solid" />
                {errors && errors.report_value && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_value}
                  </div>
                )}
              </div>
            </div>

            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <div className='d-flex justify-content-end'>
                  <button
                    className='btn btn-sm btn-light-primary btn-responsive me-2 mt-4'
                    onClick={Search}
                  >
                    Search
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => {
          setShow(false)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(167 199 230), rgb(188 204 237))", "color": "#ffffff" }}
          closeButton={() => clear()}>
          <Modal.Title>{editshow ? "Update" : "Add"}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Key :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => handelChange(e)}
                  name='report_key'
                  value={formData.report_key}
                  placeholder='Report Key'
                  className="form-control form-control-solid" />
                {errors && errors.report_key && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_key}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-4">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Value :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  onChange={(e) => handelChange(e)}
                  name='report_value'
                  value={formData.report_value}
                  placeholder='Report Value'
                  className="form-control form-control-solid" />
                {errors && errors.report_value && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.report_value}
                  </div>
                )}
              </div>
            </div>

            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <div className='d-flex justify-content-end'>
                  {
                    editshow ? (
                      <button
                        className='btn btn-sm btn-light-primary btn-responsive me-2 mt-4'
                        onClick={Postsubmit}
                        disabled={updateLoading}
                      >
                        {!updateLoading && <span className='indicator-label'>
                          Update
                        </span>
                        }
                        {updateLoading && (
                          <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    ) : (
                      <button
                        className='btn btn-sm btn-light-primary btn-responsive me-2 mt-4'
                        onClick={Postsubmit}
                        disabled={addLoading}
                      >

                        {!addLoading && <span className='indicator-label'>
                          Submit
                        </span>
                        }
                        {addLoading && (
                          <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className='card'>
        <div className="row">
          <div className='col-md-3'>
            {webReportSettingslists && webReportSettingslists.data && webReportSettingslists.data.count && (
              <span className='text-muted fw-bold d-flex fs-3 mt-2 ms-4'>
                Total:
                <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                  {webReportSettingslists && webReportSettingslists.data && webReportSettingslists.data.count}
                </span>
              </span>
            )}
          </div>
          <div className='col-md-9 mt-4'>
            <div className="d-flex justify-content-end">
              {
                <FindRole
                  role={Role}
                >
                  <button
                    className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                    onClick={() => setShow(true)}
                  >
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    {reportValue ==="Realtime Appstore Report"?"Add AppStore Report" :reportValue ==="Realtime Playstore Report" ? "Add PlayStore Report" :"Add WebReport"}
                  </button>
                </FindRole>
              }
              <button
                className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                onClick={() => setSearchShow(true)}
              >
                <KTSVG path='/media/icons/duotune/general/gen021.svg' />
                Search
              </button>
            </div>
          </div>
        </div>
        <div className='card-body py-3'>
          <div className="table-responsive">
            <table className="table gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>S.No</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Report Key</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Report Value</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-200px text-start">
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                  </FindRole>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      webReportSettingslists &&
                        webReportSettingslists.data &&
                        _.isArray(webReportSettingslists.data.result)
                        ? (
                          webReportSettingslists.data.result.map((item, i) => {
                            return (
                              <tr
                                key={i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="pb-0 pt-5  text-start">
                                  {(activePageNumber-1)*limit +(i+1)}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item && item.report_key ? item.report_key : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {item && item.report_value ? item.report_value : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  <span className={`badge ${STATUS_BADGE[item.status]}`}>
                                    {item && item.status ? item.status : "--"}
                                  </span>
                                </td>
                                <FindRole
                                  role={Role}
                                >
                                  <td className="pb-0 pt-5  text-start">
                                    <button
                                      className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
                                      onClick={(e) => onEditItm(item._id)}
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                        title="Edit BlockList"
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                      onClick={() => onDeleteItem(item._id)}
                                      title="Delete BlockList"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                </FindRole>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { webReportSettingslistStore, webReportSettingsAddStore, updatewebReportSettingStore, DeletewebReportSettingStore } = state
  return {
    webReportSettingslists: state && state.webReportSettingslistStore && state.webReportSettingslistStore.webReportSettingslists,
    loading: webReportSettingslistStore && webReportSettingslistStore.loading ? webReportSettingslistStore.loading : false,
    webReportSettingsAdd: state && state.webReportSettingsAddStore && state.webReportSettingsAddStore.webReportSettingsAdd,
    addLoading: webReportSettingsAddStore && webReportSettingsAddStore.loading ? webReportSettingsAddStore.loading : false,
    webReportSettingsIdDetail: state && state.editwebReportSettingsStore && state.editwebReportSettingsStore.webReportSettingsIdDetail,
    updateLoading: updatewebReportSettingStore && updatewebReportSettingStore.loading ? updatewebReportSettingStore.loading : false,
    updatewebReportSettingResponce: updatewebReportSettingStore && updatewebReportSettingStore.updatewebReportSettingResponce ? updatewebReportSettingStore.updatewebReportSettingResponce : '',
    DeletewebReportSetting: DeletewebReportSettingStore && DeletewebReportSettingStore.DeletewebReportSetting ? DeletewebReportSettingStore.DeletewebReportSetting : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getwebReportSettinglistDispatch: (params) => dispatch(webReportSettingActions.getwebReportSettinglist(params)),
  webReportSettingAddDispatch: (params) => dispatch(webReportSettingAddAction.webReportSettingAdd(params)),
  clearwebReportSettingAddDispatch: (params) => dispatch(webReportSettingAddAction.clearwebReportSetting(params)),
  getwebReportSettingIdDetailsDispatch: (params) => dispatch(webReportSettingGetIdActions.getwebReportSettingIdDetails(params)),
  clearwebReportSettingIdDetailsDispatch: (params) => dispatch(webReportSettingGetIdActions.clearwebReportSettingIdDetails(params)),
  updatewebReportSettingDispatch: (id, params) => dispatch(updatewebReportSettingActions.updatewebReportSetting(id, params)),
  clearupdatewebReportSettingDispatch: (id, params) => dispatch(updatewebReportSettingActions.clearupdatewebReportSetting(id, params)),
  webReportSettingDeleteDispatch: (id) => dispatch(webReportSettingDeleteActions.webReportSettingDelete(id)),
  clearwebReportSettingDeleteDispatch: (id) => dispatch(webReportSettingDeleteActions.clearwebReportSettingDelete(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(WrmRtWebReport)