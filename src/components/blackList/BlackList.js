import React, { useEffect, useState, useRef } from 'react'
import {
  rulesActions,
  queuesActions,
  BlockListActions,
  BlockListEmailActions,
  EmailBlockListActions,
  BlockListUploadActions,
  BlockListEditActions,
  BlockListDeleteActions,
  BlockListUpdateActions,
  clientCredFilterActions,
  clientIdLIstActions,
  DeleteClientActions,
  BlockListExportActions
} from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper';
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { SET_FILTER, SWEET_ALERT_MSG, STATUS_BADGE, DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT, STATUS_RESPONSE } from '../../utils/constants'
import { warningAlert, confirmationAlert } from "../../utils/alerts"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Modal from 'react-bootstrap/Modal'
import { userValidation } from './validation'
import Select from 'react-select'
import Search from './Search'
import FindRole from '../wrmManagement/Role'
import { CSVLink } from "react-csv";
import ReactHTMLTableToExcel from "react-html-table-to-excel"

function BlackList(props) {
  const {
    loading,
    clearRulesDispatch,
    getQueueslistDispatch,
    getBlockListlistDispatch,
    BlockListlists,
    getBlockListTypeDispatch,
    BlockListType,
    blockEmailDispatch,
    BlockEmailSuccess,
    BlockListUploadsDispatch,
    QueuesLists,
    BlocklistsUploads,
    clearBlockListUpload,
    blockEmailDispatchClear,
    DeletegetBlocklistDispatch,
    DelteBlockList,
    clearBlockListlistDispatch,
    clearUpdateBlockList,
    BlockListUpdateDispatch,
    BlockListUpdateSuccess,
    BlockListCount,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    clinetIdLists,
    clientIdDispatch,
    DeleteClientDispatch,
    DeleteBlockListClient,
    DeleteClientClearDispatch,
    getBlockListExport,
    BlockListExport,
    BlockListloading,
  } = props

  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const hiddenFileInput = useRef(null);
  const didMount = React.useRef(false)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({});
  const [limit, setLimit] = useState(25)
  const [page, setPage] = useState(1)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [exportShow, setExportShow] = useState(false);
  const [exportReport, setExportReport] = useState(false);
  const totalPages = BlockListCount && Math.ceil(parseInt(BlockListCount) / limit)
  const [queue, setQueue] = useState();
  const [blacklist, setBlacklist] = useState();
  const [QueueOptions, setQueueOptions] = useState(null)
  const [BlackListOptions, setBlackListOptions] = useState(null)
  const [queueDefaut, setQueueDefault] = useState(null);
  const [blackListDefault, setBlackListDefault] = useState(null);
  const [fileName, setFileName] = useState();
  const [editItem, setEditItem] = useState(null);
  const [blacklistPopup, setBlacklistPopup] = useState(false);
  const [tabDefault, setTabDefault] = useState('Manuval');
  const [editQueue, setEditQueue] = useState(null);
  const [editBlacklist, setEditBlacklist] = useState(null);
  const [blacklistLable, setBlacklistLabel] = useState("--");
  const [showEdit, setshowEdit] = useState(false)
  const [searchData, setSearchData] = useState({
    fieldName: "",
    fieldValue: "",
    queueId: "",
    fieldType: "",  
  })
  const [searchDataShow, setSearchDataShow] = useState(false)
  const [ShowDelete, setShowDelete] = useState(false)
  const [createForm, setCreateForm] = useState(false)
  //client 
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const [IndidualOption, setIndidualOption] = useState()
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState('')
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    fieldName: "",
    queueId: "",
    fieldType: "",
    fieldData: "",
    file: "",
    clientId: "",
    mcc_code: "",
    risk_classification: "",
    risk_catagory_status: "",
    manual_review: "",
    risk_level: ""
  })
  const [sorting, setSorting] = useState({
    ruleId: false,
    rulefield: false,
    ruleDescription: false,
    clientId: false
  })

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
  }, [])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : "#000",
      background: state.isSelected ? color.white : color.white,
    }),
  }

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  }
  const tabToggle = (tab) => {
    setTabDefault(tab)

    setCreateForm(false)
    setEditItem(null)
    setBlacklistLabel(null)
    setEditQueue(null)
    setEditBlacklist(null)
    setFormData(values => ({
      ...values,
      fieldName: '',
      queueId: '',
      fieldType: '',
      file: '',
      fieldData: '',
      mcc_code: "",
      risk_classification: "",
      risk_catagory_status: "",
      manual_review: "",
      risk_level: ""
    }))
    setErrors({})
  }
  const PopupToggle = (toggle) => {
    if (!toggle) {
      setCreateForm(false)
      setEditItem(null)
      setBlacklistLabel(null)
      setEditQueue(null)
      setEditBlacklist(null)
      setSelectedIndidualOption(null)
      setSelectedAsigneesOption(null)
      setFormData(values => ({
        ...values,
        fieldName: '',
        queueId: '',
        fieldType: '',
        file: '',
        fieldData: '',
        clientId: '',
        mcc_code: "",
        risk_classification: "",
        risk_catagory_status: "",
        manual_review: "",
        risk_level: ""
      }))
    }
    else {
      setCreateForm(true)
    }
    setBlacklistPopup(toggle)
  }
  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: activePageNumber,
      queueId: queue,
      fieldType: blacklist,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    getBlockListTypeDispatch(params)
  }

  const handlePageClick = (event) => {
    if (searchDataShow) {
      const pageNumber = event.selected + 1
      setActivePageNumber(pageNumber)
      const params = {
        limit: limit,
        page: pageNumber,
        fieldName: searchData && searchData.fieldName,
        fieldValue: searchData && searchData.fieldValue,
        queueId: searchData && searchData.queueId,
        fieldType: searchData && searchData.fieldType,
        clientId: searchData && searchData.clientId
          ? searchData && searchData.clientId
          : ""
      }
      setActivePageNumber(pageNumber)
      getBlockListTypeDispatch(params)
    } else {
      const pageNumber = event.selected + 1
      setActivePageNumber(pageNumber)
      const params = {
        limit: limit,
        page: pageNumber,
        queueId: queue,
        fieldType: blacklist,
        clientId: credBasedClientValue
          ? credBasedClientValue
          : ""
      }
      setActivePageNumber(pageNumber)
      getBlockListTypeDispatch(params)
    }
  }
  const SortingHandler = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})

      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getBlockListTypeDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})

      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getBlockListTypeDispatch(params)
    }
  }
  const onEditItem = (e, editRow) => {
    setEditItem(editRow)
    setBlacklistLabel(editRow.fieldName)
    const queueId = editRow.queueId && editRow.queueId._id
    const clientIdData = editRow.clientId && editRow.clientId._id

    let selectedClient = AsigneesOption.filter(q => q.value === clientIdData)
    setSelectedIndidualOption(selectedClient[0])

    let selectedQueue = QueueOptions.filter(q => q.value === queueId)
    setEditQueue(selectedQueue[0])

    let selectedBlacklist = BlackListOptions.filter(b => b.value === editRow.fieldType)
    setEditBlacklist(selectedBlacklist[0])

    setFormData(values => ({
      ...values,
      currentId: editRow._id,
      fieldName: editRow.fieldName,
      queueId: editRow.queueId && editRow.queueId._id,
      clientId: editRow.clientId && editRow.clientId._id,
      fieldType: editRow.fieldType,
      file: '',
      fieldData: editRow.fieldValue,
      mcc_code: editRow && editRow.extraFields && editRow.extraFields.mcc_code,
      risk_classification: editRow && editRow.extraFields && editRow.extraFields.risk_classification,
      risk_catagory_status: editRow && editRow.extraFields && editRow.extraFields.risk_catagory_status,
      manual_review: editRow && editRow.extraFields && editRow.extraFields.manual_review,
      risk_level: editRow && editRow.extraFields && editRow.extraFields.risk_level
    }))
    setshowEdit(true)
    PopupToggle(true)
  }
  const QueueChangeHandler = (e) => {
    if (editItem === null && createForm === false) {
      setQueue(e.value)
      setQueueDefault(e)
      localStorage.setItem('QueueValue', e.value);
    }
    else if (editItem !== null || createForm !== false) {
      setEditQueue(e)
      setFormData(values => ({ ...values, queueId: e.value }))
      setErrors((values) => ({ ...values, queueId: "" }))
      return;
    }

    const params = {
      queueId: e && e.value,
      fieldType: blacklist,
      limit: limit,
      page: page,
      ...searchParams,
      clientId: credBasedClientValue ? credBasedClientValue : ''
    }

    getBlockListTypeDispatch(params)
  }
  const BlacklistChangeHandler = (e) => {
    if (editItem === null && createForm === false) {
      setBlacklist(e.value)
      setBlackListDefault(e)
      localStorage.setItem('blackTypevalue', e.value);
    }
    else if (editItem !== null || createForm !== false) {
      setEditBlacklist(e)
      setBlacklistLabel(e.label)
      setFormData((values) => ({
        ...values,
        fieldName: e.label,
        fieldType: e.value,
      }))
      setErrors((values) => ({ ...values, fieldName: "" }))
      return;
    }

    const params = {
      fieldType: e && e.value,
      queueId: queue,
      limit: limit,
      page: page,
      ...searchParams,
      clientId: credBasedClientValue ? credBasedClientValue : ''
    }
    getBlockListTypeDispatch(params)
  }

  const FielddataChangeHandler = (event) => {
    setFormData(values => ({ ...values, fieldData: event.target.value }))
    setErrors({ ...errors, ["fieldData"]: '' })
  }

  const handelChange = (event) => {
    const { name, value } = event.target
    setFormData(values => ({ ...values, [name]: value }))
  }
  const FileChangeHandler = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setFormData((values) => ({
          ...values,
          file: files,
        }));
        setErrors((values) => ({ ...values, file: "" }));
        setFileName(files && files.name);
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  }
  const SubmitHandlerManual = (event) => {
    event.preventDefault();
    // Validate
    if (formData.clientId === "") {
      setErrors({
        ...errors,
        ["clientId"]: `Client Is Required`
      })
    }
    else if (formData.queueId === "") {
      setErrors({
        ...errors,
        ["queueId"]: `Queue Is Required`
      })
    }
    else if (formData.fieldName === "") {
      setErrors({
        ...errors,
        ["fieldName"]: `${formData.fieldName} Field Name Is Required`,
      })
    }
    else if (formData.fieldData === "" || formData.fieldData === undefined) {
      setErrors({
        ...errors,
        ["fieldData"]: `${formData.fieldName} Is Required`,
      })
    }
    // Form Submit Process
    if (formData.queueId !== '' && formData.fieldName !== '' && formData.fieldData !== '') {
      if (editItem) {
        let update_data = {
          "fieldType": formData.fieldType,
          "fieldValue": formData.fieldData,
          "clientId": formData.clientId,
        }
        let websiteUpadte = {
          "fieldType": formData.fieldType,
          "fieldValue": formData.fieldData,
          "clientId": formData.clientId,
          "mcc_code": formData.mcc_code,
          "risk_classification": formData.risk_classification,
          "risk_catagory_status": formData.risk_catagory_status,
          "manual_review": formData.manual_review,
          "risk_level": formData.risk_level,
        }
        if (formData.fieldType === 'Website_Category') {
          BlockListUpdateDispatch(formData.currentId, websiteUpadte)
        } else {
          BlockListUpdateDispatch(formData.currentId, update_data)
        }
      }
      else {
        let filteredArray = [];
        let f_array = formData.fieldData.split(',');

        f_array.forEach(element => {
          let f_value = element.trim();
          filteredArray.push({
            fieldValue: f_value,
          });
        });
        let data = {
          "clientId": formData.clientId,
          "queueId": formData.queueId,
          "fieldName": formData.fieldName,
          "fieldType": formData.fieldType,
          "blackItems": formData.fieldType === "shipping_address" ?
            [
              {
                "fieldValue": formData.fieldData
              }
            ] : filteredArray
        }
        let websiteData = {
          "clientId": formData.clientId,
          "queueId": formData.queueId,
          "fieldName": formData.fieldName,
          "fieldType": formData.fieldType,
          "mcc_code": formData.mcc_code,
          "risk_classification": formData.risk_classification,
          "risk_catagory_status": formData.risk_catagory_status,
          "manual_review": formData.manual_review,
          "risk_level": formData.risk_level,
          "blackItems": formData.fieldType === "shipping_address" ?
            [
              {
                "fieldValue": formData.fieldData
              }
            ] : filteredArray
        }
        blockEmailDispatch(formData.fieldType === 'Website_Category' ? websiteData : data)
      }
    }
  }
  const SubmitHandlerCsv = (event) => {
    event.preventDefault();
    // Validate
    if (formData.queueId === "") {
      setErrors({
        ...errors,
        ["queueId"]: `Queue Is Required`,
      })
    }
    else if (formData.fieldName === "") {
      setErrors({
        ...errors,
        ["fieldName"]: `${formData.fieldName} Field Name Is Required`,
      })
    }

    const errorMsg = userValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("file", formData.file)
      data.append("fieldName", formData.fieldName)
      data.append("queueId", formData.queueId)
      data.append("fieldType", formData.fieldType)
      data.append("clientId", formData.clientId)

      BlockListUploadsDispatch(data)
    }
  }
  const onClear = () => {
    blockEmailDispatchClear()
    clearBlockListUpload()
    clearRulesDispatch()
    clearBlockListlistDispatch()
    clearUpdateBlockList()
  }
  const onReDispatch = () => {
    onClear()
    const params = {
      queueId: queue,
      fieldType: blacklist,
      limit: limit,
      page: page,
      ...searchParams,
    }
    getBlockListTypeDispatch(params)
  }
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_BLOCKLIST,
      'warning',
      'Yes',
      'No',
      () => {
        DeletegetBlocklistDispatch(id);
        onReDispatch()
      }
    )
  }

  useEffect(() => {
    if (BlockEmailSuccess && BlockEmailSuccess.status === "ok") {
      PopupToggle(false)
      confirmationAlert(
        'success', BlockEmailSuccess && BlockEmailSuccess.message,
        'success',
        'Back to BlockList',
        'Ok',
        () => {
          onReDispatch()
        }
      )
    }
  }, [BlockEmailSuccess])

  useEffect(() => {
    if (BlocklistsUploads && BlocklistsUploads.status === "ok") {
      PopupToggle(false)
      confirmationAlert(
        'success', BlocklistsUploads && BlocklistsUploads.message,
        'success',
        'Back to BlockList',
        'Ok',
        () => {
          onReDispatch()
        }
      )
    }
  }, [BlocklistsUploads])

  useEffect(() => {
    if (BlockListUpdateSuccess && BlockListUpdateSuccess.status === "ok") {
      PopupToggle(false)
      confirmationAlert(
        'success', BlockListUpdateSuccess && BlockListUpdateSuccess.message,
        'success',
        'Back to BlockList',
        'Ok',
        () => {
          onReDispatch()
        }
      )
    }
  }, [BlockListUpdateSuccess])

  useEffect(() => {
    if (DelteBlockList && DelteBlockList.status === "ok") {
      onReDispatch()
    }
  }, [DelteBlockList])

  useEffect(() => {
    getBlockListlistDispatch()
    getQueueslistDispatch()
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      queueId: queue,
      fieldType: blacklist,
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params);
    getBlockListTypeDispatch(pickByParams)
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1);
      const params = {
        queueId: queue,
        fieldType: blacklist,
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getBlockListTypeDispatch(pickByParams)
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
      if (credBasedClientValue) {
        setExportShow(true)
      } else {
        setExportShow(false)
      }
    }
  }, [setFilterFunction, setCredFilterParams]);

  useEffect(() => {
    let options = [];
    if (QueuesLists && QueuesLists?.data) {
      QueuesLists.data.forEach(element => {
        const obj = {
          'value': element._id,
          'label': element.queueName
        }
        options.push(obj);

        if (element._id === queue) {
          setQueueDefault(obj)
        }
      });

      setQueueOptions(options)
    }
  }, [QueuesLists])

  useEffect(() => {
    let options = [];
    if (BlockListlists && BlockListlists?.data) {
      BlockListlists.data.forEach(element => {
        const obj = {
          'value': element.fieldType,
          'label': element.fieldName
        }
        options.push(obj);

        if (element.fieldType === blacklist) {
          setBlackListDefault(obj)
        }
      });
      setBlackListOptions(options)
    }
  }, [BlockListlists])

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: '' })
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
    setIndidualOption(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const handleChangeIndidual = selectedOption => {
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setErrors({ ...errors, clientId: '' })
    }
  }

  const onConfirmUpdate = () => {
    const params = {
      clientId: formData.clientId
    }
    DeleteClientDispatch(params)
  }

  const DelteClientSubmit = () => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_CLIENT,
      'warning',
      'Yes',
      'No',
      () => { onConfirmUpdate() },
      () => { }
    )
  }

  useEffect(() => {
    const DeleteData = DeleteBlockListClient && DeleteBlockListClient.data
    if (DeleteData && DeleteData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShowDelete(false)
      confirmationAlert(
        'success', DeleteData && DeleteData.message,
        'success',
        'Back to BlockList',
        'Ok',
        () => { { } }
      )
      DeleteClientClearDispatch()
    } else if (DeleteData && DeleteData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DeleteData && DeleteData.message,
        '',
        'Try again',
        '',
        () => { }
      )
      DeleteClientClearDispatch()
    }
  }, [DeleteBlockListClient])

  const data = [
    {
      fieldValue: "/Adult",
      mcc_code: 'N/A',
      risk_classification: 'Prohibited',
      risk_level: "Prohibited",
      risk_catagory_status: 'Blacklist'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Rail Transport",
      mcc_code: '4112',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Bus & Rail",
      mcc_code: '4131',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Freight & Trucking",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Mail & Package Delivery",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Hobbies & Leisure/Water Activities/Boating",
      mcc_code: '7996',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Air Travel",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Air Travel/Airport Parking & Transportation",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Travel/Cruises & Charters",
      mcc_code: '4511',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    },
    {
      fieldValue: "/Business & Industrial/Transportation & Logistics/Maritime Transport",
      mcc_code: '4214',
      risk_classification: 'Allowable',
      risk_level: "High",
      risk_catagory_status: 'Approved - High'
    }
  ]
  
  useEffect(() => {
    if (exportReport) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        skipPagination:"true",
        fieldName: searchData && searchData.fieldName,
        fieldValue: searchData && searchData.fieldValue,
        queueId: searchData && searchData.queueId,
        fieldType: searchData && searchData.fieldType,
        clientId: currentFilterParams ? currentFilterParams : searchData && searchData.clientId
      }
      getBlockListExport(params)
    }
  }, [exportReport])

  useEffect(() => {
    if (BlockListExport && BlockListExport.status==="ok") {
      const closeXlsx = document.getElementById('blackListReport')
      closeXlsx.click()
      setExportReport(false)
    } else if (BlockListExport && BlockListExport.status==="error") {
      warningAlert(
        'error',
        BlockListExport && BlockListExport.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      setExportReport(false)
    }
  }, [BlockListExport])

  return (
    <>
      <div
        type='button'
        className='d-none'
      >
        <ReactHTMLTableToExcel
          id="blackListReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="table-to-xls">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Client</th>
              <th>Field Name</th>
              <th>Field Value</th>
              <th>Manual Review</th>
              <th>Mcc Code</th>
              <th>Risk catagory status</th>
              <th>Risk classification</th>
              <th>Risk level</th>
            </tr>
          </thead>
          <tbody>
            {
              BlockListExport && BlockListExport.data ?
                BlockListExport.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      <td>
                        {item && item.clientId && item.clientId.company}
                      </td>
                      <td>
                        {item && item.fieldName}
                      </td>
                      <td>
                        {item && item.fieldValue}
                      </td>
                      <td>
                        {item && item.extraFields && item.extraFields.manual_review}
                      </td>
                      <td>
                        {item && item.extraFields && item.extraFields.mcc_code}
                      </td>
                      <td>
                        {item && item.extraFields && item.extraFields.risk_catagory_status}
                      </td>
                      <td>
                        {item && item.extraFields && item.extraFields.risk_classification}
                      </td>
                      <td>
                        {item && item.extraFields && item.extraFields.risk_level}
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      <Modal
        show={ShowDelete}
        size="lg"
        centered
        onHide={() => {
          setShowDelete(false)
          setSelectedIndidualOption(null)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(30 128 226), rgb(61 113 222))", "color": "#ffffff" }}
          closeButton={() => setShowDelete(false)}>
          <Modal.Title>Delete Client Category</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3 form-label">
                  Client :
                </label>
              </div>
              <div className='col-md-8'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name="clientId"
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeIndidual}
                  options={IndidualOption}
                  value={SelectedIndidualOption}
                  isDisabled={!IndidualOption}
                // isClearable
                />
                {errors && errors.clientId && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.clientId}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  type="submit"
                  className='btn btn-light-primary m-1 mt-8'
                  onClick={DelteClientSubmit}
                >
                  Submit
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className={`card card-custom card-stretch gutter-b`}>
        <div className="card-header border-0 pt-5">
          <div className="d-flex align-items-center">
            <span className="text-muted font-size-sm">Record(s) per Page : &nbsp;{' '}</span>
            <div>
              <select
                className='form-select w-6rem'
                data-control='select'
                data-placeholder='Select an option'
                data-allow-clear='true'
                onChange={(e) => handleRecordPerPage(e)}>
                <option value='25' selected={limit === 25 ? true : false}>25</option>
                <option value='50' selected={limit === 50 ? true : false}>50</option>
                <option value='75' selected={limit === 75 ? true : false}>75</option>
                <option value='100' selected={limit === 100 ? true : false}>100</option>
              </select>
            </div>
          </div>
          <div className="card-toolbar">
            <div className="mr-2">
              <span className="text-muted font-size-sm">Queue</span>
              <Select
                options={QueueOptions}
                className="react-c-select"
                onChange={QueueChangeHandler}
                value={queueDefaut}
                style={{ width: "200px"}}
              />
            </div>
            <div className="mr-2 ms-4 me-4">
              <span className="text-muted font-size-sm">Blacklist</span>
              <Select
                options={BlackListOptions}
                className="react-c-select"
                onChange={BlacklistChangeHandler}
                value={blackListDefault}
                style={{ width: "200px"}}
              />
            </div>
            {
              <FindRole
                role={Role}
              >
                <div>
                  <div className="text-muted font-size-sm">&nbsp;</div>
                  {exportShow ?
                    <button
                      className="btn btn-sm btn-light-success btn-responsive font-5vw me-3 pull-right"
                      onClick={() => setExportReport(true)}
                      disabled={BlockListloading}
                    >
                      <i className="bi bi-filetype-csv" />
                      Export
                    </button> : null}
                  <button
                    className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                    onClick={() => PopupToggle(true)}
                  >
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    Add BlockList
                  </button>
                  <button
                    className='btn btn-sm btn-light-danger btn-responsive font-5vw me-3 pull-right'
                    onClick={() => setShowDelete(true)}
                  >
                    <KTSVG
                      path='/media/icons/duotune/general/gen027.svg'
                    />
                    Delete All
                  </button>
                </div>
              </FindRole>
            }
            <Search
              clinetIdLists={clinetIdLists}
              QueueOptions={QueueOptions}
              BlackListOptions={BlackListOptions}
              setSearchData={setSearchData}
              getBlockListTypeDispatch={getBlockListTypeDispatch}
              setSearchDataShow={setSearchDataShow}
              setExportShow={setExportShow}
            />
          </div>
        </div>
        <div className="card-body pt-2 pb-0 pl-5 pr-5">
          <div className="table-responsive white-wrapper">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>S.No</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => SortingHandler("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>Client</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => SortingHandler("clientId")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Field Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => SortingHandler("fieldName")}
                        >
                          <i
                            className={`bi ${sorting.fieldName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Field Value</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => SortingHandler("fieldValue")}
                        >
                          <i
                            className={`bi ${sorting.fieldValue
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <FindRole
                    role={Role}
                  >
                    <th className="min-w-200px text-start">
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                  </FindRole>
                </tr>
              </thead>
              <tbody>
                {!loading ? (
                  !_.isEmpty(BlockListType && BlockListType.data) ? (
                    BlockListType && BlockListType.data.map((item, id) => {
                      return (
                        <tr
                          key={id}
                          style={
                            id === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                          <td className="pb-0 pt-5  text-start">
                            {(activePageNumber - 1) * limit + (id + 1)}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item?.clientId?.company
                              ? item.clientId?.company
                              : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item && item.fieldName ? item.fieldName : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item && item.fieldValue ? item.fieldValue : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            <span className={`badge ${STATUS_BADGE[item.status]}`}>
                              {item && item.status ? item.status : "--"}
                            </span>
                          </td>
                          <FindRole
                            role={Role}
                          >
                            <td className="pb-0 pt-5  text-start">
                              <button
                                className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
                                onClick={(e) => onEditItem(e, item)}>
                                <KTSVG
                                  path='/media/icons/duotune/art/art005.svg'
                                  className='svg-icon-3'
                                  title="Edit BlockList"
                                />
                              </button>
                              <button
                                className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                onClick={() => onDeleteItem(item._id)}
                                title="Delete BlockList"
                              >
                                <KTSVG
                                  path='/media/icons/duotune/general/gen027.svg'
                                  className='svg-icon-3'
                                />
                              </button>
                            </td>
                          </FindRole>
                        </tr>
                      )
                    })
                  )
                    : (
                      <tr className='text-center py-3'>
                        <td colSpan='100%'>No record(s) found</td>
                      </tr>
                    )
                )
                  :
                  (
                    <tr>
                      <td colSpan='100%' className='text-center'>
                        <div
                          className='spinner-border text-primary m-5'
                          role='status'
                        />
                      </td>
                    </tr>
                  )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  forcePage={activePageNumber - 1}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        show={blacklistPopup}
        size="lg"
        centered
        onHide={() => {
          PopupToggle(false)
          setshowEdit(false)
        }
        }>
        <Modal.Header
          style={{ background: "linear-gradient(60deg, rgb(30 128 226), rgb(61 113 222))", "color": "#ffffff" }}
          closeButton={() => PopupToggle(false)}>
          <Modal.Title>{showEdit ? 'Update' : 'Add'} BlockList</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={tabDefault}
            onSelect={(k) => {
              tabToggle(k)
              setCreateForm(true)
            }}
            className="ctab ftab">
            <Tab eventKey="Manuval" title="Individual">
              <form onSubmit={SubmitHandlerManual} className="dyna-block-form">
                <div className="card card-custom card-stretch gutter-b p-8">
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3 form-label">
                        Client :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name="clientId"
                        placeholder="Select..."
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeIndidual}
                        options={IndidualOption}
                        value={SelectedIndidualOption}
                        isDisabled={!IndidualOption}
                      />
                      {errors && errors.clientId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.clientId}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3 form-label">
                        Select Queues :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={QueueChangeHandler}
                        options={QueueOptions}
                        value={editQueue}
                        isClearable={true} />
                      {errors && errors.queueId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.queueId}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row mb-8">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Select BlackList :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='fieldName'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={BlacklistChangeHandler}
                        options={BlackListOptions}
                        value={editBlacklist}
                        isDisabled={false}
                      />
                      {errors && errors.fieldName && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.fieldName}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className='col-md-4'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        {blacklistLable} :
                      </label>
                    </div>
                    <div className='col-md-8'>
                      <textarea
                        onChange={(e) => FielddataChangeHandler(e)}
                        name={editItem === null ? "--" : editItem.fieldName}
                        defaultValue={editItem === null ? "" : editItem.fieldValue}
                        className="form-control form-control-solid" />
                      {errors && errors["fieldData"] && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors["fieldData"]}
                        </div>
                      )}
                    </div>
                  </div>
                  {
                    formData.fieldType === 'Website_Category' ? (
                      <>
                        <div className="row mb-4">
                          <div className='col-md-4'>
                            <label className="font-size-xs font-weight-bold mb-3  form-label">
                              Mcc Code :
                            </label>
                          </div>
                          <div className='col-md-8'>
                            <input
                              onChange={(e) => handelChange(e)}
                              name='mcc_code'
                              value={formData.mcc_code}
                              placeholder='Mcc Code'
                              className="form-control form-control-solid" />
                          </div>
                        </div>
                        <div className="row mb-4">
                          <div className='col-md-4'>
                            <label className="font-size-xs font-weight-bold mb-3  form-label">
                              Risk Classification :
                            </label>
                          </div>
                          <div className='col-md-8'>
                            <input
                              onChange={(e) => handelChange(e)}
                              name='risk_classification'
                              placeholder='Risk Classification'
                              value={formData.risk_classification}
                              className="form-control form-control-solid" />
                          </div>
                        </div>
                        <div className="row mb-4">
                          <div className='col-md-4'>
                            <label className="font-size-xs font-weight-bold mb-3  form-label">
                              Risk Level :
                            </label>
                          </div>
                          <div className='col-md-8'>
                            <input
                              onChange={(e) => handelChange(e)}
                              name='risk_level'
                              placeholder='Risk Level'
                              value={formData.risk_level}
                              className="form-control form-control-solid" />
                          </div>
                        </div>
                        <div className="row mb-4">
                          <div className='col-md-4'>
                            <label className="font-size-xs font-weight-bold mb-3  form-label">
                              Risk Catagory Status :
                            </label>
                          </div>
                          <div className='col-md-8'>
                            <input
                              onChange={(e) => handelChange(e)}
                              name='risk_catagory_status'
                              placeholder='Risk Catagory Status'
                              value={formData.risk_catagory_status}
                              className="form-control form-control-solid" />
                          </div>
                        </div>
                        <div className="row mb-4">
                          <div className='col-md-4'>
                            <label className="font-size-xs font-weight-bold mb-3  form-label">
                              Manual Review :
                            </label>
                          </div>
                          <div className='col-md-8'>
                            <select
                              className='form-select'
                              data-control='select'
                              name='manual_review'
                              value={formData.manual_review}
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => handelChange(e)}
                            >
                              <option value=''>Select</option>
                              <option value='Yes'>Yes</option>
                              <option value='No'>No</option>
                            </select>
                          </div>
                        </div>
                      </>
                    ) : null
                  }
                  <div className="row">
                    <div className='col-md-4'>
                    </div>
                    <div className='col-md-8'>
                      <button type="submit" className='btn btn-light-primary rounded-0 m-1 mt-8'>
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </Tab>
            {
              showEdit ? '' : (
                <Tab eventKey="csvUpload" title="Bulk Upload" className="dyna-block-form">
                  <form onSubmit={SubmitHandlerCsv} className="dyna-block-form">
                    <div className="card card-custom card-stretch gutter-b p-8">
                      <div className="row mb-8">
                        <div className='col-md-4'>
                          <label className="font-size-xs font-weight-bold mb-3 form-label">
                            Client :
                          </label>
                        </div>
                        <div className='col-md-8'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='clientId'
                            className='select2'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeAsignees}
                            options={AsigneesOption}
                            value={SelectedAsigneesOption}
                            isDisabled={!AsigneesOption}
                          />
                          {errors && errors.clientId && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.clientId}
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="row mb-8">
                        <div className='col-md-4'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Select Queues :
                          </label>
                        </div>
                        <div className='col-md-8'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='queueId'
                            className='select2'
                            classNamePrefix='select'
                            handleChangeReactSelect={QueueChangeHandler}
                            options={QueueOptions}
                            value={editQueue}
                            isDisabled={!QueueOptions}
                          />
                          {errors && errors.queueId && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.queueId}
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="row mb-8">
                        <div className='col-md-4'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Select BlackList :
                          </label>
                        </div>
                        <div className='col-md-8'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='fieldName'
                            className='select2'
                            classNamePrefix='select'
                            handleChangeReactSelect={BlacklistChangeHandler}
                            options={BlackListOptions}
                            value={editBlacklist}
                            isDisabled={!BlackListOptions}
                          />
                          {errors && errors.fieldName && (
                            <div className="rr mt-1">
                              <style>{".rr{color:red}"}</style>
                              {errors.fieldName}
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="row mb-8">
                        <div className='col-md-4'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                            Upload Document :
                          </label>
                        </div>
                        <div className='col-md-8'>
                          <input
                            type="file"
                            className="d-none"
                            name="file"
                            id="file"
                            multiple={false}
                            ref={hiddenFileInput}
                            onChange={FileChangeHandler} />
                          <button type="button" style={{
                            border: "2px solid",
                            width: "100%",
                          }} className="btn btn-light-primary rounded-0 m-1" onClick={handleClick}>
                            <i className="bi bi-upload" /> &nbsp;
                            Upload Document
                          </button>
                          {errors && errors.file && (
                            <div className="rr mt-1">
                              <style>
                                {
                                  ".rr{color:red}"
                                }
                              </style>
                              {errors.file}
                            </div>
                          )}
                          {fileName && fileName}
                        </div>
                      </div>
                      <div className="row mb-8">
                        <div className='col-md-4'>
                          <label className="font-size-xs font-weight-bold mb-3  form-label">
                          </label>
                        </div>
                        <div className='col-md-8'>
                          <CSVLink
                            data={data}
                            filename={"sampleFileForImport.csv"}
                            className="btn btn-sm btn-light-success btn-responsive font-5vw"
                            target="_blank"
                          >
                            <i className="bi bi-filetype-csv" />
                            Download_Sample
                          </CSVLink>
                        </div>
                      </div>
                      <div className="row">
                        <div className='col-md-4'>
                        </div>
                        <div className='col-md-8'>
                          <button className='btn btn-light-primary rounded-0 m-1 mt-8'>
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </Tab>
              )
            }
          </Tabs>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    rulesStore,
    BlockListlistStore,
    BlockListTypeStore,
    AddEmailToBlacklistKeysone,
    queueslistStore,
    BlockListUploadlistStore,
    BlockListDeletelStore,
    BlockListUpdatelistStore,
    clinetListStore,
    DeleteClientStore,
    BlockListExportStore
  } = state
  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    loading: BlockListTypeStore && BlockListTypeStore.loading && state.BlockListTypeStore.loading,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : '',
    BlockListlists: BlockListlistStore && BlockListlistStore.BlockListlists ? BlockListlistStore.BlockListlists : {},
    BlockListExport: BlockListExportStore && BlockListExportStore.BlockListExport ? BlockListExportStore.BlockListExport : {},
    BlockListloading: BlockListExportStore && BlockListExportStore.loading ? BlockListExportStore.loading : false,
    QueuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType?.data : null,
    BlockListCount: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType?.data?.count : 0,
    BlockListUpdateSuccess: BlockListUpdatelistStore && BlockListUpdatelistStore.BlockListUpdatelists ? BlockListUpdatelistStore.BlockListUpdatelists : null,
    BlockEmailSuccess: AddEmailToBlacklistKeysone && AddEmailToBlacklistKeysone.emailSuccess ? AddEmailToBlacklistKeysone.emailSuccess : null,
    BlocklistsUploads: BlockListUploadlistStore && BlockListUploadlistStore.BlocklistsUploads ? BlockListUploadlistStore.BlocklistsUploads : null,
    DelteBlockList: BlockListDeletelStore && BlockListDeletelStore.DelteBlockList ? BlockListDeletelStore.DelteBlockList : null,
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    DeleteBlockListClient: DeleteClientStore && DeleteClientStore.DeleteBlockListClient ? DeleteClientStore.DeleteBlockListClient : '',
  }
}

const mapDispatchToProps = dispatch => ({
  deleteRulesListDispatch: (id) => dispatch(rulesActions.deleteRules(id)),
  clearRulesDispatch: () => dispatch(rulesActions.clearRules()),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  getBlockListlistDispatch: (params) => dispatch(BlockListActions.getBlockListlist(params)),
  getBlockListTypeDispatch: (params) => dispatch(BlockListEmailActions.getBlockListType(params)),
  getBlockListExport: (params) => dispatch(BlockListExportActions.getBlockExport(params)),
  clearBlockListExport: () => dispatch(BlockListExportActions.clearBlockExport()),
  blockEmailDispatch: (data) => dispatch(EmailBlockListActions.addEmailBlacklistInit(data)),
  BlockListUploadsDispatch: (data) => dispatch(BlockListUploadActions.getBlockListUpload(data)),
  clearBlockListUpload: (data) => dispatch(BlockListUploadActions.clearBlockListUpload(data)),
  blockEmailDispatchClear: (data) => dispatch(EmailBlockListActions.addEmailBlacklistClear(data)),
  BlockListEditDispatch: (id) => dispatch(BlockListEditActions.getBlockListlist(id)),
  DeletegetBlocklistDispatch: (id) => dispatch(BlockListDeleteActions.DeletegetBlocklist(id)),
  clearBlockListlistDispatch: (id) => dispatch(BlockListDeleteActions.clearBlockListlist(id)),
  BlockListUpdateDispatch: (id, params) => dispatch(BlockListUpdateActions.getBlockListlist(id, params)),
  clearUpdateBlockList: () => dispatch(BlockListUpdateActions.clearUpdateBlockList()),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  DeleteClientDispatch: (data) => dispatch(DeleteClientActions.DeleteClientInit(data)),
  DeleteClientClearDispatch: (data) => dispatch(DeleteClientActions.DeleteClientClear(data)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlackList);