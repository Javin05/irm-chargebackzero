import React, { useEffect, useState } from "react";
import axios from "axios";
import * as XLSX from "xlsx";

const NpciUrls = () => {
  const [file, setFile] = useState(null);
  const [message, setMessage] = useState("");
  const [totalProcessed, setTotalProcessed] = useState(0);
  const [batchSize, setBatchSize] = useState(0);
  const [loading, setLoading] = useState(true);
  const [urlsData, setUrlsData] = useState([]);
  const [isDownloadable, setIsDownloadable] = useState(false);
  const [progress, setProgress] = useState(0);
  const [page, setPage] = useState(1);
  const [limit] = useState(10);

  useEffect(() => {
    const fetchProgress = async () => {
      try {
        const response = await axios.get(
          `${process.env.REACT_APP_API_URL}/tagScreenshot/progress`
        );
        const { totalProcessed, batchSize } = response.data;
        setTotalProcessed(totalProcessed);
        setBatchSize(batchSize);
        setProgress((totalProcessed / batchSize) * 100);

        if (totalProcessed === batchSize) {
          const urlsResponse = await axios.get(
            `${process.env.REACT_APP_API_URL}/tagScreenshot/processed-urls`
          );
          setUrlsData(urlsResponse.data); // Assuming this endpoint returns all processed data
          setIsDownloadable(true);
        }
      } catch (error) {
        console.error("Error fetching progress:", error);
      }
    };

    fetchProgress();
    const intervalId = setInterval(fetchProgress, 5000);
    return () => clearInterval(intervalId);
  }, [page, limit]);

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  const handleFileUpload = async () => {
    if (!file) {
      setMessage("Please select an Excel file to upload.");
      return;
    }

    setMessage("Processing... Please wait.");
    setIsDownloadable(false);

    try {
      const data = await file.arrayBuffer();
      const workbook = XLSX.read(data, { type: "array" });
      const sheetName = workbook.SheetNames[0];
      const sheet = workbook.Sheets[sheetName];
      const jsonData = XLSX.utils.sheet_to_json(sheet, { header: 1 });
      const pageUrls = jsonData
        .slice(1)
        .map((row) => row[0])
        .filter(Boolean);

      if (pageUrls.length === 0) {
        setMessage("No URLs found in the uploaded Excel file.");
        return;
      }

      await axios.post(
        `${process.env.REACT_APP_API_URL}/tagScreenshot/capture-screenshot`,
        { pageUrls }
      );
      setMessage("File is being processed. You can download results anytime.");
    } catch (error) {
      console.error("Error processing the file:", error);
      setMessage("Error processing the file. Please try again.");
    }
  };

  const downloadExcel = () => {
    if (urlsData && urlsData.length > 0) {
      const worksheet = XLSX.utils.json_to_sheet(
        urlsData.map(({ url, screenshotUrl }) => ({
          URL: url,
          Screenshot: screenshotUrl,
        }))
      );
      const workbook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workbook, worksheet, "Processed URLs");
      XLSX.writeFile(workbook, "Processed_URLs.xlsx");
    } else {
      console.log("No data available to download");
      setMessage("No processed data available to download.");
    }
  };

  const handlePageChange = (newPage) => {
    setPage(newPage);
  };

  return (
    <div className="container py-5">
      {/* Upload Section */}
      <div className="row justify-content-center">
        <div className="col-md-12">
          <div className="card shadow-sm">
            <div className="card-body">
              <div className="bg-light">
                <div className="bg-white shadow-sm rounded p-4">
                  <h1 className="h4 fw-bold mb-4">Upload Excel File</h1>
                  <input
                    type="file"
                    accept=".xlsx, .xls"
                    onChange={handleFileChange}
                    className="form-control mb-3"
                  />
                  <div className="d-flex justify-content-center mt-4">
                    <button
                      onClick={handleFileUpload}
                      className="btn btn-primary"
                      disabled={message === "Processing... Please wait."}
                    >
                      {message === "Processing... Please wait."
                        ? "Processing..."
                        : "Upload and Process"}
                    </button>
                    {message && (
                      <div className="alert alert-info mt-3 text-center">
                        {message}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Batch Progress Section */}
      <div className="row justify-content-center mt-5">
        <div className="col-md-12">
          <div className="card shadow-sm">
            <div className="card-body">
              <div className="bg-light">
                <div className="bg-white shadow-sm rounded p-4">
                  <h1 className="h4 fw-bold mb-4">Batch Progress</h1>
                  <p>
                    {totalProcessed} / {batchSize} URLs processed
                  </p>
                  <div className="progress mt-3" style={{ height: "25px" }}>
                    <div
                      className="progress-bar progress-bar-striped progress-bar-animated bg-success"
                      role="progressbar"
                      style={{ width: `${progress}%` }}
                    />
                  </div>
                </div>
              </div>

              <div className="d-flex justify-content-center mt-4">
                <button
                  onClick={downloadExcel}
                  disabled={!isDownloadable}
                  className="btn btn-success mt-4"
                >
                  Download Processed URLs
                </button>
              </div>

              {/* Pagination */}
              {/* <div className="d-flex justify-content-center mt-4">
                <button onClick={() => handlePageChange(page - 1)} disabled={page === 1} className="btn btn-secondary">Previous</button>
                <button onClick={() => handlePageChange(page + 1)} disabled={urlsData.length < limit} className="btn btn-secondary">Next</button>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NpciUrls;
