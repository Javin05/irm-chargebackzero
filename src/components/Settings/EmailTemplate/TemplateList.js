import React, { useEffect, useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import _ from "lodash";
import { KTSVG } from "../../../theme/helpers";
import { generalTemplateActions, deleteGeneralTemplateActions } from "../../../store/actions";
import { connect } from "react-redux";
import { getUserPermissions, headersWithAuth } from '../../../utils/helper'
import { Can } from '../../../theme/layout/components/can'
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  STATUS_BADGES,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  DELETE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from "../../../utils/constants";
import {
  warningAlert,
  confirmationAlert,
  confirmAlert,
} from "../../../utils/alerts";

const TemplateList = (props) => {
  const {
    className,
    getTemplateDispatch,
    generalTemplateData,
    loadingGT,
    messageDGT,
    statusDGT,
    deleteTemplateDispatch,
    clearDeleteTemplateDispatch,
  } = props;

  const history = useHistory();
  const url = useLocation().pathname;
  const getUsersPermissions = getUserPermissions(url, true)
  const [limit] = useState(25);
  const [activePageNumber] = useState(1);

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
    };
    getTemplateDispatch(params, headersWithAuth);
  }, []);

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_TEMPLATE,
      "warning",
      "Yes",
      "No",
      () => {
        deleteTemplateDispatch(id, headersWithAuth);
      },
      () => { }
    );
  };

  const onConfirm = () => {
    const params = {
      limit: limit,
      page: activePageNumber,
    };
    getTemplateDispatch(params, headersWithAuth);
  };

  useEffect(() => {
    if (statusDGT === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageDGT,
        "success",
        "ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearDeleteTemplateDispatch();
    } else if (statusDGT === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "Error",
        messageDGT,
        "error",
        "Close",
        "Ok",
        () => {
          onConfirm();
        },
        () => { }
      );
      clearDeleteTemplateDispatch();
    }
  }, [statusDGT]);

  return (
    <>
      <div className={`card ${className}`}>
        <div className="card-body py-3">
          <div className="d-flex  px - 2">
            <div className="d-flex justify-content-start col-md-6" />
            <div className="d-flex col-md-6 justify-content-end my-auto">
              <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <div className="my-auto me-3 mt-4">
                  <div className="my-auto">
                    <Link
                      to='/email-templates/add-template'
                      className="btn btn-sm btn-light-primary fa-pull-right"
                    >
                      {/* eslint-disable */}
                      <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                      {/* eslint-disable */}
                      Add Email Template
                    </Link>
                  </div>
                </div>
              </Can>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className="fw-bolder fs-8 text-gray-800">
                <tr>
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={UPDATE_DELETE_PERMISSION}
                  >
                    <th>
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                  </Can>
                  <th>
                    <div className="d-flex">
                      <span>NotificationType</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Subject</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className="fs-8">
                {!loadingGT ? (
                  generalTemplateData && generalTemplateData && generalTemplateData.length > 0 ? (
                    generalTemplateData.map((item, i) => {
                      return (
                        <tr
                          key={i}
                          style={
                            i === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                          <Can
                            permissons={getUsersPermissions}
                            componentPermissions={UPDATE_DELETE_PERMISSION}
                          >
                            <td className="pb-0 pt-3 text-start my-auto">
                              <Can
                                permissons={getUsersPermissions}
                                componentPermissions={UPDATE_PERMISSION}
                              >
                                <button
                                  className="btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm"
                                  onClick={() => {
                                    history.push(`/email-templates/update/${item._id}`)
                                  }}
                                >
                                  {/* eslint-disable */}
                                  <KTSVG
                                    path="/media/icons/duotune/art/art005.svg"
                                    className="svg-icon-3"
                                  />
                                  {/* eslint-enable */}
                                </button>
                              </Can>
                              <Can
                                permissons={getUsersPermissions}
                                componentPermissions={DELETE_PERMISSION}
                              >
                                <button
                                  className="btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm"
                                  onClick={() => {
                                    onDeleteItem(item._id);
                                  }}
                                >
                                  {/* eslint-disable */}
                                  <KTSVG
                                    path="/media/icons/duotune/general/gen027.svg"
                                    className="svg-icon-3"
                                  />
                                  {/* eslint-enable */}
                                </button>
                              </Can>
                            </td>
                          </Can>
                          <td className="ellipsis">
                            {item && item.notificationType
                              ? item.notificationType
                              : "--"}
                          </td>
                          <td className="ellipsis">
                            {item && item.subject ? item.subject : "--"}
                          </td>
                          <td>
                            <span
                              className={`badge ${STATUS_BADGES[item.status]}`}
                            >
                              {item.status ? item.status : "--"}
                            </span>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr className="text-center py-3">
                      <td colSpan="100%">No record(s) found</td>
                    </tr>
                  )
                ) : (
                  <tr>
                    <td colSpan="100%" className="text-center">
                      <div
                        className="spinner-border text-primary m-5"
                        role="status"
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  const { generalTemplateStore, generalTemplateDeleteStore } = state;
  return {
    generalTemplateData:
      generalTemplateStore && generalTemplateStore.generalTemplateData
        ? generalTemplateStore.generalTemplateData
        : [],
    loadingGT:
      generalTemplateStore && generalTemplateStore.loadingGT
        ? generalTemplateStore.loadingGT
        : false,
    statusDGT:
      generalTemplateDeleteStore && generalTemplateDeleteStore.statusDGT
        ? generalTemplateDeleteStore.statusDGT
        : "",
    messageDGT:
      generalTemplateDeleteStore && generalTemplateDeleteStore.messageDGT
        ? generalTemplateDeleteStore.messageDGT
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getTemplateDispatch: (params, headers) =>
    dispatch(generalTemplateActions.get(params, headers)),
  deleteTemplateDispatch: (params, headers) =>
    dispatch(deleteGeneralTemplateActions.delete(params, headers)),
  clearDeleteTemplateDispatch: () =>
    dispatch(deleteGeneralTemplateActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TemplateList);
