import React, { useEffect, useState } from 'react'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import {
  successAlert,
  warningAlert
} from "../../utils/alerts"
import { RISKSTATUS } from '../../utils/constants'

function MerchantDetails(props) {
  const {
    merchantSummary,
    convertOgmDispatch,
    convertOgmLoading,
    convertOgmResponse,
    clearConvertOgmDispatch
  } = props

  const handleOgmConvert = (id) => {
    const payload = {
      wrmId: id && id.wrmId && id.wrmId._id
    }
    convertOgmDispatch(payload)
  }

  useEffect(() => {
    if (convertOgmResponse && convertOgmResponse.status === "error") {
      warningAlert(
        'error',
        convertOgmResponse && convertOgmResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearConvertOgmDispatch()
    } else if (convertOgmResponse && convertOgmResponse.status === "ok") {
      successAlert(
        convertOgmResponse && convertOgmResponse.message,
        'success'
      )
      clearConvertOgmDispatch()
    }

  }, [convertOgmResponse])
  
  return (
    <>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='card-header border-0'>
          <h3 className='card-title align-items-start flex-column '>
            <span className='d-flex align-items-center fw-boldest my-1 fs-2'>Case Details - Case ID #{merchantSummary && merchantSummary.onboardingId ? merchantSummary.onboardingId : '--'}
            </span>
          </h3>
          <div className='col-lg-4' />
        </div>
        <div className="separator separator-dashed my-3" />
        <div className="row">
          <div className="col-lg-3 ps-10">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">First Name</th>
                  <td>
                    {merchantSummary && merchantSummary.firstName ? merchantSummary.firstName : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Last Name</th>
                  <td>
                    {merchantSummary && merchantSummary.lastName ? merchantSummary.lastName : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Business Name</th>
                  <td>
                    {merchantSummary && merchantSummary.businessName ? merchantSummary.businessName : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Entity Name</th>
                  <td>
                    {merchantSummary && merchantSummary.entityName ? merchantSummary.entityName : '--'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-lg-5">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Comments</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.comments && merchantSummary.comments.length > 0 ? merchantSummary.comments.map(e => e) : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">EmailId</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.emailId && merchantSummary.emailId.emailId ? merchantSummary.emailId.emailId : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Business Category</th>
                  <td>
                    {merchantSummary && merchantSummary.businessCategory ? merchantSummary.businessCategory : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Onboarding Updated Type</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.onboardingUpdatedType ? merchantSummary.onboardingUpdatedType : '--'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-lg-4">
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Contact Name</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.primaryContactName ? merchantSummary.primaryContactName : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Phone Number</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.phoneNumber && merchantSummary.phoneNumber.number ? merchantSummary.phoneNumber.countryCode + ' ' + merchantSummary.phoneNumber.number : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Website</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.website ? merchantSummary.website : '--'}
                  </td>
                </tr>
                <tr>
                  <th scope="row">Business Description</th>
                  <td className='Id-response-Link'>
                    {merchantSummary && merchantSummary.businessDescription ? merchantSummary.businessDescription : '--'}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='card-header border-0 p-5'>
          <div className='col-lg-3 text-center'>
            <p>Accounts Queue</p>
            <span className={`me-2 badge ${RISKSTATUS[merchantSummary && merchantSummary.accountQueueId && merchantSummary.accountQueueId.riskStatus]}`}>
              {merchantSummary && merchantSummary.accountQueueId && merchantSummary.accountQueueId.riskStatus}
            </span>
            {merchantSummary.accountsQueue === "YES" ?
              <Link to={`/account-risk-summary/update/${merchantSummary.accountQueueId && merchantSummary.accountQueueId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
          </div>
          <div className='col-lg-3 text-center'>
            <p>KYC Queue</p>
            <span className={`me-2 badge ${RISKSTATUS[merchantSummary && merchantSummary.kycId && merchantSummary.kycId.kycStatus]}`}>
              {merchantSummary && merchantSummary.kycId && merchantSummary.kycId.kycStatus}
            </span>
            {merchantSummary && merchantSummary.kycQueue === "YES" ?
              <Link to={`/static-summary/update/${merchantSummary.kycId && merchantSummary.kycId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
          </div>
          <div className='col-lg-3 text-center'>
            <p>WRM Queue</p>
            <span className={` me-2 badge ${RISKSTATUS[merchantSummary && merchantSummary.wrmId && merchantSummary.wrmId.riskStatus]}`}>
              {merchantSummary && merchantSummary.wrmId && merchantSummary.wrmId.riskStatus}
            </span>
            {merchantSummary && merchantSummary.wrmQueue === "YES" ?
              <Link to={`/risk-summary/update/${merchantSummary.wrmId && merchantSummary.wrmId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
          </div>
          {/* <div className='col-lg-3 text-center'>
            <p>OGM Queue</p>
            {merchantSummary && merchantSummary.wrmQueue === "YES" && !_.isEmpty(merchantSummary.ogmId) ?
              <>
              <span className={` me-2 badge ${RISKSTATUS[merchantSummary && merchantSummary.ogmId && merchantSummary.ogmId.riskStatus]}`}>
                {merchantSummary && merchantSummary.ogmId && merchantSummary.ogmId.riskStatus}
              </span>
                <Link to={`/ongoing-monitoring-dashboard/update/${merchantSummary.ogmId && merchantSummary.ogmId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link>
              </>
              : merchantSummary && merchantSummary.wrmQueue === "YES" && merchantSummary.wrmId.reportStatus === "COMPLETED" && _.isEmpty(merchantSummary.ogmId) ?
              <button disabled={convertOgmLoading} onClick={() => handleOgmConvert(merchantSummary)} className='btn btn-sm btn-light-success btn-responsive me-3 pull-right'>{convertOgmLoading ? <span className='indicator-progress' style={{ display: 'block' }}>
                <span className='spinner-border spinner-border-sm align-middle mx-3' />
              </span> : "Convert to Ogm"}</button> : "--"
            }
          </div> */}
          
        </div>
        <br/>
          <br/>
                  <div>
            <h3 style={{textAlign:"center",marginTop:40}}>Rule Engine</h3>
          </div>
          <table class="table">
  <thead>
    <tr>
        <th scope="col"><strong>Queue Name</strong></th>
        <th scope="col"><strong>Rule name</strong></th>
        <th scope="col"><strong>Rule action</strong></th>    
        <th scope="col"><strong>Rule condition</strong></th>    
        <th scope="col"><strong>Rule description</strong></th>
        <th scope="col"><strong>Aplication value</strong></th>
        <th scope="col"><strong>Status</strong></th>
        
     
    </tr>
  </thead>
  <tbody>
    {
       merchantSummary&&merchantSummary.rules&&merchantSummary.rules.map(rule=>
    <tr>
      <td>{rule.queueId?.queueName}</td>
      <td>{rule.ruleGroupName}</td>
      <td>{rule.action}</td>
      <td>{rule.condition}</td>
      <td>{`${rule.ruleField} ${rule.ruleOperator} `} <strong>{rule.ruleValue} </strong></td>
      <td>{rule.resultvalue}</td>
      <td>{rule.result}</td>
    </tr>)
}
  </tbody>
</table>
      <h5>Rule engine process :  {merchantSummary&&merchantSummary.rules ?merchantSummary.rules[0].ruleStatus:"--"}</h5>
      </div>
    </>
  )
}

export default MerchantDetails