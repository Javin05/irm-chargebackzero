import React, { useEffect, useState } from "react"
import moment from "moment"
import "react-circular-progressbar/dist/styles.css"
import Tabs from "react-bootstrap/Tabs"
import Tab from "react-bootstrap/Tab"
import MerchantDetails from "./merchantDetails"
import {
  OnBoardingSummaryActions,
  ConvertOgmActions
} from "../../store/actions"
import { useLocation } from 'react-router-dom'
import { connect } from "react-redux"

function OnBoardingSummary(props) {
  const {
    onBoardingIdDetails,
    getOnBoardingSummary,
    convertOgmDispatch,
    convertOgmLoading,
    convertOgmResponse,
    clearConvertOgmDispatch
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [key, setKey] = useState("OnBoarding")
  const merchantSummary = onBoardingIdDetails && onBoardingIdDetails.data

  useEffect(() => {
    getOnBoardingSummary(id)
  },[])

  return (
    <>
      <div className="mt-0">
        <Tabs
          id="controlled-tab-example"
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className="ctab mb-3"
        >
          <Tab eventKey="OnBoarding" title="ONBOARDING">
            <MerchantDetails
              merchantSummary={merchantSummary}
              convertOgmDispatch={convertOgmDispatch}
              convertOgmResponse={convertOgmResponse}
              convertOgmLoading={convertOgmLoading}
              clearConvertOgmDispatch={clearConvertOgmDispatch}
            />
          </Tab>
        </Tabs>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { onBoardingSummaryStore, convertOgmStore } = state
  return {
    onBoardingIdDetails:onBoardingSummaryStore && onBoardingSummaryStore.onBoardingSummary ? onBoardingSummaryStore.onBoardingSummary : "",
    convertOgmResponse:convertOgmStore && convertOgmStore.convertOgm ? convertOgmStore.convertOgm : "",
    convertOgmLoading:convertOgmStore && convertOgmStore.loading ? convertOgmStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getOnBoardingSummary: (id) =>
    dispatch(OnBoardingSummaryActions.getOnBoardingSummary(id)),
  convertOgmDispatch: (data) =>
    dispatch(ConvertOgmActions.convertOgmPost(data)),
  clearConvertOgmDispatch: () => 
    dispatch(ConvertOgmActions.clearConvertOgm()),
})

export default connect(mapStateToProps, mapDispatchToProps) (OnBoardingSummary)