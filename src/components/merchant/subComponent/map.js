import React from "react";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  InfoWindow,
  Marker,
  Polyline,
} from "react-google-maps";
import { MAP } from "../../../utils/constants";

class LocationSearchModal extends React.Component {
  render() {
    const { AllMarkers } = this.props;
    const google = window.google;
    const onLoad = (polyline) => {};

    const options = {
      strokeColor: "#FF0000",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#FF0000",
      fillOpacity: 0.35,
      clickable: false,
      draggable: false,
      editable: false,
      visible: true,
      radius: 30000,
      zIndex: 1,
    };

    const MapWithAMarker = withScriptjs(
      withGoogleMap((props) => (
        <GoogleMap
          defaultZoom={8}
          defaultCenter={{ lat: -34.397, lng: 150.644 }}
          style={{ height: "100vh", width: "100%" }}
        >
          {AllMarkers &&
            AllMarkers.map((mark, i) => (
              <Marker
                key={i}
                position={{
                  lat: parseFloat(mark.lat),
                  lng: parseFloat(mark.lng),
                }}
                defaultCenter={{
                  lat: parseFloat(mark.lat),
                  lng: parseFloat(mark.lng),
                }}
                draggable={false}
                icon={`${MAP[mark.area]}`}
              >
                <InfoWindow>
                  <div>{mark.area}</div>
                </InfoWindow>
                <Polyline onLoad={onLoad} path={AllMarkers} options={options} />
              </Marker>
            ))}
        </GoogleMap>
      ))
    );

    return (
      <div style={{ padding: "1rem", maxWidth: 10000 }}>
        <MapWithAMarker
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws&libraries=places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
    );
  }
}

export default LocationSearchModal;
