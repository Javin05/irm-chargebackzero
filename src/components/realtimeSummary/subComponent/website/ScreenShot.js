import React, { Fragment } from 'react'
import _ from 'lodash'
import { toAbsoluteUrl } from '../../../../theme/helpers'

function ScreenShot(props) {
  const {
    DashboardExportData,
    merchantSummary
  } = props

  return (
    <>
      <div className='row'>
        <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Url
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Terms & Condition
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot ? DashboardExportData.termsAndConditionPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Privacy Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot ? DashboardExportData.privacyPolicyPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Shipping Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot ? DashboardExportData.shippingPolicyPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Return Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.returnPolicyPageScreenshot ? DashboardExportData.returnPolicyPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Contact Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.contactUsPageScreenshot ? DashboardExportData.contactUsPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      About Us
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {merchantSummary && merchantSummary.report ?  merchantSummary.report.aboutUsPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Cancellation Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {merchantSummary && merchantSummary.report ?  merchantSummary.report.cancellationPolicyPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Refund Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {merchantSummary && merchantSummary.report ?  merchantSummary.report.refundPolicyPageScreenshot : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Product Pricing Url
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Home
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.productPricingHomePageUrl ? DashboardExportData.productPricingHomePageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Product Page
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700'>
                      {DashboardExportData && DashboardExportData.productPricingProductPageUrl ? DashboardExportData.productPricingProductPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Terms and Conditions
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot
                            ? DashboardExportData.termsAndConditionPageScreenshot : '--'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Terms and Conditions Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Privacy Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.privatePolicyPageScreenshot || DashboardExportData.privacyPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.privatePolicyPageScreenshot ? DashboardExportData.privatePolicyPageScreenshot :
                            DashboardExportData.privacyPolicyPageScreenshot
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Privacy Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Shipping Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot
                            ? DashboardExportData.shippingPolicyPageScreenshot : '--'}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Shipping Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Returns Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.returnPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.returnPolicyPageScreenshot
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Returns Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Contact Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.contactUsPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.contactUsPageScreenshot
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Contact Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  About Us
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsPageScreenshot}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          About Us Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Cancellation Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyPageScreenshot}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Cancellation Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Refund Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyPageScreenshot}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Refund Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='row g-5 g-xl-8 mb-8' >
        <h1 className='fs-2'>Product Pricing Screenshot</h1>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Home
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.productPricingHomePageUrl) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.productPricingHomePageUrl
                            ? DashboardExportData.productPricingHomePageUrl : '--'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Home Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Product Page
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(DashboardExportData && DashboardExportData.productPricingProductPageUrl ) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={DashboardExportData && DashboardExportData.productPricingProductPageUrl
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Product Page Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  )
}

export default ScreenShot