import { useEffect, useState } from "react"
import "react-circular-progressbar/dist/styles.css"
import { useLocation } from "react-router-dom"
import { connect } from 'react-redux'
import WebsiteContent from './website-content'
import Score from "./Score"
import Summarry from './Summary'
import ScreenShot from './ScreenShot'
import _ from 'lodash'
import {
  PlayStoreDashboardRTActions,
  WRMExportWebreportTypesActions,
  WRMRealtimePMAListActions
} from "../../../../store/actions"
import PlayStoreScreenShot from './playStore/ScreenShot'
import PlayStoreTransationLaundring from './playStore/TransactionLaundring'
import PlayStoreSummarry from './playStore/Summary'

function Websites(props) {
  const {
    loading,
    getRiskSummarys,
    dashboardDetails,
    merchantIddetails,
    getriskScores,
    matrixDetail,
    isLoaded,
    PlayStoreDashboardDispatch,
    PlayStoreDashboardResponse,
    getExportDispatch,
    DashboardExportLists,
    merchantSummary,
    clearExportListDispatch,
    DashboardListPmaDispatch,
    DashboardPmaLists
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [activestep, setActiveStep] = useState(0)
  const [completed] = useState({})
  const steps = getSteps()
  const PlayStorestep = PlayStoreSteps()
  const [NameData, setNameData] = useState()
  const viewData = getRiskSummarys && getRiskSummarys && getRiskSummarys.data ? getRiskSummarys.data : []
  const getData = _.isArray(viewData) ? viewData.filter(o => (o ? o : '')) :''
  const ValidData = getData && getData[0] ? getData[0] : '--'
  const domainInfoData = ValidData && ValidData.domainInfo && ValidData.domainInfo.registryData ? ValidData.domainInfo.registryData : '--'
  const successVerifyDomain = ValidData && ValidData.verifyDomain && ValidData.verifyDomain.message && ValidData.verifyDomain.message.result ? ValidData.verifyDomain.message.result : null
  const successWhoDomain = ValidData && ValidData.whoDomain && ValidData.whoDomain.data ? ValidData.whoDomain.data : null
  const webAnalysis = ValidData && ValidData.webAnalysisStatus ? ValidData.webAnalysisStatus : null
  const BusinessName = ValidData && ValidData.webAnalysisShedule && ValidData.webAnalysisShedule.business_name ? ValidData.webAnalysisShedule.business_name : ''
  const policyComplianceCheck = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ? webAnalysis.data.policy_complaince_checks : null
  const userInteractionChecks = webAnalysis && webAnalysis.data && webAnalysis.data.user_interaction_checks ? webAnalysis.data.user_interaction_checks : null
  const websitetobusinessmatch = webAnalysis && webAnalysis.data && webAnalysis.data.website_to_business_name_match ? webAnalysis.data.website_to_business_name_match : null
  const domainRepetation = ValidData && ValidData.domainReputation ? ValidData.domainReputation : null
  const DNSData = ValidData && ValidData.dnsLookup && ValidData.dnsLookup.DNSData && ValidData.dnsLookup.DNSData ? ValidData.dnsLookup.DNSData : null
  const adminContact = domainInfoData && domainInfoData.administrativeContact ? domainInfoData.administrativeContact : '--'
  const registerData = domainInfoData ? domainInfoData : '--'
  const adminContactData = domainInfoData && domainInfoData.administrativeContact ? domainInfoData.administrativeContact : '--'
  const websiteCatgories = ValidData && ValidData.websiteCategorization && ValidData.websiteCategorization.message ? ValidData.websiteCategorization.message : '--'
  const reviewAnalysis = ValidData && ValidData.reviewAnalysis ? ValidData.reviewAnalysis : '--'
  const websiteImageDetect = ValidData && ValidData.websiteImageDetect ? ValidData.websiteImageDetect : '--'
  const websiteScore = getriskScores && getriskScores.data ? getriskScores.data : '--'
  const phonevalue = Math.round(websiteScore && websiteScore.riskScore)
  const websiteData = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.website && dashboardDetails.data.dashboardData.website.data
  const websiteCategorizationV1 = ValidData && ValidData.websiteCategorizationV1
  const LogoCheck = ValidData && ValidData.logoCheck
  const websiteLink = merchantIddetails && merchantIddetails.data && merchantIddetails.data.website ? merchantIddetails.data.website : successVerifyDomain && successVerifyDomain.websiteURL ? successVerifyDomain.websiteURL : '--'
  const DashboardExportData = DashboardExportLists && DashboardExportLists.data && DashboardExportLists.data[0] ? DashboardExportLists.data[0] : 'No Data'
  useEffect(() => {
    if (websiteLink) {
      var clearURL = websiteLink.replace('https://', '').replace('http://', '').replace('www.', '').replace('/', '')
      var splitURL = clearURL.split('.')
      var domainLegalName = ''
      if (splitURL.length > 0) {
        domainLegalName = splitURL[0]
      }
      setNameData(domainLegalName)
    }
  }, [websiteLink])

  useEffect(() => {
    const params = {
      id: id
    }
    PlayStoreDashboardDispatch(params)
    getExportDispatch(params)
  }, [])

  useEffect(() => {
    const params = {
      riskId: id
    }
    DashboardListPmaDispatch(params)
  }, [])

  useEffect(() => {
    return () => { 
      clearExportListDispatch()
     }
}, [])

  function getSteps() {
    return [
      {
        label: "Summary",
        className: "btn web-label-six",
        stepCount: 0,
      },
      {
        label: "Website Content",
        className: "btn web-label-six",
        stepCount: 1,
      },
      {
        label: "Screenshots",
        stepCount: 3,
        className: "btn web-label-six",
      },
    ]
  }

  function PlayStoreSteps() {
    return [
      {
        label: "Summary",
        className: "btn web-label-six",
        stepCount: 0,
      },
      {
        label: "Transaction Laundering",
        stepCount: 1,
        className: "btn web-label-six",
      },
      {
        label: "Screenshots",
        stepCount: 2,
        className: "btn web-label-six",
      }
    ]
  }

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <Summarry
            websiteData={websiteData}
            matrixDetail={matrixDetail}
            isLoaded={isLoaded}
            successVerifyDomain={successVerifyDomain}
            adminContact={adminContact}
            adminContactData={adminContactData}
            domainInfoData={domainInfoData}
            merchantIddetails={merchantIddetails}
            websitevalue={phonevalue}
            websiteLink={websiteLink}
            ValidData={ValidData}
            DNSData={DNSData}
            BsName={NameData}
            DashboardExportData={DashboardExportData}
          />
        )
      case 1:
        return (
          <WebsiteContent
            ValidData={ValidData}
            successVerifyDomain={successVerifyDomain}
            websiteCatgories={websiteCatgories}
            websitetobusinessmatch={websitetobusinessmatch}
            websiteImageDetect={websiteImageDetect}
            userInteractionChecks={userInteractionChecks}
            webAnalysis={webAnalysis}
            policyComplianceCheck={policyComplianceCheck}
            websiteCategorizationV1={websiteCategorizationV1}
            LogoCheck={LogoCheck}
            websiteLink={websiteLink}
            DashboardExportData={DashboardExportData}
            merchantSummary={merchantSummary}
            domainRepetation={domainRepetation}
          />
        )
      case 2:
        return (
          <ScreenShot
            webAnalysis={webAnalysis}
            DashboardExportData={DashboardExportData}
            merchantSummary={merchantSummary}
          />
        )
      default:
        return "unknown step"
    }
  }
  const playStoreData = PlayStoreDashboardResponse && PlayStoreDashboardResponse.data && PlayStoreDashboardResponse.data.data

  function getPlayStoreStepContent(step) {
    switch (step) {
      case 0:
        return (
          <PlayStoreSummarry
            playStoreData={playStoreData}
          />
        )
      case 1:
        return (
          <PlayStoreTransationLaundring
          />
        )
      case 2:
        return (
          <PlayStoreScreenShot
            playStoreData={playStoreData}
          />
        )
      default:
        return "unknown step"
    }
  }

  const handleStep = (step) => () => {
    setActiveStep(step)
  }

  return (
    <>
      {
        merchantIddetails && merchantIddetails.data && merchantIddetails.data.playstoreUrl === "YES" ?
          (
            <div className="mt-0">
              <>
                <div className="d-flex">
                  {PlayStorestep.map((step, index) => (
                    <div
                      key={"A_" + index}
                      completed={completed[index]}
                      className={`my-10 mx-1 text mb-4 rounded-1 seven-label fs-8 fw-bolder text-gray-800 ${loading ? "event-disable" : ""
                        } ${step.stepCount === activestep
                          ? "btn web-label-sixActive fs-8 fw-bolder"
                          : `${step.className}`
                        }`}
                      onClick={handleStep(index)}
                    >
                      {step.label}
                    </div>
                  ))}
                </div>
                {activestep === PlayStorestep.length ? null : (
                  <>
                    <div>{getPlayStoreStepContent(activestep)}</div>
                  </>
                )}
              </>
            </div>
          ) : (
            <div className="mt-0">
              <Score
                websiteLink={websiteLink}
                websitevalue={phonevalue}
                BusinessName={BusinessName}
                ValidData={ValidData}
                BsName={NameData}
                websiteData={websiteData}
                successVerifyDomain={successVerifyDomain}
                DNSData={DNSData}
                matrixDetail={matrixDetail}
                adminContact={adminContact}
                adminContactData={adminContactData}
                websitetobusinessmatch={websitetobusinessmatch}
                webAnalysis={webAnalysis}
                LogoCheck={LogoCheck}
                websiteImageDetect={websiteImageDetect}
                userInteractionChecks={userInteractionChecks}
                domainInfoData={domainInfoData}
                domainRepetation={domainRepetation}
                registerData={registerData}
                successWhoDomain={successWhoDomain}
                websiteCategorizationV1={websiteCategorizationV1}
                reviewAnalysis={reviewAnalysis}
                merchantIddetails={merchantIddetails}
                id={id}
                DashboardExportData={DashboardExportData}
                DashboardPmaLists={DashboardPmaLists}
                merchantSummary={merchantSummary}
                playStoreData={playStoreData}
              />
              <>
                <div className="d-flex" style={{backgroundColor:'#f5f8fa'}}>
                  {steps.map((step, index) => (
                    <div
                      key={"A_" + index}
                      completed={completed[index]}
                      className={`my-10 mx-1 text mb-4 rounded-1 seven-label fs-8 fw-bolder text-gray-800 ${loading ? "event-disable" : ""
                        } ${step.stepCount === activestep
                          ? "btn web-label-sixActive fs-8 fw-bolder"
                          : `${step.className}`
                        }`}
                      onClick={handleStep(index)}
                    >
                      {step.label}
                    </div>
                  ))}
                </div>
                {activestep === steps.length ? null : (
                  <>
                    <div style={{backgroundColor:'#f5f8fa'}}>{getStepContent(activestep)}</div>
                  </>
                )}
              </>
            </div>
          )
      }
    </>
  )
}

const mapStateToProps = (state) => {
  const { WebsiteStore, riskScoreStore, PlayStoreDashboardRTStore, WRMExportWebreportlistStore, WRMRealtimePMAlistStore, WrmRealtimeUpdateDashboardGetByIdStore } = state

  return {
    WebsiteResponce: WebsiteStore && WebsiteStore.WebsiteResponce ? WebsiteStore.WebsiteResponce : {},
    getRiskSummarys: state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    dashboardDetails: WrmRealtimeUpdateDashboardGetByIdStore && WrmRealtimeUpdateDashboardGetByIdStore.WrmRealtimeUpdateDashboardGetById ? WrmRealtimeUpdateDashboardGetByIdStore.WrmRealtimeUpdateDashboardGetById : {},
    getriskScores: riskScoreStore && riskScoreStore.getriskScores ? riskScoreStore.getriskScores : {},
    PlayStoreDashboardResponse: PlayStoreDashboardRTStore && PlayStoreDashboardRTStore.PlayStoreDashboardRTResponse ? PlayStoreDashboardRTStore.PlayStoreDashboardRTResponse : {},
    DashboardExportLists: WRMExportWebreportlistStore && WRMExportWebreportlistStore.WRMExportWebReportlists ? WRMExportWebreportlistStore.WRMExportWebReportlists : '',
    DashboardPmaLists: WRMRealtimePMAlistStore && WRMRealtimePMAlistStore.WRMRealtimePMAlists ? WRMRealtimePMAlistStore.WRMRealtimePMAlists : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  PlayStoreDashboardDispatch: (params) => dispatch(PlayStoreDashboardRTActions.getPlayStoreDashboardRT(params)),
  getExportDispatch: (data) => dispatch(WRMExportWebreportTypesActions.getWRMExportWebReportlist(data)),
  clearExportListDispatch: (data) => dispatch(WRMExportWebreportTypesActions.clearWRMExportWebReportlist(data)),
  DashboardListPmaDispatch: (params) => dispatch(WRMRealtimePMAListActions.getWRMRealtimePMAlist(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Websites)