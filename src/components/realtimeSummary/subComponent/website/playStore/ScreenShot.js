import _ from 'lodash'
import { toAbsoluteUrl } from '../../../../../theme/helpers'

function PlayStoreScreenShot(props) {
  const {
    playStoreData
  } = props

  var splitURL = playStoreData && playStoreData.screenshots.split(',')
  
  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Header Image
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(playStoreData && playStoreData.header_image) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={playStoreData.header_image}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Privacy Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Icon
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(playStoreData && playStoreData.icon) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={playStoreData && playStoreData.icon}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Screenshots
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(splitURL) ? (
                        splitURL && splitURL.map((item) => {
                          return (
                            <img
                              className={`me-2 mb-2 w-100`}
                              src={item}
                              onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                              referrerpolicy="no-referrer"
                            />
                          )
                        })
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default PlayStoreScreenShot