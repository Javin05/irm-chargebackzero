import React, { useState, useEffect, Fragment } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  REGEX,
  RESPONSE_STATUS,
  SESSION,
  SWEET_ALERT_MSG,
} from "../../../../utils/constants"
import {
  WrmRTStatusAction,
} from "../../../../store/actions"
import { useLocation } from "react-router-dom"
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../../../utils/alerts"
import { KTSVG } from "../../../../theme/helpers"
import clsx from "clsx"
import Modal from 'react-bootstrap/Modal'

function WRMStatus(props) {
  const {
    loading,
    WrmStatusDispatch,
    WrmStatusres,
    clearWrmStatusDispatch,
    getWRMCommentDispatch
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("risk-summary/update/")
  const id = url && url[1]
  const [rejectValue, setRejectValue] = useState()
  const [active, setActive] = useState(false)
  const [show, setShow] = useState(false)
  const [rejectShow, setRejectShow] = useState(false)
  const [reDoShow, setReDoShow] = useState(false)
  const [commentShow, setcommentShow] = useState(false)
  const [status, setStatus] = useState('APPROVED')

  const [errors, setErrors] = useState({
    reason: "",
  })
  const [formData, setFormData] = useState({
    comment: "",
  })
  const [holdFormData, setHoldFormData] = useState({
    status: "HOLD",
    reason: "",
  })
  const [rejectFormData, setRejectFormData] = useState({
    status: "REJECTED",
    reason: "",
    rejectType: "",
    rejectMoreValue: "",
  })
  const [approveFormData, setApproveFormData] = useState({
    status: "APPROVED",
    reason: "",
  })

  const handleTab = (e) => {
    setStatus(e.target.id)
  }

  const approveSubmit = () => {
    // const errors = {}
    // if (_.isEmpty(approveFormData.reason)) {
    //   errors.reason = USER_ERROR.REASON
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
      const params = {
        ids:[id],
        comments: approveFormData.reason,
        status: status,
      }
      WrmStatusDispatch(params)
    // }
  }

  const onConfirmHold = () => {
    const params = {
      ids:[id],
      comments: holdFormData.reason,
      status: "HOLD"
    }
    WrmStatusDispatch(params)
  }

  const holdSubmit = () => {
    const errors = {}
    if (_.isEmpty(holdFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.HOLD,
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmHold()
        },
        () => { }
      )
    }
  }

  const onConfirmReject = () => {
    const params = {
      ids:[id],
      comments: rejectFormData.reason,
      status: "REJECTED"
    }
    WrmStatusDispatch(params)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmReject()
        },
        () => { }
      )
    }
  }


  const commentSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.comment)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        comments: formData.comment,
      }
      WrmStatusDispatch(params)
    }
  }


  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
    setRejectValue(e.target.value)
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  useEffect(() => {
    if (WrmStatusres && WrmStatusres.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const params = {
        riskId: id
      }
      setShow(false)
      setRejectShow(false)
      setReDoShow(false)
      setcommentShow(false)
      setFormData({
        comment: ''
      })
      setRejectFormData({
        reason: "",
        status: "REJECTED",
      })
      setApproveFormData({
        reason: "",
        status: "APPROVED"
      })
      setHoldFormData({
        reason: "",
        status: "HOLD"
      })
      setStatus('APPROVED')
      getWRMCommentDispatch(params)
      clearWrmStatusDispatch()
    } else if (WrmStatusres && WrmStatusres.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        WrmStatusres && WrmStatusres.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearWrmStatusDispatch()
      setShow(true)
    }
  }, [WrmStatusres])


  const clearPopup = () => {
    setShow(false)
    setRejectShow(false)
    setReDoShow(false)
    setcommentShow(false)
    setFormData({
      comment: ''
    })
    setRejectFormData({
      reason: "",
    })
    setApproveFormData({
      reason: "",
    })
    setHoldFormData({
      reason: "",
    })
  }

  return (
    <>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to change this Users WRM Record?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <div className="d-flex mb-5 align-items-center justify-content-center mx-5">
                      <div className="form-check w-25">
                        <input className="form-check-input" type="radio" name="flexRadioDefault" id="APPROVED" onChange={handleTab} defaultChecked />
                        <label className="form-check-label" for="APPROVED">
                          Approve
                        </label>
                      </div>
                      <div className="form-check w-25">
                        <input className="form-check-input" type="radio" name="flexRadioDefault" id="REJECTED" onChange={handleTab} />
                        <label className="form-check-label" for="REJECTED">
                          Reject
                        </label>
                      </div>
                      <div className="form-check w-25">
                        <input className="form-check-input" type="radio" name="flexRadioDefault" id="HOLD" onChange={handleTab} />
                        <label className="form-check-label" for="HOLD">
                          Hold
                        </label>
                      </div>
                      <div className="form-check w-25">
                        <input className="form-check-input" type="radio" name="flexRadioDefault" id="FRAUD" onChange={handleTab} />
                        <label className="form-check-label" for="FRAUD">
                          Fraud
                        </label>
                      </div>
                    </div>
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Comments
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 ">
                      <textarea
                        name="reason"
                        type="text"
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              approveFormData.reason && errors.reason,
                          },
                          {
                            "is-valid":
                              approveFormData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => approveChange(e)}
                        autoComplete="off"
                        value={approveFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => approveSubmit()}
                          disabled={loading}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={rejectShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Status
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Reject :
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                      <textarea
                        name="reason"
                        type="text"
                        // className='form-control'
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid": rejectFormData.reason && errors.reason,
                          },
                          {
                            "is-valid": rejectFormData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => rejectChange(e)}
                        autoComplete="off"
                        value={rejectFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => rejectSubmit()}
                        >
                          <span className="indicator-label">
                            Submit
                          </span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <Modal
        show={commentShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Add Comment
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Comment :
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                      <textarea
                        name="comment"
                        type="text"
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid": formData.comment && errors.comment,
                          },
                          {
                            "is-valid": formData.comment && !errors.comment,
                          }
                        )}
                        placeholder="Comment"
                        onChange={(e) => handleChange(e)}
                        autoComplete="off"
                        value={formData.comment || ""}
                      />
                      {errors.comment && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.comment}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => commentSubmit()}
                        >
                          <span className="indicator-label">
                            Submit
                          </span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      <div className="row">
        <div className="col-lg-12">
          <div className="card-toolbar d-flex mt-5">
            <>
              <ul className="nav">
                <li className="nav-item">
                  <a
                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                    onClick={() => {
                      setShow(true)
                    }}
                  >
                    Change Status
                  </a>
                </li>
              </ul>
            </>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    WrmRTStatusStores,
  } = state
  return {
    WrmStatusres: WrmRTStatusStores && WrmRTStatusStores.WrmRTStatusres ? WrmRTStatusStores.WrmRTStatusres : {},
    loading: WrmRTStatusStores && WrmRTStatusStores.loading ? WrmRTStatusStores.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  WrmStatusDispatch: (params) => dispatch(WrmRTStatusAction.WrmRTStatus(params)),
  clearWrmStatusDispatch: () => dispatch(WrmRTStatusAction.clearWrmRTStatus()),
})

export default connect(mapStateToProps, mapDispatchToProps)(WRMStatus)