import { useEffect, useState } from 'react'
import _ from 'lodash'
import { useLocation } from "react-router-dom"
import moment from 'moment'
import 'react-circular-progressbar/dist/styles.css'
import ReactSpeedometer from "react-d3-speedometer"
import { connect } from 'react-redux'
import { WRMCommentdeleteActions, WRMRTCommentActions, exportFullReportActions, ExportListActions, GroupScoreWeightAgeActions, dashboardRTDetailsPdfActions } from '../../../../store/actions'
import { warningAlert, successAlert, confirmationAlert } from "../../../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { PMASTATUS, STATUS_RESPONSE } from '../../../../utils/constants'
import { KTSVG } from '../../../../theme/helpers'
import WRMStatus from './wrmARH'
import CommentEdit from "./wrmCommentEdit";
import { PDFDownloadLink } from '@react-pdf/renderer';
import MyPdfComponent from '../pdf/PDFFile'
import PDFFile from '../pdf/Pdf'
import HDFCPDF from '../pdf/hdfcPdf'

function Score(props) {
  const {
    websiteLink,
    exportLoading,
    exportLists,
    successVerifyDomain,
    domainRepetation,
    merchantIddetails,
    DashboardExportData,
    DashboardPmaLists,
    WRMlists,
    DeleteWRMData,
    getWRMCommentDispatch,
    deleteWRMCommentDispatch,
    clearDeleteWRMCommentDispatch,
    merchantSummary,
    getGroupScoreWeightAgeDispatch,
    RiskScoreWeightAge,
    getPrevAlertDetailsDispatch,
    dashboardDetails,
    playStoreData
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('realtime-summary/update/')
  const currentId = url && url[1]
  const [editMode, setEditMode] = useState(false)
  const [userId, setUserId] = useState('')
  const [loadingPdf, setLoadingPdf] = useState(false);
  const [pdfText, setPdfText] = useState(false);
  const pmaData = DashboardPmaLists && DashboardPmaLists.data ? DashboardPmaLists.data : ''
  const pma = pmaData && pmaData.pma ? pmaData.pma : ''
  const pmaCheck = pmaData && pmaData.pmaCheck ? pmaData.pmaCheck : ''
  const value = parseInt(DashboardExportData && DashboardExportData.riskScore);
  const clientId = merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id
  const [pmaDataList, setPmaDAta] = useState([])
  const websitePdfData = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.website && dashboardDetails.data.dashboardData.website.data
  useEffect(() => {
    if (websitePdfData && websitePdfData.length > 0) {
      let array = websitePdfData
      const http = {
        title: 'Threshold',
        value: `**Explanation for each code\n 200- successful (No Risk)\n 301-permanent redirecting (Risky)\n  400- Bad Request (Risky)\n 400- Bad Request (Risky)\n 500- Internal Server Error (Risky)\n 502- Bad Gateway (Risky)\n 503- Service Unavailable (Risky)\n 504- Gateway Timeout (Risky)`,
      }
      array.map(function (value, index) {
        if (value['title'] === "HTTP Status Code") {
          array.splice(index + 1, 0, http);
        }
      })
      setPmaDAta(array)
    }
  }, [websitePdfData])

  useEffect(() => {
    if (websitePdfData && websitePdfData.length > 0) {
      let array = websitePdfData
      const Domain = {
        title: 'Threshold',
        value: `**Less than 90 days - High Risk\n More than 90days - Low Risk\n `,
      }
      array.map(function (value, index) {
        if (value['title'] === "Domain Vintage") {
          array.splice(index + 1, 0, Domain);
        }
      })
      setPmaDAta(array)
    }
  }, [websitePdfData])

  useEffect(() => {
    if (clientId) {
      let params = {
        clientId: clientId
      }
      getGroupScoreWeightAgeDispatch(params)
    }
  }, [clientId])

  useEffect(() => {
    const params = {
      riskId: currentId
    }
    getWRMCommentDispatch(params)
    const pdfParams = {
      id: currentId,
      type: 'pdf'
    }
    getPrevAlertDetailsDispatch(pdfParams)
  }, [currentId])

  const onDeleteItem = (id) => {
    confirmationAlert(
      "Are you sure want to delete this comment,",
      "",
      'warning',
      'Yes',
      'No',
      () => {
        deleteWRMCommentDispatch(id)
      },
      () => { { } }
    )
  }

  useEffect(() => {
    if (DeleteWRMData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const params = {
        riskId: currentId
      }
      successAlert(
        DeleteWRMData && DeleteWRMData.message,
        'success'
      )
      getWRMCommentDispatch(params)
      clearDeleteWRMCommentDispatch()
    } else if (DeleteWRMData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        DeleteWRMData.message,
        '',
        'Ok'
      )
    }
    clearDeleteWRMCommentDispatch()
  }, [DeleteWRMData])

  useEffect(() => {
    if (loadingPdf) {
      setPdfText(true)
      setTimeout(() => {
        document.getElementById("pdf-download").click()
        setPdfText(false)
      }, 10000)
    }
  }, [loadingPdf])

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className='row g-5 g-xl-8' style={{ backgroundColor: '#f5f8fa' }}>
        <div className='col-lg-12'>
          <div className="card card-xl-stretch">
            <div className="card-body pt-0">
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <span className='d-flex justify-content-center mt-4 fw-boldest my-1 fs-3'>
                    Website
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer'
                      onClick={() => window.open(websiteLink)}
                    >- {websiteLink}</a>
                  </span>
                </div>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-start mt-4 fw-boldest my-1 fs-3'>
                    Company Name
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer text-capital'
                    >{DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : ''}</a>
                  </div>
                </div>
              </div>
              <div className="separator separator-dashed my-3" />
              <div className='row'>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-center position-relative'
                  >
                    {_.isNumber(value) &&
                      <ReactSpeedometer
                        maxValue={0}
                        minValue={100}
                        value={parseInt(DashboardExportData && DashboardExportData.riskScore)}
                        customSegmentStops={[100, 75, 35, 0]}
                        segmentColors={["tomato", "gold", "limegreen"]}
                        needleColor="red"
                        startColor="green"
                        segments={3}
                        endColor="blue"
                        className='pichart'
                        currentValueText={`Over All Score: ${value}`}
                        currentValuePlaceholderStyle={"#{value}"}
                      />}
                    <p className='position-absolute' style={{ bottom: '30%', fontSize: '14px', fontWeight: 'bolder' }}>{merchantSummary && merchantSummary.report && merchantSummary.report.riskScoreCategorizationStatus === "High" ?
                      <span className='text-danger'>High</span> : merchantSummary && merchantSummary.report && merchantSummary.report.riskScoreCategorizationStatus === "Low" ? <span className='text-success'>Low</span> : merchantSummary && merchantSummary.report && merchantSummary.report.riskScoreCategorizationStatus === "Medium" ? <span className='text-warning'>Medium</span> : null}</p>
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div className='d-flex align-items-center justify-content-between px-2'>
                    {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id && merchantSummary.clientId._id === "64d37dc8ecd5f61ba4cd96f9" ?
                      !loadingPdf ?
                        <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' onClick={() => setLoadingPdf(true)}>
                          <span className='indicator-label'>
                            <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                            PDF Report
                          </span>
                        </button> :
                        <PDFDownloadLink document={
                          <PDFFile
                            DashboardExportData={DashboardExportData}
                            merchantIddetails={merchantIddetails}
                            DashboardPmaLists={DashboardPmaLists}
                            websiteData={pmaDataList}
                            successVerifyDomain={successVerifyDomain}
                            domainRepetation={domainRepetation}
                            merchantSummary={merchantSummary}
                            RiskScoreWeightAge={RiskScoreWeightAge && RiskScoreWeightAge.result}
                            exportLists={exportLists && exportLists[0]}
                            playStoreData={playStoreData}
                          />
                        } filename="FORM">
                          {({ loading }) => (loading ? <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' >Loading Document...</button> : <button id='pdf-download' className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'> {pdfText ?
                            <span className='indicator-label'>
                              <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                              Downloading....
                            </span> : !exportLoading &&
                            <span className='indicator-label'>
                              <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                              PDF Report
                            </span>
                          }</button>)}
                        </PDFDownloadLink> :
                      merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id && merchantSummary.clientId._id === "63ce2d0c89dd542bd06d6ac1" ?
                        !loadingPdf ?
                          <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' onClick={() => setLoadingPdf(true)}>
                            <span className='indicator-label'>
                              <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                              PDF Report
                            </span>
                          </button> :
                          <PDFDownloadLink document={
                            <HDFCPDF
                              DashboardExportData={DashboardExportData}
                              merchantIddetails={merchantIddetails}
                              DashboardPmaLists={DashboardPmaLists}
                              websiteData={pmaDataList}
                              successVerifyDomain={successVerifyDomain}
                              domainRepetation={domainRepetation}
                              merchantSummary={merchantSummary}
                              RiskScoreWeightAge={RiskScoreWeightAge && RiskScoreWeightAge.result}
                              exportLists={exportLists && exportLists[0]}
                              playStoreData={playStoreData}
                            />
                          } filename="FORM">
                            {({ loading }) => (loading ? <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' >Loading Document...</button> : <button id='pdf-download' className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'> {pdfText ?
                              <span className='indicator-label'>
                                <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                                Downloading....
                              </span> : !exportLoading &&
                              <span className='indicator-label'>
                                <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                                PDF Report
                              </span>
                            }</button>)}
                          </PDFDownloadLink> :
                        !loadingPdf ?
                          <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' onClick={() => setLoadingPdf(true)}>
                            <span className='indicator-label'>
                              <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                              PDF Report
                            </span>
                          </button> :
                          <PDFDownloadLink document={
                            <PDFFile
                              DashboardExportData={DashboardExportData}
                              merchantIddetails={merchantIddetails}
                              DashboardPmaLists={DashboardPmaLists}
                              websiteData={pmaDataList}
                              successVerifyDomain={successVerifyDomain}
                              domainRepetation={domainRepetation}
                              merchantSummary={merchantSummary}
                              RiskScoreWeightAge={RiskScoreWeightAge && RiskScoreWeightAge.result}
                              exportLists={exportLists && exportLists[0]}
                              playStoreData={playStoreData}
                            />
                          } filename="FORM">
                            {({ loading }) => (loading ? <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' >Loading Document...</button> : <button id='pdf-download' className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'> {pdfText ?
                              <span className='indicator-label'>
                                <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                                Downloading....
                              </span> : !exportLoading &&
                              <span className='indicator-label'>
                                <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                                PDF Report
                              </span>
                            }</button>)}
                          </PDFDownloadLink>}
                    <WRMStatus getWRMCommentDispatch={getWRMCommentDispatch} />
                  </div>
                  <div>
                    {WRMlists && WRMlists.data && WRMlists.data.length > 0 ?
                      <h2 className='mb-2 d-flex justify-content-start symbol-label text-black mb-5'>Comments</h2>
                      : null
                    }
                    {
                      WRMlists && Array.isArray(WRMlists.data) > 0 ? WRMlists.data.map((item, i) => {
                        return (
                          <ul style={{ listStyleType: 'disc', paddingLeft: '1.5rem' }}>
                            <div className="d-flex justify-content-between align-items-center mb-2 w-75">
                              <div>
                                <div className="d-flex align-item-center">
                                  <div>
                                    <li>{item && item.riskStatus}&nbsp;: </li>
                                  </div>
                                  <div>
                                    &nbsp;{item && item.comments}
                                  </div>
                                </div>
                                <div>
                                  Created Date&nbsp;: {moment(item && item.createdAt).format('DD/MM/YYYY')}
                                </div>
                              </div>
                              <div className="d-flex align-item-center px-4">
                                <button
                                  className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px'
                                  title="Edit Data"
                                  onClick={() => {
                                    setEditMode(true)
                                    setUserId(item)
                                  }}
                                >
                                  <KTSVG
                                    path='/media/icons/duotune/art/art005.svg'
                                    className='svg-icon-3'
                                  />
                                </button>
                                <button
                                  className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-4'
                                  onClick={() => onDeleteItem(item && item._id)}
                                  title="Delete"
                                >
                                  <KTSVG
                                    path='/media/icons/duotune/general/gen027.svg'
                                    className='svg-icon-3'
                                  />
                                </button>
                              </div>
                            </div>
                          </ul>
                        )
                      }) : null}
                  </div>
                </div>
              </div>
            </div>
            {/* {
              merchantIddetails && merchantIddetails.data && merchantIddetails.data.clientId && merchantIddetails.data.clientId.pma === false ? (null) : (
                <div className='col-lg-12'>
                  <div className='fs-1 fw-boldest mt-4 d-flex justify-content-start ms-4'>
                    PMA
                  </div>
                  {
                    pmaCheck && pmaCheck["PMA Flag"] === 'Yes' ? (
                      <>
                        <div className="separator separator-dashed my-3" />
                        <div className="table-responsive ms-4">
                          <table className="table" style={{ textAlign: "center" }}>
                            <thead>
                              <tr className="fw-bold fs-6 text-gray-800">
                                <th>Heavy Discounts</th>
                                <th>Login Credentials Are Required</th>
                                <th>Page Navigation Issue</th>
                                <th>Pricing is in Dollars</th>
                                <th>Pricing is not updated</th>
                                <th>Website Redirection</th>
                                <th>Website is not working</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Heavy Discounts']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Login Credentials are required']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Page Navigation Issue']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is in Dollars']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is not updated']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website Redirection']]} mt-4 justify-content-center`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website is not working']]} mt-4 justify-content-center`} />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </>
                    ) : (
                      <div className='ml-5'>
                        <p>No violation detected</p>
                        <br />
                      </div>
                    )
                  }
                  <div className="separator separator-dashed my-3" />
                </div>
              )
            } */}
          </div>
        </div>
      </div>
      <CommentEdit editMode={editMode} setEditMode={setEditMode} userId={userId} setUserId={setUserId} currentId={currentId} />
    </>
  )
}

const mapStateToProps = (state) => {
  const { exportlistStore, WRMRTlistStore, WRMdeleteStore, exportFullReportlistStore, GroupScoreWeightAgegetStore, dashboardRTPdfStore } = state;
  return {
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists.data.data : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : '',
    fullWebReportResponse: exportFullReportlistStore && exportFullReportlistStore.exportFullReportlists ? exportFullReportlistStore.exportFullReportlists : '',
    fullWebReportLoading: exportFullReportlistStore && exportFullReportlistStore.loading ? exportFullReportlistStore.loading : false,
    WRMlists: WRMRTlistStore && WRMRTlistStore.WRMRTlists ? WRMRTlistStore.WRMRTlists : '',
    WRMlistsloading: WRMRTlistStore && WRMRTlistStore.loading ? WRMRTlistStore.loading : false,
    DeleteWRMData: WRMdeleteStore && WRMdeleteStore.DeleteWRMData ? WRMdeleteStore.DeleteWRMData : '',
    RiskScoreWeightAge: GroupScoreWeightAgegetStore && GroupScoreWeightAgegetStore.GroupScoreWeightAge ? GroupScoreWeightAgegetStore.GroupScoreWeightAge : [],
    dashboardDetails: dashboardRTPdfStore && dashboardRTPdfStore.dashboardRTDetails ? dashboardRTPdfStore.dashboardRTDetails : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),
  getFullWebReport: (params) => dispatch(exportFullReportActions.getExportFullReportlist(params)),
  getWRMCommentDispatch: (params) => dispatch(WRMRTCommentActions.getWRMRTCommentlist(params)),
  deleteWRMCommentDispatch: (params) => dispatch(WRMCommentdeleteActions.delete(params)),
  clearDeleteWRMCommentDispatch: () => dispatch(WRMCommentdeleteActions.clear()),
  getGroupScoreWeightAgeDispatch: (params) => dispatch(GroupScoreWeightAgeActions.getGroupScoreWeightAge(params)),
  getPrevAlertDetailsDispatch: (id) => dispatch(dashboardRTDetailsPdfActions.getdashboardRTDetailsPdf(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Score)