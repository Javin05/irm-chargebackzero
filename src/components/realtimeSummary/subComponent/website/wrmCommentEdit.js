import React, { useState, useEffect, Fragment } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import Modal from 'react-bootstrap/Modal'
import clsx from "clsx"
import { warningAlert, successAlert } from "../../../../utils/alerts"
import { WRMRTCommentActions, updateWRMCommentActions } from "../../../../store/actions"
import { WRMCommentEditValidation } from "./wrmCommentEditData"
import { setWRMCommentData } from "./formData"
import { STATUS_RESPONSE } from '../../../../utils/constants'

function CommentEdit(props) {
    const {
        currentId,
        editMode,
        setEditMode,
        userId,
        setUserId,
        getWRMCommentDispatch,
        UpdateWRMComments,
        updateWRMCommentDispatch,
        clearUpdateDispatch
    } = props
    const [formData, setFormData] = useState({
        riskStatus: "",
        comments: "",
    })
    const [errors, setErrors] = useState({})

    const handleChanges = (e) => {
        e.preventDefault()
        setFormData((values) => ({
            ...values,
            [e.target.name]: e.target.value
        }))
        setErrors({ ...errors, [e.target.name]: '' })
    }

    const clearPopup = () => {
        setEditMode(false)
        setFormData({
            comments: "",
            riskStatus: ""
        })
        setUserId('')
    }

    useEffect(() => {
        const data = setWRMCommentData(userId)
        setFormData(data)
    }, [userId])

    const handleSubmit = () => {
        const errorMsg = WRMCommentEditValidation(formData, setErrors)
        const updateValue = {
            comments: formData.comments,
            riskStatus: formData.riskStatus,
        }
        if (_.isEmpty(errorMsg)) {
            const editId = userId && userId._id
            updateWRMCommentDispatch(editId, updateValue)
        }
    }

    useEffect(() => {
        if (UpdateWRMComments.status === STATUS_RESPONSE.SUCCESS_MSG) {
            const params = {
                riskId: currentId
            }
            successAlert(
                UpdateWRMComments && UpdateWRMComments.message,
                'success'
            )
            getWRMCommentDispatch(params)
            clearUpdateDispatch()
            clearPopup()
        } else if (UpdateWRMComments.status === STATUS_RESPONSE.ERROR_MSG) {
            warningAlert(
                'Error',
                UpdateWRMComments && UpdateWRMComments.message,
                '',
                'Ok',
            )
            clearUpdateDispatch()
        }
    }, [UpdateWRMComments])

    return (
        <>
            <Modal
                show={editMode}
                size="lg"
                centered
                onHide={() => clearPopup()}>
                <Modal.Header
                    style={{ backgroundColor: 'rgb(126 126 219)' }}
                    closeButton={() => clearPopup()}>
                    <Modal.Title
                        style={{
                            color: 'white'
                        }}
                    >
                        Update Comment
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="card card-custom card-stretch gutter-b p-8">
                        <div className="row mb-8">
                            <div className='col-md-4'>
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                    WRM Status :
                                </label>
                            </div>
                            <div className='col-md-8'>
                                <select
                                    name='riskStatus'
                                    className='form-select form-select-solid'
                                    data-control='select'
                                    data-placeholder='Select an option'
                                    data-allow-clear='true'
                                    onChange={(e) => handleChanges(e)}
                                    value={formData && formData.riskStatus || ''}
                                    disabled
                                >
                                    <option value=''>Select...</option>
                                    <option value='APPROVED'>Approved</option>
                                    <option value='REJECTED'>Rejected</option>
                                    <option value='HOLD'>Hold</option>
                                    <option value='FRAUD'>Fraud</option>
                                </select>
                            </div>
                        </div>
                        <div className="row mb-8">
                            <div className='col-md-4'>
                                <label className="font-size-xs font-weight-bold mb-3 form-label">
                                    Comments :
                                </label>
                            </div>
                            <div className='col-md-8'>
                                <textarea
                                    name="comments"
                                    type="text"
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                    )}
                                    placeholder="Comment"
                                    onChange={(e) => handleChanges(e)}
                                    autoComplete="off"
                                    value={formData && formData.comments || ""}
                                />
                                {errors && errors.comments && (
                                    <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.comments}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="row">
                            <div className='col-md-4'>
                            </div>
                            <div className='col-md-8'>
                                <button
                                    className='btn btn-light-primary m-1 mt-8 font-5vw '
                                    onClick={handleSubmit}
                                >
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )

}

const mapStateToProps = (state) => {
    const { updateWRMStore } = state
    return {
        UpdateWRMComments: updateWRMStore && updateWRMStore.saveupdateWRMResponse ? updateWRMStore.saveupdateWRMResponse : ''
    }
}

const mapDispatchToProps = (dispatch) => ({
    getWRMCommentDispatch: (params) => dispatch(WRMRTCommentActions.getWRMRTCommentlist(params)),
    updateWRMCommentDispatch: (id, params) => dispatch(updateWRMCommentActions.updateWRMComment(id, params)),
    clearUpdateDispatch: () => dispatch(updateWRMCommentActions.clearupdateWRMComment()),
})

export default connect(mapStateToProps, mapDispatchToProps)(CommentEdit)