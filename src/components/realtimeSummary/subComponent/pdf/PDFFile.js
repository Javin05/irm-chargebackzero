import React from "react";
import { Page, Text, Image, Document, StyleSheet, View, Font } from "@react-pdf/renderer";
import MyCustomFont from './fonts/Anton-Regular.ttf';
import _ from 'lodash'
import moment from 'moment'
import { PMASTATUS, RiskPdfStatus } from '../../../../utils/constants'
import { toAbsoluteUrl } from '../../../../theme/helpers'

Font.register({
  family: 'AntonFamily',
  src: MyCustomFont
})
Font.registerEmojiSource({
  format: 'png',
  url: 'https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/',
});

const styles = StyleSheet.create({
  page: {
    width: "205mm",
    marginLeft: "auto",
    marginRight: "auto",
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    padding: 20,
  },
  content: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  title: {
    fontSize: 24,
    textAlign: "center",
    fontFamily: "AntonFamily",
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "AntonFamily",

  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
    fontFamily: "AntonFamily",
  },
  logo: {
    width: '50px',
    height: '50px',
    paddingBottom: '8px',
    objectFit: 'cover'
  },


  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  logo: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  headerText: {
    color: '#153d58',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    flex: 1,
  },
  scanDate: {
    marginLeft: 'auto',
    fontSize: "10px",
    color: '#153d58'
  },

  //table css

  table: {
    flexDirection: 'column',
    border: '1 solid #92bfde',
    borderRadius: 8,
    // backgroundColor:'##92bfde'
  },
  tableRow: {
    flexDirection: 'row',
  },
  tableCellHeader: {
    flex: 1,
    padding: 5,
    fontSize: "10px",
    color: 'black',
    fontWeight: 'bold',
  },
  tableCellData: {
    flex: 1,
    padding: 5,
    fontSize: "8px",
    color: '#82878a'

  },
  heading: {
    color: '#153d58',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'start',
    flex: 1,
  },

  //list

  row: {
    flexDirection: 'row',
    borderBottomColor: '#000',
    borderBottomStyle: 'solid',
    paddingVertical: 5,
  },
  cell: {
    width: '50%',
    padding: 5,
  },
  headerCell: {
    fontWeight: 'bold',
  },
  tableCellHeaderStatus: {
    flex: 1,
    padding: 5,
    fontSize: "10px",
    color: 'black',
    fontWeight: 'bold',
    width: '30%',
  },
  tableCellImage: {
    width: 50,
    height: 50,
    marginRight: 220,
  },



  //screenshot image

  screenshotimage: {
    width: '550px',
    height: '750px',
    paddingBottom: '8px',
    objectFit: 'contain'
  },

})

const MyCustomIcon = ({ status }) => {
  const icon = status === 'NO' ? '--' : '❌'
  return <Text>{icon}</Text>
};

const PDFFile = (props) => {

  const { DashboardExportData, merchantIddetails, DashboardPmaLists, websiteData, successVerifyDomain, domainRepetation, merchantSummary, playStoreData } = props
  const filteredPeople = websiteData && websiteData.filter((item) => item.title !== "Risk Score");
  return (
    <Document>
      <Page size={{ width: 600, height: 800 }} style={styles.page}>
        <View style={styles.section}>
          <View style={styles.header}>
            {DashboardExportData && DashboardExportData.logo === "No" ? null : DashboardExportData && DashboardExportData.logo === "No Data" ? null : DashboardExportData && DashboardExportData.logo ? (
              <Text>
                <Image src={DashboardExportData.logo} style={styles.logo} />
              </Text>
            ): null}
            <Text style={styles.headerText}>WRM Reports</Text>
            <Text style={styles.scanDate}>
              Scan Date: {merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt
                ? moment(merchantIddetails.data.createdAt).format('DD/MM/YYYY')
                : 'No Data'}
            </Text>
          </View>
          {/* <View style={{ marginTop: 10 }} /> */}
          <View style={{ borderBottomWidth: 1, borderBottomColor: 'black', marginTop: 10, marginBottom: 20 }} />
          <View style={styles.table}>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Website</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.website
                  ? merchantIddetails.data.website
                  : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Queue</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.queueId && merchantIddetails.data.queueId.queueName
                  ? merchantIddetails.data.queueId.queueName
                  : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Tag</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.tag
                  ? merchantIddetails.data.tag
                  : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Date Received</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt
                  ? moment(merchantIddetails.data.createdAt).format('DD/MM/YYYY')
                  : '--'}
              </Text>
            </View>
          </View>

          <View style={{ marginTop: 10, marginBottom: 20 }} />


          <View>
            <Text style={styles.heading}>
              WRM Inputs
            </Text>
          </View>

          <View style={{ marginTop: 10, marginBottom: 20 }} />

          <View style={styles.table}>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Phone Number</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.phoneInput ? DashboardExportData.phoneInput : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Acquirer</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.acquirer ? DashboardExportData.acquirer : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Email Id</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.emailInput ? DashboardExportData.emailInput : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>UPI</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.upiInput ? DashboardExportData.upiInput : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Legal Name</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>MCC Code</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : '--'}
              </Text>
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableCellHeader}>Business Address</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : '--'}
              </Text>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 900 }} style={styles.page}>
        <View style={styles.section}>
          <View style={{ marginTop: 10, marginBottom: 20 }} />
          <Text style={styles.heading}>PMA</Text>
          <View style={{ marginTop: 10, marginBottom: 20 }} />
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            {DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && (
              Object.keys(DashboardPmaLists.data.pma).map((parameter, index) => (
                <View style={{
                  flexDirection: 'row',
                  // borderBottomWidth: 1,
                  backgroundColor: index % 2 === 0 ? '#F5F5F5' : '#FFFFFF',
                  paddingVertical: 5,
                }} key={index} >
                  <Text style={styles.tableCellHeader}>{parameter}</Text>
                  <Text style={styles.tableCellData}>
                    <MyCustomIcon status={DashboardPmaLists.data.pma[parameter]} />
                  </Text>
                </View>
              ))
            )}
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 900 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Summary
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            <Text style={styles.heading}>
              Website Summary
            </Text>
          </View>
          <View style={{ marginTop: 10, marginBottom: 20 }} />

          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>#</Text>
              <Text style={[styles.tableCellData, styles.heading]}>WRM Validation</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Comments</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Risk</Text>
            </View>
            {
              Array.isArray(filteredPeople) ? (
                filteredPeople && filteredPeople.map((item, index) => (
                  <View key={index} style={{
                    flexDirection: 'row',
                    backgroundColor: index % 2 === 0 ? '#F5F5F5' : '#FFFFFF',
                    paddingVertical: 5
                  }}>
                    <Text style={styles.tableCellHeader}>{index + 1}</Text>
                    <Text style={styles.tableCellHeader}>{item.title}</Text>
                    <Text style={styles.tableCellHeader}>{item.value}</Text>
                    <Text style={{ ...styles.tableCellHeaderStatus, ...RiskPdfStatus[item.status] }}>{item.status}</Text>
                  </View>
                ))
              ) : 'No Records Found'}
          </View>
          <View style={{ marginTop: 10, marginBottom: 20 }} />
          <View>
            <Text style={styles.heading}>
              Merchant Summary
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            <Text style={styles.heading}>
              Website
            </Text>
          </View>
          <View style={{ marginTop: 10, marginBottom: 20 }} />
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Risk Score</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.riskScore ? DashboardExportData.riskScore : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Acquirer</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantIddetails && merchantIddetails.data && merchantIddetails.data.acquirer ? merchantIddetails.data.acquirer : '--'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Marketplace</Text>
                <Text style={styles.tableCellHeader}>
                  {playStoreData && playStoreData[0] && playStoreData[0].marketplace ? playStoreData[0].marketplace : "No"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {playStoreData && playStoreData[0] && playStoreData[0].marketplace === "Yes" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Reason</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.reason ? DashboardExportData.reason : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>MCC Scrapped</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>MCC Mismatch</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.mccCodeMatch ? DashboardExportData.mccCodeMatch : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Risk Classification</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.riskClassification ? DashboardExportData.riskClassification : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Working</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.websiteWorking ? DashboardExportData.websiteWorking : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 1000 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Overall Risk
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Risk Score</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.riskScore ? DashboardExportData.riskScore : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Status</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.status ? DashboardExportData.status : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Business Risk</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.businessRisk ? DashboardExportData.businessRisk : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Web Content Risk</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.transparencyRisk ? DashboardExportData.transparencyRisk : 'No Data'}                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Health</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteHealth ? DashboardExportData.websiteHealth : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Website Content Checkup Domain Health</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteContentCheckupDomainHealth ? DashboardExportData.websiteContentCheckupDomainHealth : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Online Website Reputation</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.onlineWebsiteReputation ? DashboardExportData.onlineWebsiteReputation : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Web Security</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.webSecurity ? DashboardExportData.webSecurity : 'No Data'}                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Contact Details
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Email Id</Text>
                <Text style={styles.tableCellHeader}>
                  {
                    Array.isArray(DashboardExportData && DashboardExportData.contactDetailsEmail) ?
                      DashboardExportData && DashboardExportData.contactDetailsEmail.map((email) => {
                        return (
                          <>{email}<br />
                          </>
                        )
                      })
                      : DashboardExportData && DashboardExportData.contactDetailsEmail
                  }
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Legal Name</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Telephone</Text>
                <Text style={styles.tableCellHeader}>
                  {Array.isArray(DashboardExportData && DashboardExportData.contactDetailsPhone) ?
                    DashboardExportData && DashboardExportData.contactDetailsPhone.map((Phone) => {
                      return (
                        <>{Phone}<br />
                        </>
                      )
                    })
                    : DashboardExportData && DashboardExportData.contactDetailsPhone}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Business Address</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Merchant Location
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Business Address</Text>
                <Text style={styles.tableCellHeader}>
                  {successVerifyDomain && successVerifyDomain.location ? successVerifyDomain.location : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Company name & address as image</Text>
                <Text style={styles.tableCellHeader}>
                  '--'
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Business Address matches website address</Text>
                <Text style={styles.tableCellHeader}>
                  --
                </Text>
              </View>
            </View>
          </View>
        </View>


        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Website Content
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            <Text style={styles.heading}>
              Website Categorization
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Registrar Name</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Code</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Risk Classification</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.riskClassification ? DashboardExportData.riskClassification : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Categories</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.reason ? DashboardExportData.reason : 'No data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Paid Up Capital</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.paidUpCapital ? DashboardExportData.paidUpCapital : 'No data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Google Analytics Id Relation</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.googleAnalyticsIdRelation ? DashboardExportData.googleAnalyticsIdRelation : 'No data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 850 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Website Details
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Legal Name</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Logo</Text>
                {DashboardExportData && DashboardExportData.logo === "No" ?
                  <Image
                    src={toAbsoluteUrl('/media/avatars/No_Image_Available.jpg')}
                    style={{ ...styles.tableCellImage }}
                    onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                  /> :
                  DashboardExportData && DashboardExportData.logo === "No Data" ?
                    <Image
                      src={toAbsoluteUrl('/media/avatars/No_Image_Available.jpg')}
                      style={{ ...styles.tableCellImage }}
                      onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                    /> :
                    DashboardExportData && DashboardExportData.logo ?
                    <Image
                      src={DashboardExportData.logo}
                      style={{ ...styles.tableCellImage }}
                      onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                    /> : <Image
                    src={toAbsoluteUrl('/media/avatars/No_Image_Available.jpg')}
                    style={{ ...styles.tableCellImage }}
                    onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                  />}
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Organization Address - Transparency</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Redirection</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Legal Name Match Percentage</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.legalNameMatchPercentage ? merchantSummary.report.legalNameMatchPercentage : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Products Service Description</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.productsServicesDescription ? merchantSummary.report.productsServicesDescription : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Primary Merchant Contact Name</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.primaryMerchantContactName ? merchantSummary.report.primaryMerchantContactName : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Card Scheme Compliance(BRAM/GBPP)
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Adult Content Monitoring</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.adultContentMonitoring ? DashboardExportData.adultContentMonitoring : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Predict Medicine</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.predictMedicine ? DashboardExportData.predictMedicine : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Merchant Intelligence</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.merchantIntelligence ? DashboardExportData.merchantIntelligence : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Merchant's Policies Url
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Terms & Condition</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.termsAndConditionPageUrl ? DashboardExportData.termsAndConditionPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Shipping Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.shippingPolicyPageUrl ? DashboardExportData.shippingPolicyPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Return Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.returnPolicyPageUrl ? DashboardExportData.returnPolicyPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Contact Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.contactUsPageUrl ? DashboardExportData.contactUsPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Privacy Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.privacyPolicyPageUrl ? DashboardExportData.privacyPolicyPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>About Us</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsPageUrl ? merchantSummary.report.aboutUsPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Cancellation Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyPageUrl ? merchantSummary.report.cancellationPolicyPageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Refund Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyPageUrl ? merchantSummary.report.refundPolicyPageUrl : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 1010 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Purchase Or Registration
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Id Submission Prompt</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Website Is Accessible Without Login Prompt
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Is Accessible Without Login Prompt</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.heading}>
              Web Analysis Status
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            <Text style={styles.heading}>
              Page Activity Check
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Is Mining Happening?</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageActivityCheckMining ? DashboardExportData.pageActivityCheckMining : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Any Untrusted Downloads?</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageActivityCheckUntrustedDownloads ? DashboardExportData.pageActivityCheckUntrustedDownloads : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Page Health Check
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Content Accessibilty</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageHealthCheckContentAccessibilty ? DashboardExportData.pageHealthCheckContentAccessibilty : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Page loading Time</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageHealthCheckPageLoadingTime ? DashboardExportData.pageHealthCheckPageLoadingTime : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Page Links Connectivity Check
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Status</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageAvailabilityCheckURLStatus ? DashboardExportData.pageAvailabilityCheckURLStatus : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Success Rate</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageLinksConnectivityCheckSuccessRate ? DashboardExportData.pageLinksConnectivityCheckSuccessRate : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Page Redirection Check
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Redirection</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Website Redirection Url</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteRedirectionURL ? DashboardExportData.websiteRedirectionURL : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Address
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Address Scrapped From Website</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.addressScrappedFromWebsite ? DashboardExportData.addressScrappedFromWebsite : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Address From Google Map</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.addressFromGoogleMap ? DashboardExportData.addressFromGoogleMap : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Matched Address From Google Map</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.matchedAddressFromGoogleMap ? DashboardExportData.matchedAddressFromGoogleMap : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 1110 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Content Monitoring
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Currency</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.currenciesFoundOnWebsite ? DashboardExportData.currenciesFoundOnWebsite : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Price Listing</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.productsPriceListedInWebsite ? DashboardExportData.productsPriceListedInWebsite : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Unreasonable Price</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteContainsUnreasonablePrice ? DashboardExportData.websiteContainsUnreasonablePrice : 'No Data'}
                </Text>
              </View>
              {/* <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Accessibility</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </Text>
              </View> */}
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Average Price</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.averageProductPrice ? DashboardExportData.averageProductPrice : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Min Price Listed</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.minPriceListedInHomePage ? DashboardExportData.minPriceListedInHomePage : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Max Price Listed</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.maxPriceListedInHomePage ? DashboardExportData.maxPriceListedInHomePage : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              Malware Risk
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Malware Risk</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.malwareRisk ? DashboardExportData.malwareRisk : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Malware Present</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.malwarePresent ? DashboardExportData.malwarePresent : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              Policy Information Details
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Drop Shipping Website</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.dropShippingWebsite ? merchantSummary.report.dropShippingWebsite : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>International Keywords In Shipping Page</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.internationalKeywordsInShippingPage ? merchantSummary.report.internationalKeywordsInShippingPage : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Cod Keywords In Shipping Page</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.codKeywordsInShippingPage ? merchantSummary.report.codKeywordsInShippingPage : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Delivery Timeline Shipping</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.deliveryTimelineShippingKeyword ? merchantSummary.report.deliveryTimelineShippingKeyword : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Indian Law Keywords In TermsPage</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.indianLawKeywordsInTermsPage ? merchantSummary.report.indianLawKeywordsInTermsPage : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Company Name In TermsPage</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.companyNameInTermsPage ? merchantSummary.report.companyNameInTermsPage : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Security Keywords In PrivacyPage</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.securityKeywordsInPrivacyPage ? merchantSummary.report.securityKeywordsInPrivacyPage : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              MCA Details
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Mca Blacklisted Company And Cin</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.mcaBlacklistedCompanyAndCin ? merchantSummary.report.mcaBlacklistedCompanyAndCin : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Mca Blacklisted Status</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.mcaBlacklistedStatus ? merchantSummary.report.mcaBlacklistedStatus : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              Transaction Laundering
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Transaction laundering address matched uls</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.transaction_laundering_address_matched_uls ? merchantSummary.report.transaction_laundering_address_matched_uls : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Transaction laundering legalname matched uls</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.transaction_laundering_legalname_matched_uls ? merchantSummary.report.transaction_laundering_legalname_matched_uls : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Transaction laundering email matched uls</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.transaction_laundering_email_matched_uls ? merchantSummary.report.transaction_laundering_email_matched_uls : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Transaction laundering phone matched uls</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.transaction_laundering_phone_matched_uls ? merchantSummary.report.transaction_laundering_phone_matched_uls : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              PMA Checks
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>All Violation</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.allViolation ? merchantSummary.report.allViolation : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              Mirror Pages
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Mirror AboutUs Page</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.mirrorAboutUsPage && merchantSummary.report.mirrorAboutUsPage.length > 100 ? merchantSummary.report.mirrorAboutUsPage.substring(0, 100) + '...' : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Mirror Home Pages</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.mirrorHomePage && merchantSummary.report.mirrorHomePage.length > 100 ? merchantSummary.report.mirrorHomePage.substring(0, 100) + '...' : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              BlackList Details
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Upi in blacklisted</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.upi_in_blacklisted ? merchantSummary.report.upi_in_blacklisted : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Phone in blacklisted</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.phone_in_blacklisted ? merchantSummary.report.phone_in_blacklisted : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Email in blacklisted</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.email_in_blacklisted ? merchantSummary.report.email_in_blacklisted : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Upi blacklisted website</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.upi_blacklisted_website ? merchantSummary.report.upi_blacklisted_website : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.heading}>
              Linked Accounts
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>No of accounts upi</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.no_of_accounts_upi ? merchantSummary.report.no_of_accounts_upi : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>No of accounts phone</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.no_of_accounts_phone ? merchantSummary.report.no_of_accounts_phone : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>No of accounts email</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.no_of_accounts_email ? merchantSummary.report.no_of_accounts_email : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Linked to rejected cases</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.linked_to_rejected_cases ? merchantSummary.report.linked_to_rejected_cases : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Linked cases</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.linked_cases ? merchantSummary.report.linked_cases : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>No accounts linked</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.no_accounts_linked ? merchantSummary.report.no_accounts_linked : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Linked accounts</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.linked_accounts ? merchantSummary.report.linked_accounts : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Is account linked</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.is_account_linked ? merchantSummary.report.is_account_linked : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Rejection reason</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.rejection_reason ? merchantSummary.report.rejection_reason : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Website Content Set-up
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            <Text style={styles.heading}>
              Domain Info
            </Text>
          </View>
          <View style={{ marginTop: 10, marginBottom: 20 }} />

          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Registration Company</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : "No Data"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Registered</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRegistered ? DashboardExportData.domainRegistered : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Rank</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRank ? DashboardExportData.domainRank : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Registrant
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Registered Date</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainRegistrationDate) ? DashboardExportData.domainRegistrationDate : "No Data"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Expiry Date</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainRegistrationExpiryDate) ? DashboardExportData.domainRegistrationExpiryDate : "No Data"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Active For</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainActivefor) ? DashboardExportData.domainActivefor+" days" : "No Data"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Expiring In</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainExpiringin) ? DashboardExportData.domainExpiringin+" days" : "No Data"}                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              SSL Check
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Ssl Certificate Check</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.sslCertificateCheck ? DashboardExportData.sslCertificateCheck : "No Data"}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              IP Details
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Dns History Ip</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.dnsHistoryIp && merchantSummary.report.dnsHistoryIp.length > 100 ? merchantSummary.report.dnsHistoryIp.substring(0, 100) + '...' : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Ip Location</Text>
                <Text style={styles.tableCellHeader}>
                  {merchantSummary && merchantSummary.report && merchantSummary.report.ipLocation ? merchantSummary.report.ipLocation : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Screenshots
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Terms & Condition
              </Text>
              <Text style={styles.tableCellHeader}>
                {DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.termsAndConditionStatus],
              }}>{DashboardExportData && DashboardExportData.termsAndConditionStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Privacy Policy
              </Text>
              <Text style={styles.tableCellHeader}>
                {DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.privacyPolicyStatus],
              }}>{DashboardExportData && DashboardExportData.privacyPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Shipping Policy
              </Text>
              <Text style={styles.tableCellHeader}>
                {DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.shippingPolicyStatus],
              }}>{DashboardExportData && DashboardExportData.shippingPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Return Policy
              </Text>
              <Text style={styles.tableCellHeader}>
                {DashboardExportData && DashboardExportData.returnPolicyPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.returnPolicyStatus],
              }}>{DashboardExportData && DashboardExportData.returnPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Contact Policy
              </Text>
              <Text style={styles.tableCellHeader}>
                {DashboardExportData && DashboardExportData.contactUsPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.contactsFormStatus],
              }}>{DashboardExportData && DashboardExportData.contactsFormStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>About Us
              </Text>
              <Text style={styles.tableCellHeader}>
                {merchantSummary && merchantSummary.report ? merchantSummary.report.aboutUsPageScreenshot : 'No Data'}
              </Text>
              <Text style={{
                ...RiskPdfStatus[merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsStatus],
              }}>{merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Cancellation Policy
              </Text>
              <Text style={styles.tableCellHeader}>
                {merchantSummary && merchantSummary.report ? merchantSummary.report.cancellationPolicyPageScreenshot : 'No Data'}
              </Text>
              <Text style={{
                ...RiskPdfStatus[merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyStatus],
              }}>{merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Refund Policy
              </Text>
              <Text style={styles.tableCellHeader}>
                {merchantSummary && merchantSummary.report ? merchantSummary.report.refundPolicyPageScreenshot : 'No Data'}
              </Text>
              <Text style={{
                ...RiskPdfStatus[merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyStatus],
              }}>{merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyStatus}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={styles.heading}>
              Product Pricing Url
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={styles.row}>
              <Text style={[styles.tableCellData, styles.heading]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading]}>Description</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Home</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.productPricingHomePageUrl ? DashboardExportData.productPricingHomePageUrl : 'No Data'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Product Page</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.productPricingProductPageUrl ? DashboardExportData.productPricingProductPageUrl : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.heading}>Terms and Conditions</Text>
          <View style={{ marginTop: 10, marginBottom: 20 }} />
          {merchantSummary && merchantSummary.report && merchantSummary.report.termsAndConditionPageScreenshot === "No Data" ?
            <Text className='text-muted text-center fw-semibold text-gray-700'>
              Terms and Conditions Page Not Found
            </Text> :
            !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.termsAndConditionPageScreenshot) ? (
              <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.termsAndConditionPageScreenshot} style={styles.screenshotimage} />
            ) : (
              <Text className='text-muted fw-semibold text-gray-700'>
                Terms and Conditions Page Not Found
              </Text>
            )}
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Privacy Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.privacyPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                Privacy Policy Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.privacyPolicyPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.privacyPolicyPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  Privacy Policy Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Shipping Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.shippingPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                Shipping Policy Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.shippingPolicyPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.shippingPolicyPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  Shipping Policy Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Returns Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.returnPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                Returns Policy Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.returnPolicyPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.returnPolicyPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  Returns Policy Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Contact Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.contactUsPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                Contact Policy Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.contactUsPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.contactUsPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  Contact Policy Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              About Us
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                About Us Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.aboutUsPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  About Us Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Cancellation Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                Cancellation Policy Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.cancellationPolicyPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  Cancellation Policy Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={styles.heading}>
              Refund Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700'>
                Refund Policy Page Not Found
              </Text> :
              !_.isEmpty(merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyPageScreenshot) ? (
                <Image src={merchantSummary && merchantSummary.report && merchantSummary.report.refundPolicyPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700'>
                  Refund Policy Page Not Found
                </Text>
              )}
          </View>
        </View>
      </Page>
    </Document>
  );
};

export default PDFFile;
