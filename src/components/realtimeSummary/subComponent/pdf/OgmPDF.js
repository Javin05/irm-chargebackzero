import {
  Document,
  Font,
  Page,
  StyleSheet,
  Text,
  View,
} from "@react-pdf/renderer";
import moment from "moment";
import MyCustomFont from "./fonts/Anton-Regular.ttf";

Font.register({
  family: "AntonFamily",
  src: MyCustomFont,
});
Font.registerEmojiSource({
  format: "png",
  url: "https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/",
});

const styles = StyleSheet.create({
  page: {
    width: "205mm",
    marginLeft: "auto",
    marginRight: "auto",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    padding: 20,
  },
  content: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  title: {
    fontSize: 24,
    textAlign: "center",
    fontFamily: "AntonFamily",
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "AntonFamily",
  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
    fontFamily: "AntonFamily",
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  logo: {
    width: "100px",
    height: "40px",
    objectFit: "contain",
  },
  headerText: {
    color: "#153d58",
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "start",
  },
  scanDate: {
    textAlign: "end",
    fontSize: "11px",
    color: "#153d58",
  },
  //table css
  table: {
    flexDirection: "column",
    border: "1 solid #92bfde",
    borderRadius: 8,
  },
  tableRow: {
    flexDirection: "row",
  },
  tableCellHeader: {
    flex: 1,
    padding: 5,
    fontSize: "10px",
    color: "black",
    fontWeight: "bolder",
  },
  tableCellData: {
    flex: 1,
    padding: 5,
    fontSize: "9px",
    color: "black",
  },
  heading: {
    color: "#fff",
    fontSize: 12,
    fontWeight: "bold",
    textAlign: "start",
    flex: 1,
    fontWeight: "bold",
  },
  //list
  row: {
    flexDirection: "row",
    borderBottomColor: "#000",
    borderBottomStyle: "solid",
    paddingVertical: 5,
  },
  cell: {
    width: "50%",
    padding: 5,
  },
  headerCell: {
    fontWeight: "bold",
  },
  tableCellHeaderStatus: {
    flex: 1,
    padding: 5,
    fontSize: "10px",
    color: "black",
    fontWeight: "bold",
    width: "30%",
  },
  tableCellImage: {
    width: 50,
    height: 50,
    marginRight: 220,
  },
  tableHead: {
    fontSize: "10px",
  },
  flexTable: {
    flex: 0.5,
  },
  headerBgColor: {
    backgroundColor: "#5151b9",
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    fontWeight: "bold",
  },
  alignCenter: {
    textAlign: "center",
  },
  //screenshot image
  screenshotimage: {
    width: "550px",
    height: "750px",
    paddingBottom: "8px",
    objectFit: "contain",
  },
  ellipsisDashboardSummary: {
    maxWidth: "250px",
    height: "100px",
  },
});
const PDFDashboardSummary = ({
  LastScanData,
  CurrentScandata,
  MonitorDashboardIData,
  lastScaneDate,
}) => {
  const CurrentScandataBlackListValue =
    CurrentScandata &&
      CurrentScandata?.blackListAdvanceKeyword &&
      CurrentScandata?.blackListAdvanceKeyword?.value !== "No Data"
      ? JSON.parse(CurrentScandata.blackListAdvanceKeyword.value)
      : null;

  const LastScanDataBlackListValue =
    LastScanData?.blackListAdvanceKeyword?.value &&
      !LastScanData?.blackListAdvanceKeyword?.value === "No Data"
      ? JSON.parse(LastScanData.blackListAdvanceKeyword.value)
      : null;
  const CurrentScandataformattedKeywords = CurrentScandataBlackListValue
    ? Object.entries(CurrentScandataBlackListValue)
      .map(([key, values]) => {
        const formattedValues = values
          .map((value) => value.split(" ")[0])
          .join(", ");
        return `${key} :\n${formattedValues}`;
      })
      .join("\n\n")
    : "--";

  const LastScanDataformattedKeywords =
    LastScanDataBlackListValue && LastScanDataBlackListValue !== null
      ? Object.entries(LastScanDataBlackListValue)
        .map(([key, values]) => {
          const formattedValues = values
            .map((value) => value.split(" ")[0])
            .join(", ");
          return `${key} :\n${formattedValues}`;
        })
        .join("\n\n")
      : "";

  return (
    <Document>
      <Page size={{ width: 600, height: 800 }} style={styles.page}>
        <View style={styles.section}>
          <View style={styles.header}>
            <Text style={styles.scanDate}>Ongoing Monitoring Reports</Text>
            {/* {DashboardExportData && DashboardExportData.logo === "No" ? null : DashboardExportData && DashboardExportData.logo === "No Data" ? null : (
              <Text>
                <Image src={DashboardExportData.logo} style={styles.logo} />
              </Text>
            )} */}

            <Text style={styles.scanDate}>
              Scan Date:{" "}
              {CurrentScandata && CurrentScandata.ogm_run_date
                ? moment(CurrentScandata.ogm_run_date ?? "--").format(
                  "MMM Do YYYY"
                )
                : ""}
            </Text>
          </View>

          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "black",
              marginBottom: 20,
              marginTop: 5,
            }}
          />
          <View style={{ marginTop: 10 }} />
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text
                style={[styles.tableCellData, styles.heading, styles.tableHead]}
              >
                Changed Parameters
              </Text>
            </View>

            <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
              <Text style={[styles.tableCellData, { marginVertical: "2px" }]}>
                {MonitorDashboardIData?.changesDetectedStatus
                  ?.split(",")
                  .map((sentence) => sentence.trim())
                  .join(", ")}
              </Text>
            </View>
          </View>

          <View>
            <Text style={{ ...styles.heading, textDecoration: "underline" }}>
              Changed Parameters
            </Text>
          </View>

          <View style={[styles.table, { marginTop: 10 }]}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text
                style={[styles.tableCellData, styles.heading, styles.tableHead]}
              >
                Parameter
              </Text>
              {lastScaneDate?.[lastScaneDate.length - 2]?.ogm_run_date ? (
                <Text
                  style={[
                    styles.tableCellData,
                    styles.heading,
                    styles.tableHead,
                  ]}
                >
                  Last scan{" "}
                  {moment(
                    lastScaneDate?.[lastScaneDate.length - 2]?.ogm_run_date ??
                    "--"
                  ).format("MMM Do YYYY")}
                </Text>
              ) : (
                <Text
                  style={[
                    styles.tableCellData,
                    styles.heading,
                    styles.tableHead,
                  ]}
                >
                  Base Line Scan
                  {/* on
                {moment(MonitorDashboardIData?.ogm_start_date ?? "--").format("MMM Do YYYY")} */}
                </Text>
              )}
              <Text
                style={[styles.tableCellData, styles.heading, styles.tableHead]}
              >
                Current Scan on{" "}
                {moment(CurrentScandata.ogm_run_date ?? "--").format(
                  "MMM Do YYYY"
                )}
              </Text>
              <Text
                style={[styles.tableCellData, styles.heading, styles.tableHead]}
              >
                Changes Detected
              </Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Website Working?</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.websiteWorking?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.websiteWorking?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.websiteWorking?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Website Success Rate</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.websiteSuccessRate?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.websiteSuccessRate?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.websiteSuccessRate?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>
                  Line of Business
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.changeInLineOfBusiness?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInLineOfBusiness?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInLineOfBusiness?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>
                  MCC
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.changeInMcc?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInMcc?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInMcc?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Risk Classification</Text>
                <Text style={{ ...styles.tableCellData }}>

                  {LastScanData?.riskClassification?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>

                  {CurrentScandata?.riskClassification?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.riskClassification?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Legal Name</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.changeInLegalName?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInLegalName?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInLegalName?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Merchant Address</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.changeInMerchantAddress?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInMerchantAddress?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.changeInMerchantAddress?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Website Redirection</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.transactionLaunderingRedirection?.value ??
                    "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.transactionLaunderingRedirection?.value ??
                    "--"}
                </Text>

                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.transactionLaunderingRedirection
                    ?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Page Loading Time</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.increaseInFullPageLoadingTime?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.increaseInFullPageLoadingTime?.value ??
                    "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.increaseInFullPageLoadingTime
                    ?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              {/* <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Policy Compliance</Text>
              </View> */}
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Return Policy</Text>
                <Text style={{ ...styles.tableCellData }}>
                  <Text style={{ ...styles.tableCellData }}>
                    <a
                      className="color-primary cursor-pointer"
                      style={{ color: "#4A90E2" }}
                      onClick={() =>
                        window.open(
                          LastScanData?.returnPolicyPageUrl?.value ?? "--",
                          "_blank"
                        )
                      }
                    >
                      {LastScanData?.returnPolicyPageUrl?.value ?? "--"}
                    </a>
                  </Text>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  <a
                    className="color-primary cursor-pointer"
                    style={{ color: "#4A90E2" }}
                    onClick={() =>
                      window.open(
                        CurrentScandata?.returnPolicyPageUrl?.value ?? "--",
                        "_blank"
                      )
                    }
                  >
                    {CurrentScandata?.returnPolicyPageUrl?.value ?? "--"}
                  </a>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.returnPolicyPageUrl?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Terms and Condition</Text>
                <Text style={{ ...styles.tableCellData }}>
                  <Text style={{ ...styles.tableCellData }}>
                    <a
                      className="color-primary cursor-pointer"
                      style={{ color: "#4A90E2" }}
                      onClick={() =>
                        window.open(
                          LastScanData?.termsAndConditionPageUrl?.value ?? "--",
                          "_blank"
                        )
                      }
                    >
                      {LastScanData?.termsAndConditionPageUrl?.value ?? "--"}
                    </a>
                  </Text>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  <a
                    className="color-primary cursor-pointer"
                    style={{ color: "#4A90E2" }}
                    onClick={() =>
                      window.open(
                        CurrentScandata?.termsAndConditionPageUrl?.value ??
                        "--",
                        "_blank"
                      )
                    }
                  >
                    {CurrentScandata?.termsAndConditionPageUrl?.value ?? "--"}
                  </a>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.termsAndConditionPageUrl?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Privacy Policy</Text>
                <Text style={{ ...styles.tableCellData }}>
                  <Text style={{ ...styles.tableCellData }}>
                    <a
                      className="color-primary cursor-pointer"
                      style={{ color: "#4A90E2" }}
                      onClick={() =>
                        window.open(
                          LastScanData?.privacyPolicyPageUrl?.value ?? "--",
                          "_blank"
                        )
                      }
                    >
                      {LastScanData?.privacyPolicyPageUrl?.value ?? "--"}
                    </a>
                  </Text>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  <a
                    className="color-primary cursor-pointer"
                    style={{ color: "#4A90E2" }}
                    onClick={() =>
                      window.open(
                        CurrentScandata?.privacyPolicyPageUrl?.value ?? "--",
                        "_blank"
                      )
                    }
                  >
                    {CurrentScandata?.privacyPolicyPageUrl?.value ?? "--"}
                  </a>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.privacyPolicyPageUrl?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Shipping Policy</Text>
                <Text style={{ ...styles.tableCellData }}>
                  <Text style={{ ...styles.tableCellData }}>
                    <a
                      className="color-primary cursor-pointer"
                      style={{ color: "#4A90E2" }}
                      onClick={() =>
                        window.open(
                          LastScanData?.shippingPolicyPageUrl?.value ?? "--",
                          "_blank"
                        )
                      }
                    >
                      {LastScanData?.shippingPolicyPageUrl?.value ?? "--"}
                    </a>
                  </Text>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  <a
                    className=" cursor-pointer"
                    style={{ color: "#4A90E2" }}
                    onClick={() =>
                      window.open(
                        CurrentScandata?.shippingPolicyPageUrl?.value ?? "--",
                        "_blank"
                      )
                    }
                  >
                    {CurrentScandata?.shippingPolicyPageUrl?.value ?? "--"}
                  </a>
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.shippingPolicyPageUrl?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Domain Expiry Risk</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.domainRisk?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.domainRisk?.value ?? "--"}
                </Text>

                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.domainRisk?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Parked Domain</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.parkedDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.parkedDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.parkedDomain?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>
                  Online Reputation Drop
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.onlineReputationDrop?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.onlineReputationDrop?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.onlineReputationDrop?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Heavy Discounts</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.heavyDiscounts?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.heavyDiscounts?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.heavyDiscounts?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Non INR Pricing</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.nonInrPricing?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.nonInrPricing?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.nonInrPricing?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>
                  Contact Details - Phone
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.contactDetailsPhone?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.contactDetailsPhone?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.contactDetailsPhone?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>
                  Valid Contact - Phone
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.containsValidPhone?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.containsValidPhone?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.containsValidPhone?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}

                  {/* /////////// */}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>
                  Valid Contact - Email
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.containsValidEmail?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.containsValidEmail?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.containsValidEmail?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>
                  Contact Details - Email
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.contactDetailsEmail?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.contactDetailsEmail?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.contactDetailsEmail?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Malware Domain</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.malwareDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.malwareDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.malwareDomain?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#FFFFFF" }}>
                <Text style={styles.tableCellHeader}>Phishing Domain</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.phishingDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.phishingDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.phishingDomain?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: "#ededff" }}>
                <Text style={styles.tableCellHeader}>Spamming Domain</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanData?.spammingDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.spammingDomain?.value ?? "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.spammingDomain?.changeDetected === "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
              <View
                style={{
                  ...styles.tableRow,
                  backgroundColor: "#ededff",
                  marginTop: "10px",
                }}
              >
                <Text style={styles.tableCellHeader}>
                  Black List Advanced Keyword
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {LastScanDataformattedKeywords
                    ? LastScanDataformattedKeywords
                    : "--"}
                </Text>
                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandataformattedKeywords}
                </Text>

                <Text style={{ ...styles.tableCellData }}>
                  {CurrentScandata?.blackListAdvanceKeyword?.changeDetected ===
                    "YES"
                    ? "❌"
                    : "✅"}
                </Text>
              </View>
            </View>
          </View>
        </View>

        {/* ////////////// */}
      </Page>
    </Document>
  );
};

export default PDFDashboardSummary;
