import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import { useLocation } from 'react-router-dom'
import { merchantIdDetailsActions, GetAsigneeActions, AsiggnActions } from '../../store/actions'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import { STATUS_RESPONSE } from '../../utils/constants'
import _ from 'lodash'

function MerchantDetails(props) {
  const {
    setOpenPhone,
    setOpenEmail,
    merchantSummary,
    getAssignee,
    getAsigneeslistDispatch,
    updateAssignDispatch,
    updateAssign,
    clearAsiggnDispatch,
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [AsigneesOption, setAsignees] = useState()
  const [formData, setFormData] = useState({
    assignedTo: ''
  })
  const [value, setValue] = useState()

  useEffect(() => {
    const params = {
      tag: "IRM"
    }
    getAsigneeslistDispatch(params)
  }, [])

  const StatusArray = {
    "PENDING": "badge-light-warning",
    "APPROVED": "badge-light-success",
    "REJECTED": "badge-light-danger",
    "HOLD": "badge-light-warning",
    "MANUAL REVIEW": "badge badge-orange text-black",
  }

  const AsigneesNames = getAssignee && getAssignee.data && getAssignee.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].firstName, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const onConfirmupdate = (() => {
    updateAssignDispatch(currentId, formData)
  })

  const Assign = (() => {
    confirmationAlert(
      "Do You Want To",
      `Assign this Case #${merchantSummary && merchantSummary.riskId} to ${value}?`,
      'warning',
      'Yes',
      'No',
      () => { onConfirmupdate() },
      () => { }
    )
  })

  useEffect(() => {
    if (updateAssign && updateAssign.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        "Assigned Successfully",
        'success',
      )
      clearAsiggnDispatch()
      setFormData({
        assignedTo: ''
      })
    } else if (updateAssign && updateAssign.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        updateAssign.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearAsiggnDispatch()
    }
  }, [updateAssign])

  return (
    <>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='card-header border-0'>
          <h3 className='card-title align-items-start flex-column '>
            <span className='d-flex align-items-center fw-boldest my-1 fs-2'>Case Details - Case ID # WRM{merchantSummary && merchantSummary.riskId ? merchantSummary.riskId : '--'}
              <span className={`badge ml-2 ${merchantSummary && merchantSummary.riskStatus ? StatusArray[merchantSummary && merchantSummary.riskStatus] : ""}`} style={{ padding: "4px 12px", marginLeft: "10px", fontSize: "14px" }}>
                {merchantSummary && merchantSummary.riskStatus ? merchantSummary.riskStatus : ""}
              </span>
            </span>
          </h3>
          <div className='col-lg-4' />
          {
            !_.isEmpty(formData.assignedTo) ? (
              <span className='mt-4'>
                <button
                  className="btn btn-light-primary btn-sm"
                  onClick={Assign}
                >
                  Assign
                </button>
              </span>
            ) : (
              null
            )
          }
        </div>
        <div className="separator separator-dashed my-3" />
        <div className='card-body pt-0'>
          <div className='row g-5 g-xl-8'>
            <div className='col-sm-4 col-md-4 col-lg-4'>
              <div className='card card-xl-stretch'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Queue:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.queueId && merchantSummary.queueId.queueName ? merchantSummary.queueId.queueName : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-4 col-md-4 col-lg-4'>
              <div className='card card-xl-stretch'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Date Received:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {moment(merchantSummary && merchantSummary.createdAt ? merchantSummary.createdAt : "--").format('DD/MM/YYYY')}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12 ps-9'>
              <h3>Wrm Realtime Inputs</h3>
            </div>
            <div className='col-sm-4 col-md-4 col-lg-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Website:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.website ? merchantSummary.website : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Acquirer:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.acquirer ? merchantSummary.acquirer : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Tag:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.tag ? merchantSummary.tag : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Email Id:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold Id-response-Link' onClick={() => (setOpenEmail(true))}>
                        {merchantSummary && merchantSummary.businessEmail ? merchantSummary.businessEmail : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Phone Number:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold Id-response-Link' onClick={() => (setOpenPhone(true))}>
                        {merchantSummary && merchantSummary.businessPhone ? merchantSummary.businessPhone : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        UPI:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.upi ? merchantSummary.upi : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Legal Name:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.legalName ? merchantSummary.legalName : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-4 col-md-4 col-lg-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        MCC Code:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.mccCodeInput ? merchantSummary.mccCodeInput : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Business Address:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.businessAddress ? merchantSummary.businessAddress : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Billing Descriptor:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.billingDescriptor ? merchantSummary.billingDescriptor : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Merchant DBA:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.merchantDBA ? merchantSummary.merchantDBA : ''}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        ID1:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.id1 ? merchantSummary.id1 : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Master Card ICA:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.masterCardICA ? merchantSummary.masterCardICA : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Visa BIN:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.visaBIN ? merchantSummary.visaBIN : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-4 col-md-4 col-lg-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-body pt-0'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        City:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.city ? merchantSummary.city : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        State:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.state ? merchantSummary.state : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Country:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.country ? merchantSummary.country : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Zip:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.zip ? merchantSummary.zip : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        MID:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.mid ? merchantSummary.mid : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Primary Merchant Contact Name:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.primaryMerchantContactName ? merchantSummary.primaryMerchantContactName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                        Products Services Description:
                      </div>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <div className='text-700 pl-3 fw-bold'>
                        {merchantSummary && merchantSummary.productsServicesDescription ? merchantSummary.productsServicesDescription : '--'}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore, riskManagementlistStore, editMerchantStore, postAsigneeStore, updateAssignStore } = state

  return {
    dashboardDetails: dashboardStore && dashboardStore.dashboardDetails ? dashboardStore.dashboardDetails : {},
    riskmgmtlistdetails: riskManagementlistStore && riskManagementlistStore.riskmgmtlists ? riskManagementlistStore.riskmgmtlists : null,
    getRiskSummarys: state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    merchantIddetails: editMerchantStore && editMerchantStore.merchantIddetail ? editMerchantStore.merchantIddetail : {},
    getAssignee: postAsigneeStore && postAsigneeStore.getAssignee ? postAsigneeStore.getAssignee : {},
    updateAssign: updateAssignStore && updateAssignStore.updateAssign ? updateAssignStore.updateAssign : {}
  }
}

const mapDispatchToProps = (dispatch) => ({
  merchantIdDetail: (id) => dispatch(merchantIdDetailsActions.getmerchantIdDetailsData(id)),
  getAsigneeslistDispatch: (params) => dispatch(GetAsigneeActions.GetAsignee(params)),
  updateAssignDispatch: (id, params) => dispatch(AsiggnActions.Asiggn(id, params)),
  clearAsiggnDispatch: () => dispatch(AsiggnActions.clearAsiggn())
})

export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetails)