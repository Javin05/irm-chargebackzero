import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  SWEET_ALERT_MSG,
} from "../../utils/constants"
import {
  WrmListStatusChangeActions,
  WrmListUpdateQueueActions
} from "../../store/actions"
import { useLocation } from "react-router-dom"
import {
  confirmationAlert,
  successAlert,
  warningAlert
} from "../../utils/alerts"
import clsx from "clsx"
import Modal from 'react-bootstrap/Modal'

function Status(props) {
  const {
    loading,
    WrmStatusDispatch,
    WrmStatusChangeResponce,
    clearWrmStatusDispatch,
    setActionIds,
    actionIds,
    WrmListUpdateQueue,
    clearWrmListUpdateQueue,
    WrmListUpdateQueueResponse,
    setChecked,
    getWrmRiskManagementlistDispatch,
    getWrmStatusDispatch,
    limit
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const [status, setStatus] = useState('APPROVED')
  const [approveModal, setApproveModal] = useState(false)
  const [rejectModal, setRejectModal] = useState(false)
  const [updateQueue, setUpdateQueue] = useState(false)
  const [queueData, setQueueData] = useState({})
  const [errors, setErrors] = useState({
    reason: "",
    updateQueue:""
  })

  const [rejectFormData, setRejectFormData] = useState({
    status: "REJECTED",
    reason: "",
    rejectType: "",
    rejectMoreValue: "",
  })
  const [approveFormData, setApproveFormData] = useState({
    status: "APPROVED",
    reason: "",
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setQueueData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleTab = (e) => {
    setStatus(e.target.id)
  }
  
  const handleQueue = () => {
    const errors = {}
    if (_.isEmpty(queueData.updateQueue)) {
      errors.updateQueue = USER_ERROR.UPDATE_QUEUE
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        ids:actionIds,
        queueName: queueData && queueData.updateQueue,
      }
      WrmListUpdateQueue(params)
    }
  }

  const approveSubmit = () => {
    // const errors = {}
    // if (_.isEmpty(approveFormData.reason)) {
    //   errors.reason = USER_ERROR.REASON
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
    //   const params = {
    //     ids: actionIds,
    //     comments: approveFormData.reason,
    //     status: approveFormData.status
    //   }
    //   WrmStatusDispatch(params)
    // }
    const params = {
      ids: actionIds,
      status: status,
      comments: approveFormData.reason
    }
    WrmStatusDispatch(params)
  }

  const onConfirmReject = () => {
    const params = {
      ids: actionIds,
      comments: rejectFormData.reason,
      status: rejectFormData.status
    }
    WrmStatusDispatch(params)
  }

  const rejectSubmit = () => {
    // const errors = {}
    // if (_.isEmpty(rejectFormData.reason)) {
    //   errors.reason = USER_ERROR.REASON
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
    //   confirmationAlert(
    //     SWEET_ALERT_MSG.CONFIRMATION_TEXT,
    //     SWEET_ALERT_MSG.REJECT,
    //     "warning",
    //     "Yes",
    //     "No",
    //     () => {
    //       onConfirmReject()
    //     },
    //     () => { {} }
    //   )
    // }

    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.REJECT,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmReject()
      },
      () => { {} }
    )
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  useEffect(() => {
    if (WrmStatusChangeResponce && WrmStatusChangeResponce.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        WrmStatusChangeResponce && WrmStatusChangeResponce.message,
        'success'
      )
      setRejectModal(false)
      setApproveModal(false)
      clearWrmStatusDispatch()
      setActionIds([])
      setChecked(false)
      const params = {
        limit: limit,
        page: 1,
      }
      getWrmRiskManagementlistDispatch(params)
      getWrmStatusDispatch(params)
    } else if (WrmStatusChangeResponce && WrmStatusChangeResponce.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        WrmStatusChangeResponce && WrmStatusChangeResponce.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearWrmStatusDispatch()
    }
  }, [WrmStatusChangeResponce])

  useEffect(() => {
    if (WrmListUpdateQueueResponse && WrmListUpdateQueueResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        WrmListUpdateQueueResponse && WrmListUpdateQueueResponse.message,
        'success'
      )
      setUpdateQueue(false)
      clearWrmListUpdateQueue()
      setActionIds([])
      setChecked(false)
      const params = {
        limit: limit,
        page: 1,
      }
      getWrmRiskManagementlistDispatch(params)
      getWrmStatusDispatch(params)
    } else if (WrmListUpdateQueueResponse && WrmListUpdateQueueResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        WrmListUpdateQueueResponse && WrmListUpdateQueueResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearWrmListUpdateQueue()
    }
  }, [WrmListUpdateQueueResponse])

  const clearPopup = () => {
    setRejectModal(false)
    setApproveModal(false)
    setUpdateQueue(false)
    setRejectFormData({
      reason: "",
      rejectType: "",
      rejectMoreValue: ""
    })
    setApproveFormData({
      comment: '',
      reason: "",
    })
    setErrors({})
  }

  return (
    <>
      <Modal
        show={approveModal}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are you sure want to approve this WRM Record?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <div className="d-flex mb-5 align-items-center justify-content-center mx-5">
                    <div class="form-check w-25">
                      <input class="form-check-input" type="radio" name="flexRadioDefault" id="APPROVED" onChange={handleTab} defaultChecked />
                        <label class="form-check-label" for="APPROVED">
                          Approve
                        </label>
                    </div>
                    <div class="form-check w-25">
                      <input class="form-check-input" type="radio" name="flexRadioDefault" id="REJECTED" onChange={handleTab}/>
                        <label class="form-check-label" for="REJECTED">
                          Reject
                        </label>
                    </div>
                    <div class="form-check w-25">
                      <input class="form-check-input" type="radio" name="flexRadioDefault" id="HOLD" onChange={handleTab}/>
                        <label class="form-check-label" for="ONHOLD">
                          Hold
                        </label>
                    </div>
                    <div class="form-check w-25">
                      <input class="form-check-input" type="radio" name="flexRadioDefault" id="FRAUD" onChange={handleTab} />
                        <label class="form-check-label" for="flexRadioDefault2">
                          Fraud
                        </label>
                    </div>
                  </div>
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Comments
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 ">
                    <textarea
                      name="reason"
                      type="text"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            approveFormData.reason && errors.reason,
                        },
                        {
                          "is-valid":
                            approveFormData.reason && !errors.reason,
                        }
                      )}
                      placeholder="Message"
                      onChange={(e) => approveChange(e)}
                      autoComplete="off"
                      value={approveFormData.reason || ""}
                    />
                    {errors.reason && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.reason}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                      type="submit"
                      className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                      onClick={(event) => {
                        approveSubmit(event)
                      }}
                      disabled={loading}
                    >
                      {!loading && (
                        "Submit"
                      )}
                      {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                      )}
                    </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={rejectModal}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Status
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Reason For Reject :
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                    <textarea
                      name="reason"
                      type="text"
                      // className='form-control'
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid": rejectFormData.reason && errors.reason,
                        },
                        {
                          "is-valid": rejectFormData.reason && !errors.reason,
                        }
                      )}
                      placeholder="Message"
                      onChange={(e) => rejectChange(e)}
                      autoComplete="off"
                      value={rejectFormData.reason || ""}
                    />
                    {errors.reason && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.reason}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                      type="submit"
                      className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                      onClick={(event) => {
                        rejectSubmit(event)
                      }}
                      disabled={loading}
                    >
                      {!loading && (
                        "Submit"
                      )}
                      {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                      )}
                    </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={updateQueue}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Are You Sure Want to Update This Users Risk Management Report ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card-header">
            <div className="card-body">
              <div className="form-group row mb-4">
                <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                  <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                    Update Queue
                  </label>
                  <div className="col-lg-11 col-md-11 col-sm-11 ">
                    <select
                      name='updateQueue'
                      className='form-select form-select-solid mb-4'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                    >
                      <option value=''>Select...</option>
                      <option value='SUSPECT_WEB_RISK_MONITORING_Q'>SUSPECT_WEB_RISK_MONITORING_Q</option>
                      <option value='WEB_RISK_MONITORING_Q'>WEB_RISK_MONITORING_Q</option>
                    </select>
                    {errors.updateQueue && (
                      <div className="fv-plugins-message-container text-danger">
                        <span role="alert text-danger">
                          {errors.updateQueue}
                        </span>
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row mb-4">
                  <div className="col-lg-6" />
                  <div className="col-lg-6">
                    <div className="col-lg-11">
                      <button
                        type="submit"
                        className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                        onClick={(event) => {
                          handleQueue(event)
                        }}
                        disabled={loading}
                      >
                        {!loading && (
                          "Submit"
                        )}
                        {loading && (
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        )}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className="w-auto d-flex justify-content-between text-nowrap">
        <a
          className="nav-link btn btn-sm fw-bold btn-responsive px-2 font-7vw btn-light-success"
          onClick={() => {
            setUpdateQueue(true)
          }}
          style={{ width: '48%' }}
        >
          Update Queues
        </a>
        <a
          className="nav-link btn btn-sm fw-bold btn-responsive px-2 font-7vw btn-light-success"
          onClick={() => {
            // approveSubmit()
            setApproveModal(true)
          }}
          style={{ width: '48%' }}
        >
          Change Status
        </a>
        {/* <li className="nav-item">
              <a
                className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                onClick={() => {
                //  rejectSubmit()
                  setRejectModal(true)
                }}
              >
                Reject
              </a>
            </li> */}
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    WrmListStatusARStore,
    WrmListUpdateQueueStore
  } = state
  return {
    WrmStatusChangeResponce: WrmListStatusARStore && WrmListStatusARStore.WrmListStatusChangeResponce ? WrmListStatusARStore.WrmListStatusChangeResponce : {},
    loading: WrmListStatusARStore && WrmListStatusARStore.loading ? WrmListStatusARStore.loading : false,
    WrmListUpdateQueueResponse: WrmListUpdateQueueStore && WrmListUpdateQueueStore.WrmListUpdateQueueResponce ? WrmListUpdateQueueStore.WrmListUpdateQueueResponce :{},
  }
}

const mapDispatchToProps = (dispatch) => ({
  WrmStatusDispatch: (params) => dispatch(WrmListStatusChangeActions.WrmListStatusChange(params)),
  clearWrmStatusDispatch: () => dispatch(WrmListStatusChangeActions.clearWrmListStatusChange()),
  WrmListUpdateQueue: (params) => dispatch(WrmListUpdateQueueActions.WrmListUpdateQueue(params)),
  clearWrmListUpdateQueue: () => dispatch(WrmListUpdateQueueActions.clearWrmListUpdateQueue()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Status)