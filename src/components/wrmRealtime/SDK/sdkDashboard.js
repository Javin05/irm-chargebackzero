import _ from 'lodash'
import RiskSummary from '../../riskSummary/riskSummary'
import clsx from 'clsx'

function SdkDashboard() {

  return (
    <>
      <div className="container-fixed">
        <div
          id='kt_page_title'
          data-kt-swapper='true'
          data-kt-swapper-mode='prepend'
          data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
          className={clsx('page-title d-flex bg-skyBlue py-8')}
        >
          <h1 className='d-flex align-items-center text-dark fw-bolder my-1 fs-3 ms-10'>
            {/* Dashboard */}
          </h1>
        </div>
        <div className="card m-4">
          <RiskSummary />
        </div>
      </div>
    </>
  )
}


export default SdkDashboard
