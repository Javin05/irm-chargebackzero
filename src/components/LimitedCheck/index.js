import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Button, TextField, Typography } from '@material-ui/core';

const useStyles = makeStyles({
    container: {
        padding: '20px',
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '20px',
    },
    createButton: {
        backgroundColor: '#4CAF50',
        color: 'white',
    },
    table: {
        minWidth: 650,
    },
    editButton: {
        backgroundColor: '#2196F3',
        color: 'white',
    },
    deleteButton: {
        backgroundColor: '#f44336',
        color: 'white',
    },
    inputField: {
        width: '100%',
    },
});

const ListPage = () => {
    const classes = useStyles();
    const history = useHistory();

    const [items, setItems] = useState([
        { id: 1, name: 'Item 1' },
        { id: 2, name: 'Item 2' },
    ]);

    const handleCreate = () => {
        history.push('/create');
    };

    const handleEdit = (id) => {
        history.push(`/edit/${id}`);
    };

    const handleDelete = (id) => {
        const confirmDelete = window.confirm('Are you sure you want to delete this item?');
        if (confirmDelete) {
            setItems(items.filter(item => item.id !== id));
        }
    };

    const handleInputChange = (id, value) => {
        setItems(items.map(item => item.id === id ? { ...item, name: value } : item));
    };

    return (
        <div className={classes.container}>
            <div className={classes.header}>
                <Typography variant="h4">List Page</Typography>
                <Button 
                    variant="contained" 
                    className={classes.createButton} 
                    onClick={handleCreate}
                >
                    Create
                </Button>
            </div>

            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">S. No</TableCell>
                            <TableCell align="center">Input</TableCell>
                            <TableCell align="center">Edit</TableCell>
                            <TableCell align="center">Delete</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {items.map((item, index) => (
                            <TableRow key={item.id}>
                                <TableCell align="center">{index + 1}</TableCell>
                                <TableCell align="center">
                                    {item.name}
                                </TableCell>
                                <TableCell align="center">
                                    <Button 
                                        variant="contained" 
                                        className={classes.editButton} 
                                        onClick={() => handleEdit(item.id)}
                                    >
                                        Edit
                                    </Button>
                                </TableCell>
                                <TableCell align="center">
                                    <Button 
                                        variant="contained" 
                                        className={classes.deleteButton} 
                                        onClick={() => handleDelete(item.id)}
                                    >
                                        Delete
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default ListPage;