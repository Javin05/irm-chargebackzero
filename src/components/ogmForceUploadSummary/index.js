import React, { useEffect, useState, Fragment, useCallback } from 'react'
import _ from 'lodash'
import { OgmForceUploadSummaryAction } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper'
import SearchList from './searchList'

function OgmForceUploadSummary(props) {
  const {
    OgmForceUploadSummaryDispatch,
    OgmForceUploadSummaryData,
    loading
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [sortingParams, setSortingParams] = useState()
  const [sorting, setSorting] = useState({
    tag: false,
    createdDate: false,
    rejected: false,
    total: false,
    manualreview: false
  })
  const [formData, setFormData] = useState([])
  const [searchData, setSearchData] = useState({})
  const [selectShow, setSelectShow] = useState(false)

  useEffect(() => {
    const params = {
      limit: limit,
      page: activePageNumber,
    }
    OgmForceUploadSummaryDispatch(params)
  }, [])

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      ...sortingParams,
      limit: limit,
      page: pageNumber,
    }
    setActivePageNumber(pageNumber)
    OgmForceUploadSummaryDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC',
      }
      setSortingParams(params)
      OgmForceUploadSummaryDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC',
      }
      setSortingParams(params)
      OgmForceUploadSummaryDispatch(params)
    }
  }

  const totalPages =
    OgmForceUploadSummaryData && OgmForceUploadSummaryData.data && OgmForceUploadSummaryData.data.count
      ? Math.ceil(parseInt(OgmForceUploadSummaryData && OgmForceUploadSummaryData.data && OgmForceUploadSummaryData.data.count) / limit)
      : 1 

  return (
    <>
      <div className={`card md-10`} >
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1 ms-4'>
                {OgmForceUploadSummaryData && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3 ms-1'>
                      {OgmForceUploadSummaryData && OgmForceUploadSummaryData.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='col-lg-6'>
              <div className='d-flex justify-content-end mt-4 me-4'>
                <SearchList setSearchData={setSearchData} OgmForceUploadSummaryData={OgmForceUploadSummaryData} setSelectShow = {setSelectShow} setFormDatas = {setFormData}/>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                  <th className="min-w-150px">
                    <div className="d-flex">
                      <span>Uploaded Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("updatedAt")}
                        >
                          <i
                            className={`bi ${sorting.updatedAt
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-150px">
                    <div className="d-flex">
                      <span>FU Batch Id</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogmForceUploadId")}
                        >
                          <i
                            className={`bi ${sorting.ogmForceUploadId
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Count</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("totalCount")}
                        >
                          <i
                            className={`bi ${sorting.totalCount
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Completed</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("completedCount")}
                        >
                          <i
                            className={`bi ${sorting.completedCount
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Pending</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("notCompletedCount")}
                        >
                          <i
                            className={`bi ${sorting.notCompletedCount
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Invalid Urls</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("inValideUrlsCounts")}
                        >
                          <i
                            className={`bi ${sorting.inValideUrlsCounts
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-7'>
                {
                  !loading
                    ? (
                      Array.isArray(OgmForceUploadSummaryData &&
                        OgmForceUploadSummaryData.data)
                        ? (
                          OgmForceUploadSummaryData && OgmForceUploadSummaryData.data && OgmForceUploadSummaryData.data.map((item, i) => {
                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  <td>
                                    {item.updatedAt ? item.updatedAt : "--"}
                                  </td>
                                  <td>
                                    {item.ogmForceUploadId ? item.ogmForceUploadId : "--"}
                                  </td>
                                  <td>
                                    {item.totalCount ? item.totalCount : "0"}
                                  </td>
                                  <td>
                                    {item && item.completedCount ? item.completedCount : '0'}
                                  </td>
                                  <td>
                                    {item && item.notCompletedCount ? item.notCompletedCount : '0'}
                                  </td>
                                  <td>
                                    {item && item.inValideUrlsCounts ? item.inValideUrlsCounts : '0'}
                                  </td>
                                </tr> 
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OgmForceUploadSummaryStore } = state
  return {
    loading: OgmForceUploadSummaryStore && OgmForceUploadSummaryStore.loading ? OgmForceUploadSummaryStore.loading : false,
    OgmForceUploadSummaryData: OgmForceUploadSummaryStore && OgmForceUploadSummaryStore.OgmForceUploadSummaryData ? OgmForceUploadSummaryStore.OgmForceUploadSummaryData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  OgmForceUploadSummaryDispatch: (params) => dispatch(OgmForceUploadSummaryAction.getOgmForceUpload(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(OgmForceUploadSummary)
