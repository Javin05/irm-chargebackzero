import React, { useState } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { OgmForceUploadSummaryAction } from '../../store/actions'
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'

function SearchList(props) {
  const { OgmForceUploadSummaryDispatch, loading, setSearchData } = props
  const [error, setError] = useState({});
  const [show, setShow] = useState(false)
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [formData, setFormData] = useState({
    ogmForceUploadId: '',
  })

  const handleSearch = (e) => {
    const params = {
      ogmForceUploadId: formData.ogmForceUploadId,
      limit: 25,
      page: 1
    }
    OgmForceUploadSummaryDispatch(params)
    setShow(false)
    setSearchData(params)
  }

  const handleChanges = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...error, [e.target.name]: '' })
  }

  const handleReset = () => {
    setFormData({
      ogmForceUploadId: '',
    }
    )
    const params = {
      limit: 25,
      page: 1
    }
    OgmForceUploadSummaryDispatch(params)
    setShow(false) 
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-md btn-light-primary btn-responsive me-3 pull-right w-100px'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => { 
          handleReset()
          }}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => { 
          handleReset()
            }}
            >
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className='col-lg-12 mb-3'>
                    <div className='row justify-content-between align-items-center'>
                      <label className="col-lg-3 font-size-xs font-weight-bold mb-3 form-label">
                        Ogm Force Upload Id :
                      </label>
                      <div className='col-lg-9'>
                        <input
                          placeholder='ogmForceUploadId'
                          className={clsx(
                            'form-control form-control-lg form-control-solid',
                            { 'is-invalid': formData.ogmForceUploadId && error.ogmForceUploadId },
                            {
                              'is-valid': formData.ogmForceUploadId && !error.ogmForceUploadId
                            }
                          )}
                          onChange={(e) => handleChanges(e)}
                          type='text'
                          name='ogmForceUploadId'
                          autoComplete='off'
                          value={formData.ogmForceUploadId || ''}
                        />
                        {errors && errors.ogmForceUploadId && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {errors.ogmForceUploadId}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className='form-group row mb-4 mt-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => handleSearch()}
                          disabled={loading}
                        >
                          {loading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Search'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => handleReset()}
                        >
                          Reset
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OgmForceUploadSummaryStore } = state
  return {
    loading: OgmForceUploadSummaryStore && OgmForceUploadSummaryStore.loading ? OgmForceUploadSummaryStore.loading : false,
  }
}

const mapDispatchToProps = dispatch => ({
  OgmForceUploadSummaryDispatch: (params) => dispatch(OgmForceUploadSummaryAction.getOgmForceUpload(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)