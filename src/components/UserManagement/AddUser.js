import React, { useState, useEffect } from 'react'
import { Modal } from '../../theme/layout/components/modal'
import { KTSVG } from '../../theme/helpers'
import color from '../../utils/colors'
import _ from 'lodash'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import { connect } from 'react-redux'
import { STATUS_RESPONSE, REGEX, FILE_FORMAT_IMAGES, DROPZONE_MESSAGES, API_URL } from '../../utils/constants'
import {
  warningAlert,
  confirmAlert
} from '../../utils/alerts'
import {
  userRolesActions,
  addUserActions,
  userActions,
  addMerchantUploadActions,
  userTypesActions
} from '../../store/actions'
import { userValidation } from './validation'
import userPlaceholder from '../../assets/userPlaceholder.png'

const AddUser = (props) => {
  const {
    setShow,
    show,
    userRoleData,
    getUserroleDispatch,
    loadingAUD,
    statusAUD,
    messageAUD,
    addUserDispatch,
    clearaddUserDispatch,
    getUserDispatch,
    addMerchantUploadDispatch,
    clearaddMerchantUploadDispatch,
    dataAMUpload,
    loadingAMUpload,
    statusAMUpload,
    getUserTypeDispatch,
    getDataUserType,
    loadingGetUR,
    siteConfigs
  } = props

  const [errors, setErrors] = useState({})
  const [editMode] = useState(false)
  const [selectedUserTypeOption, setSelectedUserTypeOption] = useState('')
  const [userTypeOption, setUserTypeOption] = useState()
  const [selectedUserRoleOption, setSelectedUserRoleOption] = useState('')
  const [userRoleOption, setUserRoleOption] = useState()
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    userTypeId: '',
    roleId: '',
    password: '',
    cPassword: '',
    image: '',
    tag: "IRM"
  })

  const resetState = () => {
    setFormData({
      firstName: '',
      lastName: '',
      email: '',
      mobile: '',
      roleId: ''
    })
    setSelectedUserRoleOption()
    setErrors(null)
  }
  useEffect(() => {
    const params ={
      tag: "IRM"
    }
    getUserTypeDispatch(params)
  }, [])

  const handleChange = (e) => {
    e.persist()
    setFormData(values => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = userValidation(formData, setErrors, siteConfigs)
    if (_.isEmpty(errorMsg)) {
      addUserDispatch(formData)
    }
  }

  const onConfirm = () => {
    const params ={
      tag: "IRM"
    }
    getUserDispatch(params)
    setShow(false);
    resetState()
  }

  useEffect(() => {
    if (statusAUD === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAUD,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearaddUserDispatch()
      const params ={
        tag: "IRM"
      }
      getUserDispatch(params)
    } else if (statusAUD === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAUD,
        '',
        'Ok'
      )
      clearaddUserDispatch()
    }
  }, [statusAUD])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? _.startCase(item[name]) : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(userRoleData, 'role')
    setUserRoleOption(data)
  }, [userRoleData])

  const handleChangeUserRole = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserRoleOption(selectedOption)
      setFormData((values) => ({
        ...values,
        roleId: selectedOption.value
      }))
      setErrors((values) => ({ ...values, roleId: '' }))
    } else {
      setSelectedUserRoleOption()
      setFormData((values) => ({ ...values, roleId: '' }))
    }
  }

  useEffect(() => {
    const data = getDefaultOptions(getDataUserType, 'userType')
    setUserTypeOption(data)
  }, [getDataUserType])

  const handleChangeUserType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserTypeOption(selectedOption)
      setFormData((values) => ({
        ...values,
        userTypeId: selectedOption.value
      }))
      const roleParams = {
        skipPagination: true,
        userTypeId: selectedOption.value,
        tag: "IRM"
      }
      getUserroleDispatch(roleParams)
      setErrors((values) => ({ ...values, userTypeId: '' }))
    } else {
      setSelectedUserTypeOption()
      setFormData((values) => ({ ...values, userTypeId: '' }))
    }
  }

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_FORMAT_IMAGES, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData()
        data.append('type', "user")
        data.append('file_to_upload', files)
        addMerchantUploadDispatch(data)
        setErrors((values) => ({ ...values, image: "" }));
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID });
    }
  };

  useEffect(() => {
    if (statusAMUpload === STATUS_RESPONSE.SUCCESS_MSG) {
      if (dataAMUpload && dataAMUpload.path) {
        setFormData((values) => ({ ...values, image: dataAMUpload.path }))
      }
      clearaddMerchantUploadDispatch()
    }
  }, [statusAMUpload])

  useEffect(() => {
    return () => {
      setSelectedUserRoleOption()
      clearaddUserDispatch()
    }
  }, [])

  return (
    <>
      <Modal
        showModal={show}
        modalWidth={650}
      >
        <div className="h-auto" id="userModal">
          <div>
            <div className='modal-dialog modal-dialog-centered mw-800px'>
              <div className='modal-content'>
                <div className='modal-header'>
                  <h2 className='me-8 ms-4'>{editMode ? 'Update' : 'Add'} Users</h2>
                  <button
                    type='button'
                    className='btn btn-lg btn-icon btn-active-light-primary close'
                    data-dismiss='modal'
                    onClick={() => {
                      setShow(false)
                      setSelectedUserRoleOption()
                      resetState()
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                    {/* eslint-disable */}
                  </button>
                </div>
                <div className='container-fixed'>
                  <div className='card-header bg-lightBlue'>
                    <div className='card-body'>
                      <div className='scroll h-400px px-5'>
                        <div className="fv-row mb-7">
                          <label className="d-block fw-bold fs-6 mb-5">Profile Image</label>
                          <div className="image-input image-input-outline image-input-empty" data-kt-image-input="true">
                            <div className="image-input-wrapper w-125px h-125px">
                              <img
                                src={`${API_URL}/uploads/${formData.image}`}
                                alt="your image" style={{ width: 120, height: 120 }}
                                onError={(e) => { e.target.src = userPlaceholder }}
                              />
                            </div>
                            <label className="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="" data-bs-original-title="Change avatar" >
                              <i className="bi bi-pencil-fill fs-7"></i>
                              <input
                                type="file"
                                name="image"
                                id="file_to_upload"
                                accept=".png, .jpg, .jpeg"
                                multiple={true}
                                onChange={handleFileChange}
                              />
                              <input type="hidden" name="avatar_remove" />
                            </label>
                          </div>
                          {loadingAMUpload ? (
                            <div className="d-flex justify-content-start align-items-start py-3">
                              <div className="d-flex justify-content-start">
                                <span className="spinner-grow spinner-grow-sm" role="status">
                                  <span className="visually-hidden" />
                                </span>
                                <span className="spinner-grow spinner-grow-sm mx-1" role="status">
                                  <span className="visually-hidden" />
                                </span>
                              </div>
                              <div className="mx-2">Uploading</div>
                            </div>
                          ) :
                            null
                          }
                          <div className="form-text">Allowed file types: png, jpg, jpeg.</div>
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">First Name</label>
                          <input
                            name='firstName'
                            type='text'
                            className='form-control'
                            placeholder='First Name'
                            value={formData.firstName || ''}
                            maxLength={10}
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            onKeyPress={(e) => {
                              if (!REGEX.TEXT.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors && errors.firstName && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.firstName}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Last Name</label>
                          <input
                            name='lastName'
                            type='text'
                            className='form-control'
                            placeholder='Last Name'
                            value={formData.lastName || ''}
                            maxLength={10}
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            onKeyPress={(e) => {
                              if (!REGEX.TEXT.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors && errors.lastName && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.lastName}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Email</label>
                          <input
                            name='email'
                            type='email'
                            className='form-control'
                            placeholder='Email'
                            value={formData.email || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                          />
                          {errors && errors.email && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.email}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Phone Number</label>
                          <input
                            name='mobile'
                            type='text'
                            className='form-control'
                            placeholder='Phone Number'
                            value={formData.mobile || ''}
                            onChange={(e) => handleChange(e)}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            autoComplete='off'
                            maxLength={10}
                            onKeyPress={(e) => {
                              if (!REGEX.NUMERIC.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {errors && errors.mobile && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.mobile}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">User Type</label>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='userTypeId'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeUserType}
                            options={userTypeOption}
                            value={selectedUserTypeOption}
                          />
                          {errors && errors.userTypeId && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.userTypeId}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Role</label>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='roleId'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeUserRole}
                            options={userRoleOption}
                            value={selectedUserRoleOption}
                            isLoading={loadingGetUR}
                          />
                          {errors && errors.roleId && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.roleId}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Password</label>
                          <input
                            name='password'
                            type='password'
                            className='form-control form-control-solid'
                            id='basic-url'
                            placeholder='Password'
                            aria-describedby='basic-addon3'
                            onChange={(e) => handleChange(e)}
                            value={formData.password || ''}
                            autoComplete='off'
                          />
                          {errors && errors.password && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.password}
                            </div>
                          )}
                        </div>
                        <div className="fv-row mb-7 fv-plugins-icon-container">
                          <label className="required fw-bold fs-6 mb-2">Confirm Password</label>
                          <input
                            name='cPassword'
                            type='password'
                            className='form-control'
                            id='basic-url'
                            placeholder='Confirm Password'
                            aria-describedby='basic-addon3'
                            onChange={(e) => handleChange(e)}
                            value={formData.cPassword || ''}
                            autoComplete='off'
                          />
                          {errors && errors.cPassword && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red}'}</style>
                              {errors.cPassword}
                            </div>
                          )}
                        </div>
                        <div className='form-group row mt-4'>
                          <div className='col-lg-5' />
                          <div className='col-lg-7'>
                            <div className='col-lg-12'>
                              <button
                                className='btn btn-blue mt-7 fa-pull-right'
                                onClick={(event) => {
                                  handleSubmit(event)
                                }}
                              >
                                {loadingAUD
                                  ? (
                                    <span
                                      className='spinner-border spinner-border-sm mx-3'
                                      role='status'
                                      aria-hidden='true'
                                    />
                                  )
                                  : (
                                    'Submit'
                                  )}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    userroleStore,
    addUserStore,
    addMerchantUploadStore,
    usertypeStore,
    siteConfigStore
  } = state
  return {
    loadingGetUR: state && state.userroleStore && state.userroleStore.loadingGetUR,
    userRoleData:
      userroleStore && userroleStore.userRoleData
        ? userroleStore.userRoleData
        : {},
    messageAUD:
      addUserStore && addUserStore.messageAUD
        ? addUserStore.messageAUD
        : 0,
    statusAUD:
      addUserStore && addUserStore.statusAUD
        ? addUserStore.statusAUD
        : {},
    loadingAUD:
      addUserStore && addUserStore.loadingAUD
        ? addUserStore.loadingAUD
        : false,
    loadingAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.loadingAMUpload
        ? addMerchantUploadStore.loadingAMUpload
        : false,
    dataAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.dataAMUpload
        ? addMerchantUploadStore.dataAMUpload
        : {},
    statusAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.statusAMUpload
        ? addMerchantUploadStore.statusAMUpload
        : '',
    messageAMUpload:
      addMerchantUploadStore && addMerchantUploadStore.messageAMUpload
        ? addMerchantUploadStore.messageAMUpload
        : '',
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
    siteConfigs:
      siteConfigStore &&
      siteConfigStore.siteConfigs
      ? siteConfigStore.siteConfigs
      : {}
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserroleDispatch: (params) => dispatch(userRolesActions.getUserrole(params)),
  addUserDispatch: (data) => dispatch(addUserActions.addUser(data)),
  clearaddUserDispatch: () => dispatch(addUserActions.clearaddUser()),
  getUserDispatch: (params) => dispatch(userActions.getUser(params)),
  addMerchantUploadDispatch: (data) =>
    dispatch(addMerchantUploadActions.addMerchantUpload(data)),
  clearaddMerchantUploadDispatch: () =>
    dispatch(addMerchantUploadActions.clearaddMerchantUpload()),
  getUserTypeDispatch: (params) =>
    dispatch(userTypesActions.getUserType(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddUser)