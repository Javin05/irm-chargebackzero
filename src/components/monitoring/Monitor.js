import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  MonitorActions,
  clientCredFilterActions,
  MonitorCountsActions,
  MonitorViewsActions,
  deleteMonitorReportActions,
  viewBackendDashboardActions,
  UpdateBackendDashboardActions
} from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper'
import { RISKSTATUS, MONITOR_STATUS, DATE, NEWDATA } from '../../utils/constants'
import moment from "moment"
import AddMonitor from './Add'
import Actions from "./Actions"
import Modal from 'react-bootstrap/Modal'
import ReactJson from 'react-json-view'
import { warningAlert, confirmationAlert, confirmAlert } from "../../utils/alerts"
import {
  STATUS_RESPONSE, REGEX
} from '../../utils/constants'
import { setBackendDashData } from "./formData"
import { backendDashboardEditValidation } from "./validation"
import { DateSelector } from '../../theme/layout/components/DateSelector'
import Status from './ARH'
import OgmForceUpload from './ogmForceUpload'
import FindRole from '../wrmManagement/Role'

function MonitorList(props) {
  const {
    getMonitorlistDispatch,
    className,
    MonitorData,
    loading,
    statusEBD,
    messageEBD,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    getMonitorCountsDispatch,
    MonitorCountsData,
    getMonitorViewsDispatch,
    MonitorViewsData,
    MonitorViewloading,
    clearMonitorViewsDispatch,
    deletemonitorReportDispatch,
    DeletemonitorReportData,
    clearDeletemonitorReportDispatch,
    backendDashboardViewDispatch,
    MonitorBackendDashboardData,
    MonitorBackendDashboardLoading,
    clearBackendDashboardViewDispatch,
    updateBackendDashboardDispatch,
    UpdateBackendDashboardData,
    UpdateBackendDashboardLoadingEBD,
    clearUpdateBackendDashboardDispatch
  } = props

  const didMount = React.useRef(false)
  const location = useLocation()
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [searchData, setSearchData] = useState({})
  const [show, setShow] = useState(false)
  const [updateShow, setUpdateShow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentRoute = url && url[1]
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const [actionIds, setActionIds] = useState([])
  const [checked, setChecked] = useState(false)
  const [edit, setEdit] = useState(false)
  const [view, setView] = useState(false)
  const [forceUploadShow, setForceUploadShow] = useState(false)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })
  const [formData, setFormData] = useState({
    backendDelay: '',
    legalName: '',
    backendendNextExecutionDate: '',
    lastExecutionDate: '',
    website: '',
    batchId: '',
  })

  const resetState = () => {
    setFormData({
      backendDelay: '',
      legalName: '',
      backendendNextExecutionDate: '',
      lastExecutionDate: '',
      website: '',
      batchId: '',
    })
    setErrors({})
  }

  const [editId, setEditId] = useState('')
  const [errors, setErrors] = useState({})

  useEffect(() => {
    if (location.state) {
      const params = location.state
      params.limit = limit;
      params.page = activePageNumber;
      getMonitorlistDispatch(params)
    } else {
      const params = {
        limit: limit,
        page: activePageNumber,
      }
      getMonitorlistDispatch(params)
    }
    getMonitorCountsDispatch()
  }, [location.state])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
        clientId: currentFilterParams ? currentFilterParams : ''
      }
      const pickByParams = _.pickBy(params)
      getMonitorlistDispatch(pickByParams)
      getMonitorCountsDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
      setActionIds([])
    }
  }, [setFilterFunction, setCredFilterParams])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const data = location.state
    const credBasedParams = {
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
    }
    const params = {
      ...credBasedParams,
      ...searchData,
      limit: value,
      page: activePageNumber,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params)
    getMonitorlistDispatch(pickByParams)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const data = location.state
    const credBasedParams = {
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
    }
    const params = {
      ...credBasedParams,
      ...searchData,
      limit: limit,
      page: pageNumber,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params)
    setActivePageNumber(pageNumber)
    getMonitorlistDispatch(pickByParams)
  }

  const handleCheckboxChange = (id) => {
    setChecked(!checked);
    if (actionIds.includes(id)) {
      setActionIds(actionIds.filter((item) => item !== id))
    } else {
      setActionIds([...actionIds, id])
    }
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getMonitorlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getMonitorlistDispatch(params)
    }
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const viewChange = (id) => {
    const params = {
      ogmId: id
    }
    getMonitorViewsDispatch(params)
    setShow(true)
  }

  const viewBackend = (id) => {
    backendDashboardViewDispatch(id)
    setEditId(id)
    setUpdateShow(true)
  }

  const totalPages =
    MonitorData && MonitorData.data && MonitorData.data.count
      ? Math.ceil(parseInt(MonitorData && MonitorData.data && MonitorData.data.count) / limit)
      : 1

  const viewData = JSON.stringify(MonitorViewsData && MonitorViewsData.data, null, '\t')

  const onDeleteItem = (id) => {
    const params = {
      ogmId: id
    }
    confirmationAlert(
      "Are you sure want to delete this URL,",
      "If you delete the record, can't revert back?",
      'warning',
      'Yes',
      'No',
      () => {
        deletemonitorReportDispatch(params)
      },
      () => { }
    )
  }

  useEffect(() => {
    if (DeletemonitorReportData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        DeletemonitorReportData.message,
        'success',
        'ok',
        // () => { { } },
        // () => { { } }
      )
      getMonitorlistDispatch()
      clearDeletemonitorReportDispatch()
      setView(false)
    } else if (DeletemonitorReportData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        DeletemonitorReportData.message,
        '',
        'Ok'
      )
    }
    clearDeletemonitorReportDispatch()
  }, [DeletemonitorReportData])

  const handleInputChange = (e) => {
    e.preventDefault()
    setFormData((values) => ({
      ...values,
      [e.target.name]: e.target.value
    }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = backendDashboardEditValidation(formData, setErrors)
    const updateValue = {
      website: formData.website,
      batchId: formData.batchId,
      delay: formData.backendDelay,
      legal_name: formData.legalName,
      next_execution_date: moment(formData.backendendNextExecutionDate).format("YYYY-MM-DD"),
      last_execution_date: moment(formData.lastExecutionDate).format("YYYY-MM-DD")
    }
    if (_.isEmpty(errorMsg)) {
      updateBackendDashboardDispatch(editId, updateValue)
    }
  }

  const onConfirm = () => {
    setUpdateShow(false);
    resetState()
    setEdit(false)
  }

  useEffect(() => {
    if (statusEBD === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageEBD,
        'success',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearBackendDashboardViewDispatch()
      clearUpdateBackendDashboardDispatch()
    } else if (statusEBD === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageEBD,
        '',
        'Ok',
      )
      clearUpdateBackendDashboardDispatch()
    }
  }, [statusEBD])

  useEffect(() => {
    if (MonitorBackendDashboardData && MonitorBackendDashboardData.status == "ok") {
      const data = setBackendDashData(MonitorBackendDashboardData && MonitorBackendDashboardData.data)
      setFormData(data)
      setView(true)
    } else if (MonitorBackendDashboardData?.status === 'error') {
      setUpdateShow(false)
      warningAlert(
        'Error',
        MonitorBackendDashboardData?.message,
        '',
        'Ok')
      setView(false)
      clearBackendDashboardViewDispatch()
    }
  }, [MonitorBackendDashboardData])

  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => {
          setShow(false)
          clearMonitorViewsDispatch()
        }}
      >
        <Modal.Header
          style={{ backgroundColor: 'rgb(97 97 199)' }}
          closeButton={() => {
            setShow(false)
            clearMonitorViewsDispatch()
          }}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Changed Parameters
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            MonitorViewloading ?
              <div className="card card-custom card-stretch gutter-b p-8">
                <div className='row'>
                  <div className='col-lg-12 d-flex justify-content-center'>
                    <div
                      className='spinner-border text-primary m-5'
                      role='status'
                    />
                  </div>
                </div>
              </div>
              : (
                <div className="card card-custom card-stretch gutter-b p-8">
                  {
                    !_.isEmpty(MonitorViewsData && MonitorViewsData.data) ? (
                      <div
                      >
                        {
                          MonitorViewsData && MonitorViewsData.data.map((item, i) => {
                            return (
                              <ul className="nav" key={i}>
                                <li className="nav-item">
                                  {item}
                                </li>
                              </ul>

                            )
                          })
                        }
                      </div>
                    ) :
                      <div className='fs-4 fw-bold'> {MonitorViewsData && MonitorViewsData.message} </div>
                  }
                </div>
              )
          }
        </Modal.Body>
      </Modal>

      {view ? <Modal
        show={updateShow}
        size="lg"
        centered
        onHide={() => {
          setUpdateShow(false)
          clearBackendDashboardViewDispatch()
          setEdit(false)
          resetState()
        }}
      >
        <Modal.Header
          style={{ backgroundColor: 'rgb(97 97 199)' }}
          closeButton={() => {
            setUpdateShow(false)
            clearBackendDashboardViewDispatch()
            setEdit(false)
            resetState()
          }}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {edit ? 'Update' : 'View'}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            MonitorBackendDashboardLoading ?
              <div className="card card-custom card-stretch gutter-b p-8">
                <div className='row'>
                  <div className='col-lg-12 d-flex justify-content-center'>
                    <div
                      className='spinner-border text-primary m-5'
                      role='status'
                    />
                  </div>
                </div>
              </div>
              : (
                <div className='container-fixed'>
                  <div className='card-header bg-lightBlue'>
                    <div className='card-body p-5'>
                      {!edit ?
                        <>
                          <div className='row justify-content-end'>
                            <div className='col-lg-2'>
                              <button
                                className='btn btn-icon btn-icon-warning mt-1 fa-pull-right mb-4 bg-white'
                                onClick={() => { setEdit(true) }}
                                title="Edit"
                              >
                                <i className="bi bi-pencil-fill fs-7" />
                              </button>
                            </div>
                          </div>
                          <div className='row align-items-center'>
                            <div className='col-lg-3'>
                              <span className="fs-5 fw-bolder form-label ms-4">Website</span>
                            </div>
                            <div className='col-lg-1'>
                              <span className="fs-5 fw-bolder form-label">:</span>
                            </div>
                            <div className='col-lg-7'>
                              <span>{MonitorBackendDashboardData?.data?.website || "--"}</span>
                            </div>
                          </div>
                          <div className='row align-items-center mt-4'>
                            <div className='col-lg-3'>
                              <span className="fs-5 fw-bolder form-label ms-4">Legal Name</span>
                            </div>
                            <div className='col-lg-1'>
                              <span className="fs-5 fw-bolder form-label">:</span>
                            </div>
                            <div className='col-lg-7'>
                              <span>{MonitorBackendDashboardData?.data?.legalName || "--"}</span>
                            </div>
                          </div>
                          <div className='row align-items-center mt-4'>
                            <div className='col-lg-3'>
                              <span className="fs-5 fw-bolder form-label ms-4">BatchId</span>
                            </div>
                            <div className='col-lg-1'>
                              <span className="fs-5 fw-bolder form-label">:</span>
                            </div>
                            <div className='col-lg-7'>
                              <span>{MonitorBackendDashboardData?.data?.batchId || "--"}</span>
                            </div>
                          </div>
                          <div className='row align-items-center mt-5'>
                            <div className='col-lg-12'>
                              <table className="table p-4">
                                <thead className='border'>
                                  <tr>
                                    <th scope="col" className="fs-5 fw-bolder form-label text-center">Frontend</th>
                                    <th scope="col" className="fs-5 fw-bolder form-label text-center">Backend</th>
                                  </tr>
                                </thead>
                                <tbody className='border'>
                                  <tr>
                                    <td className='ps-3'>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Last Updated Date
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>
                                            {
                                              MonitorBackendDashboardData && MonitorBackendDashboardData.data && MonitorBackendDashboardData.data.frontendLastUpdatedDate ?
                                                moment(MonitorBackendDashboardData?.data?.frontendLastUpdatedDate).format("YYYY-MM-DD")
                                                : "--"
                                            }
                                          </span>
                                        </div>
                                      </div>
                                    </td>
                                    <td>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Last Updated Date
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>
                                            {
                                              MonitorBackendDashboardData && MonitorBackendDashboardData.data && MonitorBackendDashboardData.data.backendLastUpdatedDate ?
                                                moment(MonitorBackendDashboardData?.data?.backendLastUpdatedDate).format("YYYY-MM-DD")
                                                : "--"
                                            }
                                          </span>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className='ps-3'>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Delay
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>{MonitorBackendDashboardData?.data?.frontendDelay.toString() || "--"}</span>
                                        </div>
                                      </div>
                                    </td>
                                    <td>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Delay
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>{MonitorBackendDashboardData?.data?.backendDelay.toString() || "--"}</span>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className='ps-3'>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Next Execution Date
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>
                                            {
                                              MonitorBackendDashboardData && MonitorBackendDashboardData.data && MonitorBackendDashboardData.data.frontendNextExecutionDate ?
                                                moment(MonitorBackendDashboardData?.data?.frontendNextExecutionDate).format("YYYY-MM-DD")
                                                : "--"
                                            }
                                          </span>
                                        </div>
                                      </div>
                                    </td>
                                    <td>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Next Execution Date
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>
                                            {
                                              MonitorBackendDashboardData && MonitorBackendDashboardData.data && MonitorBackendDashboardData.data.backendendNextExecutionDate ?
                                                moment(MonitorBackendDashboardData?.data?.backendendNextExecutionDate).format("YYYY-MM-DD")
                                                : "--"
                                            }
                                          </span>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td className='ps-3'>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Last Execution Date
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>
                                            {
                                              MonitorBackendDashboardData && MonitorBackendDashboardData.data && MonitorBackendDashboardData.data.frontendLastUpdatedDate ?
                                                moment(MonitorBackendDashboardData?.data?.frontendLastUpdatedDate).format("YYYY-MM-DD")
                                                : "--"
                                            }
                                          </span>
                                        </div>
                                      </div>
                                    </td>
                                    <td>
                                      <div className='row align-items-center'>
                                        <div className='col-lg-7'>
                                          <span className="fs-6 fw-bolder form-label">
                                            Last Execution Date
                                          </span>
                                        </div>
                                        <div className='col-lg-1'>
                                          <span className="fs-6 fw-bolder form-label">:</span>
                                        </div>
                                        <div className='col-lg-4'>
                                          <span>
                                            {
                                              MonitorBackendDashboardData && MonitorBackendDashboardData.data && MonitorBackendDashboardData.data.backendLastUpdatedDate ?
                                                moment(MonitorBackendDashboardData?.data?.backendLastUpdatedDate).format("YYYY-MM-DD")
                                                : "--"
                                            }
                                          </span>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </> :
                        <>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <div className='row align-items-center'>
                              <div className='col-lg-4'>
                                <label className="fs-5 fw-bolder form-label ms-4">
                                  <span>Website :</span>
                                </label>
                              </div>
                              <div className='col-lg-7'>
                                <input
                                  name="website"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="Website"
                                  disabled
                                  autoComplete="off"
                                  value={formData.website || ""}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <div className='row align-items-center'>
                              <div className='col-lg-4'>
                                <label className="fs-5 fw-bolder form-label ms-4">
                                  <span>BatchId :</span>
                                </label>
                              </div>
                              <div className='col-lg-7'>
                                <input
                                  name="batchId"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="BatchId"
                                  autoComplete="off"
                                  disabled
                                  value={formData.batchId || ""}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <div className='row align-items-center'>
                              <div className='col-lg-4'>
                                <label className="fs-5 fw-bolder form-label ms-4">
                                  <span>Legal Name :</span>
                                </label>
                              </div>
                              <div className='col-lg-7'>
                                <input
                                  name="legalName"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="Legal Name"
                                  disabled
                                  autoComplete="off"
                                  value={formData.legalName || ""}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <div className='row align-items-center'>
                              <div className='col-lg-4'>
                                <label className="fs-5 fw-bolder form-label ms-4">
                                  <span className="required">Delay :</span>
                                </label>
                              </div>
                              <div className='col-lg-7'>
                                <input
                                  name="backendDelay"
                                  type="text"
                                  className="form-control bg-secondary"
                                  placeholder="Backend Delay"
                                  maxLength={10}
                                  onChange={(e) =>
                                    handleInputChange(e)
                                  }
                                  autoComplete="off"
                                  value={formData.backendDelay || ""}
                                />
                                {errors && errors.backendDelay && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.backendDelay}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <div className='row align-items-center'>
                              <div className='col-lg-4'>
                                <label className="fs-5 fw-bolder form-label ms-4">
                                  <span className="required">Next Execution Date :</span>
                                </label>
                              </div>
                              <div className='col-lg-7'>
                                <DateSelector
                                  name='backendendNextExecutionDate'
                                  placeholder='Next Execution Data'
                                  className='form-control'
                                  selected={formData.backendendNextExecutionDate || ''}
                                  onChange={(date) => {
                                    setErrors({ ...errors, backendendNextExecutionDate: '' })
                                    setFormData((values) => ({
                                      ...values,
                                      backendendNextExecutionDate: date
                                    }))
                                  }}
                                  dateFormat={DATE.DATE_FOR_PICKER}
                                  isClearable={true}
                                  peek={true}
                                  monthDropdown={true}
                                  yearDropdown={true}
                                  showYear={true}
                                />
                                {errors && errors.backendendNextExecutionDate && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.backendendNextExecutionDate}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className="fv-row mb-10 fv-plugins-icon-container">
                            <div className='row align-items-center'>
                              <div className='col-lg-4'>
                                <label className="fs-5 fw-bolder form-label ms-4">
                                  <span className="required">Last Execution Date :</span>
                                </label>
                              </div>
                              <div className='col-lg-7'>
                                <DateSelector
                                  name='lastExecutionDate'
                                  placeholder='Last Execution Data'
                                  className='form-control'
                                  selected={formData.lastExecutionDate || ''}
                                  onChange={(date) => {
                                    setErrors({ ...errors, lastExecutionDate: '' })
                                    setFormData((values) => ({
                                      ...values,
                                      lastExecutionDate: date
                                    }))
                                  }}
                                  dateFormat={DATE.DATE_FOR_PICKER}
                                  isClearable={true}
                                  peek={true}
                                  monthDropdown={true}
                                  yearDropdown={true}
                                  showYear={true}
                                />
                                {errors && errors.lastExecutionDate && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.lastExecutionDate}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                          <div className='form-group row mt-4'>
                            <div className='col-lg-11'>
                              <div className='row justify-content-end'>
                                <div className='col-lg-2'>
                                  <button
                                    className='btn btn-blue mt-7 fa-pull-right mb-4'
                                    onClick={() => { setEdit(false) }}
                                  >
                                    Cancel
                                  </button>
                                </div>
                                <div className='col-lg-2'>
                                  <button
                                    className='btn btn-blue mt-7 fa-pull-right mb-4'
                                    onClick={(event) => { handleSubmit() }}
                                  >
                                    {UpdateBackendDashboardLoadingEBD
                                      ? (
                                        <span
                                          className='spinner-border spinner-border-sm mx-3'
                                          role='status'
                                          aria-hidden='true'
                                        />
                                      )
                                      : (
                                        'Submit'
                                      )}
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </>}
                    </div>
                  </div>
                </div>
              )
          }
        </Modal.Body>
      </Modal> : null}

      <div id="kt_app_content_container" className="app-container">
        <div className="card-deck">
          <div className="row ">
            <div className="col-md-6 col-lg-3 col-xl-3">
              <div
                className="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-md-80 mb-10 mb-xl-10"
                style={{
                  backgroundColor: "#f1416c"
                }
                }
              >
                <div className="card-body pt-5"
                  style={{
                    backgroundColor: "rgba(0, 0, 0, 0.15)"
                  }}
                >
                  <div className="d-flex flex-column mb-6 mt-6">
                    <div className='row'>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="fs-2hx fw-bold text-white">
                          {
                            MonitorCountsData && MonitorCountsData.data && MonitorCountsData.data.totalCases
                          }
                        </div>
                      </div>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="pt-1 fw-semibold fs-7 text-white d-flex justify-content-center">
                          Total Cases
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-6 col-lg-3 col-xl-3">
              <div
                className="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-md-90 mb-5 mb-xl-10"
                style={{ backgroundColor: "#f1416c" }}
              >
                <div className="card-body pt-5"
                  style={{
                    backgroundColor: "rgba(0, 0, 0, 0.15)"
                  }}
                >
                  <div className="d-flex flex-column mb-6 mt-6">
                    <div className='row'>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="fs-2hx fw-bold text-white">
                          {
                            MonitorCountsData && MonitorCountsData.data && MonitorCountsData.data.changesDetected
                          }
                        </div>
                      </div>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="pt-1 fw-semibold fs-7 text-white d-flex justify-content-center">
                          Changes Detected
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 col-xl-3">
              <div
                className="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-md-80 mb-5 mb-xl-10"
                style={{ backgroundColor: "#f1416c" }}
              >
                <div className="card-body pt-5"
                  style={{
                    backgroundColor: "rgba(0, 0, 0, 0.15)"
                  }}
                >
                  <div className="d-flex flex-column mb-6 mt-6">
                    <div className='row'>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="fs-2hx fw-bold text-white">
                          {
                            MonitorCountsData && MonitorCountsData.data && MonitorCountsData.data.approvedStatus
                          }
                        </div>
                      </div>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="pt-1 fw-semibold fs-7 text-white d-flex justify-content-center">
                          Changes Detected - Approved
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 col-xl-3"
            >
              <div
                className="card card-flush bgi-no-repeat bgi-size-contain bgi-position-x-end h-md-80 mb-5 mb-xl-10"
                style={{ backgroundColor: "#f1416c" }}
              >
                <div className="card-body pt-5"
                  style={{
                    backgroundColor: "rgba(0, 0, 0, 0.15)"
                  }}
                >
                  <div className="d-flex flex-column mb-6 mt-6">
                    <div className='row'>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="fs-2hx fw-bold text-white">
                          {
                            MonitorCountsData && MonitorCountsData.data && MonitorCountsData.data.rejectedStatus
                          }
                        </div>
                      </div>
                      <div className="col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center">
                        <div className="pt-1 fw-semibold fs-7 text-white d-flex justify-content-center">
                          Changes Detected - Rejected
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='row justify-content-start m-2'>
            <div className='col-md-2'>
              {MonitorData && MonitorData.data && MonitorData.data.count && (
                <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                  Total:
                  <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                    {MonitorData && MonitorData.data && MonitorData.data.count}
                  </span>
                </span>
              )}
            </div>
            <div className='col-md-10'>
              <div className='row'>
                <div className='col-lg-3 d-flex'>
                  <label className='col-form-label text-lg-start'>
                    Record(s) per Page : &nbsp;{' '}
                  </label>
                  <div className='col-md-2'>
                    <select
                      className='form-select w-6rem'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleRecordPerPage(e)}
                    >
                      <option value='25'>25</option>
                      <option value='50'>50</option>
                      <option value='75'>75</option>
                      <option value='100'>100</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-9 d-flex justify-content-end overflow-break'>
                  <div className='my-auto'>
                    {
                      !_.isEmpty(actionIds) ? (
                        <Status actionIds={actionIds} setActionIds={setActionIds} getMonitorlistDispatch={getMonitorlistDispatch} getMonitorCountsDispatch={getMonitorCountsDispatch} limit={limit} />
                      ) : null
                    }
                  </div>
                  <div className='my-auto'>
                    <FindRole
                      role={Role}
                    >
                      {forceUploadShow ? <OgmForceUpload /> : null}
                    </FindRole>
                  </div>
                  <div className="my-auto">
                    <AddMonitor />
                  </div>
                  <div className="my-auto">
                    <Actions actionIds={actionIds} />
                  </div>
                  <div className="my-auto">
                    <SearchList setSearchData={setSearchData} setCredFilterParams={setCredFilterParams} limit={limit} activePageNumber={activePageNumber} setForceUploadShow={setForceUploadShow} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className="fw-bolder fs-8 text-gray-800">
                <tr>
                  <th className="min-w-50px text-center">
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                  <th className="min-w-50px text-center">
                    <div className="d-flex">
                      <span>OGM ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogmId")}
                        >
                          <i
                            className={`bi ${sorting.ogmId
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-50px text-center">
                    <div className="d-flex">
                      <span>Batch ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("batchId")}
                        >
                          <i
                            className={`bi ${sorting.batchId
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("website")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px">
                    <div className="d-flex">
                      <span>Enqueue Hours</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Priority</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Acquirer</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("acquirer")}
                        >
                          <i
                            className={`bi ${sorting.acquirer
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {/* {
                    ClinetId ? null : (
                      <th className="min-w-100px text-center">
                        <div className="d-flex">
                          <span>Merchant status</span>
                        </div>
                      </th>
                    )
                  } */}
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Onboarding date</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_start_date")}
                        >
                          <i
                            className={`bi ${sorting.ogm_start_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Current scan date</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_last_run_date")}
                        >
                          <i
                            className={`bi ${sorting.ogm_last_run_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Next scan date</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_next_run_date")}
                        >
                          <i
                            className={`bi ${sorting.ogm_next_run_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Changes detected</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Changed parameter</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      MonitorData &&
                        MonitorData.data
                        ? (
                          MonitorData.data && MonitorData.data.result.map((item, i) => {
                            const indiaDate = moment.utc(item && item.createdAt).local()
                            const todayDate = moment()
                            const duration = moment.duration(todayDate.diff(indiaDate))
                            const hours = Math.floor(duration.asHours());
                            const minutes = duration.minutes();
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                                className='text-center'
                              >
                                <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                                  <input
                                    className="form-check-input cursor-pointer"
                                    type="checkbox"
                                    value={checked}
                                    onChange={() =>
                                      handleCheckboxChange(item._id)
                                    }
                                    id="flexRadioLg"
                                  />
                                  <button
                                    className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-2'
                                    onClick={() => onDeleteItem(item._id)}
                                    title="Delete"
                                  >
                                    <KTSVG
                                      path='/media/icons/duotune/general/gen027.svg'
                                      className='svg-icon-3'
                                    />
                                  </button>
                                </td>
                                <td className="ellipsis align-items-center text-start">
                                  <sup>
                                    <span className={`me-2 text-danger ${NEWDATA[item && item.callbackType]}`}
                                    >
                                      {
                                        item && item.callbackType === "NEW" ? item.callbackType : <>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>
                                      }
                                    </span>
                                  </sup>
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => window.open(`/ongoing-monitoring-dashboard/update/${item._id}`, "_blank")}
                                    to={`/ongoing-monitoring-dashboard/update/${item._id}`}
                                  >
                                    OGM{item.ogm_Id ? item.ogm_Id : "--"}
                                  </a>
                                  <button
                                    className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px ms-3'
                                    onClick={() => { viewBackend(item && item._id) }}
                                    title="View Details"
                                  >
                                    <i className="bi bi-eye-fill fs-4" />
                                  </button>
                                </td>
                                <td className="ellipsis text-start">
                                  {
                                    item && item.batchId ? item.batchId : '--'
                                  }
                                </td>
                                <td className="ellipsis text-start">
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => window.open(item.website ? item.website : "--", "_blank")}
                                  >
                                    {
                                      item.website ? item.website : "--"
                                    }
                                  </a>
                                </td>
                                <td className="ellipsis text-start">
                                  {item && item.enqueueHours ? !_.isEmpty(item.enqueueHours) ? item.enqueueHours : `${hours}:${minutes}` : `${hours}:${minutes}`}
                                </td>
                                <td className="ellipsis text-start">
                                  {item && item.priority ? item.priority : "--"}
                                </td>
                                <td className="ellipsis text-start">
                                  {
                                    item && item.acquirer ? item.acquirer : '--'
                                  }
                                </td>
                                {/* {
                                  ClinetId ? null : (
                                    <td className="ellipsis">
                                      <span className={`badge ${RISKSTATUS[item.ogm_status && item.ogm_status]}`}>
                                        {item.ogm_status ? item.ogm_status : "--"}
                                      </span>
                                    </td>
                                  )
                                } */}
                                <td className="ellipsis">
                                  {
                                    !_.isEmpty(item.ogm_start_date) ?
                                      moment(
                                        item.ogm_start_date ? item.ogm_start_date : "--"
                                      ).format("MMM Do YY")
                                      : '--'
                                  }
                                </td>
                                <td className="ellipsis">
                                  {
                                    !_.isEmpty(item.ogm_last_run_date) ?
                                      moment(
                                        item.ogm_last_run_date ? item.ogm_last_run_date : "--"
                                      ).format("MMM Do YY")
                                      : '--'
                                  }
                                </td>
                                <td className="ellipsis">
                                  {
                                    !_.isEmpty(item.ogm_next_run_date) ?
                                      moment(
                                        item.ogm_next_run_date ? item.ogm_next_run_date : "--"
                                      ).format("MMM Do YY")
                                      : '--'
                                  }
                                </td>
                                <td className="ellipsis">
                                  {
                                    item && item.changesDetected ?
                                      <i className={`${MONITOR_STATUS[item && item.changesDetected]}`}
                                      />
                                      : '--'
                                  }
                                </td>
                                <td className="ellipsis">
                                  {
                                    item && item.changesDetected === 'YES' ?
                                      <button className='btn btn-sm btn-light-primary'
                                        onClick={() => { viewChange(item && item._id) }}
                                      >
                                        Click to view
                                      </button>
                                      : '--'
                                  }
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { MonitorStore, MonitorViewsStore, DeletemonitorReportStore, BackendDashboadReportStore, UpdateBackendDashboardReportStore } = state
  return {
    MonitorData: state && state.MonitorStore && state.MonitorStore.MonitorData,
    loading: MonitorStore && MonitorStore.loading ? MonitorStore.loading : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    MonitorCountsData: state && state.MonitorCountsStore && state.MonitorCountsStore.MonitorCountsData,
    MonitorViewsData: state && state.MonitorViewsStore && state.MonitorViewsStore.MonitorViewsData,
    MonitorViewloading: MonitorViewsStore && MonitorViewsStore.loading ? MonitorViewsStore.loading : false,
    DeletemonitorReportData: DeletemonitorReportStore && DeletemonitorReportStore.DeletemonitorReportData ? DeletemonitorReportStore.DeletemonitorReportData : '',
    MonitorBackendDashboardData: state && state.BackendDashboadReportStore && state.BackendDashboadReportStore.BackendDashboadReportData,
    MonitorBackendDashboardLoading: BackendDashboadReportStore && BackendDashboadReportStore.loading ? BackendDashboadReportStore.loading : false,
    UpdateBackendDashboardData: state && state.UpdateBackendDashboardReportStore && state.UpdateBackendDashboardReportStore.UpdateBackendDashboardReportData,
    UpdateBackendDashboardLoadingEBD: UpdateBackendDashboardReportStore && UpdateBackendDashboardReportStore.loading ? UpdateBackendDashboardReportStore.loading : false,
    statusEBD: UpdateBackendDashboardReportStore && UpdateBackendDashboardReportStore.statusEBD ? UpdateBackendDashboardReportStore.statusEBD : '',
    messageEBD: UpdateBackendDashboardReportStore && UpdateBackendDashboardReportStore.messageEBD ? UpdateBackendDashboardReportStore.messageEBD : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getMonitorlistDispatch: (params) => dispatch(MonitorActions.getMonitorlist(params)),
  setFilterFunctionDispatch: (data) =>
    dispatch(clientCredFilterActions.setFilterFunction(data)),
  getMonitorCountsDispatch: (params) => dispatch(MonitorCountsActions.getMonitorCounts(params)),
  getMonitorViewsDispatch: (params) => dispatch(MonitorViewsActions.getMonitorViews(params)),
  clearMonitorViewsDispatch: (params) => dispatch(MonitorViewsActions.clearMonitorViews(params)),
  deletemonitorReportDispatch: (params) => dispatch(deleteMonitorReportActions.deletemonitorReport(params)),
  clearDeletemonitorReportDispatch: (params) => dispatch(deleteMonitorReportActions.clearDeletemonitorReport(params)),

  backendDashboardViewDispatch: (params) => dispatch(viewBackendDashboardActions.backendDashboardView(params)),
  clearBackendDashboardViewDispatch: (params) => dispatch(viewBackendDashboardActions.clearBackendDashboardView(params)),

  updateBackendDashboardDispatch: (id, data) => dispatch(UpdateBackendDashboardActions.getUpdateBackendDashboard(id, data)),
  clearUpdateBackendDashboardDispatch: () => dispatch(UpdateBackendDashboardActions.clearUpdateBackendDashboard())
})

export default connect(mapStateToProps, mapDispatchToProps)(MonitorList)
