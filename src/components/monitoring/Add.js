import React, { useEffect, useState, useRef } from 'react'
import {
  MonitorActions,
  clientIdLIstActions,
  OGMintervalActions,
  OGMactions
} from '../../store/actions'
import { connect } from 'react-redux'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { STATUS_RESPONSE, DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT, DATE } from '../../utils/constants'
import { warningAlert, confirmationAlert, successAlert } from "../../utils/alerts"
import './styles.css'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Modal from 'react-bootstrap/Modal'
import { userValidation } from './validation'
import { getLocalStorage, removeLocalStorage } from '../../utils/helper'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { CSVLink } from "react-csv"
import { DateSelector } from '../../theme/layout/components/DateSelector'
import moment from "moment"
import clsx from 'clsx'

function AddMonitor(props) {
  const {
    getMonitorlistDispatch,
    OGMpostDAta,
    clearOGMpostDispatch,
    cleargetWebAnalysislistDispatch,
    clientIdDispatch,
    clinetIdLists,
    OGMpostloading,
    getOGMintervalDispatch,
    OGMpostDispatch,
    OGMintervalData
  } = props

  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [errors, setErrors] = useState({})
  const [error, setError] = useState({})
  const [fileName, setFileName] = useState()
  const [show, setShow] = useState(false)
  const [Webshow, setWebShow] = useState(false)
  const [key, setKey] = useState('BulkUpload')
  const [editMode, setEditMode] = useState(false)
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')

  const [formData, setFormData] = useState({
    clientId: Role === 'Client User' ? ClinetId : '',
    monitoringInterval: '',
    file: '',
    uploadedDateFrom: '',
    previousInterval: '',
    previousBatchName: ''
  })

  useEffect(() => {
    // getMonitorlistDispatch()
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
    removeLocalStorage('ExportHide')
    getOGMintervalDispatch()
  }, [])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getMonitorlistDispatch(params)
  }

  const hiddenFileInput = useRef(null)
  const handleChange = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleFileChange = (e) => {
    e.preventDefault()
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setFormData((values) => ({
          ...values,
          file: files,
        }))
        setErrors((values) => ({ ...values, file: "" }))
        setFileName(files && files.name)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID })
    }
  }

  const handleClick = (event) => {
    hiddenFileInput.current.click(event)
  }

  const OnSubmit = () => {
    const errorMsg = userValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("intervel", formData.monitoringInterval)
      data.append("previousInterval", formData.previousInterval)
      data.append("previousBatchName", !_.isEmpty(formData.previousInterval) ? formData.previousBatchName : '')
      data.append("startDate", moment(formData.uploadedDateFrom ? formData.uploadedDateFrom : "--").format("YYYY/MM/DD"))
      data.append("clientId", formData.clientId)
      data.append("file", formData.file)
      OGMpostDispatch(data)
    }
  }

  const clearPopup = () => {
    setSelectedAsigneesOption('')
    setEditMode(false)
    setShow(false)
    setWebShow(false)
    setFormData(values => ({
      ...values,
      file: '',
      monitoringInterval: '',
      clientId: '',
      uploadedDateFrom: '',
      previousInterval: '',
      previousBatchName: ''
    }))
  }

  const onConfirm = () => {
    setShow(false)
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    clearOGMpostDispatch()
    setFormData(values => ({
      ...values,
      file: '',
      uploadedDateFrom: '',
      previousInterval: '',
      previousBatchName: ''
    }))
  }

  const clear = () => {
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    clearOGMpostDispatch()
    setFormData(values => ({
      ...values,
      file: '',
      uploadedDateFrom: '',
      previousInterval: '',
      previousBatchName: ''
    }))
  }

  useEffect(() => {
    if (show === false) {
      setKey('BulkUpload')
    }
  }, [show])

  useEffect(() => {
    if (OGMpostDAta && OGMpostDAta.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        'success',
        "Assigned Successfully",
      )
      setShow(false)
      getMonitorlistDispatch()
      cleargetWebAnalysislistDispatch()
      setSelectedAsigneesOption('')
      clear()
    } else if (OGMpostDAta && OGMpostDAta.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        OGMpostDAta && OGMpostDAta.message,
        '',
        'Try again',
        '',
        () => { {} }
      )
      clearOGMpostDispatch()
    }
  }, [OGMpostDAta])

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const data = [
    { website: "http://mykono.in" },
    { website: "https://reavala.com" },
    { website: "https://www.walloon.in" },
    { website: "https://krazybox.in" },
    { website: "https://cooldown.co.in" }
  ]

  return (
    <>
      <div>
        <button
          type="button"
          className="btn btn-primary ml-1"
          onClick={() => {
            setShow(true);
          }}
        >
          {/* eslint-disable */}
          <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
          {/* eslint-disable */}
          Add web cases
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"} Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab ftab"
          >
            <Tab eventKey="BulkUpload" title="BulkUpload">
              <div className="card card-custom card-stretch gutter-b p-8">
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Client :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name='AppUserId'
                          className='select2'
                          classNamePrefix='select'
                          handleChangeReactSelect={handleChangeAsignees}
                          options={AsigneesOption}
                          value={SelectedAsigneesOption}
                          isDisabled={!AsigneesOption}
                        />
                        {error && error.client && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.client}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    null
                  )
                }
                <div className='row mt-4'>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Previous Interval :
                    </label>
                  </div>
                  <div className='col-lg-8'>
                    <select
                      name='previousInterval'
                      className='form-select form-select-solid'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={formData.previousInterval || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='ONE TIME'>ONE TIME</option>
                    </select>
                    {errors && errors.previousInterval && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.previousInterval}
                      </div>
                    )}
                  </div>
                </div>
                {
                  formData.previousInterval === 'ONE TIME' ? (
                    <div className='row mt-4'>
                    <div className='col-lg-4 mb-3'>
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Previous BatchName :
                      </label>
                    </div>
                    <div className='col-lg-8'>
                    <input
                          placeholder='Previous BatchName'
                          className={clsx(
                            'form-control form-control-lg form-control-solid',
                            { 'is-invalid': formData.previousBatchName && error.previousBatchName },
                            {
                              'is-valid': formData.previousBatchName && !error.previousBatchName
                            }
                          )}
                          onChange={(e) => handleChange(e)}
                          type='text'
                          name='previousBatchName'
                          autoComplete='off'
                          value={formData.previousBatchName || ''}
                        />
                      {errors && errors.previousBatchName && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.previousBatchName}
                        </div>
                      )}
                    </div>
                  </div>
                  ): null
                }
                <div className='row mt-4'>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Monitoring Interval :
                    </label>
                  </div>
                  <div className='col-lg-8'>
                    <div className="form-check form-check-custom form-check-solid mb-4">
                      {
                        OGMintervalData && _.isArray(OGMintervalData.data) ? (OGMintervalData.data.map((item, i) => {
                          return (
                            <label className='d-flex flex-stack mb-5 cursor-pointer' key={i}>
                              <span className='form-check form-check-custom form-check-solid me-2'>
                                <input
                                  className='form-check-input ms-2'
                                  type='radio'
                                  onChange={(e) => handleChange(e)}
                                  value={item && item.interval}
                                  name='monitoringInterval'
                                />
                              </span>
                              <span className='d-flex flex-column'>
                                <span className='fs-7 text-muted'>
                                  {item && item.interval}
                                </span>
                              </span>
                            </label>
                          )
                        })) : null
                      }
                    </div>
                    {errors && errors.monitoringInterval && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.monitoringInterval}
                      </div>
                    )}
                  </div>
                </div>

                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Uploaded Date From:
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <DateSelector
                      name='uploadedDateFrom'
                      placeholder='Uploaded Date From'
                      className='form-control'
                      selected={formData.uploadedDateFrom || ''}
                      onChange={(date) => {
                        setErrors({ ...errors, uploadedDateFrom: '' })
                        setFormData((values) => ({
                          ...values,
                          uploadedDateFrom: date
                        }))
                      }}
                      dateFormat={DATE.DATE_FOR_PICKER}
                      maxDate={new Date()}
                      isClearable={true}
                      peek={true}
                      monthDropdown={true}
                      yearDropdown={true}
                      showYear={true}
                    />
                  </div>
                </div>


                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label required">
                      Upload Document :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      type="file"
                      className="d-none"
                      name="file"
                      id="file"
                      multiple={true}
                      ref={hiddenFileInput}
                      onChange={handleFileChange}
                    />
                    <button
                      type="button"
                      style={{
                        width: "100%",
                      }}
                      className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                      onClick={handleClick}
                    >
                      <i className="bi bi-filetype-csv" />
                      Upload Document
                    </button>
                    {errors && errors.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.file}
                      </div>
                    )}
                    {fileName && fileName}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <CSVLink
                      data={data}
                      filename={"sampleFileForImport.csv"}
                      className="btn btn-sm btn-light-success btn-responsive font-5vw"
                      target="_blank"
                    >
                      <i className="bi bi-filetype-csv" />
                      Download_Sample
                    </CSVLink>
                    <div className='fs-7 mt-4 text-danger'>* Website URL alone is a mandatory input</div>
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      type='button'
                      className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                      onClick={(e) => OnSubmit(e)}
                      disabled={OGMpostloading}
                    >
                      {!OGMpostloading && <span className='indicator-label'>Submit</span>}
                      {OGMpostloading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    clinetListStore,
    OGMintervalStore,
    OGMStore
  } = state

  return {
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    OGMintervalData: OGMintervalStore && OGMintervalStore.OGMintervalData ? OGMintervalStore.OGMintervalData : '',
    OGMpostDAta: OGMStore && OGMStore.OGMpostDAta ? OGMStore.OGMpostDAta : '',
    OGMpostloading: OGMStore && OGMStore.loading ? OGMStore.loading : false,
  }
}

const mapDispatchToProps = dispatch => ({
  getMonitorlistDispatch: (data) => dispatch(MonitorActions.getMonitorlist(data)),
  cleargetWebAnalysislistDispatch: (data) => dispatch(MonitorActions.clearMonitorlist(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  getOGMintervalDispatch: (data) => dispatch(OGMintervalActions.getOGMinterval(data)),
  OGMpostDispatch: (data) => dispatch(OGMactions.OGMpost(data)),
  clearOGMpostDispatch: (data) => dispatch(OGMactions.clearOGMpost(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddMonitor)