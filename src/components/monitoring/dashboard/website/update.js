import React, { useState, useEffect } from "react"
import "bootstrap-icons/font/bootstrap-icons.css"
import { connect } from "react-redux"
import _ from "lodash"
import {
  UpdateCurrentReportActions,
  BWListActions
} from "../../../../store/actions"
import Modal from "react-bootstrap/Modal"
import clsx from "clsx"
import { useLocation } from "react-router-dom"
import { warningAlert, successAlert } from "../../../../utils/alerts"
import { STATUS_RESPONSE } from '../../../../utils/constants'
import color from "../../../../utils/colors"
import Select from 'react-select'

function Update(props) {
  const {
    CurrentScandata,
    getUpdateCurrentReportDispatch,
    UpdateCurrentReportData,
    loading,
    MonitorDashboardDispatch,
    clearUpdateCurrentReportDispatch,
    getBWListDispatch,
    BWlistData,
    MonitorDashboardIData
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  const [error, setError] = useState({})
  const [show, setShow] = useState(false)
  const [BWlistsOption, setBWlistsOption] = useState()
  const [SelectedBWlistsOption, setSelectedBWlistsOption] = useState('')
  const [editFormData, seteditFormData] = useState({
    batchId: "",
    ogmId: "",
    clientId: "",
    website: "",
    acquirer: "",
    websiteWorking: "",
    websiteSuccessRate: "",
    changeInLineOfBusiness: "",
    changeInMcc: "",
    changeInLegalName: "",
    changeInMerchantAddress: "",
    transactionLaunderingRedirection: "",
    riskClassification: "",
    increaseInFullPageLoadingTime: "",
    contactDetailsPhone: "",
    contactDetailsEmail: "",
    onlineReputationDrop: "",
    heavyDiscounts: "",
    nonInrPricing: "",
    domainRisk: "",
    parkedDomain: "",
    contactUsPageUrl: "",
    privacyPolicyPageUrl: "",
    shippingPolicyPageUrl: "",
    returnPolicyPageUrl: "",
    termsAndConditionPageUrl: "",
    riskLevel: "",
    contactUsContentStatus: "",
    privacyPolicyContentStatus: "",
    shippingPolicyContentStatus: "",
    returnPolicyContentStatus: "",
    termsAndConditionContentStatus: "",
    validPhone:'',
    validEmail:''
  })
  const BWlist = BWlistData && BWlistData.data

  const handleChanges = (e) => {
    seteditFormData((editFormData) => ({
      ...editFormData,
      [e.target.name]: e.target.value,
    }))
    setError({ ...error, [e.target.name]: "" })
  }

  const handleUpdate = () => {
    getUpdateCurrentReportDispatch(id, editFormData)
  }

  const clearPopup = () => {
    setShow(false)
  }

  useEffect(() => {
    if (!_.isEmpty(MonitorDashboardIData && MonitorDashboardIData.clientId)) {
      const params = {
        clientId: MonitorDashboardIData && MonitorDashboardIData.clientId
      }
      getBWListDispatch(params)
    }
  }, [MonitorDashboardIData])

  const inlineData = CurrentScandata && CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.value

  useEffect(() => {
    if (CurrentScandata) {
      seteditFormData({
        websiteWorking: CurrentScandata && CurrentScandata.websiteWorking && CurrentScandata.websiteWorking.value,
        websiteSuccessRate: CurrentScandata && CurrentScandata.websiteSuccessRate && CurrentScandata.websiteSuccessRate.value,
        changeInLineOfBusiness: CurrentScandata && CurrentScandata.changeInLineOfBusiness && CurrentScandata.changeInLineOfBusiness.value,
        changeInMerchantAddress: CurrentScandata && CurrentScandata.changeInMerchantAddress && CurrentScandata.changeInMerchantAddress.value,
        changeInLegalName: CurrentScandata && CurrentScandata.changeInLegalName && CurrentScandata.changeInLegalName.value,
        transactionLaunderingRedirection: CurrentScandata && CurrentScandata.transactionLaunderingRedirection && CurrentScandata.transactionLaunderingRedirection.value,
        increaseInFullPageLoadingTime: CurrentScandata && CurrentScandata.increaseInFullPageLoadingTime && CurrentScandata.increaseInFullPageLoadingTime.value,
        parkedDomain: CurrentScandata && CurrentScandata.parkedDomain && CurrentScandata.parkedDomain.value,
        onlineReputationDrop: CurrentScandata && CurrentScandata.onlineReputationDrop && CurrentScandata.onlineReputationDrop.value,
        heavyDiscounts: CurrentScandata && CurrentScandata.heavyDiscounts && CurrentScandata.heavyDiscounts.value,
        nonInrPricing: CurrentScandata && CurrentScandata.nonInrPricing && CurrentScandata.nonInrPricing.value,
        contactDetailsPhone: CurrentScandata && CurrentScandata.contactDetailsPhone && CurrentScandata.contactDetailsPhone.value,
        contactDetailsEmail: CurrentScandata && CurrentScandata.contactDetailsEmail && CurrentScandata.contactDetailsEmail.value,
        id: CurrentScandata && CurrentScandata._id,
        ogmId: CurrentScandata && CurrentScandata.ogmId,
        clientId: CurrentScandata && CurrentScandata.clientId,
        website: CurrentScandata && CurrentScandata.website,
        batchId: CurrentScandata && CurrentScandata.batchId,
        privacyPolicyPageUrl: CurrentScandata && CurrentScandata.privacyPolicyPageUrl && CurrentScandata.privacyPolicyPageUrl.value,
        shippingPolicyPageUrl: CurrentScandata && CurrentScandata.shippingPolicyPageUrl && CurrentScandata.shippingPolicyPageUrl.value,
        returnPolicyPageUrl: CurrentScandata && CurrentScandata.returnPolicyPageUrl && CurrentScandata.returnPolicyPageUrl.value,
        termsAndConditionPageUrl: CurrentScandata && CurrentScandata.termsAndConditionPageUrl && CurrentScandata.termsAndConditionPageUrl.value,
        contactUsPageUrl: CurrentScandata && CurrentScandata.contactUsPageUrl && CurrentScandata.contactUsPageUrl.value,
        domainRisk: CurrentScandata && CurrentScandata.domainRisk && CurrentScandata.domainRisk.value,
        validPhone: CurrentScandata && CurrentScandata.containsValidPhone && CurrentScandata.containsValidPhone.value,
        validEmail: CurrentScandata && CurrentScandata.containsValidEmail && CurrentScandata.containsValidEmail.value,
        malwareDomain: CurrentScandata && CurrentScandata.malwareDomain && CurrentScandata.malwareDomain.value,
        phishingDomain: CurrentScandata && CurrentScandata.phishingDomain && CurrentScandata.phishingDomain.value,
        spammingDomain: CurrentScandata && CurrentScandata.spammingDomain && CurrentScandata.spammingDomain.value,
        mccode: CurrentScandata && CurrentScandata.changeInMcc && CurrentScandata.changeInMcc.value,
        riskClassification: CurrentScandata && CurrentScandata.riskClassification && CurrentScandata.riskClassification.value,
      }) 
    }
  }, [CurrentScandata])

  useEffect(() => {
    if (UpdateCurrentReportData && UpdateCurrentReportData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        UpdateCurrentReportData && UpdateCurrentReportData.message,
        'success'
      )
      setShow(false)
      MonitorDashboardDispatch(id)
    } else if (UpdateCurrentReportData && UpdateCurrentReportData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        UpdateCurrentReportData && UpdateCurrentReportData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearUpdateCurrentReportDispatch()
    }
  }, [UpdateCurrentReportData])


  const handleChangeBWlists = selectedOption => {
    if (selectedOption !== null) {
      setSelectedBWlistsOption(selectedOption)
      seteditFormData(values => ({ ...values, changeInLineOfBusiness: selectedOption.value, mccode:selectedOption.mccode, riskClassification:selectedOption.riskClassification}))
    }
  }

  useEffect(() => {
    const BWlists = getDefaultOption(BWlist)
    setBWlistsOption(BWlists)
  }, [BWlist])

  const getDefaultOption = (BWlist) => {
    const defaultOptions = []
    for (const item in BWlist) {
      defaultOptions.push({ label: BWlist[item].fieldValue, value: BWlist[item].fieldValue, mccode:BWlist[item].extraFields.mcc_code, riskClassification:BWlist[item].extraFields.risk_classification})
    }
    return defaultOptions
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  return (
    <>
      <span className="cursor-pointer"
        onClick={() => {
          setShow(true)
        }}
      >
        <i
          className="bi bi-pencil text-warning ms-2"
          onClick={() => {
            setShow(true)
          }}
        />
      </span>
      <Modal show={show} size="xl" centered onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: "rgb(126 126 219)" }}
          closeButton={() => clearPopup()}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            Update
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Website Working :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.websiteWorking || ""}
                  name="websiteWorking"
                >
                  <option value=''>Select</option>
                  <option value='Yes'>Yes</option>
                  <option value='No'>No</option>
                </select>
                {error && error.websiteWorking && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.websiteWorking}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  website Success Rate :
                </label>
                <input
                  placeholder="Website Success Rate"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="websiteSuccessRate"
                  autoComplete="off"
                  value={editFormData.websiteSuccessRate || ""}
                />
                {error && error.websiteSuccessRate && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.websiteSuccessRate}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Line Of Business :
                </label>
                <Select
                  styles={customStyles}
                  isMulti={false}
                  name='AppUserId'
                  className='select2'
                  classNamePrefix='select'
                  onChange={handleChangeBWlists}
                  options={BWlistsOption}
                  defaultValue={() => {
                    if (inlineData !== undefined) {
                      if (BWlistsOption.hasOwnProperty(inlineData)) {
                        let one = BWlistsOption && BWlistsOption.filter(b => b.value === inlineData);
                        return one[0]
                      }
                      else {
                        return {
                          value: inlineData,
                          label: inlineData,
                        }
                      }
                    }

                  }}
                  isDisabled={!BWlistsOption}
                />
                {error && error.changeInLineOfBusiness && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.changeInLineOfBusiness}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Mcc Code :
                </label>
                <input
                  className={clsx("form-control form-control-lg form-control-solid")}
                  disabled
                  type="text"
                  name="mccCode"
                  autoComplete="off"
                  value={editFormData.mccode || ""}
                />
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Risk Classification :
                </label>
                <input
                  className={clsx("form-control form-control-lg form-control-solid")}
                  disabled
                  type="text"
                  name="riskClassification"
                  autoComplete="off"
                  value={editFormData.riskClassification || ""}
                />
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Change In LegalName :
                </label>
                <input
                  placeholder="Change In LegalName"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="changeInLegalName"
                  autoComplete="off"
                  value={editFormData.changeInLegalName || ""}
                />
                {error && error.changeInLegalName && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.changeInLegalName}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Merchant Address :
                </label>
                <input
                  placeholder="Merchant Address"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="changeInMerchantAddress"
                  autoComplete="off"
                  value={editFormData.changeInMerchantAddress || ""}
                />
                {error && error.changeInMerchantAddress && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.changeInMerchantAddress}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Website Redirection :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.transactionLaunderingRedirection || ""}
                  name="transactionLaunderingRedirection"
                >
                  <option value=''>Select</option>
                  <option value='YES'>YES</option>
                  <option value='NO'>NO</option>
                </select>
                {error && error.transactionLaunderingRedirection && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.transactionLaunderingRedirection}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Page Loading Time :
                </label>
                <input
                  placeholder="Page Loading Time"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="increaseInFullPageLoadingTime"
                  autoComplete="off"
                  value={editFormData.increaseInFullPageLoadingTime || ""}
                />
                {error && error.increaseInFullPageLoadingTime && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.increaseInFullPageLoadingTime}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Parked Domain :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.parkedDomain || ""}
                  name="parkedDomain"
                >
                  <option value=''>Select</option>
                  <option value='true'>true</option>
                  <option value='false'>false</option>
                </select>
                {error && error.parkedDomain && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.parkedDomain}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Domain Risk :
                </label>
                <input
                  placeholder="Domain Risk"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="domainRisk"
                  autoComplete="off"
                  value={editFormData.domainRisk || ""}
                />
                {error && error.domainRisk && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.domainRisk}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className="col-md-4 col-lg-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Online Reputation Drop :
                </label>
                <input
                  placeholder="Online Reputation Drop"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="onlineReputationDrop"
                  autoComplete="off"
                  value={editFormData.onlineReputationDrop || ""}
                />
                {error && error.onlineReputationDrop && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.onlineReputationDrop}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Heavy Discounts :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.heavyDiscounts || ""}
                  name="heavyDiscounts"
                >
                  <option value=''>Select</option>
                  <option value='YES'>YES</option>
                  <option value='NO'>NO</option>
                </select>
                {error && error.heavyDiscounts && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.heavyDiscounts}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Non INR Pricing :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.nonInrPricing || ""}
                  name="nonInrPricing"
                >
                  <option value=''>Select</option>
                  <option value='Yes'>Yes</option>
                  <option value='No'>No</option>
                </select>
                {error && error.nonInrPricing && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.nonInrPricing}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Contact Details Phone :
                </label>
                <input
                  placeholder="Contact Details Phone"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="contactDetailsPhone"
                  autoComplete="off"
                  value={editFormData.contactDetailsPhone || ""}
                />
                {error && error.contactDetailsPhone && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.contactDetailsPhone}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Contact Details Email :
                </label>
                <input
                  placeholder="Contact Details Email"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="contactDetailsEmail"
                  autoComplete="off"
                  value={editFormData.contactDetailsEmail || ""}
                />
                {error && error.contactDetailsEmail && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.contactDetailsEmail}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Privacy Policy Page Url :
                </label>
                <input
                  placeholder="Privacy Policy PageUrl"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="privacyPolicyPageUrl"
                  autoComplete="off"
                  value={editFormData.privacyPolicyPageUrl || ""}
                />
                {error && error.privacyPolicyPageUrl && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.privacyPolicyPageUrl}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Shipping Policy Page Url :
                </label>
                <input
                  placeholder="Shipping Policy Page Url"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="shippingPolicyPageUrl"
                  autoComplete="off"
                  value={editFormData.shippingPolicyPageUrl || ""}
                />
                {error && error.shippingPolicyPageUrl && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.shippingPolicyPageUrl}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Return Policy Page Url :
                </label>
                <input
                  placeholder="Return Policy Page Url"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="returnPolicyPageUrl"
                  autoComplete="off"
                  value={editFormData.returnPolicyPageUrl || ""}
                />
                {error && error.returnPolicyPageUrl && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.returnPolicyPageUrl}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Terms And Condition Page Url :
                </label>
                <input
                  placeholder="Terms And Condition Page Url"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="termsAndConditionPageUrl"
                  autoComplete="off"
                  value={editFormData.termsAndConditionPageUrl || ""}
                />
                {error && error.termsAndConditionPageUrl && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.termsAndConditionPageUrl}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Contact Us Page Url :
                </label>
                <input
                  placeholder="Contact Us Page Url"
                  className={clsx("form-control form-control-lg form-control-solid")}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="contactUsPageUrl"
                  autoComplete="off"
                  value={editFormData.contactUsPageUrl || ""}
                />
                {error && error.contactUsPageUrl && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.contactUsPageUrl}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Valid Contact Phone :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.validPhone || ""}
                  name="validPhone"
                >
                  <option value=''>Select</option>
                  <option value='YES'>YES</option>
                  <option value='NO'>NO</option>
                </select>
                {error && error.validPhone && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.validPhone}
                  </div>
                )}
              </div>       
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                Valid Contact Email :
                </label>
                <select
                  className='form-select'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={editFormData.validEmail || ""}
                  name="validEmail"
                >
                  <option value=''>Select</option>
                  <option value='YES'>YES</option>
                  <option value='NO'>NO</option>
                </select>
                {error && error.validEmail && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.validEmail}
                  </div>
                )}
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs  font-weight-bold mb-2 form-label">
                  Malware :
                </label>
                <div className="col-lg-11">
                  <div className="form-check form-check-custom form-check-solid mt-4">
                    <label className='d-flex flex-stack mb-5 cursor-pointer'>
                      <span className='form-check form-check-custom form-check-solid me-2'>
                        <input
                          className='form-check-input'
                          type='radio'
                          onChange={(e) => handleChanges(e)}
                          value='true'
                          name='malwareDomain'
                          checked={editFormData.malwareDomain === 'true'}
                        />
                      </span>
                      <span className='d-flex flex-column'>
                        <span className='fs-7 text-muted'>
                          True
                        </span>
                      </span>
                    </label>
                    <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                      <span className='form-check form-check-custom form-check-solid me-2'>
                        <input
                          className='form-check-input'
                          type='radio'
                          onChange={(e) => handleChanges(e)}
                          value='false'
                          name='malwareDomain'
                          checked={editFormData.malwareDomain=== 'false'}
                        />
                      </span>
                      <span className='d-flex flex-column'>
                        <span className='fs-7 text-muted'>
                          False
                        </span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs  font-weight-bold mb-2 form-label">
                  Phishing Domain :
                </label>
                <div className="col-lg-11">
                  <div className="form-check form-check-custom form-check-solid mt-4">
                    <label className='d-flex flex-stack mb-5 cursor-pointer'>
                      <span className='form-check form-check-custom form-check-solid me-2'>
                        <input
                          className='form-check-input'
                          type='radio'
                          onChange={(e) => handleChanges(e)}
                          value='true'
                          name='phishingDomain'
                          checked={editFormData.phishingDomain === 'true'}
                        />
                      </span>
                      <span className='d-flex flex-column'>
                        <span className='fs-7 text-muted'>
                          True
                        </span>
                      </span>
                    </label>
                    <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                      <span className='form-check form-check-custom form-check-solid me-2'>
                        <input
                          className='form-check-input'
                          type='radio'
                          onChange={(e) => handleChanges(e)}
                          value='false'
                          name='phishingDomain'
                          checked={editFormData.phishingDomain=== 'false'}
                        />
                      </span>
                      <span className='d-flex flex-column'>
                        <span className='fs-7 text-muted'>
                          False
                        </span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
              <div className="col-md-4 col-lg-4 mb-6">
                <label className="font-size-xs  font-weight-bold mb-2 form-label">
                  Spamming :
                </label>
                <div className="col-lg-11">
                  <div className="form-check form-check-custom form-check-solid mt-4">
                    <label className='d-flex flex-stack mb-5 cursor-pointer'>
                      <span className='form-check form-check-custom form-check-solid me-2'>
                        <input
                          className='form-check-input'
                          type='radio'
                          onChange={(e) => handleChanges(e)}
                          value='true'
                          name='spammingDomain'
                          checked={editFormData.spammingDomain === 'true'}
                        />
                      </span>
                      <span className='d-flex flex-column'>
                        <span className='fs-7 text-muted'>
                          True
                        </span>
                      </span>
                    </label>
                    <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                      <span className='form-check form-check-custom form-check-solid me-2'>
                        <input
                          className='form-check-input'
                          type='radio'
                          onChange={(e) => handleChanges(e)}
                          value='false'
                          name='spammingDomain'
                          checked={editFormData.spammingDomain=== 'false'}
                        />
                      </span>
                      <span className='d-flex flex-column'>
                        <span className='fs-7 text-muted'>
                          False
                        </span>
                      </span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="d-flex justify-content-center">
                  <button
                    className="btn btn-light-primary m-1 mt-8 font-5vw"
                    onClick={handleUpdate}
                    disabled={loading}
                  >
                    {
                      loading ? 'Updating...' : "Update"
                    }
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { UpdateCurrentReportStore, BWlistStore } = state
  return {
    loading: state && state.UpdateCurrentReportStore && state.UpdateCurrentReportStore.loading,
    UpdateCurrentReportData:
      UpdateCurrentReportStore && UpdateCurrentReportStore.UpdateCurrentReportData
        ? UpdateCurrentReportStore.UpdateCurrentReportData
        : "",
    BWlistData:
      BWlistStore && BWlistStore.BWLists && BWlistStore.BWLists.data
        ? BWlistStore.BWLists.data
        : "",
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUpdateCurrentReportDispatch: (id, editFormData) => dispatch(UpdateCurrentReportActions.getUpdateCurrentReport(id, editFormData)),
  clearUpdateCurrentReportDispatch: () => dispatch(UpdateCurrentReportActions.clearUpdateCurrentReport()),
  getBWListDispatch: (params) => dispatch(BWListActions.getBWList(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Update)
