import React, { useEffect, useRef, useState } from 'react'
import { connect } from "react-redux"
import Modal from "react-bootstrap/Modal"
import clsx from "clsx"
import _ from 'lodash'
import { DROPZONE_MESSAGES, FILE_FORMAT_CB_DOCUMENT, STATUS_RESPONSE } from '../../utils/constants'
import { OgmForceUploadAction, OGMintervalActions, clientIdLIstActions } from '../../store/actions'
import { warningAlert, confirmationAlert, successAlert } from "../../utils/alerts"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"

const OgmForceUpload = (props) => {
  const {
    forceUploadDispatch,
    forceUploadResponse,
    forceUploadLoading,
    forceUploadDispatchClear,
    getOGMintervalDispatch,
    OGMintervalData,
    clientIdDispatch,
    clinetIdLists
  } = props
  const [show, setShow] = useState(false)
  const [searchFormData, setSearchFormData] = useState({
    batchId: '',
    interval: '',
    file: ''
  })
  const [error, setError] = useState({})
  const hiddenFileInput = useRef(null)
  const [fileName, setFileName] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [AsigneesOption, setAsignees] = useState()
  
  useEffect(() => {
    getOGMintervalDispatch()
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params)
  }, [])

  const handleClick = (event) => {
    hiddenFileInput.current.click(event)
  }

  const handleChanges = (e) => {
    setSearchFormData((searchFormData) => ({
      ...searchFormData,
      [e.target.name]: e.target.value,
    }))
    setError({ ...error, [e.target.name]: "" })
  }

  const handleSearch = () => {
    const error = {}
    if (_.isEmpty(searchFormData.clientId)) {
      error.clientId = 'Please Select ClientId.'
    }
    if (_.isEmpty(searchFormData.interval)) {
      error.interval = 'Please the Ogm interval.'
    }
    if (!_.isObject(searchFormData.file)) {
      error.file = 'Please upload the csv file.'
    }
    if (_.isEmpty(error)) {
      setShow(false)
      const data = new FormData()
      data.append("intervel", searchFormData.intervel)
      data.append("batchId", searchFormData.batchId)
      data.append("file", searchFormData.file)
      forceUploadDispatch(data)
    }
    setError(error)
  }

  const handleFileChange = (e) => {
    e.preventDefault()
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(FILE_FORMAT_CB_DOCUMENT, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setSearchFormData((values) => ({
          ...values,
          file: files,
        }))
        setError((values) => ({ ...values, file: "" }))
        setFileName(files && files.name)
      } else {
        setError({
          ...error,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
      }
    } else {
      setError({ ...error, [name]: DROPZONE_MESSAGES.CSV_INVALID })
    }
  }

  const clearPopup = () => {
    setSearchFormData({})
    setShow(false)
    setFileName()
    setSelectedAsigneesOption('')
  }

  useEffect(() => {
    if (forceUploadResponse && forceUploadResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        'success',
        "Assigned Successfully",
      )
      forceUploadDispatchClear()
      clearPopup()
    } else if (forceUploadResponse && forceUploadResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        forceUploadResponse && forceUploadResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      forceUploadDispatchClear()
    }
  }, [forceUploadResponse])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setSearchFormData(values => ({ ...values, clientId: selectedOption.value, }))
      setError({ ...error, clientId: "" })
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  return (
    <div>
      <button
        type="button"
        className="btn btn-secondary mr-1"
        onClick={() => {
          setShow(true)
        }}
      >
        Force Upload
      </button>
      <Modal show={show} size="lg" centered onHide={clearPopup}>
        <Modal.Header
          style={{ backgroundColor: "rgb(126 126 219)" }}
          closeButton={clearPopup}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            Force Upload
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Client :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name='AppUserId'
                      className='select2'
                      classNamePrefix='select'
                      handleChangeReactSelect={handleChangeAsignees}
                      options={AsigneesOption}
                      value={SelectedAsigneesOption}
                      isDisabled={!AsigneesOption}
                    />
                    {error && error.clientId && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.clientId}
                      </div>
                    )}
                  </div>
                </div>
            {/* <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Batch ID:
                </label>
              </div>
              <div className="col-md-8">
                <input
                  placeholder="Batch ID"
                  className={clsx(
                    "form-control form-control-lg form-control-solid",
                    { "is-invalid": searchFormData.batchId && error.batchId },
                    {
                      "is-valid": searchFormData.batchId && !error.batchId,
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type="text"
                  name="batchId"
                  autoComplete="off"
                  value={searchFormData.batchId || ""}
                />
                {error && error.batchId && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.batchId}
                  </div>
                )}
              </div>
            </div> */}
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Ogm Interval :
                </label>
              </div>
              <div className="col-md-8">
                <select
                  name='interval'
                  className='form-select form-select-solid'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={searchFormData.interval || ''}
                >
                  <option value=''>Select Interval...</option>
                  {OGMintervalData &&
                    OGMintervalData.data.length > 0 &&
                    OGMintervalData.data.map((item, i) => (
                      <option key={i} value={item.interval}>
                        {item.interval}
                      </option>
                    ))}
                </select>
                {error && error.interval && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.interval}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label required">
                  Upload Document :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  type="file"
                  className="d-none"
                  name="file"
                  id="file"
                  multiple={true}
                  ref={hiddenFileInput}
                  onChange={handleFileChange}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClick}
                >
                  <i className="bi bi-filetype-csv" />
                  Upload Document
                </button>
                {error && error.file && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.file}
                  </div>
                )}
                {fileName && fileName}
              </div>
            </div>
            <div className="row">
              <div className="col-md-4" />
              <div className="col-md-8">
                <button
                  className="btn btn-light-primary m-1 mt-8 font-5vw "
                  onClick={handleSearch}
                  disabled={forceUploadLoading}
                >
                  {forceUploadLoading ?
                    <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                      Please wait...
                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                    </span> : "Force Upload"}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { OgmForceUploadStore, OGMintervalStore, clinetListStore } = state
  return {
    forceUploadResponse: OgmForceUploadStore && OgmForceUploadStore.OgmForceUpload ? OgmForceUploadStore.OgmForceUpload : '',
    forceUploadLoading: OgmForceUploadStore && OgmForceUploadStore.loading ? OgmForceUploadStore.loading : false,
    OGMintervalData: OGMintervalStore && OGMintervalStore.OGMintervalData ? OGMintervalStore.OGMintervalData : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  forceUploadDispatch: (params) => dispatch(OgmForceUploadAction.OgmForceUpload(params)),
  forceUploadDispatchClear: (params) => dispatch(OgmForceUploadAction.clearOgmForceUpload(params)),
  getOGMintervalDispatch: (data) => dispatch(OGMintervalActions.getOGMinterval(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(OgmForceUpload)