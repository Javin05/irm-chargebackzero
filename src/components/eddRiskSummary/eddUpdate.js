import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Modal, Button, Form, Spinner } from "react-bootstrap";
import { EDDUpdateActions } from "../../store/actions";
import { successAlert, warningAlert } from "../../utils/alerts";
import { STATUS_RESPONSE } from "../../utils/constants";

function EddUpdate(props) {
  const {
    id,
    getEDDUpdateDispatch,
    clearEDDUpdateDispatch,
    EddUpdateResponse,
    loading,
    merchantSummary,
  } = props;

  const [show, setShow] = useState(false);
  const [error, setError] = useState("");
  const [formData, setFormData] = useState({
    website: "",
    pan_number: "",
    gstin_nummber: "",
    cin_number: "",
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = () => {
    if (
      !_.isEmpty(
        formData.website ||
          formData.pan_number ||
          formData.gstin_nummber ||
          formData.cin_number
      )
    ) {
      const params = {
        website: formData.website,
        pan_number: formData.pan_number,
        gstin_nummber: formData.gstin_nummber,
        cin_number: formData.cin_number,
      };
      getEDDUpdateDispatch(id, params);
    } else {
      setError("Please fill any one of the fields");
    }
  };

  const handleReset = () => {
    setFormData({
      website: "",
      pan_number: "",
      gstin_nummber: "",
      cin_number: "",
    });
    setError("");
    setShow(false);
  };

  useEffect(() => {
    if (
      EddUpdateResponse &&
      EddUpdateResponse.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      successAlert(EddUpdateResponse.message, "success");
      handleReset();
      clearEDDUpdateDispatch();
    } else if (
      EddUpdateResponse &&
      EddUpdateResponse.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        EddUpdateResponse.message,
        "",
        "Try again",
        "",
        () => {}
      );
      setError("");
      clearEDDUpdateDispatch();
    }
  }, [EddUpdateResponse]);

  useEffect(() => {
    if (merchantSummary) {
      setFormData({
        ...formData,
        website: merchantSummary && merchantSummary.website,
        pan_number: merchantSummary && merchantSummary.pan_number,
        gstin_nummber: merchantSummary && merchantSummary.gstin_nummber,
        cin_number: merchantSummary && merchantSummary.cin_number,
      });
    }
  }, [merchantSummary]);

  return (
    <>
      <Button variant="success" size="sm" onClick={() => setShow(true)}>
        Update
      </Button>
      <Modal
        show={show}
        onHide={() => setShow(false)}
        centered
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Update</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-lightBlue">
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Website:</Form.Label>
              <Form.Control
                type="text"
                name="website"
                placeholder="Website"
                value={formData.website}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>PAN:</Form.Label>
              <Form.Control
                type="text"
                name="pan_number"
                placeholder="PAN"
                value={formData.pan_number}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>GST:</Form.Label>
              <Form.Control
                type="text"
                name="gstin_nummber"
                placeholder="GSTIN"
                value={formData.gstin_nummber}
                onChange={handleChange}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>CIN:</Form.Label>
              <Form.Control
                type="text"
                name="cin_number"
                placeholder="CIN"
                value={formData.cin_number}
                onChange={handleChange}
              />
            </Form.Group>
            {error && <div className="text-danger mb-3">{error}</div>}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="danger"
            size="sm"
            onClick={handleReset}
            disabled={loading}
          >
            Cancel
          </Button>
          <Button
            variant="primary"
            size="sm"
            onClick={handleSubmit}
            disabled={loading}
          >
            {loading ? (
              <>
                Please wait...
                <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  className="ms-2"
                />
              </>
            ) : (
              "Submit"
            )}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

const mapStateToProps = (state) => {
  const { EddUpdateStore } = state;

  return {
    EddUpdateResponse:
      EddUpdateStore && EddUpdateStore.EddUpdateRes
        ? EddUpdateStore.EddUpdateRes
        : "",
    loading:
      EddUpdateStore && EddUpdateStore.loading ? EddUpdateStore.loading : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getEDDUpdateDispatch: (id, params) =>
    dispatch(EDDUpdateActions.getEDDUpdate(id, params)),
  clearEDDUpdateDispatch: () => dispatch(EDDUpdateActions.clearEDDUpdate()),
});

export default connect(mapStateToProps, mapDispatchToProps)(EddUpdate);
