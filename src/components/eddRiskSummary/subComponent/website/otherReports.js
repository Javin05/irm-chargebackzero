import React from "react";

const OtherReports = (props) => {
  const { matrixDetail } = props;
  return (
    <div className="row g-5 g-xl-8 mb-8">
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label fw-bolder text-dark fs-3">
                Business Address
              </span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Address</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.authorised_capital
                      ? matrixDetail.business_address.address
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date and time
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.datetime
                      ? matrixDetail.business_address.datetime
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Lattitude</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.lattitude
                      ? matrixDetail.business_address.lattitude
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Longitude</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.longitude
                      ? matrixDetail.business_address.longitude
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Name</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.name
                      ? matrixDetail.business_address.name
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Risk score
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.risk_score
                      ? matrixDetail.business_address.risk_score
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Status</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.business_address &&
                    matrixDetail.business_address.status
                      ? matrixDetail.business_address.status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label fw-bolder text-dark fs-3">
                Merchant Address
              </span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Address</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.address
                      ? matrixDetail.merchant_address.address
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date and time
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.datetime
                      ? matrixDetail.merchant_address.datetime
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Lattitude</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.lattitude
                      ? matrixDetail.merchant_address.lattitude
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Longitude</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.longitude
                      ? matrixDetail.merchant_address.longitude
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Name</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.name
                      ? matrixDetail.merchant_address.name
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Risk score
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.risk_score
                      ? matrixDetail.merchant_address.risk_score
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Status</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_address &&
                    matrixDetail.merchant_address.status
                      ? matrixDetail.merchant_address.status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label fw-bolder text-dark fs-3">CIN</span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Authorised capital
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.authorised_capital
                      ? matrixDetail.cin.authorised_capital
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Cin</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail && matrixDetail.cin && matrixDetail.cin.cin
                      ? matrixDetail.cin.cin
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Class of company
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.class_of_company
                      ? matrixDetail.cin.class_of_company
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Company category
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.company_category
                      ? matrixDetail.cin.company_category
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Company name
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.company_name
                      ? matrixDetail.cin.company_name
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Company status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.company_status
                      ? matrixDetail.cin.company_status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Company subcategory
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.company_subcategory
                      ? matrixDetail.cin.company_subcategory
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date of balance sheet
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.date_of_balance_sheet
                      ? matrixDetail.cin.date_of_balance_sheet
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date of incorporation
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.date_of_incorporation
                      ? matrixDetail.cin.date_of_incorporation
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date of last agm
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.date_of_last_agm
                      ? matrixDetail.cin.date_of_last_agm
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Directors</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.directors
                      ? matrixDetail.cin.directors.map(
                          (item) => item.name + ", "
                        )
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Paid up capital
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.paid_up_capital
                      ? matrixDetail.cin.paid_up_capital
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Registered address
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.registered_address
                      ? matrixDetail.cin.registered_address
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Whether listed or not
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.cin &&
                    matrixDetail.cin.whether_listed_or_not
                      ? matrixDetail.cin.whether_listed_or_not
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label fw-bolder text-dark fs-3">GST</span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Centre jurisdiction
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.centre_jurisdiction
                      ? matrixDetail.gst.centre_jurisdiction
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Constitution of business
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.constitution_of_business
                      ? matrixDetail.gst.constitution_of_business
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date of cancellation
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.date_of_cancellation
                      ? matrixDetail.gst.date_of_cancellation
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Date of registration
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.date_of_registration
                      ? matrixDetail.gst.date_of_registration
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Gstin</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail && matrixDetail.gst && matrixDetail.gst.gstin
                      ? matrixDetail.gst.gstin
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Gstin status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.gstin_status
                      ? matrixDetail.gst.gstin_status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Legal name
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.legal_name
                      ? matrixDetail.gst.legal_name
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Nature of business activity
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.nature_of_business_activity
                      ? matrixDetail.gst.nature_of_business_activity.map(
                          (item) => item + ", "
                        )
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Taxpayer type
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.taxpayer_type
                      ? matrixDetail.gst.taxpayer_type
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Trade name
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.gst &&
                    matrixDetail.gst.trade_name
                      ? matrixDetail.gst.trade_name
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label fw-bolder text-dark fs-3">PAN</span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Aadhaar seeding status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.pan &&
                    matrixDetail.pan.aadhaar_seeding_status
                      ? matrixDetail.pan.aadhaar_seeding_status.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">Dob match</span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.pan &&
                    matrixDetail.pan.dob_match
                      ? matrixDetail.pan.dob_match
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Name match
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.pan &&
                    matrixDetail.pan.name_match
                      ? matrixDetail.pan.name_match
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Pan status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.pan &&
                    matrixDetail.pan.pan_status
                      ? matrixDetail.pan.pan_status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column ">
              <span className="card-label fw-bolder text-dark fs-3">
                Merchant personal email
              </span>
            </h3>
          </div>
          <div className="card-body pt-0 mb-2">
            <div className="mt-4">
              <h4 className="d-flex justify-content-center ">
                <span className="card-label fw-bolder text-dark fs-3">
                  Black list Details
                </span>
              </h4>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Email
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.blacklistDetails
                      ? matrixDetail.merchant_personal_email.blacklistDetails.email
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Method
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.blacklistDetails
                      ? matrixDetail.merchant_personal_email.blacklistDetails.method
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.blacklistDetails
                      ? matrixDetail.merchant_personal_email.blacklistDetails.status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="mt-4">
              <h4 className="d-flex justify-content-center ">
                <span className="card-label fw-bolder text-dark fs-3">
                  Email Details
                </span>
              </h4>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Catch all
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.catch_all.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Common
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.common.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Deliverability
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.deliverability
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Disposable
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.disposable.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Dmarc record
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.dmarc_record.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Dns valid
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.dns_valid.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Domain trust
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.domain_trust
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Domain velocity
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.domain_velocity
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    First name
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.first_name
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Fraud score
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.fraud_score.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Frequent complainer
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.frequent_complainer.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Generic
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.generic.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Honeypot
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.honeypot.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Leaked
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.leaked.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Message
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.message.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Overall score
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.overall_score
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Recent abuse
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.recent_abuse.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Request id
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.request_id
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Sanitized email
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.sanitized_email.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                   Smtp score
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.smtp_score.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    User activity
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.user_activity.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Spam trap score
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.spam_trap_score
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Suggested domain
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_personal_email &&
                    matrixDetail.merchant_personal_email.emailDetails
                      ? matrixDetail.merchant_personal_email.emailDetails.suggested_domain
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4">
        <div className="card card-xl-stretch mb-xl-8">
          <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
            <h3 className="card-title align-items-start flex-column ">
              <span className="card-label fw-bolder text-dark fs-3">
                Merchant phone
              </span>
            </h3>
          </div>
          <div className="card-body pt-0 mb-2">
            <div className="mt-4">
              <h4 className="d-flex justify-content-center ">
                <span className="card-label fw-bolder text-dark fs-3">
                  Black list Details
                </span>
              </h4>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Phone
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.blacklistDetails
                      ? matrixDetail.merchant_phone.blacklistDetails.phone
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Method
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.blacklistDetails
                      ? matrixDetail.merchant_phone.blacklistDetails.method
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.blacklistDetails
                      ? matrixDetail.merchant_phone.blacklistDetails.status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="mt-4">
              <h4 className="d-flex justify-content-center ">
                <span className="card-label fw-bolder text-dark fs-3">
                Phone Details
                </span>
              </h4>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Accurate country code
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.accurate_country_code.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Active
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.active.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Active status
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.active_status
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Carrier
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.carrier.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    City
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.city.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Country
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.country.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Dialing code
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.dialing_code
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Leaked
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.leaked.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Message
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.message.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Sms domain
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.sms_domain
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Sms email
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.sms_email
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Request id
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.request_id
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    User activity
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.user_activity.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Spammer
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.spammer.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
            <div className="align-items-center rounded p-2 mb-0 mx-2">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-dark fw-bold mb-1 fs-5">
                    Success
                  </span>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6">
                  <span className="text-muted fw-semibold  text-gray-700 text-capital">
                    {matrixDetail &&
                    matrixDetail.merchant_phone &&
                    matrixDetail.merchant_phone.phoneDetails
                      ? matrixDetail.merchant_phone.phoneDetails.success.toString()
                      : "No Data"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OtherReports;
