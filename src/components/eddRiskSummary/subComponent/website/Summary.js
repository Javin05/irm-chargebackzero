import React, { Fragment } from 'react'
import _ from 'lodash'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import MerchantInfo from './MerchantInfo'

function Summarry(props) {
  const {
    websiteData,
    matrixDetail,
    isLoaded,
    successVerifyDomain,
    adminContact,
    adminContactData,
    domainInfoData,
    merchantIddetails,
    websitevalue,
    websiteLink,
    ValidData,
    DNSData,
    BsName,
    DashboardExportData,
  } = props

  const FilterWebsiteData = _.filter(websiteData, function(o) { 
    const data = [
      'Risk Score',
      'HTTP Status Code',
      'Age'
    ]
    return _.includes(data, o.title) ? null : o
  })

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                  Website
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              {
                !websiteData
                  ? (
                    <div>
                      <div className='text-center'>
                        <div className='spinner-border text-primary m-5' role='status' />
                      </div>
                    </div>
                  )
                  :
                  !_.isEmpty(FilterWebsiteData) ?
                    FilterWebsiteData && FilterWebsiteData.map((item, i) => {
                      return (
                        <Fragment key={"FIX_3" + i}>
                          {
                            item && item.status === 'positive'
                              ? (
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                      {
                                        !_.isEmpty(item.info) ? (
                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                            className='tooltip'
                                          >
                                            {item.info}
                                          </Tooltip>}
                                            placement={"right"}
                                          >
                                            <span>
                                              <i className="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                            </span>
                                          </OverlayTrigger>
                                        ) : (
                                          null
                                        )
                                      }
                                    </div>
                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                  </div>
                                  <span className='svg-icon svg-icon-1 svg-icon-success' title={item.message}>
                                    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                                      <rect opacity='0.3' x='2' y='2' width='20' height='20' rx='10' fill='black' />
                                      <path d='M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z' fill='black' />
                                    </svg>
                                  </span>
                                </div>
                              )
                              : (
                                <>
                                  {
                                    item && item.status === 'warning'
                                      ? (
                                        <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                          <div className='flex-grow-1 me-2'>
                                            <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                              {
                                                !_.isEmpty(item.info) ? (
                                                  <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                    className='tooltip'
                                                  >
                                                    {item.info}
                                                  </Tooltip>}
                                                    placement={"right"}
                                                  >
                                                    <span>
                                                      <i className="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                    </span>
                                                  </OverlayTrigger>
                                                ) : (
                                                  null
                                                )
                                              }
                                            </div>
                                            <span className='text-muted fw-bold d-block'>{item.value}</span>
                                          </div>
                                          <span className='fw-bolder text-warning py-1'>
                                            <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu' />
                                          </span>
                                        </div>
                                      )
                                      : (
                                        <>
                                          {
                                            item && item.status === 'negative'
                                              ? (
                                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                                  <div className='flex-grow-1 me-2'>
                                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                                      {
                                                        !_.isEmpty(item.info) ? (
                                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                            className='tooltip'
                                                          >
                                                            {item.info}
                                                          </Tooltip>}
                                                            placement={"right"}
                                                          >
                                                            <span>
                                                              <i className="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                            </span>
                                                          </OverlayTrigger>
                                                        ) : (
                                                          null
                                                        )
                                                      }
                                                    </div>
                                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                                  </div>
                                                  <span className='fw-bolder text-danger py-1' title={item.message}>
                                                    <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu' />
                                                  </span>
                                                </div>
                                              )
                                              : null
                                          }
                                        </>
                                      )
                                  }
                                </>
                              )
                          }
                        </Fragment>
                      )
                    })
                    : (
                      <span className='text-dark fw-semibold fs-5 ms-4'>
                        No Records Found
                      </span>
                    )
              }
            </div>
          </div>
        </div>
        <div className="col-lg-8">
          <MerchantInfo
            matrixDetail={matrixDetail}
            isLoaded={isLoaded}
            successVerifyDomain={successVerifyDomain}
            adminContact={adminContact}
            adminContactData={adminContactData}
            domainInfoData={domainInfoData}
            merchantIddetails={merchantIddetails}
            websitevalue={websitevalue}
            websiteLink={websiteLink}
            ValidData={ValidData}
            DNSData={DNSData}
            BsName={BsName}
            DashboardExportData={DashboardExportData}
          />
        </div>
      </div>
    </>
  )
}

export default Summarry