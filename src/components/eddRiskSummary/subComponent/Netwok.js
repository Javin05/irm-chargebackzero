import { useEffect, useState, Fragment } from 'react'
import Xarrow, { useXarrow } from 'react-xarrows'
import Icofont from 'react-icofont'
import '../index.css'
import { useLocation } from 'react-router-dom'
import { Scrollbars } from 'react-custom-scrollbars'
import _ from "lodash";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'
import { toAbsoluteUrl } from '../../../theme/helpers'

const MainColor = {
  "Phone": "#009EF7",
  "IP Address": "#F1416C",
  "Email": "#50CD89",
  "Website": "#7239EA",
  "Address": "#FFC700"
}
const SubColor = {
  "Phone": "#D3EFFF",
  "IP Address": "#F9DBE4",
  "Email": "#BFFDDC",
  "Website": "#E1D7F9",
  "Address": "#FFF0B6"
}

function NetworkGraph2(props) {
  const {
    LinkAnlyticslists,
    merchantIddetails
  } = props
  const [path, pathType] = useState("straight") 
  const [highlight, setHighlight] = useState(null)
  const HighlightHandler = (lighter) => {
    setHighlight(lighter)
  }
  useEffect(() => {
    window.dispatchEvent(new Event('resize'))
  })
  const merchant = merchantIddetails && merchantIddetails.data ? merchantIddetails.data : '--'
  
  return (

    <div className='row'>
      <div className='col-lg-12 col-md-12 col-sm-12'>
        <div className='some-page-wrapper' id="triggerdiv">
        <Scrollbars style={{ width: "100%", height: 600 }}>
          <div className='graph-row'>
            <div className='column'>
              <span className='primary'>IRM-{merchant && merchant.riskId ? merchant.riskId : '--'}</span>
              <div className="box main" id="main" style={{ backgroundColor: highlight !== null ? MainColor[highlight] : "#E1F0FF" }}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/profile.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>
            </div>
            <div className='column mid-column'>
              <span style={{ color: MainColor["Phone"] }}>Phone : {merchant && merchant.report && merchant.report.contactDetailsPhone  ? merchant.report.contactDetailsPhone : '--'}</span>
              <div className="box"style={{ backgroundColor: highlight === "Phone" ? MainColor["Phone"] : SubColor["Phone"] }} id="Phone" onClick={(e) => HighlightHandler("Phone")}
              >
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/telephone.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["IP Address"] }}>IP Address :  {merchant && merchant.report && merchant.report.ipAddressOfServer  ? merchant.report.ipAddressOfServer : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"] }} id="IP Address" onClick={(e) => HighlightHandler("IP Address")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/ip.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Email"] }}>Email : {merchant && merchant.report && merchant.report.contactDetailsEmail  ? merchant.report.contactDetailsEmail : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Email" ? MainColor["Email"] : SubColor["Email"] }} id="Email" onClick={(e) => HighlightHandler("Email")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/mail.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Website"] }}>Website : {merchant && merchant.report && merchant.report.webUrl  ? merchant.report.webUrl : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Website" ? MainColor["Website"] : SubColor["Website"] }} id="Website" onClick={(e) => HighlightHandler("Website")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/global-network.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <span style={{ color: MainColor["Address"] }}>Address : {merchant && merchant.report && merchant.report.businessAddress  ? merchant.report.businessAddress : '--'}</span>
              <div className="box" style={{ backgroundColor: highlight === "Address" ? MainColor["Address"] : SubColor["Address"] }} id="Address" onClick={(e) => HighlightHandler("Address")}>
                <img
                  alt='Logo'
                  src={toAbsoluteUrl('/media/imageIcons/house.png')}
                  className='cursor-pointer h-32px w-32px'
                  style={{width:'34px'}}
                />
              </div>

              <Xarrow start={'main'} end={'Phone'} path={path} color={highlight === "Phone" ? MainColor["Phone"] : SubColor["Phone"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Phone") }} />
              <Xarrow start={'main'} end={'IP Address'} path={path} color={highlight === "IP Address" ? MainColor["IP Address"] : SubColor["IP Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("IP Address") }} />
              <Xarrow start={'main'} end={'Email'} path={path} color={highlight === "Email" ? MainColor["Email"] : SubColor["Email"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Email") }} />
              <Xarrow start={'main'} end={'Website'} path={path} color={highlight === "Website" ? MainColor["Website"] : SubColor["Website"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Website") }} />
              <Xarrow start={'main'} end={'Address'} path={path} color={highlight === "Address" ? MainColor["Address"] : SubColor["Address"]} headSize={4} strokeWidth={2} passProps={{ onClick: (e) => HighlightHandler("Address") }} />
            </div>
            <div className='column mt-4'>

              {LinkAnlyticslists && LinkAnlyticslists?.data && LinkAnlyticslists?.data.linkData && LinkAnlyticslists.data.linkData.map((dummy, index) => {

                return (
                  <Fragment key={"Z_" + index}>
                    <a
                      className='color-primary cursor-pointer'
                      onClick={() => window.open(`/risk-summary/update/${dummy._id}`, "_blank")}
                      data-bs-toggle="tooltip"
                      data-bs-custom-classname="tooltip-inverse"
                      data-bs-placement="top"
                      title="Tooltip on top">
                      IRM{dummy.riskId}
                    </a>
                    <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                      className='tooltip'
                    >
                      Email - {dummy && dummy.personalEmail} <br />
                      Source - {dummy && dummy.source.map((name) => { return (<>{name}</>) })}
                    </Tooltip>}
                      placement={"right"}
                    >
                      <div
                        key={"X_" + index}
                        className="box"
                        id={"XYZ_" + index}
                        style={{
                          backgroundColor: 'rgb(237 184 23)'
                        }}
                      >
                        <span>
                          <Icofont
                            icon={"student-alt"}
                            style={{
                              color: 'black'
                            }}
                          />
                        </span>
                      </div>
                    </OverlayTrigger>
                    {
                      dummy && dummy.source.map((item, i) => {
                        return (
                          <div key={i}>
                            <Xarrow
                              key={"Y_" + index}
                              start={item}
                              end={"XYZ_" + index}
                              path={path}
                              color={highlight === item ? MainColor[item] : SubColor[item]}
                              headSize={4}
                              strokeWidth={2}
                              dashness={highlight === item ? true : false
                              } />
                          </div>
                        )
                      })
                    }
                  </Fragment>
                )
              })}
            </div>
          </div>
        </Scrollbars>
      </div>
      </div>

    </div>
  )
}

export default NetworkGraph2