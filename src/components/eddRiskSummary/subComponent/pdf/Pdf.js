import React from "react";
import { Page, Text, Image, Document, StyleSheet, View, Font } from "@react-pdf/renderer";
import MyCustomFont from './fonts/Anton-Regular.ttf';
import _ from 'lodash'
import moment from 'moment'
import { PMASTATUS, RiskPdfStatus } from '../../../../utils/constants'
import { toAbsoluteUrl } from '../../../../theme/helpers'

Font.register({
  family: 'AntonFamily',
  src: MyCustomFont
})
Font.registerEmojiSource({
  format: 'png',
  url: 'https://cdnjs.cloudflare.com/ajax/libs/twemoji/14.0.2/72x72/',
});

const styles = StyleSheet.create({
  page: {
    width: "205mm",
    marginLeft: "auto",
    marginRight: "auto",
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    padding: 20,
  },
  content: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  title: {
    fontSize: 24,
    textAlign: "center",
    fontFamily: "AntonFamily",
  },
  text: {
    margin: 12,
    fontSize: 14,
    textAlign: "justify",
    fontFamily: "AntonFamily",
  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
  },
  pageNumber: {
    position: "absolute",
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: "center",
    color: "grey",
    fontFamily: "AntonFamily",
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  logo: {
    width: '100px',
    height: '40px',
    objectFit: 'contain'
  },
  headerText: {
    color: '#153d58',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'start',
  },
  scanDate: {
    textAlign: "end",
    fontSize: "11px",
    color: '#153d58',
  },
  //table css
  table: {
    flexDirection: 'column',
    border: '1 solid #92bfde',
    borderRadius: 8,
  },
  tableRow: {
    flexDirection: 'row',
  },
  tableCellHeader: {
    flex: 1,
    padding: 5,
    fontSize: "10px",
    color: 'black',
    fontWeight: 'bolder',
  },
  tableCellData: {
    flex: 1,
    padding: 5,
    fontSize: "9px",
    color: 'black'

  },
  heading: {
    color: '#153d58',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'start',
    flex: 1,
  },
  //list
  row: {
    flexDirection: 'row',
    borderBottomColor: '#000',
    borderBottomStyle: 'solid',
    paddingVertical: 5,
  },
  cell: {
    width: '50%',
    padding: 5,
  },
  headerCell: {
    fontWeight: 'bold',
  },
  tableCellHeaderStatus: {
    flex: 1,
    padding: 5,
    fontSize: "10px",
    color: 'black',
    fontWeight: 'bold',
    width: '30%',
  },
  tableCellImage: {
    width: 50,
    height: 50,
    marginRight: 220,
  },
  tableHead: {
    fontSize: "10px",
  },
  flexTable: {
    flex: 0.5
  },
  headerBgColor: {
    backgroundColor: "#e4f4f5"
  },
  alignCenter: {
    textAlign: 'center'
  },
  //screenshot image
  screenshotimage: {
    width: '550px',
    height: '750px',
    paddingBottom: '8px',
    objectFit: 'contain'
  },
})

const PDFFile = (props) => {

  const { DashboardExportData, merchantIddetails, DashboardPmaLists, websiteData, domainRepetation, RiskScoreWeightAge, exportLists, playStoreData, merchantSummary } = props
  const filteredPeople = websiteData && websiteData.filter((item) => item.title !== "Risk Score");
  const ipAddress = websiteData && websiteData.filter((item) => item.title === 'Ip Address');
  const MyCustomIcon = ({ status }) => {
    const icon = status === "positive" ? '✅' : '❌'
    return <Text>{icon}</Text>
  };
  const businessRisk = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Business Risk')
  const webContentRisk = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Web Content Risk')
  const transparencyRisk = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Transparency Risk')
  const websiteHealth = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Website Health')
  const websiteContentCheckupDomainHealth = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Website Content Checkup Domain Health')
  const onlineWebsiteReputation = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Online Website Reputation')
  const webSecurity = RiskScoreWeightAge && RiskScoreWeightAge.filter((item) => item.groupName === 'Web Security')

  return (
    <Document>
      <Page size={{ width: 600, height: 800 }} style={styles.page}>
        <View style={styles.section}>
          <View style={styles.header}>
            <Text style={styles.scanDate}>WRM Reports</Text>
            {DashboardExportData && DashboardExportData.logo === "No" ? null : DashboardExportData && DashboardExportData.logo === "No Data" ? null : (
              <Text>
                <Image src={DashboardExportData.logo} style={styles.logo} />
              </Text>
            )}
            <Text style={styles.scanDate}>
              Scan Date: {merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt
                ? moment(merchantIddetails.data.createdAt).format('DD/MM/YYYY')
                : 'No Data'}
            </Text>
          </View>
          <View style={{ borderBottomWidth: 1, borderBottomColor: 'black', marginBottom: 20, marginTop: 20 }} />
          <View style={{ marginTop: 30 }} />
          <View style={{ ...styles.table }}>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Website</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.website
                  ? merchantIddetails.data.website
                  : 'No Data'}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Queue</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.queueId && merchantIddetails.data.queueId.queueName
                  ? merchantIddetails.data.queueId.queueName
                  : 'No Data'}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Tag</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.tag
                  ? merchantIddetails.data.tag
                  : 'No Data'}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Date Received</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt
                  ? moment(merchantIddetails.data.createdAt).format('DD/MM/YYYY')
                  : 'No Data'}
              </Text>
            </View>
          </View>
          <View style={{ marginTop: 20 }} />
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              WRM Inputs
            </Text>
          </View>
          <View style={{ marginTop: 20 }} />
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Inputs</Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>Url</Text>
              <Text style={styles.tableCellData}>
                {merchantIddetails && merchantIddetails.data && merchantIddetails.data.website ? merchantIddetails.data.website : 'No inputs passed'}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>Legal Name</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.legalNameInput === "No Data" ? 'No inputs passed' : DashboardExportData.legalNameInput}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={styles.tableCellHeader}>City</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.city ? DashboardExportData.city : 'No inputs passed'}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={styles.tableCellHeader}>MCC Code</Text>
              <Text style={styles.tableCellData}>
                {DashboardExportData && DashboardExportData.mccCodeInput === "No Data" ? 'No inputs passed' : DashboardExportData.mccCodeInput}
              </Text>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              mFilterIt Risk Categorization
            </Text>
          </View>
          <View style={{ marginTop: 20 }} />
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Risk Score from mFilterIt</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Risk Categorization from mFilterIt</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.riskScore ? DashboardExportData.riskScore : 'No Data'}
                </Text>
                <Text style={{ ...RiskPdfStatus[merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus ? merchantIddetails.data.report.riskScoreCategorizationStatus : "--"] }}>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus ? merchantIddetails.data.report.riskScoreCategorizationStatus : '--'}</Text>
                <Text style={{ ...styles.tableCellData }}>
                  {DashboardExportData && DashboardExportData.remarks ? DashboardExportData.remarks : '--'}</Text>
              </View>
            </View>
          </View>
          <View style={{ marginTop: 20 }} />
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Risk Categorization from mFilterIt </Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Threshold for marking Risk score from mFilterIt</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>
                  <span style={{ color: "green" }}>LOW</span>
                </Text>
                <Text style={styles.tableCellData}>Risk score between 0% to 33%</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>
                  <span style={{ color: "orange" }}>MEDIUM</span>
                </Text>
                <Text style={styles.tableCellData}>Risk score between 34% to 65%</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>
                  <span style={{ color: "red" }}>HIGH</span>
                </Text>
                <Text style={styles.tableCellData}>Risk score above 65%</Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 800 }} style={styles.page}>
        <View style={styles.section}>
          <View style={{ marginTop: 10 }} />
          <Text style={{ ...styles.heading, textDecoration: 'underline' }}>Parameter Check</Text>
          <View style={{ marginTop: 30 }} />
          <View style={{ ...styles.table }}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outcome</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            {DashboardPmaLists && DashboardPmaLists.data && DashboardPmaLists.data.pma && (
              Object.keys(DashboardPmaLists.data.pma).map((parameter, index) => (
                <View style={{
                  flexDirection: 'row',
                  // borderBottomWidth: 1,
                  backgroundColor: index % 2 === 0 ? '#F5F5F5' : '#FFFFFF',
                  // paddingVertical: 5,
                }} key={index} >
                  <Text style={styles.tableCellHeader}>{parameter}</Text>
                  <Text style={styles.tableCellData}>{DashboardPmaLists.data.pma[parameter]}</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    <MyCustomIcon status={DashboardPmaLists.data.pma1[parameter]} />
                  </Text>
                </View>
              ))
            )}
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 900 }} style={styles.page}>
        <View style={styles.section}>
          <View style={{ marginTop: 20 }} />
          <View>
            <Text style={{ ...styles.heading, fontSize: '15px', fontWeight: '500', textDecoration: 'underline' }}>
              Merchant Summary
            </Text>
            <View style={{ marginTop: 30 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Website
            </Text>
          </View>
          <View style={{ marginTop: 20 }} />
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Risk Score from mFilterIt</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.riskScore ? DashboardExportData.riskScore : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus === "HIGH" ? '❌' : '✅'}</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Risk Categorization from mFilterIt</Text>
                <Text style={styles.tableCellHeader}>
                  <Text style={{ ...RiskPdfStatus[merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus ? merchantIddetails.data.report.riskScoreCategorizationStatus : "--"] }}>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus ? merchantIddetails.data.report.riskScoreCategorizationStatus : '--'}</Text>
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus === "LOW" ? '✅' : merchantIddetails.data.report.riskScoreCategorizationStatus === "MEDIUM" ? '✅' : '❌'}</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>**<span style={{ color: "green" }}>LOW</span> - Risk score between 0% to 33%</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}><span style={{ color: "orange" }}>MEDIUM</span> - Risk score between 34% to 65%</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}><span style={{ color: "red" }}>HIGH</span> - Risk score above 65%</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>{DashboardExportData && DashboardExportData.webUrl === "No Data" ? '❌' : '✅'}</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>MCC as per mFilterIt screening</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>{DashboardExportData && DashboardExportData.productCategoryCode === "No Data" ? '❌' : '✅'}</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>MCC Match</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.mccCodeInput === "No Data" ? "No inputs passed" : DashboardExportData.mccCodeMatch}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.mccCodeMatch === "Yes" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Nature of Business</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.reason ? DashboardExportData.reason : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.mccCodeMatch === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Marketplace</Text>
                <Text style={styles.tableCellHeader}>
                  {playStoreData && playStoreData[0] && playStoreData[0].marketplace ? playStoreData[0].marketplace : "No"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {playStoreData && playStoreData[0] && playStoreData[0].marketplace === "Yes" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>MCC not part of High Brand Risk as per card scheme</Text>
                <Text style={styles.tableCellHeader}>
                  Yes
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>✅</Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Working</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData.websiteWorking ? DashboardExportData.websiteWorking : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.websiteWorking === "Yes" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Website does not prompt for PII data request</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.purchaseOrRegistration ? DashboardExportData.purchaseOrRegistration : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.purchaseOrRegistration === "Yes" || "Does not ask sensitive information" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ fontSize: '8px', color: '#153d58', fontStyle: 'italic', padding: 5 }}>**PII data refers to Net Banking/ Card/ any other payment data. It also cheks if PAN/Aadhar/other Identity numbers are asked</Text>
              </View>
            </View>
          </View>
          <View style={styles.section}>
            <View style={{ marginTop: 20 }} />
            <View>
              <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
                Risk Score Weightages
              </Text>
              <View style={{ marginTop: 20 }} />
            </View>
            <View style={styles.table}>
              <View style={[styles.row, styles.headerBgColor]}>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
              </View>
              <View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Risk Score as per mFilterIt</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData.riskScore ? DashboardExportData.riskScore : 'No Data'}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus === "HIGH" ? '❌' : '✅'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Risk Categorization from mFilterIt</Text>
                  <Text style={styles.tableCellHeader}>
                    <Text style={{ ...RiskPdfStatus[merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus ? merchantIddetails.data.report.riskScoreCategorizationStatus : "--"] }}>{merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus ? merchantIddetails.data.report.riskScoreCategorizationStatus : '--'}</Text>
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {merchantIddetails && merchantIddetails.data && merchantIddetails.data.report && merchantIddetails.data.report.riskScoreCategorizationStatus === "HIGH" ? '❌' : '✅'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                  <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                    <View style={{ marginTop: 5 }} />
                    <span style={{ fontSize: '8px', marginLeft: '3px' }}>**<span style={{ color: "green" }}>LOW</span> - Risk score between 0% to 33%</span>
                  </Text>
                  <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                  <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                    <span style={{ fontSize: '8px', marginLeft: '3px' }}><span style={{ color: "orange" }}>MEDIUM</span> - Risk score between 34% to 65%</span>
                  </Text>
                  <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                  <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                    <span style={{ fontSize: '8px', marginLeft: '3px' }}><span style={{ color: "red" }}>HIGH</span> - Risk score above 65%</span>
                  </Text>
                  <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
                </View>
                <View style={[styles.row, styles.headerBgColor]}>
                  <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Risk Score Group</Text>
                  <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable, styles.alignCenter]}>{"Risk score from \n mFilterIt"}</Text>
                  <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable, styles.alignCenter]}>{"Calculated Risk Score based on Group \n *Parameter Weightage"}</Text>
                  <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable, styles.alignCenter]}>{"Default Group \n Weightage"}</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Business Risk</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.businessRisk ? DashboardExportData.businessRisk : 'No Data'}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateBusinessRisk}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{businessRisk && businessRisk[0].weightage_percentage}%</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Web Content Risk</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.webContentRisk ? DashboardExportData.webContentRisk : 'No Data'}</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateWebContentRisk}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{webContentRisk && webContentRisk[0].weightage_percentage}%</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Transparency Risk</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.transparencyRisk ? DashboardExportData.transparencyRisk : 'No Data'}</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateTransparencyRisk}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{transparencyRisk && transparencyRisk[0].weightage_percentage}%</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Website Health</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.websiteHealth ? DashboardExportData.websiteHealth : 'No Data'}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateWebsiteHealth}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{websiteHealth && websiteHealth[0].weightage_percentage}%</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Website Content Checkup Domain Health</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.websiteContentCheckupDomainHealth ? DashboardExportData.websiteContentCheckupDomainHealth : 'No Data'}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateWebsiteContentCheckupDomainHealth}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{websiteContentCheckupDomainHealth && websiteContentCheckupDomainHealth[0].weightage_percentage}%</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Online Website Reputation</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.onlineWebsiteReputation ? DashboardExportData.onlineWebsiteReputation : 'No Data'}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateOnlineWebsiteReputation}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{onlineWebsiteReputation && onlineWebsiteReputation[0].weightage_percentage}%</Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Web Security</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>
                    {DashboardExportData && DashboardExportData.webSecurity ? DashboardExportData.webSecurity : 'No Data'}</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{exportLists && exportLists.calculateWebSecurity}%</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5, textAlign: 'center' }}>{webSecurity && webSecurity[0].weightage_percentage}%</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 1000 }} style={styles.page}>
        <View style={{ marginTop: 20 }}>
          <Text style={{ ...styles.heading, fontSize: '15px', fontWeight: '500', textDecoration: 'underline' }}>
            Summary
          </Text>
          <View style={{ marginTop: 30 }} />
          <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
            Domain Summary
          </Text>
        </View>
        <View style={{ marginTop: 20 }} />
        <View style={styles.table}>
          <View style={[styles.row, styles.headerBgColor]}>
            <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>WRM Validation</Text>
            <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outcome</Text>
            <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
          </View>
          {
            Array.isArray(filteredPeople) ? (
              filteredPeople && filteredPeople.map((item, index) => (
                <View key={index} style={{
                  flexDirection: 'row',
                  backgroundColor: index % 2 === 0 ? '#F5F5F5' : '#FFFFFF',
                  paddingVertical: 5
                }}>
                  <Text style={styles.tableCellHeader}>{item.title === "Threshold" ? null : item.title}</Text>
                  <Text style={item.title === "Threshold" ? { ...styles.tableCellHeader, fontSize: '8px' } : styles.tableCellHeader}>{item.value}</Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>{item.status === "positive" ? '✅' : item.status === "negative" ? '❌' : null}</Text>
                </View>
              ))
            ) : 'No Records Found'}
        </View>
      </Page>
      <Page size={{ width: 600, height: 850 }} style={styles.page}>
        <View style={styles.section}>
          <View style={{ marginTop: 20 }} />
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Contact Details
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Email Id</Text>
                <Text style={styles.tableCellHeader}>
                  {
                    Array.isArray(DashboardExportData && DashboardExportData.contactDetailsEmail) ?
                      DashboardExportData && DashboardExportData.contactDetailsEmail.map((email) => {
                        return (
                          <>{email}<br />
                          </>
                        )
                      })
                      : DashboardExportData && DashboardExportData.contactDetailsEmail
                  }
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {
                    Array.isArray(DashboardExportData && DashboardExportData.contactDetailsEmail) ?
                      DashboardExportData && DashboardExportData.contactDetailsEmail.map((email) => {
                        return (
                          <>{email === 'No Data' ? '❌' : '✅'}<br />
                          </>
                        )
                      })
                      : DashboardExportData && DashboardExportData.contactDetailsEmail === 'No Data' ? '❌' : '✅'
                  }
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Legal Name</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Telephone</Text>
                <Text style={styles.tableCellHeader}>
                  {Array.isArray(DashboardExportData && DashboardExportData.contactDetailsPhone) ?
                    DashboardExportData && DashboardExportData.contactDetailsPhone.map((Phone) => {
                      return (
                        <>{Phone}<br />
                        </>
                      )
                    })
                    : DashboardExportData && DashboardExportData.contactDetailsPhone}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {Array.isArray(DashboardExportData && DashboardExportData.contactDetailsPhone) ?
                    DashboardExportData && DashboardExportData.contactDetailsPhone.map((Phone) => {
                      return (
                        <>{Phone === 'No Data' ? '❌' : '✅'}<br />
                        </>
                      )
                    })
                    : DashboardExportData && DashboardExportData.contactDetailsPhone === 'No Data' ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Address Scrapped From Website</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.addressScrappedFromWebsite ? DashboardExportData.addressScrappedFromWebsite : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.addressScrappedFromWebsite === 'No Data' ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Address From Google Map</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.addressFromGoogleMap ? DashboardExportData.addressFromGoogleMap : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.addressFromGoogleMap === "Address exists" ? '✅' : DashboardExportData.addressFromGoogleMap === "valid" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Business Location Match(city)</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.cityMatch ? DashboardExportData.cityMatch : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.cityMatch === "Yes" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Only International Address Available</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.onlyInternationalAddressAvailable ? DashboardExportData.onlyInternationalAddressAvailable : 'No'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.onlyInternationalAddressAvailable === "Yes" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Both International & Domestic Address Available</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.bothInternationalDomesticAddressAvailable ? DashboardExportData.bothInternationalDomesticAddressAvailable : 'No'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.bothInternationalDomesticAddressAvailable === "Yes" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>International Address Scrapped from Website</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.internationalAddressScrappedFromWebsite ? DashboardExportData.internationalAddressScrappedFromWebsite : 'No'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.internationalAddressScrappedFromWebsite === "Yes" ? '❌' : '✅'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Merchant Policy Links
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Terms & Condition</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.termsAndConditionPageUrl === "No Data" ? 'No Data' : DashboardExportData.termsAndConditionPageUrl}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Shipping Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.shippingPolicyPageUrl === "No Data" ? 'No Data' : DashboardExportData.shippingPolicyPageUrl}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Return Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.returnPolicyPageUrl === "No Data" ? 'No Data' : DashboardExportData.returnPolicyPageUrl}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Contact Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.contactUsPageUrl === "No Data" ? 'No Data' : DashboardExportData.contactUsPageUrl}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Privacy Policy</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.privacyPolicyPageUrl === "No Data" ? 'No Data' : DashboardExportData.privacyPolicyPageUrl}
                </Text>
              </View>
              {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
                <>
                  <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                    <Text style={styles.tableCellHeader}>About Us</Text>
                    <Text style={styles.tableCellHeader}>
                      {DashboardExportData && DashboardExportData.aboutUsPageUrl === "No Data" ? 'No Data' : DashboardExportData.aboutUsPageUrl}
                    </Text>
                  </View>
                  <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                    <Text style={styles.tableCellHeader}>Cancellation Policy</Text>
                    <Text style={styles.tableCellHeader}>
                      {DashboardExportData && DashboardExportData.cancellationPolicyPageUrl === "No Data" ? 'No Data' : DashboardExportData.cancellationPolicyPageUrl}
                    </Text>
                  </View>
                  <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                    <Text style={styles.tableCellHeader}>Refund Policy</Text>
                    <Text style={styles.tableCellHeader}>
                      {DashboardExportData && DashboardExportData.refundPolicyPageUrl === "No Data" ? 'No Data' : DashboardExportData.refundPolicyPageUrl}
                    </Text>
                  </View>
                </>}
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 850 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Merchant's Policies Screenshots
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={[styles.tableCellHeader, styles.flexTable]}>Terms & Condition
              </Text>
              <Text style={styles.tableCellHeader} className="text-break">
                {DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.termsAndConditionPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.termsAndConditionStatus === "No Data" ? 'No Data' : DashboardExportData.termsAndConditionStatus],
              }}>{DashboardExportData && DashboardExportData.termsAndConditionStatus === "No Data" ? 'No Data' : DashboardExportData.termsAndConditionStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={[styles.tableCellHeader, styles.flexTable]}>Privacy Policy
              </Text>
              <Text style={styles.tableCellHeader} className="text-break">
                {DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.privacyPolicyPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.privacyPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.privacyPolicyStatus],
              }}>{DashboardExportData && DashboardExportData.privacyPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.privacyPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={[styles.tableCellHeader, styles.flexTable]}>Shipping Policy
              </Text>
              <Text style={styles.tableCellHeader} className="text-break">
                {DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.shippingPolicyPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.shippingPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.shippingPolicyStatus],
              }}>{DashboardExportData && DashboardExportData.shippingPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.shippingPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
              <Text style={[styles.tableCellHeader, styles.flexTable]}>Return Policy
              </Text>
              <Text style={styles.tableCellHeader} className="text-break">
                {DashboardExportData && DashboardExportData.returnPolicyPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.returnPolicyPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.returnPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.returnPolicyStatus],
              }}>{DashboardExportData && DashboardExportData.returnPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.returnPolicyStatus}
              </Text>
            </View>
            <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
              <Text style={[styles.tableCellHeader, styles.flexTable]}>Contact Policy
              </Text>
              <Text style={styles.tableCellHeader} className="text-break">
                {DashboardExportData && DashboardExportData.contactUsPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.contactUsPageScreenshot}
              </Text>
              <Text style={{
                ...RiskPdfStatus[DashboardExportData && DashboardExportData.policyComplianceStatusContactUsPage === "Yes" ? 'accessible' : DashboardExportData.policyComplianceStatusContactUsPage === "No" || "No Data" ? "inaccessible" : "No Data"],
              }}>{DashboardExportData && DashboardExportData.policyComplianceStatusContactUsPage === "Yes" ? 'accessible' : DashboardExportData.policyComplianceStatusContactUsPage === "No" ? "inaccessible" : "No Data"}
              </Text>
            </View>
            {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
              <>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={[styles.tableCellHeader, styles.flexTable]}>About Us
                  </Text>
                  <Text style={styles.tableCellHeader} className="text-break">
                    {DashboardExportData && DashboardExportData.aboutUsPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.aboutUsPageScreenshot}
                  </Text>
                  <Text style={{
                    ...RiskPdfStatus[DashboardExportData && DashboardExportData.aboutUsStatus === "No Data" ? 'No Data' : DashboardExportData.aboutUsStatus],
                  }}>{DashboardExportData && DashboardExportData.aboutUsStatus === "No Data" ? 'No Data' : DashboardExportData.aboutUsStatus}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={[styles.tableCellHeader, styles.flexTable]}>Cancellation Policy
                  </Text>
                  <Text style={styles.tableCellHeader} className="text-break">
                    {DashboardExportData && DashboardExportData.cancellationPolicyPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.cancellationPolicyPageScreenshot}
                  </Text>
                  <Text style={{
                    ...RiskPdfStatus[DashboardExportData && DashboardExportData.cancellationPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.cancellationPolicyStatus],
                  }}>{DashboardExportData && DashboardExportData.cancellationPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.cancellationPolicyStatus}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={[styles.tableCellHeader, styles.flexTable]}>Refund Policy
                  </Text>
                  <Text style={styles.tableCellHeader} className="text-break">
                    {DashboardExportData && DashboardExportData.refundPolicyPageScreenshot === "No Data" ? 'No Data' : DashboardExportData.refundPolicyPageScreenshot}
                  </Text>
                  <Text style={{
                    ...RiskPdfStatus[DashboardExportData && DashboardExportData.refundPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.refundPolicyStatus],
                  }}>{DashboardExportData && DashboardExportData.refundPolicyStatus === "No Data" ? 'No Data' : DashboardExportData.refundPolicyStatus}
                  </Text>
                </View>
              </>}
          </View>
        </View>
        {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
          <View style={styles.section}>
            <View style={{ marginTop: 20 }}>
              <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
                T&C Checks With Policies
              </Text>
              <View style={{ marginTop: 20 }} />
            </View>
            <View style={styles.table}>
              <View style={[styles.row, styles.headerBgColor]}>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
              </View>
              <View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Drop Shipping Model</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.dropShippingWebsite === "No Data" ? 'No Data' : DashboardExportData && DashboardExportData.dropShippingWebsite}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.dropShippingWebsite === "No" ? '✅' : '❌'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={{ ...styles.tableCellHeader, fontSize: '8px', marginLeft: '3px' }}>**Checks for keywords like ‘shipped directly from the manufacturer’ or ‘international warehouse’. If the delivery timeline is ‘in weeks’, it is flagged as Yes</Text>
                  <Text style={styles.tableCellHeader}></Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Security Keywords In Privacy Page</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.securityKeywordsInPrivacyPage === "No Data" ? 'No Data' : DashboardExportData.securityKeywordsInPrivacyPage}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.securityKeywordsInPrivacyPage === "Yes" ? '✅' : '❌'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Company Name In Terms Page</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.companyNameInTermsPage === "No Data" ? 'No Data' : DashboardExportData.companyNameInTermsPage}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.companyNameInTermsPage === "Yes" ? '✅' : '❌'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>International Law Keywords In Terms Page</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.internationalLawKeywordsInTermsPage ? DashboardExportData.internationalLawKeywordsInTermsPage : 'No'}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.internationalLawKeywordsInTermsPage === "Yes" ? '❌' : '✅'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Delivery Timeline Available</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.deliveryTimelineShipping === "No Data" ? 'No Data' : DashboardExportData.deliveryTimelineShipping}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.deliveryTimelineShipping === "Yes" ? '✅' : '❌'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Delivery Timeline</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.deliveryTimelineShippingKeyword === "No Data" ? 'No Data' : DashboardExportData.deliveryTimelineShippingKeyword}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.deliveryTimelineShipping === "Yes" ? '✅' : '❌'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Cod Keywords In Shipping Page</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.codKeywordsInShippingPage === "No Data" ? 'No Data' : DashboardExportData.codKeywordsInShippingPage}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.codKeywordsInShippingPage === "No" ? '❌' : '✅'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>International Keywords In Shipping Page</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.internationalKeywordsInShippingPage === "No Data" ? 'No Data' : DashboardExportData.internationalKeywordsInShippingPage}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.internationalKeywordsInShippingPage === "No" ? '✅' : '❌'}
                  </Text>
                </View>
              </View>
            </View>
          </View>}
      </Page>
      <Page size={{ width: 600, height: 850 }} style={styles.page}>
        <Text style={{ marginTop: 20, marginBottom: 20, width: '100%', textAlign: 'center', textDecoration: 'underline' }} className='text-dark fw-semibold text-dark-700'>
          Additional Information
        </Text>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Website Details
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Legal Name Scrapped</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Legal Name Match</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped === "No Data" ? "No inputs passed" : DashboardExportData.legalNameMatch}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.legalNameScrapped === "PARTIAL MATCH" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Legal Name Match Percentage</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.legalNameMatchPercentage ? DashboardExportData.legalNameMatchPercentage : 'No Data'}%
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.legalNameMatchPercentage === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>**0 to 50% Medium Risk.</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>50 to 100% Low Risk.</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader }}>Logo</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.logo === "No" ?
                    <Image
                      src={toAbsoluteUrl('/media/avatars/No_Image_Available.jpg')}
                      style={{ ...styles.tableCellImage }}
                      onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                    /> :
                    DashboardExportData && DashboardExportData.logo === "No Data" ?
                      <Image
                        src={toAbsoluteUrl('/media/avatars/No_Image_Available.jpg')}
                        style={{ ...styles.tableCellImage }}
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      /> :
                      <Image
                        src={DashboardExportData.logo}
                        style={{ ...styles.tableCellImage }}
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                      />}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.logo === "No" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, flex: 0.65 }}>Description</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.description ? DashboardExportData.description : 'No Data'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Website Content Review
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Adult Content Available</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.adultContentMonitoring ? DashboardExportData.adultContentMonitoring : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.adultContentMonitoring === "No" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Banned Category Description</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.bannedCategoryDescription ? DashboardExportData.bannedCategoryDescription : 'No'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.bannedCategoryDescription !== "No" || "No Banned Category" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Google review</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.merchantIntelligence ? DashboardExportData.merchantIntelligence.split(' ')[0] : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.merchantIntelligence.split(' ')[0] === "Yes" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Google rating</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.merchantIntelligenceRating ? DashboardExportData.merchantIntelligenceRating : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.merchantIntelligenceRating >= 2.5 ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>**Ratings Below 2.5 - Bad Reputation.</span>
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>Ratings Above 2.6 - Good Reputation.</span>
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>If the number of reviews is less than 25, it will be considered as No Google Rating </span>
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Website Is Accessible Without Login Prompt
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Is Accessible Without Login Prompt</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt === "Yes" ? '✅' : '❌'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page size={{ width: 600, height: 1010 }} style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, fontSize: '15px', fontWeight: '500', textDecoration: 'underline' }}>
              Web Analysis Status
            </Text>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 30 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Page Health Check
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Content Accessibility</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageHealthCheckContentAccessibilty ? DashboardExportData.pageHealthCheckContentAccessibilty : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.pageHealthCheckContentAccessibilty === "Yes" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Page loading Time</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageHealthCheckPageLoadingTime ? DashboardExportData.pageHealthCheckPageLoadingTime : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.PageLoadingTimeRisk === "High Risk" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Page loading Time Risk</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.PageLoadingTimeRisk ? DashboardExportData.PageLoadingTimeRisk : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.PageLoadingTimeRisk === "High Risk" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>0-5000 ms: No Risk</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>5001-8000ms: Medium Risk</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>Above 8000 ms: High Risk</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Any Untrusted Downloads?</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.pageActivityCheckUntrustedDownloads ? DashboardExportData.pageActivityCheckUntrustedDownloads : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.pageActivityCheckUntrustedDownloads === "No" ? '✅' : '❌'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Website  Redirection Check
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Website Redirection</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.websiteRedirection === "No" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Website Redirection Url</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirectionURL : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.websiteRedirection === "No" ? '✅' : '❌'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Pricing Details
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Currency</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.currenciesFoundOnWebsite ? DashboardExportData.currenciesFoundOnWebsite : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.currenciesFoundOnWebsite && DashboardExportData.currenciesFoundOnWebsite.length > 1 ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Unreasonable Price</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteContainsUnreasonablePrice ? DashboardExportData.websiteContainsUnreasonablePrice : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.websiteContainsUnreasonablePrice === "No" ? '✅' : '❌'}
                </Text>
              </View>
              {/* <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Accessibility</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt === "Yes" ? '✅' : '❌'}
                </Text>
              </View> */}
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Min Price Listed</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.minPriceListedInHomePage ? DashboardExportData.minPriceListedInHomePage : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.minPriceListedInHomePage === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Max Price Listed</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.maxPriceListedInHomePage ? DashboardExportData.maxPriceListedInHomePage : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.maxPriceListedInHomePage === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Product Pricing URL From Home Page</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.productPricingHomePageUrl ? DashboardExportData.productPricingHomePageUrl : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.productPricingHomePageUrl === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Product Pricing URL From Product Page</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.productPricingProductPageUrl ? DashboardExportData.productPricingProductPageUrl : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.productPricingProductPageUrl === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View style={{ marginTop: 20 }}>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Malware Risk
            </Text>
            <View style={{ marginTop: 20 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Malware Risk</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.malwareRisk ? DashboardExportData.malwareRisk : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.malwareRisk === "Low" ? '✅' : '❌'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Malware Present</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.malwarePresent ? DashboardExportData.malwarePresent : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.malwarePresent === "No" ? '✅' : '❌'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              IP Details
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Ip Address</Text>
                <Text style={styles.tableCellHeader}>
                  {ipAddress && ipAddress[0] && ipAddress[0].value ? ipAddress[0].value : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {ipAddress && ipAddress[0] && ipAddress[0].value === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Ip Location</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.ipLocation ? DashboardExportData.ipLocation : 'No Data'}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.ipLocation === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Shared Ip Address</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.dnsHistoryIp && DashboardExportData.dnsHistoryIp.length > 100 ? DashboardExportData.dnsHistoryIp.substring(0, 100) + '...' : DashboardExportData.dnsHistoryIp}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.dnsHistoryIp === "No" || " " ? '✅' : '❌'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
          <View style={styles.section}>
            <View style={{ marginTop: 20 }}>
              <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
                mFilterIt Negative Database Check
              </Text>
              <View style={{ marginTop: 20 }} />
            </View>
            <View style={styles.table}>
              <View style={[styles.row, styles.headerBgColor]}>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
                <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
              </View>
              <View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Listed in Negative Database</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.linked_to_rejected_cases === "No Data" ? 'No' : DashboardExportData.linked_to_rejected_cases}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.linked_to_rejected_cases === "No" ? '✅' : '❌'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                  <Text style={styles.tableCellHeader}>Number of accounts linked</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.no_accounts_linked === "No Data" ? 'No' : DashboardExportData.no_accounts_linked}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.no_accounts_linked === 0 ? '❌' : '✅'}
                  </Text>
                </View>
                <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                  <Text style={styles.tableCellHeader}>Linked case ID</Text>
                  <Text style={styles.tableCellHeader}>
                    {DashboardExportData && DashboardExportData.linked_cases === "No Data" ? 'No' : DashboardExportData.linked_cases}
                  </Text>
                  <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                    {DashboardExportData && DashboardExportData.linked_cases === "No" ? '✅' : '❌'}
                  </Text>
                </View>
              </View>
            </View>
          </View>}
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={{ ...styles.heading, fontSize: '15px', fontWeight: '500', textDecoration: 'underline' }}>
              Website Content Set-up
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Domain Key Details
            </Text>
          </View>
          <View style={{ marginTop: 20 }} />
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Registration Company</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainRegistrationCompany === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Registrar Risk</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRegistrarRisk ? DashboardExportData.domainRegistrarRisk : "No"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainRegistrarRisk === "Yes" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Rank</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.domainRank ? DashboardExportData.domainRank : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainRank === "0" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>*No Rank - High Risk</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>1 - 1000 - Medium Risk</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}></Text>
                <Text style={{ ...styles.tableCellHeader, padding: '0px 5px' }}>
                  <span style={{ fontSize: '8px', marginLeft: '3px' }}>Above 1000 - No Risk</span>
                </Text>
                <Text style={{ ...styles.tableCellHeader, flex: 0.5, padding: '0px 5px' }}></Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Registered Date</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainRegistrationDate) ? DashboardExportData.domainRegistrationDate : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainRegistrationDate === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Expiry Date</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainRegistrationExpiryDate) ? DashboardExportData.domainRegistrationExpiryDate : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainRegistrationExpiryDate === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#FFFFFF' }}>
                <Text style={styles.tableCellHeader}>Domain Active For</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainActivefor) ? DashboardExportData.domainActivefor+" days" : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainActivefor === "No Data" ? '❌' : '✅'}
                </Text>
              </View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>Domain Expiring In</Text>
                <Text style={styles.tableCellHeader}>
                  {!_.isEmpty(DashboardExportData.domainExpiringin) ? DashboardExportData.domainExpiringin+" days" : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData && DashboardExportData.domainExpiringin < 0 ? '❌' : '✅'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.section}>
          <View>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              SSL Check
            </Text>
            <View style={{ marginTop: 10, marginBottom: 10 }} />
          </View>
          <View style={styles.table}>
            <View style={[styles.row, styles.headerBgColor]}>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Parameter</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead]}>Outputs</Text>
              <Text style={[styles.tableCellData, styles.heading, styles.tableHead, styles.flexTable]}>Remarks</Text>
            </View>
            <View>
              <View style={{ ...styles.tableRow, backgroundColor: '#F5F5F5' }}>
                <Text style={styles.tableCellHeader}>SSL Certificate Check</Text>
                <Text style={styles.tableCellHeader}>
                  {DashboardExportData && DashboardExportData.sslCertificateCheck ? DashboardExportData.sslCertificateCheck : "No Data"}
                </Text>
                <Text style={{ ...styles.tableCellData, flex: 0.5 }}>
                  {DashboardExportData.sslCertificateCheck === "No" ? '❌' : '✅'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <Text style={{ ...styles.heading, textDecoration: 'underline' }}>Terms and Conditions</Text>
          <View style={{ marginTop: 10, marginBottom: 20 }} />
          {DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot === "No" ?
            <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
              Terms and Conditions Page Not Found
            </Text> :
            DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Terms and Conditions Page Not Found
              </Text> :
              !_.isEmpty(DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot) ? (
                <Image src={DashboardExportData && DashboardExportData.termsAndConditionPageScreenshot} style={styles.screenshotimage} />
              ) : (
                <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Terms and Conditions Page Not Found
                </Text>
              )}
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Privacy Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Privacy Policy Page Not Found
              </Text> :
              DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot === "No" ?
                <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Privacy Policy Page Not Found
                </Text> :
                !_.isEmpty(DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot) ? (
                  <Image src={DashboardExportData && DashboardExportData.privacyPolicyPageScreenshot} style={styles.screenshotimage} />
                ) : (
                  <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Privacy Policy Page Not Found
                  </Text>
                )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Shipping Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Shipping Policy Page Not Found
              </Text> :
              DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot === "No" ?
                <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Shipping Policy Page Not Found
                </Text> :
                !_.isEmpty(DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot) ? (
                  <Image src={DashboardExportData && DashboardExportData.shippingPolicyPageScreenshot} style={styles.screenshotimage} />
                ) : (
                  <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Shipping Policy Page Not Found
                  </Text>
                )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Returns Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {DashboardExportData && DashboardExportData.returnPolicyPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Returns Policy Page Not Found
              </Text> :
              DashboardExportData && DashboardExportData.returnPolicyPageScreenshot === "No" ?
                <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Returns Policy Page Not Found
                </Text> :
                !_.isEmpty(DashboardExportData && DashboardExportData.returnPolicyPageScreenshot) ? (
                  <Image src={DashboardExportData && DashboardExportData.returnPolicyPageScreenshot} style={styles.screenshotimage} />
                ) : (
                  <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Returns Policy Page Not Found
                  </Text>
                )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Contact Policy
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {DashboardExportData && DashboardExportData.contactUsPageScreenshot === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Contact Policy Page Not Found
              </Text> :
              DashboardExportData && DashboardExportData.contactUsPageScreenshot === "No" ?
                <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Contact Policy Page Not Found
                </Text> :
                !_.isEmpty(DashboardExportData && DashboardExportData.contactUsPageScreenshot) ? (
                  <Image src={DashboardExportData && DashboardExportData.contactUsPageScreenshot} style={styles.screenshotimage} />
                ) : (
                  <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Contact Policy Page Not Found
                  </Text>
                )}
          </View>
        </View>
      </Page>
      {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
        <>
          <Page style={styles.page}>
            <View style={styles.section}>
              <View>
                <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
                  About Us
                </Text>
                <View style={{ marginTop: 10, marginBottom: 20 }} />
                {DashboardExportData && DashboardExportData.aboutUsPageScreenshot === "No Data" ?
                  <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    About Us Page Not Found
                  </Text> :
                  DashboardExportData && DashboardExportData.aboutUsPageScreenshot === "No" ?
                    <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                      About Us Page Not Found
                    </Text> :
                    !_.isEmpty(DashboardExportData && DashboardExportData.aboutUsPageScreenshot) ? (
                      <Image src={DashboardExportData && DashboardExportData.aboutUsPageScreenshot} style={styles.screenshotimage} />
                    ) : (
                      <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                        About Us Page Not Found
                      </Text>
                    )}
              </View>
            </View>
          </Page>
          <Page style={styles.page}>
            <View style={styles.section}>
              <View>
                <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
                  Cancellation Policy
                </Text>
                <View style={{ marginTop: 10, marginBottom: 20 }} />
                {DashboardExportData && DashboardExportData.cancellationPolicyPageScreenshot === "No Data" ?
                  <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Cancellation Policy Page Not Found
                  </Text> :
                  DashboardExportData && DashboardExportData.cancellationPolicyPageScreenshot === "No" ?
                    <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                      Cancellation Policy Page Not Found
                    </Text> :
                    !_.isEmpty(DashboardExportData && DashboardExportData.cancellationPolicyPageScreenshot) ? (
                      <Image src={DashboardExportData && DashboardExportData.cancellationPolicyPageScreenshot} style={styles.screenshotimage} />
                    ) : (
                      <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                        Cancellation Policy Page Not Found
                      </Text>
                    )}
              </View>
            </View>
          </Page>
          <Page style={styles.page}>
            <View style={styles.section}>
              <View>
                <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
                  Refund Policy
                </Text>
                <View style={{ marginTop: 10, marginBottom: 20 }} />
                {DashboardExportData && DashboardExportData.refundPolicyPageScreenshot === "No Data" ?
                  <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Refund Policy Page Not Found
                  </Text> :
                  DashboardExportData && DashboardExportData.refundPolicyPageScreenshot === "No" ?
                    <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                      Refund Policy Page Not Found
                    </Text> :
                    !_.isEmpty(DashboardExportData && DashboardExportData.refundPolicyPageScreenshot) ? (
                      <Image src={DashboardExportData && DashboardExportData.refundPolicyPageScreenshot} style={styles.screenshotimage} />
                    ) : (
                      <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                        Refund Policy Page Not Found
                      </Text>
                    )}
              </View>
            </View>
          </Page>
        </>}
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Product Pricing URL From Home Page
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {DashboardExportData && DashboardExportData.productPricingHomePageUrl === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Product Pricing URL From Home Page Not Found
              </Text> :
              DashboardExportData && DashboardExportData.productPricingHomePageUrl === "No" ?
                <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Product Pricing URL From Home Page Not Found
                </Text> :
                !_.isEmpty(DashboardExportData && DashboardExportData.productPricingHomePageUrl) ? (
                  <Image src={DashboardExportData && DashboardExportData.productPricingHomePageUrl} style={styles.screenshotimage} />
                ) : (
                  <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Product Pricing URL From Home Page Not Found
                  </Text>
                )}
          </View>
        </View>
      </Page>
      <Page style={styles.page}>
        <View style={styles.section}>
          <View>
            <Text style={{ ...styles.heading, textDecoration: 'underline' }}>
              Product Pricing URL From Product Page
            </Text>
            <View style={{ marginTop: 10, marginBottom: 20 }} />
            {DashboardExportData && DashboardExportData.productPricingProductPageUrl === "No Data" ?
              <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                Product Pricing URL From Product Page Not Found
              </Text> :
              DashboardExportData && DashboardExportData.productPricingProductPageUrl === "No" ?
                <Text className='text-muted text-center fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                  Product Pricing URL From Product Page Not Found
                </Text> :
                !_.isEmpty(DashboardExportData && DashboardExportData.productPricingProductPageUrl) ? (
                  <Image src={DashboardExportData && DashboardExportData.productPricingProductPageUrl} style={styles.screenshotimage} />
                ) : (
                  <Text className='text-muted fw-semibold text-gray-700' style={{ fontSize: '10px' }}>
                    Product Pricing URL From Product Page Not Found
                  </Text>
                )}
          </View>
        </View>
      </Page>
    </Document>
  );
};

export default PDFFile;