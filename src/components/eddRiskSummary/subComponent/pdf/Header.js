import './pdf.css';
import { jsPDF } from "jspdf";
import { renderToString } from "react-dom/server"
import { toAbsoluteUrl } from '../../../../theme/helpers'
import { forwardRef, Fragment } from "react";
import { connect } from 'react-redux'
import _ from 'lodash'
import { RISKSTATUS } from '../../../../utils/constants'
import { Link } from "react-router-dom"
import moment from 'moment'
import { Page, Text, View, Document, StyleSheet, Image, Font } from '@react-pdf/renderer';
import Header from './Header';
import MyCustomFont from './fonts/Anton-Regular.ttf';

// Font.register({
//   family: 'Oswald',
//   src: 'https://fonts.gstatic.com/s/oswald/v13/Y_TKV6o8WovbUd3m_X9aAA.ttf'
// });
Font.register({
  family: 'AntonFamily',
  src: MyCustomFont
})

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
  },
  header: {
    fontSize: 12,
    marginBottom: 20,
    textAlign: 'center',
    color: 'grey',
  },
  pageNumber: {
    position: 'absolute',
    fontSize: 12,
    bottom: 30,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'grey',
  },
  container: {
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: '#112131',
    borderBottomStyle: 'solid',
    alignItems: 'stretch',
  },
  detailColumn: {
    flexDirection: 'column',
    flexGrow: 9,
    textTransform: 'uppercase',
  },
  linkColumn: {
    flexDirection: 'column',
    flexGrow: 2,
    alignSelf: 'flex-end',
    justifySelf: 'flex-end',
    marginTop: 10,
  },
  name: {
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 15
  },
  subtitle: {
    // fontSize: 10,
    justifySelf: 'flex-end',
    width: '100px',
  },
  link: {
    fontSize: 10,
    color: 'black',
    textDecoration: 'none',
    alignSelf: 'flex-end',
    justifySelf: 'flex-end',
  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
  },
  headertabel: {
    marginTop: 5,
    fontSize: 15,
    marginBottom: 5,
  },
  scorecard: {
    marginTop: 5,
    fontSize: 20,
    marginBottom: 5,
  },
  table: {
    display: "table",
    width: "auto",
    borderStyle: "solid",
    borderWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0
  },
  tableRow: {
    margin: "auto",
    flexDirection: "row"
  },
  tableCol: {
    width: "25%",
    borderStyle: "solid",
    borderWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0
  },
  tableCell: {
    margin: "auto",
    marginTop: 5,
    fontSize: 10
  },


  table1: {
    display: "table",
    width: "auto",
    borderStyle: "solid",
    borderWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0
  },
  tableRow1: {
    margin: "auto",
    flexDirection: "row"
  },
  tableCol1: {
    width: "50%",
    borderStyle: "solid",
    borderWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0
  },
  locationtablhead: {
    margin: "auto",
    marginTop: 12,
    fontSize: 15,
    fontWeight: 'bold'
  },
  locationtablkeyhead: {
    margin: "auto",
    fontSize: 12,
    fontWeight: 'bold'
  },

})

function PDF(props, ref) {
  const {
    websiteData,
    successVerifyDomain,
    DNSData,
    matrixDetail,
    adminContact,
    adminContactData,
    BsName,
    websitetobusinessmatch,
    websiteCatgories,
    ValidData,
    webAnalysis,
    LogoCheck,
    websiteImageDetect,
    userInteractionChecks,
    domainInfoData,
    domainRepetation,
    registerData,
    successWhoDomain,
    reviewAnalysis,
    websiteLink,
    DashboardExportData, 
    merchantIddetails
  } = props


  const websitedataCategory = ValidData && ValidData.websiteCategorizationV1 &&
    ValidData.websiteCategorizationV1.data && ValidData.websiteCategorizationV1.data.classification ? ValidData.websiteCategorizationV1.data.classification : 'No Data'
  const webData = webAnalysis && webAnalysis.data ? webAnalysis.data : 'No Data'

  const policy = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ?
    webAnalysis.data.policy_complaince_checks : 'No Data'

  const screenShot = webAnalysis && webAnalysis.data && webAnalysis.data.policy_complaince_checks ?
    webAnalysis.data.policy_complaince_checks : 'No Data'

  const Email = merchantIddetails && merchantIddetails.data && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo.email ? merchantIddetails.data.assignedTo.email : 'No Data'

  return (

    <Document>
      <Page style={styles.body}>
        <View style={styles.container}>
          <View style={styles.detailColumn}>
            <Image
              style={styles.subtitle}
              src={LogoCheck && LogoCheck.original_url ? LogoCheck.original_url : LogoCheck && LogoCheck.irmLogo}
              onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
            />
            <Text style={styles.name}>WRM Merchant Scorecard</Text>
          </View>
          <View style={styles.linkColumn}>
            <Text style={styles.link}>
              Scan Date :
              {
                moment(merchantIddetails && merchantIddetails.data && merchantIddetails.data.createdAt ? merchantIddetails.data.createdAt : 'No Data').format('DD/MM/YYYY')
              }</Text>
          </View>
        </View>
        <View>
          {/* <Text style={styles.headertabel}>
            Sponsor : {adminContact.name ? adminContact.name : adminContactData.name ? adminContactData.name : BsName}
          </Text> */}
          <Text style={styles.headertabel}>
            Site : {websiteLink ? websiteLink : 'No Data'}
          </Text>
          <Text style={styles.headertabel}>
            Site : {
              Array.isArray(DNSData && DNSData.dnsRecords) ? (
                DNSData && DNSData.dnsRecords.map((item) => {
                  return (
                    <>
                      {
                        item.dnsType === "A" ? (
                          <>
                            {
                              item.address
                            },
                          </>
                        ) : ''
                      }
                    </>
                  )
                })
              ) : 'No Data'
            }
          </Text>
          <Text style={styles.headertabel}>
            Email Address : {Email}
          </Text>
          <Text style={styles.headertabel}>
            Phone : {merchantIddetails && merchantIddetails.data && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo && merchantIddetails.data.assignedTo.mobile ? merchantIddetails.data.assignedTo.mobile : 'No Data'}
          </Text>
        </View>
        <View style={styles.container}></View>
        <View>
          <Text style={styles.scorecard}>
            ScoreCard
          </Text>
        </View>
        <View style={styles.table}>
          <View style={styles.tableRow}>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>#</Text>
            </View>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>WCM Validation</Text>
            </View>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>Comments</Text>
            </View>
            <View style={styles.tableCol}>
              <Text style={styles.tableCell}>Risk</Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            {
              Array.isArray(websiteData) ?
                websiteData && websiteData.map((item, i) => {
                  return (
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>{i + 1}</Text>
                      <Text style={styles.tableCell}>{item.title}</Text>
                      <Text style={styles.tableCell}>{item.value}</Text>
                      <Text className={`${RISKSTATUS[item.status]}`} style={styles.tableCell}>{item.status ? item.status : "--"}</Text>
                    </View>
                  )
                })
                : (
                  <Text style={styles.tableCell}>No Records Found</Text>
                )
            }
          </View>
        </View>
        <View>
          <Text style={styles.scorecard}>
            Location
          </Text>
        </View>
        <View style={styles.table1}>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablhead}>Code</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablhead}>Description</Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Business Address</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>{successVerifyDomain && successVerifyDomain.location ? successVerifyDomain.location : 'No Data'}</Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Ip Address</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>{
                Array.isArray(DNSData && DNSData.dnsRecords) ? (
                  DNSData && DNSData.dnsRecords.map((item) => {
                    return (
                      <>
                        {
                          item.dnsType === "A" ? (
                            <Text>
                              {
                                item.address
                              },
                            </Text>
                          ) : ''
                        }
                      </>
                    )
                  })
                ) : 'No Data'
              }
              </Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Lat</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>
                {
                  matrixDetail && matrixDetail.businessAddressLocation.lat ? matrixDetail.businessAddressLocation.lat : 'No Data'
                }
              </Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Lng</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>
                {
                  matrixDetail && matrixDetail.businessAddressLocation.long ? matrixDetail.businessAddressLocation.long : 'No Data'
                }
              </Text>
            </View>
          </View>
        </View>


        <View>
          <Text style={styles.scorecard}>
            Personal Details
          </Text>
        </View>
        <View style={styles.table1}>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablhead}>Code</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablhead}>Description</Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Contact Details Email</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>
                {
                  adminContact &&
                    adminContact.email ?
                    adminContact.email : adminContactData && adminContactData.email ? adminContactData.email
                      : 'No Data'
                }
              </Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Legal Name</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>
              {
                  adminContact &&
                    adminContact.name ?
                    adminContact.name : adminContactData && adminContactData.name ? adminContactData.name : BsName
                }
              </Text>
            </View>
          </View>
          <View style={styles.tableRow1}>
            <View style={styles.tableCol1}>
              <Text style={styles.locationtablkeyhead}>Telephone</Text>
            </View>
            <View style={styles.tableCol1}>
              <Text style={styles.tableCell}>
              {
                  adminContact &&
                    adminContact.telephone ?
                    adminContact.telephone :
                    adminContactData &&
                      adminContactData.telephone ? adminContactData.telephone : 'No Data'
                }
              </Text>
            </View>
          </View>
        </View>

      </Page>
    </Document>
  )
}

export default PDF