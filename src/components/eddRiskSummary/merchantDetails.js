import { useEffect, useState } from "react";
import { connect } from "react-redux";
import "react-circular-progressbar/dist/styles.css";
import moment from "moment";
import { useLocation } from "react-router-dom";
import {
  merchantIdDetailsActions,
  GetAsigneeActions,
  AsiggnActions,
  eddHistoryListActions,
} from "../../store/actions";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";
import { STATUS_RESPONSE } from "../../utils/constants";
import _ from "lodash";
import Modal from "react-bootstrap/Modal";
import EddUpdate from "./eddUpdate";

function MerchantDetails(props) {
  const {
    setOpenPhone,
    setOpenEmail,
    merchantSummary,
    getAssignee,
    updateAssignDispatch,
    updateAssign,
    clearAsiggnDispatch,
    getEddHistoryListDispatch,
    clearEddHistoryListDispatch,
    EddHistoryListResponse,
    EddHistoryListloading,
  } = props;

  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("/");
  const currentId = url && url[3];
  const [AsigneesOption, setAsignees] = useState();
  const [formData, setFormData] = useState({
    assignedTo: "",
  });
  const [value, setValue] = useState();
  const [eddUpdateValue, setEddUpdateValue] = useState(false);
  const [eddHistoryShow, setEddHistoryShow] = useState(false);
  const StatusArray = {
    PENDING: "badge-light-warning",
    APPROVED: "badge-light-success",
    REJECTED: "badge-light-danger",
    HOLD: "badge-light-warning",
    "MANUAL REVIEW": "badge badge-orange text-black",
    ACTIVE: "badge-light-success",
    INACTIVE: "badge-light-danger",
  };

  const AsigneesNames =
    getAssignee && getAssignee.data && getAssignee.data.result;
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames);
    setAsignees(Asignees);
  }, [AsigneesNames]);

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = [];
    for (const item in AsigneesNames) {
      defaultOptions.push({
        label: AsigneesNames[item].firstName,
        value: AsigneesNames[item]._id,
      });
    }
    return defaultOptions;
  };

  const onConfirmupdate = () => {
    updateAssignDispatch(currentId, formData);
  };

  const Assign = () => {
    confirmationAlert(
      "Do You Want To",
      `Assign this Case #${
        merchantSummary && merchantSummary.riskId
      } to ${value}?`,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmupdate();
      },
      () => {}
    );
  };

  useEffect(() => {
    if (updateAssign && updateAssign.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert("Assigned Successfully", "success");
      clearAsiggnDispatch();
      setFormData({
        assignedTo: "",
      });
    } else if (
      updateAssign &&
      updateAssign.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        updateAssign.message,
        "",
        "Try again",
        "",
        () => {}
      );
      clearAsiggnDispatch();
    }
  }, [updateAssign]);

  useEffect(() => {
    return (
      setEddUpdateValue(true),
      setTimeout(() => {
        setEddUpdateValue(false);
      }, 1500)
    );
  }, []);

  useEffect(() => {
    if (
      EddHistoryListResponse &&
      EddHistoryListResponse.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      setEddHistoryShow(true);
    } else if (
      EddHistoryListResponse &&
      EddHistoryListResponse.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        EddHistoryListResponse && EddHistoryListResponse.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      );
    }
  }, [EddHistoryListResponse]);

  const onEddLogHistory = () => {
    getEddHistoryListDispatch(currentId);
  };

  const clear = () => {
    setEddHistoryShow(false);
    clearEddHistoryListDispatch();
  };

  return (
    <>
      <Modal show={eddHistoryShow} size="xl" centered onHide={() => clear()}>
        <Modal.Header
          style={{
            backgroundColor: "rgb(94, 98, 120)",
          }}
          closeButton={() => clear()}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            Change Log
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="table-responsive">
            <div className="scroll h-650px">
              <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                <thead>
                  <tr>
                    <th className="fs-6 fw-bold text-center">Name</th>
                    <th className="fs-6 fw-bold text-center">Previous Value</th>
                    <th className="fs-6 fw-bold text-center">Current Value</th>
                    <th className="fs-6 fw-bold text-center">Type</th>
                    <th className="fs-6 fw-bold text-center">Status</th>
                    <th className="fs-6 fw-bold text-center">Created At</th>
                    <th className="fs-6 fw-bold text-center">Updated By</th>
                    <th className="fs-6 fw-bold text-center w-150px">
                      Updated On
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {Array.isArray(
                    EddHistoryListResponse && EddHistoryListResponse.data
                  ) ? (
                    EddHistoryListResponse &&
                    EddHistoryListResponse.data.map((item, i) => {
                      return (
                        <tr
                          style={{
                            flexDirection: "row",
                            backgroundColor:
                              i % 2 === 0 ? "#EBFCFF" : "#FAFAFA",
                            paddingVertical: 5,
                          }}
                        >
                          <td className="text-center">
                            {item && item.input_name ? item.input_name : "--"}
                          </td>
                          <td className="text-center">
                            {item && item.previous_value ? item.previous_value : "--"}
                          </td>
                          <td className="text-center">
                            {item && item.values ? item.values : "--"}
                          </td>
                          <td className="text-center">
                            {item && item.input_type ? item.input_type : "--"}
                          </td>
                          <td className="text-center">
                            <span
                              className={`badge ml-2 ${
                                item && item.status
                                  ? StatusArray[item && item.status]
                                  : ""
                              }`}
                              style={{
                                padding: "4px 12px",
                                marginLeft: "10px",
                                fontSize: "14px",
                              }}
                            >
                              {item && item.status ? item.status : ""}
                            </span>
                          </td>
                          <td className="text-center">
                            {item && item.createdAt
                              ? moment(item.createdAt).format(
                                  "DD/MM/YYYY HH:mm:ss"
                                )
                              : "--"}
                          </td>
                          <td className="text-center">
                            {/* <div>{item && item.updatedUserName || item.updatedUserEmail ? `${item.updatedUserName} (${item.updatedUserEmail})` : '--'}</div> */}
                            <div>
                              {item && item.user_name_and_mail_id ? item.user_name_and_mail_id : "--"}
                            </div>
                          </td>
                          <td className="text-center">
                            {" "}
                            {item && item.updatedAt
                              ? moment(item.updatedAt).format(
                                  "DD/MM/YYYY HH:mm:ss"
                                )
                              : "--"}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <span className="text-dark fw-semibold fs-6 ms-4 text-center">
                      No Records Found
                    </span>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className="col-md-12 card card-xl-stretch mb-xl-8">
        <div className="card-header border-0">
          <h3 className="card-title align-items-start flex-column ">
            <span className="d-flex align-items-center fw-boldest my-1 fs-2">
              Case Details - Case ID # EDD
              {merchantSummary && merchantSummary.edd_id
                ? merchantSummary.edd_id
                : "--"}
              <span
                className={`badge ml-2 ${
                  merchantSummary && merchantSummary.risk_status
                    ? StatusArray[
                        merchantSummary && merchantSummary.risk_status
                      ]
                    : ""
                }`}
                style={{
                  padding: "4px 12px",
                  marginLeft: "10px",
                  fontSize: "14px",
                }}
              >
                {merchantSummary && merchantSummary.risk_status
                  ? merchantSummary.risk_status
                  : ""}
              </span>
            </span>
          </h3>
          <div className="col-lg-4" />
          {!_.isEmpty(formData.assignedTo) ? (
            <span className="mt-4">
              <button className="btn btn-light-primary btn-sm" onClick={Assign}>
                Assign
              </button>
            </span>
          ) : null}
          <div className="d-flex justify-content-end my-2">
            <button
              className="btn btn-sm btn-light-dark px-2 ms-2 w-100px"
              onClick={() => onEddLogHistory()}
            >
              {EddHistoryListloading ? "please wait..." : "Change Log"}
            </button>
          </div>
        </div>
        <div className="separator separator-dashed my-3" />
        <div className="card-body pt-0">
          <div className="row g-5 g-xl-8">
            <div className="col-sm-4 col-md-4 col-lg-4">
              <div className="card card-xl-stretch">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Queue:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary &&
                        merchantSummary.queueId &&
                        merchantSummary.queueName
                          ? merchantSummary.queueName
                          : "--"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4">
              <div className="card card-xl-stretch">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Date Received:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {moment(
                          merchantSummary && merchantSummary.createdAt
                            ? merchantSummary.createdAt
                            : "--"
                        ).format("DD/MM/YYYY")}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-between align-items-center">
              <div className="col-sm-6 col-md-6 col-lg-6 ps-9">
                <h3>Edd Inputs</h3>
              </div>
              <div className="col-sm-6 col-md-6 col-lg-6 d-flex justify-content-end my-auto mt-4 pe-9">
                <ul className="nav nav-tabs nav-line-tabs fs-6">
                  <li className="nav-item">
                    <EddUpdate
                      show={eddUpdateValue}
                      id={currentId}
                      merchantSummary={merchantSummary}
                    />
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Website:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.website
                          ? merchantSummary.website
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Acquirer:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.acquirer
                          ? merchantSummary.acquirer
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Age Of Business:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.age_of_business
                          ? merchantSummary.age_of_business
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Aadhar Submitted:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold Id-response-Link">
                        {merchantSummary && merchantSummary.aadhar_submitted
                          ? merchantSummary.aadhar_submitted
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Average Monthly Volume:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold Id-response-Link">
                        {merchantSummary &&
                        merchantSummary.average_monthly_volume
                          ? merchantSummary.average_monthly_volume
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Average Transaction Amount:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary &&
                        merchantSummary.average_transaction_amount
                          ? merchantSummary.average_transaction_amount
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        PAN Submitted :
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.pan_submitted
                          ? merchantSummary.pan_submitted
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        GST Submitted :
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.gst_submitted
                          ? merchantSummary.gst_submitted
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        CIN Submitted :
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.cin_submitted
                          ? merchantSummary.cin_submitted
                          : "--"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Legal Name:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.legal_name
                          ? merchantSummary.legal_name
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Product:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.product
                          ? merchantSummary.product
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Transaction Type:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.transaction_type
                          ? merchantSummary.transaction_type
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        MCC Code:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.mcc_code
                          ? merchantSummary.mcc_code
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Bank:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.bank
                          ? merchantSummary.bank
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Business Ownership Type:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary &&
                        merchantSummary.business_ownership_type
                          ? merchantSummary.business_ownership_type
                          : ""}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Business Registered Address:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary &&
                        merchantSummary.business_registered_address
                          ? merchantSummary.business_registered_address
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Cin Number:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.cin_number
                          ? merchantSummary.cin_number
                          : "--"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-4 col-md-4 col-lg-4">
              <div className="card card-xl-stretch mb-xl-8">
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Gstin Number:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.gstin_nummber
                          ? merchantSummary.gstin_nummber
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Merchant Name:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.merchant_name
                          ? merchantSummary.merchant_name
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Merchant Personal Address:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary &&
                        merchantSummary.merchant_personal_address
                          ? merchantSummary.merchant_personal_address
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Merchant Personal Email:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary &&
                        merchantSummary.merchant_personal_email
                          ? merchantSummary.merchant_personal_email
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Merchant Phone:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.merchant_phone
                          ? merchantSummary.merchant_phone
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Name Of Director:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.name_of_director
                          ? merchantSummary.name_of_director
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Pan Number:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.pan_number
                          ? merchantSummary.pan_number
                          : "--"}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2">
                        Payment Instrument:
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="text-700 pl-3 fw-bold">
                        {merchantSummary && merchantSummary.payment_instrument
                          ? merchantSummary.payment_instrument
                          : "--"}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    dashboardStore,
    riskManagementlistStore,
    editMerchantStore,
    postAsigneeStore,
    updateAssignStore,
    eddHistoryListStore,
  } = state;

  return {
    dashboardDetails:
      dashboardStore && dashboardStore.dashboardDetails
        ? dashboardStore.dashboardDetails
        : {},
    riskmgmtlistdetails:
      riskManagementlistStore && riskManagementlistStore.riskmgmtlists
        ? riskManagementlistStore.riskmgmtlists
        : null,
    getRiskSummarys:
      state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    merchantIddetails:
      editMerchantStore && editMerchantStore.merchantIddetail
        ? editMerchantStore.merchantIddetail
        : {},
    getAssignee:
      postAsigneeStore && postAsigneeStore.getAssignee
        ? postAsigneeStore.getAssignee
        : {},
    updateAssign:
      updateAssignStore && updateAssignStore.updateAssign
        ? updateAssignStore.updateAssign
        : {},
    EddHistoryListResponse:
      eddHistoryListStore && eddHistoryListStore.eddHistoryList
        ? eddHistoryListStore.eddHistoryList
        : {},
    EddHistoryListloading:
      eddHistoryListStore && eddHistoryListStore.loading
        ? eddHistoryListStore.loading
        : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  merchantIdDetail: (id) =>
    dispatch(merchantIdDetailsActions.getmerchantIdDetailsData(id)),
  getAsigneeslistDispatch: (params) =>
    dispatch(GetAsigneeActions.GetAsignee(params)),
  updateAssignDispatch: (id, params) =>
    dispatch(AsiggnActions.Asiggn(id, params)),
  clearAsiggnDispatch: () => dispatch(AsiggnActions.clearAsiggn()),
  getEddHistoryListDispatch: (id) =>
    dispatch(eddHistoryListActions.getEddHistoryList(id)),
  clearEddHistoryListDispatch: () =>
    dispatch(eddHistoryListActions.clearEddHistoryList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetails);
