import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import WRMStatus from "./subComponent/website/wrmARH";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { eddApiScheduledListReportActions, EddRiskManagementApiFinalListActions } from "../../store/actions";
import { STATUS_RESPONSE } from "../../utils/constants";
import { successAlert, warningAlert } from "../../utils/alerts";
import EddReports from "./eddReport";
import _ from 'lodash'

const ApiReport = (props) => {
  const {
    eddApiScheduledListResponse,
    getApiDetailsResponse,
    merchantSummary,
    loading,
    postApiReport,
    id,
    postResponse,
    EddApiScheduledListReportDispatch,
    eddApiScheduledListReportStore,
    loadingEddApiScheduledListReport
  } = props;
  const [data, setData] = useState("");
  const [formData, setFormData] = useState([]);
  const [selectAll, setSelectAll] = useState(false);

  const StatusArray = {
    "PENDING": "badge-light-warning",
    "APPROVED": "badge-light-success",
    "REJECTED": "badge-light-danger",
  }
  useEffect(() => {
    if (
      merchantSummary &&
      merchantSummary.edd_final_list_status === "PENDING"
    ) {
      setData(getApiDetailsResponse && getApiDetailsResponse.data);
    } else if (
      merchantSummary &&
      merchantSummary.edd_final_list_status === "SCHEDULED"
    ) {
      setData(eddApiScheduledListResponse && eddApiScheduledListResponse.data)
      EddApiScheduledListReportDispatch(id)
    }
  }, [getApiDetailsResponse, eddApiScheduledListResponse]);

  const handleChangeSelectAll = (e) => {
    const checked = e.target.checked;
    setSelectAll(checked);
    if (checked) {
      const allValues = data.map((item) => item.api_name);
      setFormData(allValues);
    } else {
      setFormData([]);
    }
  };

  const handleChange = (e) => {
    const { value, checked } = e.target;
    if (checked) {
      setFormData((prev) => [...prev, value]);
    } else {
      setFormData((prev) => prev.filter((item) => item !== value));
      setSelectAll(false);
    }
  };

  const handleApiReport = () => {
    const params = {
      id: id,
      api_names: formData,
    };
    postApiReport(params);
  };

  const resetExport = () => {
    EddApiScheduledListReportDispatch(id)
  };

  useEffect(() => {
    if (postResponse && postResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        postResponse && postResponse.message,
        'success'
      )
    } else if (postResponse && postResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postResponse && postResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [postResponse])

  const scheduledReport = eddApiScheduledListReportStore && eddApiScheduledListReportStore.data

  return (
    <Fragment>
    <div className="bg-white p-5 mt-3 row">
      {merchantSummary &&
        merchantSummary.edd_final_list_status === "SCHEDULED" ?
        <>
          <div className="col-sm-8">
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='d-flex align-items-center'>EDD SCHEDULED APIS LIST
                  <span className={`badge ml-2 ${merchantSummary && merchantSummary.edd_final_decision_status ? StatusArray[merchantSummary && merchantSummary.risk_status] : ""}`} style={{ padding: "4px 12px", marginLeft: "10px", fontSize: "14px" }}>
                    {merchantSummary && merchantSummary.edd_final_decision_status ? merchantSummary.edd_final_decision_status : ""}
                  </span>
                </span>
              </h3>
            </div>
            {data &&
              data
                .filter(item => item && item.apiName)
                .map((item) => (
                  <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
                    <label className="form-check-label">
                      {item && item.apiName}
                    </label>
                  </div>
                ))}
          </div>
        </> : merchantSummary &&
            merchantSummary.edd_final_list_status === "PENDING" ?
        <>
          <div className="col-sm-8">
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <h3 className='card-title align-items-start flex-column'>
                <span className='d-flex align-items-center'>EDD APIS LIST
                    <span className={`badge ml-2 ${merchantSummary && merchantSummary.edd_final_decision_status ? StatusArray[merchantSummary && merchantSummary.risk_status] : ""}`} style={{ padding: "4px 12px", marginLeft: "10px", fontSize: "14px" }}>
                      {merchantSummary && merchantSummary.edd_final_decision_status ? merchantSummary.edd_final_decision_status : ""}
                    </span>
                </span>
              </h3>
            </div>
            <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
              <input
                className="form-check-input"
                type="checkbox"
                checked={selectAll}
                onChange={handleChangeSelectAll}
              />
              <label className="form-check-label">
                Select All
              </label>
            </div>
            {data &&
              data.map((item) => (
                <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id={item && item.api_name}
                    name={item && item.api_name}
                    value={item && item.api_name}
                    checked={formData.includes(item.api_name)}
                    onChange={(e) => handleChange(e)}
                  />
                  <label className="form-check-label" for={item && item.api_name}>
                    {item && item.api_name}
                  </label>
                  <OverlayTrigger
                    overlay={
                      <Tooltip id="tooltip-disabled" className="tooltip">
                        {item.description}
                      </Tooltip>
                    }
                    placement={"right"}
                  >
                    <span>
                      <i className="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-8" />
                    </span>
                  </OverlayTrigger>
                </div>
              ))}
            <div className="text-center mt-5">
              <button className="btn btn-sm btn-success" onClick={handleApiReport} disabled={loading}>
                {loading ? (
                  <span
                    className="indicator-progress text-white"
                    style={{ display: "block" }}
                  >
                    <span className="spinner-border spinner-border-sm align-middle ms-2" />
                  </span>
                ) : (
                  "Submit"
                )}
              </button>
            </div>
          </div>
        </> : null
       }
      <div className="col-sm-4">
        {merchantSummary &&
          merchantSummary.edd_final_decision_status === "PENDING" ?
          <>
            <WRMStatus />
          </> : merchantSummary &&
            merchantSummary.edd_final_list_status === "SCHEDULED" ?
            <>
              <div className="text-end">
                <button
                  type="button"
                  className="btn btn-sm btn-light-primary m-2 close"
                  onClick={() => resetExport()}
                  disabled={loadingEddApiScheduledListReport}
                >
                  {loadingEddApiScheduledListReport ? (
                    <span
                      className="indicator-progress text-white"
                      style={{ display: "block" }}
                    >
                      <span className="spinner-border spinner-border-sm align-middle ms-2" />
                    </span>
                  ) : (
                    <span className="indicator-label">Reload</span>
                  )}
                </button>
              </div>
           </> : null}
        <h3 className="d-block">
          Comments:
        </h3>
        <label>
          {merchantSummary && merchantSummary.edd_final_decision_reason}
        </label>
      </div>
    </div>
    <div className="col-sm-12 mt-2">
    <h3 className="mb-1">
      API Response
    </h3>
    <EddReports matrixDetail={scheduledReport} merchantSummary={merchantSummary} id={id}/>
    </div>
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  const { EddApiFinalListStore, editEddApiScheduledListReportStore } = state;
  return {
    loading: EddApiFinalListStore && EddApiFinalListStore.loading,
    postResponse:
      EddApiFinalListStore && EddApiFinalListStore.AddEddApiFinalListRes
        ? EddApiFinalListStore.AddEddApiFinalListRes
        : {},
    loadingEddApiScheduledListReport: editEddApiScheduledListReportStore && editEddApiScheduledListReportStore.loading,
    eddApiScheduledListReportStore:
      editEddApiScheduledListReportStore && editEddApiScheduledListReportStore.eddApiScheduledListReportdetail
        ? editEddApiScheduledListReportStore.eddApiScheduledListReportdetail
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  postApiReport: (params) =>
    dispatch(EddRiskManagementApiFinalListActions.getEddApiFinalList(params)),
  EddApiScheduledListReportDispatch: (id) =>
    dispatch(eddApiScheduledListReportActions.getEddApiScheduledListReportData(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ApiReport);