import _ from "lodash";

export const setCategorieData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      category: data.category,
      type: data.type,
      status: data.type
    };
  }
}