import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import {
  eddMerchantIdDetailsActions,
  ReTriggerFinalApiUpdateActions,
} from "../../store/actions";
import { successAlert, warningAlert } from "../../utils/alerts";
import { STATUS_RESPONSE } from "../../utils/constants";

const EddReports = (props) => {
  const {
    matrixDetail,
    merchantSummary,
    retriggerFinalApiDispatch,
    clearRetriggerFinalApiDispatch,
    ReTriggerFinalApi,
    loading,
    id,
    getIdMerchantDispatch,
  } = props;
  const [directorsArray, setDirectorsArray] = useState([]);

  const StatusArray = {
    SCHEDULED: "badge-light-warning",
    COMPLETED: "badge-light-success",
  };

  useEffect(() => {
    if (matrixDetail) {
      const directors = matrixDetail?.probe_director_details_api?.directors;

      if (directors) {
        setDirectorsArray([directors]);
      } else if (Array.isArray(directors)) {
        setDirectorsArray(directors);
      }
    }
  }, [matrixDetail]);

  const statusUpdate = (status) => {
    const params = {
      id: id,
      [status]: "SCHEDULED",
    };
    retriggerFinalApiDispatch(params);
  };

  useEffect(() => {
    if (
      ReTriggerFinalApi &&
      ReTriggerFinalApi.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      successAlert(ReTriggerFinalApi && ReTriggerFinalApi.message, "success");
      getIdMerchantDispatch(id);
      clearRetriggerFinalApiDispatch();
    } else if (
      ReTriggerFinalApi &&
      ReTriggerFinalApi.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        ReTriggerFinalApi && ReTriggerFinalApi.message,
        "",
        "Try again",
        "",
        () => {}
      );
      clearRetriggerFinalApiDispatch();
    }
  }, [ReTriggerFinalApi]);

  return (
    <div className="row g-5 g-xl-8 mb-8">
      {!_.isEmpty(matrixDetail) ? (
        <>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    CIN or PAN Company Data Status API
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        E Filing Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_CinorPan_company_datastatus_api &&
                        matrixDetail.probe_CinorPan_company_datastatus_api
                          .efiling_status
                          ? matrixDetail.probe_CinorPan_company_datastatus_api
                              .efiling_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Base Updated
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_CinorPan_company_datastatus_api &&
                        matrixDetail.probe_CinorPan_company_datastatus_api
                          .last_base_updated
                          ? matrixDetail.probe_CinorPan_company_datastatus_api
                              .last_base_updated
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Details Updated
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_CinorPan_company_datastatus_api &&
                        matrixDetail.probe_CinorPan_company_datastatus_api
                          .last_details_updated
                          ? matrixDetail.probe_CinorPan_company_datastatus_api
                              .last_details_updated
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Filing Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_CinorPan_company_datastatus_api &&
                        matrixDetail.probe_CinorPan_company_datastatus_api
                          .last_filing_date
                          ? matrixDetail.probe_CinorPan_company_datastatus_api
                              .last_filing_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Fin Year End
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_CinorPan_company_datastatus_api &&
                        matrixDetail.probe_CinorPan_company_datastatus_api
                          .last_fin_year_end
                          ? matrixDetail.probe_CinorPan_company_datastatus_api
                              .last_fin_year_end
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Next Cin
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_CinorPan_company_datastatus_api &&
                        matrixDetail.probe_CinorPan_company_datastatus_api
                          .next_cin
                          ? matrixDetail.probe_CinorPan_company_datastatus_api
                              .next_cin
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    Director Details API
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <span className="pl-4 text-dark fw-bolder mb-1 fs-5">
                  Directors
                </span>
                <div class="table-responsive px-4">
                  <table class="table">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">Name</th>
                        <th className="min-w-150px">PAN</th>
                        <th className="min-w-150px">DIN</th>
                        <th className="min-w-150px">DIN Status</th>
                        <th className="min-w-150px">Date of birth</th>
                      </tr>
                    </thead>
                    <tbody>
                      {directorsArray &&
                        directorsArray.length > 0 &&
                        directorsArray.map((item, index) => (
                          <tr>
                            <td>{item && item.name}</td>
                            <td>{item && item.pan ? item.pan : "No Data"}</td>
                            <td>{item && item.din ? item.din : "No Data"}</td>
                            <td>
                              {item && item.din_status
                                ? item.din_status
                                : "No Data"}
                            </td>
                            <td>
                              {item && item.date_of_birth
                                ? item.date_of_birth
                                : "No Data"}
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="flex card-title align-items-start">
                  <span className="card-label fw-bolder text-dark fs-3">
                    LLPIN Base API
                  </span>
                </h3>
                {merchantSummary &&
                merchantSummary.llpin_base_api_status === "SCHEDULED" ? (
                  <>
                    <div className="text-end">
                      <span
                        className={`badge ml-2 ${
                          merchantSummary &&
                          merchantSummary.llpin_base_api_status
                            ? StatusArray[
                                merchantSummary &&
                                  merchantSummary.llpin_base_api_status
                              ]
                            : ""
                        }`}
                        style={{
                          padding: "4px 12px",
                          marginLeft: "10px",
                          fontSize: "14px",
                        }}
                      >
                        {merchantSummary &&
                        merchantSummary.llpin_base_api_status
                          ? merchantSummary.llpin_base_api_status
                          : ""}
                      </span>
                    </div>
                  </>
                ) : merchantSummary &&
                  merchantSummary.llpin_base_api_status === "COMPLETED" ? (
                  <>
                    <div className="text-end">
                      <span
                        className={`badge ml-2 ${
                          merchantSummary &&
                          merchantSummary.llpin_base_api_status
                            ? StatusArray[
                                merchantSummary &&
                                  merchantSummary.llpin_base_api_status
                              ]
                            : ""
                        }`}
                        style={{
                          padding: "4px 12px",
                          marginLeft: "10px",
                          fontSize: "14px",
                        }}
                      >
                        {merchantSummary &&
                        merchantSummary.llpin_base_api_status
                          ? merchantSummary.llpin_base_api_status
                          : ""}
                      </span>
                    </div>
                  </>
                ) : null}
                {matrixDetail &&
                matrixDetail.probe_llpin_base_api &&
                matrixDetail.probe_llpin_base_api.last_updated_date ? (
                  <>
                    <div>
                      <span className="text-dark fs-5">Last Update Date:</span>
                      <span className="text-muted text-gray-700 fs-6 pl-3">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_base_api &&
                        matrixDetail.probe_llpin_base_api.last_updated_date
                          ? matrixDetail.probe_llpin_base_api.last_updated_date
                          : "No Data"}
                      </span>
                    </div>
                  </>
                ) : null}
                {merchantSummary &&
                merchantSummary.llpin_base_api_status === "PENDING" ? (
                  <>
                    <div className="text-end">
                      <button
                        type="button"
                        className="btn btn-sm btn-light-success"
                        onClick={() => statusUpdate("llpin_base_api_status")}
                        disabled={loading}
                      >
                        {loading ? (
                          <span
                            className="indicator-progress text-white"
                            style={{ display: "block" }}
                          >
                            <span className="spinner-border spinner-border-sm align-middle ms-2" />
                          </span>
                        ) : (
                          <span className="indicator-label">Update</span>
                        )}
                      </button>
                    </div>
                  </>
                ) : null}
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Llpin Or Pan input
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_base_api &&
                        matrixDetail.probe_llpin_base_api.LlpinOrPan_input
                          ? matrixDetail.probe_llpin_base_api.LlpinOrPan_input
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <span className="pl-4 text-dark fw-bolder mb-1 fs-5">
                  Directors
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2 mt-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">Name</th>
                        <th className="min-w-150px">PAN</th>
                        <th className="min-w-150px">DIN</th>
                        <th className="min-w-150px">DIN Status</th>
                        <th className="min-w-150px">Date of birth</th>
                        <th className="min-w-150px">Designation</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_base_api &&
                        matrixDetail.probe_llpin_base_api.directors &&
                        matrixDetail.probe_llpin_base_api.directors.map(
                          (item, index) => (
                            <tr>
                              <td>{item && item.name}</td>
                              <td>{item && item.pan ? item.pan : "No Data"}</td>
                              <td>{item && item.din ? item.din : "No Data"}</td>
                              <td>
                                {item && item.din_status
                                  ? item.din_status
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_birth
                                  ? item.date_of_birth
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.designation
                                  ? item.designation
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-4 text-dark fw-bolder mb-1 fs-5 mt-2">
                  Open charges
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2 mt-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">HOLDER NAME</th>
                        <th className="min-w-150px">TYPE</th>
                        <th className="min-w-150px">ID</th>
                        <th className="min-w-150px">AMOUNT</th>
                        <th className="min-w-150px">DATE</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_base_api &&
                        matrixDetail.probe_llpin_base_api.open_charges &&
                        matrixDetail.probe_llpin_base_api.open_charges.map(
                          (item, index) => (
                            <tr>
                              <td>{item && item.holder_name}</td>
                              <td>
                                {item && item.type ? item.type : "No Data"}
                              </td>
                              <td>{item && item.id ? item.id : "No Data"}</td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    LLPIN Company Data Status API
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Llpin Or Pan input
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_company_datastatus_api &&
                        matrixDetail.probe_llpin_company_datastatus_api
                          .LlpinOrPan_input
                          ? matrixDetail.probe_llpin_company_datastatus_api
                              .LlpinOrPan_input
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        efiling status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_company_datastatus_api &&
                        matrixDetail.probe_llpin_company_datastatus_api
                          .efiling_status
                          ? matrixDetail.probe_llpin_company_datastatus_api
                              .efiling_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last base updated
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_company_datastatus_api &&
                        matrixDetail.probe_llpin_company_datastatus_api
                          .last_base_updated
                          ? matrixDetail.probe_llpin_company_datastatus_api
                              .last_base_updated
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last details updated
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_company_datastatus_api &&
                        matrixDetail.probe_llpin_company_datastatus_api
                          .last_details_updated
                          ? matrixDetail.probe_llpin_company_datastatus_api
                              .last_details_updated
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Fin Year End
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_company_datastatus_api &&
                        matrixDetail.probe_llpin_company_datastatus_api
                          .last_fin_year_end
                          ? matrixDetail.probe_llpin_company_datastatus_api
                              .last_fin_year_end
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last financial reporting date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_company_datastatus_api &&
                        matrixDetail.probe_llpin_company_datastatus_api
                          .last_financial_reporting_date
                          ? matrixDetail.probe_llpin_company_datastatus_api
                              .last_financial_reporting_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-8">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    LLPIN Comprehensive API
                  </span>
                  {matrixDetail &&
                  matrixDetail.probe_llpin_comprehensive_api &&
                  matrixDetail.probe_llpin_comprehensive_api
                    .last_updated_date ? (
                    <>
                      <div>
                        <span className="text-dark fs-5">
                          Last Update Date:
                        </span>
                        <span className="text-muted text-gray-700 fs-6 pl-3">
                          {matrixDetail &&
                          matrixDetail.probe_llpin_comprehensive_api &&
                          matrixDetail.probe_llpin_comprehensive_api
                            .last_updated_date
                            ? matrixDetail.probe_llpin_comprehensive_api
                                .last_updated_date
                            : "No Data"}
                        </span>
                      </div>
                    </>
                  ) : null}
                </h3>
                {merchantSummary &&
                merchantSummary.llpin_comprehensive_api_status ===
                  "SCHEDULED" ? (
                  <>
                    <div className="text-end">
                      <span
                        className={`badge ml-2 ${
                          merchantSummary &&
                          merchantSummary.llpin_comprehensive_api_status
                            ? StatusArray[
                                merchantSummary &&
                                  merchantSummary.llpin_comprehensive_api_status
                              ]
                            : ""
                        }`}
                        style={{
                          padding: "4px 12px",
                          marginLeft: "10px",
                          fontSize: "14px",
                        }}
                      >
                        {merchantSummary &&
                        merchantSummary.llpin_comprehensive_api_status
                          ? merchantSummary.llpin_comprehensive_api_status
                          : ""}
                      </span>
                    </div>
                  </>
                ) : merchantSummary &&
                  merchantSummary.llpin_comprehensive_api_status ===
                    "COMPLETED" ? (
                  <>
                    <div className="text-end">
                      <span
                        className={`badge ml-2 ${
                          merchantSummary &&
                          merchantSummary.llpin_comprehensive_api_status
                            ? StatusArray[
                                merchantSummary &&
                                  merchantSummary.llpin_comprehensive_api_status
                              ]
                            : ""
                        }`}
                        style={{
                          padding: "4px 12px",
                          marginLeft: "10px",
                          fontSize: "14px",
                        }}
                      >
                        {merchantSummary &&
                        merchantSummary.llpin_comprehensive_api_status
                          ? merchantSummary.llpin_comprehensive_api_status
                          : ""}
                      </span>
                    </div>
                  </>
                ) : null}
                {merchantSummary &&
                merchantSummary.llpin_comprehensive_api_status === "PENDING" ? (
                  <>
                    <div className="text-end">
                      <button
                        type="button"
                        className="btn btn-sm btn-light-success"
                        onClick={() =>
                          statusUpdate("llpin_comprehensive_api_status")
                        }
                        disabled={loading}
                      >
                        {loading ? (
                          <span
                            className="indicator-progress text-white"
                            style={{ display: "block" }}
                          >
                            <span className="spinner-border spinner-border-sm align-middle ms-2" />
                          </span>
                        ) : (
                          <span className="indicator-label">Update</span>
                        )}
                      </button>
                    </div>
                  </>
                ) : null}
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Llpin Or Pan input
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .LlpinOrPan_input
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .LlpinOrPan_input
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Financial Year
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .financial_year
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .financial_year
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Credit Rating Rationale
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Rating Agency
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_rating_rationale &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_rating_rationale.rating_agency
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .credit_rating_rationale.rating_agency
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Doc ID
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_rating_rationale &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_rating_rationale.doc_id
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .credit_rating_rationale.doc_id
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Financial Year
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_rating_rationale &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_rating_rationale.financial_year
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .credit_rating_rationale.financial_year
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Holding Entities
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Company
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities.company
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .holding_entities.company
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">Llp</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities.llp
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .holding_entities.llp
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Others
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities.others
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .holding_entities.others
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Financial Year
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .holding_entities.financial_year
                          ? matrixDetail.probe_llpin_comprehensive_api
                              .holding_entities.financial_year
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Llp
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">Llpin</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp.llpin
                          ? matrixDetail.probe_llpin_comprehensive_api.llp.llpin
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Legal Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .legal_name
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .legal_name
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Classification
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .classification
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .classification
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Cirp Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .cirp_status
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .cirp_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Efiling Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .efiling_status
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .efiling_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">Email</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp.email
                          ? matrixDetail.probe_llpin_comprehensive_api.llp.email
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">PAN</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp.pan
                          ? matrixDetail.probe_llpin_comprehensive_api.llp.pan
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        website
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp.website
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .website
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Incorporation Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .incorporation_date
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .incorporation_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Annual Returns Filed Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .last_annual_returns_filed_date
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .last_annual_returns_filed_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Financial Reporting Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .last_financial_reporting_date
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .last_financial_reporting_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Sum Of Charges
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .sum_of_charges
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .sum_of_charges
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Total Contribution Received
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .total_contribution_received
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .total_contribution_received
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Total Obligation Of Contribution
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.llp &&
                        matrixDetail.probe_llpin_comprehensive_api.llp
                          .total_obligation_of_contribution
                          ? matrixDetail.probe_llpin_comprehensive_api.llp
                              .total_obligation_of_contribution
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Probe Financial Score
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Efficiency Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.probe_financial_score.efficiency_score
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.probe_financial_score.efficiency_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Growth Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.probe_financial_score.growth_score
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.probe_financial_score.growth_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Liquidity Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.probe_financial_score.liquidity_score
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.probe_financial_score.liquidity_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Overall Financial Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.probe_financial_score.overall_financial_score
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.probe_financial_score.overall_financial_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Profitability Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.probe_financial_score.profitability_score
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.probe_financial_score.profitability_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Solvency Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.probe_financial_score.solvency_score
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.probe_financial_score.solvency_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Struck Off 248 Details
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Restored Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .struckoff248_details &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.struckoff248_details.restored_status
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.struckoff248_details.restored_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Struck Off Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .struckoff248_details &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.struckoff248_details.struck_off_status
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.struckoff248_details.struck_off_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Subsidiary Entities
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Company
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .subsidiary_entities &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.subsidiary_entities.company
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.subsidiary_entities.company
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Financial Year
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .subsidiary_entities &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.subsidiary_entities.financial_year
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.subsidiary_entities.financial_year
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">Llp</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .subsidiary_entities &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.subsidiary_entities.llp
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.subsidiary_entities.llp
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Others
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .subsidiary_entities &&
                        matrixDetail?.probe_llpin_comprehensive_api
                          ?.subsidiary_entities.others
                          ? matrixDetail.probe_llpin_comprehensive_api
                              ?.subsidiary_entities.others
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Charge Sequence
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">HOLDER NAME</th>
                        <th className="min-w-150px">NUMBER OF HOLDER</th>
                        <th className="min-w-150px">PROPERTY PARTICULARS</th>
                        <th className="min-w-150px">PROPERTY TYPE</th>
                        <th className="min-w-150px">CHARGE ID</th>
                        <th className="min-w-150px">STATUS</th>
                        <th className="min-w-150px">DATE</th>
                        <th className="min-w-150px">FILING DATE</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .charge_sequence &&
                        matrixDetail.probe_llpin_comprehensive_api.charge_sequence.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.holder_name
                                  ? item.holder_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.number_of_holder
                                  ? item.number_of_holder
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.property_particulars
                                  ? item.property_particulars
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.property_type
                                  ? item.property_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.charge_id
                                  ? item.charge_id
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                              <td>
                                {item && item.filing_date
                                  ? item.filing_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Credit Ratings
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">TYPE OF LOAN</th>
                        <th className="min-w-150px">RATING AGENCY</th>
                        <th className="min-w-150px">RATING</th>
                        <th className="min-w-150px">RATING DATE</th>
                        <th className="min-w-150px">CURRENCY</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .credit_ratings &&
                        matrixDetail.probe_llpin_comprehensive_api.credit_ratings.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.type_of_loan
                                  ? item.type_of_loan
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.rating_agency
                                  ? item.rating_agency
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.rating ? item.rating : "No Data"}
                              </td>
                              <td>
                                {item && item.rating_date
                                  ? item.rating_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.currency
                                  ? item.currency
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Director Network
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">Name</th>
                        <th className="min-w-150px">DIN</th>
                        <th className="min-w-150px">PAN</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .director_network &&
                        matrixDetail.probe_llpin_comprehensive_api.director_network.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>{item && item.din ? item.din : "No Data"}</td>
                              <td>{item && item.pan ? item.pan : "No Data"}</td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Directors
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">Name</th>
                        <th className="min-w-150px">PAN</th>
                        <th className="min-w-150px">DIN</th>
                        <th className="min-w-150px">DIN Status</th>
                        <th className="min-w-150px">Date of birth</th>
                        <th className="min-w-150px">DESIGNATION</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.directors &&
                        matrixDetail.probe_llpin_comprehensive_api.directors.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>{item && item.pan ? item.pan : "No Data"}</td>
                              <td>{item && item.din ? item.din : "No Data"}</td>
                              <td>
                                {item && item.din_status
                                  ? item.din_status
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_birth
                                  ? item.date_of_birth
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.designation
                                  ? item.designation
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Email
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">EMAIL ID</th>
                        <th className="min-w-150px">STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.email &&
                        matrixDetail.probe_llpin_comprehensive_api.email.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.emailId
                                  ? item.emailId
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Establishments Registered With EPFO
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">ESTABLISHMENT ID</th>
                        <th className="min-w-150px">ESTABLISHMENT NAME</th>
                        <th className="min-w-150px">EXEMPTION STATUS EDLI</th>
                        <th className="min-w-150px">
                          EXEMPTION STATUS PENSION
                        </th>
                        <th className="min-w-150px">EXEMPTION STATUS PF</th>
                        <th className="min-w-150px">
                          PRICIPAL BUSINESS ACTIVITIES
                        </th>
                        <th className="min-w-150px">WORKING STATUS</th>
                        <th className="min-w-150px">CITY</th>
                        <th className="min-w-150px">ADDRESS</th>
                        <th className="min-w-150px">NO OF EMPLOYEES</th>
                        <th className="min-w-150px">DATE OF SETUP</th>
                        <th className="min-w-150px">LATEST WAGE MONTH</th>
                        <th className="min-w-150px">LATEST DATE OF CREDIT</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .establishments_registered_with_epfo &&
                        matrixDetail.probe_llpin_comprehensive_api.establishments_registered_with_epfo.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.establishment_id
                                  ? item.establishment_id
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.establishment_name
                                  ? item.establishment_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.exemption_status_edli
                                  ? item.exemption_status_edli
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.exemption_status_pension
                                  ? item.exemption_status_pension
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.exemption_status_pf
                                  ? item.exemption_status_pf
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.principal_business_activities
                                  ? item.principal_business_activities
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.working_status
                                  ? item.working_status
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.city ? item.city : "No Data"}
                              </td>
                              <td>
                                {item && item.address
                                  ? item.address
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.no_of_employees
                                  ? item.no_of_employees
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_setup
                                  ? item.date_of_setup
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.latest_wage_month
                                  ? item.latest_wage_month
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.latest_date_of_credit
                                  ? item.latest_date_of_credit
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Financials
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">ESTABLISHMENT ID</th>
                        <th className="min-w-150px">ESTABLISHMENT NAME</th>
                        <th className="min-w-150px">EXEMPTION STATUS EDLI</th>
                        <th className="min-w-150px">
                          EXEMPTION STATUS PENSION
                        </th>
                        <th className="min-w-150px">EXEMPTION STATUS PF</th>
                        <th className="min-w-150px">
                          PRICIPAL BUSINESS ACTIVITIES
                        </th>
                        <th className="min-w-150px">WORKING STATUS</th>
                        <th className="min-w-150px">CITY</th>
                        <th className="min-w-150px">ADDRESS</th>
                        <th className="min-w-150px">NO OF EMPLOYEES</th>
                        <th className="min-w-150px">DATE OF SETUP</th>
                        <th className="min-w-150px">LATEST WAGE MONTH</th>
                        <th className="min-w-150px">LATEST DATE OF CREDIT</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.financials &&
                        matrixDetail.probe_llpin_comprehensive_api.financials.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.establishment_id
                                  ? item.establishment_id
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.establishment_name
                                  ? item.establishment_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.exemption_status_edli
                                  ? item.exemption_status_edli
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.exemption_status_pension
                                  ? item.exemption_status_pension
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.exemption_status_pf
                                  ? item.exemption_status_pf
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.principal_business_activities
                                  ? item.principal_business_activities
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.working_status
                                  ? item.working_status
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.city ? item.city : "No Data"}
                              </td>
                              <td>
                                {item && item.address
                                  ? item.address
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.no_of_employees
                                  ? item.no_of_employees
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_setup
                                  ? item.date_of_setup
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.latest_wage_month
                                  ? item.latest_wage_month
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.latest_date_of_credit
                                  ? item.latest_date_of_credit
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  GST Details
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">CENTRE JURISDICTION</th>
                        <th className="min-w-150px">COMPANY NAME</th>
                        <th className="min-w-150px">GSTIN</th>
                        <th className="min-w-150px">
                          NATURE OF BUSINESS ACTIVITIES
                        </th>
                        <th className="min-w-150px">STATE JURISDICTION</th>
                        <th className="min-w-150px">TRADE NAME</th>
                        <th className="min-w-150px">TAX PAYER TYPE</th>
                        <th className="min-w-150px">STATE</th>
                        <th className="min-w-150px">DATE OF REGISTRATION</th>
                        <th className="min-w-150px">STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .gst_details &&
                        matrixDetail.probe_llpin_comprehensive_api.gst_details.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.centreJurisdiction
                                  ? item.centreJurisdiction
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.companyName
                                  ? item.companyName
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.gstin ? item.gstin : "No Data"}
                              </td>
                              <td>
                                {item && item.natureOfBusinessActivities
                                  ? item.natureOfBusinessActivities
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.stateJurisdiction
                                  ? item.stateJurisdiction
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.tradeName
                                  ? item.tradeName
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.taxpayerType
                                  ? item.taxpayerType
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.state ? item.state : "No Data"}
                              </td>
                              <td>
                                {item && item.dateOfRegistration
                                  ? item.dateOfRegistration
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Individual Partners
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NAME</th>
                        <th className="min-w-150px">DESIGNATION</th>
                        <th className="min-w-150px">ID TYPE</th>
                        <th className="min-w-150px">ID</th>
                        <th className="min-w-150px">OBLIGATION CONTRIBUTION</th>
                        <th className="min-w-150px">RECEIVED CONTRIBUTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .individual_partners &&
                        matrixDetail.probe_llpin_comprehensive_api.individual_partners.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>
                                {item && item.designation
                                  ? item.designation
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.id_type
                                  ? item.id_type
                                  : "No Data"}
                              </td>
                              <td>{item && item.id ? item.id : "No Data"}</td>
                              <td>
                                {item && item.obligation_contribution
                                  ? item.obligation_contribution
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.received_contribution
                                  ? item.received_contribution
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Industry Segments
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">INDUSTRY</th>
                        <th className="min-w-150px">SEGMENTS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .industry_segments &&
                        matrixDetail.probe_llpin_comprehensive_api.industry_segments.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.industry
                                  ? item.industry
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.segments
                                  ? item.segments
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Legal History
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">PETITIONER</th>
                        <th className="min-w-150px">RESPONDENT</th>
                        <th className="min-w-150px">CASE CATEGORY</th>
                        <th className="min-w-150px">CASE NUMBER</th>
                        <th className="min-w-150px">CASE STATUS</th>
                        <th className="min-w-150px">CASE TYPE</th>
                        <th className="min-w-150px">COURT</th>
                        <th className="min-w-150px">DATE</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .legal_history &&
                        matrixDetail.probe_llpin_comprehensive_api.legal_history.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.petitioner
                                  ? item.petitioner
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.respondent
                                  ? item.respondent
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_category
                                  ? item.case_category
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_number
                                  ? item.case_number
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_status
                                  ? item.case_status
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_type
                                  ? item.case_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.court ? item.court : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Open Charges
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">HOLDER NAME</th>
                        <th className="min-w-150px">ID</th>
                        <th className="min-w-150px">TYPE</th>
                        <th className="min-w-150px">DATE</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .open_charges &&
                        matrixDetail.probe_llpin_comprehensive_api.open_charges.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.holder_name
                                  ? item.holder_name
                                  : "No Data"}
                              </td>
                              <td>{item && item.id ? item.id : "No Data"}</td>
                              <td>
                                {item && item.type ? item.type : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Open Charges Latest Event
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">HOLDER NAME</th>
                        <th className="min-w-150px">ID</th>
                        <th className="min-w-150px">TYPE</th>
                        <th className="min-w-150px">INSTRUMENT DESCRIPTION</th>
                        <th className="min-w-150px">JOINT HOLDING</th>
                        <th className="min-w-150px">
                          MODIFICATION PARTICULARS
                        </th>
                        <th className="min-w-150px">PROPERTY PARTICULARS</th>
                        <th className="min-w-150px">PROPERTY TYPE</th>
                        <th className="min-w-150px">CONSORTIUM HOLDING</th>
                        <th className="min-w-150px">NUMBER OF CHARGE HOLDER</th>
                        <th className="min-w-150px">EXTENT AND OPERATION</th>
                        <th className="min-w-150px">OTHER TERMS</th>
                        <th className="min-w-150px">RATE OF INTEREST</th>
                        <th className="min-w-150px">TERMS OF PAYMENT</th>
                        <th className="min-w-150px">DATE</th>
                        <th className="min-w-150px">FILLING DATE</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .open_charges_latest_event &&
                        matrixDetail.probe_llpin_comprehensive_api.open_charges_latest_event.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.holder_name
                                  ? item.holder_name
                                  : "No Data"}
                              </td>
                              <td>{item && item.id ? item.id : "No Data"}</td>
                              <td>
                                {item && item.type ? item.type : "No Data"}
                              </td>
                              <td>
                                {item && item.instrument_description
                                  ? item.instrument_description
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.joint_holding
                                  ? item.joint_holding
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.modification_particulars
                                  ? item.modification_particulars
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.property_particulars
                                  ? item.property_particulars
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.property_type
                                  ? item.property_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.consortium_holding
                                  ? item.consortium_holding
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.number_of_chargeholder
                                  ? item.number_of_chargeholder
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.extent_and_operation
                                  ? item.extent_and_operation
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.other_terms
                                  ? item.other_terms
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.rate_of_interest
                                  ? item.rate_of_interest
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.terms_of_payment
                                  ? item.terms_of_payment
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                              <td>
                                {item && item.filing_date
                                  ? item.filing_date
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Phone
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">PHONE NUMBER</th>
                        <th className="min-w-150px">STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api.phone &&
                        matrixDetail.probe_llpin_comprehensive_api.phone.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.phoneNumber
                                  ? item.phoneNumber
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Principal Business Activities
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">BUSINESS CLASSIFICATION</th>
                        <th className="min-w-150px">
                          PRICIPAL BUSINESS ACTIVITIES
                        </th>
                        <th className="min-w-150px">YEAR</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .principal_business_activities &&
                        matrixDetail.probe_llpin_comprehensive_api.principal_business_activities.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.business_classification
                                  ? item.business_classification
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.principal_business_activities
                                  ? item.principal_business_activities
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Summary Designated Partners
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">CATEGORY</th>
                        <th className="min-w-150px">FINANCIAL YEAR</th>
                        <th className="min-w-150px">INDIAN DESIG PARTNER</th>
                        <th className="min-w-150px">OTHER DESIG PARTNER</th>
                        <th className="min-w-150px">PARTNER</th>
                        <th className="min-w-150px">TOTAL</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_llpin_comprehensive_api &&
                        matrixDetail.probe_llpin_comprehensive_api
                          .summary_designated_partners &&
                        matrixDetail.probe_llpin_comprehensive_api.summary_designated_partners.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.category
                                  ? item.category
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.financial_year
                                  ? item.financial_year
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.indian_desig_partner
                                  ? item.indian_desig_partner
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.other_desig_partner
                                  ? item.other_desig_partner
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.partner
                                  ? item.partner
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.total ? item.total : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    Search API
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Name of Company Input
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_search_api &&
                        matrixDetail.probe_search_api.name_of_company_input
                          ? matrixDetail.probe_search_api.name_of_company_input
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Has More
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_search_api &&
                        matrixDetail.probe_search_api.has_more
                          ? matrixDetail.probe_search_api.has_more.toString()
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Llp Count
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_search_api &&
                        matrixDetail.probe_search_api.llp_count
                          ? matrixDetail.probe_search_api.llp_count
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Company Count
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_search_api &&
                        matrixDetail.probe_search_api.company_count
                          ? matrixDetail.probe_search_api.company_count
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Total Count
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_search_api &&
                        matrixDetail.probe_search_api.total_count
                          ? matrixDetail.probe_search_api.total_count
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Companies
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">CIN</th>
                        <th className="min-w-150px">LEGAL NAME</th>
                        <th className="min-w-150px">STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_search_api &&
                        matrixDetail.probe_search_api.companies &&
                        matrixDetail.probe_search_api.companies.map(
                          (item, index) => (
                            <tr>
                              <td>{item && item.cin ? item.cin : "No Data"}</td>
                              <td>
                                {item && item.legal_name
                                  ? item.legal_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-8">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    CIN Company Base Details API
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Company Details
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Legal Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.legal_name
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.legal_name
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Classification
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.classification
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.classification
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">Email</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.email
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.email
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">CIN</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.cin
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.cin
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Active Compliance
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.active_compliance
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.active_compliance
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Efiling Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.efiling_status
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.efiling_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.status
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Authorized Capital
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.authorized_capital
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.authorized_capital
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Paid Up Capital
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.paid_up_capital
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.paid_up_capital
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Sum Of Charges
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.sum_of_charges
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.sum_of_charges
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Incorporation Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.incorporation_date
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.incorporation_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Filing Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.last_filing_date
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.last_filing_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last AGM Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .company &&
                        matrixDetail?.probe_cin_company_base_details_api
                          ?.company.last_agm_date
                          ? matrixDetail.probe_cin_company_base_details_api
                              .company.last_agm_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Authorized Signatories
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NAME</th>
                        <th className="min-w-150px">GENDER</th>
                        <th className="min-w-150px">AGE</th>
                        <th className="min-w-150px">PAN</th>
                        <th className="min-w-150px">DATE OF BIRTH</th>
                        <th className="min-w-150px">DESIGNATION</th>
                        <th className="min-w-150px">
                          DATE OF APPOINTMENT FOR CURRENT DESIGNATION
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .authorized_signatories &&
                        matrixDetail.probe_cin_company_base_details_api.authorized_signatories.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>
                                {item && item.gender ? item.gender : "No Data"}
                              </td>
                              <td>{item && item.age ? item.age : "No Data"}</td>
                              <td>{item && item.pan ? item.pan : "No Data"}</td>
                              <td>
                                {item && item.date_of_birth
                                  ? item.date_of_birth
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.designation
                                  ? item.designation
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.date_of_appointment_for_current_designation
                                  ? item.date_of_appointment_for_current_designation
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Open Charges
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">HOLDER NAME</th>
                        <th className="min-w-150px">ID</th>
                        <th className="min-w-150px">TYPE</th>
                        <th className="min-w-150px">DATE</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cin_company_base_details_api &&
                        matrixDetail.probe_cin_company_base_details_api
                          .open_charges &&
                        matrixDetail.probe_cin_company_base_details_api.open_charges.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.holder_name
                                  ? item.holder_name
                                  : "No Data"}
                              </td>
                              <td>{item && item.id ? item.id : "No Data"}</td>
                              <td>
                                {item && item.type ? item.type : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-12">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header web-tab-header pt-4 pb-3 border-0 mb-4">
                <h3 className="card-title align-items-start flex-column">
                  <span className="card-label fw-bolder text-dark fs-3">
                    CIN or PAN Company Comprehensive API
                  </span>
                  {matrixDetail &&
                  matrixDetail.probe_cinorpan_company_comprehensive_api &&
                  matrixDetail.probe_cinorpan_company_comprehensive_api
                    .last_updated_date ? (
                    <>
                      <div>
                        <span className="text-dark fs-5">
                          Last Update Date:
                        </span>
                        <span className="text-muted text-gray-700 fs-6 pl-3">
                          {matrixDetail &&
                          matrixDetail.probe_cinorpan_company_comprehensive_api &&
                          matrixDetail.probe_cinorpan_company_comprehensive_api
                            .last_updated_date
                            ? matrixDetail
                                .probe_cinorpan_company_comprehensive_api
                                .last_updated_date
                            : "No Data"}
                        </span>
                      </div>
                    </>
                  ) : null}
                </h3>
                {merchantSummary &&
                merchantSummary.cin_or_pan_company_comprehensive_api_status ===
                  "SCHEDULED" ? (
                  <>
                    <div className="text-end">
                      <span
                        className={`badge ml-2 ${
                          merchantSummary &&
                          merchantSummary.cin_or_pan_company_comprehensive_api_status
                            ? StatusArray[
                                merchantSummary &&
                                  merchantSummary.cin_or_pan_company_comprehensive_api_status
                              ]
                            : ""
                        }`}
                        style={{
                          padding: "4px 12px",
                          marginLeft: "10px",
                          fontSize: "14px",
                        }}
                      >
                        {merchantSummary &&
                        merchantSummary.cin_or_pan_company_comprehensive_api_status
                          ? merchantSummary.cin_or_pan_company_comprehensive_api_status
                          : ""}
                      </span>
                    </div>
                  </>
                ) : merchantSummary &&
                  merchantSummary.cin_or_pan_company_comprehensive_api_status ===
                    "COMPLETED" ? (
                  <>
                    <div className="text-end">
                      <span
                        className={`badge ml-2 ${
                          merchantSummary &&
                          merchantSummary.cin_or_pan_company_comprehensive_api_status
                            ? StatusArray[
                                merchantSummary &&
                                  merchantSummary.cin_or_pan_company_comprehensive_api_status
                              ]
                            : ""
                        }`}
                        style={{
                          padding: "4px 12px",
                          marginLeft: "10px",
                          fontSize: "14px",
                        }}
                      >
                        {merchantSummary &&
                        merchantSummary.cin_or_pan_company_comprehensive_api_status
                          ? merchantSummary.cin_or_pan_company_comprehensive_api_status
                          : ""}
                      </span>
                    </div>
                  </>
                ) : null}
                {merchantSummary &&
                merchantSummary.cin_or_pan_company_comprehensive_api_status ===
                  "PENDING" ? (
                  <>
                    <div className="text-end">
                      <button
                        type="button"
                        className="btn btn-sm btn-light-success"
                        onClick={() =>
                          statusUpdate(
                            "cin_or_pan_company_comprehensive_api_status"
                          )
                        }
                        disabled={loading}
                      >
                        {loading ? (
                          <span
                            className="indicator-progress text-white"
                            style={{ display: "block" }}
                          >
                            <span className="spinner-border spinner-border-sm align-middle ms-2" />
                          </span>
                        ) : (
                          <span className="indicator-label">Update</span>
                        )}
                      </button>
                    </div>
                  </>
                ) : null}
              </div>
              <div className="card-body pt-0">
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Company Details
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Legal Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.legal_name
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .legal_name
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">CIN</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.cin
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .cin
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">PAN</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.pan
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .pan
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">Email</span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.email
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .email
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Website
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.website
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .website
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.status
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Efiling Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.efiling_status
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .efiling_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Active Compliance
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.active_compliance
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .active_compliance
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Classification
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.classification
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .classification
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Incorporation Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.incorporation_date
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .incorporation_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last Filing Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.last_filing_date
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .last_filing_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Last AGM Date
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.last_agm_date
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .last_agm_date
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Authorized Capital
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.authorized_capital
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .authorized_capital
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Paid Up Capital
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.paid_up_capital
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .paid_up_capital
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Sum Of Charges
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .company.sum_of_charges
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api.company
                              .sum_of_charges
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Description
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .description &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .description.desc_thousand_char
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .description.desc_thousand_char
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Financial Score
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Efficiency Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score.efficiency_score
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .probe_financial_score.efficiency_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Growth Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score.growth_score
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .probe_financial_score.growth_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Liquidity Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score.liquidity_score
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .probe_financial_score.liquidity_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Overall Financial Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score.overall_financial_score
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .probe_financial_score.overall_financial_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Profitability Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score.profitability_score
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .probe_financial_score.profitability_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Solvency Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .probe_financial_score.solvency_score
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .probe_financial_score.solvency_score
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bolder mb-1 fs-5 text-center">
                  Struck Off 248 Details
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Restored Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .struckoff248_details &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .struckoff248_details.restored_status
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .struckoff248_details.restored_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-2">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Struck Off Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .struckoff248_details &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .struckoff248_details.struck_off_status
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .struckoff248_details.struck_off_status
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Associate Entities
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">LEGAL NAME</th>
                        <th className="min-w-150px">CIN</th>
                        <th className="min-w-150px">ACTIVE COMPLIANCE</th>
                        <th className="min-w-150px">CITY</th>
                        <th className="min-w-150px">INCORPORATION DATE</th>
                        <th className="min-w-150px">STATUS</th>
                        <th className="min-w-150px">
                          SHARE HOLDING PERCENTAGE
                        </th>
                        <th className="min-w-150px">PAID UP CAPITAL</th>
                        <th className="min-w-150px">SUM OF CHARGES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .associate_entities &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .associate_entities.company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.associate_entities.company.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.legal_name
                                  ? item.legal_name
                                  : "No Data"}
                              </td>
                              <td>{item && item.cin ? item.cin : "No Data"}</td>
                              <td>
                                {item && item.active_compliance
                                  ? item.active_compliance
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.city ? item.city : "No Data"}
                              </td>
                              <td>
                                {item && item.incorporation_date
                                  ? item.incorporation_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                              <td>
                                {item && item.share_holding_percentage
                                  ? item.share_holding_percentage
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.paid_up_capital
                                  ? item.paid_up_capital
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.sum_of_charges
                                  ? item.sum_of_charges
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Authorized Signatories
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2 mt-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NAME</th>
                        <th className="min-w-150px">GENDER</th>
                        <th className="min-w-150px">AGE</th>
                        <th className="min-w-150px">DATE OF BIRTH</th>
                        <th className="min-w-150px">DESIGNATION</th>
                        <th className="min-w-150px">
                          DATE OF APPOINTMENT FOR CURRENT DESIGNATION
                        </th>
                        <th className="min-w-150px">PAN</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .authorized_signatories &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.authorized_signatories.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>
                                {item && item.gender ? item.gender : "No Data"}
                              </td>
                              <td>{item && item.age ? item.age : "No Data"}</td>
                              <td>
                                {item && item.date_of_birth
                                  ? item.date_of_birth
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.designation
                                  ? item.designation
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.date_of_appointment_for_current_designation
                                  ? item.date_of_appointment_for_current_designation
                                  : "No Data"}
                              </td>
                              <td>{item && item.pan ? item.pan : "No Data"}</td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Charge Sequence
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">HOLDER NAME</th>
                        <th className="min-w-150px">NUMBER OF HOLDER</th>
                        <th className="min-w-150px">STATUS</th>
                        <th className="min-w-150px">CHARGE ID</th>
                        <th className="min-w-150px">DATE</th>
                        <th className="min-w-150px">FILING DATE</th>
                        <th className="min-w-150px">PROPERTY PARTICULARS</th>
                        <th className="min-w-150px">PROPERTY TYPE</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .charge_sequence &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.charge_sequence.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.holder_name
                                  ? item.holder_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.number_of_holder
                                  ? item.number_of_holder
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                              <td>
                                {item && item.charge_id
                                  ? item.charge_id
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                              <td>
                                {item && item.filing_date
                                  ? item.filing_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.property_particulars
                                  ? item.property_particulars
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.property_type
                                  ? item.property_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Contact Details
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">EMAIL</th>
                        <th className="min-w-150px">STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .contact_details &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .contact_details.email &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.contact_details.email.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.emailId
                                  ? item.emailId
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">PHONE NUMBER</th>
                        <th className="min-w-150px">STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .contact_details &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .contact_details.phone &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.contact_details.phone.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.phoneNumber
                                  ? item.phoneNumber
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Credit Ratings
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">RATING AGENCY</th>
                        <th className="min-w-150px">TYPE OF LOAN</th>
                        <th className="min-w-150px">RATING</th>
                        <th className="min-w-150px">RATING DATE</th>
                        <th className="min-w-150px">CURRENCY</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .credit_ratings &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.credit_ratings.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.rating_agency
                                  ? item.rating_agency
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.type_of_loan
                                  ? item.type_of_loan
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.rating ? item.rating : "No Data"}
                              </td>
                              <td>
                                {item && item.rating_date
                                  ? item.rating_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.currency
                                  ? item.currency
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Director Network
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NAME</th>
                        <th className="min-w-150px">DIN</th>
                        <th className="min-w-150px">PAN</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.director_network.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>{item && item.din ? item.din : "No Data"}</td>
                              <td>{item && item.pan ? item.pan : "No Data"}</td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Director Share Holdings
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">FULL NAME</th>
                        <th className="min-w-150px">DIN PAN</th>
                        <th className="min-w-150px">DESIGNATION</th>
                        <th className="min-w-150px">NUMBER OF SHARES</th>
                        <th className="min-w-150px">PERCENTAGE HOLDING</th>
                        <th className="min-w-150px">YEAR</th>
                        <th className="min-w-150px">FINANCIAL YEAR</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.director_shareholdings.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.full_name
                                  ? item.full_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.din_pan
                                  ? item.din_pan
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.designation
                                  ? item.designation
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.no_of_shares
                                  ? item.no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.percentage_holding
                                  ? item.percentage_holding
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                              <td>
                                {item && item.financial_year
                                  ? item.financial_year
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Establishments Registered With EPFO
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">ESTABLISHMENT NAME</th>
                        <th className="min-w-150px">ESTABLISHMENT ID</th>
                        <th className="min-w-150px">CITY</th>
                        <th className="min-w-150px">ADDRESS</th>
                        <th className="min-w-150px">DATE OF SETUP</th>
                        <th className="min-w-150px">
                          PRICIPAL BUSINESS ACTIVITIES
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .establishments_registered_with_epfo &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.establishments_registered_with_epfo.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.establishment_name
                                  ? item.establishment_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.establishment_id
                                  ? item.establishment_id
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.city ? item.city : "No Data"}
                              </td>
                              <td>
                                {item && item.address
                                  ? item.address
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_setup
                                  ? item.date_of_setup
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.principal_business_activities
                                  ? item.principal_business_activities
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Financial Parameters
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NATURE</th>
                        <th className="min-w-150px">PROPOSED</th>
                        <th className="min-w-150px">YEAR</th>
                        <th className="min-w-150px">
                          EMPLOYEE BENEFIT EXPENSE
                        </th>
                        <th className="min-w-150px">GROSS FIXED ASSETS</th>
                        <th className="min-w-150px">
                          PRESCRIBED CSR EXPENDITURE
                        </th>
                        <th className="min-w-150px">
                          TOTAL AMOUNT CSR SPENT FOR FINANCIAL YEAR
                        </th>
                        <th className="min-w-150px">
                          TRANSACTION RELATED PARTIES AS 18
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .financial_parameters &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.financial_parameters.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.nature ? item.nature : "No Data"}
                              </td>
                              <td>
                                {item && item.proposed_dividend
                                  ? item.proposed_dividend
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                              <td>
                                {item && item.employee_benefit_expense
                                  ? item.employee_benefit_expense
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.gross_fixed_assets
                                  ? item.gross_fixed_assets
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.prescribed_csr_expenditure
                                  ? item.prescribed_csr_expenditure
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.total_amount_csr_spent_for_financial_year
                                  ? item.total_amount_csr_spent_for_financial_year
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.transaction_related_parties_as_18
                                  ? item.transaction_related_parties_as_18
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Financials
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NATURE</th>
                        <th className="min-w-150px">FILING STANDARD</th>
                        <th className="min-w-150px">FILING TYPE</th>
                        <th className="min-w-150px">STATED ON</th>
                        <th className="min-w-150px">YEAR</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .financials &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.financials.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.nature ? item.nature : "No Data"}
                              </td>
                              <td>
                                {item && item.filing_standard
                                  ? item.filing_standard
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.filing_type
                                  ? item.filing_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.stated_on
                                  ? item.stated_on
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  GST Details
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">COMPANY NAME</th>
                        <th className="min-w-150px">TRADE NAME</th>
                        <th className="min-w-150px">TAXPAYER TYPE</th>
                        <th className="min-w-150px">CENTRE JURISDICTION</th>
                        <th className="min-w-150px">GSTIN</th>
                        <th className="min-w-150px">
                          NATURE OF BUSINESS ACTIVITIES
                        </th>
                        <th className="min-w-150px">STATE</th>
                        <th className="min-w-150px">STATE JURISDICTION</th>
                        <th className="min-w-150px">STATUS</th>
                        <th className="min-w-150px">DATE OF REGISTRATION</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .gst_details &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.gst_details.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.company_name
                                  ? item.company_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.trade_name
                                  ? item.trade_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.taxpayer_type
                                  ? item.taxpayer_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.centre_jurisdiction
                                  ? item.centre_jurisdiction
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.gstin ? item.gstin : "No Data"}
                              </td>
                              <td>
                                {item && item.nature_of_business_activities
                                  ? item.nature_of_business_activities
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.state ? item.state : "No Data"}
                              </td>
                              <td>
                                {item && item.state_jurisdiction
                                  ? item.state_jurisdiction
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_registration
                                  ? item.date_of_registration
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Holding Entities
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">LEGAL NAME</th>
                        <th className="min-w-150px">CIN</th>
                        <th className="min-w-150px">CITY</th>
                        <th className="min-w-150px">ACTIVE COMPLIANCE</th>
                        <th className="min-w-150px">INCORPORATION DATE</th>
                        <th className="min-w-150px">STATUS</th>
                        <th className="min-w-150px">PAID UP CAPITAL</th>
                        <th className="min-w-150px">
                          SHARE HOLDING PERCENTAGE
                        </th>
                        <th className="min-w-150px">SUM OF CHARGES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .holding_entities &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .holding_entities.company &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.holding_entities.company.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.legal_name
                                  ? item.legal_name
                                  : "No Data"}
                              </td>
                              <td>{item && item.cin ? item.cin : "No Data"}</td>
                              <td>
                                {item && item.city ? item.city : "No Data"}
                              </td>
                              <td>
                                {item && item.active_compliance
                                  ? item.active_compliance
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.incorporation_date
                                  ? item.incorporation_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                              <td>
                                {item && item.paid_up_capital
                                  ? item.paid_up_capital
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.share_holding_percentage
                                  ? item.share_holding_percentage
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.sum_of_charges
                                  ? item.sum_of_charges
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Joint Ventures
                </span>
                <div className="text-dark fw-bold mb-1 fs-5 text-center">
                  Llp
                </div>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">LEGAL NAME</th>
                        <th className="min-w-150px">Llpin</th>
                        <th className="min-w-150px">CITY</th>
                        <th className="min-w-150px">INCORPORATION DATE</th>
                        <th className="min-w-150px">
                          SHARE HOLDING PERCENTAGE
                        </th>
                        <th className="min-w-150px">STATUS</th>
                        <th className="min-w-150px">SUM OF CHARGES</th>
                        <th className="min-w-150px">TOTAL OBLIGATION</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .joint_ventures &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .joint_ventures.llp &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.joint_ventures.llp.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.legal_name
                                  ? item.legal_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.llpin ? item.llpin : "No Data"}
                              </td>
                              <td>
                                {item && item.city ? item.city : "No Data"}
                              </td>
                              <td>
                                {item && item.incorporation_date
                                  ? item.incorporation_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.share_holding_percentage
                                  ? item.share_holding_percentage
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.status ? item.status : "No Data"}
                              </td>
                              <td>
                                {item && item.sum_of_charges
                                  ? item.sum_of_charges
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.total_obligation_of_contribution
                                  ? item.total_obligation_of_contribution
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Legal Cases Of Financial Disputes
                </span>
                <div className="text-dark fw-bold mb-1 fs-5 text-center">
                  Payable
                </div>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">LITIGANT</th>
                        <th className="min-w-150px">
                          TYPE OF FINANCIAL DISPUTE
                        </th>
                        <th className="min-w-150px">VERDICT</th>
                        <th className="min-w-150px">COURT</th>
                        <th className="min-w-150px">CASE NO</th>
                        <th className="min-w-150px">DATE OF JUDGEMENT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .legal_cases_of_financial_disputes &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .legal_cases_of_financial_disputes.payable &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.legal_cases_of_financial_disputes.payable.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.litigant
                                  ? item.litigant
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.type_of_financial_dispute
                                  ? item.type_of_financial_dispute
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.verdict
                                  ? item.verdict
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.court ? item.court : "No Data"}
                              </td>
                              <td>
                                {item && item.case_no
                                  ? item.case_no
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date_of_judgement
                                  ? item.date_of_judgement
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Legal History
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">COURT</th>
                        <th className="min-w-150px">CASE CATEGORY</th>
                        <th className="min-w-150px">CASE NUMBER</th>
                        <th className="min-w-150px">CASE STATUS</th>
                        <th className="min-w-150px">CASE TYPE</th>
                        <th className="min-w-150px">DATE</th>
                        <th className="min-w-150px">PETITIONER</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .legal_history &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.legal_history.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.court ? item.court : "No Data"}
                              </td>
                              <td>
                                {item && item.case_category
                                  ? item.case_category
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_number
                                  ? item.case_number
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_status
                                  ? item.case_status
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.case_type
                                  ? item.case_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                              <td>
                                {item && item.petitioner
                                  ? item.petitioner
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  MSME Supplier Payment Delays
                </span>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Latest Period
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.delays_for_period &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.delays_for_period
                          .latest_period
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .msme_supplier_payment_delays.delays_for_period
                              .latest_period
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center rounded p-2 mb-0 mx-3">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-dark fw-bold mb-1 fs-5">
                        Total Amount Due For Period
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-muted fw-semibold  text-gray-700 text-capital">
                        {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.delays_for_period &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.delays_for_period
                          .total_amount_due_for_period
                          ? matrixDetail
                              .probe_cinorpan_company_comprehensive_api
                              .msme_supplier_payment_delays.delays_for_period
                              .total_amount_due_for_period
                          : "No Data"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="text-dark fw-bold mb-1 fs-5 text-center">
                  Delays For Period
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Delays
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">SUPPLIER NAME</th>
                        <th className="min-w-150px">SUPPLIER PAN</th>
                        <th className="min-w-150px">AMOUNT DUE FROM DATE</th>
                        <th className="min-w-150px">AMOUNT DUE</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.delays_for_period &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.delays_for_period
                          .delays &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.msme_supplier_payment_delays.delays_for_period.delays.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.supplier_name
                                  ? item.supplier_name
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.supplier_pan
                                  ? item.supplier_pan
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount_due_from_date
                                  ? item.amount_due_from_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.amount_due
                                  ? item.amount_due
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Trend
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">PERIOD</th>
                        <th className="min-w-150px">AMOUNT</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .msme_supplier_payment_delays.trend &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.msme_supplier_payment_delays.trend.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.period ? item.period : "No Data"}
                              </td>
                              <td>
                                {item && item.amount ? item.amount : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Name History
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">NAME</th>
                        <th className="min-w-150px">DATE</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .name_history &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.name_history.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.name ? item.name : "No Data"}
                              </td>
                              <td>
                                {item && item.date ? item.date : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Principal Business Activities
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">BUSINESS ACTIVITY CODE</th>
                        <th className="min-w-150px">
                          MAIN ACTIVITY GROUP CODE
                        </th>
                        <th className="min-w-150px">
                          MAIN ACTIVITY GROUP DESCRIPTION
                        </th>
                        <th className="min-w-150px">
                          BUSINESS ACTIVITY DESCRIPTION
                        </th>
                        <th className="min-w-150px">PERCENTAGE OF TRUN OVER</th>
                        <th className="min-w-150px">YEAR</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .principal_business_activities &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.principal_business_activities.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.business_activity_code
                                  ? item.business_activity_code
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.main_activity_group_code
                                  ? item.main_activity_group_code
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.main_activity_group_description
                                  ? item.main_activity_group_description
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.business_activity_description
                                  ? item.business_activity_description
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.percentage_of_turnover
                                  ? item.percentage_of_turnover
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Securities Allotment
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">INSTRUMENT</th>
                        <th className="min-w-150px">ALLOTMENT TYPE</th>
                        <th className="min-w-150px">ALLOTMENT DATE</th>
                        <th className="min-w-150px">
                          NOMINAL AMOUNT PER SECURITY
                        </th>
                        <th className="min-w-150px">NUMBER OF SECURITIES</th>
                        <th className="min-w-150px">PREMIUM AMOUNT RAISED</th>
                        <th className="min-w-150px">TOTAL AMOUNT RAISED</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .securities_allotment &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.securities_allotment.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.instrument
                                  ? item.instrument
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.allotment_type
                                  ? item.allotment_type
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.allotment_date
                                  ? item.allotment_date
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.nominal_amount_per_security
                                  ? item.nominal_amount_per_security
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.number_of_securities_allotted
                                  ? item.number_of_securities_allotted
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.premium_amount_per_security
                                  ? item.premium_amount_per_security
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.total_amount_raised
                                  ? item.total_amount_raised
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Share Holdings
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">SHARE HOLDER</th>
                        <th className="min-w-150px">CATEGORY</th>
                        <th className="min-w-150px">YEAR</th>
                        <th className="min-w-150px">FINANCIAL YEAR</th>
                        <th className="min-w-150px">
                          TOTAL PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">TOTAL NO OF SHARES</th>
                        <th className="min-w-150px">BANK HELD NO OF SHARES</th>
                        <th className="min-w-150px">
                          BANK HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          BODY CORPORATE HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          BODY CORPORATE HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          CENTRAL GOVERMENT HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          CENTRAL GOVERMENT HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          FINANCIAL INSTITUTION HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          FINANCIAL INSTITUTION HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          FINANCIAL INSTITUTION INVESTORS HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          FINANCIAL INSTITUTION INVESTORS HELD PERCENTAGE OF
                          SHARES
                        </th>
                        <th className="min-w-150px">
                          FOREIGN HELD OTHER THAN NRI NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          FOREIGN HELD OTHER THAN NRI PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          GOVERMENT COMPANY HELD NO SHARES
                        </th>
                        <th className="min-w-150px">
                          GOVERMENT COMPANY HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          INDIAN HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          INDIAN HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          INSURANCE COMPANY HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          INSURANCE COMPANY HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          MUTUAL FUNDS HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          MUTUAL FUNDS HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">NRI HELD NO OF SHARES</th>
                        <th className="min-w-150px">
                          NRI HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          OTHERS HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          OTHERS HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          STATE GOVERMENT HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          STATE GOVERMENT HELD PERCENTAGE OF SHARES
                        </th>
                        <th className="min-w-150px">
                          VENTURES CAPITAL HELD NO OF SHARES
                        </th>
                        <th className="min-w-150px">
                          VENTURES CAPITAL HELD PERCENTAGE OF SHARES
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .shareholdings &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.shareholdings.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.shareholders
                                  ? item.shareholders
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.category
                                  ? item.category
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                              <td>
                                {item && item.financial_year
                                  ? item.financial_year
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.total_percentage_of_shares
                                  ? item.total_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.total_no_of_shares
                                  ? item.total_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.bank_held_no_of_shares
                                  ? item.bank_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.bank_held_percentage_of_shares
                                  ? item.bank_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.body_corporate_held_no_of_shares
                                  ? item.body_corporate_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.body_corporate_held_percentage_of_shares
                                  ? item.body_corporate_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.central_government_held_no_of_shares
                                  ? item.central_government_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.central_government_held_percentage_of_shares
                                  ? item.central_government_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.financial_institutions_held_no_of_shares
                                  ? item.financial_institutions_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.financial_institutions_held_percentage_of_shares
                                  ? item.financial_institutions_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.financial_institutions_investors_held_no_of_shares
                                  ? item.financial_institutions_investors_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.financial_institutions_investors_held_percentage_of_shares
                                  ? item.financial_institutions_investors_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.foreign_held_other_than_nri_no_of_shares
                                  ? item.foreign_held_other_than_nri_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.foreign_held_other_than_nri_percentage_of_shares
                                  ? item.foreign_held_other_than_nri_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.government_company_held_no_shares
                                  ? item.government_company_held_no_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.government_company_held_percentage_of_shares
                                  ? item.government_company_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.indian_held_no_of_shares
                                  ? item.indian_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.indian_held_percentage_of_shares
                                  ? item.indian_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.insurance_company_held_no_of_shares
                                  ? item.insurance_company_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.insurance_company_held_percentage_of_shares
                                  ? item.insurance_company_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.mutual_funds_held_no_of_shares
                                  ? item.mutual_funds_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.mutual_funds_held_percentage_of_shares
                                  ? item.mutual_funds_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.nri_held_no_of_shares
                                  ? item.nri_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.nri_held_percentage_of_shares
                                  ? item.nri_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.others_held_no_of_shares
                                  ? item.others_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.others_held_percentage_of_shares
                                  ? item.others_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.state_government_held_no_of_shares
                                  ? item.state_government_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.state_government_held_percentage_of_shares
                                  ? item.state_government_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.venture_capital_held_no_of_shares
                                  ? item.venture_capital_held_no_of_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item &&
                                item.venture_capital_held_percentage_of_shares
                                  ? item.venture_capital_held_percentage_of_shares
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
                <span className="pl-5 text-dark fw-bolder mb-1 fs-5">
                  Share Holdings Summary
                </span>
                <div className="table-responsive px-4">
                  <table className="table table-hover table-rounded table-striped border gs-2">
                    <thead>
                      <tr class="fw-bold fs-6 text-gray-800">
                        <th className="min-w-150px">PROMOTER</th>
                        <th className="min-w-150px">YEAR</th>
                        <th className="min-w-150px">FINANCIAL YEAR</th>
                        <th className="min-w-150px">PUBLIC</th>
                        <th className="min-w-150px">TOTAL</th>
                        <th className="min-w-150px">TOTAL PREFERENCE SHARES</th>
                        <th className="min-w-150px">TOTAL EQUITY SHARES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {matrixDetail &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api
                          .shareholdings_summary &&
                        matrixDetail.probe_cinorpan_company_comprehensive_api.shareholdings_summary.map(
                          (item, index) => (
                            <tr>
                              <td>
                                {item && item.promoter
                                  ? item.promoter
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.year ? item.year : "No Data"}
                              </td>
                              <td>
                                {item && item.financial_year
                                  ? item.financial_year
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.public ? item.public : "No Data"}
                              </td>
                              <td>
                                {item && item.total ? item.total : "No Data"}
                              </td>
                              <td>
                                {item && item.total_preference_shares
                                  ? item.total_preference_shares
                                  : "No Data"}
                              </td>
                              <td>
                                {item && item.total_equity_shares
                                  ? item.total_equity_shares
                                  : "No Data"}
                              </td>
                            </tr>
                          )
                        )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        ""
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  const { EddReTriggerFinalApiUpdateStore } = state;
  return {
    ReTriggerFinalApi:
      EddReTriggerFinalApiUpdateStore &&
      EddReTriggerFinalApiUpdateStore.ReTriggerFinalApiRes
        ? EddReTriggerFinalApiUpdateStore.ReTriggerFinalApiRes
        : {},
    loading:
      EddReTriggerFinalApiUpdateStore && EddReTriggerFinalApiUpdateStore.loading
        ? EddReTriggerFinalApiUpdateStore.loading
        : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  retriggerFinalApiDispatch: (params) =>
    dispatch(ReTriggerFinalApiUpdateActions.getReTriggerFinalApiUpdate(params)),
  clearRetriggerFinalApiDispatch: () =>
    dispatch(ReTriggerFinalApiUpdateActions.clearReTriggerFinalApiUpdate()),
  getIdMerchantDispatch: (id) =>
    dispatch(eddMerchantIdDetailsActions.getEddMerchantIdDetailsData(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EddReports);
