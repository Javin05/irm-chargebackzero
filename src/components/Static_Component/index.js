import React from "react";
import { Switch, Route } from "react-router-dom";
import StaticComponent from "../kycDashboard/StaticComponent";

function Static_Component(props) {
  return (
    <Switch>
      <Route path="/static-summary/:id">
        <StaticComponent />
      </Route>
    </Switch>
  );
}

export default Static_Component;
