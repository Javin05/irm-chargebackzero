import React, { useState, useEffect, Fragment } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import _ from 'lodash'

function WrmTable(props) {
    const { tableData, statusChange } = props;

    const options = {
        sizePerPageList: [5, 10, 15, 20],
        sizePerPage: 15,
        page: 1,
        noDataText: "Data not available for this search ....",
    };

    function getCaret(direction) {
        if (direction === "asc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-up-circle-fill text-primary"></i>
                </span>
            );
        }
        if (direction === "desc") {
            return (
                <span className="ml-2">
                    <i className="bi bi-arrow-down-circle-fill text-primary"></i>
                </span>
            );
        }
        return (
            <span className="ml-2">
                <i className="bi bi-arrow-down-up"></i>
            </span>
        );
    }

    function wrmIdFormatter(cell) {
        if (!cell) {
            return "--";
        }
        return cell && `${'WRM'}${cell}`;
    }

    return (
        <Fragment>
            {tableData && (
                <Fragment>
                    <BootstrapTable
                        options={options}
                        data={tableData}
                        pagination={true}
                        table
                        //striped
                        hover
                    >
                        <TableHeaderColumn
                            width="80"
                            dataField="riskId"
                            dataSort={true}
                            caretRender={getCaret}
                            dataFormat={wrmIdFormatter}
                            isKey
                        >
                            Case ID
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="website"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Website
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="acquirer"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Acquirer
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="tag"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Tag
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="reason"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Reason
                        </TableHeaderColumn>
                        <TableHeaderColumn width="150" dataField="riskClasification">
                            Risk classification
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="riskScore"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Risk score
                        </TableHeaderColumn>
                        <TableHeaderColumn
                            width="150"
                            dataField="riskLevel"
                            dataSort={true}
                            caretRender={getCaret}
                        >
                            Risk Level
                        </TableHeaderColumn>
                    </BootstrapTable>
                </Fragment>
            )}
        </Fragment>
    );
}
export default WrmTable;
