import _ from 'lodash'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import Modal from 'react-bootstrap/Modal'
import ReactPaginate from 'react-paginate'
import { connect } from 'react-redux'
import { FeedBackActions, SendFeedBackactions, FeedBackViewActions, NotificationFeedBackActions, FeedBackCloseActions, clientIdLIstActions } from '../../store/actions'
import { KTSVG, toAbsoluteUrl } from '../../theme/helpers'
import { successAlert, warningAlert } from '../../utils/alerts'
import { RISKSTATUS, STATUS_RESPONSE } from '../../utils/constants'
import { getLocalStorage } from '../../utils/helper'

const FeedBack = (props) => {
  const {
    notificationFeedBackData,
    loading,
    getFeedBackChatList,
    clearFeedBackChatList,
    sendFeedBack,
    getFeedBackList,
    sendFeedBackResponse,
    getFeedBackView,
    sendFeedBackResponseLoading,
    getNotificationFeedBackDispatch,
    closeFeedBackDispatch,
    closeFeedBackResponseLoading,
    closeFeedBackResponse,
    clearCloseFeedBackDispatch,
    clinetIdLists,
    clientIdDispatch,
    clearSendFeedBack
  } = props
  const [limit, setLimit] = useState(25)
  const [refresh, setRefresh] = useState(false)
  const [search, setSearch] = useState(false)
  const [feedBackId, setFeedBackId] = useState('')
  const [feedBackDashId, setFeedBackDashId] = useState('')
  const [feedBackData, setFeedBackData] = useState('')
  const [feedBackModal, setFeedBackModal] = useState(false)
  const [showSend, setShowSend] = useState(false)
  const [formData, setFormData] = useState({
    tag: '',
    website: '',
    clientId:''
  })
  const [, setShow] = useState(false)
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  useEffect(() => {
    if(!_.isEmpty(formData.tag) || !_.isEmpty(formData.website) || !_.isEmpty(formData.clientId)){
      setSearch(true)
    } else {
      setSearch(false)
      setRefresh(false)
    }
  },[formData])

  const handleSearch = () => {
    setShow(false)
    const params = {
      tag: formData.tag.trim(),
      website: formData.website.trim(),
      clientId: formData.clientId.trim(),
      limit: 25,
      page: 1
    }
    if(!_.isEmpty(formData.tag) || !_.isEmpty(formData.website) || !_.isEmpty(formData.clientId)){
      setRefresh(true)
    }
    getNotificationFeedBackDispatch(params)
  }

  const handleReset = () => {
    setShow(false)
    setFormData({
      tag: '',
      website: '',
      clientId:''
    })
    setRefresh(false)
    setSearch(false)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
  }

  const totalPages =
    notificationFeedBackData && notificationFeedBackData.data && notificationFeedBackData.data.count
      ? Math.ceil(parseInt(notificationFeedBackData && notificationFeedBackData.data && notificationFeedBackData.data.count) / limit)
      : 1

  const handleFeedBackModal = (riskmgmtlist) => {
    const id = riskmgmtlist && riskmgmtlist._id
    const riskId = riskmgmtlist && riskmgmtlist.riskId
    setFeedBackId(riskmgmtlist && riskmgmtlist.riskId)
    setFeedBackDashId(riskmgmtlist)
    setFeedBackModal(true)
    getFeedBackView(id)
    getFeedBackChatList(riskId)
  }

  const handleAcceptFeedback = () => {
    const id = feedBackDashId && feedBackDashId._id
    closeFeedBackDispatch(id)
  }

  const handleChatList = () => {
    const data = {
      feedback: feedBackData,
      riskId: feedBackId
    }
    sendFeedBack(data)
  }

  // const scrollToBottom = () => {
  //   const element = document.getElementById('feed-back-child');
  //   element.scrollTop = element.scrollHeight;
  // }

  useEffect(() => {
    if (sendFeedBackResponse && sendFeedBackResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      // getFeedBackChatList(feedBackId) // riskmanagement/feedback GET_ALL(Wrongly call api)
      const params = {
        limit: 25,
        page: 1
      }
      getNotificationFeedBackDispatch(params);
      clearSendFeedBack()
      setFeedBackData('')
      setShowSend(false)
      // scrollToBottom()
    } else if (sendFeedBackResponse && sendFeedBackResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        sendFeedBackResponse && sendFeedBackResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
    }
  }, [sendFeedBackResponse])

  useEffect(() => {
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params);
    return() => {
      clearCloseFeedBackDispatch()
    }
  },[])

  useEffect(() => {
    if (closeFeedBackResponse && closeFeedBackResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      getFeedBackChatList(feedBackId)
      setFeedBackData('')
      setShowSend(false)
      setFeedBackModal(false) 
      successAlert(
        closeFeedBackResponse && closeFeedBackResponse.message,
        'success'
      )
      const params = {
        limit: 25,
        page: 1
      }
      getNotificationFeedBackDispatch(params)
      clearCloseFeedBackDispatch()
    } else if (closeFeedBackResponse && closeFeedBackResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        closeFeedBackResponse && closeFeedBackResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
    }
  }, [closeFeedBackResponse])

  useEffect(() => {
    if (feedBackData && feedBackData.length >= 1) {
      setShowSend(true)
    } else {
      setShowSend(false)
    }
  }, [feedBackData])

  return (
    <>
      <Modal
        show={feedBackModal}
        size="lg"
        centered
        onHide={() => {
          setTimeout(() => {
            clearFeedBackChatList()
          }, 500)
          setFeedBackModal(false)
        }}>
        <Modal.Header
          closeButton={() => {
            setTimeout(() => {
              clearFeedBackChatList()
            }, 500)
            setFeedBackModal(false)
          }}>
          <Modal.Title
            style={{
              color: 'black'
            }}
          >
            Feedback model
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {getFeedBackList && getFeedBackList.status === STATUS_RESPONSE.ERROR_MSG ?
            <div className='text-center'>
              <i className="bi bi-envelope-exclamation fs-1"></i>
              <div>{getFeedBackList && getFeedBackList.message}</div>
            </div> :
            getFeedBackList && getFeedBackList.status === STATUS_RESPONSE.SUCCESS_MSG ?
              <div id='feed-back-child' className='feed-back-modal px-2'>
                {getFeedBackList && getFeedBackList.data && getFeedBackList.data.map((item, index) => item && item.viewSide === "right" ?
                  <div dir='rtl' className='mt-4' key={index}>
                    <div className='d-flex align-items-center'>
                      <img src={toAbsoluteUrl('/media/avatars/150-2.jpg')} alt='metronic' className='feedback-image' />
                      <div dir='ltr' className='mx-3 text-dark'>{item && item.senderName}</div>
                      <div dir='ltr' className='text-muted'>{moment(item && item.createdAt).format('DD-MM-YYYY hh:mm')}</div>
                    </div>
                    <div dir='ltr' className='text-dark bg-light-primary p-5 rounded mt-1 w-60'>{item && item.feedback}</div>
                    <div className='text-end'>{item && item.viewStatus === 'VIEWED' ? <i className="bi bi-eye-fill"></i> : null}</div>
                  </div> :
                  item && item.viewSide === "left" ?
                    <div className='mt-4' key={index}>
                      <div className='d-flex align-items-center'>
                        <img src={toAbsoluteUrl('/media/avatars/300-25.jpg')} alt='metronic' className='feedback-image' />
                        <div className='mx-3 text-dark'>{item && item.senderName}</div>
                        <div className='text-muted'>{moment(item && item.createdAt).format('DD-MM-YYYY hh:mm')}</div>
                      </div>
                      <div className='text-dark bg-light-info p-5 rounded mt-1 w-60'>{item && item.feedback}</div>
                      <div>{item && item.viewStatus === 'VIEWED' ? <i className="bi bi-eye-fill"></i> : null}</div>
                    </div> : null
                )}
              </div> :
              <div className='text-center'>
                <div
                  className='spinner-border text-primary m-5'
                  role='status'
                />
                <div>Loading Feedback....</div>
              </div>}
          {feedBackDashId && feedBackDashId.feedbackStatus === "STARTED" ?
            <div className='text-start'>
              <button className='btn btn-success btn-sm mt-2' onClick={handleAcceptFeedback} disabled={closeFeedBackResponseLoading}>Accept and Close</button>
            </div> : null}
          {feedBackDashId && feedBackDashId.feedbackStatus === "CLOSED" ? null :
            <div className='position-sticky position-chat-sticky mt-2'>
              <textarea
                name="reason"
                type="text"
                placeholder='Type a feedback here...'
                className='feedback-chat w-90 border-0'
                onChange={(e) => {
                  setFeedBackData(e.target.value)
                }}
                autoComplete="off"
                autoFocus
                value={feedBackData || ""}
              />
              <button className='btn btn-primary btn-sm position-absolute end-0 mt-2' onClick={handleChatList} disabled={!showSend || sendFeedBackResponseLoading}>Send</button>
            </div>}
        </Modal.Body>
      </Modal>
      <div className={`card`}>
        <div className='card-body py-3'>
          <div className='p-5 pb-0 text-end'>
            <button
              type='button'
              className='btn btn-sm btn-light-primary btn-responsive font-5vw'
              data-toggle='modal'
              data-target='#searchModal'
              onClick={() => { setShow(true) }}
            >
              {/* eslint-disable */}
              <KTSVG path='/media/icons/duotune/general/gen021.svg' />
              {/* eslint-disable */}
              Search
            </button>
            <div
              className='modal fade'
              id='searchModal'
              tabIndex='-1'
              role='dialog'
              aria-labelledby='exampleModalLabel'
              aria-hidden="''"
              data-backdrop="static"
              data-keyboard="false"
            >
              <div className='modal-dialog modal-dialog-centered mw-800px'>
                <div className='modal-content'>
                  <div className='modal-header'>
                    <h2 className='me-8'>Search</h2>
                    <button type="button"
                      data-repeater-delete=""
                      className="btn btn-sm btn-icon btn-light-danger"
                      data-dismiss='modal'
                      onClick={() => { handleReset() }}
                    >
                      <span className="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                          <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor"></rect>
                          <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor"></rect>
                        </svg>
                      </span>
                    </button>
                  </div>
                  <div className='modal-body bg-lightBlue'>
                    <div className='form-group row mb-4 align-items-center'>
                      <div className='col-lg-3 text-start'>
                        <label className='font-size-xs font-weight-bold form-label'>
                          Website:
                        </label>
                      </div>
                      <div className='col-lg-9'>
                        <input
                          name='website'
                          type='text'
                          className='form-control'
                          placeholder='Website'
                          onChange={(e) => handleChange(e)}
                          autoComplete='off'
                          value={formData.website || ''}
                        />
                      </div>
                    </div>
                    <div className='form-group row mb-4 align-items-center'>
                      <div className='col-lg-3 text-start'>
                        <label className='font-size-xs font-weight-bold form-label'>
                          Tag:
                        </label>
                      </div>
                      <div className='col-lg-9'>
                        <input
                          name='tag'
                          type='text'
                          className='form-control'
                          placeholder='Tag'
                          onChange={(e) => handleChange(e)}
                          autoComplete='off'
                          value={formData.tag || ''}
                        />
                      </div>
                    </div>
                    <div className='form-group row mb-4 align-items-center'>
                      <div className='col-lg-3 text-start'>
                        <label className='font-size-xs font-weight-bold form-label'>
                          Client id:
                        </label>
                      </div>
                      <div className='col-lg-9'>
                        <select
                          name="clientId"
                          className="form-select form-select-solid"
                          data-control="select"
                          data-placeholder="Select an option"
                          data-allow-clear="true"
                          onChange={(e) => handleChange(e)}
                          value={formData.clientId || ""}
                        >
                          <option value="">Select...</option>
                          {clinetIdLists && clinetIdLists.data && clinetIdLists.data.result.map((ele) => (
                            <option value={ele._id} key={ele._id}>
                              {ele.company}
                            </option>
                          ))}
                        </select>
                      </div>
                    </div>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-6' />
                      <div className='col-lg-6'>
                        <div className='col-lg-12'>
                          {search ? 
                          <button
                            type='button'
                            className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                            onClick={() => handleSearch()}
                            data-dismiss='modal'
                          >
                            Search
                          </button> : null}
                          {refresh ? <button
                            type='button'
                            className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                            onClick={() => handleReset()}
                            data-dismiss='modal'
                          >
                            Reset
                          </button> : null}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Case ID</span>
                      {/* <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.deviceID
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div> */}
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Website</span>
                      {/* <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("website")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div> */}
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Tag</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Feedback Status</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Feedback</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      notificationFeedBackData &&
                        notificationFeedBackData.data
                        ? (
                          notificationFeedBackData.data && notificationFeedBackData.data.map((riskmgmtlist, i) => {
                            const indiaDate = moment.utc(riskmgmtlist && riskmgmtlist.createdAt).local()
                            const todayDate = moment()
                            const duration = moment.duration(todayDate.diff(indiaDate))
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <span>
                                    WRM-{riskmgmtlist.riskId ? riskmgmtlist.riskId : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  <a
                                    className='color-primary cursor-pointer'
                                    onClick={() => window.open(riskmgmtlist.website ? riskmgmtlist.website : "--", "_blank")}
                                  >
                                    {
                                      riskmgmtlist.website ? riskmgmtlist.website : "--"
                                    }
                                  </a>
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist.tag ? riskmgmtlist.tag : "--"}
                                </td>
                                <td className="ellipsis">
                                    <span className={`badge ${RISKSTATUS[riskmgmtlist && riskmgmtlist.feedbackStatus]}`}>
                                      {riskmgmtlist.feedbackStatus ? riskmgmtlist.feedbackStatus : "--"}
                                    </span>
                                </td>
                                <td className="ellipsis">
                                  {riskmgmtlist && riskmgmtlist.feedbackStatus === "CLOSED" ? 
                                  <button className="btn btn-primary-outline position-relative" onClick={() => handleFeedBackModal(riskmgmtlist)}>
                                    <i className="bi bi-x text-danger fs-2"></i>
                                </button>:
                                  <button className="btn btn-primary-outline position-relative" onClick={() => handleFeedBackModal(riskmgmtlist)}>
                                    <i className="bi bi-chat-dots-fill fs-3 text-primary"></i>
                                  </button>}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { NotificationFeedBackStore, FeedBackStore, SendFeedbackStore, FeedBackCloseViewStore, clinetListStore } = state
  return {
    notificationFeedBackData: NotificationFeedBackStore && NotificationFeedBackStore.NotificationFeedBackData ? NotificationFeedBackStore.NotificationFeedBackData : {},
    loading: NotificationFeedBackStore && NotificationFeedBackStore.loading ? NotificationFeedBackStore.loading : false,
    getFeedBackList: FeedBackStore && FeedBackStore.FeedBackData ? FeedBackStore.FeedBackData : '',
    getFeedBackLoading: FeedBackStore && FeedBackStore.loading ? FeedBackStore.loading : '',
    sendFeedBackResponse: SendFeedbackStore && SendFeedbackStore.SendFeedBackData ? SendFeedbackStore.SendFeedBackData : '',
    sendFeedBackResponseLoading: SendFeedbackStore && SendFeedbackStore.loading ? SendFeedbackStore.loading : '',
    closeFeedBackResponseLoading: FeedBackCloseViewStore && FeedBackCloseViewStore.loading ? FeedBackCloseViewStore.loading : '',
    closeFeedBackResponse: FeedBackCloseViewStore && FeedBackCloseViewStore.FeedBackClose ? FeedBackCloseViewStore.FeedBackClose : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists: "",
  }
}

const mapDispatchToProps = (dispatch) => ({
  getFeedBackChatList: (params) => dispatch(FeedBackActions.getFeedBackList(params)),
  clearFeedBackChatList: () => dispatch(FeedBackActions.clearFeedBackList()),
  sendFeedBack: (data) => dispatch(SendFeedBackactions.sendFeedBackPost(data)),
  clearSendFeedBack: () => dispatch(SendFeedBackactions.clearSendFeedBackPost()),
  getFeedBackView: (params) => dispatch(FeedBackViewActions.getFeedBackViewList(params)),
  getNotificationFeedBackDispatch: (params) => dispatch(NotificationFeedBackActions.getNotificationFeedBackList(params)),
  closeFeedBackDispatch: (params) => dispatch(FeedBackCloseActions.getCloseFeedBackViewList(params)),
  clearCloseFeedBackDispatch: () => dispatch(FeedBackCloseActions.clearCloseFeedBackViewList()),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FeedBack)