import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import {
  CircularProgressbar,
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import moment from "moment";
import { DATE } from "../../utils/constants";
import { Link, useLocation } from "react-router-dom";
import BusinessPhone from "../riskSummary/subComponent/businessPhone";
import _ from "lodash";
//import { ForceGraph2D, ForceGraph3D, ForceGraphVR, ForceGraphAR } from 'react-force-graph';
import * as d3 from "d3";
import Chart from "react-google-charts";
import AnyChart from "anychart-react";
import LineTo from "react-lineto";
// import Flow from './graph/Graph.js'
// import ReactFlow from 'react-flow-renderer';

const elements = [
  { id: "1", data: { label: "Node 1" }, position: { x: 250, y: 5 } },
  // you can also pass a React component as a label
  { id: "2", data: { label: <div>Node 2</div> }, position: { x: 100, y: 100 } },
  { id: "e1-2", source: "1", target: "2", animated: true },
];

const sankeyData = [
  ["From", "To", "Weight"],
  ["sundaram P", "Phone", 1],
  ["sundaram P", "Email", 1],
  ["sundaram P", "Website", 1],
  ["sundaram P", "IP Address", 1],
  ["sundaram P", "Device", 1],
  ["Phone", "IRM100101", 1],
  ["Phone", "IRM100111", 1],
  ["Phone", "IRM100123", 1],
  ["Phone", "IRM100122", 1],
  ["Email", "IRM100101", 1],
  ["Email", "IRM100121", 1],
  ["Email", "IRM100120", 1],
  ["Email", "IRM100119", 1],
  ["Email", "IRM100118", 1],
  ["Website", "IRM100117", 1],
  ["Website", "IRM100116", 1],
  ["Website", "IRM100113", 1],
  ["IP Address", "IRM100112", 1],
  ["IP Address", "IRM100114", 1],
  ["IP Address", "IRM100115", 1],
  ["IP Address", "IRM100113", 1],
  ["Device", "IRM100101", 1],
  ["Device", "IRM100111", 1],
  ["Device", "IRM100112", 1],
  ["Device", "IRM100113", 1],
];

// export const options = {
//   sankey: {
//     link: { color: { fill: "#d799ae" } },
//     node: {
//       colors: ["#a61d4c"],
//       label: { color: "#871b47" },
//     },
//   },
// };

export const options = {
  sankey: {
    node: {
      label: {
        fontName: "Times-Roman",
        fontSize: 14,
        color: "#871b47",
        bold: true,
        italic: true,
      },
    },

    // node: {
    //   // node colors will cycle thru array
    //   colors: [
    //     'magenta',
    //     'magenta',
    //     'cyan',
    //     'green',
    //     'yellow',
    //     'magenta',
    //     'green',
    //     'blue'
    //   ]
    // },
    // link: {
    //   colorMode: 'source'
    // }
  },
};

function Phone(props) {
  const {
    className,
    getRiskSummaryDispatch,
    loading,
    getRiskSummarys,
    phone,
    dashboardDetails,
    totalPhoneScore,
    kyc_dashboard_summary,
    kyc_dashboard_details,
    isLoading_kyc_dashboard_summary,
    AllDashboardRes
  } = props;

  const viewData =
    getRiskSummarys && getRiskSummarys && getRiskSummarys.data
      ? getRiskSummarys.data
      : [];
  const getData = viewData.filter((o) => (o ? o : null));
  const viewPhone = AllDashboardRes && AllDashboardRes.data && AllDashboardRes.data.phone ? AllDashboardRes.data.phone : '--'
  const blockListing = viewPhone && viewPhone.verify_phone_v2 ? viewPhone.verify_phone_v2 : "--";
  const blockListingCheck = viewPhone && viewPhone.blacklisted_phone_check ? viewPhone.blacklisted_phone_check : "--";

  const phonePositive =
    kyc_dashboard_details &&
      kyc_dashboard_details.data &&
      kyc_dashboard_details.data.phonepositive
      ? kyc_dashboard_details.data.phonepositive
      : "--"
  const phoneNegative =
    kyc_dashboard_details &&
      kyc_dashboard_details?.data &&
      kyc_dashboard_details.data.phonenegative
      ? kyc_dashboard_details.data.phonenegative
      : "--"
  const phoneTotal = kyc_dashboard_details && kyc_dashboard_details.data && kyc_dashboard_details.data.phoneTotalScore ? kyc_dashboard_details.data.phoneTotalScore : 0

  return (
    <div>
      <div>
        <div className="row mt-8">
          <div className="col-lg-4" />
          <div className="col-lg-4">
            <h1 className="d-flex justify-content-center mb-4">
              Individual Phone
            </h1>
          </div>
        </div>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={phoneTotal}
            text={`${phoneTotal}%`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              // pathColor: 'mediumseagreen',
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {phoneTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8 mt-8">
          <div className="col-lg-12">
            <h5 className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {isLoading_kyc_dashboard_summary ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(phonePositive) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    phonePositive &&
                    phonePositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {isLoading_kyc_dashboard_summary ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(phoneNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    phoneNegative &&
                    phoneNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </h5>
          </div>
        </div>
        <div className="row">
          <div className="mb-8">
            <a href="#" className="d-flex justify-content-center fs-2">
              CONFIDENCE MATRIX
            </a>
          </div>
        </div>
        <div className="row g-5 g-xl-8 mb-8">
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Block Listing
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Case
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListingCheck && blockListingCheck.case
                          ? blockListingCheck.case
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Method
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListingCheck && blockListingCheck.method
                          ? blockListingCheck.method
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Phone Number Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListingCheck && blockListingCheck.phone_number_status
                          ? blockListingCheck.phone_number_status
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Risk Score
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListingCheck && blockListingCheck.risk_score
                          ? blockListingCheck.risk_score
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListing && blockListing.name
                          ? blockListing.name
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Location
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        City
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListing && blockListing.city ? blockListing.city : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        country
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListing && blockListing.country ? blockListing.country : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Zip Code
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListing && blockListing.zip_code
                          ? blockListing.zip_code
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Region
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {blockListing && blockListing.region
                          ? blockListing.region
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">Sms</span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Country Code
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {
                          blockListing &&
                            blockListing.dialing_code ?
                            blockListing.dialing_code
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Line Type
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {
                          blockListing &&
                            blockListing.line_type ?
                            blockListing.line_type
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Phone Number
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {
                          blockListing &&
                            blockListing.formatted ?
                            blockListing.formatted
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Sms Domain
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {
                          blockListing &&
                            blockListing.sms_domain ?
                            blockListing.sms_domain
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Sms Email
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                      {
                          blockListing &&
                            blockListing.sms_email ?
                            blockListing.sms_email
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Current Phone Status
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Device Status
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {
                          blockListing &&
                            blockListing.active_status ?
                            blockListing.active_status
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Carrier
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {
                          blockListing &&
                            blockListing.carrier ?
                            blockListing.carrier
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Line Type
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {
                          blockListing &&
                            blockListing.line_type ?
                            blockListing.line_type
                            : "--"
                        }
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore } = state;
  return {
    getRiskSummarys: state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    dashboardDetails: dashboardStore && dashboardStore.dashboardDetails ? dashboardStore.dashboardDetails : {},
  }
};
const mapDispatchToProps = (dispatch) => ({
  // getRiskSummaryDispatch: (id) =>  dispatch(riskSummaryActions.getRiskSummary(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Phone);
