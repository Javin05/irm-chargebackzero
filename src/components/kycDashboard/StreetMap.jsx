import React, { useState, useEffect } from "react";
import { GoogleMap, StreetViewPanorama } from "@react-google-maps/api";
import { MAP } from '../../utils/constants'

function StreetMap(props) {
  const { mapData } = props
  const [viewDefault, setViewDefault] = useState()
  const mapContainerStyle = {
    height: "450px",
    width: "100%"
  }
  const center = {
    lat: 37.5247596,
    lng: -122.2583719
  }

  useEffect(() => {
    if (mapData) {
      let businessAddressLocation = {
        lat: mapData && mapData.lat,
        lng: mapData && mapData.long
      }
      setViewDefault(businessAddressLocation)
    }
  }, [mapData])

  return (
    <GoogleMap
      id="googlemap-streetview"
      mapContainerStyle={mapContainerStyle}
      zoom={17}
    center={viewDefault}
    >
      <StreetViewPanorama
        position={viewDefault}
        enableCloseButton={false}
        linksControl={false}
        addressControl={true}
        visible={true}
        motionTracking={true}
        motionTrackingControl={true}
      />
    </GoogleMap>
  );
}

export default StreetMap;