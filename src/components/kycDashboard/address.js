import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash";

import BusinessAddress from "../riskSummary/subComponent/businessAddress";
import MapGoogle from "../maps/MapGoogle";
import MapGoogleSingle from "../maps/MapGoogleSingle";
import StreetMap from "../maps/StreetMap";

function Address(props) {
  const { 
    summary, 
    dashboard, 
    isLoaded, 
    matrixDetail, 
    kyc_dashboard_details, 
    isLoading_kyc_dashboard_summary,
    AllDashboardRes
  } = props;

  const addressPositive =
    kyc_dashboard_details &&
      kyc_dashboard_details.data &&
      kyc_dashboard_details.data.businessAddresspositive
      ? kyc_dashboard_details.data.businessAddresspositive
      : "--"
  const addressNegative =
    kyc_dashboard_details &&
      kyc_dashboard_details?.data &&
      kyc_dashboard_details.data.businessAddressnegative
      ? kyc_dashboard_details.data.businessAddressnegative
      : "--"

  const addressTotal = kyc_dashboard_details && kyc_dashboard_details.data && kyc_dashboard_details.data.businessAddressTotalScore ? kyc_dashboard_details.data.businessAddressTotalScore : 0
  const viewAddress = AllDashboardRes && AllDashboardRes.data && AllDashboardRes.data.address && AllDashboardRes.data.address.address_check ? AllDashboardRes.data.address.address_check : '--'

  let allMarkers = [];
  if (viewAddress) {
    allMarkers.push({
      lat: viewAddress && viewAddress.lattitude,
      lng: viewAddress && viewAddress.longitude,
      area: "BUSINESSADDRESS"
    })
  }

  return (
    <>
      <div className="container-fixed">
        <h1 className="d-flex justify-content-center mb-4 mt-8">
          Business Address
        </h1>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={addressTotal}
            text={`${addressTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {addressTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8">
          <div className="col-lg-12">
            <div className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {isLoading_kyc_dashboard_summary ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(addressPositive) ? (
                    <span className=" text-danger " role="status">
                      N/A
                    </span>
                  ) : (
                    addressPositive &&
                    addressPositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {isLoading_kyc_dashboard_summary ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(addressNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    addressNegative &&
                    addressNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8 mb-12">
          <div className="col-lg-3">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="card-title align-items-start flex-column ">
                  <span className="card-label fw-bolder text-dark">
                    Address
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Resident Name
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {viewAddress && viewAddress.businessName
                          ? viewAddress.businessName
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Address
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {viewAddress && viewAddress.address
                          ? viewAddress.address
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8">
          <div className="col-lg-6">
            <div className="card w-744px h-450px">
              { !_.isEmpty(allMarkers) ? (
                <MapGoogle mapMarkers={allMarkers} address={"address"} />
              ) : null}
            </div>
          </div>
          <div className="col-lg-6">
            <div
              style={{
                height: "450px",
                backgroundColor: "#eeeeee",
              }}
            >
              {!_.isEmpty(allMarkers) ? <StreetMap mapData={allMarkers} /> : null}
            </div>
          </div>
        </div>
      </div>
      {/* <div className="mt-10">
        <BusinessAddress
          summary={summary}
          dashboard={dashboard}
          splitData={matrixDetail}
          isLoaded={false}
        />
      </div> */}
    </>
  );
}

export default Address;
