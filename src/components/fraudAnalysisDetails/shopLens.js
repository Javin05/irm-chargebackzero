import React from "react";
import inhouseimg from "../../images/shared image.jpg"

const Shoplens = () => {
  return (
    <>
      <div className="container-fixed">
        <div className="row">
          {/* Left Section */}
          <div className="col-5 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">Shop Lens View</span>
                      <span className="text-muted">December 13, 2024</span>
                    </div>
                    
                    <div className="mt-5">
                      <img
                        src="https://content3.jdmagicbox.com/bangalore/p7/080pxx80.xx80.140607121130.w2p7/catalogue/pets-store-sanjay-nagar-bangalore-pet-shops-for-dog-9.jpg"
                        className="img-fluid"
                        alt="Property Image"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Right Section */}
          <div className="col-7 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">Details:</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Shop name:</span>
                      <span className="text-muted">Pets store .in</span>
                    </div>
                    <div className="d-flex justify-content-between">
                      <span className="h5">Shop name (English):</span>
                      <span className="text-muted">Pets store .in</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Shop name Visible:</span>
                      <span className="text-muted">True</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Shop board type:</span>
                      <span className="text-muted">Permanent</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Shop Movable:</span>
                      <span className="text-muted">No Data</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Shop Category:</span>
                      <span className="text-muted">Pet Supplies and Services</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Mcc Code:</span>
                      <span className="text-muted">5995</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Confidence Score:</span>
                      <span className="text-muted">90</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Shop Category 2:</span>
                      <span className="text-muted">Pet Veterinary Services</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Mcc Code 2:</span>
                      <span className="text-muted">0742</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Status:</span>
                      <span className="text-muted">Completed</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Message:</span>
                      <span className="text-muted">No Data</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Data Table Section */}
        <div className="container-fixed mt-5">
          <div className="card">
            <div className="card-header">
              <span className="h5 mt-5">Data Table</span>
            </div>
            <div className="card-body">
              <table className="table table-bordered text-center">
                <thead>
                  <tr>
                    <th className="font-weight-bold">S.No</th>
                    <th className="font-weight-bold">Fields</th>
                    <th className="font-weight-bold">Client Input</th>
                    <th className="font-weight-bold">API Data</th>
                    <th className="font-weight-bold">Result</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <th>Business Name</th>
                    <td>Pets store.in</td>
                    <td>Pets store.in</td>
                    <td><span className="badge bg-success">match</span></td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Category</td>
                    <td>pet shop</td>
                    <td>pet shop</td>
                    <td><span className="badge bg-success">match</span></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fixed mt-5">
        <div className="row">
          {/* Left Section */}
          <div className="col-5 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">Street View Snapshot</span>
                      <span className="text-muted">December 13, 2024</span>
                    </div>
                    <div className="d-flex justify-content-start mt-3">
                      <strong>Type of Property:</strong>
                    </div>
                    <div className="d-flex justify-content-start mt-2">
                      <span className="badge badge-secondary">
                        <i className="fas fa-building"></i> Apartment
                      </span>
                      <span className="badge badge-secondary mr-3">
                        <i className="fas fa-check-circle"></i> Not Vacant
                      </span>
                      <span className="badge badge-secondary">
                        <i className="fas fa-check-circle"></i> Deliverables
                      </span>
                    </div>
                    <div className="mt-5">
                    <iframe
                      src="https://www.google.com/maps/embed?pb=!4v1699763762822!6m8!1m7!1sGdR69Ax8_YHHwKFtQDmmwg!2m2!1d13.034411!2d77.5759616!3f93.9!4f88.03!5f0.7820865974627469"
                      width="100%"
                      height="400"
                      style={{ border: "0" }}
                      allowFullScreen
                      loading="lazy"
                    ></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Right Section */}
          <div className="col-7 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">Signages:</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span>Is the business name or DBA displayed on signages</span>
                      <span className="text-muted">Yes</span>
                    </div>
                    <div className="d-flex justify-content-between">
                      <span>Are there any other business names mentioned on signages:</span>
                      <span className="text-muted">Yes</span>
                    </div>
                    <hr />
                    <div className="mt-5">
                      <span className="text-muted">Other Names Mentioned</span><br />
                      <strong>1. Pets shop</strong>
                    </div>
                    <hr />
                    <div className="mt-5">
                      <span className="text-muted">Product or service based on the signages</span><br />
                      <strong>1. Pets shop</strong><br />
                      <strong>2. Pet Food and Accessories</strong><br />
                      <strong>3. Pet Care Services</strong><br />
                    </div>
                    <hr />
                    <div className="mt-5">
                      <span className="h5">Location insights</span>
                      <div className="row mt-3">
                        {/* First Box */}
                        <div className="col-md-6">
                          <div className="p-3 bg-light rounded d-flex justify-content-between align-items-center shadow-sm">
                            <div>
                              <small className="text-muted">No. of vehicles found parked</small>
                              <div className="h4 mb-0"><i className="fas fa-bus"></i> 2</div>
                            </div>
                          </div>
                        </div>
                        {/* Second Box */}
                        <div className="col-md-6">
                          <div className="p-3 bg-light rounded d-flex justify-content-between align-items-center shadow-sm">
                            <div>
                              <small className="text-muted">No. of parking spaces found</small>
                              <div className="h4 mb-0"><i className="fas fa-parking"></i> 3</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row mt-3">
                        {/* Third Box */}
                        <div className="col-md-12">
                          <div className="p-3 bg-light rounded d-flex justify-content-between align-items-center shadow-sm">
                            <div>
                              <small className="text-muted">
                                No. of vehicles found parked with the name of the business on it
                              </small>
                              <div className="h4 mb-0"><i className="fas fa-truck"></i> 4</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fixed mt-5">
        <div className="row">
          {/* Left Section */}
          <div className="col-5 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">Shop Inventory View</span>
                      <span className="text-muted">December 13, 2024</span>
                    </div>
                    
                    <div className="mt-5">
                      <img
                        src={inhouseimg}
                        className="img-fluid"
                        alt="Property Image"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Right Section */}
          <div className="col-7 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">Summary of Inventory Value on the Shelf:</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Item Names:</span>
                      <span className="text-muted">Calcium, Puropet, Me-O</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between">
                      <span className="h3">Quantity:</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Calcium:</span>
                      <span className="text-muted">12 bottles</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Puropet:</span>
                      <span className="text-muted">10 bags</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Me-O:</span>
                      <span className="text-muted">22 Bags</span>
                    </div>
                    <div className="d-flex justify-content-between mt-1">
                      <span className="h5">Product Total Value:</span>
                      <span className="text-muted">Not applicable due to unclear price.</span>
                    </div>
                    <hr />
                    <div className="d-flex justify-content-between mt-5">
                      <span className="h3">Estimations Based on Typical Retail Values:</span>
                    </div>
                    <div >
                      <p className="h5">Calcium:</p>
                      <span className="text-muted">Assuming each bottle costs around ₹200, total value = 12 * ₹200 = ₹2400</span>
                    </div>
                    <div >
                      <p className="h5 mt-3">Puropet:</p>
                      <span className="text-muted">Assuming each bag costs around ₹300, total value = 10 * ₹300 = ₹3000</span>
                    </div>
                    <div >
                      <p className="h5 mt-3">Me-O:</p>
                      <span className="text-muted">Me-O: Assuming each bag costs around ₹150, total value = 22 * ₹150 = ₹3300</span>
                    </div>

                    <div className="mt-5">
                      <span className="h5 mt-5">Overall Total Value:</span><br />
                      <span className="text-muted text-justify">&nbsp; Due to the lack of visible and clear price information, it is not possible to calculate the exact overall total value of the inventory on the shelf. However, we can summarize the quantities and provide an estimation based on typical retail values if provided.</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fixed mt-5">
        <div className="row">
          {/* Right Section */}
          <div className="col-12 d-flex">
            <div className="card w-100">
              <div className="card-header">
                <div className="card-body">
                  <div className="container mx-auto bg-white shadow-md rounded p-6">
                    <div className="d-flex justify-content-between">
                      <span className="h5">GST Details</span>
                    </div>
                    <div className="table-responsive mt-3">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Category</th>
                            <th>Base Amount (₹)</th>
                            <th>GST Amount (₹)</th>
                            <th>Total Invoice Amount (₹)</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Sales</td>
                            <td>2400</td>
                            <td>432</td>
                            <td>2832</td>
                          </tr>
                          <tr>
                            <td>Purchase</td>
                            <td>3000</td>
                            <td>540</td>
                            <td>3540</td>
                          </tr>
                          <tr>
                            <td>Difference</td>
                            <td>3300</td>
                            <td>594</td>
                            <td>3894</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="mt-5">
                      <h6>Key Financial Metrics</h6>
                      <p>
                        <strong>Net GST Payable:</strong> ₹1566
                      </p>
                      <p>
                        <strong>Gross Profit:</strong> ₹1566 (Estimated)
                      </p>
                      <p>
                        <strong>GST Rate Applied:</strong> 18%
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  );
};

export default Shoplens;
