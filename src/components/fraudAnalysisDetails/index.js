import React, { Fragment, useEffect } from "react";
import { connect } from "react-redux";
import {
  FraudToolDetailActions,
  FraudToolListActions,
} from "../../store/actions";
import { useLocation } from "react-router-dom";

const FraudAnalysisDetails = ({
  getFraudToolBoxDetails,
  FraudAnalysisDetails,
  loading,
}) => {
  const location = useLocation();
  let pathToArray = location && location.pathname.split("/");
  let id = pathToArray[3];
  useEffect(() => {
    let params = {};
    params.id = id;
    getFraudToolBoxDetails(params);
  }, [id]);

  const showResponse = (apiName, response) => {
    let fraudToolResponseData = response?.external_response;
    let input = response?.input_values;

    if (apiName === "Aadhar Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Aadhar card number: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {response?.input_values?.aadhaar_number
                  ? response?.input_values?.aadhaar_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Gender: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                  ?.source_output?.gender
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                      ?.source_output?.gender
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                  ?.source_output?.state
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                      ?.source_output?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Mobile:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                  ?.source_output?.mobile_number
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                      ?.source_output?.mobile_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Age band:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                  ?.source_output?.age_band
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                      ?.source_output?.age_band?.lower_limit +
                    "-" +
                    fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                      ?.source_output?.age_band?.upper_limit
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Aadhar Image") {
      return (
        <>
          <>
            <h1 className="d-flex justify-content-center">
              Aadhar card OCR details
            </h1>
            <br />
            {fraudToolResponseData?.ocr?.status ? (
              <>
                <div className="d-flex justify-content-center">
                  <i
                    className="bi bi-check-circle-fill"
                    style={{
                      color: "#208f20",
                      backgroundColor: "transparent",
                      fontSize: "2.75rem",
                    }}
                  />
                </div>
                <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                  OCR conversion successfull!!
                </div>
              </>
            ) : (
              <>
                <div className="d-flex justify-content-center">
                  <i
                    className="bi bi-x-circle-fill"
                    style={{
                      color: "#FF474C",
                      backgroundColor: "transparent",
                      fontSize: "2.75rem",
                    }}
                  />
                </div>
                <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                  OCR conversion failed!!
                </div>
              </>
            )}

            <div className="text-center mb-20">
              <img src={input.aadhaar_front_image_url} height={400} />
            </div>
            <div className="row ">
              <div className="col-md-4">
                <p>ID number as per image: </p>{" "}
              </div>

              <div className="col-md-8 ml-auto ">
                <strong>
                  {fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_aadhaar
                    ?.result?.extraction_output?.id_number
                    ? fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_aadhaar
                        ?.result?.extraction_output?.id_number
                    : "No data"}
                </strong>
              </div>
            </div>
          </>

          <>
            <h1 className="d-flex justify-content-center mt-5 mb-5">
              Aadhar card information
            </h1>
            <br />
            {fraudToolResponseData?.mrz?.status ? (
              <>
                <div className="d-flex justify-content-center">
                  <i
                    className="bi bi-check-circle-fill"
                    style={{
                      color: "#208f20",
                      backgroundColor: "transparent",
                      fontSize: "2.75rem",
                    }}
                  />
                </div>
                <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                  Verification successfull!!
                </div>
              </>
            ) : (
              <>
                <div className="d-flex justify-content-center">
                  <i
                    className="bi bi-x-circle-fill"
                    style={{
                      color: "#FF474C",
                      backgroundColor: "transparent",
                      fontSize: "2.75rem",
                    }}
                  />
                </div>
                <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                  Verification failed!!
                </div>
              </>
            )}

            <div className="row ">
              <div className="col-md-4">
                <p>Gender: </p>{" "}
              </div>
              <div className="col-md-8 ml-auto ">
                <strong>
                  {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                    ?.source_output?.gender
                    ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                        ?.source_output?.gender
                    : "No data"}
                </strong>
              </div>
              <div className="col-md-4">
                <p>State:</p>{" "}
              </div>
              <div className="col-md-8 ml-auto ">
                <strong>
                  {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                    ?.source_output?.state
                    ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                        ?.source_output?.state
                    : "No data"}
                </strong>
              </div>
              <div className="col-md-4">
                <p>Age band:</p>{" "}
              </div>
              <div className="col-md-8 ml-auto ">
                <strong>
                  {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                    ?.source_output?.age_band
                    ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                        ?.source_output?.age_band?.lower_limit +
                      "-" +
                      fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                        ?.source_output?.age_band?.upper_limit
                    : "No data"}
                </strong>
              </div>
              <div className="col-md-4">
                <p>Mobile:</p>{" "}
              </div>
              <div className="col-md-8 ml-auto ">
                <strong>
                  {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                    ?.source_output?.mobile_number
                    ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                        ?.source_output?.mobile_number
                    : "No data"}
                </strong>
              </div>
              <div className="col-md-4">
                <p>Status:</p>{" "}
              </div>
              <div className="col-md-8 ml-auto ">
                <strong>
                  {fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                    ?.source_output?.status
                    ? fraudToolResponseData?.mrz?.data[0]?.verify_aadhar?.result
                        ?.source_output?.status
                    : "No data"}
                </strong>
              </div>
            </div>
          </>
        </>
      );
    } else if (apiName === "Pan Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>PAN card status: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                  ?.source_output?.pan_status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                      ?.source_output?.pan_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Name match: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.name_match.toString()
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.name_match.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DOB Match: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.dob_match.toString()
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.dob_match.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Pan Image") {
      return (
        <>
          <h1 className="d-flex justify-content-center">PAN OCR details</h1>
          <br />
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                OCR conversion successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                OCR conversion failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.pan_image_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Name as per image: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_pan?.result
                  ?.extraction_output?.name_on_card
                  ? fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_pan?.result
                      ?.extraction_output?.name_on_card
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>PAN id as per image: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_pan?.result
                  ?.extraction_output?.id_number
                  ? fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_pan?.result
                      ?.extraction_output?.id_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth as per image: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_pan?.result
                  ?.extraction_output?.date_of_birth
                  ? fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_pan?.result
                      ?.extraction_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
          </div>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            PAN information
          </h1>
          <br />
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>PAN card status: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                  ?.source_output?.pan_status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                      ?.source_output?.pan_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Name match: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.name_match.toString()
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.name_match.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DOB Match: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.dob_match.toString()
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result?.source_output?.dob_match.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_pan_v1?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Driving Licence Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.address
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.dob
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.dob
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of issue: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.date_of_issue
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.date_of_issue
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Validity from: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.nt_validity_from
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.nt_validity_from
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Validity To: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.nt_validity_to
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.nt_validity_to
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Issuing RTO: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.issuing_rto_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.issuing_rto_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>COV Category </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.cov_details[0]?.category
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.cov_details[0]?.category
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>COV </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.cov_details[0]?.cov
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.cov_details[0]?.cov
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>COV Issue date </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.cov_details[0]?.issue_date
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.cov_details[0]?.issue_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of last transaction </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.date_of_last_transaction
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.date_of_last_transaction
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DL status </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.dl_status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.dl_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Face image </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.face_image
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.face_image
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Gender </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.gender
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.gender
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Relative name </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.relatives_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.relatives_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Source </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.source
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.source
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Driving Licence Image") {
      return (
        <>
          <h1 className="d-flex justify-content-center">
            Driving license OCR Details
          </h1>
          <br />
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div class="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                OCR conversion successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                OCR conversion failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.driving_license_image_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>ID number as per image:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_driving_license_ocr
                  ?.result?.extraction_output?.id_number
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_driving_license_ocr?.result?.extraction_output
                      ?.id_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_driving_license_ocr
                  ?.result?.extraction_output?.date_of_birth
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_driving_license_ocr?.result?.extraction_output
                      ?.date_of_birth
                  : "No data"}
              </strong>
            </div>
          </div>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            Driving license information
          </h1>
          <br />
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.address
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.dob
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.dob
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of issue: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.date_of_issue
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.date_of_issue
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Validity from: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.nt_validity_from
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.nt_validity_from
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Validity To: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.nt_validity_to
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.nt_validity_to
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Issuing RTO: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.issuing_rto_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.issuing_rto_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>COV Category </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.cov_details[0]?.category
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.cov_details[0]?.category
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>COV </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.cov_details[0]?.cov
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.cov_details[0]?.cov
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>COV Issue date </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.cov_details[0]?.issue_date
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.cov_details[0]?.issue_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of last transaction </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.date_of_last_transaction
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.date_of_last_transaction
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DL status </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.dl_status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.dl_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Face image </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.face_image
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.face_image
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Gender </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.gender
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.gender
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Relative name </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.relatives_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.relatives_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Source </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                  ?.result?.source_output?.source
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_driving_licence
                      ?.result?.source_output?.source
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "passport_details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.date_of_birth
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Passport status: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.passport_status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.passport_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Passport Image") {
      return (
        <>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            Passport information
          </h1>
          <br />
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  class="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div class="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  class="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div class="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.passport_image_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Name on the card: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                  ?.result?.extraction_output?.name_on_card
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                      ?.result?.extraction_output?.name_on_card
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                  ?.result?.extraction_output?.date_of_birth
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                      ?.result?.extraction_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>File number: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                  ?.result?.extraction_output?.file_number
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                      ?.result?.extraction_output?.file_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Passport number: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                  ?.result?.extraction_output?.id_number
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                      ?.result?.extraction_output?.id_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                  ?.result?.extraction_output?.state
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_passport_ocr
                      ?.result?.extraction_output?.state
                  : "No data"}
              </strong>
            </div>
          </div>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            Passport information
          </h1>
          <br />
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  class="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div class="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  class="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div class="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.date_of_birth
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Passport status: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.passport_status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.passport_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_passport?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Voter Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.name_on_card
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.name_on_card
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.date_of_birth
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>District: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.district
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.district
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.state
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Gender: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.gender
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.gender
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>PS Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.ps_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.ps_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Source: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.source
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.source
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>ST Code: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.st_code
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.st_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Voter id status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.status
                  : fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.Message}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Voter Image") {
      return (
        <>
          <h1 className="d-flex justify-content-center">
            Voter ID OCR Details
          </h1>
          <br />
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                OCR conversion successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                OCR conversion failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.voter_id_image_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>ID number as per image:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_voterid_ocr?.result
                  ?.extraction_output?.id_number
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_voterid_ocr
                      ?.result?.extraction_output?.id_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_voterid_ocr?.result
                  ?.extraction_output?.date_of_birth
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_voterid_ocr
                      ?.result?.extraction_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_voterid_ocr?.result
                  ?.extraction_output?.name_on_card
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_voterid_ocr
                      ?.result?.extraction_output?.name_on_card
                  : "No data"}
              </strong>
            </div>
          </div>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            Voter ID information
          </h1>
          <br />
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.name_on_card
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.name_on_card
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.date_of_birth
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>District: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.district
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.district
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.state
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Gender: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.gender
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.gender
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>PS Name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.ps_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.ps_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Source: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.source
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.source
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>ST Code: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.st_code
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.st_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Voter id status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.result?.source_output?.status
                  : fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_voter_id
                      ?.source_output?.status}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Bank Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.name_at_bank
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.name_at_bank
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Account exists?:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.account_exists
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.account_exists
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Bank account number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.bank_account_number
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.bank_account_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>IFSC Code:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.ifsc_code
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.ifsc_code
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Cancellation Cheque Image") {
      return (
        <>
          <h1 className="d-flex justify-content-center">
            Cancelation cheque OCR Details
          </h1>
          <br />
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                OCR conversion successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                OCR conversion failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.cancellation_cheque_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Account holder name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr?.result
                  ?.extraction_output?.account_name
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr
                      ?.result?.extraction_output?.account_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Account number::</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr?.result
                  ?.extraction_output?.account_name
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr
                      ?.result?.extraction_output?.account_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>IFSC Code:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr?.result
                  ?.extraction_output?.ifsc_code
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr
                      ?.result?.extraction_output?.ifsc_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Bank name: </p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr?.result
                  ?.extraction_output?.bank_names
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr
                      ?.result?.extraction_output?.bank_names
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Bank address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr?.result
                  ?.extraction_output?.bank_address
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr
                      ?.result?.extraction_output?.bank_address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Account type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr?.result
                  ?.extraction_output?.account_type
                  ? fraudToolResponseData?.ocr?.data[0]?.verify_cheque_ocr
                      ?.result?.extraction_output?.account_type
                  : "No data"}
              </strong>
            </div>
          </div>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            Bank verificaton as per cheque
          </h1>
          <br />
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.name_at_bank
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.name_at_bank
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Account exists?:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.account_exists
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.account_exists
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Bank account number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.bank_account_number
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.bank_account_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>IFSC Code:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                  ?.result?.ifsc_code
                  ? fraudToolResponseData?.mrz?.data[0]?.bank_verification_v1
                      ?.result?.ifsc_code
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Gst Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4 ">
              <p>Legal name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.legal_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.legal_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of registration:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.date_of_registration
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.date_of_registration
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Trade name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.trade_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.trade_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4 ">
              <p>Constitution of business:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.onstitution_of_business
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.onstitution_of_business
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of cancellation:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.date_of_cancellation
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.date_of_cancellation
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>GSTIN:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output?.gstin
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.gstin
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>GSTIN Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.gstin_status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.gstin_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Nature of business activity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.ature_of_principal_place_of_business
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_gst_certificate?.result?.source_output?.principal_place_of_business_fields?.ature_of_principal_place_of_business.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business bulding name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.building_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.building_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business city:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.city
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.city
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place door number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.door_number
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.door_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place dts:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.dst
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.dst
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business floor_number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.floor_numer
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.floor_numer
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business latitue:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.latitude
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.latitude
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business location:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.location
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.location
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business lognitude:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.longitude
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.longitude
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business pincode:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.pincode
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.pincode
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business state name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.state_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.state_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business street:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.street
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.street
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Tax payer type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.taxpayer_type
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.taxpayer_type
                  : "No data"}
              </strong>
            </div>
          </div>{" "}
        </>
      );
    } else if (apiName === "Gst Image") {
      return (
        <>
          <h1 className="d-flex justify-content-center">GST OCR Details</h1>
          <br />
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                OCR conversion successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div class="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                OCR conversion failed!!
              </div>
            </>
          )}
          <div className="text-center mt-20">
            <img src={input.gst_image_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4 ">
              <p>GST Number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_gst_certificate_ocr
                  ?.result?.extraction_output?.gstin
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_gst_certificate_ocr?.result?.extraction_output
                      ?.gstin
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Legal name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_gst_certificate_ocr
                  ?.result?.extraction_output?.legal_name
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_gst_certificate_ocr?.result?.extraction_output
                      ?.legal_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Trade name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_gst_certificate_ocr
                  ?.result?.extraction_output?.trade_name
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_gst_certificate_ocr?.result?.extraction_output
                      ?.trade_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Pan number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_gst_certificate_ocr
                  ?.result?.extraction_output?.pan_number
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_gst_certificate_ocr?.result?.extraction_output
                      ?.pan_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.verify_gst_certificate_ocr
                  ?.result?.extraction_output?.address
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.verify_gst_certificate_ocr?.result?.extraction_output
                      ?.address
                  : "No data"}
              </strong>
            </div>
          </div>
          <h1 className="d-flex justify-content-center mt-5 mb-5">
            GST Verifcation
          </h1>
          <br />
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4 ">
              <p>Legal name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.legal_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.legal_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of registration:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.date_of_registration
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.date_of_registration
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Trade name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.trade_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.trade_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4 ">
              <p>Constitution of business:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.onstitution_of_business
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.onstitution_of_business
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of cancellation:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.date_of_cancellation
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.date_of_cancellation
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>GSTIN:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output?.gstin
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.gstin
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>GSTIN Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.gstin_status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.gstin_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Nature of business activity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.ature_of_principal_place_of_business
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_gst_certificate?.result?.source_output?.principal_place_of_business_fields?.ature_of_principal_place_of_business.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business bulding name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.building_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.building_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business city:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.city
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.city
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place door number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.door_number
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.door_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place dts:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.dst
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.dst
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business floor_number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.floor_numer
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.floor_numer
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business latitue:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.latitude
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.latitude
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business location:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.location
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.location
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business lognitude:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.longitude
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.longitude
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business pincode:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.pincode
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.pincode
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business state name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.principal_place_of_business_fields
                  ?.principal_place_of_business_address?.state_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.principal_place_of_business_fields
                      ?.principal_place_of_business_address?.state_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Principapl place business street:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.street
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.street
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Tax payer type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_gst_certificate?.result?.source_output
                  ?.taxpayer_type
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_gst_certificate?.result?.source_output
                      ?.taxpayer_type
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Address Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4 ">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.address_check?.address
                  ? fraudToolResponseData?.mrz?.data[0]?.address_check?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Lattitude:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.address_check?.lattitude
                  ? fraudToolResponseData?.mrz?.data[0]?.address_check
                      ?.lattitude
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Logitude:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.address_check?.longitude
                  ? fraudToolResponseData?.mrz?.data[0]?.address_check
                      ?.longitude
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Risk score:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.address_check?.risk_score
                  ? fraudToolResponseData?.mrz?.data[0]?.address_check
                      ?.risk_score
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.address_check?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.address_check?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Cin Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  class="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4 ">
              <p>Company name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                  ?.source_output?.company_name
                  ? fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                      ?.source_output?.company_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Company category:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                  ?.source_output?.company_category
                  ? fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                      ?.source_output?.company_category
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of incorporation:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                  ?.source_output?.date_of_incorporation
                  ? fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                      ?.source_output?.date_of_incorporation
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>registered address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                  ?.source_output?.registered_address
                  ? fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                      ?.source_output?.registered_address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.cin_verify_mca?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Phone Number Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4 ">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Email:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                  ?.associated_email_addresses?.emails[0]
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.associated_email_addresses?.emails[0]
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Active:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.active.toString()
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.active.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Carrier:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.carrier
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.carrier
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>city:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.city
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.city
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Country:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.country
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.country
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Dialing Code:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                  ?.dialing_code
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.dialing_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Formatted:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.formatted
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.formatted
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Fraud score:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                  ?.fraud_score
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.fraud_score
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Leaked:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.leaked
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.leaked
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Line type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.line_type
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.line_type
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Local format:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                  ?.local_format
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.local_format
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Message:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.message
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.message
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Prepaid:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.prepaid
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.prepaid
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Recent abuse:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                  ?.recent_abuse
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.recent_abuse
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Region:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.region
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.region
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Risky:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.risky
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.risky
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>SMS Domain:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                  ?.sms_domain
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.sms_domain
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>SNS email:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.sms_email
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.sms_email
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Spammer:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.spammer
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.spammer
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Timezone:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2?.timezone
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_phone_v2
                      ?.timezone
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Email Id Details") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>First name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.first_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.first_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Is valid:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.valid
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.valid?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Catch all:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.catch_all
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.catch_all?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Common:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.common
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.common?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Disposable:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.disposable
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.disposable?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Deliverability:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.deliverability
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.deliverability
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DMARC Record:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.dns_valid
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.dns_valid?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DNC Valid:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.dns_valid
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.dns_valid?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>DNC Valid:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.dns_valid
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.dns_valid?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Domain age:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.domain_age?.human
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.domain_age?.human
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Domain trust:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.domain_trust
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.domain_trust
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Domain velocity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.domain_velocity
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.domain_velocity
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>First seen:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.first_seen?.human
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.first_seen?.human
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Fraud score:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.valid
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.valid?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Frequent complainer:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.frequent_complainer
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.frequent_complainer?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Generic:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.generic
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.generic?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Honeypot:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.honeypot
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.honeypot?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Leaked:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.leaked
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.leaked?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Recent abuse:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.recent_abuse
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.recent_abuse?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Risky tld:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.risky_tld
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.risky_tld?.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Sanitized email:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.sanitized_email
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.sanitized_email
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>SMTP Score:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.smtp_score
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.smtp_score
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Spam trap score:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                  ?.spam_trap_score
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.spam_trap_score
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Suspect:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_email_v2?.suspect
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_email_v2
                      ?.suspect
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Shop Image Analyzer") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mt-20">
            <img src={input.shop_image_url} height={400} />
          </div>
          <div className="row mt-10 mb-20 ">
            <div className="col-md-4">
              <p>Shop name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.result?.shop_name
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                      ?.result?.shop_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop Name (English):</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.result?.shop_name_translated
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                      ?.result?.shop_name_translated
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop name visible:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.result?.shop_name_visible
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer?.result?.shop_name_visible.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop board type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.result?.shop_board_type
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                      ?.result?.shop_board_type
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop movable:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.result?.shop_movable
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer?.result?.shop_movable.toString()
                  : "No data"}
              </strong>
            </div>
            {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer?.result?.mcc_categories?.map(
              (ele, index) => {
                return (
                  <>
                    <div className="col-md-4">
                      <p>Shop Catogory {index + 1}:</p>{" "}
                    </div>
                    <div className="col-md-8 ml-auto ">
                      <strong>{ele.category}</strong>
                    </div>
                    <div className="col-md-4">
                      <p>MCC Code {index + 1}:</p>{" "}
                    </div>
                    <div className="col-md-8 ml-auto ">
                      <strong>{ele.mcc_code}</strong>
                    </div>
                    <div className="col-md-4">
                      <p>Reason {index + 1}:</p>{" "}
                    </div>
                    <div className="col-md-8 ml-auto ">
                      <strong>{ele.reason}</strong>
                    </div>
                    <div className="col-md-4">
                      <p>Confidence score {index + 1}:</p>{" "}
                    </div>
                    <div className="col-md-8 ml-auto ">
                      <strong>{ele.confidence_score}</strong>
                    </div>
                  </>
                );
              }
            )}
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                      ?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Message:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                  ?.message
                  ? fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer
                      ?.message
                  : "No data"}
              </strong>
            </div>
            {(input.business_name || input?.category) && (
              <>
                <h4 className="text-center mt-10 mb-20">
                  Input and verification data comparison
                </h4>
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">
                        <strong>SL</strong>
                      </th>
                      <th scope="col">
                        <strong>Fields</strong>
                      </th>
                      <th scope="col">
                        <strong>client input</strong>
                      </th>
                      <th scope="col">
                        <strong>data from api</strong>
                      </th>
                      <th scope="col">
                        <strong>Result</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {input?.business_name && (
                      <tr>
                        <th scope="row">1</th>
                        <td>Busines Name</td>
                        <td>{input?.business_name}</td>
                        <td>
                          {
                            fraudToolResponseData?.mrz?.data[0]
                              ?.shop_image_analyzer?.result
                              ?.shop_name_translated
                          }
                        </td>
                        <td>
                          {input?.business_name?.toLowerCase() ===
                          fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer?.result?.shop_name_translated?.toLowerCase()
                            ? "matched"
                            : "does not match"}
                        </td>
                      </tr>
                    )}
                    {input?.category && (
                      <tr>
                        <th scope="row">2</th>
                        <td>Category</td>
                        <td>{input?.category}</td>
                        <td>
                          {
                            fraudToolResponseData?.mrz?.data[0]
                              ?.shop_image_analyzer?.result?.mcc_categories[0]
                              .category
                          }
                        </td>
                        <td>
                          {fraudToolResponseData?.mrz?.data[0]?.shop_image_analyzer?.result?.mcc_categories[0].category.toLowerCase() ===
                          input?.business_name?.toLowerCase()
                            ? "matched"
                            : "does not match"}
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </>
            )}
          </div>
        </>
      );
    } else if (apiName === "Shop Lens") {
      return (
        <>
          {fraudToolResponseData?.ocr?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mt-20">
            <img src={input.files} height={400} />
          </div>
          <div className="row mt-10 mb-20 ">
            <div className="col-md-4">
              <p>Shop name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.shopName
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopName
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop name extracted:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.shopNameExtracted
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopNameExtracted
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop detected:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.shopDetected
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.shopDetected.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop owner name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.shopOwnerName
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopOwnerName
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop owner name extracted:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.shopOwnerNameExtracted
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopOwnerNameExtracted
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>GST number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.gstNumber
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.gstNumber
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Contact number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.contactNumber
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.contactNumber
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop board Standee detected:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.boardStandeeDetected
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.boardStandeeDetected.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Shop Inventory detected:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.inventoryDetected
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.inventoryDetected.toString()
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>isMovable:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.isMovable
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.isMovable
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.address
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address extracted:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.addressExtracted
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.addressExtracted
                  : "No data"}
              </strong>
            </div>
            {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.mccDetails?.map(
              (ele, index) => {
                return (
                  <>
                    <div className="col-md-4">
                      <p>Description {index + 1}:</p>{" "}
                    </div>
                    <div className="col-md-8 ml-auto ">
                      <strong>{ele.description}</strong>
                    </div>
                    <div className="col-md-4">
                      <p>MCC Code {index + 1}:</p>{" "}
                    </div>
                    <div className="col-md-8 ml-auto ">
                      <strong>{ele.mccCode}</strong>
                    </div>
                  </>
                );
              }
            )}
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.status
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.datetime
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.datetime
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Message:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                  ?.message
                  ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.message
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Ind Electricity Bill") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.electricity_bill_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Account number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.account_number
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.account_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.address
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Bill number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.bill_number
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.bill_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Email id:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.email_id
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.email_id
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Full name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.full_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.full_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Mobile number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.mobile_no
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.mobile_no
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Total due amount:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_electricity_bill?.result?.extraction_output
                  ?.total_due_amount
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_electricity_bill?.result
                      ?.extraction_output?.total_due_amount
                  : "No data"}
              </strong>
            </div>
            {/* <pre className="col-md-8 ml-auto">
                                    {JSON.stringify(fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_electricity_bill?.result?.extraction_output, null, 2)}
                                  </pre> */}
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Ind Broadband Bill") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.broadband_bill_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_broadband_bill?.result?.extraction_output
                  ?.full_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_broadband_bill?.result?.extraction_output
                      ?.full_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_ind_broadband_bill?.result?.extraction_output
                  ?.address
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_ind_broadband_bill?.result?.extraction_output
                      ?.address
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Ind Gas Bill") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.gas_bill_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_gas_bill
                  ?.result?.extraction_output?.full_name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_gas_bill
                      ?.result?.extraction_output?.full_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_gas_bill
                  ?.result?.extraction_output?.address
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_gas_bill
                      ?.result?.extraction_output?.address
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Deed Of Lease") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Lesee name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_deed_of_lease
                  ?.result?.extraction_output?.lesee_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_deed_of_lease?.result?.extraction_output
                      ?.lesee_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Lessor name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_deed_of_lease
                  ?.result?.extraction_output?.lessor_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_deed_of_lease?.result?.extraction_output
                      ?.lessor_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Licensee Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_deed_of_lease
                  ?.result?.extraction_output?.licensee_address
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_deed_of_lease?.result?.extraction_output
                      ?.licensee_address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Licensor Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_deed_of_lease
                  ?.result?.extraction_output?.licensor_address
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_deed_of_lease?.result?.extraction_output
                      ?.licensor_address
                  : "No data"}
              </strong>
            </div>
            {/* <pre className="col-md-8 ml-auto">
                                  {/* <pre className="col-md-8 ml-auto">
                                    {JSON.stringify(fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_broadband_bill?.result?.extraction_output, null, 2)}
                                  </pre> */}
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Ind Fssai") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.fssai_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                  ?.result?.extraction_output?.name
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                      ?.result?.extraction_output?.name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                  ?.result?.extraction_output?.address
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                      ?.result?.extraction_output?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Id:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                  ?.result?.extraction_output?.id_number
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                      ?.result?.extraction_output?.id_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Issue date:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                  ?.result?.extraction_output?.issue_date
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                      ?.result?.extraction_output?.issue_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Expire day:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                  ?.result?.extraction_output?.expiry_date
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                      ?.result?.extraction_output?.expiry_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Kind of business:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                  ?.result?.extraction_output?.kind_of_business
                  ? fraudToolResponseData?.mrz?.data[0]?.verify_sol_ind_fssai
                      ?.result?.extraction_output?.kind_of_business
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Verify Sol International Passport") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="text-center mb-20">
            <img src={input.international_passport_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.given_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.given_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of birth:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.date_of_birth
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.date_of_birth
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Nationality:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.nationality
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.nationality
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Passport ID:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.id_number
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.id_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of issue:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.date_of_issue
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.date_of_issue
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of expiry</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.date_of_expiry
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.date_of_expiry
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Gender:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_international_passport?.result?.extraction_output
                  ?.gender
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_international_passport?.result
                      ?.extraction_output?.gender
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "verify_sol_ind_shop_license") {
      return <></>;
    } else if (apiName === "Verify Sol Establishment Epfo") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Establishment name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.establishment_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.establishment_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Establishment code:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.establishment_code
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.establishment_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.address
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>City:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.city
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.city
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.state
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Country:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.country
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.country
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Pin:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.pin_code
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.pin_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Last updated on EPFO:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.last_updated_on_epfo
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.last_updated_on_epfo
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>PAN status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.pan_status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.pan_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Ownership Type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.ownership_type
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.ownership_type
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Working status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.working_status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.working_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of establishment setup:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.date_of_establishment_setup
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.date_of_establishment_setup
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>EPFO Office name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]
                  ?.verify_sol_establishment_epfo?.result?.source_output
                  ?.details[0]?.epfo_office_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_establishment_epfo?.result?.source_output
                      ?.details[0]?.epfo_office_name
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Udyam Aadhaar") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}

          <div className="row ">
            <div className="col-md-4">
              <p>Udyam Aadhar Number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {input?.udyam_aadhaar_number
                  ? input?.udyam_aadhaar_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Enterprise name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.general_details?.enterprise_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.general_details?.enterprise_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Expiry date:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.general_details?.expiry_date
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.general_details?.expiry_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Major activity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.general_details?.major_activity
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.general_details?.major_activity
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.general_details?.state
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.general_details?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>District:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.general_details?.dic_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.general_details?.dic_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Official Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.official_address?.state
                  ? `${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar?.result?.source_output?.official_address?.block} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar?.result?.source_output?.official_address?.door} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar?.result?.source_output?.official_address?.town} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar?.result?.source_output?.official_address?.district} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar?.result?.source_output?.official_address?.state} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar?.result?.source_output?.official_address.pin}`
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Official Email:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.official_address?.email
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.official_address?.email
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Mobile:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyam_aadhaar
                  ?.result?.source_output?.official_address?.mobile
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyam_aadhaar?.result?.source_output
                      ?.official_address?.mobile
                  : "No data"}
              </strong>
            </div>
          </div>
          
    
        </>
      );
    } else if (apiName === "Ind Fssai") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Company Name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.company_name
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.company_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>License number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.license_no
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.license_no
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Validity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.validity
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.validity
                  : "No data"}
              </strong>
            </div>{" "}
            <div className="col-md-4">
              <p>License type:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.license_type
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.license_type
                  : "No data"}
              </strong>
            </div>{" "}
            <div className="col-md-4">
              <p>Premise Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.premise_address
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.premise_address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.state
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Pincode:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.company_details?.pincode
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                      ?.source_output?.company_details?.pincode
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Products:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result
                  ?.source_output?.products
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_fssai?.result?.source_output?.products.join(
                      ""
                    )
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Ind Roc Iec") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Enterprise name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.enterprise_name
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.enterprise_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Enterprise PAN:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.nterprise_pan
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.nterprise_pan
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>IEC Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.iec_status
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.iec_status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Enterprise address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.enterprise_address
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.enterprise_address
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>City</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.city
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.city
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.state
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Pincode:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.pincode
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.pincode
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Import export code:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.import_export_code
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.import_export_code
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Category of exporters:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.category_of_exporters
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.category_of_exporters
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of issuance:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.category_of_exporters
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.category_of_exporters
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of Issue:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.date_of_issuance
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.date_of_issuance
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of Suspension:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.date_of_suspension
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.date_of_suspension
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Date of Cancelation:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.date_of_cancellation
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.date_of_cancellation
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                  ?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]?.ind_roc_iec?.result
                      ?.source_output?.status
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Doc Ocr Ind Udyog Aadhar") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}

          <div className="text-center mb-20">
            <img src={input.udyog_image_url} height={400} />
          </div>
          <div className="row ">
            <div className="col-md-4">
              <p>Udyog Aadhar Number:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.ocr?.data[0]?.doc_ocr_ind_udyog_aadhar
                  ?.result?.extraction_output?.aadhaar_number
                  ? fraudToolResponseData?.ocr?.data[0]
                      ?.doc_ocr_ind_udyog_aadhar?.result?.extraction_output
                      ?.aadhaar_number
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Enterprise name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.enterprise_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.enterprise_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Expiry date:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.expiry_date
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.expiry_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Major activity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.major_activity
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.major_activity
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.state
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>District:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.dic_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.dic_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.address_details[0].address_1.state
                  ? `${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.area} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1?.door} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.road} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.district} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.state} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.pin}`
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    } else if (apiName === "Verify Sol Udyog Aadhaar") {
      return (
        <>
          {fraudToolResponseData?.mrz?.status ? (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-check-circle-fill"
                  style={{
                    color: "#208f20",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                Verification successfull!!
              </div>
            </>
          ) : (
            <>
              <div className="d-flex justify-content-center">
                <i
                  className="bi bi-x-circle-fill"
                  style={{
                    color: "#FF474C",
                    backgroundColor: "transparent",
                    fontSize: "2.75rem",
                  }}
                />
              </div>
              <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                Verification failed!!
              </div>
            </>
          )}
          <div className="row ">
            <div className="col-md-4">
              <p>Enterprise name:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.enterprise_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.enterprise_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Expiry date:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.expiry_date
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.expiry_date
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Status:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.status
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output?.status
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Major activity:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.major_activity
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.major_activity
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>State:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.state
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.state
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>District:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.general_details?.dic_name
                  ? fraudToolResponseData?.mrz?.data[0]
                      ?.verify_sol_udyog_aadhaar?.result?.source_output
                      ?.general_details?.dic_name
                  : "No data"}
              </strong>
            </div>
            <div className="col-md-4">
              <p>Address:</p>{" "}
            </div>
            <div className="col-md-8 ml-auto ">
              <strong>
                {fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar
                  ?.result?.source_output?.address_details[0].address_1.state
                  ? `${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.area} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1?.door} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.road} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.district} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.state} ${fraudToolResponseData?.mrz?.data[0]?.verify_sol_udyog_aadhaar?.result?.source_output?.address_details[0].address_1.pin}`
                  : "No data"}
              </strong>
            </div>
          </div>
        </>
      );
    }
  };
  return (
    <Fragment>
      {loading ||
      FraudAnalysisDetails?.data?.fraud_tool_status !== "COMPLETED" ? (
        <div className="container-fluid">
          <div className="row justify-content-center">
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
          <div className="row justify-content-center text-center mt-20">
            <strong>Processing your request. Please wait.</strong>
          </div>
        </div>
      ) : (
        showResponse(
          FraudAnalysisDetails?.data?.api_name,
          FraudAnalysisDetails?.data
        )
      )}
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  const {
    WrmOperationManagementStore,
    clinetListStore,
    FraudAnalysisDetailManagementStore,
  } = state;
  console.log(FraudAnalysisDetailManagementStore);
  return {
    FraudAnalysisDetails:
      FraudAnalysisDetailManagementStore &&
      FraudAnalysisDetailManagementStore.fraudToolDetailData,
    loading:
      WrmOperationManagementStore && WrmOperationManagementStore.loading
        ? WrmOperationManagementStore.loading
        : false,
    WrmOperationActionStatus:
      state &&
      state.wrmOperationManagementActionStore &&
      state.wrmOperationManagementActionStore,
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getFraudToolBoxDetails: (params) =>
    dispatch(FraudToolDetailActions.request(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FraudAnalysisDetails);
