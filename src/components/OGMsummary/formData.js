import _ from "lodash"

export const setTagSummaryData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      approvedCases: data.approvedCases.toString(),
      rejectedCases: data.rejectedCases.toString(),
      tag: data.tag.toString(),
      deliveryDate: data && data.deliveryDate ? new Date(data.deliveryDate) : '',
      uploadedDate: data && data.uploadedDate ? new Date(data && data.uploadedDate) : ''
    }
  }
}