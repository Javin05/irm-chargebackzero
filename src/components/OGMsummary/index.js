import React, { useEffect, useState, Fragment } from 'react'
import _ from 'lodash'
import { OGMSummaryActions, clientIdLIstActions } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import moment from "moment"
import SearchList from './searchList'
import axios from 'axios'
import { HEADER, SESSION } from '../../utils/constants'
import { getLocalStorage } from '../../utils/helper'
import { warningAlert } from '../../utils/alerts'
import ReactHTMLTableToExcel from "react-html-table-to-excel"

function OgmSummary(props) {
  const {
    className,
    getOGMSummarylistDispatch,
    OGMsummarylists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    clinetIdLists,
    clientIdDispatch
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [searchParams, setSearchParams] = useState({})
  const [sorting, setSorting] = useState({
    tag: false,
    createdDate: false,
    rejected: false,
    total: false,
    manualreview: false
  })
  const [formData, setFormData] = useState([])
  const [searchData, setSearchData] = useState({})
  const [exportReport, setExportReport] = useState('')
  const [exportReportLoading, setExportReportLoading] = useState(false)
  const [updateFormData, setupdateFormData] = useState({
    deliveryDate: '',
    rejectedCases: '',
    approvedCases: '',
    uploadedDate: '',
    file: '',
    tag: ''
  })
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })

  const handleChanges = (e) => {
    setupdateFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }

  useEffect(() => {
    getOGMSummarylistDispatch()
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params)
  }, [])


  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  // useEffect(() => {
  //   if (didMount.current && setFilterFunction) {
  //     const currentFilterParams = setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
  //     setActivePageNumber(1)
  //     const params = {
  //       limit: limit,
  //       page: 1,
  //       clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
  //     }
  //     const pickByParams = _.pickBy(params)
  //     getOGMSummarylistDispatch(pickByParams)
  //     setFilterFunctionDispatch(false)
  //     setSearchParams(currentFilterParams)
  //   }
  // }, [setFilterFunction, setCredFilterParams])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      // clientId: setCredFilterParams.clientId
      //   ? setCredFilterParams.clientId
      //   : "",
      deliveryDateFrom: searchData.deliveryDateFrom,
      deliveryDateTo: searchData.deliveryDateTo,
      status: searchData.status,
      tag: searchData.tag,
      uploadedDateFrom: searchData.uploadedDateFrom,
      uploadedDateTo: searchData.uploadedDateTo,
    }
    setActivePageNumber(pageNumber)
    getOGMSummarylistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getOGMSummarylistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getOGMSummarylistDispatch(params)
    }
  }

  const totalPages =
    OGMsummarylists && OGMsummarylists.data && OGMsummarylists.data.count
      ? Math.ceil(parseInt(OGMsummarylists && OGMsummarylists.data && OGMsummarylists.data.count) / limit)
      : 1


  const ExportData = () => {
    setExportReportLoading(true)
    const params = {
      ...searchData,
      skipPagination: true
    }

    const headers = {
      "Authorization": `${HEADER.BEARER} ${getLocalStorage(SESSION.TOKEN)}`,
      "Content-Type": HEADER.CONTENT_TYPE
    }
    const url = `https://irm-chargebackzero-api.chargebackzero.com/api/v1/ongoingmonitoring/getSummary`
    axios.get(url, { headers, params }).then((response) => {
      if (response.status === 200) {
        setExportReport(response)
      }
    })
  }

  const clear = () => {
    setExportReportLoading(false)
    setExportReport('')
  }

  useEffect(() => {
    if (exportReport && exportReport.data && exportReport.data.status === 'ok') {
      if (Array.isArray(exportReport && exportReport.data && exportReport.data.data && exportReport.data.data.result)) {
        const closeXlsx = document.getElementById('ogmSummaryCsvReport')
        closeXlsx.click()
        
        setExportReportLoading(false)
      } else if (exportReport && exportReport.data && exportReport.data.status === 'error') {
        warningAlert(
          'error',
          exportReport && exportReport.data && exportReport.data.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
      }
    }
  }, [exportReport])

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-target='#ogmSummaryCsvReport'
      >
        <ReactHTMLTableToExcel
          id="ogmSummaryCsvReport"
          className="download-table-xls-button"
          table="ogmSummaryCsvtable"
          filename={`OGM-EXPORT-BATCH-REPORT`}
          sheet="tablexls"
        />
      </div>
      <div className={`card md-10 ${className}`} >
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1 ms-4'>
                {OGMsummarylists && OGMsummarylists.data && OGMsummarylists.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3 ms-1'>
                      {OGMsummarylists.data.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='col-lg-6'>
              <div className='d-flex justify-content-end mt-4 me-4'>
                {_.isEmpty(searchData) ? null :
                  <button
                    type='button'
                    className='btn btn-sm btn-light-success fa-pull-right w-150px me-2'
                    onClick={ExportData}
                    disabled={exportReportLoading}
                  >
                    {exportReportLoading
                      ? (
                        <span
                          className='spinner-border spinner-border-sm mx-3'
                          role='status'
                          aria-hidden='true'
                        />
                      )
                      : (
                        'Export Data'
                      )}
                  </button>}
                <SearchList setCredFilterParams={setCredFilterParams} setSearchData={setSearchData} />
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Batch Uploaded Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("createdAt")}
                        >
                          <i
                            className={`bi ${sorting.createdAt
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Batch Name</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogmSummaryId")}
                        >
                          <i
                            className={`bi ${sorting.ogmSummaryId
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Ogm Interval</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_interval")}
                        >
                          <i
                            className={`bi ${sorting.ogm_interval
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Ogm start Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_start_date")}
                        >
                          <i
                            className={`bi ${sorting.ogm_start_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Count</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("totalCount")}
                        >
                          <i
                            className={`bi ${sorting.totalCount
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Duplicate Urls</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("duplicateUrls")}
                        >
                          <i
                            className={`bi ${sorting.duplicateUrls
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Valid Url Counts</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("validUrlCounts")}
                        >
                          <i
                            className={`bi ${sorting.validUrlCounts
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Invalid Urls Counts</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("inValideUrlsCounts")}
                        >
                          <i
                            className={`bi ${sorting.inValideUrlsCounts
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Not in WRM</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("notInWebriskCounts")}
                        >
                          <i
                            className={`bi ${sorting.notInWebriskCounts
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-7 ms-2'>
                {
                  !loading
                    ? (
                      Array.isArray(OGMsummarylists &&
                        OGMsummarylists.data && OGMsummarylists.data.result)
                        ? (
                          OGMsummarylists && OGMsummarylists.data && OGMsummarylists.data.result.map((item, i) => {
                            const createdAt =
                              moment(
                                item.createdAt ? item.createdAt : "--"
                              ).format("MMM Do YY")
                            const ogmStartDate =
                              moment(
                                item.ogm_start_date ? item.ogm_start_date : "--"
                              ).format("MMM Do YY")

                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  <td>
                                    {
                                      createdAt === 'Invalid date' ? '--' : createdAt
                                    }
                                  </td>
                                  <td>
                                    OGM-BATCH-{item && item.ogmSummaryId ? item.ogmSummaryId : '--'}
                                  </td>
                                  <td>
                                    {item && item.ogm_interval ? item.ogm_interval : '--'}
                                  </td>
                                  <td>
                                    {
                                      ogmStartDate === 'Invalid date' ? '--' : ogmStartDate
                                    }
                                  </td>
                                  <td>
                                    {item && item.totalCount ? item.totalCount : '--'}
                                  </td>
                                  <td>
                                    {item && item.duplicateUrls ? item.duplicateUrls : '--'}
                                  </td>
                                  <td>
                                    {item && item.validUrlCounts ? item.validUrlCounts : '--'}
                                  </td>
                                  <td>
                                    {item && item.inValideUrlsCounts ? item.inValideUrlsCounts : '--'}
                                  </td>
                                  <td>
                                    {item && item.notInWebriskCounts ? item.notInWebriskCounts : '--'}
                                  </td>
                                </tr>
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* csv Report */}
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="ogmSummaryCsvtable">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Batch Uploaded Date</th>
              <th>Batch Name</th>
              <th>Ogm Interval</th>
              <th>Ogm start Date</th>
              <th>Total Count</th>
              <th>Duplicate Urls</th>
              <th>Valid Url Counts</th>
              <th>Invalid Urls Counts</th>
              <th>Not in WRM</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(exportReport && exportReport.data && exportReport.data.data && exportReport.data.data.result) ?
                exportReport && exportReport.data && exportReport.data.data && exportReport.data.data.result.map((item, it) => {
                  const createdAt =
                    moment(
                      item.createdAt ? item.createdAt : "--"
                    ).format("MMM Do YY")
                  const ogmStartDate =
                    moment(
                      item.ogm_start_date ? item.ogm_start_date : "--"
                    ).format("MMM Do YY")
                  return (
                    <tr key={it}>
                      <td>
                        {createdAt === 'Invalid date' ? '--' : createdAt}
                      </td>
                      <td>
                        OGM-BATCH-{item && item.ogmSummaryId ? item.ogmSummaryId : '--'}
                      </td>
                      <td>
                        {item && item.ogm_interval ? item.ogm_interval : '--'}
                      </td>
                      <td>
                        {ogmStartDate === 'Invalid date' ? '--' : ogmStartDate}
                      </td>
                      <td>
                        {item && item.totalCount ? item.totalCount : '--'}
                      </td>
                      <td>
                        {item && item.duplicateUrls ? item.duplicateUrls : '--'}
                      </td>
                      <td>
                        {item && item.validUrlCounts ? item.validUrlCounts : '--'}
                      </td>
                      <td>
                        {item && item.inValideUrlsCounts ? item.inValideUrlsCounts : '--'}
                      </td>
                      <td>
                        {item && item.notInWebriskCounts ? item.notInWebriskCounts : '--'}
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OGMsummaryStore } = state

  return {
    loading: OGMsummaryStore && OGMsummaryStore.loading ? OGMsummaryStore.loading : false,
    OGMsummarylists: OGMsummaryStore && OGMsummaryStore.OGMsummarylists ? OGMsummaryStore.OGMsummarylists : {},    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
    state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
  setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
    ? state.clientCrudFilterStore.setCredFilterParams : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getOGMSummarylistDispatch: (params) => dispatch(OGMSummaryActions.getOGMSummarylist(params)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(OgmSummary)
