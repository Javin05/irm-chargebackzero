import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { OGMSummaryActions } from '../../store/actions'
import { DATE } from '../../utils/constants'
import { DateSelector } from '../../theme/layout/components/DateSelector'
import moment from "moment"
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'
import color from "../../utils/colors"
import ReactSelect from '../../theme/layout/components/ReactSelect'

function SearchList(props) {
  const { getOGMSummarylistDispatch, loading, setCredFilterParams , setSearchData, clinetIdLists} = props
  const [error, setError] = useState({});
  const [show, setShow] = useState(false)
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [formData, setFormData] = useState({
    ogmSummaryId: '',
    ogm_interval: '',
    ogm_start_date: '',
    createdAtFrom: '',
    createdAtTo: '',
    clientId:''
  })
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData({...formData, clientId:selectedOption.value})
    }
  }

  const [condition, setcondition] = useState(true)
  const [clear, setClear] = useState(false)

  const handleSearch = (e) => {
    // const errors = {}
    // if (_.isEmpty(formData.tag)) {
    //   errors.tag = 'Tag is required.'
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
      setShow(false)
      const UpDate = moment(formData.ogm_start_date).format("YYYY-MM-DD")
      const DpDate = moment(formData.createdAtFrom).format("YYYY-MM-DD")
      const DeFrDate = moment(formData.createdAtTo).format("YYYY-MM-DD")
      const params = {
        ogm_start_date: UpDate === 'Invalid date' ? '' : UpDate ,
        createdAtFrom:  DpDate  === 'Invalid date' ? '' : DpDate ,
        createdAtTo:  DeFrDate  === 'Invalid date' ? '' : DeFrDate ,
        ogmSummaryId: formData.ogmSummaryId,
        ogm_interval: formData.ogm_interval,
        clientId: formData.clientId
      }
      getOGMSummarylistDispatch(params)
      setSearchData(params)
    // }
  }

  const handleChanges = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...error, [e.target.name]: '' })
  }

  const handleReset = () => {
    setFormData({
      uploadedDateFrom: clear,
      uploadedDateTo: new Date(),
    })
    setSearchData({})
    const params = {
      limit: 25,
      page: 1
    }
    getOGMSummarylistDispatch(params)
    setSelectedAsigneesOption('')
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-md btn-light-primary btn-responsive me-3 pull-right w-100px'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => setShow(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => {setShow(false)
            setFormData({})
            setSelectedAsigneesOption('')}}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className="col-lg-4 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Client :
                    </label>
                    <div className='col-lg-11'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                        // isDisabled={!AsigneesOption}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Ogm Summary Id :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Ogm Summary Id'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': formData.ogmSummaryId && error.ogmSummaryId },
                          {
                            'is-valid': formData.ogmSummaryId && !error.ogmSummaryId
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='ogmSummaryId'
                        autoComplete='off'
                        value={formData.ogmSummaryId || ''}
                      />
                      {errors && errors.ogmSummaryId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.ogmSummaryId}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Ogm Interval :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='ogm_interval'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.ogm_interval || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='WEEKLY'>WEEKLY</option>
                        <option value='MONTHLY'>MONTHLY</option>
                        <option value='QUARTERLY'>QUARTERLY</option>
                        <option value='ONE TIME'>ONE TIME</option>
                      </select>
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                    Ogm Start Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogm_start_date'
                        placeholder='Ogm Start Date'
                        className='form-control'
                        selected={formData.ogm_start_date || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, ogm_start_date: '' })
                          setFormData((values) => ({
                            ...values,
                            ogm_start_date: date
                          }))
                          setcondition(false)
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                    Created At From:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='createdAtFrom'
                        placeholder='Created At From'
                        className='form-control'
                        selected={formData.createdAtFrom || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, createdAtFrom: '' })
                          setFormData((values) => ({
                            ...values,
                            createdAtFrom: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Created At To:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='createdAtTo'
                        placeholder='Created At To'
                        className='form-control'
                        selected={formData.createdAtTo || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, createdAtTo: '' })
                          setFormData((values) => ({
                            ...values,
                            createdAtTo: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='form-group row mb-4 mt-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => handleSearch()}
                          disabled={loading}
                        >
                          {loading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Search'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => handleReset()}
                        >
                          Reset
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OGMsummaryStore,clinetListStore } = state
  return {
    loading: OGMsummaryStore && OGMsummaryStore.loading ? OGMsummaryStore.loading : false,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
  }
}

const mapDispatchToProps = dispatch => ({
  getOGMSummarylistDispatch: (params) => dispatch(OGMSummaryActions.getOGMSummarylist(params))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)