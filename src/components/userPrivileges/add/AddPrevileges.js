import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";
import { KTSVG } from "../../../theme/helpers";
import { STATUS_RESPONSE } from "../../../utils/constants";
import ReactSelect from "../../../theme/layout/components/ReactSelect";
import { Modal } from "../../../theme/layout/components/modal";
import {
  userRolesActions,
  userTypesActions,
  updatePrivilegesActions,
  getCompPermissionActions,
  getMenuCompActions,
  addPrivilegesActions,
} from "../../../store/actions";
import {
  warningAlert,
  confirmationAlert,
  confirmAlert,
} from "../../../utils/alerts";
import { userRoleValidation } from "../validation";
import color from "../../../utils/colors";
import _ from "lodash";

const AddPrevileges = (props) => {
  const {
    getUserroleDispatch,
    getUserTypeDispatch,
    userRoleData,
    getDataUserType,
    getCompPermissionDispatch,
    clearGetMenuCompDispatch,
    clearCompPermissionDispatch,
    clearUpdatePrivilegesDispatch,
    statusUpdatePrivi,
    messageUpdatePrivi,
    loadingUpdatePrivi,
    setShow,
    show,
    getMenuComp,
    loadingGMC,
    getMenuCompActionsDispatch,
    addPrivilegesDispatch,
    clearAddPrivilegesDispatch,
    loadingAddPrivi,
    statusAddPrivi,
    messageAddPrivi,
    updateId,
    editMode,
    setEditMode,
    setUpdateId,
    updatePrivilegesDispatch,
    getCompPermission,
    loadingGCP,
    statusGCP,
    statusGMC
  } = props;
  const history = useHistory()
  const didMount = React.useRef(false)
  const [errors, setErrors] = useState({});
  const [componentDetails, setComponentDetails] = useState([]);
  const [selectedUserRoleOption, setSelectedUserRoleOption] = useState("");
  const [userRoleOption, setUserRoleOption] = useState();
  const [selectedUserTypeOption, setSelectedUserTypeOption] = useState("");
  const [userTypeOption, setUserTypeOption] = useState();
  const [formData, setFormData] = useState({
    roleId: "",
    usertypeId: "",
    tag:"IRM",
    userPrivileges: [
      {
        componentId: "",
        permissions: []
      }
    ]
  });

  const resetState = () => {
    setSelectedUserRoleOption()
    setSelectedUserTypeOption()
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  };

  const getDefaultOptions = (data, name) => {
    const defaultOptions = [];
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? _.startCase(item[name]) : ""}`,
          value: item._id,
        })
      );
      return defaultOptions;
    }
  };

  useEffect(() => {
    const data = getDefaultOptions(userRoleData, "role");
    setUserRoleOption(data);
  }, [userRoleData]);

  useEffect(() => {
    const data = getDefaultOptions(getDataUserType, "userType");
    setUserTypeOption(data);
  }, [getDataUserType]);

  const handleChangeRole = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserRoleOption(selectedOption);
      setFormData((values) => ({
        ...values,
        roleId: selectedOption.value,
      }));
      setErrors((values) => ({ ...values, roleId: "" }));
    } else {
      setSelectedUserRoleOption();
      setFormData((values) => ({ ...values, rolroleId: "" }));
    }
  };

  const checkNameExist = (arr, name, id) => {
    let hasExist = false
    arr && arr.map((item) => {
      if (item && item.componentId === id) {
        item.permissions && item.permissions.map(obj => {
          if (obj === name) {
            hasExist = true
          }
        })
      }
    });
    return hasExist
  }

  const handleCheckbox = (e, id, parantName) => {
    const { name, checked } = e.target;
    setErrors((values) => ({ ...values, userPermissions: "" }));
    let compIdHasExist = false
    let currentUserId = 0;
    formData.userPrivileges && formData.userPrivileges.forEach((item, i) => {
      if (item && item.componentId && _.includes(item.componentId, id)) {
        compIdHasExist = true
      }
      if (item && item.componentId === id) {
        currentUserId = i
      }
    })
    const permissionsHasValues = formData.userPrivileges &&
      formData.userPrivileges[currentUserId] &&
      formData.userPrivileges[currentUserId].permissions &&
      formData.userPrivileges[currentUserId].permissions.length > 0
    setComponentDetails((values) => (
      values.map(menu =>
        menu && menu.permissions &&
          menu.permissions.length > 0 &&
          menu.component === parantName ?
          Object.assign(menu, {
            permissions: menu.permissions && menu.permissions.map(menuPermission =>
              menuPermission && menuPermission.label && menuPermission.label === name ?
                Object.assign(menuPermission, {
                  menu: checked ? "YES" : "NO"
                }) : menuPermission
            )
          })
          : menu && menu.submenu && menu.submenu.length > 0 ?
            Object.assign(menu, {
              submenu: menu.submenu.map(subMenu =>
                subMenu.component === parantName ?
                  Object.assign(subMenu, {
                    permissions: subMenu.permissions && subMenu.permissions.map(subMenuPermission =>
                      subMenuPermission && subMenuPermission.label && subMenuPermission.label === name ?
                        Object.assign(subMenuPermission, {
                          menu: checked ? "YES" : "NO"
                        }) : subMenuPermission
                    )
                  }) : subMenu && subMenu.submenu && subMenu.submenu.length > 0 ?
                    Object.assign(subMenu, {
                      submenu: subMenu.submenu.map(child =>
                        child.component === parantName ?
                          Object.assign(child, {
                            permissions: child.permissions && child.permissions.map(childPermission =>
                              childPermission && childPermission.label && childPermission.label === name ?
                                Object.assign(childPermission, {
                                  menu: checked ? "YES" : "NO"
                                }) : childPermission
                            )
                          })
                          : child
                      )
                    })
                    : subMenu
              )
            })
            : menu
      )
    ));
    const nameHasExist = checkNameExist(formData.userPrivileges && formData.userPrivileges, name, id)
    if (nameHasExist) {
      setFormData((values) => ({
        ...values,
        userPrivileges: values.userPrivileges.map(obj =>
          obj && obj.componentId === id ?
            Object.assign(obj, {
              componentId: id,
              permissions: obj.permissions && obj.permissions.filter((x) => x !== name ? x : null)
            }) : obj
        )
      }));
      return
    }
    if (permissionsHasValues) {
      if (compIdHasExist) {
        setFormData((values) => ({
          ...values,
          userPrivileges: values.userPrivileges.map(o =>
            o && o.componentId === id ?
              Object.assign(o, {
                componentId: id,
                permissions: [
                  ...values.userPrivileges[currentUserId].permissions, name
                ]
              }) : o
          )
        }));
      } else {
        setFormData((values) => ({
          ...values,
          userPrivileges: [
            ...values.userPrivileges,
            {
              componentId: id,
              permissions: [name]
            }
          ]
        }));
      };
    } else {
      setFormData((values) => ({
        ...values,
        userPrivileges: [
          ...values.userPrivileges,
          {
            componentId: id,
            permissions: [name]
          }
        ]
      }));
    }
  };

  const handleChangeUserType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedUserTypeOption(selectedOption);
      setFormData((values) => ({
        ...values,
        usertypeId: selectedOption.value,
      }));
      const params = {
        userTypeId: selectedOption.value,
        tag:"IRM",
        skipPagination: 'true'
      }
      getUserroleDispatch(params)

      setErrors((values) => ({ ...values, usertypeId: "" }));
    } else {
      setSelectedUserTypeOption();
      setFormData((values) => ({ ...values, usertypeId: "" }));
    }
  };

  const handleSubmit = () => {
    const getPrivileges = formData && formData.userPrivileges &&
      formData.userPrivileges.length > 0 &&
      formData.userPrivileges.filter((x) => x.permissions && x.permissions.length > 0 ? x : null)
    const paylaod = {
      ...formData,
      userPrivileges: getPrivileges
    }
    const errorMsg = userRoleValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        updatePrivilegesDispatch(paylaod);
      } else {
        addPrivilegesDispatch(paylaod);
      }
    }
  };

  useEffect(() => {
    if (show) {
      const params ={
        tag:"IRM"
      }
      // getUserroleDispatch(params);
      getUserTypeDispatch(params);
      if (!editMode) {
        getMenuCompActionsDispatch(params);
      }
    } else {
      clearCompPermissionDispatch()
      clearGetMenuCompDispatch()
    }
  }, [show]);

  const onCloseFun = () => {
    setSelectedUserRoleOption();
    setSelectedUserTypeOption();
    setComponentDetails()
    clearGetMenuCompDispatch()
    clearCompPermissionDispatch()
    setErrors({})
    setUpdateId({
      userTypeId: "",
      roleId: ""
    })
    setEditMode(false)
    setFormData({
      roleId: "",
      usertypeId: "",
      userPrivileges: [
        {
          componentId: "",
          permissions: []
        }
      ]
    })
  }

  const onConfirm = () => {
    onCloseFun()
    history.push("/user-privileges")
  };

  useEffect(() => {
    if (statusUpdatePrivi === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageUpdatePrivi,
        "success",
        "Back to Users Previleges",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearUpdatePrivilegesDispatch();
    } else if (statusUpdatePrivi === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "Error",
        messageUpdatePrivi,
        "error",
        "Close",
        "Ok",
        () => {
          onConfirm();
        },
        () => { }
      );
      clearUpdatePrivilegesDispatch();
    }
  }, [statusUpdatePrivi]);

  useEffect(() => {
    if (statusAddPrivi === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        "Success",
        messageAddPrivi,
        "success",
        "Back to Users Previleges",
        "Ok",
        () => {
          onConfirm();
        },
        () => { }
      );
      clearAddPrivilegesDispatch();
    } else if (statusAddPrivi === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "Error",
        messageAddPrivi,
        "error",
        "Ok",
        "",
        () => { },
        () => { }
      );
      clearAddPrivilegesDispatch();
    }
  }, [statusAddPrivi]);

  useEffect(() => {
    if (didMount.current) {
      if (statusGCP === STATUS_RESPONSE.SUCCESS_MSG) {
        if (getCompPermission && getCompPermission.length > 0 &&  editMode) {
          setComponentDetails(getCompPermission)
          const getUserPrivileges = []
          const getUserPrivilegesCompId = []
          getCompPermission && getCompPermission.forEach(parent => {
            if (parent && parent.permissions && parent.permissions.length > 0) {
              const tempPermissons = []
              parent.permissions.forEach(parentPermission => {
                if (parentPermission && parentPermission.menu === "YES") {
                  tempPermissons.push(
                    parentPermission.label
                  )
                  if (!getUserPrivilegesCompId.includes(parent._id)) {
                    getUserPrivilegesCompId.push(parent._id)
                    getUserPrivileges.push(
                      {
                        componentId: parent._id,
                        permissions: tempPermissons
                      }
                    )
                  }
                }
              }
              )
            }
            if (parent && parent.submenu && parent.submenu.length > 0) {
              parent.submenu.forEach(child => {
                const tempPermissons = []
                child.permissions.forEach(childPermission => {
                  if (childPermission && childPermission.menu === "YES") {
                    tempPermissons.push(
                      childPermission.label
                    )
                    if (!getUserPrivilegesCompId.includes(child._id)) {
                      getUserPrivilegesCompId.push(child._id)
                      getUserPrivileges.push(
                        {
                          componentId: child._id,
                          permissions: tempPermissons
                        }
                      )
                    }
                  }
                }
                )
                if (child && child.submenu && child.submenu.length > 0) {
                  child.submenu.map(grantChild => {
                    grantChild && grantChild.permissions &&
                      grantChild.permissions.length > 0 &&
                      grantChild.permissions.map(grantChildPer => {
                        if (grantChildPer && grantChildPer.menu === "YES") {
                          const tempChildPermissons = []
                          if (!getUserPrivilegesCompId.includes(grantChild._id)) {
                            tempChildPermissons.push(
                              grantChildPer.label
                            )
                            getUserPrivilegesCompId.push(grantChild._id)
                            getUserPrivileges.push(
                              {
                                componentId: grantChild._id,
                                permissions: tempChildPermissons
                              }
                            )
                          }
                        }
                      })
                  })
                }
              }
              )
            }
          })
          setFormData((values) => ({
            ...values,
            userPrivileges: getUserPrivileges
          }));
        }
      }
    }
  }, [statusGCP]);

  useEffect(() => {
    if (statusGMC === STATUS_RESPONSE.SUCCESS_MSG) {
      if (getMenuComp && getMenuComp.length > 0 && !editMode) {
        setComponentDetails(getMenuComp)
      }
    }
  }, [statusGMC, getMenuComp]);

  useEffect(() => {
    if (editMode) {
      if (updateId.userTypeId && updateId.userTypeId.value) {
        const detailsParams = {
          usertypeId: updateId.userTypeId.value,
          roleId: updateId.roleId.value,
          tag: 'IRM'
        }
        getCompPermissionDispatch(detailsParams);
        setSelectedUserTypeOption(updateId.userTypeId)
        setSelectedUserRoleOption(updateId.roleId)
        setFormData((values) => ({
          ...values,
          roleId: updateId.roleId.value,
          usertypeId: updateId.userTypeId.value
        }));
      }
    }
  }, [editMode]);

  useEffect(() => {
    if (!didMount.current) { didMount.current = true }
    return () => {
      setSelectedUserRoleOption();
      setSelectedUserTypeOption();
      onCloseFun()
    };
  }, []);

  return (
    <>
      <Modal showModal={show} modalWidth={1000}>
        <div className="" id="PrivilegeModal">
          <div>
            <div className="modal-dialog modal-dialog-centered mw-1000px">
              <div className="modal-content">
                <div className="modal-header">
                  <h2 className="me-8 mb-4 mt-4 ms-4">{editMode ? "Edit" : "Add"} User Previlege</h2>
                  <button
                    type="button"
                    className="btn btn-lg btn-icon btn-active-light-primary close m-4"
                    data-dismiss="modal"
                    onClick={() => {
                      onCloseFun()
                      setShow(false);
                      resetState()
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG
                      path="/media/icons/duotune/arrows/arr061.svg"
                      className="svg-icon-1"
                    />
                    {/* eslint-disable */}
                  </button>
                </div>
                <div className="container-fixed">
                  <div className="card-header bg-lightBlue">
                    <div className="card-body scroll h-550px px-5">
                      <div className="fv-row mb-10 fv-plugins-icon-container">
                        <label className="fs-5 fw-bolder form-label mb-2 mt-4">
                          <span className="required">User Type:</span>
                        </label>
                        <div className="row">
                          <div className="col-lg-1" />
                          <div className="col-lg-6">
                            <ReactSelect
                              styles={customStyles}
                              isMulti={false}
                              name="usertypeId"
                              className="basic-single"
                              classNamePrefix="select"
                              handleChangeReactSelect={handleChangeUserType}
                              options={userTypeOption}
                              value={selectedUserTypeOption}
                            />
                          </div>
                        </div>
                        {errors && errors.usertypeId && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {errors.usertypeId}
                          </div>
                        )}
                      </div>
                      <div className="fv-row mb-10 fv-plugins-icon-container">
                        <label className="fs-5 fw-bolder form-label mb-2">
                          <span className="required">User Role :</span>
                        </label>
                        <div className="row">
                          <div className="col-lg-1" />
                          <div className="col-lg-6">
                            <ReactSelect
                              styles={customStyles}
                              isMulti={false}
                              name="roleId"
                              className="basic-single"
                              classNamePrefix="select"
                              handleChangeReactSelect={handleChangeRole}
                              options={userRoleOption}
                              value={selectedUserRoleOption}
                            />
                          </div>
                        </div>
                        {errors && errors.roleId && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {errors.roleId}
                          </div>
                        )}
                      </div>
                      <label className="fs-5 fw-bolder form-label mb-2">
                        <span className="required mb-6">
                          Role Permissions :
                        </span>
                        {errors && errors.userPermissions && (
                          <span className="rr mt-1 mx-4 fw-normal">
                            <style>{".rr{color:red} "}</style>
                            {errors.userPermissions}
                          </span>
                        )}
                      </label>
                      <div className="scroll h-350px">
                        {loadingGMC || loadingGCP ? (
                          <div className="d-flex justify-content-center py-5">
                            <div
                              className="spinner-border text-primary m-5"
                              role="status"
                            />
                          </div>
                        ) : componentDetails && componentDetails.length > 0 ? (
                          componentDetails.map((menu, i) => (
                            <div className="fv-row" key={i}>
                              <div className="fs-4 fw-bolder form-label text-gray-900 mb-2 mt-8">
                                {menu.component}
                              </div>
                              <div className="px-20 d-flex scroll py-3">
                                {menu.permissions && menu.permissions.length > 0 ? (
                                  editMode ? (
                                    menu.permissions.map((permission, i) => (
                                      <label key={i}
                                        className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20"
                                      >
                                        <input
                                          className="form-check-input"
                                          type="checkbox"
                                          value=""
                                          checked={permission.menu === "YES" || false}
                                          onChange={(e) => handleCheckbox(e, menu._id, menu.component)}
                                          name={permission.label}
                                        />
                                        <span className="form-check-label text-gray-600">
                                          {permission.label}
                                        </span>
                                      </label>
                                    ))
                                  ) : (
                                    menu.permissions.map((permission, i) => (
                                      <label key={i}
                                        className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20"
                                      >
                                        <input
                                          className="form-check-input"
                                          type="checkbox"
                                          onChange={(e) => handleCheckbox(e, menu._id, menu.component)}
                                          name={permission}
                                        />
                                        <span className="form-check-label text-gray-600">
                                          {permission}
                                        </span>
                                      </label>
                                    ))
                                  )
                                ) : null}
                              </div>
                              {menu.submenu && menu.submenu.length > 0
                                ? menu.submenu.map((subMenu, index) => (
                                  <div key={index}>
                                    <div className="fs-5 fw-bolder form-label text-gray-800 mb-2 mt-2 p-4">
                                      {subMenu.component}
                                    </div>
                                    <div className="px-20 d-flex scroll py-3">
                                      {subMenu.permissions && subMenu.permissions.length > 0 ? (
                                        subMenu.permissions.map((subPermission, i) => (
                                          editMode ? (
                                            <label key={i}
                                              className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20"
                                            >
                                              <input
                                                className="form-check-input"
                                                type="checkbox"
                                                value=""
                                                checked={subPermission.menu === "YES" || false}
                                                onChange={(e) => handleCheckbox(e, subMenu._id, subMenu.component)}
                                                name={subPermission.label}
                                              />
                                              <span className="form-check-label text-gray-600">
                                                {subPermission.label}
                                              </span>
                                            </label>
                                          ) : (
                                            <label key={i}
                                              className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20"
                                            >
                                              <input
                                                className="form-check-input"
                                                type="checkbox"
                                                onChange={(e) => handleCheckbox(e, subMenu._id, subMenu.component)}
                                                name={subPermission}
                                              />
                                              <span className="form-check-label text-gray-600">
                                                {subPermission}
                                              </span>
                                            </label>
                                          )
                                        ))
                                      ) : null
                                      }
                                    </div>
                                    {subMenu.submenu &&
                                      subMenu.submenu.length > 0
                                      ? subMenu.submenu &&
                                      subMenu.submenu.map((child, id) => (
                                        <div key={id}>
                                          <div className="fs-5 fw-bolder form-label text-gray-700 mb-2 mt-2 p-6 px-20">
                                            {child.component}
                                          </div>
                                          <div className="d-flex justify-content-center scroll py-3">
                                            {editMode ? (
                                              child.permissions.map((superSubPermission, i) => (
                                                <label key={i}
                                                  className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20"
                                                >
                                                  <input
                                                    className="form-check-input"
                                                    type="checkbox"
                                                    value=""
                                                    checked={superSubPermission.menu === "YES" || false}
                                                    onChange={(e) => handleCheckbox(e, child._id, child.component)}
                                                    name={superSubPermission.label}
                                                  />
                                                  <span className="form-check-label text-gray-600">
                                                    {superSubPermission.label}
                                                  </span>
                                                </label>
                                              ))
                                            ) : (
                                              child.permissions.map((superSubPermission, i) => (
                                                <label key={i}
                                                  className="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20"
                                                >
                                                  <input
                                                    className="form-check-input"
                                                    type="checkbox"
                                                    onChange={(e) => handleCheckbox(e, child._id, child.component)}
                                                    name={superSubPermission}
                                                  />
                                                  <span className="form-check-label text-gray-600">
                                                    {superSubPermission}
                                                  </span>
                                                </label>
                                              ))
                                            )}
                                          </div>
                                        </div>
                                      ))
                                      : null}
                                  </div>
                                ))
                                : null}
                            </div>
                          ))
                        ) : null}
                      </div>
                      <div className="form-group row mt-4">
                        <div className="col-lg-6" />
                        <div className="col-lg-6">
                          <div className="col-lg-10">
                            <button
                              className="btn btn-blue mt-7 fa-pull-right"
                              disabled={loadingGMC}
                              onClick={() => {
                                handleSubmit();
                              }}
                            >
                              {loadingAddPrivi || loadingUpdatePrivi ? (
                                <span
                                  className="spinner-border spinner-border-sm mx-3"
                                  role="status"
                                  aria-hidden="true"
                                />
                              ) : (
                                "Submit"
                              )}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => {
  const {
    userroleStore,
    usertypeStore,
    getMenuCompStore,
    addPrivilegesStore,
    updatePrivilegesStore,
    getCompPermissionStore
  } = state;
  return {
    userRoleData:
      userroleStore && userroleStore.userRoleData
        ? userroleStore.userRoleData
        : {},
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
    loadingGetRole:
      usertypeStore && usertypeStore.loadingGetRole
        ? usertypeStore.loadingGetRole
        : false,
    statusUpdatePrivi:
      updatePrivilegesStore && updatePrivilegesStore.statusUpdatePrivi
        ? updatePrivilegesStore.statusUpdatePrivi
        : "",
    messageUpdatePrivi:
      updatePrivilegesStore && updatePrivilegesStore.messageUpdatePrivi
        ? updatePrivilegesStore.messageUpdatePrivi
        : "",
    loadingUpdatePrivi:
      updatePrivilegesStore && updatePrivilegesStore.loadingUpdatePrivi
        ? updatePrivilegesStore.loadingUpdatePrivi
        : false,
    loadingGCP:
      getCompPermissionStore && getCompPermissionStore.loadingGCP
        ? getCompPermissionStore.loadingGCP
        : false,
    getCompPermission:
      getCompPermissionStore && getCompPermissionStore.getCompPermission
        ? getCompPermissionStore.getCompPermission
        : [],
    statusGCP:
      getCompPermissionStore && getCompPermissionStore.statusGCP
        ? getCompPermissionStore.statusGCP
        : '',
    getMenuComp:
      getMenuCompStore && getMenuCompStore.getMenuComp
        ? getMenuCompStore.getMenuComp
        : [],
    loadingGMC:
      getMenuCompStore && getMenuCompStore.loadingGMC
        ? getMenuCompStore.loadingGMC
        : false,
    statusGMC:
      getMenuCompStore && getMenuCompStore.statusGMC
        ? getMenuCompStore.statusGMC
        : '',
    addPrivileges:
      addPrivilegesStore && addPrivilegesStore.addPrivileges
        ? addPrivilegesStore.addPrivileges
        : {},
    statusAddPrivi:
      addPrivilegesStore && addPrivilegesStore.statusAddPrivi
        ? addPrivilegesStore.statusAddPrivi
        : "",
    messageAddPrivi:
      addPrivilegesStore && addPrivilegesStore.messageAddPrivi
        ? addPrivilegesStore.messageAddPrivi
        : "",
    loadingAddPrivi:
      addPrivilegesStore && addPrivilegesStore.loadingAddPrivi
        ? addPrivilegesStore.loadingAddPrivi
        : false
  };
};

const mapDispatchToProps = (dispatch) => ({
  getUserroleDispatch: (params) =>
    dispatch(userRolesActions.getUserrole(params)),
  getUserTypeDispatch: (params) =>
    dispatch(userTypesActions.getUserType(params)),
  updatePrivilegesDispatch: (data) =>
    dispatch(updatePrivilegesActions.update(data)),
  clearUpdatePrivilegesDispatch: () =>
    dispatch(updatePrivilegesActions.clear()),
  getCompPermissionDispatch: (params) => dispatch(getCompPermissionActions.get(params)),
  clearCompPermissionDispatch: () => dispatch(getCompPermissionActions.clear()),
  getMenuCompActionsDispatch: (data) => dispatch(getMenuCompActions.get(data)),
  clearGetMenuCompDispatch: () => dispatch(getMenuCompActions.clear()),
  addPrivilegesDispatch: (data) =>
    dispatch(addPrivilegesActions.add(data)),
  clearAddPrivilegesDispatch: () => dispatch(addPrivilegesActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddPrevileges);
