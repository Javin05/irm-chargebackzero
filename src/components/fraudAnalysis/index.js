import React, { useEffect, useState } from "react";
import _ from "lodash";
import { FraudToolListActions } from "../../store/actions";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
import { getLocalStorage, removeLocalStorage } from "../../utils/helper";
import { SET_FILTER } from "../../utils/constants";
import { RISKSTATUS } from "../../utils/constants";
import Form from "./Form";
import moment from "moment";

function WrmOperationManagementList(props) {
  const {
    className,
    loading,
    fraudToolActionstatus,
    getFraudToolListDispatch,
    fraudToolList,
  } = props;

  const didMount = React.useRef(false);
  const [limit, setLimit] = useState(20);
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [Value, setValue] = useState(false);
  const paginationSearch = JSON.parse(getLocalStorage("WRM_OPS_SEARCH"));
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER);
    const credBasedParams = {
      clientId: credBasedClientValue,
    };
    const params = {
      limit: limit,
      page: activePageNumber,
      ...credBasedParams,
      ...searchParams,
      ...paginationSearch,
    };
    const pickByParams = _.pickBy(params);
    getFraudToolListDispatch(pickByParams);
  }, [activePageNumber]);

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: activePageNumber,
    };
    setActivePageNumber(pageNumber);
    getFraudToolListDispatch(params);
  };

  const totalPages =
    fraudToolList && fraudToolList.count
      ? Math.ceil(parseInt(fraudToolList && fraudToolList.count) / limit)
      : 1;

  useEffect(() => {
    return (
      setValue(true),
      setTimeout(() => {
        setValue(false);
      }, 1500)
    );
  }, []);

  const hadelReset = () => {
    const params = {
      limit: limit,
      page: 1,
      ...searchParams,
    };
    removeLocalStorage("WRM_OPS_SEARCH");
    setValue(true);
  };

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  });

  return (
    <>
      <div className={`card ${className}`}>
        <div className="card-body py-3">
          <div className="row">
            <div className="d-flex justify-content-start col-md-12 col-lg-12">
              <div className="col-md-6 mt-1 ms-2">
                {fraudToolList && fraudToolList && fraudToolList.count && (
                  <span className="text-muted fw-bold d-flex fs-3 mt-2">
                    Total:
                    <span className="text-gray-700 fw-bolder text-hover-primary fs-3">
                      {fraudToolList && fraudToolList && fraudToolList.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className="d-flex justify-content-center col-md-12 col-lg-12">
              <div className="col-md-8 mt-1 ms-2">
                <Form />
              </div>
            </div>
            <div className="d-flex col-md-12 col-lg-12 justify-content-end my-auto mt-4">
              <ul className="nav nav-tabs nav-line-tabs fs-6">
                <li className="nav-item">
                  {/* <SearchList Value={Value}
                    clientList={clinetIdLists && clinetIdLists.data && clinetIdLists.data.result ? clinetIdLists.data.result : []}
                  /> */}
                </li>
              </ul>
            </div>
          </div>

          <br />
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className="fw-bolder fs-8 text-gray-800">
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Analysis Id</span>
                    </div>
                  </th>
                  {Role === "Admin" && (
                    <th>
                      <div className="d-flex">
                        <span>Client name</span>
                      </div>
                    </th>
                  )}

                  <th>
                    <div className="d-flex">
                      <span>API name</span>
                    </div>
                  </th>

                  <th>
                    <div className="d-flex">
                      <span>Job id</span>
                    </div>
                  </th>

                  {/* <th>
                    <div className="d-flex col-sm-8" >
                      <span>Input</span>

                    </div>
                   
                  </th>    */}
                  <th>
                    <div className="d-flex">
                      <span>Fraud tool status</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Created at</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className="fs-8">
                <tr>
                  <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                    <a
                      className="color-primary cursor-pointer"
                      // onClick={()=>handleSearch("","","","",riskoperationmgmtlist.caseId)}
                      onClick={() =>
                        window.open(`fraudanalysis-tool/shoplens`, "_blank")
                      }
                      to={`/wrm-management`}
                    >
                      <span>
                        <i
                          style={{ color: "blue", fontSize: "20px" }}
                          className="bi bi-eye-fill"
                        ></i>
                      </span>
                    </a>
                  </td>
                  <td className="ellipsis">
                    <>FAT108711</>
                  </td>

                  <td className="ellipsis">
                    <>IRM Test User</>
                  </td>

                  <td className="ellipsis">
                    <>Shop lens - inhouse api</>
                  </td>
                  <td className="ellipsis">
                    <>
                      <span>67580110919a31264c2eba22</span>
                    </>
                  </td>
                  <td className="ellipsis">
                    <span class="badge badge-success text-black">
                      Completed
                    </span>
                  </td>
                  <td className="ellipsis">
                    <span>1 min ago</span>
                  </td>
                </tr>

                {!loading ? (
                  fraudToolList && fraudToolList.data ? (
                    fraudToolList.data &&
                    fraudToolList.data.map((ele, i) => {
                      return (
                        <tr
                          key={"reef_" + i}
                          style={
                            i === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                          <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                            <a
                              className="color-primary cursor-pointer"
                              // onClick={()=>handleSearch("","","","",riskoperationmgmtlist.caseId)}
                              onClick={() =>
                                window.open(
                                  `/fraudanalysis-tool/update/${ele._id}`,
                                  "_blank"
                                )
                              }
                              to={`/wrm-management`}
                            >
                              <span>
                                <i
                                  style={{ color: "blue", fontSize: "20px" }}
                                  className="bi bi-eye-fill"
                                ></i>
                              </span>
                            </a>
                          </td>
                          <td className="ellipsis">
                            <>{ele.analysisId ? ele.analysisId : "--"}</>
                          </td>
                          {Role === "Admin" && (
                            <td className="ellipsis">
                              <>
                                {ele.client_id ? ele.client_id.company : "--"}
                              </>
                            </td>
                          )}

                          <td className="ellipsis">
                            <>{ele.api_name ? ele.api_name : "--"}</>
                          </td>

                          <td className="ellipsis">
                            <>{ele._id ? ele._id : "--"}</>
                          </td>

                          {/* <td className="ellipsis col-sm-8">
                                  <>
                                    {ele.input_details ? JSON.stringify(ele.input_details.value) : "--"}
                                  </>
                                
                                </td> */}
                          <td className="ellipsis">
                            <>
                              <span
                                className={`badge ${
                                  RISKSTATUS[ele && ele.fraud_tool_status]
                                }`}
                              >
                                {ele.fraud_tool_status
                                  ? ele.fraud_tool_status
                                  : "--"}
                              </span>
                            </>
                          </td>
                          <td className="ellipsis">
                            {ele.createdAt
                              ? moment(ele.createdAt)
                                  .startOf("minute")
                                  .fromNow()
                              : "--"}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr className="text-center py-3">
                      <td colSpan="100%">
                        No record(s) found. Click below button to reset
                        <br />
                        {
                          <div className="my-auto">
                            <button
                              onClick={() => hadelReset()}
                              type="button"
                              className="btn btn-lg btn-primary btn-responsive font-6vw me-3 pull-right"
                            >
                              Reset
                            </button>
                          </div>
                        }
                      </td>
                    </tr>
                  )
                ) : (
                  <tr>
                    <td colSpan="100%" className="text-center">
                      <div
                        className="spinner-border text-primary m-5"
                        role="status"
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={4}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    WrmOperationManagementStore,
    clinetListStore,
    FraudAnalysisListManagementStore,
  } = state;
  return {
    fraudToolList:
      state &&
      state.FraudAnalysisListManagementStore &&
      state.FraudAnalysisListManagementStore.fraudToolData,
    loading:
      WrmOperationManagementStore && FraudAnalysisListManagementStore.loading
        ? FraudAnalysisListManagementStore.loading
        : false,
    fraudToolActionstatus:
      state &&
      state.FraudAnalysisListManagementStore &&
      state.FraudAnalysisListManagementStore,
    WrmOperationActionStatus:
      state &&
      state.wrmOperationManagementActionStore &&
      state.wrmOperationManagementActionStore,
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getFraudToolListDispatch: (params) =>
    dispatch(FraudToolListActions.request(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrmOperationManagementList);
