import { USER_MANAGEMENT_ERROR, REGEX } from "../../utils/constants";
import _ from 'lodash'

export const FraudAnalysisFormValidation = (values, setErrors,api_name) =>{
    const errors = {};
    if(api_name==="aadhar_details"){
  
          if(!values.aadhaar_number&&!values.document){
            errors.aadhaar_number= 'Aadhar number/Image  is required.'
        }
    }
    if(!values.api_name){
        errors.api_name = 'Document  is required.'
    }
    if( api_name==="pan_image"||api_name==="driving_licence_image"||api_name==="passport_image"||api_name==='voter_image'||api_name==='cancellation_cheque_image'||api_name === "gst_image"||api_name === "shop_image_analyzer"||api_name === "shop_lens"||api_name === "verify_sol_ind_electricity_bill"||api_name === "verify_sol_ind_broadband_bill"||api_name === "verify_sol_ind_gas_bill"||api_name === "verify_sol_deed_of_lease"||api_name==="verify_sol_international_passport"||api_name==="doc_ocr_ind_udyog_aadhar"||api_name==="doc_ocr_ind_gst_certificate"){
      
        if(!values.document){
            errors.document = 'File is required.'
        }
    }
    if(api_name==="pan_details"){        
        if(!values.pan_number&&!values.document){
            errors.pan_number= 'Pan numbe/Image is required.'
        }
    }

    if(api_name==="driving_licence_details"){
      
        // if(!values.driving_licence_number){
        //     errors.driving_licence_number = 'Licence number is required.'
        // }
        // if(!values.date_of_birth){
        //     errors.date_of_birth = 'Date of birth is required.'
        // }
        if((!values.driving_licence_numberr||!values.date_of_birth)&&!values.document){
             errors.driving_licence_number = 'Licence number/Image is required.'
        }
    }

    if(api_name==="passport_details"){
    
        if(!values.file_number&&!values.document){
            errors.file_number = 'Passport info/Image is required.'
       }
    }
    if(api_name==="voter_details"){
        if((!values.voter_id_number||!values.full_name)&&!values.document){
            errors.driving_licence_number = 'Voter info/Image is required.'
       }
    }
    if(api_name==="bank_details"){
      
        if(!values.account_number){
            errors.account_number = 'Bank account number is required.'
        }
        if(!values.ifsc_code){
            errors.ifsc_code = 'IFSC code is required.'
        }
    }
    if(api_name==="address_details"){
      
        if(!values.address){
            errors.address = 'Address is required.'
        }
        if(!values.state){
            errors.state = 'State is required.'
        }
        if(!values.city){
            errors.city = 'City is required.'
        }
        if(!values.pincode){
            errors.pincode = 'Pincode is required.'
        }
    }
    if(api_name==="gst_details"){
        if(!values.gst_number&&!values.document){
            errors.driving_licence_number = 'GST Number/Image is required.'
       }
    }
    if(api_name==="cin_details"){
      
        if(!values.cin_number){
            errors.cin_number = 'CIN is required.'
        }

    }
    if(api_name==="phone_number_details" ){
        if(!values.country_code){
            errors.country_code = 'Country code is required.'
        }
        if(!values.phone_number){
            errors.phone_number = 'Phone number is required.'
        }
    }
    if(api_name==="email_id_details"){
        if(!values.email_id){
            errors.email_id = 'Email  is required.'
        }
    }
    if(api_name==="verify_sol_udyog_aadhaar"){
        if(!values.udyog_aadhaar_number&&!values.document){
            errors.udyog_aadhaar_number = 'Udyog Aadhaar Number or image is required.'
        }
    }
    if(api_name==="verify_sol_udyam_aadhaar"){
        if(!values.udyam_aadhaar_number){
            errors.udyam_aadhaar_number = 'Udyam Aadhaar Number  is required.'
        }
    }
    if(api_name==="verify_sol_ind_shop_license"){
        if(!values.ind_shop_license_number){
            errors.ind_shop_license_number = 'Indian shop license Number is required.'
        }
    }
    if(api_name==="verify_sol_establishment_epfo"){
        if(!values.establishment_code_number){
            errors.establishment_code_number = 'EPFO number is required.'
        }
        if(!values.establishment_name){
            errors.establishment_name = 'Establishment name is required.'
        }
    }
    if(api_name==="ind_fssai"){
        if(!values.fbo_registration_no&&!values.document){
            errors.fbo_registration_no= 'FSSAI Reg Number/Image is required.'
        }
    }
    if(api_name==="ind_roc_iec"){
        if(!values.import_export_code){
            errors.import_export_code = 'Import Export Code is required.'
        }
    }
    if(!values.clientId){
        errors.clientId = 'Client  is required.'
    }
    
  setErrors(errors); 
  return errors;
}