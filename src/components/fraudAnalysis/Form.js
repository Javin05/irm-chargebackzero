import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import {
  FraudAnalysisActions,
  FraudToolListActions,
  FraudToolfileUploadActions,
  clientIdLIstActions,
} from "../../store/actions";
import clsx from "clsx";
import { RESTRICTED_FILE_FORMAT_TYPE } from "../../constants";
import _ from "lodash";
import { DROPZONE_MESSAGES } from "../../utils/constants";
import { FraudAnalysisFormValidation } from "./validator";
import { getLocalStorage } from "../../utils/helper";
import Modal from "react-bootstrap/Modal";
import Verification from "./Verification";
import { successAlert } from "../../utils/alerts";
import { formatMs } from "@material-ui/core";

let document = [
  {
    name: "Shop Lens",
    keyName: "shop_lens",
    category: "business",
  },
  {
    name: "Shop Image Analyzer",
    keyName: "shop_image_analyzer",
    category: "business",
  },
  {
    name: "Website",
    keyName: "website",
    category: "business",
  },
  {
    name: "Indian shop license Number",
    keyName: "verify_sol_ind_shop_license",
    category: "business",
  },
  {
    name: "Udyog Aadhaar Number",
    keyName: "verify_sol_udyog_aadhaar",
    category: "business",
  },
  {
    name: "Udyam Aadhaar Number",
    keyName: "verify_sol_udyam_aadhaar",
    category: "business",
  },
  {
    name: "FSSAI details",
    keyName: "ind_fssai",
    category: "business",
  },
  {
    name: "Bank Account Details",
    keyName: "bank_details",
    category: "business",
  },
  {
    name: "Cancellation Cheque Image",
    keyName: "cancellation_cheque_image",
    category: "business",
  },
  {
    name: "GST Details",
    keyName: "gst_details",
    category: "business",
  },
  // {
  //   name: "GST Certificate Image",
  //   keyName: "gst_image",
  //   category: "business",
  // },
  {
    name: "Company Identification Number(CIN)",
    keyName: "cin_details",
    category: "business",
  },
  {
    name: "EPFO Details",
    keyName: "verify_sol_establishment_epfo",
    category: "business",
  },
  {
    name: "Indian Import Export Details",
    keyName: "ind_roc_iec",
    category: "business",
  },
  {
    name: "PAN Number",
    keyName: "pan_details",
    category: "business",
  },
  // {
  //   name: "PAN Image",
  //   keyName: "pan_image",
  //   category: "business",
  // },
  {
    name: "PAN Number",
    keyName: "pan_details",
    category: "individual",
  },
  // {
  //   name: "PAN Image",
  //   keyName: "pan_image",
  //   category: "individual",
  // },
  {
    name: "Aadhar Number",
    keyName: "aadhar_details",
    category: "individual",
  },
  // {
  //   name: "Aadhar Image Details",
  //   keyName: "aadhar_image",
  //   category: "individual",
  // },

  {
    name: "Driving Licence",
    keyName: "driving_licence_details",
    category: "individual",
  },
  // {
  //   name: "Driving Licence Image",
  //   keyName: "driving_licence_image",
  //   category: "individual",
  // },
  {
    name: "Passport",
    keyName: "passport_details",
    category: "individual",
  },
  // {
  //   name: "Passport Image",
  //   keyName: "passport_image",
  //   category: "individual",
  // },
  {
    name: "Voter Id",
    keyName: "voter_details",
    category: "individual",
  },
  // {
  //   name: "Voter Image",
  //   keyName: "voter_image",
  //   category: "individual",
  // },
  {
    name: "Bank Account Details",
    keyName: "bank_details",
    category: "individual",
  },
  {
    name: "Cancellation Cheque Image",
    keyName: "cancellation_cheque_image",
    category: "individual",
  },

  {
    name: "Address",
    keyName: "address_details",
    category: "business",
  },

  {
    name: "Electricity Bill Image",
    keyName: "verify_sol_ind_electricity_bill",
    category: "individual",
  },
  {
    name: "Indian broadband bill Image",
    keyName: "verify_sol_ind_broadband_bill",
    category: "individual",
  },
  {
    name: "Indian gas bill Image",
    keyName: "verify_sol_ind_gas_bill",
    category: "individual",
  },
  {
    name: "Deed Of Lease document image",
    keyName: "verify_sol_deed_of_lease",
    category: "individual",
  },
  // {
  //   name: "Indain FSSAI Document Image",
  //   keyName: "verify_sol_ind_fssai",
  //   category: "business",
  // },

  {
    name: "International Passport Image",
    keyName: "verify_sol_international_passport",
    category: "individual",
  },
  {
    name: "Ind Udyog Aadhar",
    keyName: "doc_ocr_ind_udyog_aadhar",
    category: "business",
  },
  {
    name: "IP address",
    keyName: "ip_address",
    category: "business",
  },
  {
    name: "IP address",
    keyName: "ip_address",
    category: "individual",
  },
  {
    name: "Mobile",
    keyName: "phone_number_details",
    category: "business",
  },
  {
    name: "Email",
    keyName: "email_id_details",
    category: "business",
  },
  {
    name: "Mobile",
    keyName: "phone_number_details",
    category: "individual",
  },
  {
    name: "Email",
    keyName: "email_id_details",
    category: "individual",
  },

  //     name: "Udoyg Aadhar Image",
  //     keyName: "verify_ocr_udyam_reg_doc"
  // },
  //   {
  //     name: "Labour document UP",
  //     keyName: "verify_ocr_up_labour_doc"
  // },
  // {

  // {
  //     name: "Labour document Delhi",
  //     keyName: "verify_ocr_delhi_labour_doc"
  // },
  // {
  //     name: "Labour document Telangana",
  //     keyName: "verify_ocr_telangana_labour_doc"
  // },
  // {
  //     name: "Labour document Haryana",
  //     keyName: "verify_ocr_haryana_labour_doc"
  // },
  // {
  //     name: "Drug document MP",
  //     keyName: "verify_ocr_mp_drug_doc"
  // },
  // {
  //     name: "Drug and Food document Haryana",
  //     keyName: "verify_ocr_haryana_food_drug_doc"
  // },
  // {
  //     name: "Drug document Rajasthan",
  //     keyName: "verify_ocr_rajasthan_drug_doc"
  // },
  // {
  //     name: "Drug document Karnataka",
  //     keyName: "verify_ocr_karnataka_drug_doc"
  // },
  // {
  //     name: "Municipal document Ahmedabad",
  //     keyName: "verify_ocr_ahmedabad_municipal_doc"
  // },
  // {
  //     name: "Drug document Delhi",
  //     keyName: "verify_ocr_delhi_drug_doc"
  // },
  // {
  //     name: "Chennai document Greater",
  //     keyName: "verify_ocr_greater_chennai_doc"
  // },
  // {
  //     name: "Hyd document Greater",
  //     keyName: "verify_ocr_greater_hyd_doc"
  // },
  // {
  //     name: "Drug document Gujarat",
  //     keyName: "verify_ocr_gujarat_drug_doc"
  // },
  // {
  //     name: "Drug document Maharashtra",
  //     keyName: "verify_ocr_maharashtra_drug_doc"
  // },
  // {
  //     name: "Registration document WB",
  //     keyName: "verify_ocr_wb_registration_doc"
  // },
  // {
  //     name: "Labour document UP image",
  //     keyName: "verify_up_labour_doc"
  // },
  // {
  //     name: "Labour document Delhi image",
  //     keyName: "verify_delhi_labour_doc"
  // },
  // {
  //     name: "Labour document Telangana image",
  //     keyName: "verify_telangana_labour_doc"
  // },
  // {
  //     name: "Labour document Haryana image",
  //     keyName: " verify_haryana_labour_doc"
  // },
  // {
  //     name: "Drug document MP image",
  //     keyName: "verify_mp_drug_doc"
  // },
  // {
  //     name: "Drug and Food document Haryana image",
  //     keyName: "verify_haryana_food_drug_doc"
  // },
  // {
  //     name: "Drug document Rajasthan image",
  //     keyName: "verify_rajasthan_drug_doc"
  // },
  // {
  //     name: "Drug document Karnataka image",
  //     keyName: "verify_karnataka_drug_doc"
  // },
  // {
  //     name: "Municipal document Ahmedabad image",
  //     keyName: "verify_ahmedabad_municipal_doc"
  // },
  // {
  //     name: "Drug document Delhi image",
  //     keyName: "verify_delhi_drug_doc"
  // },
  // {
  //     name: "Chennai document Chennai image",
  //     keyName: "verify_greater_chennai_doc"
  // },
  // {
  //     name: "Hyd document Hyd image",
  //     keyName: "verify_greater_hyd_doc"
  // },
  // {
  //     name: "Drug document Gujarat image",
  //     keyName: "verify_gujarat_drug_doc"
  // },
  // {
  //     name: "Drug document Maharashtra image",
  //     keyName: "verify_maharashtra_drug_doc"
  // },
  // {
  //     name: "Registration document WB image",
  //     keyName: "verify_wb_registration_doc"
  // }
];
const states = [
  {
    name: "Karnataka",
    state: "karnataka",
  },
  {
    name: "Telangana",
    state: "telangana",
  },
  {
    name: "West Bengal",
    state: "west_bengal",
  },
  {
    name: "Rajasthan",
    state: "rajasthan",
  },
  {
    name: "UP",
    state: "uttar_pradesh",
  },
];
const category = [
  {
    name: "Business Due Diligence",
    key: "business",
  },
  {
    name: "Personal Due Deligence",
    key: "individual",
  },
];
const categoryList = [
  "Automobiles and vehicles",
  "Beauty and Wellness",
  "BFSI",
  "Business Services",
  "Cable and Broadband",
  "Corporate Alliance",
  "Education",
  "Entertainment",
  "Event Service",
  "Food",
  "Gas and Petrol",
  "Government",
  "Health Care",
  "Home Services",
  "Housing",
  "Individual Services",
  "LFR and Brands",
  "Logistics",
  "Milk Diary and Corporative",
  "NGO",
  "Parking and Tolls",
  "Personal Services",
  "Retail and Shopping",
  "Transport",
  "Travelling",
  "Tution and Classes",
  "Utitlies",
  "Wholesale trade and B2B",
];
const FraudAnalysis = ({
  clientIdDispatch,
  clinetIdLists,
  fraudAnalysisToolDispatch,
  fraudToolfileDispatch,
  fileUpload,
  fraudToolResponseData,
  loading,
  fraudToolResponseMessage,
  fraudToolResponseStatus,
  getFraudToolListDispatch,
  clearFraudToolFormDispatch,
  clearFileUploadDispatch,
}) => {
  const [errors, setErrors] = useState({});
  const [showformg, setShowformg] = useState(false);
  const [fileIndex, setfileIndex] = useState();
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  const [disables, setDisables] = useState(false);
  const [modalHeader, setModalHeader] = useState("");
  const [show, setShow] = useState(false);
  const [docCategory, setDocCategory] = React.useState("");

  const [formData, setFormData] = useState({
    clientId: "",
    api_name: "",
    aadhaar_number: "",
    pan_number: "",
    full_name: "",
    date_of_birth: "",
    driving_licence_number: "",
    passport_number: "",
    voter_id_number: "",
    name_on_card: "",
    account_number: "",
    ifsc_code: "",
    gst_number: "",
    address: "",
    city: "",
    state: "",
    pincode: "",
    cin_number: "",
    country_code: "",
    phone_number: "",
    email_id: "",
    document: false,
    documentLink: "",
    udyog_aadhaar_number: "",
    udyam_aadhaar_number: "",
    ind_shop_license_number: "",
    establishment_code_number: "",
    establishment_name: "",
    fbo_registration_no: "",
    import_export_code: "",
    file_number: "",
    website: "",
    ip_address: "",
    shop_category: "",
    business_name: "",
  });
  const hiddenFileInput = useRef(null);
  useEffect(() => {
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params);
  }, []);
  const handleChanges = (e) => {
    console.log(e.target);
    if (e.target.placeholder === "Aadhar number") {
      let value = e.target.value.replace(/\D/g, "");
      setFormData((manualFormData) => ({
        ...manualFormData,
        [e.target.name]: value,
      }));
    } else {
      setFormData((manualFormData) => ({
        ...manualFormData,
        [e.target.name]: e.target.value,
      }));
    }
    setErrors({ ...errors, [e.target.name]: "" });
  };
  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target;
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, " ");
      setState((values) => ({ ...values, [name]: getData.trim() }));
    } else {
      setState((values) => ({ ...values, [name]: "" }));
    }
  };
  useEffect(() => {
    fileUpload?.fileLink?.data?.path && setDisables(false);
    fileUpload?.fileLink?.data?.path &&
      setFormData({
        ...formData,
        documentLink: fileUpload?.fileLink?.data?.path,
      });
  }, [fileUpload]);
  let clientList =
    clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
      ? clinetIdLists.data.result
      : [];
  const handleSubmit = () => {
    const errors = FraudAnalysisFormValidation(
      formData,
      setErrors,
      formData.api_name
    );
    if (_.isEmpty(errors)) {
      let data = {};
      data.clientId = formData.clientId;
      data.api_name = formData.api_name;
      if (formData.api_name === "aadhar_details") {
        if (formData.aadhaar_number.length > 5) {
          data.value = { aadhaar_number: formData.aadhaar_number };
        } else {
          data.api_name = "aadhar_image";
          data.value = { aadhaar_front_image_url: formData.documentLink };
        }
      } else if (formData.api_name === "pan_details") {
        if (formData.pan_number.length > 5) {
          data.value = {
            pan_number: formData.pan_number,
            full_name: formData.full_name,
            date_of_birth: formData.date_of_birth,
          };
        } else {
          data.api_name = "pan_image";
          data.value = { pan_image_url: formData.documentLink };
        }
      } else if (formData.api_name === "passport_details") {
        if (formData.file_number.length > 4) {
          data.value = {
            file_number: formData.file_number,
          };
        } else {
          data.api_name = "passport_image";
          data.value = { passport_image_url: formData.documentLink };
        }
      } else if (formData.api_name === "voter_details") {
        if (formData.voter_id_number.length > 5) {
          data.value = {
            voter_id_number: formData.voter_id_number,
            name_on_card: formData.full_name,
          };
        } else {
          data.api_name = "voter_image";
          data.value = { voter_id_image_url: formData.documentLink };
        }
      } else if (formData.api_name === "bank_details") {
        data.value = {
          account_number: formData.account_number,
          ifsc_code: formData.ifsc_code,
        };
      } else if (formData.api_name === "cancellation_cheque_image") {
        data.value = { cancellation_cheque_url: formData.documentLink };
      } else if (formData.api_name === "gst_details") {
        if (formData.gst_number.length > 5) {
          data.value = { gst_number: formData.gst_number };
        } else {
          data.api_name = "gst_image";
          data.value = { gst_image_url: formData.documentLink };
        }
      } else if (formData.api_name === "address_details") {
        data.value = {
          address: formData.address,
          state: formData.state,
          city: formData.city,
          pin: formData.pincode,
        };
      } else if (formData.api_name === "cin_details") {
        data.value = { cin_number: formData.cin_number };
      } else if (formData.api_name === "phone_number_details") {
        data.value = {
          country_code: formData.country_code,
          phone_number: formData.phone_number,
        };
      } else if (formData.api_name === "email_id_details") {
        data.value = { email_id: formData.email_id };
      } else if (formData.api_name === "driving_licence_details") {
        if (formData.driving_licence_number.length > 5) {
          data.value = {
            driving_licence_number: formData.driving_licence_number,
            date_of_birth: formData.date_of_birth,
          };
        } else {
          data.api_name = "driving_licence_image";
          data.value = { driving_license_image_url: formData.documentLink };
        }
       
      }else if (formData.api_name === "shop_image_analyzer") {
        data.value = { shop_image_url: formData.documentLink ,business_name:formData.business_name, category:formData.shop_category};
      } else if (formData.api_name === "shop_lens") {
        data.value = { files: formData.documentLink};
      } else if (formData.api_name === "verify_sol_ind_electricity_bill") {
        data.value = { electricity_bill_url: formData.documentLink };
      } else if (formData.api_name === "verify_sol_ind_broadband_bill") {
        data.value = { broadband_bill_url: formData.documentLink };
      } else if (formData.api_name === "verify_sol_ind_gas_bill") {
        data.value = { gas_bill_url: formData.documentLink };
      } else if (formData.api_name === "verify_sol_deed_of_lease") {
        data.value = { deed_of_lease_url: formData.documentLink };
      } else if (formData.api_name === "ind_fssai") {
        if (formData.fbo_registration_no.length > 5) {
          data.value = { fbo_registration_no: formData.fbo_registration_no };
        } else {
          data.api_name = "verify_sol_ind_fssai";
          data.value = { fssai_url: formData.documentLink };
        }
      } else if (formData.api_name === "verify_sol_international_passport") {
        data.value = { international_passport_url: formData.documentLink };
      } else if (formData.api_name === "verify_sol_udyog_aadhaar") {
        if (formData.udyog_aadhaar_number.length > 5) {
          data.value = { udyog_aadhaar_number: formData.udyog_aadhaar_number };
        } else {
          data.api_name = "doc_ocr_ind_udyog_aadhar";
          data.value = { udyog_image_url: formData.documentLink };
        }
      } else if (formData.api_name === "verify_sol_udyam_aadhaar") {
        data.value = { udyam_aadhaar_number: formData.udyam_aadhaar_number };
      } else if (formData.api_name === "verify_sol_ind_shop_license") {
        data.value = {
          ind_shop_license_number: formData.ind_shop_license_number,
          state: formData.state,
        };
      } else if (formData.api_name === "verify_sol_establishment_epfo") {
        data.value = {
          establishment_code_number: formData.establishment_code_number,
          establishment_name: formData.establishment_name,
        };
      } else if (formData.api_name === "ind_roc_iec") {
        data.value = { import_export_code: formData.import_export_code };
      }
      // else if (
      //   formData.api_name === "verify_ocr_up_labour_doc" ||
      //   formData.api_name === "verify_ocr_delhi_labour_doc" ||
      //   formData.api_name === "verify_ocr_telangana_labour_doc" ||
      //   formData.api_name === "verify_ocr_haryana_labour_doc" ||
      //   formData.api_name === "verify_ocr_mp_drug_doc" ||
      //   formData.api_name === "verify_ocr_haryana_food_drug_doc" ||
      //   formData.api_name === "verify_ocr_rajasthan_drug_doc" ||
      //   formData.api_name === "verify_ocr_karnataka_drug_doc" ||
      //   formData.api_name === "verify_ocr_ahmedabad_municipal_doc" ||
      //   formData.api_name === "verify_ocr_greater_chennai_doc" ||
      //   formData.api_name === "verify_ocr_greater_hyd_doc" ||
      //   formData.api_name === "verify_ocr_gujarat_drug_doc" ||
      //   formData.api_name === "verify_ocr_maharashtra_drug_doc" ||
      //   formData.api_name === "verify_ocr_wb_registration_doc" ||
      //   formData.api_name === "verify_up_labour_doc" ||
      //   formData.api_name === "verify_delhi_labour_doc" ||
      //   formData.api_name === "verify_telangana_labour_doc" ||
      //   formData.api_name === "verify_haryana_labour_doc" ||
      //   formData.api_name === "verify_mp_drug_doc" ||
      //   formData.api_name === "verify_haryana_food_drug_doc" ||
      //   formData.api_name === "verify_rajasthan_drug_doc" ||
      //   formData.api_name === "verify_karnataka_drug_doc" ||
      //   formData.api_name === "verify_ahmedabad_municipal_doc" ||
      //   formData.api_name === "verify_delhi_drug_doc" ||
      //   formData.api_name === "verify_greater_hyd_doc" ||
      //   formData.api_name === "verify_gujarat_drug_doc" ||
      //   formData.api_name === "verify_maharashtra_drug_doc" ||
      //   formData.api_name === "verify_wb_registration_doc"
      // ) {
      //   data.value = { image_url: formData.documentLink };
      // }
      fraudAnalysisToolDispatch({ data: { ...data } });
      let apiName = document.find((ele) => ele.keyName === formData.api_name);
      setModalHeader(apiName.name);
      setShow(true);
      setFormData({
        ...formData,
        aadhaar_number: "",
        pan_number: "",
        full_name: "",
        date_of_birth: "",
        driving_licence_number: "",
        passport_number: "",
        voter_id_number: "",
        name_on_card: "",
        account_number: "",
        ifsc_code: "",
        gst_number: "",
        address: "",
        city: "",
        state: "",
        pincode: "",
        cin_number: "",
        country_code: "",
        phone_number: "",
        email_id: "",
        document: false,
        udyog_aadhaar_number: "",
        udyam_aadhaar_number: "",
        ind_shop_license_number: "",
        establishment_code_number: "",
        establishment_name: "",
        fbo_registration_no: "",
        import_export_code: "",
        file_number: "",
        category: "",
        website: "",
        ip_address: "",
        shop_category: "",
        business_name: "",
      });
    }
    clearFileUploadDispatch();
  };
  const FileChangeHandler = (e) => {
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    setDisables(true);
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData();
        data.append("type", "merchantDeclarationFrom");
        data.append("file_to_upload", files);
        fraudToolfileDispatch(data);
        setFormData({ ...formData, document: true });
      } else {
        // setDisables(false);
        setErrors({
          ...errors,
          [name]: `File size must below ${
            fileSize / 1048576
          } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID });
      setShowformg(false);
    }
  };
  const handleClick = (event, i) => {
    hiddenFileInput.current.click(event);
    setfileIndex(i);
  };
  const showForm = (apiName) => {
    if (apiName === "aadhar_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Aadhar number:
              </label>
            </div>
            <div className="col-md-6 ">
              <input
                placeholder="Aadhar number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.aadhaar_number && errors.aadhaar_number,
                  },
                  {
                    "is-valid":
                      formData.aadhaar_number && !errors.aadhaar_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                name="aadhaar_number"
                autoComplete="off"
                value={formData.aadhaar_number || ""}
              />
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.aadhaar_number}</span>
                </div>
              }
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>

          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Aadhar Image:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              }
            </div>
          </div>
        </>
      );
    } else if (apiName === "pan_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Pan Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="PAN Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.pan_number && errors.pan_number,
                  },
                  {
                    "is-valid": formData.pan_number && !errors.pan_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="pan_number"
                autoComplete="off"
                value={formData.pan_number || ""}
              />
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.pan_number}</span>
                </div>
              }
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                PAN Image:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {errors.document && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "driving_licence_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Driving License Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Driving License Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.driving_licence_number &&
                      errors.driving_licence_number,
                  },
                  {
                    "is-valid":
                      formData.driving_licence_number &&
                      !errors.driving_licence_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="driving_licence_number"
                autoComplete="off"
                value={formData.driving_licence_number || ""}
              />
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.driving_licence_number}
                  </span>
                </div>
              }
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required ">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Date of birth:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="DD-MM-YYYY"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.date_of_birth && errors.date_of_birth,
                  },
                  {
                    "is-valid": formData.date_of_birth && !errors.date_of_birth,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="date_of_birth"
                autoComplete="off"
                value={formData.date_of_birth || ""}
              />
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.date_of_birth}</span>
                </div>
              }
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Driving Licence Image:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {errors.document && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "passport_details") {
      return (
        <>
          {/* <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Passport Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Passport Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.passport_number && errors.passport_number,
                  },
                  {
                    "is-valid":
                      formData.passport_number && !errors.passport_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="passport_number"
                autoComplete="off"
                value={formData.passport_number || ""}
              />
              {errors.passport_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.passport_number}</span>
                </div>
              )}
            </div>
          </div> */}
             <div className="row mb-8">
             <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                File number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="File number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.file_number && errors.file_number,
                  },
                  {
                    "is-valid": formData.file_number && !errors.file_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="file_number"
                autoComplete="off"
                value={formData.file_number || ""}
              />
              {errors.file_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.file_number}</span>
                </div>
              )}
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Passport Image:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {errors.document && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "website") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Website:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Website"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.website && errors.website,
                  },
                  {
                    "is-valid": formData.website && !errors.website,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="website"
                autoComplete="off"
                value={formData.website || ""}
              />
              {errors.website && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.website}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "voter_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Voter ID Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Voter ID number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.voter_id_number && errors.voter_id_number,
                  },
                  {
                    "is-valid":
                      formData.voter_id_number && !errors.voter_id_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="voter_id_number"
                autoComplete="off"
                value={formData.voter_id_number || ""}
              />
              {errors.voter_id_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.voter_id_number}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Full name:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Full name"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.full_name && errors.full_name },
                  {
                    "is-valid": formData.full_name && !errors.full_name,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="full_name"
                autoComplete="off"
                value={formData.full_name || ""}
              />
              {errors.full_name && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.full_name}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Voter ID Image:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {errors.document && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "bank_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Account Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Account Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.account_number && errors.account_number,
                  },
                  {
                    "is-valid":
                      formData.account_number && !errors.account_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="account_number"
                autoComplete="off"
                value={formData.account_number || ""}
              />
              {errors.account_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.account_number}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                IFSC Code:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="IFSC Code"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.ifsc_code && errors.ifsc_code },
                  {
                    "is-valid": formData.ifsc_code && !errors.ifsc_code,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="ifsc_code"
                autoComplete="off"
                value={formData.ifsc_code || ""}
              />
              {errors.ifsc_code && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.ifsc_code}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "cancellation_cheque_image") {
      return (
        <div className="row mb-8">
          <div className="col-md-3">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Cancelation cheque:
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {errors.document && (
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            )}
          </div>
        </div>
      );
    } else if (apiName === "gst_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                GST Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="GST Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.gst_number && errors.gst_number,
                  },
                  {
                    "is-valid": formData.gst_number && !errors.gst_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="gst_number"
                autoComplete="off"
                value={formData.gst_number || ""}
              />
              {errors.gst_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.gst_number}</span>
                </div>
              )}
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                GST document:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {errors.document && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "address_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Address:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Address"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.address && errors.address },
                  {
                    "is-valid": formData.address && !errors.address,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="address"
                autoComplete="off"
                value={formData.address || ""}
              />
              {errors.address && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.address}</span>
                </div>
              )}
            </div>
          </div>

          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                City:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="City"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.city && errors.city },
                  {
                    "is-valid": formData.city && !errors.city,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="city"
                autoComplete="off"
                value={formData.city || ""}
              />
              {errors.city && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.city}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                State:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="State"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.state && errors.state },
                  {
                    "is-valid": formData.state && !errors.state,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="state"
                autoComplete="off"
                value={formData.state || ""}
              />
              {errors.state && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.state}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Pincode:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Pin code"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.pincode && errors.pincode },
                  {
                    "is-valid": formData.pincode && !errors.pincode,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="pincode"
                autoComplete="off"
                value={formData.pincode || ""}
              />
              {errors.pincode && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.pincode}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "cin_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Company Identification Number(CIN):
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="CIN"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.cin_number && errors.cin_number,
                  },
                  {
                    "is-valid": formData.cin_number && !errors.cin_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="cin_number"
                autoComplete="off"
                value={formData.cin_number || ""}
              />
              {errors.cin_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.cin_number}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "phone_number_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Country code:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Country code"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.country_code && errors.country_code,
                  },
                  {
                    "is-valid": formData.country_code && !errors.country_code,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="country_code"
                autoComplete="off"
                value={formData.country_code || ""}
              />
              {errors.country_code && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.country_code}</span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Phone Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Phone number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid": formData.phone_number && errors.phone_number,
                  },
                  {
                    "is-valid": formData.phone_number && !errors.phone_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="phone_number"
                autoComplete="off"
                value={formData.phone_number || ""}
              />
              {errors.phone_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.phone_number}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "email_id_details") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Email Id:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Email"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  { "is-invalid": formData.email_id && errors.email_id },
                  {
                    "is-valid": formData.email_id && !errors.email_id,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="email_id"
                autoComplete="off"
                value={formData.email_id || ""}
              />
              {errors.email_id && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.email_id}</span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "shop_image_analyzer") {
      return (
        <>
        <div className="row mb-8">
            <div className="col-md-3 ">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Business name:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Business name"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.business_name &&
                      errors.business_name,
                  },
                  {
                    "is-valid":
                      formData.business_name &&
                      !errors.business_name,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="business_name"
                autoComplete="off"
                value={formData.business_name || ""}
              />
              {errors.udyog_aadhaar_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.udyog_aadhaar_number}
                  </span>
                </div>
              )}
            </div>
          </div>
        <div className="row mb-8">
              <div className="col-md-3">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Category:
                </label>
              </div>
              <div className="col-md-6">
                <select
                  name="shop_category"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={formData.shop_category || ""}
                >
                  <option value="">Select...</option>
                  {categoryList.map((category) => (
                    <option value={category} key={category}>
                      {category}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="row mb-8">
          <div className="col-md-3 required">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Shop Image:
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            }
          </div>
        </div>
              </>
      );
    } else if (apiName === "shop_lens") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Shop Image:
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                      ? "Uploaded"
                      : "Upload"}
                </a>
              </div>
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              }
            </div>
          </div>
        </>
      );
    } else if (apiName === "verify_sol_ind_electricity_bill") {
      return (
        <div className="row mb-8">
          <div className="col-md-3">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Electricity Bill Image:
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            }
          </div>
        </div>
      );
    } else if (apiName === "verify_sol_ind_broadband_bill") {
      return (
        <div className="row mb-8">
          <div className="col-md-3">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Indian broadband bill Image:
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            }
          </div>
        </div>
      );
    } else if (apiName === "verify_sol_ind_gas_bill") {
      return (
        <div className="row mb-8">
          <div className="col-md-3">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              India gas bill image
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            }
          </div>
        </div>
      );
    } else if (apiName === "verify_sol_deed_of_lease") {
      return (
        <div className="row mb-8">
          <div className="col-md-3">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Deed Of Lease document image
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            }
          </div>
        </div>
      );
    } else if (apiName === "verify_sol_international_passport") {
      return (
        <div className="row mb-8">
          <div className="col-md-3">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              International Passport Image
            </label>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
            <div className="d-flex justify-content-start">
              <input
                type="file"
                className="d-none"
                name="document"
                id="document"
                accept="image/*"
                multiple={false}
                ref={hiddenFileInput}
                onChange={(e) => {
                  FileChangeHandler(e);
                }}
              />
              <a
                className={
                  showformg
                    ? "btn btn-success btn-sm"
                    : "btn btn-sm btn-light-primary"
                }
                onClick={() => {
                  handleClick();
                }}
              >
                {fileUpload.loading
                  ? "Uploading..."
                  : fileUpload?.fileLink?.message === "Created successfully"
                  ? "Uploaded"
                  : "Upload"}
              </a>
            </div>
            {
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">{errors.document}</span>
              </div>
            }
          </div>
        </div>
      );
    } else if (apiName === "verify_sol_udyog_aadhaar") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Udyog Aadhaar Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.udyog_aadhaar_number &&
                      errors.udyog_aadhaar_number,
                  },
                  {
                    "is-valid":
                      formData.udyog_aadhaar_number &&
                      !errors.udyog_aadhaar_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="udyog_aadhaar_number"
                autoComplete="off"
                value={formData.udyog_aadhaar_number || ""}
              />
              {errors.udyog_aadhaar_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.udyog_aadhaar_number}
                  </span>
                </div>
              )}
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Image
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              }
            </div>
          </div>
        </>
      );
    } else if (apiName === "verify_sol_udyam_aadhaar") {
      return (
        <div className="row mb-8">
          <div className="col-md-3 required">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Udyam Aadhaar Number:
            </label>
          </div>
          <div className="col-md-6">
            <input
              placeholder="Udyam Aadhaar Number"
              className={clsx(
                "form-control form-control-lg form-control-solid",
                {
                  "is-invalid":
                    formData.udyam_aadhaar_number &&
                    errors.udyam_aadhaar_number,
                },
                {
                  "is-valid":
                    formData.udyam_aadhaar_number &&
                    !errors.udyam_aadhaar_number,
                }
              )}
              onChange={(e) => handleChanges(e)}
              onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
              type="text"
              name="udyam_aadhaar_number"
              autoComplete="off"
              value={formData.udyam_aadhaar_number || ""}
            />
            {errors.udyam_aadhaar_number && (
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">
                  {errors.udyam_aadhaar_number}
                </span>
              </div>
            )}
          </div>
        </div>
      );
    } else if (apiName === "verify_sol_ind_shop_license") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Indian shop license Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Indian shop license Number"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.ind_shop_license_number &&
                      errors.ind_shop_license_number,
                  },
                  {
                    "is-valid":
                      formData.ind_shop_license_number &&
                      !errors.ind_shop_license_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="ind_shop_license_number"
                autoComplete="off"
                value={formData.ind_shop_license_number || ""}
              />
              {errors.ind_shop_license_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.ind_shop_license_number}
                  </span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                State:
              </label>
            </div>
            <div className="col-md-6">
              <select
                name="state"
                className="form-select form-select-solid"
                data-control="select"
                data-placeholder="Select an option"
                data-allow-clear="true"
                onChange={(e) => handleChanges(e)}
                value={formData.state || ""}
              >
                <option value="">Select...</option>
                {states.map((state) => (
                  <option value={state.state} key={state.state}>
                    {state.name}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </>
      );
    } else if (apiName === "verify_sol_establishment_epfo") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                EPFO establishment name:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="EPFO establishment name"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.establishment_name && errors.establishment_name,
                  },
                  {
                    "is-valid":
                      formData.establishment_name && !errors.establishment_name,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="establishment_name"
                autoComplete="off"
                value={formData.establishment_name || ""}
              />
              {errors.establishment_name && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.establishment_name}
                  </span>
                </div>
              )}
            </div>
          </div>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                EPFO establishment code:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="EPFO establishment code"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.establishment_code_number &&
                      errors.establishment_code_number,
                  },
                  {
                    "is-valid":
                      formData.establishment_code_number &&
                      !errors.establishment_code_number,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="establishment_code_number"
                autoComplete="off"
                value={formData.establishment_code_number || ""}
              />
              {errors.establishment_code_number && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.establishment_code_number}
                  </span>
                </div>
              )}
            </div>
          </div>
        </>
      );
    } else if (apiName === "ind_fssai") {
      return (
        <>
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                FSSAI Number:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="FSSAI Details"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.fbo_registration_no &&
                      errors.fbo_registration_no,
                  },
                  {
                    "is-valid":
                      formData.fbo_registration_no &&
                      !errors.fbo_registration_no,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="fbo_registration_no"
                autoComplete="off"
                value={formData.fbo_registration_no || ""}
              />
              {errors.fbo_registration_no && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.fbo_registration_no}
                  </span>
                </div>
              )}
            </div>
          </div>
          <p className="text-center">-------- OR --------</p>
          <div className="row mb-8">
            <div className="col-md-3">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                FSSAI Image
              </label>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
              <div className="d-flex justify-content-start">
                <input
                  type="file"
                  className="d-none"
                  name="document"
                  id="document"
                  accept="image/*"
                  multiple={false}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    FileChangeHandler(e);
                  }}
                />
                <a
                  className={
                    showformg
                      ? "btn btn-success btn-sm"
                      : "btn btn-sm btn-light-primary"
                  }
                  onClick={() => {
                    handleClick();
                  }}
                >
                  {fileUpload.loading
                    ? "Uploading..."
                    : fileUpload?.fileLink?.message === "Created successfully"
                    ? "Uploaded"
                    : "Upload"}
                </a>
              </div>
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.document}</span>
                </div>
              }
            </div>
          </div>
        </>
      );
    } else if (apiName === "ind_roc_iec") {
      return (
        <div className="row mb-8">
          <div className="col-md-3 required">
            <label className="font-size-xs font-weight-bold mb-3  form-label">
              Import Export Code:
            </label>
          </div>
          <div className="col-md-6">
            <input
              placeholder="Import Export Code"
              className={clsx(
                "form-control form-control-lg form-control-solid",
                {
                  "is-invalid":
                    formData.import_export_code && errors.import_export_code,
                },
                {
                  "is-valid":
                    formData.import_export_code && !errors.import_export_code,
                }
              )}
              onChange={(e) => handleChanges(e)}
              onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
              type="text"
              name="import_export_code"
              autoComplete="off"
              value={formData.import_export_code || ""}
            />
            {errors.import_export_code && (
              <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                <span role="alert text-danger">
                  {errors.import_export_code}
                </span>
              </div>
            )}
          </div>
        </div>
      );
    }
    //else if (apiName === "verify_ocr_up_labour_doc") {
    //   return(<div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Labour doucument UP
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>)
    // } else if (apiName === "verify_ocr_delhi_labour_doc") {
    //   return(<div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //      Labour document Delhi
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>)
    // } else if (apiName === "verify_ocr_telangana_labour_doc") {
    //  return(
    //   <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Labour document Telangana
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //  )
    // } else if (apiName === "verify_ocr_haryana_labour_doc") {
    //   return (
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Labour document Haryana
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_ocr_mp_drug_doc") {
    //   return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document MP
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //   )
    // } else if (apiName === "verify_ocr_haryana_food_drug_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Drug and Food document Haryana
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_ocr_rajasthan_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Rajasthan
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ocr_karnataka_drug_doc") {
    //     return(
    //       <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Karnataka
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //     )
    // } else if (apiName === "verify_ocr_ahmedabad_municipal_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Municipal document Ahmedabad
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ocr_delhi_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Delhi
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ocr_greater_chennai_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Chennai document Greater
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ocr_greater_hyd_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Hyd document Greater
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_ocr_gujarat_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Drug document Gujarat
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //    )
    // } else if (apiName === "verify_ocr_maharashtra_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Maharashtra
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ocr_wb_registration_doc") {
    //    return (
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Registration document WB
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //    )
    // } else if (apiName === "verify_up_labour_doc") {
    //    return (
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Labour document UP image
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //    )
    // } else if (apiName === "verify_delhi_labour_doc") {
    //    return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Labour document Delhi image
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //    )
    // } else if (apiName === "verify_telangana_labour_doc") {
    //    return (
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Labour document Telangana image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_haryana_labour_doc") {
    //    return(<div className="row mb-8">
    //    <div className="col-md-3">
    //      <label className="font-size-xs font-weight-bold mb-3  form-label">
    //      Labour document Haryana image
    //      </label>
    //    </div>
    //    <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //      <div className="d-flex justify-content-start">
    //        <input
    //          type="file"
    //          className="d-none"
    //          name="document"
    //          id="document"
    //          accept="image/*"
    //          multiple={false}
    //          ref={hiddenFileInput}
    //          onChange={(e) => {
    //            FileChangeHandler(e);
    //          }}
    //        />
    //        <a
    //          className={
    //            showformg
    //              ? "btn btn-success btn-sm"
    //              : "btn btn-sm btn-light-primary"
    //          }
    //          onClick={() => {
    //            handleClick();
    //          }}
    //        >
    //          {fileUpload.loading
    //            ? "Uploading..."
    //            : fileUpload?.fileLink?.message === "Created successfully"
    //            ? "Uploaded"
    //            : "Upload"}
    //        </a>
    //      </div>
    //      {
    //        <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //          <span role="alert text-danger">{errors.document}</span>
    //        </div>
    //      }
    //    </div>
    //  </div>)
    // } else if (apiName === "verify_mp_drug_doc") {
    //   return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document MP image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //   )
    // } else if (apiName === "verify_haryana_food_drug_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Drug and Food document Haryana image
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_rajasthan_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Rajasthan image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_karnataka_drug_doc") {
    //    return (
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Karnataka image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ahmedabad_municipal_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Municipal document Ahmedabad image
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_delhi_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Delhi image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_greater_chennai_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Chennai document Chennai image
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_greater_hyd_doc") {
    //   return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Hyd document Hyd
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //   )
    // } else if (apiName === "verify_gujarat_drug_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Drug document Gujarat
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    // } else if (apiName === "verify_maharashtra_drug_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Drug document Maharashtra image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_wb_registration_doc") {
    //    return(
    //     <div className="row mb-8">
    //   <div className="col-md-3">
    //     <label className="font-size-xs font-weight-bold mb-3  form-label">
    //     Registration document WB image
    //     </label>
    //   </div>
    //   <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //     <div className="d-flex justify-content-start">
    //       <input
    //         type="file"
    //         className="d-none"
    //         name="document"
    //         id="document"
    //         accept="image/*"
    //         multiple={false}
    //         ref={hiddenFileInput}
    //         onChange={(e) => {
    //           FileChangeHandler(e);
    //         }}
    //       />
    //       <a
    //         className={
    //           showformg
    //             ? "btn btn-success btn-sm"
    //             : "btn btn-sm btn-light-primary"
    //         }
    //         onClick={() => {
    //           handleClick();
    //         }}
    //       >
    //         {fileUpload.loading
    //           ? "Uploading..."
    //           : fileUpload?.fileLink?.message === "Created successfully"
    //           ? "Uploaded"
    //           : "Upload"}
    //       </a>
    //     </div>
    //     {
    //       <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //         <span role="alert text-danger">{errors.document}</span>
    //       </div>
    //     }
    //   </div>
    // </div>
    //    )
    // } else if (apiName === "verify_ocr_udyam_reg_doc") {
    //   return(
    //     <div className="row mb-8">
    //     <div className="col-md-3">
    //       <label className="font-size-xs font-weight-bold mb-3  form-label">
    //       Udoyg Aadhar Image
    //       </label>
    //     </div>
    //     <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
    //       <div className="d-flex justify-content-start">
    //         <input
    //           type="file"
    //           className="d-none"
    //           name="document"
    //           id="document"
    //           accept="image/*"
    //           multiple={false}
    //           ref={hiddenFileInput}
    //           onChange={(e) => {
    //             FileChangeHandler(e);
    //           }}
    //         />
    //         <a
    //           className={
    //             showformg
    //               ? "btn btn-success btn-sm"
    //               : "btn btn-sm btn-light-primary"
    //           }
    //           onClick={() => {
    //             handleClick();
    //           }}
    //         >
    //           {fileUpload.loading
    //             ? "Uploading..."
    //             : fileUpload?.fileLink?.message === "Created successfully"
    //             ? "Uploaded"
    //             : "Upload"}
    //         </a>
    //       </div>
    //       {
    //         <div className="fv-plugins-message-container text-danger mt-4 mb-4">
    //           <span role="alert text-danger">{errors.document}</span>
    //         </div>
    //       }
    //     </div>
    //   </div>
    //   )
    //}
  };
  useEffect(() => {
    if (fraudToolResponseStatus === "ok") {
      successAlert(fraudToolResponseMessage, "success");
    }
    const params = {
      limit: 20,
      page: 1,
    };
    getFraudToolListDispatch(params);
    clearFraudToolFormDispatch();
  }, [fraudToolResponseStatus]);
  return (
    <>
      <div className={`card`}>
        <div className="card-body py-3 m-8">
          {Role === "Admin" && (
            <div className="row mb-8">
              <div className="col-md-3 required">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Client:
                </label>
              </div>
              <div className="col-md-6">
                <select
                  name="clientId"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={formData.clientId || ""}
                >
                  <option value="">Select...</option>
                  {clientList?.map((ele) => (
                    <option value={ele._id} key={ele._id}>
                      {ele.company}
                    </option>
                  ))}
                </select>
                {
                  <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                    <span role="alert text-danger">{errors.clientId}</span>
                  </div>
                }
              </div>
            </div>
          )}
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Category:
              </label>
            </div>
            <div className="col-md-6">
              <select
                name="operationStatus"
                className="form-select form-select-solid"
                data-control="select"
                data-placeholder="Select an option"
                data-allow-clear="true"
                onChange={(e) => setDocCategory(e.target.value)}
                value={docCategory || ""}
              >
                <option value="">Select...</option>
                {category.map((item) => (
                  <option value={item.key} key={item.key}>
                    {item.name}
                  </option>
                ))}
              </select>
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.api_name}</span>
                </div>
              }
            </div>
          </div>

          {docCategory && (
            <div className="row mb-8">
              <div className="col-md-3 required">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Document:
                </label>
              </div>
              <div className="col-md-6">
                <select
                  name="api_name"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={formData.api_name || ""}
                >
                  <option value="">Select...</option>
                  {document
                    .filter((item) => item.category === docCategory)
                    .map((item, index) => (
                      <option value={item.keyName} key={index}>
                        {item.name}
                      </option>
                    ))}
                </select>
                {
                  <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                    <span role="alert text-danger">{errors.api_name}</span>
                  </div>
                }
              </div>
            </div>
          )}
          {showForm(formData.api_name)}
          <div className="row mb-8">
            <div className="col-md-5"></div>
            <div className="col-md-2">
              <button
                className="btn btn-primary m-1 mt-8 font-5vw "
                onClick={handleSubmit}
                disabled={disables}
              >
                Submit
              </button>
            </div>
          </div>
          <div className="col-md-4"></div>
          {/* </div> */}
        </div>
      </div>
      <Verification
        show={show}
        setShow={setShow}
        fraudToolResponseData={fraudToolResponseData}
        loading={loading}
        formData={formData}
        modalHeader={modalHeader}
        setFormData={setFormData}
      />
    </>
  );
};
const mapStateToProps = (state) => {
  const {
    FraudAnalysisDocumetUploadStore,
    clinetListStore,
    fruadAnalysisStore,
  } = state;
  return {
    WrmOperationActionStatus:
      state &&
      state.wrmOperationManagementActionStore &&
      state.wrmOperationManagementActionStore,
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
    fileUpload:
      FraudAnalysisDocumetUploadStore && FraudAnalysisDocumetUploadStore,
    fraudToolResponseMessage:
      fruadAnalysisStore && fruadAnalysisStore.fraudAnalysisMessage,
    loading: fruadAnalysisStore && fruadAnalysisStore.loadingFAT,
    fraudToolResponseStatus:
      fruadAnalysisStore && fruadAnalysisStore.fraudAnalysisStatus,
  };
};

const mapDispatchToProps = (dispatch) => ({
  clientIdDispatch: (data) =>
    dispatch(clientIdLIstActions.getclientIdList(data)),
  fraudAnalysisToolDispatch: (data) =>
    dispatch(FraudAnalysisActions.request(data)),
  fraudToolfileDispatch: (params) =>
    dispatch(FraudToolfileUploadActions.request(params)),
  getFraudToolListDispatch: (params) =>
    dispatch(FraudToolListActions.request(params)),
  clearFraudToolFormDispatch: () => dispatch(FraudAnalysisActions.clear()),
  clearFileUploadDispatch: () => dispatch(FraudToolfileUploadActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FraudAnalysis);
