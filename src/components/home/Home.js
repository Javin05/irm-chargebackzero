import React, { useEffect, useState } from 'react'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import _ from 'lodash'
import { connect } from 'react-redux'
import IRMList from './irmList'
import clsx from 'clsx'
import { queuesActions } from '../../store/actions'
import routeConfig from '../../routing/routeConfig'
import { useHistory } from 'react-router-dom'

function Home(props) {
  const {
    getQueueslistDispatch,
    queuesLists,
  } = props
  const history = useHistory()

  useEffect(() => {
    getQueueslistDispatch()
  }, [])

  const [showTable, setShowTable] = useState(false)
  const [value, setValue] = useState('')
  const [searchValue, setSearchValue] = useState('')
  const [routing, setRouting] = useState('')
  const [searchNode, setSearchNode] = useState('')
  const [QueueOption, setQueue] = useState()
  const [SelectedQueueOption, setSelectedQueueOption] = useState('')
  const OngoingMonitoring = [
    { value: 'ogm_Id', label: 'Case Id' },
    { value: 'riskId', label: 'Case Id' },
    { value: 'batchId', label: 'Batch ID' },
    { value: 'ogm_interval', label: 'Ogm Interval' },
    { value: 'website', label: 'Website' },
    { value: 'tag', label: 'Tag' },
    { value: 'phone', label: 'Phone' },
    { value: 'personalEmail', label: 'Email' },
  ]

  useEffect(() => {
    switch (value) {
      case "SUSPECT_ONGOING_MONITORING_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "tag" && item.value !== 'phone'&& item.value !== 'personalEmail' && item.value !== 'riskId'))
        setShowTable(true)
        setRouting(routeConfig.monitar)
        break;
      case "ONGOING_MONITORING_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "tag" && item.value !== 'phone'&& item.value !== 'personalEmail' && item.value !== 'riskId'))
        setShowTable(true)
        setRouting(routeConfig.monitar)
        break;
      case 'SUSPECT_WEB_RISK_MONITORING_Q':
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "batchId" && item.value !== 'ogm_interval' && item.value !== 'phone' && item.value !== 'personalEmail' && item.value !== 'ogm_Id'))
        setShowTable(true)
        setRouting(routeConfig.wrmRiskmanagement)
        break;
      case "WEB_RISK_MONITORING_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "batchId" && item.value !== 'ogm_interval' && item.value !== 'phone' && item.value !== 'personalEmail' && item.value !== 'ogm_Id'))
        setShowTable(true)
        setRouting(routeConfig.wrmRiskmanagement)
        break;
      case "AML_Q":
        // history.push(routeConfig.amlQueue, params)
        setRouting(routeConfig.amlQueue)
        break;
      case "SUSPECT_TXN_Q":
        // history.push(routeConfig.transaction, params)
        setRouting(routeConfig.transaction)
        break;
      case "MERCHANT_ONBOARDING_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "batchId" && item.value !== 'tag' && item.value !== 'ogm_interval' && item.value !== 'ogm_Id'))
        setShowTable(true)
        setRouting(routeConfig.accountsRiskmanagement)
        break;
      case "SUSPECT_MERCHANT_ONBOARDING_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "batchId" && item.value !== 'tag' && item.value !== 'ogm_interval' && item.value !== 'ogm_Id'))
        setShowTable(true)
        setRouting(routeConfig.accountsRiskmanagement)
        break;
      case "TRANSACTION_LAUNDERING_Q":
        // history.push('/upcoming', params)
        setRouting('/upcoming')
        break;
      case "KYC_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "batchId" && item.value !== 'tag' && item.value !== 'ogm_interval' && item.value !== 'website' && item.value !== 'ogm_Id'))
        setShowTable(true)
        setRouting(routeConfig.KYC)
        break;
      case "SUSPECT_KYC_Q":
        setSearchValue(OngoingMonitoring.filter((item) => item.value !== "batchId" && item.value !== 'tag' && item.value !== 'ogm_interval' && item.value !== 'website' && item.value !== 'ogm_Id'))
        setShowTable(true)
        setRouting(routeConfig.KYC)
        break;
    }
  },[value])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChange = (e) => {
    setSearchNode({...searchNode, [e.target.name]: e.target.value})
  }

  const handleChangeQueue = selectedOption => {
    if (selectedOption !== null) {
      setSelectedQueueOption(selectedOption)
      setValue(selectedOption.label)
    }
  }

  const queueNames = queuesLists && queuesLists.data
  useEffect(() => {
    const Queue = getDefaultOption(queueNames)
    setQueue(Queue)
  }, [queueNames])

  const getDefaultOption = (queueNames) => {
    const defaultOptions = []
    for (const item in queueNames) {
      defaultOptions.push({ label: queueNames[item].queueName, value: queueNames[item]._id })
    }
    return defaultOptions
  }
  const handleReset = () => {
    setShowTable(false)
    setSelectedQueueOption('')
    setSearchNode('')
    setSearchValue('')
    setRouting('')
  }

  const handleQueueClick = () => {
    function objectToQueryString(obj) {
      const keys = Object.keys(obj);
      const keyValuePairs = keys.map(key => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
      });
      return keyValuePairs.join('&');
    }
    history.push(`${routing}?${objectToQueryString(searchNode)}`, searchNode)
  }

  return (
    <div className='bg-pad pe-0'>
      <div className='card-header bg-black '>
        <div className='card-body'>
          <div className='container-fixed '>
            <div className='card-body'>
              <div className='row home-mb'>
                <div className='d-flex justify-content-center mt-15'>
                  <label className='font-size-xs font-weight-bold mb-3 mt-3 text-center text-white form-label mt-13'>
                    Select Queues : &nbsp;
                  </label>
                  <div className='col-lg-5 pr-3 me-2 mb-10 mt-10'>
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name='AppUserId'
                      className='select2'
                      classNamePrefix='select'
                      handleChangeReactSelect={handleChangeQueue}
                      options={QueueOption}
                      value={SelectedQueueOption}
                      isDisabled={!QueueOption}
                    />
                  </div>
                </div>
                <div className='col-sm-12 px-10'>
                  <div className='row g-4 mb-4 justify-content-center align-items-center'>
                    {
                      showTable ? (
                        searchValue.map((item, index) => (
                          <>
                            <div className='col-sm-3' key={index}>
                              <label className='font-size-xs font-weight-bold mb-3 mt-3 text-center text-white form-label'>
                                {item.label} &nbsp;
                              </label>
                              <div className='col-lg-2 pr-3 me-2 w-100'>
                                <input
                                  placeholder={item.label}
                                  className={clsx(
                                    'form-control form-control-md form-control-solid'
                                  )}
                                  onChange={(e) => handleChange(e)}
                                  type='text'
                                  name={item.value}
                                  autoComplete='off'
                                />
                              </div>
                            </div>
                            
                          </>
                        ))) : null}

                        {showTable ?
                          <div className='col-sm-12 text-center'>
                            <button
                              type='button'
                              className='btn btn-sm btn-primary fa-pull-center mt-1 me-5'
                              data-dismiss='modal'
                              onClick={() => {
                                handleReset()
                              }}
                            >
                              Reset
                            </button>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary fa-pull-center mt-1'
                              data-dismiss='modal'
                              onClick={() => {
                                handleQueueClick()
                              }}
                            >
                              Search
                            </button>
                          </div> : null
                        }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='mb-0 home-body'>
        <IRMList value={value} />
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  const { queueslistStore } = (state)
  return {
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)