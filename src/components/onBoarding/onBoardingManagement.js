import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import {
  onBoardingActions,
} from '../../store/actions'
import { connect } from 'react-redux'
import SearchList from './searchList'
import ReactPaginate from 'react-paginate'
import { unsetLocalStorage, getLocalStorage, removeLocalStorage } from '../../utils/helper';
import { RISKSTATUS, REVIEWTYPE, SET_FILTER, CREATE_PERMISSION } from '../../utils/constants'
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import { STATUS_RESPONSE } from '../../utils/constants'
import FindRole from './Role'
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'
import KYCminiAdd from "./mini-OnBoarding/Add"

function OnBoardingManagementList(props) {
  const {
    getOnBoardinglistDispatch,
    className,
    onBoardinglists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction
  } = props

  const didMount = React.useRef(false);
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1)
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [show, setShow] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [activeKyc, setActiveKyc] = useState({})
  const pathName = useLocation().pathname
  const getUsersPermissions = getUserPermissions(pathName, true)
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false
  })

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params);
    getOnBoardinglistDispatch(pickByParams)
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      const params = {
        limit: limit,
        page: activePageNumber,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getOnBoardinglistDispatch(pickByParams)
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      ...searchParams,
      tag: paginationSearch.tag ? paginationSearch.tag : '',
      riskStatus: paginationSearch.riskStatus ? paginationSearch.riskStatus : '',
      reportStatus: paginationSearch.reportStatus ? paginationSearch.reportStatus : ''
    }
    setActivePageNumber(pageNumber)
    getOnBoardinglistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getOnBoardinglistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getOnBoardinglistDispatch(params)
    }
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  })

  const totalPages =
    onBoardinglists && onBoardinglists.data && onBoardinglists.data.count
      ? Math.ceil(parseInt(onBoardinglists && onBoardinglists.data && onBoardinglists.data.count) / limit)
      : 1

  return (

    <>
      <KYCminiAdd setShow={setShow} show={show} editMode={editMode} activeKyc={activeKyc} setActiveKyc={setActiveKyc} />
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="csvReport"
          className="download-table-xls-button"
          table="table-to-xlss"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-3'>
              <div className='col-md-6 mt-1 ms-2'>
                {onBoardinglists && onBoardinglists.data && onBoardinglists.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {onBoardinglists && onBoardinglists.data && onBoardinglists.data.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='d-flex col-md-9 col-lg-9 justify-content-end mt-4'>
              <div className='my-auto'>
                <SearchList />
              </div>
              <div className="my-auto me-3">
                <button
                  type="button"
                  className="btn btn-sm btn-light-primary font-5vw pull-right"
                  data-toggle="modal"
                  data-target="#addModal"
                  onClick={() => {
                    setShow(true)
                    setEditMode(false)
                  }}
                >
                  <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                  Add Merchant
                </button>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th className="">
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="">
                    <div className="d-flex">
                      <span>Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("phone")}
                        >
                          <i
                            className={`bi ${sorting.phone
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Personal Email</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("personalEmail")}
                        >
                          <i
                            className={`bi ${sorting.personalEmail
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Business Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("businessName")}
                        >
                          <i
                            className={`bi ${sorting.businessName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {/* <th className="min-w-100px text-center">
                  <div className="d-flex">
                    <span>Business Phone</span>
                    <div className="min-w-25px text-end">
                      <div
                        className="cursor-pointer"
                        onClick={() => handleSorting("businessPhone")}
                      >
                        <i
                          className={`bi ${sorting.businessPhone
                            ? "bi-arrow-up-circle-fill"
                            : "bi-arrow-down-circle"
                            } text-primary`}
                        />
                      </div>
                    </div>
                  </div>
                </th> */}
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Website</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("website")}
                        >
                          <i
                            className={`bi ${sorting.website
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Accounts Queue</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>KYC Queue</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>WRM Queue</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>OGM Queue</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      onBoardinglists &&
                        onBoardinglists.data
                        ? (
                          onBoardinglists.data && onBoardinglists.data.map((onBoardinglist, i) => {
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <>
                                    <Link to={`/onboarding/update/${onBoardinglist._id}`} className="ellipsis">
                                      IRM{onBoardinglist && onBoardinglist.onboardingId}
                                    </Link>
                                  </>
                                </td>
                                <td className="ellipsis">
                                  {onBoardinglist.phoneNumber && onBoardinglist.phoneNumber.countryCode ? onBoardinglist.phoneNumber.countryCode + ' ' + onBoardinglist.phoneNumber.number : "--"}
                                </td>
                                <td className="ellipsis">
                                  {onBoardinglist.emailId && onBoardinglist.emailId.emailId ? onBoardinglist.emailId.emailId : "--"}
                                </td>
                                <td className="ellipsis">
                                  {onBoardinglist.businessName ? onBoardinglist.businessName : "--"}
                                </td>
                                {/* <td className="ellipsis">
                                {onBoardinglist.personalEmail ? onBoardinglist.personalEmail : "--"}
                              </td> */}
                                <td className="ellipsis">
                                  {onBoardinglist.website ? onBoardinglist.website : "--"}
                                </td>
                                <td>
                                  <div className='d-flex justify-content-between align-items-center'>
                                    <span className={`me-2 badge ${RISKSTATUS[onBoardinglist && onBoardinglist.accountQueueId && onBoardinglist.accountQueueId.riskStatus]}`}>
                                      {onBoardinglist && onBoardinglist.accountQueueId && onBoardinglist.accountQueueId.riskStatus}
                                    </span>
                                    {onBoardinglist.accountsQueue === "YES" ?
                                      <Link to={`/account-risk-summary/update/${onBoardinglist.accountQueueId && onBoardinglist.accountQueueId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
                                  </div>
                                </td>
                                <td>
                                  <div className='d-flex justify-content-between align-items-center'>
                                    <span className={`me-2 badge ${RISKSTATUS[onBoardinglist && onBoardinglist.kycId && onBoardinglist.kycId.kycStatus]}`}>
                                      {onBoardinglist && onBoardinglist.kycId && onBoardinglist.kycId.kycStatus}
                                    </span>
                                    {onBoardinglist.kycQueue === "YES" ?
                                      <Link to={`/static-summary/update/${onBoardinglist.kycId && onBoardinglist.kycId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
                                  </div>
                                </td>
                                <td>
                                  <div className='d-flex justify-content-between align-items-center'>
                                    <span className={` me-2 badge ${RISKSTATUS[onBoardinglist && onBoardinglist.wrmId && onBoardinglist.wrmId.riskStatus]}`}>
                                      {onBoardinglist && onBoardinglist.wrmId && onBoardinglist.wrmId.riskStatus}
                                    </span>
                                    {onBoardinglist.wrmQueue === "YES" ?
                                      <Link to={`/risk-summary/update/${onBoardinglist.wrmId && onBoardinglist.wrmId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link> : "--"}
                                  </div>
                                </td>
                                <td>
                                  <div className='d-flex justify-content-between align-items-center'>
                                    {
                                      onBoardinglist && onBoardinglist.wrmId && onBoardinglist.wrmId.wrmQueue === "YES" && !_.isEmpty(onBoardinglist.ogmId) ? (
                                        <>
                                          <span className={` me-2 badge ${RISKSTATUS[onBoardinglist && onBoardinglist.ogmId && onBoardinglist.ogmId.riskStatus]}`}>
                                            {onBoardinglist && onBoardinglist.ogmId && onBoardinglist.ogmId.riskStatus}
                                          </span>
                                          <Link to={`/ongoing-monitoring-dashboard/update/${onBoardinglist.ogmId && onBoardinglist.ogmId._id}`} className='btn btn-sm btn-light-primary btn-responsive me-3 pull-right'>Click</Link>
                                        </>
                                      ) : '--'
                                    }
                                  </div>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div >
    </>
  );
}

const mapStateToProps = (state) => {
  const { onBoardinglistStore } = state
  return {
    onBoardinglists: state && state.onBoardinglistStore && state.onBoardinglistStore.onBoardinglists,
    loading: onBoardinglistStore && onBoardinglistStore.loading ? onBoardinglistStore.loading : false
  }
}

const mapDispatchToProps = (dispatch) => ({
  getOnBoardinglistDispatch: (params) => dispatch(onBoardingActions.getOnBoardinglist(params))
});

export default connect(mapStateToProps, mapDispatchToProps)(OnBoardingManagementList);