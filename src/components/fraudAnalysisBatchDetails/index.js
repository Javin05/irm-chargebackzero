import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
import moment from "moment";
import { RISKSTATUS } from "../../utils/constants";
import {
  getLocalStorage,
  removeLocalStorage,
} from "../../utils/helper";
import {
  FraudAnalysisTagListActions,
  FraudToolDetailActions,
} from "../../store/actions";
import ViewSummaryTag from "./viewSummary";
import { useLocation } from "react-router-dom";

const FraudAnalysisBatchDetails = ({
  className,
  getFraudToolTagDispatch,
  getFraudToolBoxDetailsDispatch,
  FraudAnalysisTagDetails,
  loading,
  FraudAnalysisDetails,
  FraudAnalysisDetailsloading,
}) => {
  const location = useLocation();
  let pathToArray = location && location.pathname.split("/");
  let id = pathToArray[3];
  const [limit, setLimit] = useState(20);
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [modelShow,setModelShow] = useState(false)
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  useEffect(() => {
    let params = {
      limit: limit,
      page: activePageNumber,
      summary_id: id
    };
    getFraudToolTagDispatch(params);
  }, [id]);

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: activePageNumber,
      summary_id: id
    };
    setActivePageNumber(pageNumber);
    getFraudToolTagDispatch(params);
  };

  const totalPages =
    FraudAnalysisTagDetails && FraudAnalysisTagDetails.count
      ? Math.ceil(parseInt(FraudAnalysisTagDetails && FraudAnalysisTagDetails.count) / limit)
      : 1;
  const handleReset = () => {
    const params = {
      limit: limit,
      page: 1,
      summary_id: id
    };
    getFraudToolTagDispatch(params)
  };

  const handleModelOpen = (id) => {
	let params = {};
    params.id = id;
    getFraudToolBoxDetailsDispatch(params);
	setModelShow(true)
  }
    return (
      <>
        <div className={`card ${className}`}>
          <div className="card-body py-3">
            <div className="table-responsive">
              <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                <thead className="fw-bolder fs-8 text-gray-800">
                  <tr>
                    <th>
                      <div className="d-flex">
                        <span>Action</span>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex">
                        <span>Analysis ID</span>
                      </div>
                    </th>
                    {Role === "Admin" && (
                      <th>
                        <div className="d-flex">
                          <span>Client name</span>
                        </div>
                      </th>
                    )}
                    <th>
                      <div className="d-flex">
                        <span>API name</span>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex">
                        <span>Job id</span>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex">
                        <span>Fraud tool status</span>
                      </div>
                    </th>
                    <th>
                      <div className="d-flex">
                        <span>Created at</span>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody className="fs-8">
                  {!loading ? (
                    FraudAnalysisTagDetails && FraudAnalysisTagDetails.data ? (
                      FraudAnalysisTagDetails.data &&
                      FraudAnalysisTagDetails.data.map((ele, i) => {
                        return (
                          <tr
                            key={"reef_" + i}
                            style={
                              i === 0
                                ? { borderColor: "black" }
                                : { borderColor: "white" }
                            }
                          >
                            <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                              <a
                                className="color-primary cursor-pointer"
                                onClick={() =>
									handleModelOpen(ele._id)
                                }
                                to={`/fraudanalysis-batchupload`}
                              >
                                <span>
                                  <i
                                    style={{ color: "blue", fontSize: "20px" }}
                                    className="bi bi-eye-fill"
                                  ></i>
                                </span>
                              </a>
                            </td>
                            <td className="ellipsis">
                              <>{ele.analysisId ? ele.analysisId : "--"}</>
                            </td>
                            {Role === "Admin" && (
                              <td className="ellipsis">
                                <>
                                  {ele.client_id ? ele.client_id.company : "--"}
                                </>
                              </td>
                            )}

                            <td className="ellipsis">
                              <>{ele.api_name ? ele.api_name : "--"}</>
                            </td>

                            <td className="ellipsis">
                              <>{ele._id ? ele._id : "--"}</>
                            </td>
                            <td className="ellipsis">
                              <>
                                <span
                                  className={`badge ${RISKSTATUS[ele && ele.fraud_tool_status]
                                    }`}
                                >
                                  {ele.fraud_tool_status
                                    ? ele.fraud_tool_status
                                    : "--"}
                                </span>
                              </>

                            </td>
                            <td className="ellipsis">
                              {ele.createdAt ? moment(ele.createdAt).startOf('minute').fromNow() : "--"}
                            </td>
                          </tr>
                        );
                      })
                    ) : (
                      <tr className="text-center py-3">
                        <td colSpan="100%">
                          No record(s) found. Click below button to reset
                          <br />
                          {
                            <div className="my-auto">
                              <button
                                onClick={() => handleReset()}
                                type="button"
                                className="btn btn-lg btn-primary btn-responsive font-6vw me-3 pull-right"
                              >
                                Reset
                              </button>
                            </div>
                          }
                        </td>
                      </tr>
                    )
                  ) : (
                    <tr>
                      <td colSpan="100%" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
            <div className="form-group row mb-4 mt-6">
              <div className="col-lg-12 mb-4 align-items-end d-flex">
                <div className="col-lg-12">
                  <ReactPaginate
                    nextLabel="Next >"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={4}
                    marginPagesDisplayed={2}
                    pageCount={totalPages}
                    previousLabel="< Prev"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                    renderOnZeroPageCount={null}
                  />
                </div>
              </div>
            </div>
			<div>
				<ViewSummaryTag modelShow={modelShow} setModelShow={setModelShow} FraudAnalysisDetails={FraudAnalysisDetails} FraudAnalysisDetailsloading={FraudAnalysisDetailsloading} />
			</div>
          </div>
        </div> 
      </>
    );
  };

const mapStateToProps = (state) => {
  const {
    FraudAnalysisTagListStore,
	FraudAnalysisDetailManagementStore
  } = state;
  return {
    FraudAnalysisTagDetails:
      FraudAnalysisTagListStore &&
        FraudAnalysisTagListStore.fraudAnalysisTagData ? FraudAnalysisTagListStore.fraudAnalysisTagData : '',
    loading:
      FraudAnalysisTagListStore && FraudAnalysisTagListStore.loading
        ? FraudAnalysisTagListStore.loading
        : false,
	FraudAnalysisDetails:
		FraudAnalysisDetailManagementStore &&
		FraudAnalysisDetailManagementStore.fraudToolDetailData ? FraudAnalysisDetailManagementStore.fraudToolDetailData: '',
	FraudAnalysisDetailsloading:
		FraudAnalysisDetailManagementStore &&
		FraudAnalysisDetailManagementStore.loading ? FraudAnalysisDetailManagementStore.loading: '',
  };
};

const mapDispatchToProps = (dispatch) => ({
  getFraudToolTagDispatch: (params) =>
    dispatch(FraudAnalysisTagListActions.request(params)),
  getFraudToolBoxDetailsDispatch: (params) =>
    dispatch(FraudToolDetailActions.request(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FraudAnalysisBatchDetails);
