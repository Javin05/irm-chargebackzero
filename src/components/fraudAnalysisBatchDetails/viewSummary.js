import React, { useState, useEffect } from "react";
import { Modal } from "../../theme/layout/components/modal";
import { connect } from "react-redux";
import { KTSVG } from "../../theme/helpers";
import { FraudToolDetailActions } from "../../store/actions";
import _ from "lodash";

function ViewSummaryTag(props) {
  const {
    modelShow,
    setModelShow,
    FraudAnalysisDetails,
    FraudAnalysisDetailsloading,
    getFraudToolTagClearDispatch,
  } = props;

  const handleClose = () => {
    getFraudToolTagClearDispatch();
    setModelShow(false);
  };

  let FraudAnalysisDetailsApiName = FraudAnalysisDetails && FraudAnalysisDetails?.data

  let fraudToolSummaryResponseData =
    FraudAnalysisDetails &&
    FraudAnalysisDetails?.data &&
    FraudAnalysisDetails?.data.external_response;
  let input =
    FraudAnalysisDetails &&
    FraudAnalysisDetails?.data &&
    FraudAnalysisDetails?.data?.input_values;

  return (
    <>
    {FraudAnalysisDetailsApiName && FraudAnalysisDetailsApiName.api_name === "Shop Lens" &&
      <Modal showModal={modelShow} modalWidth={800}>
        <div className="" id="userModal">
          <div>
            <div
              className="modal-dialog modal-dialog-centered mw-800px"
              style={{ backgroundColor: "transparent" }}
            >
              <div className="modal-content">
                <div className="modal-header">
                  <h2 className="me-8 mt-4 mb-4 ms-4">View Details</h2>
                  <button
                    type="button"
                    className="btn btn-lg btn-icon btn-active-light-danger close me-4"
                    data-dismiss="modal"
                    onClick={handleClose}
                  >
                    <KTSVG
                      path="/media/icons/duotune/arrows/arr061.svg"
                      className="svg-icon-1"
                    />
                  </button>
                </div>
                <div className="separator separator-dashed border-secondary mt-4 mb-4" />
                <div className="container-fixed">
                  {FraudAnalysisDetailsloading ? (
                    <div className="container-fluid">
                      <div className="row justify-content-center">
                        <div className="spinner-border" role="status">
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                      <div className="row justify-content-center text-center mt-20">
                        <strong>Processing your request. Please wait.</strong>
                      </div>
                    </div>
                  ) : (
                    <div className="card-header">
                      <div className="card-body" style={{height: "75vh", overflow: "scroll"}}>
                        {fraudToolSummaryResponseData?.ocr?.status ? (
                          <>
                            <div className="d-flex justify-content-center">
                              <i
                                className="bi bi-check-circle-fill"
                                style={{
                                  color: "#208f20",
                                  backgroundColor: "transparent",
                                  fontSize: "2.75rem",
                                }}
                              />
                            </div>
                            <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                              Verification successfull!!
                            </div>
                          </>
                        ) : (
                          <>
                            <div className="d-flex justify-content-center">
                              <i
                                className="bi bi-x-circle-fill"
                                style={{
                                  color: "#FF474C",
                                  backgroundColor: "transparent",
                                  fontSize: "2.75rem",
                                }}
                              />
                            </div>
                            <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                              Verification failed!!
                            </div>
                          </>
                        )}
                        <div className="text-center mt-10">
                          <img src={input.files} height={300} width={600} />
                        </div>
                        <div className="row mt-10 mb-20">
                          <div className="col-md-4">
                            <p>Shop name:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.shopName
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.shopName
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Shop name extracted:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.shopNameExtracted
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.shopNameExtracted
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Shop detected:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.shopDetected
                                ? fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.shopDetected.toString()
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Shop owner name:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.shopOwnerName
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.shopOwnerName
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Shop owner name extracted:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.shopOwnerNameExtracted
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.shopOwnerNameExtracted
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>GST number:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.gstNumber
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.gstNumber
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Contact number:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.contactNumber
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.contactNumber
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Articles:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.articles?.length > 0
                                ? fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.articles.join(', ')
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Ranked Industry Descriptions:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.rankedIndustryDescriptions?.length > 0
                                ? fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.rankedIndustryDescriptions.join(', ')
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Shop board Standee detected:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.boardStandeeDetected
                                ? fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.boardStandeeDetected.toString()
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Shop Inventory detected:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.inventoryDetected
                                ? fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.inventoryDetected.toString()
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>isMovable:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.isMovable
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.isMovable
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Address:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.address
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.address
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Address extracted:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.addressExtracted
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.addressExtracted
                                : "No data"}
                            </strong>
                          </div>
                          {fraudToolSummaryResponseData?.ocr?.data[0]?.shoplens360_api?.mccDetails?.map(
                            (ele, index) => {
                              return (
                                <>
                                  <div className="col-md-4">
                                    <p>Description {index + 1}:</p>{" "}
                                  </div>
                                  <div className="col-md-8 ml-auto ">
                                    <strong>{ele.description}</strong>
                                  </div>
                                  <div className="col-md-4">
                                    <p>MCC Code {index + 1}:</p>{" "}
                                  </div>
                                  <div className="col-md-8 ml-auto ">
                                    <strong>{ele.mccCode}</strong>
                                  </div>
                                </>
                              );
                            }
                          )}
                          <div className="col-md-4">
                            <p>Status:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.status
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.status
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Date:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.datetime
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.datetime
                                : "No data"}
                            </strong>
                          </div>
                          <div className="col-md-4">
                            <p>Message:</p>{" "}
                          </div>
                          <div className="col-md-8 ml-auto ">
                            <strong>
                              {fraudToolSummaryResponseData?.ocr?.data[0]
                                ?.shoplens360_api?.message
                                ? fraudToolSummaryResponseData?.ocr?.data[0]
                                    ?.shoplens360_api?.message
                                : "No data"}
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      }
    </>
  );
}

const mapStateToProps = (state) => {
  const { usertypeStore } = state;

  return {
    getDataUserType:
      usertypeStore && usertypeStore.userTypeData
        ? usertypeStore.userTypeData
        : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  getFraudToolTagClearDispatch: () => dispatch(FraudToolDetailActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewSummaryTag);
