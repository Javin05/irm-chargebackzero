import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import '../../styles/dashboard/posts.scss'

function Reports (props) {


  return (
    <div className='card p-3' style={{ height: '100%' }}>
      <iframe
        src='https://app.powerbi.com/view?r=eyJrIjoiODJhNDBiNTgtYzMxYy00YmM3LTlkY2UtNDFmNmRkNmQxNjY4IiwidCI6IjE1M2RhMTU0LTY3NGMtNDViOS1hMWU1LWI0MGZhY2ZlOWU3MiJ9'
        // src='https://app.powerbi.com/view?r=eyJrIjoiODJhNDBiNTgtYzMxYy00YmM3LTlkY2UtNDFmNmRkNmQxNjY4IiwidCI6IjE1M2RhMTU0LTY3NGMtNDViOS1hMWU1LWI0MGZhY2ZlOWU3MiJ9&pageName=ReportSection'
        width='100%'
        height='100%'
      />
    </div>
  )
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reports)
