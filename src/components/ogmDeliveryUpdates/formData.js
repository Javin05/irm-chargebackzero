import _ from "lodash"

export const setOGMDeliveryUpdateData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      client_id: data && data.client_id ? data.client_id : '',
      batch_id: data && data.batch_id ? data.batch_id.toString() : '',
      interval: data && data.interval ? data.interval.toString() : '',
      delivery_date: data && data.delivery_date ? new Date(data.delivery_date) : '',
      ogm_last_run_date: data && data.ogm_last_run_date ? new Date(data.ogm_last_run_date) : '',
      ogm_next_run_date: data && data.ogm_next_run_date ? new Date(data && data.ogm_next_run_date) : '',
      totalCount: data && data.totalCount ? data.totalCount.toString() : '',
      fileName: data && data.fileName ? data.fileName.toString() : '',
      billing_status: data && data.billing_status ? data.billing_status.toString() : '',
      s3fileName: data && data.s3fileName ? data.s3fileName.toString() : '',
      s3filePath: data && data.s3filePath ? data.s3filePath.toString() : '',



    }
  }
}