import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { OGMDeliveryUpdateActions } from '../../store/actions'
import { DATE } from '../../utils/constants'
import { DateSelector } from '../../theme/layout/components/DateSelector'
import moment from "moment"
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'
import color from "../../utils/colors"
import ReactSelect from '../../theme/layout/components/ReactSelect'

function SearchList(props) {
  const { getOGMDeliveryUpdatelistDispatch, loading, setCredFilterParams, setSearchData, clinetIdLists } = props
  const [error, setError] = useState({});
  const [show, setShow] = useState(false)
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [formData, setFormData] = useState({
    clientId: '',
    batchId: '',
    interval: '',
    ogmLastRunDate: '',
    ogmNextRunDate: '',
    deliveryDateFrom: '',
    deliveryDateTo: '',
    totalCount: '',
  })
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData({ ...formData, clientId: selectedOption.value })
    }
  }

  const [condition, setcondition] = useState(true)
  const [clear, setClear] = useState(false)

  const handleSearch = (e) => {
    // const errors = {}
    // if (_.isEmpty(formData.tag)) {
    //   errors.tag = 'Tag is required.'
    // }
    // setErrors(errors)
    // if (_.isEmpty(errors)) {
    setShow(false)
    const UpDate = moment(formData.ogmLastRunDate).format("YYYY-MM-DD")
    const DpDate = moment(formData.ogmNextRunDate).format("YYYY-MM-DD")
    const DeFrDate = moment(formData.deliveryDateFrom).format("YYYY-MM-DD")
    const DeToDate = moment(formData.deliveryDateTo).format("YYYY-MM-DD")
    const params = {
      ogmLastRunDate: UpDate === 'Invalid date' ? '' : UpDate,
      ogmNextRunDate: DpDate === 'Invalid date' ? '' : DpDate,
      deliveryDateFrom: DeFrDate === 'Invalid date' ? '' : DeFrDate,
      deliveryDateTo: DeToDate === 'Invalid date' ? '' : DeToDate,
      batchId: formData.batchId,
      interval: formData.interval,
      clientId: formData.clientId,
      totalCount: formData.totalCount,
      fileName: formData.fileName,
      billing_status: formData.billing_status,
    }
    getOGMDeliveryUpdatelistDispatch(params)
    setSearchData(params)
    // }
  }

  const handleChanges = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...error, [e.target.name]: '' })
  }

  const handleReset = () => {
    setFormData({
      batchId: '',
      interval: '',
      ogmLastRunDate: '',
      ogmNextRunDate: '',
      deliveryDateFrom: '',
      deliveryDateTo: '',
      totalCount: '',
      fileName: "",
      billing_status: ""
    })
    setSearchData({})
    const params = {
      limit: 25,
      page: 1
    }
    getOGMDeliveryUpdatelistDispatch(params)
    setSelectedAsigneesOption('')
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-md btn-light-primary btn-responsive me-3 pull-right w-100px'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => setShow(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => {
            setShow(false)
            setFormData({})
            setSelectedAsigneesOption('')
          }}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className="col-lg-4 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Client :
                    </label>
                    <div className='col-lg-11'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                      // isDisabled={!AsigneesOption}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Batch Id :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Ogm Summary Id'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': formData.batchId && error.batchId },
                          {
                            'is-valid': formData.batchId && !error.batchId
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='batchId'
                        autoComplete='off'
                        value={formData.batchId || ''}
                      />
                      {errors && errors.batchId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.batchId}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      File Name :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Ogm File Name'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': formData.fileName && error.fileName },
                          {
                            'is-valid': formData.fileName && !error.fileName
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='fileName'
                        autoComplete='off'
                        value={formData.fileName || ''}
                      />
                      {errors && errors.fileName && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.fileName}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Frequency :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='interval'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.interval || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='WEEKLY'>WEEKLY</option>
                        <option value='MONTHLY'>MONTHLY</option>
                        <option value='QUARTERLY'>QUARTERLY</option>
                        <option value='ONE TIME'>ONE TIME</option>
                      </select>
                    </div>
                  </div>
                  <div className='col-lg-4 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Ogm Last Run Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogmLastRunDate'
                        placeholder='Ogm Start Date'
                        className='form-control'
                        selected={formData.ogmLastRunDate || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, ogmLastRunDate: '' })
                          setFormData((values) => ({
                            ...values,
                            ogmLastRunDate: date
                          }))
                          setcondition(false)
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Ogm Next Run Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogmNextRunDate'
                        placeholder='Created At From'
                        className='form-control'
                        selected={formData.ogmNextRunDate || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, ogmNextRunDate: '' })
                          setFormData((values) => ({
                            ...values,
                            ogmNextRunDate: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Delivery Date From:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='deliveryDateFrom'
                        placeholder='Delivery Date From'
                        className='form-control'
                        selected={formData.deliveryDateFrom || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, deliveryDateFrom: '' })
                          setFormData((values) => ({
                            ...values,
                            deliveryDateFrom: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Delivery Date To:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='deliveryDateTo'
                        placeholder='Delivery Date To'
                        className='form-control'
                        selected={formData.deliveryDateTo || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, deliveryDateTo: '' })
                          setFormData((values) => ({
                            ...values,
                            deliveryDateTo: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Total Count:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Total count'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': formData.totalCount && error.totalCount },
                          {
                            'is-valid': formData.totalCount && !error.totalCount
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='totalCount'
                        autoComplete='off'
                        value={formData.totalCount || ''}
                      />
                      {errors && errors.totalCount && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.totalCount}
                        </div>
                      )}
                    </div>
                  </div>


                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Billing Status :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='billing_status'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.billing_status || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='COMPLETE'>Complete</option>
                        <option value='PENDING'>Pending</option>
                      </select>
                    </div>
                    {/* // */}

                  </div>
                  <div className='form-group row mb-4 mt-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => handleSearch()}
                          disabled={loading}
                        >
                          {loading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Search'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => handleReset()}
                        >
                          Reset
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OGMDeliveryUpdateStore, clinetListStore } = state
  return {
    loading: OGMDeliveryUpdateStore && OGMDeliveryUpdateStore.loading ? OGMDeliveryUpdateStore.loading : false,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
  }
}

const mapDispatchToProps = dispatch => ({
  getOGMDeliveryUpdatelistDispatch: (params) => dispatch(OGMDeliveryUpdateActions.getOGMDeliveryUpdatelist(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)