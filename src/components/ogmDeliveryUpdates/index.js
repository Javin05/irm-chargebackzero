import axios from 'axios'
import clsx from 'clsx'
import _ from 'lodash'
import moment from "moment"
import React, { Fragment, useEffect, useState } from 'react'
import Modal from 'react-bootstrap/Modal'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import ReactPaginate from 'react-paginate'
import { connect } from 'react-redux'
import { CommomFileAction, OGMDeliveryUpdateActions, OGMDeliveryUpdatedeleteActions, addOGMDeliveryUpdateActions, clientIdLIstActions, updateOGMDeliveryUpdateActions } from '../../store/actions'
import { KTSVG } from "../../theme/helpers"
import { DateSelector } from '../../theme/layout/components/DateSelector'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import { confirmAlert, confirmationAlert, warningAlert } from '../../utils/alerts'
import color from "../../utils/colors"
import { DATE, HEADER, SESSION, STATUS_RESPONSE } from '../../utils/constants'
import { getLocalStorage } from '../../utils/helper'
import { setOGMDeliveryUpdateData } from './formData'
import SearchList from './searchList'

function OgmDeliveryUpdates(props) {
  const {
    className,
    getOGMDeliveryUpdatelistDispatch,
    OGMDeliveryUpdatelists,
    loading,
    addOGMDeliveryUpdateDispatch,
    clearAddOGMDeliveryUpdateDispatch,
    addOGMDeliveryUpdate,
    addOGMDeliveryUpdateloading,
    updateOGMDeliveryUpdateDispatch,
    clearUpdateOGMDeliveryUpdateDispatch,
    updateOGMDeliveryUpdate,
    updateOGMDeliveryUpdateloading,
    deleteOGMDeliveryUpdateDispatch,
    clearDeleteOGMDeliveryUpdateDispatch,
    deleteOGMDeliveryUpdate,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    clinetIdLists,
    clientIdDispatch,
    CommonFileLoading,
    CommonFileDocument,
    CommonFileDispatch,
    ClearCommonFileDispatch
  } = props

  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [searchParams, setSearchParams] = useState({})
  const [sorting, setSorting] = useState({
    batch_id: false,
    interval: false,
    delivery_date: false,
    totalCount: false,
    ogm_last_run_date: false,
    ogm_next_run_date: false
  })
  const [formData, setFormData] = useState([])
  const [searchData, setSearchData] = useState({})
  const [exportReport, setExportReport] = useState('')
  const [exportReportLoading, setExportReportLoading] = useState(false)
  const [updateFormData, setupdateFormData] = useState({
    deliveryDate: '',
    rejectedCases: '',
    approvedCases: '',
    uploadedDate: '',
    file: '',
    tag: ''
  })
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })

  const [show, setShow] = useState(false)
  const [showEdit, setShowEdit] = useState(false)
  const [updateId, setUpdateId] = useState('')
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [AsigneesOptionEdit, setAsigneesEdit] = useState()
  const [SelectedAsigneesOptionEdit, setSelectedAsigneesOptionEdit] = useState('')

  const [addFormData, setAddFormData] = useState({
    client_id: '',
    batch_id: '',
    interval: '',
    delivery_date: '',
    ogm_last_run_date: '',
    ogm_next_run_date: '',
    totalCount: '',
    fileName: "",
    billing_status: ""
  })

  const [errorsAdd, setErrorsAdd] = useState({})

  const [ogmUpdateFormData, setOGMUpdateFormData] = useState({
    client_id: '',
    batch_id: '',
    interval: '',
    delivery_date: '',
    ogm_last_run_date: '',
    ogm_next_run_date: '',
    totalCount: '',
    fileName: "",
    billing_status: "",
    s3fileName: "",
    s3filePath: ""
  })

  const [errorsUpdate, setErrorsUpdate] = useState({})

  const handleChanges = (e) => {
    setupdateFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const [selectedFile, setSelectedFile] = useState(null);
  const [preview, setPreview] = useState(null);
  const [error, setError] = useState("");

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangesAdd = (e) => {
    setAddFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrorsAdd({ ...errorsAdd, [e.target.name]: '' })
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setAddFormData({ ...addFormData, client_id: selectedOption.value })
    }
  }

  const handleChangesEdit = (e) => {
    setOGMUpdateFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrorsUpdate({ ...errorsUpdate, [e.target.name]: '' })
  }

  const handleChangeAsigneesEdit = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOptionEdit(selectedOption)
      setOGMUpdateFormData({ ...ogmUpdateFormData, client_id: selectedOption.value })
    }
  }

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }

  useEffect(() => {
    const param = {
      limit: limit,
      page: activePageNumber
    }
    getOGMDeliveryUpdatelistDispatch(param)
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params)
  }, [])


  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      // clientId: setCredFilterParams.clientId
      //   ? setCredFilterParams.clientId
      //   : "",
      interval: searchData.interval,
      batchId: searchData.batchId,
      ogmLastRunDate: searchData.ogmLastRunDate,
      ogmNextRunDate: searchData.ogmNextRunDate,
      deliveryDateFrom: searchData.deliveryDateFrom,
      deliveryDateTo: searchData.deliveryDateTo,
    }
    setActivePageNumber(pageNumber)
    getOGMDeliveryUpdatelistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getOGMDeliveryUpdatelistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getOGMDeliveryUpdatelistDispatch(params)
    }
  }

  const totalPages =
    OGMDeliveryUpdatelists && OGMDeliveryUpdatelists.data && OGMDeliveryUpdatelists.data.count
      ? Math.ceil(parseInt(OGMDeliveryUpdatelists && OGMDeliveryUpdatelists.data && OGMDeliveryUpdatelists.data.count) / limit)
      : 1


  const ExportData = () => {
    setExportReportLoading(true)
    const params = {
      ...searchData,
      skipPagination: true
    }

    const headers = {
      "Authorization": `${HEADER.BEARER} ${getLocalStorage(SESSION.TOKEN)}`,
      "Content-Type": HEADER.CONTENT_TYPE
    }
    const url = `https://irm-chargebackzero-api.chargebackzero.com/api/v1/ongoingmonitoring/getSummary`
    axios.get(url, { headers, params }).then((response) => {
      if (response.status === 200) {
        setExportReport(response)
      }
    })
  }

  const clear = () => {
    setExportReportLoading(false)
    setExportReport('')
  }

  useEffect(() => {
    if (exportReport && exportReport.data && exportReport.data.status === 'ok') {
      if (Array.isArray(exportReport && exportReport.data && exportReport.data.data && exportReport.data.data.result)) {
        const closeXlsx = document.getElementById('ogmSummaryCsvReport')
        closeXlsx.click()

        setExportReportLoading(false)
      } else if (exportReport && exportReport.data && exportReport.data.status === 'error') {
        warningAlert(
          'error',
          exportReport && exportReport.data && exportReport.data.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
      }
    }
  }, [exportReport])

  const OGMDeliveryAdd = () => {
    setShow(true)
  }

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(addFormData.client_id)) {
      errors.client_id = 'Client ID is required.'
    }
    if (_.isEmpty(addFormData.batch_id)) {
      errors.batch_id = 'Batch ID is required.'
    }
    if (_.isEmpty(addFormData.interval)) {
      errors.interval = 'Interval is required.'
    }
    if (_.isEmpty(moment(addFormData.delivery_date).format("YYYY-MM-DD"))) {
      errors.delivery_date = 'Delivery Date is required.'
    }
    if (_.isEmpty(moment(addFormData.ogm_last_run_date).format("YYYY-MM-DD"))) {
      errors.ogm_last_run_date = 'OGM Last Run Date is required.'
    }
    if (_.isEmpty(moment(addFormData.ogm_next_run_date).format("YYYY-MM-DD"))) {
      errors.ogm_next_run_date = 'OGM Next Run Date is required.'
    }
    if (_.isEmpty(addFormData.totalCount)) {
      errors.totalCount = 'total count is required.'
    }
    if (_.isEmpty(CommonFileDocument?.data)) {
      errors.file = 'File is required.'
    }

    if (_.isEmpty(CommonFileDocument?.data)) {
      errors.file = 'Something went wrong. Please try again.'
    }
    setErrorsAdd(errors)
    if (_.isEmpty(errors)) {
      const data = {
        client_id: addFormData.client_id,
        batch_id: addFormData.batch_id,
        interval: addFormData.interval,
        delivery_date: moment(addFormData.delivery_date).format("YYYY-MM-DD"),
        ogm_last_run_date: moment(addFormData.ogm_last_run_date).format("YYYY-MM-DD"),
        ogm_next_run_date: moment(addFormData.ogm_next_run_date).format("YYYY-MM-DD"),
        totalCount: addFormData.totalCount,
        fileName: addFormData.fileName,
        billing_status: addFormData.billing_status,
        s3fileName: CommonFileDocument?.data?.originalname,
        s3filePath: CommonFileDocument?.data?.path
      }


      if (CommonFileDocument?.data?.originalname && CommonFileDocument?.data?.path)
        addOGMDeliveryUpdateDispatch(data)
      ClearCommonFileDispatch()
      setSelectedFile(null)
    }
  }

  useEffect(() => {
    if (addOGMDeliveryUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        addOGMDeliveryUpdate.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      getOGMDeliveryUpdatelistDispatch()
      clearAddOGMDeliveryUpdateDispatch()
      setSelectedAsigneesOption('')
      setShow(false)
      setAddFormData({
        client_id: '',
        batch_id: '',
        interval: '',
        delivery_date: '',
        ogm_last_run_date: '',
        ogm_next_run_date: '',
        totalCount: '',
      })
    } else if (addOGMDeliveryUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        addOGMDeliveryUpdate.message,
        '',
        'Ok'
      )
    }
  }, [addOGMDeliveryUpdate])

  const tagSummaryEdit = (item) => {

    setShowEdit(true)
    const data = setOGMDeliveryUpdateData(item)


    setOGMUpdateFormData(data)
    setUpdateId(item && item._id)
  }

  useEffect(() => {
    if (ogmUpdateFormData.client_id && AsigneesOptionEdit?.length) {
      const selectedData = AsigneesOptionEdit.find((item) => item.value === ogmUpdateFormData.client_id);

      if (selectedData) {
        setSelectedAsigneesOptionEdit({ label: selectedData.label, value: selectedData.value });
      }
    }
  }, [updateId, ogmUpdateFormData.client_id, AsigneesOptionEdit]);


  const updateTagSubmit = () => {
    const errors = {}
    if (_.isEmpty(ogmUpdateFormData.client_id)) {
      errors.client_id = 'Client ID is required.'
    }
    if (_.isEmpty(ogmUpdateFormData.batch_id)) {
      errors.batch_id = 'Batch ID is required.'
    }
    if (_.isEmpty(ogmUpdateFormData.interval)) {
      errors.interval = 'Interval is required.'
    }
    if (_.isEmpty(moment(ogmUpdateFormData.delivery_date).format("YYYY-MM-DD"))) {
      errors.delivery_date = 'Delivery Date is required.'
    }
    if (_.isEmpty(moment(ogmUpdateFormData.ogm_last_run_date).format("YYYY-MM-DD"))) {
      errors.ogm_last_run_date = 'OGM Last Run Date is required.'
    }
    if (_.isEmpty(moment(ogmUpdateFormData.ogm_next_run_date).format("YYYY-MM-DD"))) {
      errors.ogm_next_run_date = 'OGM Next Run Date is required.'
    }
    if (_.isEmpty(moment(ogmUpdateFormData.totalCount).format("YYYY-MM-DD"))) {
      errors.totalCount = 'total count is required.'
    }
    setErrorsUpdate(errors)


    if (_.isEmpty(errors)) {
      const data = {
        client_id: ogmUpdateFormData.client_id,
        batch_id: ogmUpdateFormData.batch_id,
        interval: ogmUpdateFormData.interval,
        fileName: ogmUpdateFormData.fileName,
        billing_status: ogmUpdateFormData.billing_status,
        delivery_date: moment(ogmUpdateFormData.delivery_date).format("YYYY-MM-DD"),
        ogm_last_run_date: moment(ogmUpdateFormData.ogm_last_run_date).format("YYYY-MM-DD"),
        ogm_next_run_date: moment(ogmUpdateFormData.ogm_next_run_date).format("YYYY-MM-DD"),
        totalCount: ogmUpdateFormData.totalCount,
        // s3fileName: ogmUpdateFormData.s3fileName,
        // s3filePath: ogmUpdateFormData.s3filePath
        s3fileName: CommonFileDocument?.data?.originalname ?? ogmUpdateFormData.s3fileName,
        s3filePath: CommonFileDocument?.data?.path ?? ogmUpdateFormData.s3filePath
      }
      updateOGMDeliveryUpdateDispatch(updateId, data)
    }
  }

  useEffect(() => {
    if (updateOGMDeliveryUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        updateOGMDeliveryUpdate.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      getOGMDeliveryUpdatelistDispatch()
      clearUpdateOGMDeliveryUpdateDispatch()
      setUpdateId('')
      setSelectedAsigneesOptionEdit('')
      setShowEdit(false)
      setOGMUpdateFormData({
        client_id: '',
        batch_id: '',
        interval: '',
        delivery_date: '',
        ogm_last_run_date: '',
        ogm_next_run_date: '',
        totalCount: '',
        billing_status: '',
        fileName: '',
        s3fileName: '',
        s3filePath: ''
      })
    } else if (updateOGMDeliveryUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        updateOGMDeliveryUpdate.message,
        '',
        'Ok'
      )
    }
  }, [updateOGMDeliveryUpdate])



  const onConfirm = (id) => {
    const params = {
      id: id
    }
    deleteOGMDeliveryUpdateDispatch(params)
  }

  const onDeleteItem = (id) => {
   
    confirmationAlert(
      "Are you sure want to delete this Data,",
      "If you delete the record, can't revert back?",
      'warning',
      'Yes',
      'No',
      () => {
        onConfirm(id)
      },
      () => { }
    )
  }

  useEffect(() => {
    if (deleteOGMDeliveryUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        deleteOGMDeliveryUpdate.message,
        'success',
        'ok',
        // () => { { } },
        // () => { { } }
      )
      getOGMDeliveryUpdatelistDispatch()
      clearDeleteOGMDeliveryUpdateDispatch()
    } else if (deleteOGMDeliveryUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        deleteOGMDeliveryUpdate.message,
        '',
        'Ok'
      )
    }
  }, [deleteOGMDeliveryUpdate])

  const OGMDeliveryAddCloseModal = () => {
    setAddFormData({
      client_id: '',
      batch_id: '',
      interval: '',
      delivery_date: '',
      ogm_last_run_date: '',
      ogm_next_run_date: '',
      totalCount: ''
    })
    setErrorsAdd({})
    setShow(false)
  }

  const OGMDeliveryUpdateCloseModal = () => {
    setOGMUpdateFormData({
      client_id: '',
      batch_id: '',
      interval: '',
      delivery_date: '',
      ogm_last_run_date: '',
      ogm_next_run_date: '',
      totalCount: ''
    })
    setErrorsUpdate({})
    setShowEdit(false)
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  //Add
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  //Edit
  useEffect(() => {
    const Asignees = getDefaultOptionEdit(AsigneesNames)
    setAsigneesEdit(Asignees)
  }, [AsigneesNames])

  const getDefaultOptionEdit = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }


  const handleFileChange = (e) => {
    const file = e.target.files[0];

    if (file) {
      // Allow only Excel and CSV files
      const allowedTypes = [
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xlsx
        "text/csv", // .csv
      ];
      const maxSize = 10 * 1024 * 1024; // 10MB

      if (!allowedTypes.includes(file.type)) {
        setError("Only Excel (.xlsx) or CSV (.csv) files are allowed.");
        setSelectedFile(null);
        return;
      }

      if (file.size > maxSize) {
        setError("File size should not exceed 10MB.");
        setSelectedFile(null);
        return;
      }
      setSelectedFile(file)
      const fileData = new FormData()
      fileData.append('type', 'ogm-delivery-updates')
      fileData.append('file_to_upload', file)
      CommonFileDispatch(fileData)
      setError("");
    }
  };


  const handleExportReport = (filePath) => {
    const link = document.createElement('a');
    link.href = filePath;
    link.download = filePath.split('/').pop(); // Extract file name from path
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };


  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => setShow(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => setShow(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Add
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className="col-lg-4 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 form-label">
                      Client :
                    </label>
                    <div className='col-lg-11'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                      // isDisabled={!AsigneesOption}
                      />
                      {errorsAdd && errorsAdd.client_id && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.client_id}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      File Name:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='File Name'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': addFormData.fileName && errorsAdd.fileName },
                          {
                            'is-valid': addFormData.fileName && !errorsAdd.fileName
                          }
                        )}
                        onChange={(e) => handleChangesAdd(e)}
                        type='text'
                        name='fileName'
                        autoComplete='off'
                        value={addFormData.fileName || ''}
                      />
                      {errorsAdd && errorsAdd.fileName && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.fileName}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Batch ID:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Batch ID'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': addFormData.batch_id && errorsAdd.batch_id },
                          {
                            'is-valid': addFormData.batch_id && !errorsAdd.batch_id
                          }
                        )}
                        onChange={(e) => handleChangesAdd(e)}
                        type='text'
                        name='batch_id'
                        autoComplete='off'
                        value={addFormData.batch_id || ''}
                      />
                      {errorsAdd && errorsAdd.batch_id && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.batch_id}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Frequency :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='interval'
                        className={clsx(
                          'form-select form-select-solid',
                          { 'is-invalid': addFormData.interval && errorsAdd.interval },
                          {
                            'is-valid': addFormData.interval && !errorsAdd.interval
                          }
                        )}
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChangesAdd(e)}
                        value={addFormData.interval || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='WEEKLY'>WEEKLY</option>
                        <option value='MONTHLY'>MONTHLY</option>
                        <option value='QUARTERLY'>QUARTERLY</option>
                        <option value='ONE TIME'>ONE TIME</option>
                      </select>
                      {errorsAdd && errorsAdd.interval && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.interval}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Delivery Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='delivery_date'
                        placeholder='Delivery Date'
                        className='form-control'
                        selected={addFormData.delivery_date || ''}
                        onChange={(date) => {
                          setErrorsAdd({ ...errorsAdd, delivery_date: '' })
                          setAddFormData((values) => ({
                            ...values,
                            delivery_date: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        // isClearable={clear}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                      {errorsAdd && errorsAdd.delivery_date && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.delivery_date}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      OGM Last Run Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogm_last_run_date'
                        placeholder='OGM Last Run Date'
                        className='form-control'
                        selected={addFormData.ogm_last_run_date || ''}
                        onChange={(date) => {
                          setErrorsAdd({ ...errorsAdd, ogm_last_run_date: '' })
                          setAddFormData((values) => ({
                            ...values,
                            ogm_last_run_date: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      // isClearable={true}
                      />
                      {errorsAdd && errorsAdd.ogm_last_run_date && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.ogm_last_run_date}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      OGM Next Run Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogm_next_run_date'
                        placeholder='OGM Next Run Date'
                        className='form-control'
                        selected={addFormData.ogm_next_run_date || ''}
                        onChange={(date) => {
                          setErrorsAdd({ ...errorsAdd, ogm_next_run_date: '' })
                          setAddFormData((values) => ({
                            ...values,
                            ogm_next_run_date: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      // isClearable={true}
                      />
                      {errorsAdd && errorsAdd.ogm_next_run_date && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.ogm_next_run_date}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Total Count:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Total count'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': addFormData.totalCount && errorsAdd.totalCount },
                          {
                            'is-valid': addFormData.totalCount && !errorsAdd.totalCount
                          }
                        )}
                        onChange={(e) => handleChangesAdd(e)}
                        type='text'
                        name='totalCount'
                        autoComplete='off'
                        value={addFormData.totalCount || ''}
                      />
                      {errorsAdd && errorsAdd.totalCount && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.totalCount}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Billing status :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='billing_status'
                        className={clsx(
                          'form-select form-select-solid',
                          { 'is-invalid': addFormData.billing_status && errorsAdd.billing_status },
                          {
                            'is-valid': addFormData.billing_status && !errorsAdd.billing_status
                          }
                        )}
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChangesAdd(e)}
                        value={addFormData.billing_status || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='COMPLETE'>Complete</option>
                        <option value='PENDING'>Pending</option>

                      </select>
                      {errorsAdd && errorsAdd.billing_status && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsAdd.billing_status}
                        </div>
                      )}
                    </div>
                  </div>
                  {/* ///// */}


                  <div className="container mt-4">
                    <h3>Add File Upload</h3>
                    <input
                      type="file"
                      onChange={handleFileChange}
                      className="form-control"
                      accept=".xlsx,.csv"
                    />

                    {error && <div className="alert alert-danger mt-2">{error}</div>}
                    {errorsAdd && errorsAdd.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errorsAdd.file}
                      </div>
                    )}
                    {selectedFile && (
                      <div className="mt-3 text-success">
                        <p>{CommonFileDocument?.data?.originalname}</p>
                      </div>
                    )}

                  </div>


                  {/* /// */}
                  <div className='form-group row mb-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => handleSubmit()}
                          disabled={addOGMDeliveryUpdateloading}
                        >
                          {
                            addOGMDeliveryUpdateloading
                              ? (
                                <span
                                  className='spinner-border spinner-border-sm mx-3'
                                  role='status'
                                  aria- hidden='true'
                                />
                              )
                              : (
                                'Submit'
                              )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => OGMDeliveryAddCloseModal()}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <Modal
        show={showEdit}
        size="lg"
        centered
        onHide={() => setShowEdit(false)}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => setShowEdit(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Update
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className="col-lg-4 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 form-label">
                      Client :
                    </label>
                    <div className='col-lg-11'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsigneesEdit}
                        options={AsigneesOptionEdit}
                        value={SelectedAsigneesOptionEdit}
                        isDisabled={!AsigneesOptionEdit}
                      />
                      {errorsUpdate && errorsUpdate.client_id && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.client_id}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      File Name:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='File Name'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': ogmUpdateFormData.fileName && errorsUpdate.fileName },
                          {
                            'is-valid': ogmUpdateFormData.fileName && !errorsUpdate.fileName
                          }
                        )}
                        onChange={(e) => handleChangesEdit(e)}
                        type='text'
                        name='fileName'
                        autoComplete='off'
                        value={ogmUpdateFormData.fileName || ''}
                      />
                      {errorsUpdate && errorsUpdate.fileName && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.fileName}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Batch ID:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Batch ID'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': ogmUpdateFormData.batch_id && errorsUpdate.batch_id },
                          {
                            'is-valid': ogmUpdateFormData.batch_id && !errorsUpdate.batch_id
                          }
                        )}
                        onChange={(e) => handleChangesEdit(e)}
                        type='text'
                        name='batch_id'
                        autoComplete='off'
                        value={ogmUpdateFormData.batch_id || ''}
                      />
                      {errorsUpdate && errorsUpdate.batch_id && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.batch_id}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Frequency :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='interval'
                        className={clsx(
                          'form-select form-select-solid',
                          { 'is-invalid': ogmUpdateFormData.interval && errorsUpdate.interval },
                          {
                            'is-valid': ogmUpdateFormData.interval && !errorsUpdate.interval
                          }
                        )}
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChangesEdit(e)}
                        value={ogmUpdateFormData.interval || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='WEEKLY'>WEEKLY</option>
                        <option value='MONTHLY'>MONTHLY</option>
                        <option value='QUARTERLY'>QUARTERLY</option>
                        <option value='ONE TIME'>ONE TIME</option>
                      </select>
                      {errorsUpdate && errorsUpdate.interval && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.interval}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Delivery Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='delivery_date'
                        placeholder='Delivery Date'
                        className='form-control'
                        selected={ogmUpdateFormData.delivery_date || ''}
                        onChange={(date) => {
                          setErrorsUpdate({ ...errorsUpdate, delivery_date: '' })
                          setOGMUpdateFormData((values) => ({
                            ...values,
                            delivery_date: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        // isClearable={clear}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                      {errorsUpdate && errorsUpdate.delivery_date && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.delivery_date}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      OGM Last Run Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogm_last_run_date'
                        placeholder='OGM Last Run Date'
                        className='form-control'
                        selected={ogmUpdateFormData.ogm_last_run_date || ''}
                        onChange={(date) => {
                          setErrorsUpdate({ ...errorsUpdate, ogm_last_run_date: '' })
                          setOGMUpdateFormData((values) => ({
                            ...values,
                            ogm_last_run_date: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      // isClearable={true}
                      />
                      {errorsUpdate && errorsUpdate.ogm_last_run_date && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.ogm_last_run_date}
                        </div>
                      )}
                    </div>
                  </div>

                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Billing status :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='billing_status'
                        className={clsx(
                          'form-select form-select-solid',
                          { 'is-invalid': ogmUpdateFormData.billing_status && errorsUpdate.billing_status },
                          {
                            'is-valid': ogmUpdateFormData.billing_status && !errorsUpdate.billing_status
                          }
                        )}
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChangesEdit(e)}
                        value={ogmUpdateFormData.billing_status || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='COMPLETE'>Complete</option>
                        <option value='PENDING'>Pending</option>
                      </select>
                      {errorsUpdate && errorsUpdate.billing_status && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.billing_status}
                        </div>
                      )}
                    </div>
                  </div>
                  {/* <div className='col-lg-4 mb-3'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      OGM Next Run Date:
                    </label>
                    <div className='col-lg-11'>
                      <DateSelector
                        name='ogm_next_run_date'
                        placeholder='OGM Next Run Date'
                        className='form-control'
                        selected={ogmUpdateFormData.ogm_next_run_date || ''}
                        onChange={(date) => {
                          setErrorsUpdate({ ...errorsUpdate, ogm_next_run_date: '' })
                          setOGMUpdateFormData((values) => ({
                            ...values,
                            ogm_next_run_date: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        maxDate={new Date()}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      // isClearable={true}
                      />
                      {errorsUpdate && errorsUpdate.ogm_next_run_date && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.ogm_next_run_date}
                        </div>
                      )}
                    </div>
                  </div> */}
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Total Count:
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Total count'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': ogmUpdateFormData.totalCount && errorsUpdate.totalCount },
                          {
                            'is-valid': ogmUpdateFormData.totalCount && !errorsUpdate.totalCount
                          }
                        )}
                        onChange={(e) => handleChangesEdit(e)}
                        type='text'
                        name='totalCount'
                        autoComplete='off'
                        value={ogmUpdateFormData.totalCount || ''}
                      />
                      {errorsUpdate && errorsUpdate.totalCount && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errorsUpdate.totalCount}
                        </div>
                      )}
                    </div>
                  </div>


                  <div className="container mt-4">
                    <h3>Excel/CSV File Upload</h3>
                    <input
                      type="file"
                      onChange={handleFileChange}
                      className="form-control"
                      accept=".xlsx,.csv"
                    />

                    {error && <div className="alert alert-danger mt-2">{error}</div>}
                    {errorsAdd && errorsAdd.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errorsAdd.file}
                      </div>
                    )}
                    {ogmUpdateFormData.s3fileName && (
                      <div className="mt-3 text-success">
                        <p>{CommonFileDocument?.data?.originalname ?? ogmUpdateFormData.s3fileName}</p>
                      </div>
                    )}

                  </div>
                  <div className='form-group row mb-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => updateTagSubmit()}
                          disabled={updateOGMDeliveryUpdateloading}
                        >
                          {updateOGMDeliveryUpdateloading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Update'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => OGMDeliveryUpdateCloseModal()}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <div
        type='button'
        className='d-none'
        data-target='#ogmSummaryCsvReport'
      >
        <ReactHTMLTableToExcel
          id="ogmSummaryCsvReport"
          className="download-table-xls-button"
          table="ogmSummaryCsvtable"
          filename={`OGM-EXPORT-BATCH-REPORT`}
          sheet="tablexls"
        />
      </div>
      <div className={`card md-10 ${className}`} >
        <div className='card-body py-3'>
          <div className='d-flex px-2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1 ms-4'>
                {OGMDeliveryUpdatelists && OGMDeliveryUpdatelists.data && OGMDeliveryUpdatelists.data.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3 ms-1'>
                      {OGMDeliveryUpdatelists.data.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='col-lg-6'>
              <div className='d-flex justify-content-end mt-4 me-4'>
                <button
                  type='button'
                  className='btn btn-sm btn-light-success fa-pull-right w-150px me-2'
                  onClick={OGMDeliveryAdd}
                >
                  Add OGM Delivery
                </button>
                {/* {_.isEmpty(searchData) ? null :
                  <button
                    type='button'
                    className='btn btn-sm btn-light-success fa-pull-right w-150px me-2'
                    onClick={ExportData}
                    disabled={exportReportLoading}
                  >
                    {exportReportLoading
                      ? (
                        <span
                          className='spinner-border spinner-border-sm mx-3'
                          role='status'
                          aria-hidden='true'
                        />
                      )
                      : (
                        'Export Data'
                      )}
                  </button>} */}
                <SearchList setCredFilterParams={setCredFilterParams} setSearchData={setSearchData} />
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                  <th className="min-w-50px text-center">
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Batch Name</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("batch_id")}
                        >
                          <i
                            className={`bi ${sorting.batch_id
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>OGM Interval</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("interval")}
                        >
                          <i
                            className={`bi ${sorting.interval
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>File Name</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("fileName")}
                        >
                          <i
                            className={`bi ${sorting.interval
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Deliver Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("delivery_date")}
                        >
                          <i
                            className={`bi ${sorting.delivery_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Count</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("totalCount")}
                        >
                          <i
                            className={`bi ${sorting.totalCount
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>OGM Last Run Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_last_run_date")}
                        >
                          <i
                            className={`bi ${sorting.ogm_last_run_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>OGM Next Run Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ogm_next_run_date")}
                        >
                          <i
                            className={`bi ${sorting.ogm_next_run_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Billing status</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billing_status")}
                        >
                          <i
                            className={`bi ${sorting.ogm_next_run_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Export</span>
                      {/* <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("billing_status")}
                        >
                          <i
                            className={`bi ${sorting.ogm_next_run_date
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div> */}
                    </div>
                  </th>


                </tr>
              </thead>
              <tbody className='fs-7 ms-2'>
                {
                  !loading
                    ? (
                      Array.isArray(OGMDeliveryUpdatelists &&
                        OGMDeliveryUpdatelists.data && OGMDeliveryUpdatelists.data.result)
                        ? (
                          OGMDeliveryUpdatelists && OGMDeliveryUpdatelists.data && OGMDeliveryUpdatelists.data.result.map((item, i) => {
                            const createdAt =
                              moment(
                                item.createdAt ? item.createdAt : "--"
                              ).format("MMM Do YY")
                            const deliverDate =
                              moment(
                                item.delivery_date ? item.delivery_date : "--"
                              ).format("MMM Do YY")
                            const lastRunDate =
                              moment(
                                item.ogm_last_run_date ? item.ogm_last_run_date : "--"
                              ).format("MMM Do YY")
                            const nextRunDate =
                              moment(
                                item.ogm_next_run_date ? item.ogm_next_run_date : "--"
                              ).format("MMM Do YY")

                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  <td>
                                    <button
                                      className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px ms-2'
                                      onClick={() => { tagSummaryEdit(item) }}
                                      title="Update"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-2'
                                      onClick={() => onDeleteItem(item._id)}
                                      title="Delete"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                  <td>
                                    OGM-BATCH-{item && item.batch_id ? item.batch_id : '--'}
                                  </td>
                                  <td>
                                    {item && item.interval ? item.interval : '--'}
                                  </td>
                                  <td>
                                    {item && item.interval ? item.fileName : '--'}
                                  </td>
                                  <td>
                                    {
                                      deliverDate === 'Invalid date' ? '--' : deliverDate
                                    }
                                  </td>
                                  <td>
                                    {item && item.totalCount ? item.totalCount : '--'}
                                  </td>
                                  <td>
                                    {lastRunDate === 'Invalid date' ? '--' : lastRunDate}
                                  </td>
                                  <td>
                                    {nextRunDate === 'Invalid date' ? '--' : nextRunDate}
                                  </td>
                                  <td className="pb-0 pt-5  text-start">
                                    <span
                                      className={`badge ${item.billing_status === 'COMPLETE' ? 'badge-light-success' : 'badge-light-warning'}`}
                                    >
                                      {item?.billing_status || '--'}
                                    </span>

                                  </td>



                                  <td className="ellipsis">
                                    {item && item.s3filePath && (
                                      <button className="btn btn-success btn-sm"
                                        onClick={() => handleExportReport(item.s3filePath)}
                                        disabled={!item.s3filePath}
                                      >
                                        {item.s3filePath &&
                                          <span className='indicator-label'>
                                            <i className="bi bi-filetype-csv" />
                                            Download
                                          </span>
                                        }
                                        {!item.s3filePath && (
                                          <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                                            Please wait...
                                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                          </span>
                                        )}
                                      </button>
                                    )}
                                  </td>
                                </tr>
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* csv Report */}
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="ogmSummaryCsvtable">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Batch Uploaded Date</th>
              <th>Batch Name</th>
              <th>Ogm Interval</th>
              <th>Ogm start Date</th>
              <th>Total Count</th>
              <th>Duplicate Urls</th>
              <th>Valid Url Counts</th>
              <th>Invalid Urls Counts</th>
              <th>Not in WRM</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(exportReport && exportReport.data && exportReport.data.data && exportReport.data.data.result) ?
                exportReport && exportReport.data && exportReport.data.data && exportReport.data.data.result.map((item, it) => {
                  const createdAt =
                    moment(
                      item.createdAt ? item.createdAt : "--"
                    ).format("MMM Do YY")
                  const ogmStartDate =
                    moment(
                      item.ogm_start_date ? item.ogm_start_date : "--"
                    ).format("MMM Do YY")
                  return (
                    <tr key={it}>
                      <td>
                        {createdAt === 'Invalid date' ? '--' : createdAt}
                      </td>
                      <td>
                        OGM-BATCH-{item && item.ogmSummaryId ? item.ogmSummaryId : '--'}
                      </td>
                      <td>
                        {item && item.ogm_interval ? item.ogm_interval : '--'}
                      </td>
                      <td>
                        {ogmStartDate === 'Invalid date' ? '--' : ogmStartDate}
                      </td>
                      <td>
                        {item && item.totalCount ? item.totalCount : '--'}
                      </td>
                      <td>
                        {item && item.duplicateUrls ? item.duplicateUrls : '--'}
                      </td>
                      <td>
                        {item && item.validUrlCounts ? item.validUrlCounts : '--'}
                      </td>
                      <td>
                        {item && item.inValideUrlsCounts ? item.inValideUrlsCounts : '--'}
                      </td>
                      <td>
                        {item && item.notInWebriskCounts ? item.notInWebriskCounts : '--'}
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { OGMDeliveryUpdateStore, addOGMDeliveryUpdateStore, updateOGMDeliveryUpdateStore, OGMDeliveryUpdatedeleteStore, clinetListStore, CommonFileStore } = state

  return {
    OGMDeliveryUpdatelists: OGMDeliveryUpdateStore && OGMDeliveryUpdateStore.OGMDeliveryUpdatelists ? OGMDeliveryUpdateStore.OGMDeliveryUpdatelists : {},
    loading: OGMDeliveryUpdateStore && OGMDeliveryUpdateStore.loading ? OGMDeliveryUpdateStore.loading : false,
    addOGMDeliveryUpdate: addOGMDeliveryUpdateStore && addOGMDeliveryUpdateStore.ogmDeliveryUpdatePost ? addOGMDeliveryUpdateStore.ogmDeliveryUpdatePost : {},
    addOGMDeliveryUpdateloading: addOGMDeliveryUpdateStore && addOGMDeliveryUpdateStore.loading ? addOGMDeliveryUpdateStore.loading : false,
    updateOGMDeliveryUpdate: updateOGMDeliveryUpdateStore && updateOGMDeliveryUpdateStore.saveupdateOGMDeliveryUpdateResponse ? updateOGMDeliveryUpdateStore.saveupdateOGMDeliveryUpdateResponse : {},
    updateOGMDeliveryUpdateloading: updateOGMDeliveryUpdateStore && updateOGMDeliveryUpdateStore.loading ? updateOGMDeliveryUpdateStore.loading : false,
    deleteOGMDeliveryUpdate: OGMDeliveryUpdatedeleteStore && OGMDeliveryUpdatedeleteStore.DeleteODMDeliveryUpdateData ? OGMDeliveryUpdatedeleteStore.DeleteODMDeliveryUpdateData : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    CommonFileDocument: CommonFileStore && CommonFileStore.commonFileRes ? CommonFileStore.commonFileRes : {},
    CommonFileLoading: CommonFileStore && CommonFileStore.loading ? CommonFileStore.loading : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getOGMDeliveryUpdatelistDispatch: (params) => dispatch(OGMDeliveryUpdateActions.getOGMDeliveryUpdatelist(params)),
  addOGMDeliveryUpdateDispatch: (params) => dispatch(addOGMDeliveryUpdateActions.addOGMDeliveryUpdate(params)),
  clearAddOGMDeliveryUpdateDispatch: () => dispatch(addOGMDeliveryUpdateActions.clearaddOGMDeliveryUpdate()),
  updateOGMDeliveryUpdateDispatch: (id, params) => dispatch(updateOGMDeliveryUpdateActions.updateOGMDeliveryUpdate(id, params)),
  clearUpdateOGMDeliveryUpdateDispatch: () => dispatch(updateOGMDeliveryUpdateActions.clearupdateOGMDeliveryUpdate()),
  deleteOGMDeliveryUpdateDispatch: (params) => dispatch(OGMDeliveryUpdatedeleteActions.delete(params)),
  clearDeleteOGMDeliveryUpdateDispatch: () => dispatch(OGMDeliveryUpdatedeleteActions.clear()),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  CommonFileDispatch: (params) => dispatch(CommomFileAction.CommonFile(params)),
  ClearCommonFileDispatch: (params) => dispatch(CommomFileAction.clearCommonFile(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(OgmDeliveryUpdates)