import _ from 'lodash';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, useLocation, useHistory } from 'react-router-dom';
import ReactSelect from 'react-select';
import {
  backWhiteListActions,
  BlackWhiteListGetIdActions,
  clientIdLIstActions,
  inputFieldActions,
  queuesActions,
  updateBlackWhiteListActions
} from '../../store/actions';
import color from "../../utils/colors";
import { userValidation } from "../blackList/validation";
import { STATUS_RESPONSE } from '../../utils/constants';
import { confirmationAlert, warningAlert } from '../../utils/alerts';

function BlackWhiteListForm(props) {
  const {
    postBackWhiteListDispatch,
    updateBackWhiteListDispatch,
    addBlackWhiteList,
    className,
    loadingGR,
    queuesLists,
    clinetIdLists,
    getQueueslistDispatch,
    clientIdDispatch
    , getBlackWhiteListIdDispatch,
    loadingARG,
    loadingUR,
    blackWhiteListIdDetail,
    clearBackWhiteListIdDetailsDispatch,
    statusEA,
    clearupdateBackWhiteListDispatch,
    statusAR,
    messageAR,
    PostclearBlackWhiteListDispatch,
    statusUR,
    messageUR
  } = props;

  const pathName = useLocation().pathname;
  const url = pathName.split('/');
  const currentId = url[3];
  const queue = url[2];

  const [formData, setFormData] = useState({
    clientId: '',
    fields: '',
    queueId: '',
    value: '',
    purpose: '',
    type: '',
  });
  const history = useHistory()

  const [editMode, setEditMode] = useState(false)
  const [QueueOption, setQueue] = useState()

  const [SelectedQueueOption, setSelectedQueueOption] = useState('')
  const [selectedAssignee, setSelectedAssignee] = useState(null);

  const [error, setError] = useState({});
  const [queueValue, setQueueValue] = useState()
  const [AsigneesOption, setAsignees] = useState([]);
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState(null);

  const queueNames = queuesLists && queuesLists.data

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultClientOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])


  useEffect(() => {
    if (blackWhiteListIdDetail) {
      setFormData(blackWhiteListIdDetail);

      if (AsigneesOption.length > 0) {
        const selectedClient = AsigneesOption.find(
          (x) => x.value === blackWhiteListIdDetail.clientId
        );

        if (selectedClient) {
          setSelectedAsigneesOption(selectedClient);
        }
      }

      if (QueueOption?.length > 0) {
        const selectedQueue = QueueOption.find(
          (x) => x.value === blackWhiteListIdDetail.queueId
        );

        if (selectedQueue) {
          setSelectedQueueOption(selectedQueue);
        }
      }
    }
  }, [AsigneesOption, QueueOption, blackWhiteListIdDetail]);



  const onConfirm = () => {
    PostclearBlackWhiteListDispatch()
    setFormData({
      clientId: '',
      fields: '',
      queueId: '',
      value: '',
      purpose: '',
      type: '',
    })
    history.push('/whitelist-blacklist')
  }



  const clear = () => {
    setFormData({
      clientId: '',
      fields: '',
      queueId: '',
      value: '',
      purpose: '',
      type: '',
    })
  }
  console.log(messageUR, "statusAR")

  useEffect(() => {
    if (statusAR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        messageAR,
        'success',
        'Back to BackWhiteList',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      PostclearBlackWhiteListDispatch()
    } else if (statusAR === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        messageAR,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      PostclearBlackWhiteListDispatch()
    }
  }, [statusAR, messageAR])



  useEffect(() => {
    if (currentId) {
      getBlackWhiteListIdDispatch(currentId)
      setEditMode(true)
    } else {
      setEditMode(false)
    }
  }, [currentId])


  useEffect(() => {
    const Queue = getDefaultOption(queueNames)
    setQueue(Queue)
    if (!_.isEmpty(formData.queueId)) {
      const selOption = _.filter(Queue, function (x) { if (_.includes(formData.queueId._id, x.value)) { return x } })
      setSelectedQueueOption(selOption)
    }
  }, [queueNames])

  const getDefaultOption = (queueNames) => {
    const defaultOptions = []
    for (const item in queueNames) {
      defaultOptions.push({ label: queueNames[item].queueName, value: queueNames[item]._id })
    }
    return defaultOptions
  }


  useEffect(() => {
    clientIdDispatch(); // Fetch client IDs on mount
  }, []);



  const getDefaultClientOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  useEffect(() => {

    getQueueslistDispatch()
  }, [])
  const handleChangeQueue = selectedOption => {

    if (selectedOption !== null) {
      setSelectedQueueOption(selectedOption)
      setFormData(values => ({ ...values, queueId: selectedOption.value }))
      setQueueValue(selectedOption.label)
    }
  }
  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prev) => ({ ...prev, [name]: value }));
  };


  const handleSubmit = (e) => {
    const errorMsg = userValidation(formData, setError)

    if (_.isEmpty(error)) {
      if (editMode) {
        updateBackWhiteListDispatch(currentId, formData)
      } else {
        postBackWhiteListDispatch(formData)
      }

    }
  }


  const onConfirmEdit = () => {
    history.push('/whitelist-blacklist')
    setFormData({
      clientId: '',
      fields: '',
      queueId: '',
      value: '',
      purpose: '',
      type: '',
    })
  }


  useEffect(() => {

    console.log(messageUR, "messageUR")
    if (statusUR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        messageUR,
        'success',
        'Back to Black List & White List',
        'Ok',
        () => { onConfirmEdit() },
        () => { onConfirmEdit() }
      )
      clearupdateBackWhiteListDispatch()
    } else if (statusUR === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        messageUR,
        '',
        'Try again',
        '',
        () => { onConfirmEdit() }
      )
      clearupdateBackWhiteListDispatch()
    }
  }, [statusUR])
  return (
    <div style={{ backgroundColor: '#d4e3f6' }} className={`card ${className}`}>
      {loadingGR ? (
        <div className='d-flex justify-content-center py-5'>
          <div className='spinner-border text-primary m-5' role='status' />
        </div>
      ) : (
        <div className='row mt-10'>
          <div className='col-lg-4'>
            <div className='col-md-10 mx-10'>
              <label className='col-form-label col-md-8 text-lg-start required'>Client ID:</label>
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name='AppUserId'
                className='select2'
                classNamePrefix='select'
                onChange={handleChangeAsignees}
                options={AsigneesOption}
                value={SelectedAsigneesOption}
                isDisabled={currentId ? true : false}
              />
              {error && error.client && (
                <div className="rr mt-1">
                  <style>{".rr{color:red}"}</style>
                  {error.client}
                </div>
              )}</div>
          </div>
          <div className='col-lg-4'>
            <div className='col-md-10 mx-10'>
              <label className='col-form-label col-md-8 text-lg-start required'>Queue:</label>
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name='AppUserId'
                className='select2'
                classNamePrefix='select'
                onChange={handleChangeQueue}
                options={QueueOption}
                value={SelectedQueueOption}
                isDisabled={currentId ? true : false}
              />
            </div>
          </div>

          <div className='col-lg-4'>
            <div className='col-md-10 mx-10'>
              <label className='col-form-label col-md-8 text-lg-start required'>Fields:</label>
              <input
                type='text'
                name='fields'
                className='form-control form-control-solid'
                onChange={handleChange}
                value={formData.fields}
                placeholder='Enter Field'
              />
            </div>

          </div>

          <div className='col-lg-4'>
            <div className='col-md-10 mx-10'>
              <label className='col-form-label col-md-8 text-lg-start required'>Value:</label>
              <textarea
                type='text'
                name='value'
                className='form-control form-control-solid'
                onChange={handleChange}
                value={formData.value}
                placeholder='Enter Value'
              />
            </div>
          </div>

          <div className='col-lg-4'>
            <div className='col-md-10 mx-10'>
              <label className='col-form-label col-md-8 text-lg-start required'>Purpose:</label>
              <select
                name='purpose'
                className='form-control form-control-solid'
                onChange={handleChange}
                value={formData.purpose}
              >
                <option value=''>Select Purpose</option>
                <option value='COMMON'>Common</option>
                <option value='CLIENTBASE'>Client Database</option>
              </select>
            </div>
          </div>

          <div className='col-lg-4'>
            <div className='col-md-10 mx-10'>
              <label className='col-form-label col-md-8 text-lg-start required'>Type:</label>
              <select
                name='type'
                className='form-control form-control-solid'
                onChange={handleChange}
                value={formData.type}
              >
                <option value=''>Select Type</option>
                <option value='BLACKLIST'>Blacklist</option>
                <option value='WHITELIST'>Whitelist</option>

              </select>
            </div>
          </div>

          <div className=' ml-auto justify-content-end'>
            <button
              type='button'
              className='btn btn-sm btn-light-primary m-2  fa-pull-right'
              onClick={(e) => handleSubmit(e)}
              disabled={loadingUR || loadingARG}
            >

              {loadingUR || loadingARG ? (
                <span className='indicator-progress' style={{ display: 'block' }}>
                  Please wait...
                  <span className='spinner-border spinner-border-sm align-middle ms-2' />
                </span>
              ) : "Submit"
              }
            </button>


            <Link
              to='/whitelist-blacklist'
              disabled={loadingUR || loadingARG}
              className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
            >
              Go Back
            </Link>
          </div>

        </div>
      )}
    </div>
  );
}

const mapStateToProps = state => {
  const {
    clinetListStore, queueslistStore, blackWhiteListStore, editBlackWhiteListStore, updateBlackWhiteListStore,
  } = state
  return {
    addBlackWhiteList: blackWhiteListStore && blackWhiteListStore.addBlackWhiteList ? blackWhiteListStore.addBlackWhiteList : '',
    loadingGR: blackWhiteListStore && blackWhiteListStore.loadingARG ? blackWhiteListStore.loadingARG : false,
    loadingUR: updateBlackWhiteListStore && updateBlackWhiteListStore.loadingUR ? updateBlackWhiteListStore.loadingUR : false,
    blackWhiteListIdDetail: editBlackWhiteListStore && editBlackWhiteListStore.BlackWhiteListIdDetail ? editBlackWhiteListStore.BlackWhiteListIdDetail : {},
    statusEA: editBlackWhiteListStore && editBlackWhiteListStore.statusEA ? editBlackWhiteListStore.statusEA : '',

    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    messageAR: blackWhiteListStore && blackWhiteListStore.messageAR ? blackWhiteListStore.messageAR : '',
    statusAR: blackWhiteListStore && blackWhiteListStore.statusAR ? blackWhiteListStore.statusAR : '',
    messageUR: updateBlackWhiteListStore && updateBlackWhiteListStore.messageUR ? updateBlackWhiteListStore.messageUR : '',
    statusUR: updateBlackWhiteListStore && updateBlackWhiteListStore.statusUR ? updateBlackWhiteListStore.statusUR : '',


  }
}

const mapDispatchToProps = (dispatch) => ({
  postBackWhiteListDispatch: (params) => dispatch(backWhiteListActions.postBlackWhiteList(params)),
  PostclearBlackWhiteListDispatch: () => dispatch(backWhiteListActions.PostclearBlackWhiteList()),
  getBlackWhiteListIdDispatch: (params) => dispatch(BlackWhiteListGetIdActions.getBlackWhiteListIdDetails(params)),
  clearBackWhiteListIdDetailsDispatch: () => dispatch(BlackWhiteListGetIdActions.clearBlackWhiteListIdDetails()),
  clearupdateBackWhiteListDispatch: () => dispatch(updateBlackWhiteListActions.clearupdateBlackWhiteList()),

  getInputListDispatch: (params) => dispatch(inputFieldActions.getInputFields(params)),
  updateBackWhiteListDispatch: (id, params) => dispatch(updateBlackWhiteListActions.updateBlackWhiteList(id, params)),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BlackWhiteListForm);
