import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { clientCredFilterActions, queuesActions, backWhiteListActions, clientIdLIstActions } from '../../store/actions';
import { KTSVG } from '../../theme/helpers';
import { confirmationAlert, successAlert, warningAlert } from "../../utils/alerts";
import color from "../../utils/colors";
import { SET_FILTER, STATUS_BADGE, STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants';
import { getLocalStorage, unsetLocalStorage } from '../../utils/helper';

function BlackWhiteList(props) {
  const {
    getBlackWhiteListlistDispatch,
    className,
    blackWhiteList,
    loading,
    DeleteBlackWhiteList,
    clearBlackWhiteListDispatch,
    queuesLists,
    getQueueslistDispatch,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    deleteBlackWhiteListDispatch,
    clientIdDispatch,
    clinetIdLists
  } = props
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [SelectedQueueOption, setSelectedQueueOption] = useState('')
  const [QueueOption, setQueue] = useState()
  const [queueIDValue, setQueueIDValue] = useState()
  const [passIDValue, setPassIDValue] = useState()
  const queueNames = queuesLists && queuesLists.data
  const [queueid, setQueueid] = useState();
  //localStorage.getItem('dashboardKey') !== null ? localStorage.getItem('dashboardKey') : "624fc67fae69dc1e03f47ebd"
  const [formData, setFormData] = useState({
    queues: ''
  })
  const [updatedList, setUpdatedList] = useState([]);

  const didMount = React.useRef(false)
  const [sorting, setSorting] = useState({
    blackWhiteListsId: false,
    blackWhiteListsfield: false,
    blackWhiteListsDescription: false,
    clientId: false
  })

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params);
    getBlackWhiteListlistDispatch(pickByParams)
    getQueueslistDispatch()
    clientIdDispatch()
  }, [])



  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1);
      const params = {
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getBlackWhiteListlistDispatch(pickByParams)
      getQueueslistDispatch()
      clientIdDispatch()
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: 1,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    getBlackWhiteListlistDispatch(params)
    setActivePageNumber(1);
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    setActivePageNumber(pageNumber)
    getBlackWhiteListlistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getBlackWhiteListlistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getBlackWhiteListlistDispatch(params)
    }
  }

  const totalPages =
    blackWhiteList && blackWhiteList.count
      ? Math.ceil(parseInt(blackWhiteList && blackWhiteList.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = '/merchant-login';
  };


  const onConfirmDelete = (id) => {
    deleteBlackWhiteListDispatch(id);
  }
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_BLOCKLIST,
      'warning',
      'Yes',
      'No',
      () => { onConfirmDelete(id) },
      () => { }
    )
  }

  useEffect(() => {
    if (DeleteBlackWhiteList && DeleteBlackWhiteList.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        DeleteBlackWhiteList && DeleteBlackWhiteList.message,
        'success'
      )
      clearBlackWhiteListDispatch()

    } else if (DeleteBlackWhiteList && DeleteBlackWhiteList.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DeleteBlackWhiteList && DeleteBlackWhiteList.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearBlackWhiteListDispatch()

    }
  }, [DeleteBlackWhiteList])


  useEffect(() => {
    getBlackWhiteListlistDispatch()
  }, [DeleteBlackWhiteList])
  // useEffect(() => {
  //   const params = {
  //     queueId: queueid
  //   }
  //   getBlackWhiteListlistDispatch(params)
  // }, [queueid])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  let options = [];
  if (queuesLists && queuesLists?.data) {
    queuesLists.data.forEach(element => {
      const obj = {
        'value': element._id,
        'label': element.queueName
      }
      options.push(obj);
    });
  }

  const chooseDashboardHandler = (e) => {
    // setDashboard(e.label)
    setQueueid(e.value)
    localStorage.setItem('dashboardKey', e.value);
    const params = {
      queueId: e && e.value,
      limit: limit,
      page: 1
    }
    getBlackWhiteListlistDispatch(params)
  }

  
  useEffect(() => {
    const Queue = getDefaultOption(queueNames)
    setQueue(Queue)
    if (!_.isEmpty(formData.queues)) {
      const selOption = _.filter(Queue, function (x) { if (_.includes(formData.queues._id, x.value)) { return x } })
      setSelectedQueueOption(selOption)
    }
  }, [queueNames])

  const getDefaultOption = (queueNames) => {
    const defaultOptions = []
    for (const item in queueNames) {
      defaultOptions.push({ label: queueNames[item].queueName, value: queueNames[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })



  useEffect(() => {
    if (blackWhiteList?.result?.length && queuesLists.data?.length) {
      setUpdatedList((prevList) => {
        return blackWhiteList.result.map((item) => {
          const queueName = queuesLists.data?.find((data) => data._id === item.queueId)?.queueName || "--";
          const clientName = clinetIdLists?.data?.result?.find((client) => client._id === item.clientId)?.company || "--";

          return { ...item, queueName, clientName };
        });
      });
    }
  }, [blackWhiteList, queuesLists, clinetIdLists]);


  return (
    <>
      <div className={`card p-7 ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px-2'>
            <div className='d-flex justify-content-start col-md-5'>
              <div className='col-md-3 col-lg-3 col-sm-3 mt-1'>
                {blackWhiteList && blackWhiteList.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {blackWhiteList.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3 col-lg-3 col-sm-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='col-md-2 col-lg-2 col-sm-2 d-flex' />
            <div className='col-md-4 col-lg-4 col-sm-4 d-flex me-2'>
              <label className='col-form-label text-lg-start me-2'>
                Queues :
              </label>
              <div className='col-md-6 col-sm-6 '>

                {queueid !== null ? (
                  <Select
                    options={options}
                    className="react-c-select"
                    onChange={chooseDashboardHandler}
                    value={options.find(op => {
                      return op.value === queueid
                    })} />
                ) : (
                  <Select
                    options={options}
                    className="react-c-select"
                    onChange={chooseDashboardHandler} />
                )
                }


                {/* <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='AppUserId'
                  className='select2'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeQueue}
                  options={QueueOption}
                  value={SelectedQueueOption}
                  isDisabled={!QueueOption}
                /> */}
              </div>
            </div>
            <div className='d-flex col-md-3 col-lg-3 col-sm-3 my-auto'>
              <div className='my-auto me-3'>
                <Link
                  // to={`/blackWhiteList-form/${passIDValue}`}
                  to={`/whitelist-blacklist-form/add`}

                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                  <span className="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                      <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                    </svg>
                  </span>
                  Add List
                </Link>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th >
                    S.No
                  </th>
                  <th>
                    Client
                  </th>
                  <th className=" ">
                    fields
                  </th>
                  <th className=" text-start">
                    type
                  </th>
                  <th className="">
                    purpose
                  </th>
                  <th className="">
                    Values
                  </th>
                  <th className="">
                    queid
                  </th>
                  {/* <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th> */}
                  <th className="">
                    Action
                  </th>

                  {/* <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Edit</span>

                    </div>
                  </th> */}
                </tr>
              </thead>
              <tbody>
                {
                  !loading
                    ? (
                      blackWhiteList &&
                        blackWhiteList.result && blackWhiteList.result.length !== 0
                        ? (
                          blackWhiteList.result && updatedList.map((blackWhiteLists, id) => {
                            return (
                              <tr
                                key={id}
                                style={
                                  id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="pb-0 pt-5  text-start">
                                  {id + 1}
                                </td>
                                {/* <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists.blackWhiteListsGroupId && blackWhiteLists.blackWhiteListsGroupId ? blackWhiteLists.blackWhiteListsGroupId : "--"}
                                </td> */}
                                <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists?.clientName ?? "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists?.fields ?? "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists?.type ?? "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists?.purpose ?? "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists?.value ?? "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {blackWhiteLists?.queueName ?? "--"}
                                </td>
                                {/* <td className="pb-0 pt-5  text-start">
                                  WHERE(
                                  {
                                    blackWhiteLists.customBlackWhiteList && blackWhiteLists.customBlackWhiteList.map((item, i) => {
                                      return (
                                        <>
                                          <div key={i}>
                                            <span className='text-gray-800 me-2'>{item.blackWhiteListsField}</span>
                                            <span className='text-gray-800 fs-4 me-2'>{item.blackWhiteListsOperator}</span>
                                            <span className='text-gray-800 me-2'>
                                              {`${item.blackWhiteListsValue} ${i.length !== 0 ? blackWhiteLists.condition : ""}`}

                                            </span>
                                          </div>
                                        </>
                                      )
                                    })
                                  }
                                  )
                                </td> */}
                                {/* <td className="pb-0 pt-5  text-start">
                                  <span
                                    className={`badge ${STATUS_BADGE[blackWhiteLists.status && blackWhiteLists.status]}`
                                    }>
                                    {blackWhiteLists.status ? blackWhiteLists.status : "--"}
                                  </span>
                                </td> */}
                                <td className="pb-0 pt-5  text-start">
                                  <div title="Edit customer"
                                    className=" text-start btn btn-icon btn-hover-primary btn-sm mx-3"
                                  >
                                    <Link
                                      to={`/whitelist-blacklist-forms/update/${blackWhiteLists._id}`}
                                      className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4" href="/client-onboarding/update/621c7764174b5b0004719991">
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </Link>
                                  </div>
                                  <button
                                    className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                    onClick={() => onDeleteItem(blackWhiteLists._id)}
                                    title="Delete customer"
                                  >
                                    {/* {/ eslint-disable /} */}
                                    <KTSVG
                                      path='/media/icons/duotune/general/gen027.svg'
                                      className='svg-icon-3'
                                    />
                                    {/* {/ eslint-enable /} */}
                                  </button>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  forcePage={activePageNumber - 1}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}

const mapStateToProps = state => {
  const { blackWhiteListStore, queueslistStore, clinetListStore } = state
  return {
    blackWhiteList: state && state.blackWhiteListStore && state.blackWhiteListStore.blackWhiteList?.data,
    loading: state && state.blackWhiteListStore && state.blackWhiteListStore.loading,
    DeleteBlackWhiteList: blackWhiteListStore && blackWhiteListStore.DeleteBlackWhiteList ? blackWhiteListStore.DeleteBlackWhiteList : '',
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',

  }
}
const mapDispatchToProps = dispatch => ({

  getBlackWhiteListlistDispatch: (params) => dispatch(backWhiteListActions.getBlackWhiteList(params)),
  deleteBlackWhiteListDispatch: (id) => dispatch(backWhiteListActions.deleteBlackWhiteList(id)),
  clearBlackWhiteListDispatch: () => dispatch(backWhiteListActions.clearBlackWhiteList()),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),

})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlackWhiteList);