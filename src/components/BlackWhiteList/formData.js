import _ from "lodash";

export const setBlackWhiteListData = (data) => {
  if (data) {
    return {
      clientId: data.clientId,
      fields: data.fields,
      queueId: data.queueId,
      value: data.value,
      purpose: data.purpose,
      type: data.type,
    };
  }
}