import React, { useEffect, useState } from "react"
import { connect } from "react-redux"
import "react-circular-progressbar/dist/styles.css"
import { useLocation } from "react-router-dom"
import "./index.css"
import Websites from './tabs/index'
import { WrmOperationBackendApiAllScheduleAction, WrmOperationManagementBackendAPIStatusActions, WrmOperationManagementDetailActions, WrmOperatorActions, WrmReportEditAction } from '../../store/actions';
import { useHistory } from 'react-router-dom';
import { getLocalStorage, removeLocalStorage } from "../../utils/helper"


function AccountRiskSummary(props) {
  const {
    wrmOperatorActionDispatch,
    getWrmOperationDetails,
    WrmOperationManagementDetails,
    WrmOperationActionStatus,
    WrmOperationManagement,
    WrmOperationManagementBackendAPIStatusDispatch,
    loading,
    WrmReportEditStatus,
    WrmBackendAPIresponseStatus,
    clearBackendAPISchedule,
    clearWRMEditReport
   
  } = props

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]
  useEffect(()=>{
    let params ={}
    params.id = id
    getWrmOperationDetails(params)
    clearBackendAPISchedule()
    clearWRMEditReport()
  },[id,WrmOperationActionStatus,WrmReportEditStatus,WrmBackendAPIresponseStatus])
    const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  console.log(Role)
 const [ids,setIds] = useState([])
  const handleAcceptAnalyst = ()=>{
    let params={}
    params.name = "analyst accept work";
    params.case = id;
    wrmOperatorActionDispatch(params)
  }
  const handleSubmitAnalyst = ()=>{
    let params={}
   params.name = "analyst complete";
   params.case = id;
   wrmOperatorActionDispatch(params)
   history.push('/wrm-management')  
  }
  const handleAcceptQA = ()=>{
    let params={}
    params.name = "qa accept work";
    params.case = id;
    wrmOperatorActionDispatch(params)
  }
  const handleSubmitQA = ()=>{
   let params={}
   params.name = "qa complete";
   params.case = id;
   wrmOperatorActionDispatch(params)  
   history.push('/wrm-management')
  }
  const handleHoldTask = ()=>{
   let params={}
   params.name = "on hold";
   params.case = id;
   wrmOperatorActionDispatch(params)
  
  }
  const handleRework = ()=>{
   let params={}
   params.name = "rework";
   params.case = id;
   wrmOperatorActionDispatch(params)
  
  }
  const handleTaskResume= ()=>{
   let params={}
   params.name = "resume";
   params.case = id;
   wrmOperatorActionDispatch(params)
  }

  React.useEffect(()=>{
    WrmOperationManagementBackendAPIStatusDispatch(id)
  },[id,WrmBackendAPIresponseStatus])
  const history = useHistory();


  useEffect(()=>{
     setIds(WrmOperationManagement?WrmOperationManagement.result:[])
  },[WrmOperationManagement])

  return (
    <>        
            
      {!loading ?<>  {WrmOperationManagementDetails&&  <div className="col-xs-12 d-flex" style={{ display:"flex",justifyContent:"center",alignItem:"center"}}>
                <div  className="text-center d-grid gap-2 d-md-flex">
                    {(WrmOperationManagementDetails?.workStatus==="PENDING" && WrmOperationManagementDetails?.operationStatus==="ANALYST REVIEW")&&
                    <div>
                    <button type="button" className="btn btn-primary me-3"onClick={()=>handleAcceptAnalyst()}>Analyst Accept</button>
                    </div>
                    }
                    {WrmOperationManagementDetails?.workStatus==="IN PROGRESS" && WrmOperationManagementDetails?.operationStatus==="ANALYST REVIEW" && WrmOperationManagementDetails.taskAnalystAccepted&&
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleSubmitAnalyst();}}>Submit</button>
                    </div>
                    }
                  
                   
                    {(WrmOperationManagementDetails?.workStatus==="PENDING" && WrmOperationManagementDetails?.operationStatus==="QA REVIEW"|| WrmOperationManagementDetails?.workStatus==="ISSUE RESOLVED"  && WrmOperationManagementDetails?.operationStatus==="QA REVIEW")&& 
                    <div>
                    <button type="button" className="btn btn-primary me-3"onClick={()=>handleAcceptQA()}>QA Accept</button>
                    </div>
                    }
                     {WrmOperationManagementDetails?.workStatus==="IN PROGRESS" && WrmOperationManagementDetails?.operationStatus==="QA REVIEW" && WrmOperationManagementDetails?.taskQAAccepted &&
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>{handleSubmitQA();}}>Submit</button>
                    </div>
                    }
                     {WrmOperationManagementDetails?.workStatus!=="COMPLETED"?WrmOperationManagementDetails?.workStatus==="ON HOLD" ?
                    <div>
                    <button type="button" className="btn btn-primary me-3" onClick={()=>handleTaskResume()}>Resume</button>
                    </div>:
                    <button type="button" className="btn btn-light-primary me-3" onClick={()=>{handleHoldTask();}}>Hold</button>:null
                    }
                  {(Role!== "Analyst" || Role === "QA Analyst" )&&WrmOperationManagementDetails?.workStatus==="COMPLETED"&& WrmOperationManagementDetails?.operationStatus==="QA REVIEW" &&
                      <button type="button" className="btn btn-light-primary me-3" onClick={()=>{handleRework();}}>Rework</button>
                  }
                </div>
        </div>}

            <br/>
            <br/>
          <div className="col-md-12 card card-xl-stretch mb-xl-8">   
              <div className="separator separator-dashed my-3" />
              <div className="card-body pt-0" >
                <div className="row g-5 g-xl-8">
                    
                {WrmOperationManagementDetails&&<Websites
                  WrmOperationManagementDetails={WrmOperationManagementDetails}
                />}
            </div>
        </div> 
    </div></>:     (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )}
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    WrmOperationManagementDetails: state && state.WrmOperationManagementDetailStore && state.WrmOperationManagementDetailStore.WrmOperationManagementDetail,
    // loading: WrmOperationManagementStore && WrmOperationManagementStore.loading ? WrmOperationManagementStore.loading : false,
    WrmOperationActionStatus:state&&state.wrmOperationManagementActionStore && state.wrmOperationManagementActionStore,
    loading: state && state.WrmOperationManagementDetailStore && state.WrmOperationManagementDetailStore.loading,
    WrmOperationManagement: state && state.WrmOperationManagementStore && state.WrmOperationManagementStore.WrmOperationManagement&&state.WrmOperationManagementStore.WrmOperationManagement.data,
    WrmReportEditStatus: state && state.WrmReportEditStore &&state.WrmReportEditStore.WrmEditRerportMessage,
    WrmBackendAPIresponseStatus: state.WrmBackendApiSchedulerStore&& state.WrmBackendApiSchedulerStore?state.WrmBackendApiSchedulerStore.WRM_API_STATUS:''

  }
}

const mapDispatchToProps = (dispatch) => ({
  getWrmOperationDetails: (id)=>dispatch(WrmOperationManagementDetailActions.getWrmOperationDetail(id)),
  wrmOperatorActionDispatch:(params)=>dispatch(WrmOperatorActions.wrmOperatorActionInit(params)),
  WrmOperationManagementBackendAPIStatusDispatch:(id)=>dispatch(WrmOperationManagementBackendAPIStatusActions.getWrmOperationManagemntBackendAPIStatus(id)),
  clearBackendAPISchedule:()=>dispatch(WrmOperationBackendApiAllScheduleAction.clearWrmApiScheduleAll()),
  clearWRMEditReport:()=>dispatch(WrmReportEditAction.cleareditWrmReport())
})

export default connect(mapStateToProps, mapDispatchToProps)(AccountRiskSummary)