import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _, { values } from 'lodash'
import {
  GetClientsActions,
  WrmOperationManagementActions,
  WrmOperationManagementDetailActions,
  WrmReportEditAction,
  getWrmOperationDetails
} from '../../store/actions'
// import './styles.css';
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setLocalStorage, getLocalStorage } from '../../utils/helper';
import { DateSelector } from '../../theme/layout/components/DateSelector'
import { DATE, SET_FILTER } from '../../utils/constants'
import moment from "moment"

function WrmReportEdit(props) {
  const {

    getWrmOperationManagementlistDispatch,
    Value,
    report,
    editWrmReport,
    id
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [buil, setBUild] = useState(false);
  const [dataValue, setDataValue] = useState({});
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const headClientId = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
  const [manualFormData, setManualFormData] = useState({
    domainActivefor:report?report.domainActivefor:'',
    websiteWorking: report?report.websiteWorking:'',
    legalNameInput: report?report.legalNameInput:'',
    legalNameScrapped: report?report.legalNameScrapped:'',
    legalNameMatch: report?report.legalNameMatch:'',
    domainRegistered: report?report.domainRegistered:'',
    domainRegistrationCompany:report?report.domainRegistrationCompany:'',
    domainRegistrationDate:report?report.domainRegistrationDate==="No Data"?"":new Date(report.domainRegistrationDate):'',
    domainRegistrationExpiryDate:report?report.domainRegistrationExpiryDate==="No Data"?"":new Date(report.domainRegistrationExpiryDate):'',
    sslCertificateCheck:report?report.sslCertificateCheck:'',
    contactDetailsPhone:report?report.contactDetailsPhone:'',
    contactDetailsEmail:report?report.contactDetailsEmail:'',
    merchantIntelligence:report?report.merchantIntelligence:'',
    policyComplianceStatusContactUsPage:report?report.policyComplianceStatusContactUsPage:'',
    policyContainsValidPhone:report&&report&&report.contactDetailsPhone?"TRUE":'FALSE',
    policyContainsValidEmail:report&&report.contactDetailsEmail?"TRUE":'FALSE',
    contactsFormStatus:report?report.contactsFormStatus:'',
    privacyPolicyStatus:report?report.privacyPolicyStatus:'',
    returnPolicyStatus:report?report.returnPolicyStatus:'',
    shippingPolicyStatus:report?report.shippingPolicyStatus:'',
    termsAndConditionStatus:report?report.termsAndConditionStatus:'',
    contactUsPageUrl:report?report.contactUsPageUrl:'No Data',
    privacyPolicyPageUrl:report?report.privacyPolicyPageUrl:'No Data',
    shippingPolicyPageUrl:report?report.shippingPolicyPageUrl:'No Data',
    returnPolicyPageUrl:report?report.returnPolicyPageUrl:'No Data',
    termsAndConditionPageUrl:report?report.termsAndConditionPageUrl:'No Data',
    currenciesFoundOnWebsite:report?report.currenciesFoundOnWebsite:'',
    maxPriceListedInHomePage:report?report.maxPriceListedInHomePage:'',
    minPriceListedInHomePage:report?report.minPriceListedInHomePage:'',
    productPricePageLinks:report?report.productPricePageLinks:'',
    websiteContainsUnreasonablePrice:report?report.websiteContainsUnreasonablePrice:'',
    websiteIsAccessibleWithoutLoginPrompt:report?report.websiteIsAccessibleWithoutLoginPrompt:'',
    websiteRedirection:report?report.websiteRedirection:'',
    websiteRedirectionURL:report?report.websiteRedirectionURL:'',
    domainRegistrarRisk:report?report.domainRegistrarRisk:'',
    merchantIntelligenceRating:report?report.merchantIntelligenceRating:'',
    domainExpiringin:report?report.domainExpiringin:'',
    websiteIsNotWorking:report?report.websiteIsNotWorking:'',
    pricingIsNotUpdated:report?report.pricingIsNotUpdated:'',
    loginCredentialsAreRequired:report?report.loginCredentialsAreRequired:'',
    pricingIsInDollars:report?report.pricingIsInDollars:'',
    highDiscounts:report?report.highDiscounts:'',
    missingPolicyLinks:report?report.missingPolicyLinks:'',
    websiteRedirectionPMA:report?report.websiteRedirectionPMA:'',
    pageNavigationIssue:report?report.pageNavigationIssue:'',
    pmaFlag:report?report.pmaFlag:'',
    adultContentMonitoring:report?report.adultContentMonitoring:'',
    termsAndConditionPageScreenshot:report?report.termsAndConditionPageScreenshot:'No Data',
    privacyPolicyPageScreenshot:report?report.privacyPolicyPageScreenshot:'No Data',
    shippingPolicyPageScreenshot:report?report.shippingPolicyPageScreenshot:'No Data',
    returnPolicyPageScreenshot:report?report.returnPolicyPageScreenshot:'No Data',
    contactUsPageScreenshot:report?report.contactUsPageScreenshot:'No Data',
    logo:report?report.logo:'No Data',
    multipleLineOfBusinesses: report?report.multipleLineOfBusinesses:"No Data",
    mccDescription: report?report.mccDescription:"No Data",
    proposedCategory: report?report.proposedCategory:"No Data",
    proposedMCC: report?report.proposedMCC:"No Data",
    ProposedDescription: report?report.ProposedDescription:"No Data",
    riskClassification: report?report.riskClassification:"No Data", 
    readiness: report?report.readiness:"No Data",
    purchaseOrRegistration: report?report.purchaseOrRegistration:"No Data",
    httpStatusCode: report?report.httpStatusCode:"No Data",
    domainAge: report?report.domainAge:"No Data",
    domainRank: report?report.domainRank:"No Data",
    contentKeywordResults: report?report.contentKeywordResults:"No Data",
    pageAvailabilityCheckURLStatus: report?report.pageAvailabilityCheckURLStatus:"No Data",
    pageHealthCheckContentAccessibilty: report?report.pageHealthCheckContentAccessibilty:"No Data",
    pageHealthCheckPageLoadingTime: report?report.pageHealthCheckPageLoadingTime:"No Data",
    contactsFormStatus: report ? report.contactsFormStatus : "No Data",
    privacyPolicyStatus: report ? report.privacyPolicyStatus : "No Data",
    returnPolicyStatus: report ? report.returnPolicyStatus : "No Data",
    termsAndConditionStatus: report ? report.termsAndConditionStatus : "No Data",
    currenciesFoundOnWebsite: report ? report.currenciesFoundOnWebsite : "No Data",
    averageProductPrice: report ? report.averageProductPrice : "No Data",
    riskScore: report ? report.riskScore : "No Data",
    businessRisk: report ? report.businessRisk : "No Data",
    webContentRisk: report ? report.webContentRisk : "No Data",
    transparencyRisk: report ? report.transparencyRisk : "No Data",
    websiteHealth: report ? report.websiteHealth : "No Data",
    websiteContentCheckupDomainHealth: report ? report.websiteContentCheckupDomainHealth : "No Data",
    onlineWebsiteReputation: report ? report.onlineWebsiteReputation : "No Data",
    webSecurity: report ? report.webSecurity : "No Data",
    loginCredentialsRequired: report ? report.loginCredentialsRequired : "No Data",
    pricingInDollars: report ? report.pricingInDollars : "No Data",
    phoneMatchedWithTransactionLaunderingUrl: report ? report.phoneMatchedWithTransactionLaunderingUrl : "No Data",
    emailMatchedWithTransactionLaunderingUrl: report ? report.emailMatchedWithTransactionLaunderingUrl : "No Data",
    legalNameMatchedWithTransactionLaunderingUrl: report ? report.legalNameMatchedWithTransactionLaunderingUrl : "No Data",
    addressMatchedWithTransactionLaunderingUrl: report ? report.addressMatchedWithTransactionLaunderingUrl : "No Data",
    addressScrappedFromWebsite: report ? report.addressScrappedFromWebsite : "No Data",
    addressMatchPercentage: report ? report.addressMatchPercentage : "No Data",
    addressValidity: report ? report.addressValidity : "No Data",
    blacklistedBusinessDetails: report ? report.blacklistedBusinessDetails : "No Data",
    mcaBlacklistedStatus: report ? report.mcaBlacklistedStatus : "No Data",
    mcaBlacklistedCompanyAndCIN: report ? report.mcaBlacklistedCompanyAndCIN : "No Data",
    mccCodeInput: report ? report.mccCodeInput : "No Data",
    mccCodeMatch: report ? report.mccCodeMatch : "No Data",
    allViolations: report ? report.allViolations : "No Data",
    legalNamePartialMatchPercentage: report ? report.legalNamePartialMatchPercentage : "No Data",
    pageLinksConnectivityCheckSuccessRate: report ? report.pageLinksConnectivityCheckSuccessRate : "No Data",
    presharedAddress: report ? report.presharedAddress : "No Data",
    emailInput: report ? report.emailInput : "No Data",
    phoneInput: report ? report.phoneInput : "No Data",
    upiInput: report ? report.upiInput : "No Data",
    email_in_blacklisted: report ? report.email_in_blacklisted : "No Data",
    phone_in_blacklisted: report ? report.phone_in_blacklisted : "No Data",
    upi_in_blacklisted: report ? report.upi_in_blacklisted : "No Data",
    rejection_reason: report ? report.rejection_reason : "No Data",
    blacklistStatus: report ? report.blacklistStatus : "No Data",
    is_account_linked: report ? report.is_account_linked : "No Data",
    linked_accounts: report ? report.linked_accounts : "No Data",
    no_accounts_linked: report ? report.no_accounts_linked : "No Data",
    linked_cases: report ? report.linked_cases : "No Data",
    linked_to_rejected_cases: report ? report.linked_to_rejected_cases : "No Data",
    no_of_accounts_email: report ? report.no_of_accounts_email : "No Data",
    no_of_accounts_phone: report ? report.no_of_accounts_phone : "No Data",
    no_of_accounts_upi: report ? report.no_of_accounts_upi : "No Data",
    facebookFollowers: report ? report.facebookFollowers : "No Data",
    facebookLikes: report ? report.facebookLikes : "No Data",
    facebookJoinedOn: report ? report.facebookJoinedOn : "No Data",
    instagramFollowers: report ? report.instagramFollowers : "No Data",
    instagramPost: report ? report.instagramPost : "No Data",
    youtubeSubscribers: report ? report.youtubeSubscribers : "No Data",
    youtubeVideos: report ? report.youtubeVideos : "No Data", 
    youtubeJoinedOn: report ? report.youtubeJoinedOn : "No Data",
    linkedInFollowers: report ? report.linkedInFollowers : "No Data",
    twitterFollowers: report ? report.twitterFollowers : "No Data",
    twitterJoinedOn: report ? report.twitterJoinedOn : "No Data",
    pinterestFollowers: report ? report.pinterestFollowers : "No Data",
    onboardingDate: report ? report.onboardingDate : "No Data",
    negativeKeywords: report ? report.negativeKeywords : "No Data",
    contentKeywordRisk: report ? report.contentKeywordRisk : "No Data",
    blacklistAdvanceKeyword: report ? report.blacklistAdvanceKeyword : "No Data",
    productsPriceList: report ? report.productsPriceList : "No Data",
    pageActivityCheckMining: report ? report.pageActivityCheckMining : "No Data",
    pageActivityCheckUntrustedDownloads: report ? report.pageActivityCheckUntrustedDownloads : "No Data",
    suspiciousDomain: report ? report.suspiciousDomain : "No Data",
    parkedDomain: report ? report.parkedDomain : "No Data",
    spamming: report ? report.spamming : "No Data",
    phishing: report ? report.phishing : "No Data",
    curreciesFoundOrNot: report ? report.curreciesFoundOrNot : "No Data",
    malwarePresent: report ? report.malwarePresent : "No Data",
    domainRegistered: report ? report.domainRegistered : "No Data",
    riskLevel: report ? report.riskLevel : "No Data",
    malwareRisk: report ? report.malwareRisk : "No Data",
  })


  const clearPopup = () => {
    setShow(false)
  }

  const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))
  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
      website: tagSearch.website,
      tag: tagSearch.tag,
      riskStatus: tagSearch.riskStatus,
      reportStatus: tagSearch.reportStatus,

    }))
  }, [show])
  useEffect(() => {
    if (Value === true) {
      setManualFormData(values => ({
        ...values,
        website: '',
        acquirer: '',
        tag: '',
        riskStatus: '',
        reportStatus: '',
        createdAtFrom: '',
        createdAtTo: ''
      }))
    } else {
      setBUild(true)
    }
  }, [Value])

   const handleChange = (e) => {
    console.log("e.target.name: ",e.target.name)
    console.log("e.target.value: ",e.target.value)
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }
  const handleEdit = ()=>{
      editWrmReport(id,manualFormData)
      setShow(false)    
  }
  return (
    <>
      <div className='ml-3'>
        <button
          type='button'
          className='btn btn-sm btn-primary font-5vw me-3 pull-right'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          {/* <KTSVG path='/media/icons/duotune/general/gen021.svg' /> */}
          {/* eslint-disable */}
          Edit report
        </button>
      </div>

      <Modal
        show={show}
        size="xl"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
           Edit WRM Report
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className='col-lg-12'>
          <div className='card-header bg-skyBlue py-10 mb-8'>
            <div className='card-body ml-2'>
              <div className='form-group row mb-6'>
                
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Line of business:</label>
                  <div className='col-lg-11'>
                    <input
                      placeholder='Line of business'
                      className='form-control'
                      onChange={(e) => handleChange(e)}
                      type='text'
                      name='multipleLineOfBusinesses'
                      autoComplete='off'
                      value={manualFormData.multipleLineOfBusinesses || ''}
                    />
                  </div>
                </div>
                <div class='col-lg-3 mb-2'>
                  <label class='fs-xs fw-bold mb-2 form-label'>Domain Active For:</label>
                  <div class='col-lg-11'>
                      <input placeholder='Domain Active For?' class='form-control' type='text' name='domainActivefor' autocomplete='off' value={manualFormData.domainActivefor || ''} onChange={(e) => handleChange(e)}/>
                  </div>
                </div>
                <div className='col-lg-3 mb-2 '>
                  <label className='fs-xs fw-bold mb-2 form-label'>Legal Name Scrapped:</label>
                  <div className='col-lg-11'>
                    <input
                    placeholder='Legal Name Scrapped?'
                    className='form-control'
                    onChange={(e) => handleChange(e)}
                    type='text'
                    name='legalNameScrapped'
                    autoComplete='off'
                    value={manualFormData.legalNameScrapped || ''}
                  />
                  </div>
                </div>
                <div className='col-lg-3 mb-2 '>
                  <label className='fs-xs fw-bold mb-2 form-label'>Risk Classification:</label>
                  <div className='col-lg-11'>
                    <input
                    placeholder='Risk Classification'
                    className='form-control'
                    onChange={(e) => handleChange(e)}
                    type='text'
                    name='riskClassification'
                    autoComplete='off'
                    value={manualFormData.riskClassification || ''}
                  />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Registration Company:</label>
                  <div className='col-lg-11'>
                    <input
                      name='domainRegistrationCompany'
                      type='text'
                      className='form-control'
                      placeholder='ARN'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainRegistrationCompany || ''}
                      autoComplete='off'
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Registration Date:</label>
                  <div className='col-lg-11'>
                  <input
                      name='domainRegistrationDate'
                      type='text'
                      className='form-control'
                      placeholder='ARN'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainRegistrationDate || ''}
                      autoComplete='off'
                    />
                  </div>
                </div> 
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Expiry Date:</label>
                  <div className='col-lg-11'>
                  <input
                      name='domainRegistrationExpiryDate'
                      type='text'
                      className='form-control'
                      placeholder='ARN'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainRegistrationExpiryDate || ''}
                      autoComplete='off'
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Adult Content Monitoring:</label>
                  <div className='col-lg-11'>
                        <input
                      name='adultContentMonitoring'
                      type='text'
                      className='form-control'
                      placeholder='Adult Content Monitoring'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.adultContentMonitoring || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Readiness:</label>
                  <div className='col-lg-11'>
                      <input
                      name='readiness'
                      type='text'
                      className='form-control'
                      placeholder='Readiness'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.adultContentMonitoring || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Contact Details Phone:</label>
                  <div className='col-lg-11'>
                    <input
                      name='contactDetailsPhone'
                      type='text'
                      className='form-control'
                      placeholder='Contact Details Phone'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.contactDetailsPhone || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Contact Details Email:</label>
                  <div className='col-lg-11'>
                      <input
                      name='contactDetailsEmail'
                      type='text'
                      className='form-control'
                      placeholder='Contact Details Email'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.contactDetailsEmail || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Purchase Or Registration:</label>
                  <div className='col-lg-11'>
                      <input
                      name='purchaseOrRegistration'
                      type='text'
                      className='form-control'
                      placeholder='Purchase Or Registration'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.purchaseOrRegistration || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Merchant Intelligence:</label>
                  <div className='col-lg-11'>
                      <input
                      name='merchantIntelligence'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.merchantIntelligence || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Http Status Code:</label>
                  <div className='col-lg-11'>
                    <input
                      name='httpStatusCode'
                      type='text'
                      className='form-control'
                      placeholder='Http Status Code'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.httpStatusCode || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Age:</label>
                  <div className='col-lg-11'>
                      <input
                      name='domainAge'
                      type='text'
                      className='form-control'
                      placeholder='Domain Age'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainAge || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Rank:</label>
                    <div className='col-lg-11'>
                      <input
                      name='domainRank'
                      type='text'
                      className='form-control'
                      placeholder='Domain Rank'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainRank || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Default Template like Lorem ipsum:</label>
                  <div className='col-lg-11'>
                      <input
                      name='contentKeywordResults'
                      type='text'
                      className='form-control'
                      placeholder='Default Template like Lorem ipsum'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.contentKeywordResults || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Page Availability Check Url Status:</label>
                  <div className='col-lg-11'>
                      <input
                      name='pageAvailabilityCheckURLStatus'
                      type='text'
                      className='form-control'
                      placeholder='Page Availability Check Url Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.pageAvailabilityCheckURLStatus || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Page Health Check Content Accessibilty:</label>
                  <div className='col-lg-11'>
                      <input
                      name='pageHealthCheckContentAccessibilty'
                      type='text'
                      className='form-control'
                      placeholder='Page Availability Check Url Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.pageHealthCheckContentAccessibilty || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Page Health Check Page Loading Time:</label>
                  <div className='col-lg-11'>
                      <input
                      name='pageHealthCheckPageLoadingTime'
                      type='text'
                      className='form-control'
                      placeholder='Page Health Check Page Loading Time'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.pageHealthCheckPageLoadingTime || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
               
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Contacts Form Status:</label>
                  <div className='col-lg-11'>
                      <input
                      name='contactsFormStatus'
                      type='text'
                      className='form-control'
                      placeholder='Contacts Form Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.contactsFormStatus || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Privacy Policy Status:</label>
                  <div className='col-lg-11'>
                      <input
                      name='privacyPolicyStatus'
                      type='text'
                      className='form-control'
                      placeholder='Privacy Policy Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.privacyPolicyStatus || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Return Policy Status:</label>
                  <div className='col-lg-11'>
                      <input
                      name='returnPolicyStatus'
                      type='text'
                      className='form-control'
                      placeholder='Privacy Policy Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.returnPolicyStatus || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Shipping Policy Status:</label>
                  <div className='col-lg-11'>
                      <input
                      name='shippingPolicyStatus'
                      type='text'
                      className='form-control'
                      placeholder='Privacy Policy Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.shippingPolicyStatus || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Terms And Condition Status:</label>
                  <div className='col-lg-11'>
                      <input
                      name='termsAndConditionStatus'
                      type='text'
                      className='form-control'
                      placeholder='Privacy Policy Status'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.termsAndConditionStatus || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Currencies found on website:</label>
                  <div className='col-lg-11'>
                        <input
                      name='currenciesFoundOnWebsite'
                      type='text'
                      className='form-control'
                      placeholder='Currencies found on website'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.currenciesFoundOnWebsite || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Average Product Price:</label>
                  <div className='col-lg-11'>
                        <input
                      name='averageProductPrice'
                      type='text'
                      className='form-control'
                      placeholder='Currencies found on website'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.averageProductPrice || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Website Is Accessible Without Login Prompt:</label>
                  <div className='col-lg-11'>
                    <input
                      name='websiteIsAccessibleWithoutLoginPrompt'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.websiteIsAccessibleWithoutLoginPrompt || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Merchant Intelligence Rating:</label>
                  <div className='col-lg-11'>
                    <input
                      name='merchantIntelligenceRating'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.merchantIntelligenceRating || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Missing Policy Links:</label>
                  <div className='col-lg-11'>
                      <input
                      name='missingPolicyLinks'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.missingPolicyLinks || ''}
                      autoComplete='off'
                      
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-2 '>
                  <label className='fs-xs fw-bold mb-2 form-label'>Legal Name Input:</label>
                  <div className='col-lg-11'>
                    <input
                    placeholder='Legal Name Input?'
                    className='form-control'
                    onChange={(e) => handleChange(e)}
                    type='text'
                    name='legalNameInput'
                    autoComplete='off'
                    value={manualFormData.legalNameInput|| ''}
                  />
                  </div>
                </div>
                
                <div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Domain Expiring In:</label>
    <div class='col-lg-11'>
        <input placeholder='Domain Expiring In?' class='form-control' type='text' name='domainExpiringIn' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.domainExpiringIn || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Risk Score:</label>
    <div class='col-lg-11'>
        <input placeholder='Risk Score?' class='form-control' type='text' name='riskScore' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.riskScore || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Status:</label>
    <div class='col-lg-11'>
        <input placeholder='Status?' class='form-control' type='text' name='status' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.status || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Business Risk:</label>
    <div class='col-lg-11'>
        <input placeholder='Business Risk?' class='form-control' type='text' name='businessRisk' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.businessRisk || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Web Content Risk:</label>
    <div class='col-lg-11'>
        <input placeholder='Web Content Risk?' class='form-control' type='text' name='webContentRisk' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.webContentRisk || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Transparency Risk:</label>
    <div class='col-lg-11'>
        <input placeholder='Transparency Risk?' class='form-control' type='text' name='transparencyRisk' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.transparencyRisk || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Website Health:</label>
    <div class='col-lg-11'>
        <input placeholder='Website Health?' class='form-control' type='text' name='websiteHealth' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.websiteHealth || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Website Content Checkup Domain Health:</label>
    <div class='col-lg-11'>
        <input placeholder='Website Content Checkup Domain Health?' class='form-control' type='text' name='websiteContentCheckupDomainHealth' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.websiteContentCheckupDomainHealth || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Online Website Reputation:</label>
    <div class='col-lg-11'>
        <input placeholder='Online Website Reputation?' class='form-control' type='text' name='onlineWebsiteReputation' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.onlineWebsiteReputation || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Web Security:</label>
    <div class='col-lg-11'>
        <input placeholder='Web Security?' class='form-control' type='text' name='webSecurity' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.webSecurity || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Website Is Not Working:</label>
    <div class='col-lg-11'>
        <input placeholder='Website Is Not Working?' class='form-control' type='text' name='websiteIsNotWorking' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.websiteIsNotWorking || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Pricing Is Not Updated:</label>
    <div class='col-lg-11'>
        <input placeholder='Pricing Is Not Updated?' class='form-control' type='text' name='pricingIsNotUpdated' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.pricingIsNotUpdated || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Login Credentials Are Required:</label>
    <div class='col-lg-11'>
        <input placeholder='Login Credentials Are Required?' class='form-control' type='text' name='loginCredentialsRequired' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.loginCredentialsRequired || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Pricing Is In Dollars:</label>
    <div class='col-lg-11'>
        <input placeholder='Pricing Is In Dollars?' class='form-control' type='text' name='pricingInDollars' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.pricingInDollars || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>High Discounts:</label>
    <div class='col-lg-11'>
        <input placeholder='High Discounts?' class='form-control' type='text' name='highDiscounts' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.highDiscounts || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Website Redirection Pma:</label>
    <div class='col-lg-11'>
        <input placeholder='Website Redirection Pma?' class='form-control' type='text' name='websiteRedirectionPma' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.websiteRedirectionPma || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Page Navigation Issue:</label>
    <div class='col-lg-11'>
        <input placeholder='Page Navigation Issue?' class='form-control' type='text' name='pageNavigationIssue' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.pageNavigationIssue || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Pma Flag:</label>
    <div class='col-lg-11'>
        <input placeholder='Pma Flag?' class='form-control' type='text' name='pmaFlag' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.pmaFlag || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Phone Matched with Transaction Laundering URL:</label>
    <div class='col-lg-11'>
        <input placeholder='Phone Matched with Transaction Laundering URL?' class='form-control' type='text' name='phoneMatchedWithTransactionLaunderingUrl' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.phoneMatchedWithTransactionLaunderingUrl || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Email Matched with Transaction Laundering URL:</label>
    <div class='col-lg-11'>
        <input placeholder='Email Matched with Transaction Laundering URL?' class='form-control' type='text' name='emailMatchedWithTransactionLaunderingUrl' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.emailMatchedWithTransactionLaunderingUrl || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Legal Name Matched with Transaction Laundering URL:</label>
    <div class='col-lg-11'>
        <input placeholder='Legal Name Matched with Transaction Laundering URL?' class='form-control' type='text' name='legalNameMatchedWithTransactionLaunderingUrl' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.legalNameMatchedWithTransactionLaunderingUrl || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Address Matched with Transaction Laundering URL:</label>
    <div class='col-lg-11'>
        <input placeholder='Address Matched with Transaction Laundering URL?' class='form-control' type='text' name='addressMatchedWithTransactionLaunderingUrl' autocomplete='off' onChange={(e) => handleChange(e)} value={manualFormData.addressMatchedWithTransactionLaunderingUrl || ''} />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Address Scrapped From Website:</label>
    <div class='col-lg-11'>
        <input placeholder='Address Scrapped From Website?' class='form-control' type='text' name='addressScrappedFromWebsite' 
               value={manualFormData.addressScrappedFromWebsite} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Address Match Percentage:</label>
    <div class='col-lg-11'>
        <input placeholder='Address Match Percentage?' class='form-control' type='text' name='addressMatchPercentage' 
               value={manualFormData.addressMatchPercentage} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Address Validity:</label>
    <div class='col-lg-11'>
        <input placeholder='Address Validity?' class='form-control' type='text' name='addressValidity' 
               value={manualFormData.addressValidity} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Blacklisted Business Details:</label>
    <div class='col-lg-11'>
        <input placeholder='Blacklisted Business Details?' class='form-control' type='text' name='blacklistedBusinessDetails' 
               value={manualFormData.blacklistedBusinessDetails} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>MCA Blacklisted Status:</label>
    <div class='col-lg-11'>
        <input placeholder='MCA Blacklisted Status?' class='form-control' type='text' name='mcaBlacklistedStatus' 
               value={manualFormData.mcaBlacklistedStatus} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>MCA Blacklisted Company and CIN:</label>
    <div class='col-lg-11'>
        <input placeholder='MCA Blacklisted Company and CIN?' class='form-control' type='text' name='mcaBlacklistedCompanyAndCIN' 
               value={manualFormData.mcaBlacklistedCompanyAndCIN} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Tag:</label>
    <div class='col-lg-11'>
        <input placeholder='Tag?' class='form-control' type='text' name='tag' 
               value={manualFormData.tag} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Acquirer:</label>
    <div class='col-lg-11'>
        <input placeholder='Acquirer?' class='form-control' type='text' name='acquirer' 
               value={manualFormData.acquirer} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Mcc Code Input:</label>
    <div class='col-lg-11'>
        <input placeholder='Mcc Code Input?' class='form-control' type='text' name='mccCodeInput' 
               value={manualFormData.mccCodeInput} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Mcc Code Match:</label>
    <div class='col-lg-11'>
        <input placeholder='Mcc Code Match?' class='form-control' type='text' name='mccCodeMatch' 
               value={manualFormData.mccCodeMatch} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>All Violations:</label>
    <div class='col-lg-11'>
        <input placeholder='All Violations?' class='form-control' type='text' name='allViolations' 
               value={manualFormData.allViolations} 
               onChange={(e) => handleChange(e)}
               autocomplete='off' />
    </div>
</div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Max Price Listed:</label>
                  <div className='col-lg-11'>
                    <input
                      name='maxPriceListedInHomePage'
                      type='text'
                      className='form-control'
                      placeholder='maxPriceListedInHomePage'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.maxPriceListedInHomePage || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Min Price Listed:</label>
                  <div className='col-lg-11'>
                    <input
                      name='minPriceListedInHomePage'
                      type='text'
                      className='form-control'
                      placeholder='Min Price Listed'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.minPriceListedInHomePage || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
              
                <div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Legal Name Partial Match Percentage:</label>
    <div class='col-lg-11'>
        <input placeholder='Legal Name Partial Match Percentage?' class='form-control' type='text' name='legalNamePartialMatchPercentage' 
               value={manualFormData.legalNamePartialMatchPercentage} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Page Links Connectivity Check Success Rate:</label>
    <div class='col-lg-11'>
        <input placeholder='Page Links Connectivity Check Success Rate?' class='form-control' type='text' name='pageLinksConnectivityCheckSuccessRate' 
               value={manualFormData.pageLinksConnectivityCheckSuccessRate} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Preshared Address:</label>
    <div class='col-lg-11'>
        <input placeholder='Preshared Address?' class='form-control' type='text' name='presharedAddress' 
               value={manualFormData.presharedAddress} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Email - Input:</label>
    <div class='col-lg-11'>
        <input placeholder='Email - Input?' class='form-control' type='text' name='emailInput' 
               value={manualFormData.emailInput} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Email in Blacklisted:</label>
    <div class='col-lg-11'>
        <input placeholder='Email in Blacklisted?' class='form-control' type='text' name='email_in_blacklisted' 
               value={manualFormData.email_in_blacklisted} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Phone - Input:</label>
    <div class='col-lg-11'>
        <input placeholder='Phone - Input?' class='form-control' type='text' name='phoneInput' 
               value={manualFormData.phoneInput} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Phone in Blacklisted:</label>
    <div class='col-lg-11'>
        <input placeholder='Phone in Blacklisted?' class='form-control' type='text' name='phone_in_blacklisted' 
               value={manualFormData.phone_in_blacklisted} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>UPI - Input:</label>
    <div class='col-lg-11'>
        <input placeholder='UPI - Input?' class='form-control' type='text' name='upiInput' 
               value={manualFormData.upiInput} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>UPI in Blacklisted:</label>
    <div class='col-lg-11'>
        <input placeholder='UPI in Blacklisted?' class='form-control' type='text' name='upi_in_blacklisted' 
               value={manualFormData.upi_in_blacklisted} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Rejection Reason:</label>
    <div class='col-lg-11'>
        <input placeholder='Rejection Reason?' class='form-control' type='text' name='rejection_reason' 
               value={manualFormData.rejection_reason} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Blacklist Status:</label>
    <div class='col-lg-11'>
        <input placeholder='Blacklist Status?' class='form-control' type='text' name='blacklistStatus' 
               value={manualFormData.blacklistStatus} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Is Account Linked:</label>
    <div class='col-lg-11'>
        <input placeholder='Is Account Linked?' class='form-control' type='text' name='is_account_linked' 
               value={manualFormData.is_account_linked} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Linked Accounts:</label>
    <div class='col-lg-11'>
        <input placeholder='Linked Accounts?' class='form-control' type='text' name='linked_accounts' 
               value={manualFormData.linked_accounts} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>No Accounts Linked:</label>
    <div class='col-lg-11'>
        <input placeholder='No Accounts Linked?' class='form-control' type='text' name='no_accounts_linked' 
               value={manualFormData.no_accounts_linked} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Linked Cases:</label>
    <div class='col-lg-11'>
        <input placeholder='Linked Cases?' class='form-control' type='text' name='linked_cases' 
               value={manualFormData.linked_cases} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>
<div class='col-lg-3 mb-2'>
    <label class='fs-xs fw-bold mb-2 form-label'>Linked to Rejected Cases:</label>
    <div class='col-lg-11'>
        <input placeholder='Linked to Rejected Cases?' class='form-control' type='text' name='linked_to_rejected_cases' 
               value={manualFormData.linked_to_rejected_cases} 
               onChange={(e) => handleChange(e)} 
               autocomplete='off' />
    </div>
</div>






<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">No of Accounts Email:</label>
    <div class="col-lg-11">
        <input 
            placeholder="No of Accounts Email?" 
            class="form-control" 
            type="text" 
            name="no_of_accounts_email" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.no_of_accounts_email || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">No of Accounts Phone:</label>
    <div class="col-lg-11">
        <input 
            placeholder="No of Accounts Phone?" 
            class="form-control" 
            type="text" 
            name="no_of_accounts_phone" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.no_of_accounts_phone || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">No of Accounts UPI:</label>
    <div class="col-lg-11">
        <input 
            placeholder="No of Accounts UPI?" 
            class="form-control" 
            type="text" 
            name="no_of_accounts_upi" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.no_of_accounts_upi || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Facebook - Followers:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Facebook - Followers?" 
            class="form-control" 
            type="text" 
            name="facebookFollowers" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.facebookFollowers || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Facebook - Likes:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Facebook - Likes?" 
            class="form-control" 
            type="text" 
            name="facebookLikes" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.facebookLikes || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Facebook - Joined On:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Facebook - Joined On?" 
            class="form-control" 
            type="text" 
            name="facebookJoinedOn" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.facebookJoinedOn || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Instagram - Followers:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Instagram - Followers?" 
            class="form-control" 
            type="text" 
            name="instagramFollowers" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.instagramFollowers || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Instagram - Post:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Instagram - Post?" 
            class="form-control" 
            type="text" 
            name="instagramPost" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.instagramPost || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">YouTube - Subscribers:</label>
    <div class="col-lg-11">
        <input 
            placeholder="YouTube - Subscribers?" 
            class="form-control" 
            type="text" 
            name="youtubeSubscribers" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.youtubeSubscribers || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">YouTube - Videos:</label>
    <div class="col-lg-11">
        <input 
            placeholder="YouTube - Videos?" 
            class="form-control" 
            type="text" 
            name="youtubeVideos" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.youtubeVideos || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">YouTube - Joined On:</label>
    <div class="col-lg-11">
        <input 
            placeholder="YouTube - Joined On?" 
            class="form-control" 
            type="text" 
            name="youtubeJoinedOn" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.youtubeJoinedOn || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">LinkedIn - Followers:</label>
    <div class="col-lg-11">
        <input 
            placeholder="LinkedIn - Followers?" 
            class="form-control" 
            type="text" 
            name="linkedInFollowers" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.linkedInFollowers || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Twitter - Followers:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Twitter - Followers?" 
            class="form-control" 
            type="text" 
            name="twitterFollowers" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.twitterFollowers || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Twitter - Joined On:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Twitter - Joined On?" 
            class="form-control" 
            type="text" 
            name="twitterJoinedOn" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.twitterJoinedOn || ''}
        />
    </div>
</div>
<div class="col-lg-3 mb-2">
    <label class="fs-xs fw-bold mb-2 form-label">Pinterest - Followers:</label>
    <div class="col-lg-11">
        <input 
            placeholder="Pinterest - Followers?" 
            class="form-control" 
            type="text" 
            name="pinterestFollowers" 
            autocomplete="off" 
            onChange={(e) => handleChange(e)} 
            value={manualFormData.pinterestFollowers || ''}
        />
    </div>
</div>










<div className="col-lg-3 mb-2">
    <label className="fs-xs fw-bold mb-2 form-label">Onboarding Date:</label>
    <div className="col-lg-11">
        <input 
            placeholder="Onboarding Date?" 
            className="form-control" 
            type="text" 
            name="onboardingDate" 
            autocomplete="off" 
            value={manualFormData.onboardingDate} 
            onChange={(e) => handleChange(e)} 
        />
    </div>
</div>

<div className="col-lg-3 mb-2">
    <label className="fs-xs fw-bold mb-2 form-label">Negative Keywords:</label>
    <div className="col-lg-11">
        <input 
            placeholder="Negative Keywords?" 
            className="form-control" 
            type="text" 
            name="negativeKeywords" 
            autocomplete="off" 
            value={manualFormData.negativeKeywords} 
            onChange={(e) => handleChange(e)} 
        />
    </div>
</div>

<div className="col-lg-3 mb-2">
    <label className="fs-xs fw-bold mb-2 form-label">Content Keyword Risk:</label>
    <div className="col-lg-11">
        <input 
            placeholder="Content Keyword Risk?" 
            className="form-control" 
            type="text" 
            name="contentKeywordRisk" 
            autocomplete="off" 
            value={manualFormData.contentKeywordRisk} 
            onChange={(e) => handleChange(e)} 
        />
    </div>
</div>

<div className="col-lg-3 mb-2">
    <label className="fs-xs fw-bold mb-2 form-label">Blacklist Advance Keyword:</label>
    <div className="col-lg-11">
        <input 
            placeholder="Blacklist Advance Keyword?" 
            className="form-control" 
            type="text" 
            name="blacklistAdvanceKeyword" 
            autocomplete="off" 
            value={manualFormData.blacklistAdvanceKeyword} 
            onChange={(e) => handleChange(e)} 
        />
    </div>
</div>

<div className="col-lg-3 mb-2">
    <label className="fs-xs fw-bold mb-2 form-label">Products Price List:</label>
    <div className="col-lg-11">
        <input 
            placeholder="Products Price List?" 
            className="form-control" 
            type="text" 
            name="productsPriceList" 
            autocomplete="off" 
            value={manualFormData.productsPriceList} 
            onChange={(e) => handleChange(e)} 
        />
    </div>
</div>

            
            <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Legal Name Match:</label>
                  <div className='col-lg-11'>
                    <select
                      name='legalNameMatch'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.legalNameMatch || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='TRUE'>TRUE</option>
                      <option value='FALSE'>FALSE</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Suspicious Domain:</label>
                  <div className='col-lg-11'>
                    <select
                      name='suspiciousDomain'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.suspiciousDomain || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>True</option>
                      <option value='No'>False</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Parked Domain:</label>
                  <div className='col-lg-11'>
                    <select
                      name='parkedDomain'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.parkedDomain || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>True</option>
                      <option value='No'>False</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Spamming:</label>
                  <div className='col-lg-11'>
                    <select
                      name='spamming'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.spamming || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>True</option>
                      <option value='No'>False</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Phishing:</label>
                  <div className='col-lg-11'>
                    <select
                      name='phishing'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.phishing || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>True</option>
                      <option value='No'>False</option>
                    </select>
                  </div>
                </div>

                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Policy Compliance Status Contactus Page:</label>
                  <div className='col-lg-11'>
                    <select
                      name='policyComplianceStatusContactUsPage'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.policyComplianceStatusContactUsPage || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='TRUE'>TRUE</option>
                      <option value='FALSE'>FALSE</option>
                    </select>
                  </div>
                </div> 
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Policy Contains Valid Phone:</label>
                  <div className='col-lg-11'>
                    <select
                      name='policyContainsValidPhone'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.policyContainsValidPhone || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='TRUE'>TRUE</option>
                      <option value='FALSE'>FALSE</option>
                    </select>
                  </div>
                </div>  
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Policy Contains Valid Email:</label>
                  <div className='col-lg-11'>
                      <select
                      name='policyContainsValidEmail'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.policyContainsValidEmail || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='TRUE'>TRUE</option>
                      <option value='FALSE'>FALSE</option>
                    </select>
                  </div>
                </div>  

                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Currencies Found Or Not:</label>
                  <div className='col-lg-11'>
                    <select
                      name='curreciesFoundOrNot'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.curreciesFoundOrNot || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>True</option>
                      <option value='No'>False</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Website Contains Unreasonable Price:</label>
                  <div className='col-lg-11'>
                  <select
                      name='websiteContainsUnreasonablePrice'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.websiteContainsUnreasonablePrice || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='TRUE'>TRUE</option>
                      <option value='FALSE'>FALSE</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                <label className='fs-xs fw-bold mb-2 form-label'>Page Activity Check Mining:</label>
                <div className='col-lg-11'>
                  <select
                    name='pageActivityCheckMining'
                    className='form-select'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleChange(e)}
                    value={manualFormData.pageActivityCheckMining || ''}
                  >
                    <option value=''>Select...</option>
                    <option value='Yes'>True</option>
                    <option value='No'>False</option>
                  </select>
                </div>
            </div>
            <div className='col-lg-3 mb-3'>
                <label className='fs-xs fw-bold mb-2 form-label'>Page Activity Check Untrusted Downloads:</label>
                <div className='col-lg-11'>
                  <select
                    name='pageActivityCheckUntrustedDownloads'
                    className='form-select'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleChange(e)}
                    value={manualFormData.pageActivityCheckUntrustedDownloads || ''}
                  >
                    <option value=''>Select...</option>
                    <option value='Yes'>True</option>
                    <option value='No'>False</option>
                  </select>
                </div>
            </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Website Working?:</label>
                  <div className='col-lg-11'>
                    <select
                      name='websiteWorking'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.websiteWorking || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Malware Present:</label>
                  <div className='col-lg-11'>
                    <select
                      name='malwarePresent'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.malwarePresent || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Registered:</label>
                  <div className='col-lg-11'>
                    <select
                      name='domainRegistered'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainRegistered || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Ssl Certificate Check:</label>
                  <div className='col-lg-11'>
                    <select
                      name='sslCertificateCheck'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.sslCertificateCheck || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
             
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Safety:</label>
                  <div className='col-lg-11'>
                    <select
                      name='safety'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.safety || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Suspicious:</label>
                  <div className='col-lg-11'>
                    <select
                      name='suspicious'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.suspicious || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
               
               
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Website Redirection:</label>
                  <div className='col-lg-11'>
                  <select
                      name='websiteRedirection'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.websiteRedirection || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
                
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Domain Registrar Risk:</label>
                  <div className='col-lg-11'>
                    <select
                      name='domainRegistrarRisk'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.domainRegistrarRisk || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>Yes</option>
                      <option value='No'>No</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Risk Level:</label>
                  <div className='col-lg-11'>
                    <select
                      name='riskLevel'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.riskLevel || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>High</option>
                      <option value='No'>Medium</option>
                      <option value='No'>Low</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-3 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Malware Risk:</label>
                  <div className='col-lg-11'>
                    <select
                      name='malwareRisk'
                      className='form-select'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.malwareRisk || ''}
                    >
                      <option value=''>Select...</option>
                      <option value='Yes'>High</option>
                      <option value='No'>Medium</option>
                      <option value='No'>Low</option>
                    </select>
                  </div>
                </div>
                <div className='col-lg-6 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Contact Us Page Url:</label>
                  <div className='col-lg-11'>
                    <input
                      name='contactUsPageUrl'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.contactUsPageUrl ||'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-6 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Privacy Policy Page Url:</label>
                  <div className='col-lg-11'>
                    <input
                      name='privacyPolicyPageUrl'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.privacyPolicyPageUrl || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-6 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Shipping Policy Page Url:</label>
                  <div className='col-lg-11'>
                      <input
                      name='shippingPolicyPageUrl'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.shippingPolicyPageUrl || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-6 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Return Policy Page Url:</label>
                  <div className='col-lg-11'>
                    <input
                      name='returnPolicyPageUrl'
                      type='text'
                      className='form-control'
                      placeholder='return policy urls'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.returnPolicyPageUrl || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-6 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Terms And Condition Page Url:</label>
                  <div className='col-lg-11'>
                    <input
                      name='termsAndConditionPageUrl'
                      type='text'
                      className='form-control'
                      placeholder='terms and condtion url'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.termsAndConditionPageUrl || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>

                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Logo:</label>
                  <div className='col-lg-11'>
                    <input
                      name='logo'
                      type='text'
                      className='form-control'
                      placeholder='Logo'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.logo || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Contact Us Page screenshot:</label>
                  <div className='col-lg-11'>
                    <input
                      name='contactUsPageScreenshot'
                      type='text'
                      className='form-control'
                      placeholder='contactUsPageScreenshot'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.contactUsPageScreenshot || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Privacy Policy Page screenshot:</label>
                  <div className='col-lg-11'>
                    <input
                      name='privacyPolicyPageScreenshot'
                      type='text'
                      className='form-control'
                      placeholder='privacyPolicyPageScreenshot'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.privacyPolicyPageScreenshot || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Shipping Policy Page screenshot:</label>
                  <div className='col-lg-11'>
                      <input
                      name='shippingPolicyPageScreenshot'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.shippingPolicyPageScreenshot || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Return Policy Page screenshot:</label>
                  <div className='col-lg-11'>
                    <input
                      name='returnPolicyPageScreenshot'
                      type='text'
                      className='form-control'
                      placeholder='return policy urls'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.returnPolicyPageScreenshot || 'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Terms And Condition Page screenshot:</label>
                  <div className='col-lg-11'>
                    <input
                      name='termsAndConditionPageScreenshot'
                      type='text'
                      className='form-control'
                      placeholder='terms and condtion url'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.termsAndConditionPageScreenshot ||'No Data'}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-12 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label' >Product Price Page Links:</label>
                  <div className='col-lg-11'>
                  <textarea
                      name='productPricePageLinks'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.productPricePageLinks || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className='col-lg-6 mb-3'>
                  <label className='fs-xs fw-bold mb-2 form-label'>Website Redirection Url:</label>
                  <div className='col-lg-11'>
                    <input
                      name='websiteRedirectionURL'
                      type='text'
                      className='form-control'
                      placeholder='Merchant Intelligence'
                      onChange={(e) => handleChange(e)}
                      value={manualFormData.websiteRedirectionURL || ''}
                      autoComplete='off'
                      // onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
            































































































































                <div className='col-lg-12 mb-3'>
                  <button 
                    type='button'
                    className='btn btn-sm btn-primary font-5vw me-3'
                    onClick={()=>setShow(false)}
                  >Cancel</button>
                  <button 
                    type='button'
                    className='btn btn-sm btn-primary font-5vw me-3'
                  onClick={()=>handleEdit()}>Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
WrmOperatorsListStore,
riskManagementlistStore,
RiskLevelStore
  } = state
  return {
    loading: riskManagementlistStore &&riskManagementlistStore.loading,
    loadingRL: RiskLevelStore &&RiskLevelStore.loadingRL,
    wrmOperatorsList: WrmOperatorsListStore.WrmOperators?WrmOperatorsListStore.WrmOperators.data:[]
  }
}

const mapDispatchToProps = dispatch => ({
  getWrmOperationManagementlistDispatch: (params) => dispatch(WrmOperationManagementActions.getWrmOperationManagemnt(params)),
  editWrmReport:(id,params)=>dispatch(WrmReportEditAction.editWrmReportRequest(id,params)),
  getWrmOperationDetails: (id)=>dispatch(WrmOperationManagementDetailActions.getWrmOperationDetail(id)),

})
  
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WrmReportEdit)