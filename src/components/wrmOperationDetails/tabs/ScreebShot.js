import React, { Fragment } from 'react'
import _ from 'lodash'
import { toAbsoluteUrl } from '../../../theme/helpers'
import UploadImage from '../UploadImage'

function ScreenShot(props) {
  const {
  WrmOperationManagementDetails,
  } = props
  return (
    <>
         <div className='row g-5 g-xl-8 mb-8 xxl-8' >
           <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                Image Urls
                </span>
              </h3>
            </div>
            {WrmOperationManagementDetails?.workStatus==="IN PROGRESS" && WrmOperationManagementDetails.taskQAAccepted&& <UploadImage
              id={WrmOperationManagementDetails.wrmId._id}
              /> }
             
            <div className='card-body pt-0'>
                 <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Logo
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.logo}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.logo : 'No Data'}
                     </a>                    
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Terms & Condition
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.termsAndConditionPageScreenshot}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.termsAndConditionPageScreenshot : 'No Data'}
                       </a>
                    </span>
                  </div>
                </div>
              </div>
              
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Privacy Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                    <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.privacyPolicyPageScreenshot}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.privacyPolicyPageScreenshot : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Shipping Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                    <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.shippingPolicyPageScreenshot}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.shippingPolicyPageScreenshot : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Return Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.returnPolicyPageScreenshot}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.returnPolicyPageScreenshot : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Contact Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.contactUsPageScreenshot}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.contactUsPageScreenshot : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  URLs
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
        
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Terms & Condition
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                  <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.termsAndConditionPageUrl}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.termsAndConditionPageUrl : 'No Data'}
                    </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Privacy Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.privatePolicyPageUrl}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.privatePolicyPageUrl : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Shipping Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.shippingPolicyPageUrl}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.shippingPolicyPageUrl : 'No Data'}
                      </a>
                     </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Return Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.returnPolicyPageUrl}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.returnPolicyPageUrl : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Contact us url
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 '>
                      <a className='color-primary cursor-pointer' onClick={() => window.open(`${WrmOperationManagementDetails?.report?.contactUsPageUrl}`, "_blank")}>
                      {WrmOperationManagementDetails && WrmOperationManagementDetails.report ? WrmOperationManagementDetails.report.contactUsPageUrl : 'No Data'}
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
</div>
      <div className='row g-5 g-xl-8 mb-8' >
         <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Logo
                </span>
              </h3> 
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(WrmOperationManagementDetails?.report?.logo) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={WrmOperationManagementDetails?.report?.logo
                            ?WrmOperationManagementDetails?.report?.logo : '--'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Terms and Conditions Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Terms and Conditions
                </span>
              </h3> 
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(WrmOperationManagementDetails?.report?.termsAndConditionPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={WrmOperationManagementDetails?.report?.termsAndConditionPageScreenshot
                            ?WrmOperationManagementDetails?.report?.termsAndConditionPageScreenshot : '--'
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Terms and Conditions Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Privacy Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(WrmOperationManagementDetails?.report?.privatePolicyPageScreenshot || WrmOperationManagementDetails?.report?.privacyPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={WrmOperationManagementDetails?.report?.privatePolicyPageScreenshot ? WrmOperationManagementDetails?.report?.privatePolicyPageScreenshot :
                            WrmOperationManagementDetails?.report?.privacyPolicyPageScreenshot
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Privacy Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Shipping Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(WrmOperationManagementDetails.report && WrmOperationManagementDetails.report.shippingPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                           src={WrmOperationManagementDetails.report && WrmOperationManagementDetails.report.shippingPolicyPageScreenshot
                            ? WrmOperationManagementDetails.report.shippingPolicyPageScreenshot : '--'}
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : ( 
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Shipping Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Returns Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(WrmOperationManagementDetails.report && WrmOperationManagementDetails.report.returnPolicyPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={WrmOperationManagementDetails.report && WrmOperationManagementDetails.report.returnPolicyPageScreenshot
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Returns Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Contact Policy
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center  rounded p-2 mb-0'>
                <div className='row'>
                  <div className='col-lg-12 col-md-12 col-sm-12'>
                    {
                      !_.isEmpty(WrmOperationManagementDetails.report && WrmOperationManagementDetails.report.contactUsPageScreenshot) ? (
                        <img
                          className='w-100'
                          referrerpolicy="no-referrer"
                          src={WrmOperationManagementDetails.report && WrmOperationManagementDetails.report.contactUsPageScreenshot
                          }
                          onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        />
                      ) : (
                        <span className='text-muted fw-semibold text-gray-700 ms-8'>
                          Contact Policy Page Not Found
                        </span>
                      )
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ScreenShot