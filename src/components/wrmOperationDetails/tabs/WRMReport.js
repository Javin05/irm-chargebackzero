import wrmOperationDetails from ".."
import EditReport from "../EditReport"
function WRMReport(props) {
  const {
    WrmOperationManagementDetails,
    exportclientReports
  } = props
const data = [
    {
        "name": "Web Url",
        "value": `${WrmOperationManagementDetails?.report?.webUrl}`,
         "show": "NO"
    },
    {
        "name": "Tag",
        "value": `${WrmOperationManagementDetails?.report?.tag}`,
         "show": "NO"
    },
    {
        "name": "Status",
        "value": `${WrmOperationManagementDetails?.report?.status}`,
         "show": "NO"
    },
    {
        "name": "Reason",
        "value": `${WrmOperationManagementDetails?.report?.reason}`,
         "show": "NO"
    },
    {
        "name": "Acquirer",
        "value": `${WrmOperationManagementDetails?.report?.acquirer}`,
         "show": "NO"
    },
    {
        "name": "Risk Classification",
        "value": `${WrmOperationManagementDetails?.report?.riskClassification}`,
         "show": "NO"
    },
    {
        "name": "Risk Level",
        "value": `${WrmOperationManagementDetails?.report?.riskLevel}`,
         "show": "NO"
    },
    {
        "name": "Level1 Status",
        "value": `${WrmOperationManagementDetails?.report?.level1Status}`,
         "show": "NO"
    },
    {
        "name": "Level1 Category",
        "value": `${WrmOperationManagementDetails?.report?.levelCategory}`,
         "show": "NO"
    },
    {
        "name": "Level2 Status",
        "value": `${WrmOperationManagementDetails?.report?.level2Status}`,
         "show": "NO"
    },
    {
        "name": "Level2 Category",
        "value": `${WrmOperationManagementDetails?.report?.level2Category}`,
         "show": "NO"
    },
     {
        "name": "Mcc Code Input",
        "value": `${WrmOperationManagementDetails?.report?.mccCodeInput}`,
         "show": "NO"
    },
    {
        "name": "Mcc Code Match",
        "value": `${WrmOperationManagementDetails?.report?.mccCodeMatch}`,
         "show": "NO"
    },
    {
        "name": "Mcc Code Scrapped",
        "value": `${WrmOperationManagementDetails?.report?.productCategoryCode}`,
         "show": "NO"
    },
    {
        "name": "Website Working?",
        "value": `${WrmOperationManagementDetails?.report?.websiteWorking}`,
         "show": "NO"
    },
    {
        "name": "Legal Name",
        "value": `${WrmOperationManagementDetails?.report?.legalName}`,
         "show": "NO"
    },
    {
        "name": "Legal Name Input",
        "value": `${WrmOperationManagementDetails?.report?.legalNameInput}`,
         "show": "NO"
    },
    {
        "name": "Legal Name Scrapped",
        "value": `${WrmOperationManagementDetails?.report?.legalNameScrapped}`,
         "show": "NO"
    },
    {
        "name": "Legal Name Match",
        "value": `${WrmOperationManagementDetails?.report?.legalNameMatch}`,
         "show": "NO"
    },
    {
        "name": "Suspicious Domain",
        "value": `${WrmOperationManagementDetails?.report?.suspiciousDomain}`,
         "show": "NO"
    },
    {
        "name": "Malware Present",
        "value": `${WrmOperationManagementDetails?.report?.malwarePresent}`,
         "show": "NO"
    },
    {
        "name": "Malware Risk",
        "value": `${WrmOperationManagementDetails?.report?.malwareRisk}`,
         "show": "NO"
    },
    {
        "name": "Domain Registered",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistered}`,
         "show": "NO"
    },
    {
        "name": "Domain Registration Company",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrationCompany}`,
         "show": "NO"
    },
    {
        "name": "Domain Registration Date",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrationDate}`,
         "show": "NO"
    },
    {
        "name": "Domain Registration Expiry Date",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrationExpiryDate}`,
         "show": "NO"
    },
    {
        "name": "Domain Registrar Risk",
        "value": `${WrmOperationManagementDetails?.report?.domainRegistrarRisk}`,
         "show": "NO"
    },
    {
        "name": "Ssl Certificate Check",
        "value": `${WrmOperationManagementDetails?.report?.sslCertificateCheck}`,
         "show": "NO"
    },
    {
        "name": "Adult Content Monitoring",
        "value": `${WrmOperationManagementDetails?.report?.adultContentMonitoring}`,
         "show": "NO"
    },
    {
        "name": "Adult Content Monitoring Keyword",
        "value": `${WrmOperationManagementDetails?.report?.adultContentMonitoringKeyword}`,
         "show": "NO"
    },
    {
        "name": "Product Category",
        "value": `${WrmOperationManagementDetails?.report?.productCategory}`,
         "show": "NO"
    },
    {
        "name": "Merchant Policy Link Work",
        "value": `${WrmOperationManagementDetails?.report?.merchantPolicyLinkWork}`,
         "show": "NO"
    },
    {
        "name": "Negative Keywords",
        "value": `${WrmOperationManagementDetails?.report?.negativeKeywords}`,
         "show": "NO"
    },
    {
        "name": "Readiness",
        "value": `${WrmOperationManagementDetails?.report?.readiness}`,
         "show": "NO"
    },
    {
        "name": "Transparency",
        "value": `${WrmOperationManagementDetails?.report?.transparency}`,
         "show": "NO"
    },
    {
        "name": "Contact Details Phone",
        "value": `${WrmOperationManagementDetails?.report?.contactDetailsPhone}`,
         "show": "NO"
    },
    {
        "name": "Contact Details Email",
        "value": `${WrmOperationManagementDetails?.report?.contactDetailsEmail}`,
         "show": "NO"
    },
    {
        "name": "Purchase Or Registration",
        "value": `${WrmOperationManagementDetails?.report?.purchaseOrRegistration}`,
         "show": "NO"
    },
    {
        "name": "Merchant Intelligence",
        "value": `${WrmOperationManagementDetails?.report?.merchantIntelligence}`,
         "show": "NO"
    },
    {
        "name": "Merchant Intelligence Rating",
        "value": `${WrmOperationManagementDetails?.report?.merchantIntelligenceRating}`,
         "show": "NO"
    },
    {
        "name": "Logo",
        "value": `${WrmOperationManagementDetails?.report?.logo}`,
         "show": "NO"
    },
    {
        "name": "Ip Address Of Server",
        "value": `${WrmOperationManagementDetails?.report?.ipAddressOfServer}`,
         "show": "NO"
    },
    {
        "name": "Suggestion",
        "value": `${WrmOperationManagementDetails?.report?.suggestion}`,
         "show": "NO"
    },
    {
        "name": "Content Repetition",
        "value": `${WrmOperationManagementDetails?.report?.contentRepetition}`,
         "show": "NO"
    },
    {
        "name": "Default Template like Loreal ipsum",
        "value": `${WrmOperationManagementDetails?.report?.contentKeywordResults}`,
         "show": "NO"
    },
    {
        "name": "Business Category",
        "value": `${WrmOperationManagementDetails?.report?.businessCategory}`,
         "show": "NO"
    },
    {
        "name": "Parked Domain",
        "value": `${WrmOperationManagementDetails?.report?.parkedDomain}`,
         "show": "NO"
    },
    {
        "name": "Spamming",
        "value": `${WrmOperationManagementDetails?.report?.spamming}`,
         "show": "NO"
    },
    {
        "name": "Http Status Code",
        "value": `${WrmOperationManagementDetails?.report?.httpStatusCode}`,
         "show": "NO"
    },
    {
        "name": "Page Size",
        "value": `${WrmOperationManagementDetails?.report?.pageSize}`,
         "show": "NO"
    },
    {
        "name": "Content Type",
        "value": `${WrmOperationManagementDetails?.report?.contentType}`,
         "show": "NO"
    },
    {
        "name": "Web Server",
        "value": `${WrmOperationManagementDetails?.report?.webServer}`,
         "show": "NO"
    },
    {
        "name": "Dns Sec Org",
        "value": `${WrmOperationManagementDetails?.report?.dnsSecOrg}`,
         "show": "NO"
    },
    {
        "name": "Domain Aliases",
        "value": `${WrmOperationManagementDetails?.report?.domainAliases}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact State",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactState}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact City",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactCity}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Country",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactCountry}`,
         "show": "NO"
    },
    {
        "name": "Administrative Country Code",
        "value": `${WrmOperationManagementDetails?.report?.administrativeCountryCode}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Email",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactEmail}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Name",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactName}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Telephone",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactTelephone}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Organization",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactOrganization}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Postal Code",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactPostalCode}`,
         "show": "NO"
    },
    {
        "name": "Administrative Contact Street1",
        "value": `${WrmOperationManagementDetails?.report?.administrativeContactStreet1}`,
         "show": "NO"
    },
    {
        "name": "Estimated Domain Age",
        "value": `${WrmOperationManagementDetails?.report?.estimatedDomainAge}`,
         "show": "NO"
    },
    {
        "name": "Domain Registered Date",
        "value": `${WrmOperationManagementDetails?.report?.domainRegisteredDate}`,
         "show": "NO"
    },
    {
        "name": "Domain Expiry Date",
        "value": `${WrmOperationManagementDetails?.report?.domainExpiryDate}`,
         "show": "NO"
    },
    {
        "name": "Domain Active For",
        "value": `${WrmOperationManagementDetails?.report?.domainActiveFor}`,
         "show": "NO"
    },
    {
        "name": "Domain Expiring In",
        "value": `${WrmOperationManagementDetails?.report?.domainExpiringin}`,
         "show": "NO"
    },
    {
        "name": "Geo City",
        "value": `${WrmOperationManagementDetails?.report?.geoCity}`,
         "show": "NO"
    },
    {
        "name": "Geo Country",
        "value": `${WrmOperationManagementDetails?.report?.geoCountry}`,
         "show": "NO"
    },
    {
        "name": "Geo Country Code",
        "value": `${WrmOperationManagementDetails?.report?.geoCountryCode}`,
         "show": "NO"
    },
    {
        "name": "Geo Latitude",
        "value": `${WrmOperationManagementDetails?.report?.geoLatitude}`,
         "show": "NO"
    },
    {
        "name": "Geo Longitude",
        "value": `${WrmOperationManagementDetails?.report?.geoLongitude}`,
         "show": "NO"
    },
    {
        "name": "Geo Postal Code",
        "value": `${WrmOperationManagementDetails?.report?.geoPostalCode}`,
         "show": "NO"
    },
    {
        "name": "Geo State",
        "value": `${WrmOperationManagementDetails?.report?.geoState}`,
         "show": "NO"
    },
    {
        "name": "Geo State Code",
        "value": `${WrmOperationManagementDetails?.report?.geoStateCode}`,
         "show": "NO"
    },
    {
        "name": "Geo Street Address",
        "value": `${WrmOperationManagementDetails?.report?.geoStreetAddress}`,
         "show": "NO"
    },
    {
        "name": "Geo Street Name",
        "value": `${WrmOperationManagementDetails?.report?.geoStreetName}`,
         "show": "NO"
    },
    {
        "name": "Geo Street Number",
        "value": `${WrmOperationManagementDetails?.report?.geoStreetNumber}`,
         "show": "NO"
    },
    {
        "name": "Page Activity Check Mining",
        "value": `${WrmOperationManagementDetails?.report?.pageActivityCheckMining}`,
         "show": "NO"
    },
    {
        "name": "Page Activity Check Untrusted Downloads",
        "value": `${WrmOperationManagementDetails?.report?.pageActivityCheckUntrustedDownloads}`,
         "show": "NO"
    },
    {
        "name": "Page Availability Check Url Status",
        "value": `${WrmOperationManagementDetails?.report?.pageAvailabilityCheckURLStatus}`,
         "show": "NO"
    },
    {
        "name": "Page Health Check Content Accessibilty",
        "value": `${WrmOperationManagementDetails?.report?.pageHealthCheckContentAccessibilty}`,
         "show": "NO"
    },
    {
        "name": "Page Health Check Page Loading Time",
        "value": `${WrmOperationManagementDetails?.report?.pageHealthCheckPageLoadingTime}`,
         "show": "NO"
    },
    {
        "name": "Page Links Connectivity Check Success Rate",
        "value": `${WrmOperationManagementDetails?.report?.pageLinksConnectivityCheckSuccessRate}`,
         "show": "NO"
    },
    {
        "name": "Page Redirection Check Domain Redirection",
        "value": `${WrmOperationManagementDetails?.report?.pageRedirectionCheckDomainRedirection}`,
         "show": "NO"
    },
    {
        "name": "Malware Monitoring",
        "value": `${WrmOperationManagementDetails?.report?.malwareMonitoring}`,
         "show": "NO"
    },
    {
        "name": "Policy Compliance Status Contactus Page",
        "value": `${WrmOperationManagementDetails?.report?.policyComplianceStatusContactUsPage}`,
         "show": "NO"
    },
    {
        "name": "Policy Contains Valid Phone",
        "value": `${WrmOperationManagementDetails?.report?.policyContainsValidPhone}`,
         "show": "NO"
    },
    {
        "name": "Policy Contains Valid Email",
        "value": `${WrmOperationManagementDetails?.report?.policyContainsValidEmail}`,
         "show": "NO"
    },
    {
        "name": "Contact Form Status",
        "value": `${WrmOperationManagementDetails?.report?.contactsFormStatus}`,
         "show": "NO"
    },
    {
        "name": "Privacy Policy Status",
        "value": `${WrmOperationManagementDetails?.report?.privacyPolicyStatus}`,
         "show": "NO"
    },
    {
        "name": "Return Policy Status",
        "value": `${WrmOperationManagementDetails?.report?.returnPolicyStatus}`,
         "show": "NO"
    },
    {
        "name": "Shipping Policy Status",
        "value": `${WrmOperationManagementDetails?.report?.shippingPolicyStatus}`,
         "show": "NO"
    },
    {
        "name": "Terms And Condition Status",
        "value": `${WrmOperationManagementDetails?.report?.termsAndConditionStatus}`,
         "show": "NO"
    },
    {
        "name": "Website To Business Name Mcc Code Match",
        "value": `${WrmOperationManagementDetails?.report?.websiteToBusinessNameMccCodeMatch}`,
         "show": "NO"
    },
    {
        "name": "Website To Business Name Match Rating",
        "value": `${WrmOperationManagementDetails?.report?.websiteToBusinessNameMatchRating}`,
         "show": "NO"
    },
    {
        "name": "Content Keyword Risk",
        "value": `${WrmOperationManagementDetails?.report?.contentKeywordRisk}`,
         "show": "NO"
    },
    {
        "name": "Ssl Vulnerability Info",
        "value": `${WrmOperationManagementDetails?.report?.sslVulnerabilityInfo}`,
         "show": "NO"
    },
    {
        "name": "Ssl Check Info",
        "value": `${WrmOperationManagementDetails?.report?.sslCheckInfo}`,
         "show": "NO"
    },
    {
        "name": "User Interaction Checks Bank Detail Submission",
        "value": `${WrmOperationManagementDetails?.report?.userInteractionChecksBankDetailSubmission}`,
         "show": "NO"
    },
    {
        "name": "User Interaction Checks Id Proof Submission",
        "value": `${WrmOperationManagementDetails?.report?.userInteractionChecksIdProofSubmission}`,
         "show": "NO"
    },
    {
        "name": "User Interaction Checks Seller Redirection",
        "value": `${WrmOperationManagementDetails?.report?.userInteractionChecksSellerRedirection}`,
         "show": "NO"
    },
    {
        "name": "Predict Medicine",
        "value": `${WrmOperationManagementDetails?.report?.predictMedicine}`,
         "show": "NO"
    },
    {
        "name": "Shopify",
        "value": `${WrmOperationManagementDetails?.report?.shopify}`,
         "show": "NO"
    },
    {
        "name": "Woo Commerce",
        "value": `${WrmOperationManagementDetails?.report?.wooCommerce}`,
         "show": "NO"
    },
    {
        "name": "Safety",
        "value": `${WrmOperationManagementDetails?.report?.safety}`,
         "show": "NO"
    },
    {
        "name": "Suspicious",
        "value": `${WrmOperationManagementDetails?.report?.suspicious}`,
         "show": "NO"
    },
    {
        "name": "Phishing",
        "value": `${WrmOperationManagementDetails?.report?.phishing}`,
         "show": "NO"
    },
    {
        "name": "Domain Age",
        "value": `${WrmOperationManagementDetails?.report?.domainAge}`,
         "show": "NO"
    },
    {
        "name": "Domain Rank",
        "value": `${WrmOperationManagementDetails?.report?.domainRank}`,
         "show": "NO"
    },
    {
        "name": "Business Address",
        "value": `${WrmOperationManagementDetails?.report?.businessAddress}`,
         "show": "NO"
    },
    {
        "name": "Website Tags",
        "value": `${WrmOperationManagementDetails?.report?.websiteTags}`,
         "show": "NO"
    },
    {
        "name": "Facebook Url",
        "value": `${WrmOperationManagementDetails?.report?.facebookURL}`,
         "show": "NO"
    },
    {
        "name": "Facebook Likes",
        "value": `${WrmOperationManagementDetails?.report?.facebookLikes}`,
         "show": "NO"
    },
    {
        "name": "Twitter Url",
        "value": `${WrmOperationManagementDetails?.report?.twitterURL}`,
         "show": "NO"
    },
    {
        "name": "Twitter Followers",
        "value": `${WrmOperationManagementDetails?.report?.twitterFollowers}`,
         "show": "NO"
    },
    {
        "name": "Linkedin Url",
        "value": `${WrmOperationManagementDetails?.report?.linkedInURL}`,
         "show": "NO"
    },
    {
        "name": "Language Detected",
        "value": `${WrmOperationManagementDetails?.report?.languageDetected}`,
         "show": "NO"
    },
    {
        "name": "Contact us Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.contactUsPageScreenshot}`,
         "show": "NO"
    },
    {
        "name": "Privacy Policy Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.privacyPolicyPageScreenshot}`,
         "show": "NO"
    },
    {
        "name": "Shipping Policy Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.shippingPolicyPageScreenshot}`,
         "show": "NO"
    },
    {
        "name": "Return Policy Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.returnPolicyPageScreenshot}`,
         "show": "NO"
    },
    {
        "name": "Terms And Condition Page Screenshot",
        "value": `${WrmOperationManagementDetails?.report?.termsAndConditionPageScreenshot}`,
         "show": "NO"
    },
    {
        "name": "Description",
        "value": `${WrmOperationManagementDetails?.report?.description}`,
         "show": "NO"
    },
    {
        "name": "Risk Category Status",
        "value": `${WrmOperationManagementDetails?.report?.riskCategoryStatus}`,
         "show": "NO"
    },
    {
        "name": "Contact Us Page Url",
        "value": `${WrmOperationManagementDetails?.report?.contactUsPageUrl}`,
         "show": "NO"
    },
    {
        "name": "Privacy Policy Page Url",
        "value": `${WrmOperationManagementDetails?.report?.privacyPolicyPageUrl}`,
         "show": "NO"
    },
    {
        "name": "Shipping Policy Page Url",
        "value": `${WrmOperationManagementDetails?.report?.shippingPolicyPageUrl}`,
         "show": "NO"
    },
    {
        "name": "Return Policy Page Url",
        "value": `${WrmOperationManagementDetails?.report?.returnPolicyPageUrl}`,
         "show": "NO"
    },
    {
        "name": "Terms And Condition Page Url",
        "value": `${WrmOperationManagementDetails?.report?.termsAndConditionPageUrl}`,
         "show": "NO"
    },
    {
        "name": "List Of Negative Keywords Found In Contact Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInContactPolicy}`,
         "show": "NO"
    },
    {
        "name": "List Of Negative Keywords Found In Privacy Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInPrivacyPolicy}`,
         "show": "NO"
    },
    {
        "name": "List Of Negative Keywords Found In Return Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInReturnPolicy}`,
         "show": "NO"
    },
    {
        "name": "List Of Negative Keywords Found In Shipping Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInShippingPolicy}`,
         "show": "NO"
    },
    {
        "name": "List Of Negative Keywords Found In Terms Policy",
        "value": `${WrmOperationManagementDetails?.report?.listOfNegativeKeywordsFoundInTermsPolicy}`,
         "show": "NO"
    },
    {
        "name": "Merchant Address Being Mapped With Existing Urls",
        "value": `${WrmOperationManagementDetails?.report?.marchentAddressScrappedFromWebsite}`,
         "show": "NO"
    },
    {
        "name": "Emails Mapped With Emails From Existing Urls",
        "value": `${WrmOperationManagementDetails?.report?.emailsMappedWithEmailsFromExistingUrls}`,
         "show": "NO"
    },
    {
        "name": "Match Found With Existing Emails In System",
        "value": `${WrmOperationManagementDetails?.report?.matchFoundWithExistingEmailsInSystem}`,
         "show": "NO"
    },
    {
        "name": "Match Found With Phone Numbers In System",
        "value": `${WrmOperationManagementDetails?.report?.matchFoundWithPhoneNumbersInSystem}`,
         "show": "NO"
    },
    {
        "name": "Phone Numbers Mapped With Phone Numbers From Existing Urls",
        "value": `${WrmOperationManagementDetails?.report?.phoneNumbersMappedWithPhoneNumbersFromExistingUrls}`,
         "show": "NO"
    },
    {
        "name": "Whitelisted Keywords Found In Privacy Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInPrivacyPolicy}`,
         "show": "NO"
    },
    {
        "name": "Whitelisted Keywords Found In Return Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInReturnPolicy}`,
         "show": "NO"
    },
    {
        "name": "Whitelisted Keywords Found In Shipping Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInShippingPolicy}`,
         "show": "NO"
    },
    {
        "name": "Whitelisted Keywords Found In Terms And Conditions Policy",
        "value": `${WrmOperationManagementDetails?.report?.whitelistedKeywordsFoundInTermsAndConditionsPolicy}`,
         "show": "NO"
    },
    {
        "name": "Currencies Found On Website",
        "value": `${WrmOperationManagementDetails?.report?.currenciesFoundOnWebsite}`,
         "show": "NO"
    },
    {
        "name": "Currencies Found Or Not",
        "value": `${WrmOperationManagementDetails?.report?.curreciesFoundOrNot}`,
         "show": "NO"
    },
    {
        "name": "Max Price Listed",
        "value": `${WrmOperationManagementDetails?.report?.maxPriceListedInHomePage}`,
         "show": "NO"
    },
    {
        "name": "Min Price Listed",
        "value": `${WrmOperationManagementDetails?.report?.minPriceListedInHomePage}`,
         "show": "NO"
    },
    {
        "name": "Average Product Price",
        "value": `${WrmOperationManagementDetails?.report?.averageProductPrice}`,
         "show": "NO"
    },
    {
        "name": "Product Price Page Links",
        "value": `${WrmOperationManagementDetails?.report?.productPricePageLinks}`,
         "show": "NO"
    },
    {
        "name": "Website Contains Unreasonable Price",
        "value": `${WrmOperationManagementDetails?.report?.websiteContainsUnreasonablePrice}`,
         "show": "NO"
    },
    {
        "name": "Website Is Accessible Without Login Prompt",
        "value": `${WrmOperationManagementDetails?.report?.websiteIsAccessibleWithoutLoginPrompt}`,
         "show": "NO"
    },
    {
        "name": "Unreasonable Offers",
        "value": `${WrmOperationManagementDetails?.report?.unreasonableOffers}`,
         "show": "NO"
    },
    {
        "name": "Merchant Address Already Present",
        "value": `${WrmOperationManagementDetails?.report?.merchantAddressAlreadyPresent}`,
         "show": "NO"
    },
    {
        "name": "Website Redirection",
        "value": `${WrmOperationManagementDetails?.report?.websiteRedirection}`,
         "show": "NO"
    },
    {
        "name": "Website Redirection Url",
        "value": `${WrmOperationManagementDetails?.report?.websiteRedirectionURL}`,
         "show": "NO"
    },
    {
        "name": "Blacklist Advance Keyword",
        "value": `${WrmOperationManagementDetails?.report?.blackListAdvanceKeyword}`,
         "show": "NO"
    },
    {
        "name": "Website Reachability Description",
        "value": `${WrmOperationManagementDetails?.report?.websiteReachabilityDescription}`,
         "show": "NO"
    },
    {
        "name": "Domain Count",
        "value": `${WrmOperationManagementDetails?.report?.domainCount}`,
         "show": "NO"
    },
    {
        "name": "Reverse Ip Dns Analysis",
        "value": `${WrmOperationManagementDetails?.report?.reverseIpDnsAnalysis}`,
         "show": "NO"
    },
    {
        "name": "Orginal Brand Name",
        "value": `${WrmOperationManagementDetails?.report?.orginalBrandName}`,
         "show": "NO"
    },
    {
        "name": "Email Reachable",
        "value": `${WrmOperationManagementDetails?.report?.emailReachable}`,
         "show": "NO"
    },
    {
        "name": "Email Disposable",
        "value": `${WrmOperationManagementDetails?.report?.emailDisposable}`,
         "show": "NO"
    },
    {
        "name": "Mail Accepts",
        "value": `${WrmOperationManagementDetails?.report?.mailAccepts}`,
         "show": "NO"
    },
    {
        "name": "Email Records",
        "value": `${WrmOperationManagementDetails?.report?.emailRecords}`,
         "show": "NO"
    },
    {
        "name": "Email Smtp Connect",
        "value": `${WrmOperationManagementDetails?.report?.emailSmtpConnect}`,
         "show": "NO"
    },
    {
        "name": "Email Catch All",
        "value": `${WrmOperationManagementDetails?.report?.emailCatchAll}`,
         "show": "NO"
    },
    {
        "name": "Email Deliverable",
        "value": `${WrmOperationManagementDetails?.report?.emailDeliverable}`,
         "show": "NO"
    },
    {
        "name": "Email Disabled",
        "value": `${WrmOperationManagementDetails?.report?.emailDisabled}`,
         "show": "NO"
    },
    {
        "name": "Email Domain",
        "value": `${WrmOperationManagementDetails?.report?.emailDomain}`,
         "show": "NO"
    },
    {
        "name": "Email Valid Syntax",
        "value": `${WrmOperationManagementDetails?.report?.emailValidSyntax}`,
         "show": "NO"
    },
    {
        "name": "Address Scrapped From Website",
        "value": `${WrmOperationManagementDetails?.report?.addressScrappedFromWebsite}`,
         "show": "NO"
    },
    {
        "name": "Address From Google Map",
        "value": `${WrmOperationManagementDetails?.report?.addressFromGoogleMap}`,
         "show": "NO"
    },
    {
        "name": "Matched Address From Google Map",
        "value": `${WrmOperationManagementDetails?.report?.matchedAddressFromGoogleMap}`,
         "show": "NO"
    },
    {
        "name": "Blacklisted Address Pincode",
        "value": `${WrmOperationManagementDetails?.report?.blacklistedAddressPinCode}`,
         "show": "NO"
    },
    {
        "name": "Preshared Address",
        "value": `${WrmOperationManagementDetails?.report?.presharedAddress}`,
         "show": "NO"
    },
    {
        "name": "Banned Category",
        "value": `${WrmOperationManagementDetails?.report?.bannedCategory}`,
         "show": "NO"
    },
    {
        "name": "Banned Category State",
        "value": `${WrmOperationManagementDetails?.report?.bannedCategoryState}`,
         "show": "NO"
    },
    {
        "name": "Banned Category Status",
        "value": `${WrmOperationManagementDetails?.report?.bannedCategoryStatus}`,
         "show": "NO"
    },
    {
        "name": "Social Media Links",
        "value": `${WrmOperationManagementDetails?.report?.socialMediaLinks}`,
         "show": "NO"
    },
    {
        "name": "Blacklisted Merchant Address",
        "value": `${WrmOperationManagementDetails?.report?.blackListedMerchantAddress}`,
         "show": "NO"
    },
    {
        "name": "Blacklisted Buisness Details",
        "value": `${WrmOperationManagementDetails?.report?.blackListedBuisnessDetails}`,
         "show": "NO"
    },
    {
        "name": "Possible Categories",
        "value": `${WrmOperationManagementDetails?.report?.possibleCategories}`,
         "show": "NO"
    },
    {
        "name": "Is Refurbished",
        "value": `${WrmOperationManagementDetails?.report?.isRefurbished}`,
         "show": "NO"
    },
    {
        "name": "Risk Score",
        "value": `${WrmOperationManagementDetails?.report?.riskScore}`,
         "show": "NO"
    },
    {
        "name": "Category Risk Weightage",
        "value": `${WrmOperationManagementDetails?.report?.categoryRiskWeightage}`,
         "show": "NO"
    },
    {
        "name": "Business Risk",
        "value": `${WrmOperationManagementDetails?.report?.businessRisk}`,
         "show": "NO"
    },
    {
        "name": "Web Content Risk",
        "value": `${WrmOperationManagementDetails?.report?.webContentRisk}`,
         "show": "NO"
    },
    {
        "name": "Transparency Risk",
        "value": `${WrmOperationManagementDetails?.report?.transparencyRisk}`,
         "show": "NO"
    },
    {
        "name": "Website Health",
        "value": `${WrmOperationManagementDetails?.report?.websiteHealth}`,
         "show": "NO"
    },
    {
        "name": "Website Content Checkup Domain Health",
        "value": `${WrmOperationManagementDetails?.report?.websiteContentCheckupDomainHealth}`,
         "show": "NO"
    },
    {
        "name": "Online Website Reputation",
        "value": `${WrmOperationManagementDetails?.report?.onlineWebsiteReputation}`,
         "show": "NO"
    },
    {
        "name": "Web Security",
        "value": `${WrmOperationManagementDetails?.report?.webSecurity}`,
         "show": "NO"
    },
    {
        "name": "Website Is Not Working",
        "value": `${WrmOperationManagementDetails?.report?.websiteIsNotWorking}`,
         "show": "NO"
    },
    {
        "name": "Pricing Is Not Updated",
        "value": `${WrmOperationManagementDetails?.report?.pricingIsNotUpdated}`,
         "show": "NO"
    },
    {
        "name": "Login Credentials Are Required",
        "value": `${WrmOperationManagementDetails?.report?.loginCredentialsAreRequired}`,
         "show": "NO"
    },
    {
        "name": "Pricing Is In Dollars",
        "value": `${WrmOperationManagementDetails?.report?.pricingIsInDollars}`,
         "show": "NO"
    },
    {
        "name": "High Discounts",
        "value": `${WrmOperationManagementDetails?.report?.highDiscounts}`,
         "show": "NO"
    },
    {
        "name": "Missing Policy Links",
        "value": `${WrmOperationManagementDetails?.report?.missingPolicyLinks}`,
         "show": "NO"
    },
    {
        "name": "Website Redirection Pma",
        "value": `${WrmOperationManagementDetails?.report?.websiteRedirectionPMA}`,
         "show": "NO"
    },
    {
        "name": "Page Navigation Issue",
        "value": `${WrmOperationManagementDetails?.report?.pageNavigationIssue}`,
         "show": "NO"
    },
    {
        "name": "Pma Flag",
        "value": `${WrmOperationManagementDetails?.report?.pmaFlag}`,
         "show": "NO"
    },
    {
        "name": "Pma List",
        "value": `${WrmOperationManagementDetails?.report?.pmaList}`,
         "show": "NO"
    },
    {
        "name": "All Violations",
        "value": `${WrmOperationManagementDetails?.report?.allViolations}`,
         "show": "NO"
    },
    {
        "name": "Google Analytics Id Relation",
        "value": `${WrmOperationManagementDetails?.report?.googleAnalyticsIdRelation}`,
         "show": "NO"
    },
    {
        "name": "Paid Up Capital",
        "value": `${WrmOperationManagementDetails?.report?.paidUpCapital}`,
         "show": "NO"
    },
    {
        "name": "Product Pricing Home Page URL",
        "value": `${WrmOperationManagementDetails?.report?.productPricingHomePageUrl}`,
         "show": "NO"
    },
    {
        "name": "Product Pricing Product Page URL",
        "value": `${WrmOperationManagementDetails?.report?.productPricingProductPageUrl}`,
         "show": "NO"
    },
    {
        "name": "Email in Blacklisted",
        "value": `${WrmOperationManagementDetails?.report?.email_in_blacklisted}`,
         "show": "NO"
    },
    {
        "name": "Phone in Blacklisted",
        "value": `${WrmOperationManagementDetails?.report?.phone_in_blacklisted}`,
         "show": "NO"
    },
    {
        "name": "UPI in Blacklisted",
        "value": `${WrmOperationManagementDetails?.report?.upi_in_blacklisted}`,
         "show": "NO"
    },
    {
        "name": "Rejection Reason",
        "value": `${WrmOperationManagementDetails?.report?.rejection_reason}`,
         "show": "NO"
    },
    {
        "name": "Is Account Linked",
        "value": `${WrmOperationManagementDetails?.report?.is_account_linked}`,
         "show": "NO"
    },
    {
        "name": "Linked Accounts",
        "value": `${WrmOperationManagementDetails?.report?.linked_accounts}`,
         "show": "NO"
    },
    {
        "name": "No Accounts Linked",
        "value": `${WrmOperationManagementDetails?.report?.no_accounts_linked}`,
         "show": "NO"
    },
    {
        "name": "Linked Cases",
        "value": `${WrmOperationManagementDetails?.report?.linked_cases}`,
         "show": "NO"
    },
    {
        "name": "Linked to Rejected Cases",
        "value": `${WrmOperationManagementDetails?.report?.linked_to_rejected_cases}`,
         "show": "NO"
    },
    {
        "name": "No of Accounts Email",
        "value": `${WrmOperationManagementDetails?.report?.no_of_accounts_email}`,
         "show": "NO"
    },
    {
        "name": "No of Accounts Phone",
        "value": `${WrmOperationManagementDetails?.report?.no_of_accounts_phone}`,
         "show": "NO"
    },
    {
        "name": "No of Accounts UPI",
        "value": `${WrmOperationManagementDetails?.report?.no_of_accounts_upi}`,
         "show": "NO"
    },
    {
        "name": "Blacklist Status",
        "value": `${WrmOperationManagementDetails?.report?.blacklistStatus}`,
         "show": "NO"
    },
    {
        "name": "Email - Input",
        "value": `${WrmOperationManagementDetails?.report?.emailInput}`,
         "show": "NO"
    },
    {
        "name": "Phone - Input",
        "value": `${WrmOperationManagementDetails?.report?.phoneInput}`,
         "show": "NO"
    },
    {
        "name": "UPI - Input",
        "value": `${WrmOperationManagementDetails?.report?.upiInput}`,
         "show": "NO"
    },
    {
        "name": "Billing Descriptor",
        "value": `${WrmOperationManagementDetails?.report?.billingDescriptor}`,
         "show": "NO"
    },
    {
        "name": "Merchant DBA",
        "value": `${WrmOperationManagementDetails?.report?.merchantDBA}`,
         "show": "NO"
    },
    {
        "name": "ID 1",
        "value": `${WrmOperationManagementDetails?.report?.id1}`,
         "show": "NO"
    },
    {
        "name": "Master Card ICA",
        "value": `${WrmOperationManagementDetails?.report?.masterCardICA}`,
         "show": "NO"
    },
    {
        "name": "Visa BIN",
        "value": `${WrmOperationManagementDetails?.report?.visaBIN}`,
         "show": "NO"
    },
    {
        "name": "City",
        "value": `${WrmOperationManagementDetails?.report?.city}`,
         "show": "NO"
    },
    {
        "name": "State",
        "value": `${WrmOperationManagementDetails?.report?.state}`,
         "show": "NO"
    },
    {
        "name": "Country",
        "value": `${WrmOperationManagementDetails?.report?.country}`,
         "show": "NO"
    },
    {
        "name": "Zip",
        "value": `${WrmOperationManagementDetails?.report?.zip}`,
         "show": "NO"
    },
    {
        "name": "Primary Merchant Contact Name",
        "value": `${WrmOperationManagementDetails?.report?.primaryMerchantContactName}`,
         "show": "NO"
    },
    {
        "name": "Products Services Description",
        "value": `${WrmOperationManagementDetails?.report?.productsServicesDescription}`,
         "show": "NO"
    },
    {
        "name": "Mirror Website - Home Page",
        "value": `${WrmOperationManagementDetails?.report?.mirrorHomePage}`,
         "show": "NO"
    },
    {
        "name": "Mirror Website - About us",
        "value": `${WrmOperationManagementDetails?.report?.mirrorAboutUsPage}`,
         "show": "NO"
    },
    {
        "name": "Facebook - Followers",
        "value": `${WrmOperationManagementDetails?.report?.facebookFollowers}`,
         "show": "NO"
    },
    {
        "name": "Facebook - Likes",
        "value": `${WrmOperationManagementDetails?.report?.facebookTotalLikes}`,
         "show": "NO"
    },
    {
        "name": "Facebook - Joined On",
        "value": `${WrmOperationManagementDetails?.report?.facebookJoinedOn}`,
         "show": "NO"
    },
    {
        "name": "Instagram - Followers",
        "value": `${WrmOperationManagementDetails?.report?.instagramFollowers}`,
         "show": "NO"
    },
    {
        "name": "Instagram - Post",
        "value": `${WrmOperationManagementDetails?.report?.instagramPost}`,
         "show": "NO"
    },
    {
        "name": "Youtube - Subscribers",
        "value": `${WrmOperationManagementDetails?.report?.youtubeSubscribers}`,
         "show": "NO"
    },
    {
        "name": "Youtube - Videos",
        "value": `${WrmOperationManagementDetails?.report?.youtubeVideos}`,
         "show": "NO"
    },
    {
        "name": "Youtube - Joined On",
        "value": `${WrmOperationManagementDetails?.report?.youtubeJoinedOn}`,
         "show": "NO"
    },
    {
        "name": "Linked In - Followers",
        "value": `${WrmOperationManagementDetails?.report?.linkedFollowers}`,
         "show": "NO"
    },
    {
        "name": "Twitter - Followers",
        "value": `${WrmOperationManagementDetails?.report?.twitterTotalFollowers}`,
         "show": "NO"
    },
    {
        "name": "Twitter -Joined On",
        "value": `${WrmOperationManagementDetails?.report?.twitterJoinedOn}`,
         "show": "NO"
    },
    {
        "name": "Pinterest - Followers",
        "value": `${WrmOperationManagementDetails?.report?.pinterestFollowers}`,
         "show": "NO"
    }
]
exportclientReports?.data.map(ele=>{ 
let objIndex = data.findIndex(obj=>obj.name===ele.report_value)
if(objIndex >= 0 && ele.is_enable==="YES"){
    data[objIndex].show = "YES"
}
})
console.log(exportclientReports?.data.filter(ele=>ele.report_type==="Contactus Page Screenshot"))
let showData =data.filter(ele=>ele.show=="YES")
let total = data.filter(ele=>ele.show=="YES").length
let average = Math.ceil(total/2)
  return (
    <>
      <div className='row g-5 g-xl-8 mb-8 xl-8 xl-8' >
        <div className='col-xl-6'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-body pt-0">
            {WrmOperationManagementDetails?.workStatus === "IN PROGRESS" &&WrmOperationManagementDetails.taskQAAccepted&& <EditReport report = {WrmOperationManagementDetails.report} id={WrmOperationManagementDetails._id}/>}
             {showData?.map(item=>      item.show ==="YES"&&
             <div className="align-items-center rounded p-2 mb-0 ms-12">
              <div className='row' key={item.name}>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                  
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     {item.name}
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                      {item.name === "Web Url"||item.name==="Logo"||item.name==="Contact us Page Screenshot"||item.name==="Contact Us Page Url"||item.name==="Privacy Policy Page Screenshot"||item.name==="Shipping Policy Page Screenshot"||item.name==="Return Policy Page Screenshot" || item.name==="Terms And Condition Page Screenshot"|| item.name ==="Contactus Page Url" ||item.name ==="Privacy Policy Page Url"||item.name==="Shipping Policy Page Url"||item.name === "Product Pricing Home Page URL"||item.name==="Product Pricing Product Page URL"||item.name==="Return Policy Page Url"||item.name==="Terms And Condition Page Url"||item.name==="Product Price Page Links"?
                       <a className='color-primary cursor-pointer' onClick={() => window.open(`${item.value}`, "_blank")}>
                      { item.value }                    
                      </a> : 
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {item.value}                    
                    </span>}
                   
                  </div>
                </div>
              </div>).slice(0,average)}
            </div>
          </div>
        </div>

        <div className='col-xl-6'>
          <div className='card card-xl-stretch mb-xl-12'>
            <div className='card-body pt-0'>
                {showData?.map(item=>item.show==="YES" &&   <div className="align-items-center rounded p-2 mb-0 ms-8">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     {item.name}
                    </span>
                  </div>
                   <div className='col-lg-6 col-md-6 col-sm-6'>
                      {item.name === "Web Url"||item.name==="Logo"||item.name==="Contact us Page Screenshot"||item.name==="Contact Us Page Url"||item.name==="Privacy Policy Page Screenshot"||item.name==="Shipping Policy Page Screenshot"||item.name==="Return Policy Page Screenshot" || item.name==="Terms And Condition Page Screenshot"|| item.name ==="Contactus Page Url" ||item.name ==="Privacy Policy Page Url"||item.name==="Shipping Policy Page Url"||item.name === "Product Pricing Home Page URL"||item.name==="Return Policy Page Url"||item.name==="Terms And Condition Page Url"||item.name==="Product Price Page Links"?
                       <a className='color-primary cursor-pointer' onClick={() => window.open(`${item.value}`, "_blank")}>
                      { item.value }                    
                      </a> : 
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {item.value}                    
                    </span>}
                   
                  </div>
                </div>
              </div>
                  ).slice(average)}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default WRMReport