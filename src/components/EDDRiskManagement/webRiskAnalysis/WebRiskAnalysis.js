import React, { useEffect, useState, useRef } from "react";
import {
  WebAnalysisActions,
  getWebAnalysisActions,
  ManualWebAnalysisActions,
  DeleteWebAnalysisActions,
  EditWebAnalysisActions,
  updateWebAnalysisActions,
  clientIdLIstActions,
  ExportListActions,
  WrmRiskManagementActions,
  PlayStoreExportActions,
  UpdateWebReportActions,
  CategoryStatusActions,
  ExportWRMActions,
  ExportPlaystoreActions,
  ExportAppstoreActions,
  exportClientWebActions,
  exportClientPlayStoreActions,
  AppStoreExportActions,
  exportClientAppStoreActions,
  AddEddRiskManagementActions,
  BusinessTypeActions,
  BusinessAgeActions,
  BusinessNdxActions,
  PaymentInstrumentActions,
} from "../../../store/actions";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
import _ from "lodash";
import { KTSVG } from "../../../theme/helpers";
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  DROPZONE_MESSAGES,
  FILE_CSV_DOCUMENT,
  STATUS_BADGE,
  DATE,
} from "../../../utils/constants";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../../utils/alerts";
import "./styles.css";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Modal from "react-bootstrap/Modal";
import { userValidation, manualValidation, WebValidation } from "./validation";
import { setWebAnalysisData } from "./formData";
import clsx from "clsx";
import SearchList from "./searchList";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import { getLocalStorage, removeLocalStorage } from "../../../utils/helper";
import ReactSelect from "../../../theme/layout/components/ReactSelect";
import color from "../../../utils/colors";
import { CSVLink } from "react-csv";
import FindRole from "../Role";
import { DateSelector } from "../../../theme/layout/components/DateSelector";

function WebRiskAnalysis(props) {
  const {
    className,
    loading,
    getWebAnalysisDispatch,
    BlockListType,
    WebAnalysisDispatch,
    getWebAnalysis,
    postCSVWebAnalysis,
    clearImportDispatch,
    cleargetWebAnalysislistDispatch,
    postManualWebAnalysisDispatch,
    postManualWebAnalysis,
    clearManualWebAnalysisDispatch,
    DeleteWebAnalysisDispatch,
    deleteWebAnalysis,
    DeleteClearDispatch,
    EditWebAnalysisDispatch,
    EditWebAnalysis,
    UpdateWebAnalysisDispatch,
    EditclearWebAnalysis,
    UpdateClearDispatch,
    UpdateWebAnalysis,
    clientIdDispatch,
    clinetIdLists,
    getExportDispatch,
    exportLists,
    exportShow,
    postCSVLoading,
    getWrmRiskManagementlistDispatch,
    Value,
    setexportShow,
    setExportBtn,
    exportBtn,
    exportLoading,
    clearExportListDispatch,
    postManualWebLoading,
    showPma,
    getPlayStoreExportDispatch,
    playStoreExportLoading,
    playStoreExportResponse,
    appStoreExportLoading,
    appStoreExportResponse,
    clearPlayStoreExportDispatch,
    UpdateWebReportDispatch,
    ClearUpdateWebReportDispatch,
    UploadWebloading,
    UpdateWebReportRes,
    GetClientsRes,
    exportCategoryStatusLoading,
    exportCategoryStatusData,
    CategoryStatusDispatch,
    clearCategoryStatusDispatch,
    businessTypeDispatch,
    businessTypeLists,
    businessAgeDispatch,
    businessAgeLists,
    businessNdxDispatch,
    businessNdxLists,
    paymentInstrumentDispatch,
    paymentInstrumentLists,
    exportClientWebDispatch,
    exportclientReports,
    exportClientPlayStoreDispatch,
    exportClientPlayStoreReports,
    setCredFilterParams,
    setFilter,
    limit,
    WrmRiskManagement,
    setSearchParams,
    ExportAppstoreDispatch,
    ClearExportAppstoreDispatch,
    getAppStoreExportDispatch,
    clearAppStoreExportDispatch,
    exportClientAppStoreDispatch,
    exportClientAppStoreReports,
  } = props;

  const ClinetId = JSON.parse(getLocalStorage("CLIENTID"));
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  const ClientName = JSON.parse(getLocalStorage("CLIENTNAME"));
  const paginationSearch = JSON.parse(getLocalStorage("TAG"));
  const [, setData] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [errors, setErrors] = useState({});
  const [error, setError] = useState({});
  const [fileName, setFileName] = useState();
  const [show, setShow] = useState(false);
  const [Webshow, setWebShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [key, setKey] = useState("Individual");
  const [editMode, setEditMode] = useState(false);
  const [currentId, setcurrentId] = useState();
  const [AsigneesOption, setAsignees] = useState();
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState("");
  const [IndidualOption, setIndidualOption] = useState();
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState("");
  const [BusinessTypeOption, setBusinessTypeOption] = useState();
  const [SelectedBusinessTypeOption, setSelectedBusinessTypeOption] = useState("");
  const [BusinessAgeOption, setBusinessAgeOption] = useState();
  const [SelectedBusinessAgeOption, setSelectedBusinessAgeOption] = useState("");
  const [BusinessNdxOption, setBusinessNdxOption] = useState();
  const [SelectedBusinessNdxOption, setSelectedBusinessNdxOption] = useState("");
  const [PaymentInstrumentOption, setPaymentInstrumentOption] = useState();
  const [SelectedPaymentInstrumentOption, setSelectedPaymentInstrumentOption] = useState("");
  const [reportShow, setReportShow] = useState(false);
  const [formData, setFormData] = useState({
    client_id: Role === "Client User" ? ClinetId : "",
    tag: "",
    file: "",
    batchType: "Live",
  });

  const [manualFormData, setManualFormData] = useState({
    client_id: Role === "Client User" ? ClinetId : "",
    website: "",
    merchant_personal_email: "",
    merchant_phone: "",
    acquirer: "",
    legal_name: "",
    business_registered_address: "",
    mcc_code: "",
    merchant_name: "",
    merchant_personal_address: "",
    business_ownership_type: "",
    payment_instrument: "",
    age_of_business: "",
    ndx: "",
    average_monthly_volume: "",
    average_transaction_amount: "",
    cin_number: "",
    pan_number: "",
    gstin_nummber: "",
    product: "",
    bank: "",
    transaction_type: "",
    date_of_incorporation: "",
  });
  const [webFormData, setWebFormData] = useState({
    file: "",
  });
  const [sorting, setSorting] = useState({
    tag: false,
  });

  useEffect(() => {
    // getWebAnalysisDispatch()
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params);
    businessTypeDispatch(params);
    businessAgeDispatch(params);
    businessNdxDispatch(params);
    paymentInstrumentDispatch(params);
    removeLocalStorage("ExportHide");
  }, []);

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: pageNumber,
    };
    setActivePageNumber(pageNumber);
    getWebAnalysisDispatch(params);
  };

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name];
      setSorting(sorting);
      setData({});
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "DESC",
      };
      getWebAnalysisDispatch(params);
    } else {
      const filter = _.mapValues(sorting, () => {
        return false;
      });
      filter[name] = !filter[name];
      setSorting(filter);
      setData({});
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "ASC",
      };
      getWebAnalysisDispatch(params);
    }
  };
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({
      ...manualFormData,
      [e.target.name]: e.target.value,
    }));
    setError({ ...error, [e.target.name]: "" });
  };

  const hiddenFileInput = useRef(null);
  const handleChange = (e) => {
    setFormData((formData) => ({
      ...formData,
      [e.target.name]: e.target.value,
    }));
    setErrors({ ...errors, [e.target.name]: "" });
  };

  const handleFileChange = (e) => {
    e.preventDefault();
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(FILE_CSV_DOCUMENT, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setWebFormData((values) => ({
          ...values,
          file: files,
        }));
        setFormData((values) => ({
          ...values,
          file: files,
        }));
        setErrors((values) => ({ ...values, file: "" }));
        setFileName(files && files.name);
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${
            fileSize / 1048576
          } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID });
    }
  };

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  };

  const OnSubmit = () => {
    const errorMsg = userValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      const data = new FormData();
      data.append("tag", formData.tag);
      data.append("client_id", formData.client_id);
      data.append("file", formData.file);
      data.append("batchType", formData.batchType);
      WebAnalysisDispatch(data);
    }
  };

  const OnWebSubmit = () => {
    const errorMsg = WebValidation(webFormData, setErrors);
    if (_.isEmpty(errorMsg)) {
      const data = new FormData();
      data.append("tag", paginationSearch.tag ? paginationSearch.tag : "");
      data.append("file", webFormData.file);
      UpdateWebReportDispatch(data);
    }
  };

  const onConfirmUpdate = (currentId) => {
    UpdateWebAnalysisDispatch(currentId, manualFormData);
  };

  const handelSubmit = () => {
    const errorMsg = manualValidation(manualFormData, setError);
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        confirmationAlert(
          SWEET_ALERT_MSG.CONFIRMATION_TEXT,
          SWEET_ALERT_MSG.UPDATE_WEB,
          "warning",
          "Yes",
          "No",
          () => {
            onConfirmUpdate(currentId);
          },
          () => {}
        );
      } else {
        postManualWebAnalysisDispatch(manualFormData);
      }
    }
  };

  const clearPopup = () => {
    EditclearWebAnalysis();
    setSelectedIndidualOption("");
    setSelectedBusinessTypeOption("")
    setSelectedBusinessAgeOption("")
    setSelectedBusinessNdxOption("")
    setSelectedPaymentInstrumentOption("")
    setEditMode(false);
    setShow(false);
    setWebShow(false);
    setSelectedAsigneesOption("");
    setErrors({});
    setFileName("");
    setFormData((values) => ({
      ...values,
      file: "",
      tag: "",
    }));
    setManualFormData((values) => ({
      ...values,
      website: "",
      tag: "",
    }));
    setWebFormData((values) => ({
      ...values,
      website: "",
      tag: "",
    }));
  };

  const onConfirm = () => {
    setShow(false);
    setFileName(null);
    cleargetWebAnalysislistDispatch();
    clearImportDispatch();
    clearManualWebAnalysisDispatch();
    setFormData((values) => ({
      ...values,
      file: "",
      tag: "",
    }));
    setManualFormData((values) => ({
      ...values,
      website: "",
      tag: "",
    }));
  };

  const clear = () => {
    setFileName(null);
    cleargetWebAnalysislistDispatch();
    clearImportDispatch();
    clearManualWebAnalysisDispatch();
    setManualFormData({
      website: "",
      tag: "",
    });
    setFormData((values) => ({
      ...values,
      file: "",
      tag: "",
    }));
  };

  useEffect(() => {
    if (show === false) {
      setKey("Individual");
    }
  }, [show]);

  useEffect(() => {
    if (
      postCSVWebAnalysis &&
      postCSVWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      confirmationAlert(
        "success",
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        "success",
        "Back to Web RisK Analysis",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          clear();
        },
        clearImportDispatch()
      );
      // getWebAnalysisDispatch()
      cleargetWebAnalysislistDispatch();
      getWrmRiskManagementlistDispatch();
      setSelectedAsigneesOption("");
      setFileName("");
      clearManualWebAnalysisDispatch();
    } else if (
      postCSVWebAnalysis &&
      postCSVWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        "",
        "Try again",
        "",
        () => {
          clear();
        }
      );
      clearImportDispatch();
      clearManualWebAnalysisDispatch();
    }
  }, [postCSVWebAnalysis]);
  
  useEffect(() => {
    if (
      postManualWebAnalysis &&
      postManualWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      confirmationAlert(
        "success",
        postManualWebAnalysis && postManualWebAnalysis.message,
        "success",
        "Back to Web RisK Analysis",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          clear();
        }
      );
      // getWebAnalysisDispatch()/
      clearManualWebAnalysisDispatch();
      getWrmRiskManagementlistDispatch();
      setSelectedIndidualOption("");
      setSelectedBusinessTypeOption("")
      setSelectedBusinessAgeOption("")
      setSelectedBusinessNdxOption("")
      setSelectedPaymentInstrumentOption("")
      setFileName("");
    } else if (
      postManualWebAnalysis &&
      postManualWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        postManualWebAnalysis && postManualWebAnalysis.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      );
      clearManualWebAnalysisDispatch();
    }
  }, [postManualWebAnalysis]);

  const onConfirmDelete = (id) => {
    DeleteWebAnalysisDispatch(id);
  };
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_WEB,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmDelete(id);
      },
      () => {}
    );
  };

  useEffect(() => {
    if (
      deleteWebAnalysis &&
      deleteWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      successAlert(deleteWebAnalysis && deleteWebAnalysis.message, "success");
      // getWebAnalysisDispatch()
      DeleteClearDispatch();
    } else if (
      deleteWebAnalysis &&
      deleteWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        deleteWebAnalysis && deleteWebAnalysis.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      );
      DeleteClearDispatch();
    }
  }, [deleteWebAnalysis]);

  useEffect(() => {
    if (
      EditWebAnalysis &&
      EditWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      const getByIdData = EditWebAnalysis && EditWebAnalysis.data;
      const data = setWebAnalysisData(getByIdData);
      setManualFormData(data);
    }
  }, [EditWebAnalysis]);

  useEffect(() => {
    if (
      UpdateWebAnalysis &&
      UpdateWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      successAlert(
        // UpdateWebAnalysis && UpdateWebAnalysis.message,
        "Updated Successfully",
        "success"
      );
      // getWebAnalysisDispatch()
      EditclearWebAnalysis();
      UpdateClearDispatch();
      setShow(false);
    } else if (
      UpdateWebAnalysis &&
      UpdateWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        UpdateWebAnalysis && UpdateWebAnalysis.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      );
      DeleteClearDispatch();
    }
  }, [UpdateWebAnalysis]);

  useEffect(() => {
    if (
      UpdateWebReportRes &&
      UpdateWebReportRes.status === STATUS_RESPONSE.SUCCESS_MSG
    ) {
      successAlert(
        // UpdateWebReportRes && UpdateWebReportRes.message,
        "Updated Successfully",
        "success"
      );
      ClearUpdateWebReportDispatch();
      setWebFormData((values) => ({
        ...values,
        file: "",
        tag: "",
      }));
      setWebShow(false);
    } else if (
      UpdateWebReportRes &&
      UpdateWebReportRes.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        UpdateWebReportRes && UpdateWebReportRes.message,
        "",
        "Try again",
        "",
        () => {
          {
          }
        }
      );
      ClearUpdateWebReportDispatch();
    }
  }, [UpdateWebReportRes]);

  const totalPages =
    BlockListType && BlockListType.count
      ? Math.ceil(parseInt(BlockListType && BlockListType.count) / limit)
      : 1;

  const AsigneesNames =
    clinetIdLists && clinetIdLists.data && clinetIdLists.data.result;
  const businessTypeOption =
    businessTypeLists && businessTypeLists.data;
  const businessAgeOption =
    businessAgeLists && businessAgeLists.data;
  const businessNdxOption =
    businessNdxLists && businessNdxLists.data;
  const paymentInstrumentOption =
    paymentInstrumentLists && paymentInstrumentLists.data;
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames);
    setAsignees(Asignees);
    setIndidualOption(Asignees);
  }, [AsigneesNames]);

  useEffect(() => {
    const businessType = getDefaultOptionBusinessType(businessTypeOption);
    setBusinessTypeOption(businessType);
  }, [businessTypeOption]);

  useEffect(() => {
    const businessAge = getDefaultOptionBusinessAge(businessAgeOption);
    setBusinessAgeOption(businessAge);
  }, [businessAgeOption]);

  // useEffect(() => {
  //   const businessNdx = getDefaultOptionBusinessNdx(businessNdxOption);
  //   setBusinessNdxOption(businessNdx);
  // }, [businessNdxOption]);

  useEffect(() => {
    const paymentInstrument = getDefaultOptionPaymentInstrument(paymentInstrumentOption);
    setPaymentInstrumentOption(paymentInstrument);
  }, [paymentInstrumentOption]);


  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = [];
    for (const item in AsigneesNames) {
      defaultOptions.push({
        label: AsigneesNames[item].company,
        value: AsigneesNames[item]._id,
      });
    }
    return defaultOptions;
  };

  const getDefaultOptionBusinessType = (BusinessTypes) => {
    const defaultOptions = [];
    for (const item in BusinessTypes) {
      defaultOptions.push({
        label: BusinessTypes[item].business_type,
        value: BusinessTypes[item]._id,
      });
    }
    return defaultOptions;
  };

  const getDefaultOptionBusinessAge = (BusinessAge) => {
    const defaultOptions = [];
    for (const item in BusinessAge) {
      defaultOptions.push({
        label: BusinessAge[item].business_type,
        value: BusinessAge[item]._id,
      });
    }
    return defaultOptions;
  };

  const getDefaultOptionBusinessNdx = (BusinessAge) => {
    const defaultOptions = [];
    for (const item in BusinessAge) {
      defaultOptions.push({
        label: BusinessAge[item].business_ndx,
        value: BusinessAge[item]._id,
      });
    }
    return defaultOptions;
  };

  const getDefaultOptionPaymentInstrument = (PaymentInstrument) => {
    const defaultOptions = [];
    for (const item in PaymentInstrument) {
      defaultOptions.push({
        label: PaymentInstrument[item].payment_instruments,
        value: PaymentInstrument[item]._id,
      });
    }
    return defaultOptions;
  };

  const customStyles = {
    control: (base) => ({
      ...base,
      borderColor: "white",
      borderRadius: "4px",
      backgroundColor: "#f5f8fa",
      padding: "3px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  };

  const handleChangeAsignees = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption);
      setFormData((values) => ({ ...values, client_id: selectedOption.value }));
    }
  };

  const handleBusinessType = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedBusinessTypeOption(selectedOption);
      setManualFormData((values) => ({
        ...values,
        business_ownership_type: selectedOption.label,
      }));
    }
  };

  const handleBusinessAge = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedBusinessAgeOption(selectedOption);
      setManualFormData((values) => ({
        ...values,
        age_of_business: selectedOption.label,
      }));
    }
  };

  const handlePaymentInstrument = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedPaymentInstrumentOption(selectedOption);
      setManualFormData((values) => ({
        ...values,
        payment_instrument: selectedOption.label,
      }));
    }
  };

  const handleChangeIndidual = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption);
      setManualFormData((values) => ({
        ...values,
        client_id: selectedOption.value,
      }));
    }
  };

  const exportParams = JSON.parse(getLocalStorage("TAG"));
  const exported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      acquirer: exportParams && exportParams.acquirer,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      acquirer: exportParams && exportParams.acquirer,
      riskLevel: exportParams && exportParams.riskLevel,
      client_id: setCredFilterParams.client_id
        ? setCredFilterParams.client_id
        : "",
      batchType: exportParams && exportParams.batchType,
    };
    // ExportWRMDispatch(exportParams && exportParams.tag, "tag")
    getExportDispatch(params);
  };

  const appStoreExported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      acquirer: exportParams && exportParams.acquirer,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      acquirer: exportParams && exportParams.acquirer,
      riskLevel: exportParams && exportParams.riskLevel,
      client_id: setCredFilterParams.client_id
        ? setCredFilterParams.client_id
        : "",
      batchType: exportParams && exportParams.batchType,
    };
    // ExportAppstoreDispatch(exportParams && exportParams.tag, "tag")
    getAppStoreExportDispatch(params);
  };

  const playStorExported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      riskLevel: exportParams && exportParams.riskLevel,
      acquirer: exportParams && exportParams.acquirer,
      client_id: setCredFilterParams.client_id
        ? setCredFilterParams.client_id
        : "",
      batchType: exportParams && exportParams.batchType,
    };
    getPlayStoreExportDispatch(params);
    // ExportPlaystoreDispatch(exportParams && exportParams.tag, "tag")
  };
  useEffect(() => {
    if (GetClientsRes && !_.isEmpty(GetClientsRes && GetClientsRes._id)) {
      exportClientWebDispatch(GetClientsRes && GetClientsRes._id);
      exportClientPlayStoreDispatch(GetClientsRes && GetClientsRes._id);
      exportClientAppStoreDispatch(GetClientsRes && GetClientsRes._id);
    }
  }, [GetClientsRes]);

  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === "ok") {
      if (
        Array.isArray(exportLists && exportLists.data && exportLists.data.data)
      ) {
        const closeXlsx = document.getElementById("bulkCsvReport");
        closeXlsx.click();
        clearExportListDispatch();
      } else if (
        _.isEmpty(exportLists && exportLists.data && exportLists.data.data)
      ) {
        warningAlert(
          "error",
          exportLists && exportLists.data && exportLists.data.message,
          "",
          "Try again",
          "",
          () => {
            clear();
          }
        );
        clearExportListDispatch();
      } else if (
        !_.isEmpty(
          exportLists &&
            exportLists.data &&
            exportLists.data.data &&
            exportLists.data.data.file
        )
      ) {
        const reportData =
          exportLists &&
          exportLists.data &&
          exportLists.data.data &&
          exportLists.data.data.file;
        const data = reportData;
        const link = document.createElement("a");
        const url = window.URL || window.webkitURL;
        const revokeUrlAfterSec = 1000000;
        link.href = data;
        // link.target = "_blank"
        document.body.append(link);
        link.click();
        link.remove();
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec);
        clearExportListDispatch();
      }
    }
  }, [exportLists]);

  useEffect(() => {
    if (
      playStoreExportResponse &&
      playStoreExportResponse.data &&
      playStoreExportResponse.data.status === "ok"
    ) {
      if (
        Array.isArray(
          playStoreExportResponse &&
            playStoreExportResponse.data &&
            playStoreExportResponse.data.data
        )
      ) {
        const closeXlsx = document.getElementById("PlayStoreReport");
        closeXlsx.click();
        clearPlayStoreExportDispatch();
      } else if (
        _.isEmpty(
          playStoreExportResponse &&
            playStoreExportResponse.data &&
            playStoreExportResponse.data.data
        )
      ) {
        warningAlert(
          "error",
          playStoreExportResponse &&
            playStoreExportResponse.data &&
            playStoreExportResponse.data.message,
          "",
          "Try again",
          "",
          () => {
            clear();
          }
        );
        clearPlayStoreExportDispatch();
      } else if (
        !_.isEmpty(
          playStoreExportResponse &&
            playStoreExportResponse.data &&
            playStoreExportResponse.data.data &&
            playStoreExportResponse.data.data.file
        )
      ) {
        const data =
          playStoreExportResponse &&
          playStoreExportResponse.data &&
          playStoreExportResponse.data.data &&
          playStoreExportResponse.data.data.file;
        const link = document.createElement("a");
        const url = window.URL || window.webkitURL;
        const revokeUrlAfterSec = 1000000;
        link.href = data;
        document.body.append(link);
        link.click();
        link.remove();
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec);
        clearPlayStoreExportDispatch();
      }
    }
  }, [playStoreExportResponse]);

  useEffect(() => {
    if (
      appStoreExportResponse &&
      appStoreExportResponse.data &&
      appStoreExportResponse.data.status === "ok"
    ) {
      if (
        Array.isArray(
          appStoreExportResponse &&
            appStoreExportResponse.data &&
            appStoreExportResponse.data.data
        )
      ) {
        const closeXlsx = document.getElementById("AppStoreReport");
        closeXlsx.click();
        clearAppStoreExportDispatch();
      } else if (
        _.isEmpty(
          appStoreExportResponse &&
            appStoreExportResponse.data &&
            appStoreExportResponse.data.data
        )
      ) {
        warningAlert(
          "error",
          appStoreExportResponse &&
            appStoreExportResponse.data &&
            appStoreExportResponse.data.message,
          "",
          "Try again",
          "",
          () => {
            clear();
          }
        );
        clearAppStoreExportDispatch();
      } else if (
        !_.isEmpty(
          appStoreExportResponse &&
            appStoreExportResponse.data &&
            appStoreExportResponse.data.data &&
            appStoreExportResponse.data.data.file
        )
      ) {
        const data =
          appStoreExportResponse &&
          appStoreExportResponse.data &&
          appStoreExportResponse.data.data &&
          appStoreExportResponse.data.data.file;
        const link = document.createElement("a");
        const url = window.URL || window.webkitURL;
        const revokeUrlAfterSec = 1000000;
        link.href = data;
        document.body.append(link);
        link.click();
        link.remove();
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec);
        clearAppStoreExportDispatch();
      }
    }
  }, [appStoreExportResponse]);

  const data = [
    {
      website: "https://payu.in/",
      merchant_personal_email: "nihalelton@gmail.com",
      merchant_phone: 8838678689,
      acquirer: "nihalelton@ybl",
      legal_name: "PayU",
      acquirer: "Test",
      business_registered_address: "",
      mcc_code: "",
      merchant_name: "",
      billingDescriptor: "",
      merchantDBA: "",
      id1: "",
      masterCardICA: "",
      visaBIN: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      mid: "",
      primaryMerchantContactName: "",
      productsServicesDescription: "",
    },
    {
      website: "http://enagic.co.in",
      merchant_personal_email: "sundar@gmail.com",
      merchant_phone: 8003556833,
      acquirer: "sundar@paytm",
      legal_name: "Enagic",
      acquirer: "Test",
      business_registered_address: "",
      mcc_code: "",
      merchant_name: "",
      billingDescriptor: "",
      merchantDBA: "",
      id1: "",
      masterCardICA: "",
      visaBIN: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      mid: "",
      primaryMerchantContactName: "",
      productsServicesDescription: "",
    },
    {
      website: "https://gigie.in/",
      merchant_personal_email: "shiyam.shenll@gmail.com",
      merchant_phone: 7904566977,
      acquirer: "shiyam@okaxis",
      legal_name: "Gigie",
      acquirer: "Test",
      business_registered_address: "",
      mcc_code: "",
      merchant_name: "",
      billingDescriptor: "",
      merchantDBA: "",
      id1: "",
      masterCardICA: "",
      visaBIN: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      mid: "",
      primaryMerchantContactName: "",
      productsServicesDescription: "",
    },
    {
      website: "https://gonuclei.com/",
      merchant_personal_email: "jagannathan.arumugam@mfilterit.com",
      merchant_phone: 9884856553,
      acquirer: "jagan@okaxis",
      legal_name: "Gonuclei",
      acquirer: "Test",
      business_registered_address: "",
      mcc_code: "",
      merchant_name: "",
      billingDescriptor: "",
      merchantDBA: "",
      id1: "",
      masterCardICA: "",
      visaBIN: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      mid: "",
      primaryMerchantContactName: "",
      productsServicesDescription: "",
    },
    {
      website: "https://curlvana.in/",
      merchant_personal_email: "u.deepika@chargebackzero.com",
      merchant_phone: 9677056759,
      acquirer: "deepika@ybl",
      legal_name: "Curlvana",
      acquirer: "Test",
      business_registered_address: "",
      mcc_code: "",
      merchant_name: "",
      billingDescriptor: "",
      merchantDBA: "",
      id1: "",
      masterCardICA: "",
      visaBIN: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      mid: "",
      primaryMerchantContactName: "",
      productsServicesDescription: "",
    },
    {
      website: "https://www.planify.in/",
      merchant_personal_email: "s.sweta@chargebackzero.com",
      merchant_phone: 9790756603,
      acquirer: "sweta@okaxis",
      legal_name: "Planify",
      acquirer: "Test",
      business_registered_address: "",
      mcc_code: "",
      merchant_name: "",
      billingDescriptor: "",
      merchantDBA: "",
      id1: "",
      masterCardICA: "",
      visaBIN: "",
      city: "",
      state: "",
      country: "",
      zip: "",
      mid: "",
      primaryMerchantContactName: "",
      productsServicesDescription: "",
    },
  ];

  const ClientShow = ["KYC User", "Client User", "Phonepe User"];
  const AdminValidate = ["Analyst", "Admin"];

  useEffect(() => {
    if (exportCategoryStatusData && exportCategoryStatusData.status === "ok") {
      if (
        Array.isArray(exportCategoryStatusData && exportCategoryStatusData.data)
      ) {
        const closeXlsx = document.getElementById("categoryCsvReport");
        closeXlsx.click();
        clearCategoryStatusDispatch();
      } else if (
        exportCategoryStatusData &&
        exportCategoryStatusData.status === "error"
      ) {
        warningAlert(
          "error",
          exportCategoryStatusData && exportCategoryStatusData.message,
          "",
          "Try again",
          "",
          () => {
            clear();
          }
        );
        clearCategoryStatusDispatch();
      }
    }
  }, [exportCategoryStatusData]);

  const categoryStatus = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      acquirer: exportParams && exportParams.acquirer,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      acquirer: exportParams && exportParams.acquirer,
      riskLevel: exportParams && exportParams.riskLevel,
      client_id: setCredFilterParams.client_id
        ? setCredFilterParams.client_id
        : "",
      batchType: exportParams && exportParams.batchType,
    };
    CategoryStatusDispatch(params);
  };

  return (
    <>
      <div type="button" className="d-none" data-target="#categoryCsvReport">
        <ReactHTMLTableToExcel
          id="categoryCsvReport"
          className="download-table-xls-button"
          table="categoryCsvModel"
          filename={`${
            exportParams && exportParams.tag
              ? exportParams.tag
              : exportParams.website
          }-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type="button"
        className="d-none"
        data-toggle="modal"
        data-target="#categoryModal"
        onClick={() => {}}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename={`${
            exportParams && exportParams.tag ? exportParams.tag : "IRM"
          }-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type="button"
        className="d-none"
        data-toggle="modal"
        data-target="#categoryModal"
        onClick={() => {}}
      >
        <ReactHTMLTableToExcel
          id="PlayStoreReport"
          className="download-table-xls-button"
          table="PlayStoreReport-table"
          filename={`${
            exportParams && exportParams.tag ? exportParams.tag : "IRM"
          }-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type="button"
        className="d-none"
        // data-toggle='modal'
        // data-target='#categoryModal'
        onClick={() => {}}
      >
        <ReactHTMLTableToExcel
          id="AppStoreReport"
          className="download-table-xls-button"
          table="AppStoreReport-table"
          filename={`${
            exportParams && exportParams.tag ? exportParams.tag : "IRM"
          }-report`}
          sheet="tablexls"
        />
      </div>
      <div className={`card px-2 ${className} w-100`}>
        <div className="card-body py-3">
          <div className="row align-items-center justify-content-end g-3">
            <div className="col-3">
              <SearchList
                setExportBtn={setExportBtn}
                Value={Value}
                setexportShow={setexportShow}
                setReportShow={setReportShow}
                setFilter={setFilter}
                limit={limit}
                setSearchParams={setSearchParams}
              />
            </div>
            {Role !== "Analyst" ? (
              <div className="col-3">
                <button
                  className="btn btn-sm btn-light-primary btn-responsive font-7vw pull-right w-100"
                  onClick={() => {
                    setShow(true);
                    setEditMode(false);
                  }}
                >
                  <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                  Add Case
                </button>
              </div>
            ) : (
              ""
            )}
            {exportShow && Role !== "Analyst" ? (
              WrmRiskManagement && WrmRiskManagement.data ? (
                <>
                  {reportShow ? (
                    <>
                      <div className="col-3">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-success btn-responsive font-5vw w-100 pull-right"
                          onClick={(e) => exported(e)}
                          disabled={exportLoading}
                        >
                          {!exportLoading && (
                            <span className="indicator-label">
                              <i className="bi bi-filetype-csv" />
                              Web Report Export
                            </span>
                          )}
                          {exportLoading && (
                            <span
                              className="indicator-progress text-success"
                              style={{ display: "block" }}
                            >
                              Please wait...
                              <span className="spinner-border spinner-border-sm align-middle ms-2" />
                            </span>
                          )}
                        </button>
                      </div>
                      <div className="col-3">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-success btn-responsive font-5vw w-100 pull-right"
                          onClick={(e) => playStorExported(e)}
                          disabled={playStoreExportLoading}
                        >
                          {!playStoreExportLoading && (
                            <span className="indicator-label">
                              <i className="bi bi-filetype-csv" />
                              Play Store Report Export
                            </span>
                          )}
                          {playStoreExportLoading && (
                            <span
                              className="indicator-progress text-success"
                              style={{ display: "block" }}
                            >
                              Please wait...
                              <span className="spinner-border spinner-border-sm align-middle ms-2" />
                            </span>
                          )}
                        </button>
                      </div>
                      <div className="col-3">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-success btn-responsive font-5vw w-100 pull-right"
                          onClick={(e) => appStoreExported(e)}
                          disabled={appStoreExportLoading}
                        >
                          {!appStoreExportLoading && (
                            <span className="indicator-label">
                              <i className="bi bi-filetype-csv" />
                              App Store Report Export
                            </span>
                          )}
                          {appStoreExportLoading && (
                            <span
                              className="indicator-progress text-success"
                              style={{ display: "block" }}
                            >
                              Please wait...
                              <span className="spinner-border spinner-border-sm align-middle ms-2" />
                            </span>
                          )}
                        </button>
                      </div>
                    </>
                  ) : null}
                  {_.includes(AdminValidate, Role) ? (
                    <>
                      <div className="col-3">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary btn-responsive font-5vw w-100"
                          onClick={(e) => {
                            setWebShow(true);
                          }}
                        >
                          Upload Web Report
                        </button>
                      </div>
                      <div className="col-3">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-success btn-responsive font-5vw w-100"
                          onClick={(e) => categoryStatus(e)}
                          disabled={exportCategoryStatusLoading}
                        >
                          {!exportCategoryStatusLoading && (
                            <span className="indicator-label">
                              {/* <i className="bi bi-filetype-csv" /> */}
                              Download Category Status
                            </span>
                          )}
                          {exportCategoryStatusLoading && (
                            <span
                              className="indicator-progress text-success"
                              style={{ display: "block" }}
                            >
                              Please wait...
                              <span className="spinner-border spinner-border-sm align-middle ms-2" />
                            </span>
                          )}
                        </button>
                      </div>
                    </>
                  ) : null}
                </>
              ) : null
            ) : null}
          </div>
          {/* csv Report */}
          <div
            className="table-responsive"
            style={{
              display: "none",
            }}
          >
            <table className="table" id="categoryCsvModel">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  <th>Website</th>
                  <th>API Category</th>
                  <th>Level1 Category</th>
                  <th>Level2 Category</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {Array.isArray(
                  exportCategoryStatusData && exportCategoryStatusData.data
                )
                  ? exportCategoryStatusData &&
                    exportCategoryStatusData.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          <td>{item.website}</td>
                          <td>
                            {item && item.apiCategory ? item.apiCategory : "--"}
                          </td>
                          <td>
                            {item && item.level1Category
                              ? item.level1Category
                              : "--"}
                          </td>
                          <td>
                            {item && item.level2Category
                              ? item.level2Category
                              : "--"}
                          </td>
                          <td>{item && item.status ? item.status : "--"}</td>
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            </table>
          </div>
          <div
            className="table-responsive"
            style={{
              display: "none",
            }}
          >
            <table className="table" id="table-to-xls">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  {exportclientReports &&
                    exportclientReports.data &&
                    exportclientReports.data.map((item, i) => {
                      return <th key={i}>{item.report_value}</th>;
                    })}
                </tr>
              </thead>
              <tbody className="fs-10">
                {_.isArray(
                  exportLists && exportLists.data && exportLists.data.data
                )
                  ? exportLists &&
                    exportLists.data &&
                    exportLists.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          {exportclientReports &&
                            exportclientReports.data &&
                            exportclientReports.data.map((itemReport, i) => {
                              let reportTdData = itemReport.report_key
                                ? item[itemReport.report_key]
                                : "No Data";
                              return _.isArray(reportTdData) ? (
                                <td>
                                  {item[itemReport.report_key].toString()}
                                </td>
                              ) : (
                                <td>
                                  {item[itemReport.report_key]
                                    ? item[itemReport.report_key]
                                    : "No Data"}
                                </td>
                              );
                            })}
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            </table>
          </div>
          <div
            className="table-responsive"
            style={{
              display: "none",
            }}
          >
            <table className="table" id="PlayStoreReport-table">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  {exportClientPlayStoreReports &&
                    exportClientPlayStoreReports.data.map((item) => {
                      return <th>{item.report_value}</th>;
                    })}
                </tr>
              </thead>
              <tbody className="fs-10">
                {_.isArray(
                  playStoreExportResponse &&
                    playStoreExportResponse.data &&
                    playStoreExportResponse.data.data
                )
                  ? playStoreExportResponse &&
                    playStoreExportResponse.data &&
                    playStoreExportResponse.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          {exportClientPlayStoreReports &&
                            exportClientPlayStoreReports.data.map(
                              (itemReport, i) => {
                                let reportTdData = itemReport.report_key
                                  ? item[itemReport.report_key]
                                  : "No Data";
                                return _.isArray(reportTdData) ? (
                                  <td key={i}>
                                    {item[itemReport.report_key].toString()}
                                  </td>
                                ) : (
                                  <td>
                                    {item[itemReport.report_key]
                                      ? item[itemReport.report_key]
                                      : "No Data"}
                                  </td>
                                );
                              }
                            )}
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            </table>
          </div>
          <div
            className="table-responsive"
            style={{
              display: "none",
            }}
          >
            <table className="table" id="AppStoreReport-table">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  {exportClientAppStoreReports &&
                    exportClientAppStoreReports.data.map((item, i) => {
                      return <th keu={i}>{item.report_value}</th>;
                    })}
                </tr>
              </thead>
              <tbody className="fs-10">
                {_.isArray(
                  appStoreExportResponse &&
                    appStoreExportResponse.data &&
                    appStoreExportResponse.data.data
                )
                  ? appStoreExportResponse &&
                    appStoreExportResponse.data &&
                    appStoreExportResponse.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          {exportClientAppStoreReports &&
                            exportClientAppStoreReports.data.map(
                              (itemReport, i) => {
                                let reportTdData = itemReport.report_key
                                  ? item[itemReport.report_key]
                                  : "No Data";
                                return _.isArray(reportTdData) ? (
                                  <td key={i}>
                                    {item[itemReport.report_key].toString()}
                                  </td>
                                ) : (
                                  <td>
                                    {item[itemReport.report_key]
                                      ? item[itemReport.report_key]
                                      : "No Data"}
                                  </td>
                                );
                              }
                            )}
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Modal show={Webshow} size="lg" centered onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: "rgb(126 126 219)" }}
          closeButton={() => clearPopup()}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            Upload Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className="col-md-4">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Web Upload Document :
                </label>
              </div>
              <div className="col-md-8">
                <input
                  type="file"
                  className="d-none"
                  name="Uploadfile"
                  id="file"
                  multiple={true}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    handleFileChange(e);
                    e.target.value = null;
                  }}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClick}
                >
                  <i className="bi bi-filetype-csv" />
                  Upload Document
                </button>
                {errors && errors.file && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.file}
                  </div>
                )}
                {fileName && fileName}
              </div>
            </div>
            <div className="row">
              <div className="col-md-4"></div>
              <div className="col-md-8">
                <button
                  type="button"
                  className="btn btn-sm btn-light-primary m-2 fa-pull-right"
                  onClick={(e) => OnWebSubmit(e)}
                  disabled={UploadWebloading}
                >
                  {!UploadWebloading && (
                    <span className="indicator-label">Submit</span>
                  )}
                  {UploadWebloading && (
                    <span
                      className="indicator-progress text-danger"
                      style={{ display: "block" }}
                    >
                      Please wait...
                      <span className="spinner-border spinner-border-sm align-middle ms-2" />
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal show={show} size="lg" centered onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: "rgb(126 126 219)" }}
          closeButton={() => clearPopup()}
        >
          <Modal.Title
            style={{
              color: "white",
            }}
          >
            {editMode ? "Update" : "Add"} Web Case
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab ftab"
          >
            {/* <Tab eventKey="BulkUpload" title="BulkUpload">
              <div className="card card-custom card-stretch gutter-b p-8">
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Client :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name='AppUserId'
                          className='select2'
                          classNamePrefix='select'
                          handleChangeReactSelect={handleChangeAsignees}
                          options={AsigneesOption}
                          value={SelectedAsigneesOption}
                          isDisabled={!AsigneesOption}
                        />
                        {error && error.client && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.client}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    null
                  )
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': formData.tag && errors.tag },
                        {
                          'is-valid': formData.tag && !errors.tag
                        }
                      )}
                      onChange={(e) => handleChange(e)}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={formData.tag || ''}
                    />
                    {errors && errors.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.tag}
                      </div>
                    )}
                  </div>
                </div>
                {
                  Role === 'Admin' || Role === 'Analyst' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Batch Type:
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <select
                          name='batchType'
                          className='form-select form-select-solid'
                          data-control='select'
                          data-placeholder='Select an option'
                          data-allow-clear='true'
                          onChange={(e) => handleChange(e)}
                          value={formData.batchType || ''}
                        >
                          <option value='Live'>Live</option>
                          <option value='Test'>Test</option>
                        </select>
                      </div>
                    </div>
                  ) : null
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Upload Document :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      type="file"
                      className="d-none"
                      name="file"
                      id="file"
                      multiple={true}
                      ref={hiddenFileInput}
                      onChange={(e) => {
                        handleFileChange(e)
                        e.target.value = null
                      }}
                    />
                    <button
                      type="button"
                      style={{
                        width: "100%",
                      }}
                      className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                      onClick={handleClick}
                    >
                      <i className="bi bi-filetype-csv" />
                      Upload Document
                    </button>
                    {errors && errors.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.file}
                      </div>
                    )}
                    {fileName && fileName}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <CSVLink
                      data={data}
                      filename={"WRMSampleFileForImport.csv"}
                      className="btn btn-sm btn-light-success btn-responsive font-5vw"
                      target="_blank"
                    >
                      <i className="bi bi-filetype-csv" />
                      Download_Sample
                    </CSVLink>
                    <div className='fs-7 mt-4 text-danger'>* Website URL alone is a mandatory input</div>
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      type='button'
                      className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                      onClick={(e) => OnSubmit(e)}
                      disabled={postCSVLoading}
                    >
                      {!postCSVLoading && <span className='indicator-label'>Submit</span>}
                      {postCSVLoading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab> */}
            <Tab
              eventKey="Individual"
              title="Individual"
              style={{ height: "50vh", overflow: "scroll" }}
            >
              <div className="card card-custom card-stretch gutter-b p-8">
                {Role === "Admin" ? (
                  <div className="row mb-8">
                    <div className="col-md-4">
                      <label className="font-size-xs font-weight-bold mb-3  form-label">
                        Client :
                      </label>
                    </div>
                    <div className="col-md-8">
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name="Indidual"
                        className="select2"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeIndidual}
                        options={IndidualOption}
                        value={SelectedIndidualOption}
                        isDisabled={!IndidualOption}
                      />
                      {error && error.client && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {error.client}
                        </div>
                      )}
                    </div>
                  </div>
                ) : null}
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Website :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Website"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid": manualFormData.website && error.website,
                        },
                        {
                          "is-valid": manualFormData.website && !error.website,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="website"
                      autoComplete="off"
                      value={manualFormData.website || ""}
                    />
                    {error && error.website && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.website}
                      </div>
                    )}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3 form-label">
                      Merchant Name :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Merchant Name"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.merchant_name && error.merchant_name,
                        },
                        {
                          "is-valid":
                            manualFormData.merchant_name &&
                            !error.merchant_name,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="merchant_name"
                      autoComplete="off"
                      value={manualFormData.merchant_name || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3 form-label">
                      Merchant personal address :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Merchant personal address"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.merchant_personal_address &&
                            error.merchant_personal_address,
                        },
                        {
                          "is-valid":
                            manualFormData.merchant_personal_address &&
                            !error.merchant_personal_address,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="merchant_personal_address"
                      autoComplete="off"
                      value={manualFormData.merchant_personal_address || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Merchant personal email :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Email ID"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.merchant_personal_email &&
                            error.merchant_personal_email,
                        },
                        {
                          "is-valid":
                            manualFormData.merchant_personal_email &&
                            !error.merchant_personal_email,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="merchant_personal_email"
                      autoComplete="off"
                      value={manualFormData.merchant_personal_email || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Merchant Phone Number :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Phone Number"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.merchant_phone &&
                            error.merchant_phone,
                        },
                        {
                          "is-valid":
                            manualFormData.merchant_phone &&
                            !error.merchant_phone,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      maxLength={10}
                      name="merchant_phone"
                      autoComplete="off"
                      value={manualFormData.merchant_phone || ""}
                      onKeyPress={(e) => {
                        if (!/^[0-9 .]+$/.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Acquirer :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Acquirer"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.acquirer && error.acquirer,
                        },
                        {
                          "is-valid":
                            manualFormData.acquirer && !error.acquirer,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="acquirer"
                      autoComplete="off"
                      value={manualFormData.acquirer || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Legal Name :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Legal Name"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.legal_name && error.legal_name,
                        },
                        {
                          "is-valid":
                            manualFormData.legal_name && !error.legal_name,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="legal_name"
                      autoComplete="off"
                      value={manualFormData.legal_name || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Business Address :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <textarea
                      placeholder="Business Address"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.business_registered_address &&
                            error.business_registered_address,
                        },
                        {
                          "is-valid":
                            manualFormData.business_registered_address &&
                            !error.business_registered_address,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="business_registered_address"
                      autoComplete="off"
                      value={manualFormData.business_registered_address || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Mcc Code :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Mcc Code"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.mcc_code && error.mcc_code,
                        },
                        {
                          "is-valid":
                            manualFormData.mcc_code && !error.mcc_code,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="mcc_code"
                      autoComplete="off"
                      value={manualFormData.mcc_code || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3 form-label">
                      Business ownership type :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="Indidual"
                      className="select2"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleBusinessType}
                      options={BusinessTypeOption}
                      value={SelectedBusinessTypeOption}
                      isDisabled={!BusinessTypeOption}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Payment instrument :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="Indidual"
                      className="select2"
                      classNamePrefix="select"
                      handleChangeReactSelect={handlePaymentInstrument}
                      options={PaymentInstrumentOption}
                      value={SelectedPaymentInstrumentOption}
                      isDisabled={!PaymentInstrumentOption}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Ndx :
                    </label>
                  </div>
                  <div className="col-md-8">
                    {/* <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="Indidual"
                      className="select2"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleBusinessNdx}
                      options={BusinessNdxOption}
                      value={SelectedBusinessNdxOption}
                      isDisabled={!BusinessNdxOption}
                    /> */}
                    <input
                      placeholder="Ndx"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.ndx && error.ndx,
                        },
                        {
                          "is-valid":
                            manualFormData.ndx && !error.ndx,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="ndx"
                      autoComplete="off"
                      value={manualFormData.ndx || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Age of business :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name="Indidual"
                      className="select2"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleBusinessAge}
                      options={BusinessAgeOption}
                      value={SelectedBusinessAgeOption}
                      isDisabled={!BusinessAgeOption}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Average monthly volume :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Average monthly volume"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.average_monthly_volume &&
                            error.average_monthly_volume,
                        },
                        {
                          "is-valid":
                            manualFormData.average_monthly_volume &&
                            !error.average_monthly_volume,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="average_monthly_volume"
                      autoComplete="off"
                      value={manualFormData.average_monthly_volume || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Average transaction amount :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Average transaction amount"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.average_transaction_amount &&
                            error.average_transaction_amount,
                        },
                        {
                          "is-valid":
                            manualFormData.average_transaction_amount &&
                            !error.average_transaction_amount,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="average_transaction_amount"
                      autoComplete="off"
                      value={manualFormData.average_transaction_amount || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Cin number :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Cin number"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.cin_number && error.cin_number,
                        },
                        {
                          "is-valid":
                            manualFormData.cin_number && !error.cin_number,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="cin_number"
                      autoComplete="off"
                      value={manualFormData.cin_number || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Pan number :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Pan number"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.pan_number && error.pan_number,
                        },
                        {
                          "is-valid":
                            manualFormData.pan_number && !error.pan_number,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="pan_number"
                      autoComplete="off"
                      value={manualFormData.pan_number || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Name of director :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Name of director"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.name_of_director &&
                            error.name_of_director,
                        },
                        {
                          "is-valid":
                            manualFormData.name_of_director &&
                            !error.name_of_director,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="name_of_director"
                      autoComplete="off"
                      value={manualFormData.name_of_director || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Gst in number :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Gst in number"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.gstin_nummber && error.gstin_nummber,
                        },
                        {
                          "is-valid":
                            manualFormData.gstin_nummber &&
                            !error.gstin_nummber,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="gstin_nummber"
                      autoComplete="off"
                      maxLength={15}
                      value={manualFormData.gstin_nummber || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Product :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Product"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid": manualFormData.product && error.product,
                        },
                        {
                          "is-valid": manualFormData.product && !error.product,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="product"
                      autoComplete="off"
                      value={manualFormData.product || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Bank :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Bank"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        { "is-invalid": manualFormData.bank && error.bank },
                        {
                          "is-valid": manualFormData.bank && !error.bank,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="bank"
                      autoComplete="off"
                      value={manualFormData.bank || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Transaction type :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <input
                      placeholder="Transaction type"
                      className={clsx(
                        "form-control form-control-lg form-control-solid",
                        {
                          "is-invalid":
                            manualFormData.transaction_type &&
                            error.transaction_type,
                        },
                        {
                          "is-valid":
                            manualFormData.transaction_type &&
                            !error.transaction_type,
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type="text"
                      name="transaction_type"
                      autoComplete="off"
                      value={manualFormData.transaction_type || ""}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className="col-md-4">
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Date of incorporation :
                    </label>
                  </div>
                  <div className="col-md-8">
                    <DateSelector
                      name="date_of_incorporation"
                      placeholder="Date of incorporation"
                      className="form-control w-100"
                      selected={manualFormData.date_of_incorporation || ""}
                      onChange={(date) => {
                        setManualFormData((values) => ({
                          ...values,
                          date_of_incorporation: date,
                        }));
                      }}
                      dateFormat={DATE.DATE_FOR_PICKER}
                      maxDate={new Date()}
                      isClearable={true}
                      peek={true}
                      monthDropdown={true}
                      yearDropdown={true}
                      showYear={true}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 text-center">
                    <button
                      className="btn btn-light-primary m-1 mt-8 font-5vw "
                      onClick={handelSubmit}
                      disabled={postManualWebLoading}
                    >
                      {!postManualWebLoading && (
                        <span className="indicator-label">Submit</span>
                      )}
                      {postManualWebLoading && (
                        <span
                          className="indicator-progress text-danger"
                          style={{ display: "block" }}
                        >
                          Please wait...
                          <span className="spinner-border spinner-border-sm align-middle ms-2" />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    rulesStore,
    BlockListlistStore,
    BlockListTypeStore,
    AddEmailToBlacklistKeysone,
    queueslistStore,
    BlockListUploadlistStore,
    BlockListEditlistStore,
    BlockListDeletelStore,
    BlockListUpdatelistStore,
    webclientReportStore,
    getWebAnalysisStore,
    WebAnalysisStore,
    AddEddRiskManagementStore,
    DeleteWebAnalysisStore,
    EditWebAnalysisStore,
    UpdateWebAnalysisStore,
    clinetListStore,
    exportlistStore,
    PlayStoreExportStore,
    UpdateWebReportManageStore,
    GetClientsStore,
    CategoryStatusManageStore,
    ExportPlaystoreStore,
    ExportWRMStore,
    AppStoreExportStore,
    BusinessTypeStore,
    BusinessAgeStore,
    BusinessNdxStore,
    PaymentInstrumentStore
  } = state;

  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    DeleteRules:
      rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : "",
    BlockListlists:
      BlockListlistStore && BlockListlistStore.BlockListlists
        ? BlockListlistStore.BlockListlists
        : {},
    BlockListType:
      BlockListTypeStore && BlockListTypeStore.BlockListType
        ? BlockListTypeStore.BlockListType
        : {},
    BlockListUpdateSuccess:
      BlockListUpdatelistStore && BlockListUpdatelistStore.BlockListUpdatelists
        ? BlockListUpdatelistStore.BlockListUpdatelists
        : {},
    BlockEmailSuccess:
      AddEmailToBlacklistKeysone && AddEmailToBlacklistKeysone.emailSuccess
        ? AddEmailToBlacklistKeysone.emailSuccess
        : {},
    queuesLists:
      queueslistStore && queueslistStore.queueslists
        ? queueslistStore.queueslists
        : {},
    BlocklistsUploads:
      BlockListUploadlistStore && BlockListUploadlistStore.BlocklistsUploads
        ? BlockListUploadlistStore.BlocklistsUploads
        : "",
    BlockListEditlists:
      BlockListEditlistStore && BlockListEditlistStore.BlockListEditlists
        ? BlockListEditlistStore.BlockListEditlists
        : "",
    DelteBlockList:
      BlockListDeletelStore && BlockListDeletelStore.DelteBlockList
        ? BlockListDeletelStore.DelteBlockList
        : "",
    loading:
      getWebAnalysisStore &&
      getWebAnalysisStore.loading &&
      getWebAnalysisStore.loading,
    getWebAnalysis:
      getWebAnalysisStore && getWebAnalysisStore.getWebAnalysis
        ? getWebAnalysisStore.getWebAnalysis
        : "",
    postCSVWebAnalysis:
      WebAnalysisStore && WebAnalysisStore.postCSVWebAnalysis
        ? WebAnalysisStore.postCSVWebAnalysis
        : "",
    postCSVLoading:
      WebAnalysisStore && WebAnalysisStore.loading
        ? WebAnalysisStore.loading
        : "",
    postManualWebAnalysis:
      AddEddRiskManagementStore &&
      AddEddRiskManagementStore.AddEddRiskManagementRes
        ? AddEddRiskManagementStore.AddEddRiskManagementRes
        : "",
    postManualWebLoading:
      AddEddRiskManagementStore && AddEddRiskManagementStore.loading
        ? AddEddRiskManagementStore.loading
        : "",
    deleteWebAnalysis:
      DeleteWebAnalysisStore && DeleteWebAnalysisStore.deleteWebAnalysis
        ? DeleteWebAnalysisStore.deleteWebAnalysis
        : "",
    EditWebAnalysis:
      EditWebAnalysisStore && EditWebAnalysisStore.EditWebAnalysis
        ? EditWebAnalysisStore.EditWebAnalysis
        : "",
    UpdateWebAnalysis:
      UpdateWebAnalysisStore && UpdateWebAnalysisStore.UpdateWebAnalysis
        ? UpdateWebAnalysisStore.UpdateWebAnalysis
        : "",
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
    exportLists:
      exportlistStore && exportlistStore.exportLists
        ? exportlistStore.exportLists
        : "",
    exportLoading:
      exportlistStore && exportlistStore.loading ? exportlistStore.loading : "",
    playStoreExportResponse:
      PlayStoreExportStore && PlayStoreExportStore.PlayStoreExportResponse
        ? PlayStoreExportStore.PlayStoreExportResponse
        : "",
    playStoreExportLoading:
      PlayStoreExportStore && PlayStoreExportStore.loading
        ? PlayStoreExportStore.loading
        : "",
    appStoreExportResponse:
      AppStoreExportStore && AppStoreExportStore.AppStoreExportResponse
        ? AppStoreExportStore.AppStoreExportResponse
        : "",
    appStoreExportLoading:
      AppStoreExportStore && AppStoreExportStore.loading
        ? AppStoreExportStore.loading
        : "",
    UploadWebloading:
      UpdateWebReportManageStore &&
      UpdateWebReportManageStore.loading &&
      UpdateWebReportManageStore.loading,
    UpdateWebReportRes:
      UpdateWebReportManageStore &&
      UpdateWebReportManageStore.UpdateWebReportRes &&
      UpdateWebReportManageStore.UpdateWebReportRes,
    GetClientsRes:
      GetClientsStore &&
      GetClientsStore.GetClientsRes &&
      GetClientsStore.GetClientsRes.data
        ? GetClientsStore.GetClientsRes.data
        : {},
    exportCategoryStatusLoading:
      CategoryStatusManageStore && CategoryStatusManageStore.loading
        ? CategoryStatusManageStore.loading
        : "",
    exportCategoryStatusData:
      CategoryStatusManageStore && CategoryStatusManageStore.CategoryStatusData
        ? CategoryStatusManageStore.CategoryStatusData
        : "",
    exportWRMData:
      state.ExportWRMStore && ExportWRMStore.dataEAWL
        ? ExportWRMStore.dataEAWL
        : {},
    exportPlaystoreData:
      ExportPlaystoreStore && ExportPlaystoreStore.dataPSEL
        ? ExportPlaystoreStore.dataPSEL
        : {},
    exportclientReports:
      webclientReportStore && webclientReportStore.exportclientReports
        ? webclientReportStore.exportclientReports
        : {},
    exportClientPlayStoreReports:
      state &&
      state.exportClientPlayStoreReportStore &&
      state.exportClientPlayStoreReportStore.exportClientPlayStoreReports,
    exportClientAppStoreReports:
      state &&
      state.exportClientAppStoreReportStore &&
      state.exportClientAppStoreReportStore.exportClientAppStoreReports,
    businessTypeLists:
      BusinessTypeStore && BusinessTypeStore.BusinessType
        ? BusinessTypeStore.BusinessType
        : "",
    businessAgeLists:
        BusinessAgeStore && BusinessAgeStore.BusinessAge
          ? BusinessAgeStore.BusinessAge
          : "",
    businessNdxLists:
          BusinessNdxStore && BusinessNdxStore.BusinessNdx
            ? BusinessNdxStore.BusinessNdx
            : "",
    paymentInstrumentLists:
            PaymentInstrumentStore && PaymentInstrumentStore.PaymentInstrument
              ? PaymentInstrumentStore.PaymentInstrument
              : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  WebAnalysisDispatch: (data) =>
    dispatch(WebAnalysisActions.getWebAnalysis(data)),
  clearImportDispatch: (data) =>
    dispatch(WebAnalysisActions.clearWebAnalysis(data)),
  getWebAnalysisDispatch: (data) =>
    dispatch(getWebAnalysisActions.getgetWebAnalysislist(data)),
  cleargetWebAnalysislistDispatch: (data) =>
    dispatch(getWebAnalysisActions.cleargetWebAnalysislist(data)),
  postManualWebAnalysisDispatch: (data) =>
    dispatch(AddEddRiskManagementActions.getUpdateWebReport(data)),
  clearManualWebAnalysisDispatch: (data) =>
    dispatch(AddEddRiskManagementActions.clearUpdateWebReport(data)),
  DeleteWebAnalysisDispatch: (data) =>
    dispatch(DeleteWebAnalysisActions.deleteWebAnalysisDetails(data)),
  DeleteClearDispatch: (data) =>
    dispatch(DeleteWebAnalysisActions.clearWebAnalysisDetails(data)),
  EditWebAnalysisDispatch: (id) =>
    dispatch(EditWebAnalysisActions.EditWebAnalysisDetails(id)),
  EditclearWebAnalysis: () =>
    dispatch(EditWebAnalysisActions.EditclearWebAnalysisDetails()),
  UpdateWebAnalysisDispatch: (data) =>
    dispatch(updateWebAnalysisActions.updateWebAnalysisDetails(data)),
  UpdateClearDispatch: (data) =>
    dispatch(updateWebAnalysisActions.updateclearWebAnalysisDetails(data)),
  clientIdDispatch: (data) =>
    dispatch(clientIdLIstActions.getclientIdList(data)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  getWrmRiskManagementlistDispatch: (params) =>
    dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params)),
  clearExportListDispatch: (data) =>
    dispatch(ExportListActions.clearExportList(data)),
  getPlayStoreExportDispatch: (data) =>
    dispatch(PlayStoreExportActions.getPlayStoreExport(data)),
  getAppStoreExportDispatch: (data) =>
    dispatch(AppStoreExportActions.getAppStoreExport(data)),
  clearAppStoreExportDispatch: (data) =>
    dispatch(AppStoreExportActions.clearAppStoreExport(data)),
  clearPlayStoreExportDispatch: (data) =>
    dispatch(PlayStoreExportActions.clearPlayStoreExport(data)),
  UpdateWebReportDispatch: (data) =>
    dispatch(UpdateWebReportActions.getUpdateWebReport(data)),
  ClearUpdateWebReportDispatch: (data) =>
    dispatch(UpdateWebReportActions.clearUpdateWebReport(data)),
  CategoryStatusDispatch: (params) =>
    dispatch(CategoryStatusActions.getCategoryStatusManage(params)),
  clearCategoryStatusDispatch: () =>
    dispatch(CategoryStatusActions.clearCategoryStatusManage()),
  ExportWRMDispatch: (id, query) =>
    dispatch(ExportWRMActions.getExportWRM(id, query)),
  ClearExportWRMDispatch: () => dispatch(ExportWRMActions.clearExportWRM()),
  ExportPlaystoreDispatch: (id, query) =>
    dispatch(ExportPlaystoreActions.getExportPlaystore(id, query)),
  ClearExportPlaystoreDispatch: () =>
    dispatch(ExportPlaystoreActions.clearExportPlaystore()),
  ExportAppstoreDispatch: (id, query) =>
    dispatch(ExportAppstoreActions.getExportAppstore(id, query)),
  ClearExportAppstoreDispatch: () =>
    dispatch(ExportAppstoreActions.clearExportAppstore()),
  exportClientWebDispatch: (params) =>
    dispatch(exportClientWebActions.exportClientWeb(params)),
  exportClientPlayStoreDispatch: (params) =>
    dispatch(exportClientPlayStoreActions.exportClientPlayStore(params)),
  exportClientAppStoreDispatch: (params) =>
    dispatch(exportClientAppStoreActions.exportClientAppStore(params)),
  businessTypeDispatch: (data) =>
    dispatch(BusinessTypeActions.getBusinessType(data)),
  businessAgeDispatch: (data) =>
    dispatch(BusinessAgeActions.getBusinessAge(data)),
  businessNdxDispatch: (data) =>
    dispatch(BusinessNdxActions.getBusinessNdx(data)),
  paymentInstrumentDispatch: (data) =>
    dispatch(PaymentInstrumentActions.getPaymentInstrument(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WebRiskAnalysis); 