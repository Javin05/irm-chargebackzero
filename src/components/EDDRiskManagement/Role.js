import _ from 'lodash'
const Client =[
  'KYC User',
  'Client User',
  'Phonepe User',
  'Icici User',
  'kyc-user',
  'AW User',
  'WRM USER',
  'Pinlab User'
]

function FindRole(props) {
  const { role, children } = props
  const viewRole = (value) => {
    let valid = false
    if (Client.includes(value)) {
      return valid = false
    } else {
      return valid = true
    }
  }

  return (
    !_.isEmpty(role) && viewRole(role) ? (
      <>{children}</>
    ) : (
      null
    )
  )
}
export default FindRole