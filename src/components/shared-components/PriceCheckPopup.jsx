import React, { useState, useEffect, Fragment } from "react"
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import Spinner from 'react-bootstrap/Spinner';
import * as Yup from 'yup'
import { KTSVG } from '../../theme/helpers'
import { connect } from 'react-redux'
import { Formik, Field, Form as Formikform, ErrorMessage } from 'formik'
import { useParams } from 'react-router-dom'
import {
  PriceActions,
  WrmRiskManagementActions
} from '../../store/actions'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"

const PriceCheckPopup = (props) => {
  // let { id } = useParams();
  const {
    loading,
    postPriceCheckDispatch,
    postPriceCheckSuccess,
    fetchPriceCheckData,
    fetchPriceCheckDispatch,
    blockListValue,
    postPopupClear,
    tagSearch,
    getWrmRiskManagementlistDispatch
  } = props
  const [show, setShow] = useState(false)
  const [classGroup, setClassGroup] = useState(false)
  const [policyGroup, setPolicyGroup] = useState(true)
  const [multipleScreenshots, setMultipleScreenshots] = useState(null)
  const [fValues, setFvalues] = useState({
    contact: '',
    privacy: '',
    return: '',
    shipping: '',
    terms_and_conditions: '',
    product_container: '',
    name_class: '',
    price_class: '',
    inner_p_class: '',
    inner_d_class: '',
    choose_policy: '',
    about_us: '',
    refund: '',
    cancellation: ''
  })
  const id = blockListValue
  const validationSchema = Yup.object().shape({
    // contact: Yup.string()
    //        .required('Contact is required'),
    //    privacy: Yup.string()
    //        .required('Privacy Policy is required'),
    //    return: Yup.string()
    //        .required('Return Policy is required'),
    //    shipping: Yup.string()
    //        .required('email is required'),
    //    terms_and_conditions: Yup.string()
    //        .required('lastname is required'),

    // product_container: Yup.string()
    //     .required('Product Container is required'),
    // name_class: Yup.string()
    //     .required('Name Class is required'),
    // price_class: Yup.string()
    //     .required('Price Class is required'),
    // inner_p_class: Yup.string()
    //     .required('Inner Product Class is required'),
    // inner_d_class: Yup.string()
    //     .required('Inner Discount Class is required')
  })
  const clearValues = () => {
    let clear = {
      contact: '',
      privacy: '',
      return: '',
      shipping: '',
      terms_and_conditions: '',
      product_container: '',
      name_class: '',
      price_class: '',
      inner_p_class: '',
      inner_d_class: '',
      choose_policy: '',
      about_us: '',
      refund: '',
      cancellation: ''
    }
    setFvalues(clear)
  }
  const ScreenshotOptions = (props) => {
    const { fieldName, values, setFieldValue } = props
    let fieldValue = values[fieldName]

    return (
      <div>
        {
          (() => {
            if (multipleScreenshots && multipleScreenshots.hasOwnProperty(fieldName)) {
              let multiOption = multipleScreenshots[fieldName].map((item, index) => {
                return (
                  <Fragment key={"Frog_" + index}>
                    <label className="mb-2">
                      <Field
                        type="radio"
                        className="mr-2"
                        name="choose_policy"
                        value={item.policy_screenshot}
                        checked={fieldValue === item.policy_screenshot}
                        onChange={() => {
                          setFieldValue(fieldName, item.policy_screenshot)
                        }} />
                      {item.policy_screenshot}
                    </label>
                    <br />
                  </Fragment>
                )
              });
              return multiOption;
            }
            else {
              return null
            }
          })()
        }
      </div>
    )
  }

  const loadScreenshots = (fetchPriceCheckData) => {
    console.log('fetchPriceCheckData', fetchPriceCheckData)
    let availablePriceChecks = {}
    Object.keys(fetchPriceCheckData).map(function (priceKey, priceIndex) {
      if (Object.keys(fetchPriceCheckData[priceKey]).length === 0 && fetchPriceCheckData[priceKey].constructor === Object) {
      }
      else {
        if (Array.isArray(fetchPriceCheckData[priceKey])) {
          setMultipleScreenshots({
            'privacy': fetchPriceCheckData[priceKey]
          })
        }
        else {
          availablePriceChecks[priceKey] = fetchPriceCheckData[priceKey].policy_screenshot
        }
        //Sample Multiple Call
        let screenShots = [
          {
            policy_screenshot: "https://mf-onboarding-repository.s3.amazonaws.com/policy_pag…ignature=90UMLANXLBfq%2BFdWooUssaWM9wc%3D&Expires=1000",
            status: "accessible"
          },
          {
            policy_screenshot: "https://mf-onboarding-repository.s3.amazonaws.com/policy_pag…ignature=90UMLANXLBfq%2BFdWooUssaWM9wc%3D&Expires=1656068971",
            status: "accessible"
          }
        ]
        setMultipleScreenshots({
          'privacy': screenShots
        })
      }
    })
    let employee = {
      ...fValues,
      ...availablePriceChecks
    }
    setFvalues(employee)
  }

  const handleSave = (formfields) => {
    let formData = [
      {
        "key": "privacy policy",
        "value": [formfields.privacy]
      },
      {
        "key": "return policy",
        "value": [formfields.return]
      },
      {
        "key": "contact",
        "value": [formfields.contact]
      },
      {
        "key": "terms and conditions",
        "value": [formfields.terms_and_conditions]
      },
      {
        "key": "shipping policy",
        "value": [formfields.shipping]
      },
      {
        "key": "about us",
        "value": [formfields.about_us]
      },
      {
        "key": "cancellation policy",
        "value": [formfields.cancellation]
      },
      {
        "key": "refund policy",
        "value": [formfields.refund]
      }
    ]

    postPriceCheckDispatch(id, {
      policyCompliance: formData
    })
  }

  function onSubmit(fields, { setStatus, setSubmitting, resetForm }) {
    handleSave(fields, () => {
      resetForm(fValues)
    })
    setSubmitting(false);
  }

  /*
  Fetch PriceCheck Dispatch
  */
  useEffect(() => {
    if (show) {
      fetchPriceCheckDispatch(id)
    }
    return () => {
      postPopupClear()
      let clear = {
        contact: '',
        privacy: '',
        return: '',
        shipping: '',
        terms_and_conditions: '',
        product_container: '',
        name_class: '',
        price_class: '',
        inner_p_class: '',
        inner_d_class: '',
        choose_policy: '',
      }
      setFvalues(clear)
    }
  }, [show])

  // console.log('fValues', fValues)

  /*
  Post PriceCheck Load Data
  */
  useEffect(() => {
    if (fetchPriceCheckData) {
      loadScreenshots(fetchPriceCheckData)
    }
  }, [fetchPriceCheckData])

  /*
  Post PriceCheck Success
  */
  useEffect(() => {
    if (postPriceCheckSuccess && postPriceCheckSuccess.status === 'ok') {
      successAlert(
        postPriceCheckSuccess && postPriceCheckSuccess.message,
        'success'
      )
      setShow(false)
      postPopupClear()
      getWrmRiskManagementlistDispatch(tagSearch)

    } else if (postPriceCheckSuccess && postPriceCheckSuccess.status === 'ok') {
      warningAlert(
        'error',
        postPriceCheckSuccess && postPriceCheckSuccess.message,
        '',
        'Try again',
        '',
        () => { }
      )
      postPopupClear()
    }
  }, [postPriceCheckSuccess])


  return (
    <Fragment>
      {/*<Button variant="secondary" onClick={()=>{setShow(true)}}>Price Check</Button>*/}
      <a className="btn btn-sm btn-light-success fw-bolder px-4 me-1"
        onClick={() => { setShow(true) }}>
        <KTSVG path="/media/icons/duotune/finance/fin008.svg" />
        Update Tags
      </a>
      <Modal
        show={show}
        onHide={() => {
          setShow(false)
          clearValues()
        }}
        className="price-check-modal"
        size="lg"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Update Tags</Modal.Title>
        </Modal.Header>
        <Modal.Body className="pt-2">
        {!loading &&<Formik
            enableReinitialize={true}
            initialValues={fValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}

          >
            {({ values, errors, touched, isSubmitting, setFieldTouched, setFieldValue, resetForm }) => {
              return (
                <Formikform
                  id="popup_form"
                  className="popup_form"
                  noValidate>
                  {classGroup ? (
                    <Fragment>
                      <Form.Group className="mb-5" controlId="product_container">
                        <Form.Label>Product Container</Form.Label>
                        <Field
                          name="product_container"
                          placeholder="Enter product container"
                          className={'form-control' + (errors.product_container && touched.product_container ? ' is-invalid' : '')} />
                        <ErrorMessage name="product_container" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="name_class">
                        <Form.Label>Name Class</Form.Label>
                        <Field
                          name="name_class"
                          placeholder="Enter name class"
                          className={'form-control' + (errors.name_class && touched.name_class ? ' is-invalid' : '')} />
                        <ErrorMessage name="name_class" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="price_class">
                        <Form.Label>Price Class</Form.Label>
                        <Field
                          name="price_class"
                          placeholder="Enter price class"
                          className={'form-control' + (errors.price_class && touched.price_class ? ' is-invalid' : '')} />
                        <ErrorMessage name="price_class" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="inner_p_class">
                        <Form.Label>Inner Product Class</Form.Label>
                        <Field
                          name="inner_p_class"
                          placeholder="Enter inner product class"
                          className={'form-control' + (errors.inner_p_class && touched.inner_p_class ? ' is-invalid' : '')} />
                        <ErrorMessage name="inner_p_class" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="inner_d_class">
                        <Form.Label>Inner Discount Class</Form.Label>
                        <Field
                          name="inner_d_class"
                          placeholder="Enter inner discount class"
                          className={'form-control' + (errors.inner_d_class && touched.inner_d_class ? ' is-invalid' : '')} />
                        <ErrorMessage name="inner_d_class" component="div" className="invalid-feedback" />
                      </Form.Group>
                    </Fragment>
                  ) : null}
                  {policyGroup ? (
                    <Fragment>
                      <div className="mt-5 mb-5">
                        <h3>Merchant Policy Check</h3>
                      </div>
                      <Form.Group className="mb-5" controlId="privacy">
                        <Form.Label>Privacy Policy</Form.Label>
                        {/* <ScreenshotOptions 
											        	fieldName={"privacy"}
											        	values={values}
											        	setFieldValue={setFieldValue}/>											         */}
                        <Field
                          name="privacy"
                          placeholder="Enter privacy policy"
                          className={'form-control' + (errors.privacy && touched.privacy ? ' is-invalid' : '')}
                        />
                        <ErrorMessage name="privacy" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="return">
                        <Form.Label>Return Policy</Form.Label>
                        <ScreenshotOptions
                          fieldName={"return"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="return"
                          placeholder="Enter return policy"
                          className={'form-control' + (errors.return && touched.return ? ' is-invalid' : '')} />
                        <ErrorMessage name="return" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="contact">
                        <Form.Label>Contact</Form.Label>
                        <ScreenshotOptions
                          fieldName={"Contact"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="contact"
                          placeholder="Enter contact"
                          className={'form-control' + (errors.contact && touched.contact ? ' is-invalid' : '')} />
                        <ErrorMessage name="contact" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="terms_and_conditions">
                        <Form.Label>Terms and Conditions</Form.Label>
                        <ScreenshotOptions
                          fieldName={"terms_and_conditions"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="terms_and_conditions"
                          placeholder="Enter terms and Conditions"
                          className={'form-control' + (errors.terms_and_conditions && touched.terms_and_conditions ? ' is-invalid' : '')} />
                        <ErrorMessage name="terms_and_conditions" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="shipping">
                        <Form.Label>Shipping Policy</Form.Label>
                        <ScreenshotOptions
                          fieldName={"shipping"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="shipping"
                          placeholder="Enter shipping policy"
                          className={'form-control' + (errors.shipping && touched.shipping ? ' is-invalid' : '')} />
                        <ErrorMessage name="shipping" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="about_us">
                        <Form.Label>About Us Policy</Form.Label>
                        <ScreenshotOptions
                          fieldName={"about_us"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="about_us"
                          placeholder="Enter about us policy"
                          className={'form-control' + (errors.about_us && touched.about_us ? ' is-invalid' : '')} />
                        <ErrorMessage name="about_us" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="refund">
                        <Form.Label>Refund Policy</Form.Label>
                        <ScreenshotOptions
                          fieldName={"refund"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="refund"
                          placeholder="Enter refund policy"
                          className={'form-control' + (errors.refund && touched.refund ? ' is-invalid' : '')} />
                        <ErrorMessage name="refund" component="div" className="invalid-feedback" />
                      </Form.Group>
                      <Form.Group className="mb-5" controlId="cancellation">
                        <Form.Label>Cancellation Policy</Form.Label>
                        <ScreenshotOptions
                          fieldName={"cancellation"}
                          values={values}
                          setFieldValue={setFieldValue} />
                        <Field
                          name="cancellation"
                          placeholder="Enter cancellation policy"
                          className={'form-control' + (errors.cancellation && touched.cancellation ? ' is-invalid' : '')} />
                        <ErrorMessage name="cancellation" component="div" className="invalid-feedback" />
                      </Form.Group>
                    </Fragment>
                  ) : null}
                  <Form.Group className="mb-5">
                    <button
                      type="reset"
                      className="btn btn-secondary mr-4 me-4"
                      onClick={() => {
                        clearValues()
                      }}>
                      Reset
                    </button>
                    <button
                      type="submit"
                      disabled={isSubmitting}
                      className="btn btn-primary">
                      {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                      Create
                    </button>
                  </Form.Group>
                </Formikform>
              )
            }}
          </Formik>}
          {loading && (
            <div className="custom-overlay text-center">
              <div className="loaderu text-center">
                <Spinner animation="border" variant="primary" />
              </div>
            </div>
          )}
        </Modal.Body>
      </Modal>
    </Fragment>
  )
}

const mapStateToProps = state => {
  const {
    PriceStore,
  } = state
  return {
    loading: state && state.PriceStore && state.PriceStore.loading,
    fetchPriceCheckData: PriceStore && PriceStore.priceCheckList ? PriceStore.priceCheckList : null,
    postPriceCheckSuccess: PriceStore && PriceStore.priceSuccess && PriceStore.priceSuccess.data ? PriceStore.priceSuccess.data : '',
  }
}
const mapDispatchToProps = dispatch => ({
  fetchPriceCheckDispatch: (id) => dispatch(PriceActions.fetchPopupInit(id)),
  postPriceCheckDispatch: (id, formData) => dispatch(PriceActions.postPopupInit(id, formData)),
  postPopupClear: () => dispatch(PriceActions.postPopupClear()),
  getWrmRiskManagementlistDispatch: (params) => dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params)),
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PriceCheckPopup);
