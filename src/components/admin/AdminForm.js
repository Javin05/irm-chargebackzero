import { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Link, useLocation } from 'react-router-dom'
import 'react-phone-input-2/lib/style.css'
import {
  inputFieldActions, AdminAddAction, RulesGetIdActions, updateRulesActions,
  AdminGetIdActions, updateAdminActions
} from '../../store/actions'
import { adminValidation } from './validation'
import { setAdminData } from './formData'
import { STATUS_RESPONSE, DATE } from '../../utils/constants'
import { warningAlert, confirmationAlert } from "../../utils/alerts"
import { DateSelector } from '../../theme/layout/components/DateSelector'

function AdminForm(props) {
  const {
    postAdminListDispatch,
    getInputListDispatch,
    className,
    addRules,
    clearAdminIdDetailsDispatch,
    loadingGR,
    loadingARG,
    loadingUR,
    messageUR,
    clearupdateRulesDispatch,
    AdminsAdd,
    AdminsIdDetail,
    PostclearAdmin,
    updateAdminDispatch,
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[2]
  const [limit, setLimit] = useState(25)
  const [editMode, setEditMode] = useState(false)
  const [errors, setErrors] = useState()
  const [formData, setFormData] = useState({
    Ssn: '',
    firstName: '',
    lastName: '',
    dob: '',
    address: '',
    city: '',
    state: '',
    zip: ',',
    homePhone: '',
    cellPhone: '',
    employeName: '',
    employePhone: '',
    notes: ''
  })

  useEffect(() => {
    if (currentId) {
      setEditMode(true)
    } else {
      setEditMode(false)
    }
  }, [currentId])

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getInputListDispatch(params)
  }, [])

  const handleChange = (e) => {
    setErrors({ ...errors, [e.target.name]: '' })
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSubmit = (e) => {
    const errors = adminValidation(formData, setErrors)
    if (_.isEmpty(errors)) {
      if (editMode) {
        updateAdminDispatch(currentId, formData)
      } else {
        postAdminListDispatch(formData)
      }
    }
  }

  const onConfirm = () => {
    props.history.push('/admin')
    window.location.reload(false)
    setFormData({
      Ssn: '',
      firstName: '',
      lastName: '',
      dob: '',
      address: '',
      city: '',
      state: '',
      zip: ',',
      homePhone: '',
      cellPhone: '',
      employeName: '',
      employePhone: '',
      notes: ''
    })
  }

  const clear = () => {
    setFormData({
      Ssn: '',
      firstName: '',
      lastName: '',
      dob: '',
      address: '',
      city: '',
      state: '',
      zip: ',',
      homePhone: '',
      cellPhone: '',
      employeName: '',
      employePhone: '',
      notes: ''
    })
  }

  useEffect(() => {
    if (AdminsIdDetail.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const setData = setAdminData(AdminsIdDetail)
      setFormData(setData)
      clearAdminIdDetailsDispatch()
    } else {
      clearAdminIdDetailsDispatch()
    }
  }, [AdminsIdDetail.status])

  useEffect(() => {
    if (AdminsAdd && AdminsAdd.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        AdminsAdd && AdminsAdd.message,
        'success',
        'Back to Admin',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      PostclearAdmin()
    } else if (AdminsAdd && AdminsAdd.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        AdminsAdd && AdminsAdd.message,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      PostclearAdmin()
    }
  }, [addRules])

  const onConfirmEdit = () => {
    props.history.push('/rules')
    setFormData({
      adminSsn: '',
      firstName: '',
      lastName: '',
      adminDob: '',
      adminAddress: '',
      adminCity: '',
      adminState: '',
      adminZip: ',',
      adminHome: '',
      adminCell: '',
      adminEmployeName: '',
      adminEmployePhone: '',
      adminNotes: ''
    })
  }

  useEffect(() => {
    if (AdminsIdDetail.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        messageUR,
        'success',
        'Back to Rules',
        'Ok',
        () => { onConfirmEdit() },
        () => { onConfirmEdit() }
      )
      clearupdateRulesDispatch()
    } else if (AdminsIdDetail.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        messageUR,
        '',
        'Try again',
        '',
        () => { onConfirmEdit() }
      )
      clearupdateRulesDispatch()
    }
  }, [AdminsIdDetail.status])

  return (
    <>
      <div style={{ backgroundColor: '#d4e3f6' }} className={`card ${className}`}>
        {
          loadingGR ? (
            <div className='d-flex justify-content-center py-5'>
              <div className='spinner-border text-primary m-5' role='status' />
            </div>
          ) : (
            <>
              <div className="row mt-10 me-2">
                <div className="col-md ms-1 ">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> SSN/SIN:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input type="checkbox" className="form-check-input mt-3 ms-0"></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.Ssn || ''}
                        type="text" name="Ssn"
                        className="form-control  form-control-solid ms-3"
                        placeholder="ssn/sin" />
                      {errors && errors.Ssn && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.Ssn}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> First Name:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-1" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.firstName || ''}
                        type="text" name="firstName"
                        className="form-control  form-control-solid ms-3"
                        placeholder="first name" />
                      {errors && errors.firstName && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.firstName}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> Last Name:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.lastName || ''}
                        type="text" name="lastName"
                        className="form-control  form-control-solid ms-3"
                        placeholder="last name" />
                      {errors && errors.lastName && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.lastName}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md ">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> DOB:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11 '>
                      <DateSelector
                        name='dob'
                        placeholder='DOB'
                        className='form-control'
                        selected={formData.dob || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, dob: '' })
                          setFormData((values) => ({
                            ...values,
                            dob: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FOR_PICKER}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                      {errors && errors.dob && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.dob}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-10 me-2">
                <div className="col-md ">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> address:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-1" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.address || ''}
                        type="text" name="address"
                        className="form-control  form-control-solid ms-3 "
                        placeholder="address" />
                      {errors && errors.address && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.address}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> city:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.city || ''}
                        type="text" name="city"
                        className="form-control  form-control-solid ms-3 "
                        placeholder="city" />
                      {errors && errors.city && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.city}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> state/Province:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.state || ''}
                        type="text" name="state"
                        className="form-control  form-control-solid ms-3"
                        placeholder="state/province" />
                      {errors && errors.state && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.state}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md ">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> zip/Postal Code:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11 '>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.zip || ''}
                        type="text" name="zip"
                        className="form-control  form-control-solid ms-3"
                        placeholder="zip/postal code" />
                      {errors && errors.zip && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.zip}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-10 me-2">
                <div className="col-md ">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> homePhone Phone:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-1" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.homePhone || ''}
                        type="text" name="homePhone"
                        className="form-control  form-control-solid ms-3 "
                        placeholder="homePhone phone"
                        maxLength={10}
                      />
                      {errors && errors.homePhone && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.homePhone}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> cellPhone Phone:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.cellPhone || ''}
                        type="text" name="cellPhone"
                        className="form-control  form-control-solid ms-3 "
                        placeholder="cell phone"
                        maxLength={10}
                      />
                      {errors && errors.cellPhone && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.cellPhone}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> Employee Name:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11'>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.employeName || ''}
                        type="text" name="employeName"
                        className="form-control  form-control-solid ms-3 "
                        placeholder="employee name" />
                      {errors && errors.employeName && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.employeName}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="col-md">
                  <label className='col-form-label col-md-4 text-lg-start ms-10 w-200px'> Employee Phone Number:</label>
                  <div className='row'>
                    <div className='col-sm-1'>
                      <input className="form-check-input mt-3 ms-0" type="checkbox" value=""></input>
                    </div>
                    <div className='col-sm-11 '>
                      <input
                        onChange={(e) => handleChange(e)}
                        value={formData.employePhone || ''}
                        type="text" name="employePhone"
                        className="form-control  form-control-solid ms-3"
                        placeholder="phone number"
                        maxLength={10}
                      />
                      {errors && errors.employePhone && (
                        <div className='fv-plugins-message-container text-danger ms-2'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.employePhone}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <div className='row mt-20 me-5'>
                <div className='col-md-12'>
                  <label className='col-form-label col-md-4 text-lg-start ms-10'> notes:</label>
                  <div className='row'>
                    <div className='col-sm-12'>
                      <textarea
                        onChange={(e) => handleChange(e)}
                        value={formData.notes || ''}
                        type="text" name="notes"
                        className="form-control  form-control-solid ms-3 me-20 "
                        placeholder="Comments" />
                    </div>
                  </div>
                </div>
              </div>
            </>
          )
        }
        <div className='row mt-10'>
          <div className='col-lg-4' >
          </div>
          <div className='col-lg-4' >
          </div>
          <div className='col-lg-4 '>
            <button
              type='button'
              className='btn btn-sm btn-light-primary m-2 fa-pull-right'
              onClick={(e) => handleSubmit(e)}
              disabled={loadingUR || loadingARG}
            >
              {loadingUR || loadingARG ? (
                <span className='indicator-progress' style={{ display: 'block' }}>
                  Please wait...
                  <span className='spinner-border spinner-border-sm align-middle ms-2' />
                </span>
              ) : "Submit"
              }
            </button>
            <Link
              to='/admin'
              disabled={loadingUR || loadingARG}
              className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
            >
              cancel
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  const { editRulesStore, rulesStore, updateRulesStore, AdminsAddStore, editAdminsStore } = state
  return {
    AdminsAdd: AdminsAddStore && AdminsAddStore.AdminsAdd ? AdminsAddStore.AdminsAdd : '',
    AdminsIdDetail: editAdminsStore && editAdminsStore.AdminsIdDetail ? editAdminsStore.AdminsIdDetail : '',
    updateAdminResponce: updateRulesStore && updateRulesStore.updateAdminResponce ? updateRulesStore.updateAdminResponce : '',
    addRules: rulesStore && rulesStore.addRules ? rulesStore.addRules : '',
    loadingARG: rulesStore && rulesStore.loadingARG ? rulesStore.loadingARG : false,
    inputfields: state && state.inputFieldsStore && state.inputFieldsStore.inputfields,
    loadingGR: editRulesStore && editRulesStore.loadingGR ? editRulesStore.loadingGR : false,
    RulesIdDetail: editRulesStore && editRulesStore.RulesIdDetail ? editRulesStore.RulesIdDetail : {},
    statusEA: editRulesStore && editRulesStore.statusEA ? editRulesStore.statusEA : '',
    loadingUR: updateRulesStore && updateRulesStore.loadingUR ? updateRulesStore.loadingUR : false,
    updateRulesResponce: updateRulesStore && updateRulesStore.updateRulesResponce ? updateRulesStore.updateRulesResponce : {},
    messageUR: updateRulesStore && updateRulesStore.messageUR ? updateRulesStore.messageUR : '',
    statusUR: updateRulesStore && updateRulesStore.statusUR ? updateRulesStore.statusUR : '',
  }
}

const mapDispatchToProps = dispatch => ({
  PostclearAdmin: () => dispatch(AdminAddAction.clearAdmin()),
  postAdminListDispatch: (params) => dispatch(AdminAddAction.AdminAdd(params)),
  clearAdminIdDetailsDispatch: () => dispatch(AdminGetIdActions.clearAdminIdDetails()),
  getAdminIdDetailsDispatch: (params) => dispatch(AdminGetIdActions.getAdminIdDetails(params)),
  updateAdminDispatch: (id, params) => dispatch(updateAdminActions.updateAdmin(id, params)),
  getInputListDispatch: (params) => dispatch(inputFieldActions.getInputFields(params)),
  getRulesIdDispatch: (params) => dispatch(RulesGetIdActions.getRulesIdDetails(params)),
  clearAdminIdDetailsDispatch: () => dispatch(RulesGetIdActions.clearRulesIdDetails()),
  updateRulesDispatch: (id, params) => dispatch(updateRulesActions.updateRules(id, params)),
  clearupdateRulesDispatch: () => dispatch(updateRulesActions.clearupdateRules())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminForm);
