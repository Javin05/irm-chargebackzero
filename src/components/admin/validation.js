import _ from 'lodash'

export const adminValidation = (values, setErrors) => {
  const errors = {};
  if (!values.Ssn) {
    errors.Ssn = 'SSN/SIN field is required.'
  }
  if (!values.firstName) {
    errors.firstName = 'First Name field is required.'
  }
  if (!values.lastName) {
    errors.lastName = 'Last Name field is required.'
  }
  if (!values.dob) {
    errors.dob = 'DOB field is required.'
  }
  if (!values.address) {
    errors.address = 'Address field is required.'
  }
  if (!values.city) {
    errors.city = 'City field is required.'
  }
  if (!values.state) {
    errors.state = 'State/Province field is required.'
  }
  if (!values.zip) {
    errors.zip = 'Zip/Postal code field is required.'
  }
  if (!values.homePhone) {
    errors.homePhone = 'Home phone field is required.'
  }
  if (!values.cellPhone) {
    errors.cellPhone = 'Cell number field is required.'
  }
  if (!values.employeName) {
    errors.employeName = 'Employe Name field is required.'
  }
  if (!values.employePhone) {
    errors.employePhone = 'Employe Phone number field is required.'
  }
  setErrors(errors); 
  return errors;
};