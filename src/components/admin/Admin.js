import { useEffect, useState } from "react";
import { rulesActions, AdminActions } from "../../store/actions";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
import { Link } from "react-router-dom";
import _ from "lodash";
import { KTSVG } from "../../theme/helpers";
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  STATUS_BADGE,
} from "../../utils/constants";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";
import moment from "moment";

function Admin(props) {
  const {
    getRuleslistDispatch,
    deleteRulesListDispatch,
    className,
    Adminslists,
    loading,
    DeleteRules,
    AdminListDispatch,
  } = props;
  const [limit, setLimit] = useState(25);
  const [, setData] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1);

  const [sorting, setSorting] = useState({
    ruleId: false,
    rulefield: false,
    ruleDescription: false,
  });
  useEffect(() => {
    const params = {
      limit: limit,
      page: 1,
    };
    AdminListDispatch(params);
  }, []);

  const handleRecordPerPage = (e) => {
    const { value } = e.target;
    setLimit(value);
  };

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: pageNumber,
    };
    setActivePageNumber(pageNumber);
    getRuleslistDispatch(params);
  };

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name];
      setSorting(sorting);
      setData({});
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "DESC",
      };
      getRuleslistDispatch(params);
    } else {
      const filter = _.mapValues(sorting, () => {
        return false;
      });
      filter[name] = !filter[name];
      setSorting(filter);
      setData({});
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "ASC",
      };
      getRuleslistDispatch(params);
    }
  };

  const totalPages =
    Adminslists && Adminslists.count
      ? Math.ceil(parseInt(Adminslists && Adminslists.count) / limit)
      : 1;

  const onConfirmDelete = (id) => {
    deleteRulesListDispatch(id);
  };
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_RULES,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmDelete(id);
      },
      () => {}
    );
  };

  useEffect(() => {
    if (DeleteRules && DeleteRules.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(DeleteRules && DeleteRules.message, "success");
    } else if (
      DeleteRules &&
      DeleteRules.status === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "error",
        DeleteRules && DeleteRules.message,
        "",
        "Try again",
        "",
        () => {}
      );
    }
  }, [DeleteRules]);

  return (
    <>
      <div className={`card p-7 ${className}`}>
        <div className="card-body py-3">
          <div className="d-flex  px-2">
            <div className="d-flex justify-content-start col-md-6">
              <div className="col-md-3 mt-1">
                {Adminslists && Adminslists.count && (
                  <span className="text-muted fw-bold d-flex fs-3 mt-2">
                    Total: &nbsp;{" "}
                    <span className="text-gray-700 fw-bolder text-hover-primary fs-3">
                      {Adminslists.count}
                    </span>
                  </span>
                )}
              </div>
              <div className="col-md-7 d-flex">
                <label className="col-form-label text-lg-start">
                  Record(s) per Page : &nbsp;{" "}
                </label>
                <div className="col-md-3">
                  <select
                    className="form-select w-6rem"
                    data-control="select"
                    data-placeholder="Select an option"
                    data-allow-clear="true"
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="75">75</option>
                    <option value="100">100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="d-flex col-md-6 justify-content-end my-auto">
              <div className="my-auto"></div>
              <div className="my-auto me-3">
                <Link
                  to="/add-admin"
                  className="btn btn-sm btn-light-primary btn-responsive font-5vw"
                >
                  <span className="svg-icon svg-icon-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="11.364"
                        y="20.364"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-90 11.364 20.364)"
                        fill="currentColor"
                      ></rect>
                      <rect
                        x="4.36396"
                        y="11.364"
                        width="16"
                        height="2"
                        rx="1"
                        fill="currentColor"
                      ></rect>
                    </svg>
                  </span>
                  Add New Record
                </Link>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className="fw-bolder fs-8 text-gray-800">
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>S.No</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${
                              sorting.riskid
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-25px text-start">
                    <div className="d-flex">
                      <span>Action</span>
                      <div className="min-w-30px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("lastName")}
                        >
                          <i
                            className={`bi ${
                              sorting.lastName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>SSN/SIN</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ruleId")}
                        >
                          <i
                            className={`bi ${
                              sorting.riskid
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>First Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ruleId")}
                        >
                          <i
                            className={`bi ${
                              sorting.riskid
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Last Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${
                              sorting.status
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Date Of Birth</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Address</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>City</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>State/Province</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Zip/Postal Code</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Home Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Cell Phone</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Employer Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Employer Phone Number</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Note</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Created Date</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Created By</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${
                              sorting.firstName
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {!loading ? (
                  Adminslists && Adminslists.data ? (
                    Adminslists.data.map((item, _id) => {
                      return (
                        <tr
                          key={_id}
                          style={
                            _id === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                          <td className="pb-0 pt-5  text-start">{_id + 1}</td>
                          <td className="pb-0 pt-5  text-start">
                            <div
                              title="Edit customer"
                              className=" text-start btn btn-icon btn-hover-primary btn-sm mx-3"
                            >
                              <Link
                                to={`/update/${item._id}`}
                                className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4"
                                href="/client-onboarding/update/621c7764174b5b0004719991"
                              >
                                <KTSVG
                                  path="/media/icons/duotune/art/art005.svg"
                                  className="svg-icon-3"
                                />
                              </Link>
                            </div>
                            <button
                              className="btn btn-icon btn-icon-danger btn-sm w-10px h-10px"
                              onClick={() => onDeleteItem(item._id)}
                              title="Delete customer"
                            >
                              {/* {/ eslint-disable /} */}
                              <KTSVG
                                path="/media/icons/duotune/general/gen027.svg"
                                className="svg-icon-3"
                              />
                              {/* {/ eslint-enable /} */}
                            </button>
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.ssn && item.ssn ? item.ssn : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.firstName && item.firstName
                              ? item.firstName
                              : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.lastName ? item.lastName : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.dob ? item.dob : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.address ? item.address : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.city ? item.city : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.state ? item.state : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.postalCode ? item.postalCode : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.phone ? item.phone : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.cellPhone ? item.cellPhone : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.employerName ? item.employerName : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.employerPhoneNumber
                              ? item.employerPhoneNumber
                              : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {item.notes ? item.notes : "--"}
                          </td>
                          <td className="pb-0 pt-5  text-start fs-7">
                            {moment(
                              item.createdAt ? item.createdAt : "--"
                            ).format("DD/MM/YYYY")}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            {`${item.firstName}${item.lastName} `}
                          </td>
                          <td className="pb-0 pt-5  text-start">
                            <span
                              className={`badge ${
                                STATUS_BADGE[item.status && item.status]
                              }`}
                            >
                              {item.status ? item.status : "--"}
                            </span>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr className="text-center py-3">
                      <td colSpan="100%">No record(s) found</td>
                    </tr>
                  )
                ) : (
                  <tr>
                    <td colSpan="100%" className="text-center">
                      <div
                        className="spinner-border text-primary m-5"
                        role="status"
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { rulesStore } = state;
  return {
    Adminslists: state && state.AdminslistStore && state.AdminslistStore.Adminslists,
    loading: state && state.rulesStore && state.rulesStore.loading,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : "",
  };
};
const mapDispatchToProps = (dispatch) => ({
  getRuleslistDispatch: (params) => dispatch(rulesActions.getRules(params)),
  deleteRulesListDispatch: (id) => dispatch(rulesActions.deleteRules(id)),
  AdminListDispatch: (params) => dispatch(AdminActions.getAdminlist(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
