import _ from "lodash"

export const setTagSummaryData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      approvedCases: data.approvedCases.toString(),
      rejectedCases: data.rejectedCases.toString(),
      tag: data.tag.toString(),
      totalDeliveryCases: data && data.totalDeliveryCases ? data.totalDeliveryCases.toString() : '',
      deliveryDate: data && data.deliveryDate ? new Date(data.deliveryDate) : '',
      uploadedDate: data && data.uploadedDate ? new Date(data && data.uploadedDate) : '',
      clientId: data.clientId,
    }
  }
}

export const setTagSummaryEditData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      tag: data.tag.toString(),
      uploadedDate: data && data.uploadedDate ? new Date(data.uploadedDate) : '',
      deliveryDate: data && data.resolvedDate ? new Date(data.resolvedDate) : '',
      uniqueCases: data.unique_counts.toString(),
      duplicateCases: data.duplicate_counts.toString(),
      totalDeliveryCases: data && data.totalCases ? data.totalCases.toString() : '',
      status: data.status.toString(),
    }
  }
}