import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { TagSummaryAction, GetClientsActions, clientIdLIstActions, BatchSummaryExportAction } from '../../store/actions'
import { DATE } from '../../utils/constants'
import { DateSelector, DateTimeSelector } from '../../theme/layout/components/DateSelector'
import moment from "moment"
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { warningAlert } from "../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"

function SearchList(props) {
  const { TagSummaryDispatch, loading, setCredFilterParams, setSearchData, getClientsWrmDispatch, clientIdDispatch, clinetIdLists, BatchSummaryExportDispatch, batchExportLoading, exportSummaryData, BatchSummaryClearDispatch, TagSummaryData, setSelectShow, setFormDatas } = props
  const [error, setError] = useState({});
  const [show, setShow] = useState(false)
  const [errors, setErrors] = useState({
    email: '',
    password: ''
  })
  const [formData, setFormData] = useState({
    tag: '',
    status: '',
    uploadedDateFrom: '',
    uploadedDateTo: '',
    deliveryDateFrom: '',
    deliveryDateTo: '',
    clientId: '',
    batchType:'',
    type: '',
  })
  const [condition, setcondition] = useState(true)
  const [clear, setClear] = useState(false)
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [exportShow, setExportShow] = useState(false)

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const handleSearch = (e) => {
    setShow(false)
    const UpDate = moment(formData.uploadedDateFrom).format("YYYY-MM-DD LT")
    const DpDate = moment(formData.uploadedDateTo).format("YYYY-MM-DD LT")
    const DeFrDate = moment(formData.deliveryDateFrom).format("YYYY-MM-DD LT")
    const DeToDate = moment(formData.deliveryDateTo).format("YYYY-MM-DD LT")
    const params = {
      uploadedDateFrom: UpDate === 'Invalid date' ? '' : UpDate,
      uploadedDateTo: DpDate === 'Invalid date' ? '' : DpDate,
      deliveryDateFrom: DeFrDate === 'Invalid date' ? '' : DeFrDate,
      deliveryDateTo: DeToDate === 'Invalid date' ? '' : DeToDate,
      tag: formData.tag,
      status: formData.status,
      clientId: formData.clientId,
      batchType:formData.batchType,
      type: formData.type
    }
    TagSummaryDispatch(params)
    setSearchData(params)
    const data = {
      tag: formData.tag,
    }
    getClientsWrmDispatch(data)
    setExportShow(true)
    setSelectShow(true)
  }

  const handleChanges = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...error, [e.target.name]: '' })
  }

  const handleReset = () => {
    setFormData({
      tag: '',
      status: '',
      uploadedDateFrom: '',
      uploadedDateTo: '',
      deliveryDateFrom: '',
      deliveryDateTo: '',
      clientId: '',
      batchType:'',
      type: ''
    }
    )
    const params = {
      limit: 25,
      page: 1
    }
    TagSummaryDispatch(params)
    setExportShow(false)
    setSelectShow(false)
    setSelectedAsigneesOption("")
    setShow(false) 
    setFormDatas([])
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const exportSummary = () => {
    const UpDate = moment(formData.uploadedDateFrom).format("YYYY-MM-DD LT")
    const DpDate = moment(formData.uploadedDateTo).format("YYYY-MM-DD LT")
    const DeFrDate = moment(formData.deliveryDateFrom).format("YYYY-MM-DD LT")
    const DeToDate = moment(formData.deliveryDateTo).format("YYYY-MM-DD LT")
    const dataFormat = {
      uploadedDateFrom: UpDate === 'Invalid date' ? '' : UpDate,
      uploadedDateTo: DpDate === 'Invalid date' ? '' : DpDate,
      deliveryDateFrom: DeFrDate === 'Invalid date' ? '' : DeFrDate,
      deliveryDateTo: DeToDate === 'Invalid date' ? '' : DeToDate,
      tag: formData.tag,
      status: formData.status,
      clientId: formData.clientId,
      batchType:formData.batchType,
      type: formData.type
    }
    BatchSummaryExportDispatch(dataFormat)
  }

  useEffect(() => {
    if (exportSummaryData && exportSummaryData.status === 'ok') {
      if (Array.isArray(exportSummaryData && exportSummaryData.data)) {
        const closeXlsx = document.getElementById('exportSummary')
        closeXlsx.click()
        BatchSummaryClearDispatch()
      }
    } else if (exportSummaryData && exportSummaryData.status === 'error') {
      warningAlert(
        'error',
        exportSummaryData && exportSummaryData.message,
        '',
        'Try again',
        '',
        () => { {} }
      )
      BatchSummaryClearDispatch()
    }
  }, [exportSummaryData])

  useEffect(() =>{
    if(TagSummaryData && TagSummaryData.data && Array.isArray(TagSummaryData.data.result) && exportShow){
      setExportShow(true)
      setSelectShow(true)
    }else{
      setExportShow(false)
      setSelectShow(false)
    }
  },[TagSummaryData])

  console.log('exportSummaryData=====>', exportSummaryData && exportSummaryData.data)

  return (
    <>
    <div
        type='button'
        className='d-none'
        data-target='#exportSummary'
      >
        <ReactHTMLTableToExcel
          id="exportSummary"
          className="download-table-xls-button"
          table="exportSummaryModel"
          filename="exportSummary"
          sheet="tablexls"
        />
      </div>
           <div className="table-responsive" style={{
            display: "none"
          }}>
            <table className="table" id="exportSummaryModel">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  <th>Date</th>
                  <th>Tag</th>
                  <th>UploadedDate</th>
                  {/* <th>Count</th> */}
                  <th>Status</th>
                  <th>Resolved Date</th>
                  <th>SLA</th>
                  <th>Duplicate</th>
                  <th>Unique</th>
                  <th>Total Cases</th>
                </tr>
              </thead>
              <tbody>
                {
                  Array.isArray(exportSummaryData && exportSummaryData.data) ?
                    exportSummaryData && exportSummaryData.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          <td>
                            {item && item.date ? item.date : '--'}
                          </td>
                          <td>
                            {item && item.tag ? item.tag : '--'}
                          </td>
                          <td>
                            {item && item.uploadedDate ? item.uploadedDate : '--'}
                          </td>
                          {/* <td>
                          {item && item.count ? item.count : '--'}
                          </td> */}
                          <td>
                            {item && item.status ? item.status : '--'}
                          </td>
                          <td>
                            {item && item.resolvedDate ? item.resolvedDate : '--'}
                          </td>
                          <td>
                            {item && item.hoursConsumed ? item.hoursConsumed : '--'}
                          </td>
                          <td>
                            {item && item.duplicate_counts ? item.duplicate_counts : item && item.duplicateCount ? item.duplicateCount : '0'}
                          </td>
                          <td>
                            {item && item.unique_counts ? item.unique_counts : item && item.uniqueCount ? item.uniqueCount : '0'}
                          </td>
                          <td>
                            {item && item.totalCases ? item.totalCases : '0'}
                          </td>
                        </tr>
                      )
                    })
                    : null
                }
              </tbody>
            </table>
          </div>
      <div>
        {exportShow ?
          (<button
            type='button'
            className='btn btn-md btn-light-success btn-responsive pull-right w-150px me-2'
            onClick={(e) => exportSummary(e)}
            disabled={batchExportLoading}
          >
            {!batchExportLoading &&
              <span className='indicator-label'>
                <i className="bi bi-filetype-csv" />
                Export Summary
              </span>
            }
            {batchExportLoading && (
              <span className='indicator-progress text-success' style={{ display: 'block' }}>
                Please wait...
                <span className='spinner-border spinner-border-sm align-middle ms-2' />
              </span>
            )}
          </button>
          ) :
          null}
        <button
          type='button'
          className='btn btn-md btn-light-primary btn-responsive me-3 pull-right w-100px'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>


      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => { 
          handleReset()
          }}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => { 
          handleReset()
            }}
            >
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'>
            <div className='card-header'>
              <div className='card-body'>
                <div className='form-group row mb-4'>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Client :
                    </label>
                    <div className='col-lg-11'>
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name='AppUserId'
                        className='select2'
                        classNamePrefix='select'
                        handleChangeReactSelect={handleChangeAsignees}
                        options={AsigneesOption}
                        value={SelectedAsigneesOption}
                        isDisabled={!AsigneesOption}
                      />
                      {error && error.client && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {error.client}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Tag :
                    </label>
                    <div className='col-lg-11'>
                      <input
                        placeholder='Tag'
                        className={clsx(
                          'form-control form-control-lg form-control-solid',
                          { 'is-invalid': formData.tag && error.tag },
                          {
                            'is-valid': formData.tag && !error.tag
                          }
                        )}
                        onChange={(e) => handleChanges(e)}
                        type='text'
                        name='tag'
                        autoComplete='off'
                        value={formData.tag || ''}
                      />
                      {errors && errors.tag && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.tag}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Status :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='status'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.status || ''}
                      >
                        <option value=''>Select...</option>
                        <option value='PROCESSING'>PROCESSING</option>
                        <option value='COMPLETED'>COMPLETED</option>
                        <option value='HOLD'>HOLD</option>
                        <option value='DUPLICATE'>DUPLICATE</option>
                      </select>
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3 form-label'>
                      Uploaded Date From:
                    </label>
                    <div className='col-lg-11'>
                      <DateTimeSelector
                        name='uploadedDateFrom'
                        placeholder='Uploaded Date From'
                        className='form-control'
                        selected={formData.uploadedDateFrom || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, uploadedDateFrom: '' })
                          setFormData((values) => ({
                            ...values,
                            uploadedDateFrom: date
                          }))
                          setcondition(false)
                        }}
                        dateFormat={DATE.DATE_FORMAT_MINS_SECS}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Uploaded Date To:
                    </label>
                    <div className='col-lg-11'>
                      <DateTimeSelector
                        name='uploadedDateTo'
                        placeholder='Uploaded Date To'
                        className='form-control'
                        selected={formData.uploadedDateTo || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, uploadedDateTo: '' })
                          setFormData((values) => ({
                            ...values,
                            uploadedDateTo: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FORMAT_MINS_SECS}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Delivery Date From:
                    </label>
                    <div className='col-lg-11'>
                      <DateTimeSelector
                        name='deliveryDateFrom'
                        placeholder='Delivery Date From'
                        className='form-control'
                        selected={formData.deliveryDateFrom || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, deliveryDateFrom: '' })
                          setFormData((values) => ({
                            ...values,
                            deliveryDateFrom: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FORMAT_MINS_SECS}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mb-3 mt-4'>
                    <label className='font-size-xs font-weight-bold mb-3  form-label'>
                      Delivery Date To:
                    </label>
                    <div className='col-lg-11'>
                      <DateTimeSelector
                        name='deliveryDateTo'
                        placeholder='Delivery Date To'
                        className='form-control'
                        selected={formData.deliveryDateTo || ''}
                        onChange={(date) => {
                          setErrors({ ...errors, deliveryDateTo: '' })
                          setFormData((values) => ({
                            ...values,
                            deliveryDateTo: date
                          }))
                        }}
                        dateFormat={DATE.DATE_FORMAT_MINS_SECS}
                        maxDate={new Date()}
                        isClearable={true}
                        peek={true}
                        monthDropdown={true}
                        yearDropdown={true}
                        showYear={true}
                      />
                    </div>
                  </div>
                  <div className='col-lg-4 mt-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Batch Type :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='batchType'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.batchType || ''}
                      >
                        <option value=''>All</option>
                        <option value='Live'>Live</option>
                        <option value='Test'>Test</option>
                      </select>
                    </div>
                  </div>
                  <div className='col-lg-4 mt-4 mb-3'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Type :
                    </label>
                    <div className='col-lg-11'>
                      <select
                        name='type'
                        className='form-select form-select-solid'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChanges(e)}
                        value={formData.type || ''}
                      >
                        <option value=''>All</option>
                        <option value='WRM'>WRM</option>
                        <option value='EDD'>EDD</option>
                      </select>
                    </div>
                  </div>
                  <div className='form-group row mb-4 mt-4'>
                    <div className='col-lg-6' />
                    <div className='col-lg-6'>
                      <div className='col-lg-11'>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary m-2 fa-pull-right w-100px'
                          onClick={() => handleSearch()}
                          disabled={loading}
                        >
                          {loading
                            ? (
                              <span
                                className='spinner-border spinner-border-sm mx-3'
                                role='status'
                                aria-hidden='true'
                              />
                            )
                            : (
                              'Search'
                            )}
                        </button>
                        <button
                          type='button'
                          className='btn btn-sm btn-light-danger m-2 fa-pull-right close w-100px'
                          onClick={() => handleReset()}
                        >
                          Reset
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = (state) => {
  const { TagSummaryStore, clinetListStore, BatchSummaryExportStore } = state
  return {
    loading: TagSummaryStore && TagSummaryStore.loading ? TagSummaryStore.loading : false,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',

    batchExportLoading: BatchSummaryExportStore && BatchSummaryExportStore.loading ? BatchSummaryExportStore.loading : false,
    exportSummaryData: BatchSummaryExportStore && BatchSummaryExportStore.BatchSummaryExportData ? BatchSummaryExportStore.BatchSummaryExportData : '',
  }
}

const mapDispatchToProps = dispatch => ({
  TagSummaryDispatch: (params) => dispatch(TagSummaryAction.TagSummary(params)),
  getClientsWrmDispatch: (data) => dispatch(GetClientsActions.getClientsWrm(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  BatchSummaryExportDispatch: (params) => dispatch(BatchSummaryExportAction.BatchSummaryExport(params)),
  BatchSummaryClearDispatch: () => dispatch(BatchSummaryExportAction.BatchSummaryExportClear())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)