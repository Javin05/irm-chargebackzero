import React, { useEffect, useState, Fragment, useCallback } from 'react'
import _ from 'lodash'
import { WrmRealtimeSummaryAction, clientCredFilterActions, UpdatetagstatusAction, TagSummaryIdAction, TagSummaryUpdateAction, clientIdLIstActions, PostSendEmailAction, OGMactions, SummaryPlaytoreExportAction, exportClientWebActions, exportClientPlayStoreActions, GetClientsActions, WrmRealtimeSummaryExportAction } from '../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { getLocalStorage } from '../../utils/helper'
import { warningAlert, confirmAlert } from "../../utils/alerts"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { RISKSTATUS } from '../../utils/constants'
import SearchList from './searchList'
import moment from "moment"
import {
  STATUS_RESPONSE,
  DATE,
  REGEX,
} from '../../utils/constants'
import clsx from 'clsx'
import Modal from 'react-bootstrap/Modal'
import { DateSelector } from '../../theme/layout/components/DateSelector'
import { setTagSummaryData } from './formData'
import axiosInstance from "../../services"
import serviceList from "../../services/serviceList"

function WrmRealtimeSummary(props) {
  const {
    className,
    TagSummaryDispatch,
    TagSummaryData,
    loading,
    getExportDispatch,
    exportLoading,
    exportLists,
    clearExportListDispatch,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    UpdatetagstatusDispatch,
    TagSummaryUpdateDispatch,
    UpdatetagstatusData,
    CleartagstatusDispatch,
    TagSummaryIdData,
    ClearTagSummaryUpdateDispatch,
    TagSummaryupdateLoading,
    TagSummaryUpdateData,
    ClearIdDispatch,
    clinetIdLists,
    clientIdDispatch,
    PostSendEmailDispatch,
    PostSendEmailDataloading,
    PostSendEmailData,
    SendEmailClearDispatch,
    OGMpostDispatch,
    OGMpostDAta,
    clearOGMpostDispatch,
    OGMpostloading,
    playStoreExportResponse,
    getPlayStoreExportDispatch,
    clearPlayStoreExportDispatch,
    playStoreExportLoading,
    exportClientPlayStoreDispatch,
    exportClientWebDispatch,
    GetClientsRes,
    exportclientReports,
    exportClientPlayStoreReports,
    getClientsWrmDispatch,
  } = props

  const didMount = React.useRef(false)
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [show, setShow] = useState(false)
  const [showEmail, setShowEmail] = useState(false)
  const [showOGM, setShowOGM] = useState(false)
  const [Id, setId] = useState('')
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [searchParams, setSearchParams] = useState({})
  const [fileName, setFileName] = useState()
  const [sortingParams, setSortingParams] = useState()
  const [sorting, setSorting] = useState({
    tag: false,
    createdDate: false,
    rejected: false,
    total: false,
    manualreview: false
  })
  const [formData, setFormData] = useState([])
  const [searchData, setSearchData] = useState({})
  const [listId, setListID] = useState()
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [selectShow, setSelectShow] = useState(false)
  const [updateFormData, setupdateFormData] = useState({
    deliveryDate: '',
    rejectedCases: '',
    approvedCases: '',
    uploadedDate: '',
    file: '',
    tag: '',
    totalDeliveryCases: '',
  })

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }

  useEffect(() => {
    TagSummaryDispatch()
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params)
  }, [])

  useEffect(() => {
    if (GetClientsRes && !_.isEmpty(GetClientsRes && GetClientsRes._id)) {
      const data = {
        selecttag: "multiple"
      }
      exportClientWebDispatch(GetClientsRes && GetClientsRes._id, data)
      exportClientPlayStoreDispatch(GetClientsRes && GetClientsRes._id, data)
    }
  }, [GetClientsRes])

  useEffect(() => {
    if (!_.isEmpty(formData)) {
      const data = {
        tag: formData && formData[0]
      }
      getClientsWrmDispatch(data)
    }
  }, [formData])

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1)
      const params = {
        ...sortingParams,
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : '',
      }
      const pickByParams = _.pickBy(params)
      TagSummaryDispatch(pickByParams)
      setFilterFunctionDispatch(false)
      setSearchParams(currentFilterParams)
    }
  }, [setFilterFunction, setCredFilterParams])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      ...sortingParams,
      limit: limit,
      page: pageNumber,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : searchData.clientId ? searchData.clientId : "",
      deliveryDateFrom: searchData.deliveryDateFrom,
      deliveryDateTo: searchData.deliveryDateTo,
      status: searchData.status,
      tag: searchData.tag,
      uploadedDateFrom: searchData.uploadedDateFrom,
      uploadedDateTo: searchData.uploadedDateTo,
    }
    setActivePageNumber(pageNumber)
    TagSummaryDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC',
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : "",
      }
      setSortingParams(params)
      TagSummaryDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC',
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : "",
      }
      setSortingParams(params)
      TagSummaryDispatch(params)
    }
  }

  const exported = () => {
    const params = {
      tag: formData.toString()
    }
    getExportDispatch(params)
  }

  const clear = () => {
    setFormData([])
  }

  useEffect(() => {
    if (exportLists && exportLists.status === 'ok') {
      if (Array.isArray(exportLists && exportLists.data)) {
        const closeXlsx = document.getElementById('bulkCsvReport')
        closeXlsx.click()
        clearExportListDispatch()
      } else if (_.isEmpty(exportLists && exportLists.data)) {
        warningAlert(
          'error',
          exportLists && exportLists.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearExportListDispatch()
      } else if (!_.isEmpty(exportLists && exportLists.data.file)) {
        const DataUrl = exportLists && exportLists.data.file
        const data = DataUrl
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearExportListDispatch()
      }
    }else if (exportLists && exportLists.status === 'error') {
      warningAlert(
        'error',
        exportLists && exportLists.message,
        '',
        'Try again',
        '',
        () => { {} }
      )
      clearExportListDispatch()
    }
  }, [exportLists])

  useEffect(() => {
    if (UpdatetagstatusData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        UpdatetagstatusData.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      CleartagstatusDispatch()
    } else if (UpdatetagstatusData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        UpdatetagstatusData.message,
        '',
        'Ok'
      )
    }
    CleartagstatusDispatch()
  }, [UpdatetagstatusData])

  useEffect(() => {
    if (TagSummaryIdData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShow(true)
      const getIdData = TagSummaryIdData && TagSummaryIdData.data
      const data = setTagSummaryData(getIdData)
      setupdateFormData(data)
      setId(getIdData && getIdData._id)
    } else if (TagSummaryIdData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        TagSummaryIdData.message,
        '',
        'Ok'
      )
    }
  }, [TagSummaryIdData])

  useEffect(() => {
    if (TagSummaryUpdateData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        TagSummaryUpdateData.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      ClearTagSummaryUpdateDispatch()
      ClearIdDispatch()
      setShow(false)
      setupdateFormData({
        deliveryDate: '',
        rejectedCases: '',
        approvedCases: '',
        uploadedDate: '',
        file: ''
      })
      setFileName('')
    } else if (TagSummaryUpdateData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        TagSummaryUpdateData.message,
        '',
        'Ok'
      )
    }
    ClearTagSummaryUpdateDispatch()
  }, [TagSummaryUpdateData])

  useEffect(() => {
    if (PostSendEmailData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        PostSendEmailData.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      SendEmailClearDispatch()
      ClearIdDispatch()
      setShowEmail(false)
    } else if (PostSendEmailData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        PostSendEmailData.message,
        '',
        'Ok'
      )
    }
    SendEmailClearDispatch()
  }, [PostSendEmailData])

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const queue = getDefaultOption(AsigneesNames)
    if (!_.isEmpty(listId)) {
      const selOption = _.filter(queue, function (x) {
        if (_.includes(listId, x.value)) {
          setAsignees(x.label)
          return x
        }
      })
      setSelectedAsigneesOption(selOption)
    } else {
      setSelectedAsigneesOption()
    }
  }, [listId])

  const totalPages =
    TagSummaryData && TagSummaryData.data && TagSummaryData.data.count
      ? Math.ceil(parseInt(TagSummaryData && TagSummaryData.data && TagSummaryData.data.count) / limit)
      : 1

  useEffect(() => {
    if (OGMpostDAta.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        OGMpostDAta.message,
        'success',
        'ok',
        () => { { } },
        () => { { } }
      )
      TagSummaryDispatch()
      clearOGMpostDispatch()
      setShowOGM(false)
    } else if (OGMpostDAta.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        OGMpostDAta.message,
        '',
        'Ok'
      )
    }
    clearOGMpostDispatch()
  }, [OGMpostDAta])

  useEffect(() => {
    if (playStoreExportResponse && playStoreExportResponse.status === 'ok') {
      if (Array.isArray(playStoreExportResponse && playStoreExportResponse.data )) {
        const closeXlsx = document.getElementById('PlayStoreReport')
        closeXlsx.click()
        clearPlayStoreExportDispatch()
      } else if (_.isEmpty(playStoreExportResponse && playStoreExportResponse.data )) {
        warningAlert(
          'error',
          playStoreExportResponse && playStoreExportResponse.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearPlayStoreExportDispatch()
      } else if (!_.isEmpty(playStoreExportResponse && playStoreExportResponse.data.file)) {
        const data = playStoreExportResponse && playStoreExportResponse.data.file
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearPlayStoreExportDispatch()
      }
    }else if (playStoreExportResponse && playStoreExportResponse.status === 'error') {
      warningAlert(
        'error',
        playStoreExportResponse && playStoreExportResponse.message,
        '',
        'Try again',
        '',
        () => { {} }
      )
      clearPlayStoreExportDispatch()
    }
  }, [playStoreExportResponse])

  const playStorExported = () => {
    const params = {
      tag: formData.toString()
    }
    getPlayStoreExportDispatch(params)
  }

  useEffect(() => {
    return () => {
      ClearIdDispatch()
    }
  }, [])

  const handleCheckboxChange = useCallback((e) => {
    e.persist()
    if(e.target.checked === true) {
      const params = {
        ...searchData,
        skipPagination: 'true',
      };
      const url = serviceList.wrmRealtimeSummary;
      axiosInstance.get(url, { params }).then((response) => {
        if (response.data && response.data.status === 'ok') {
          const data = response.data.data.result;
          const result = data && data.map((item) =>{
            return item.tag
          })
          setFormData(result);
        }
      });
    }else if(e.target.checked === false) {
      setFormData([])
    }
  }, [searchData, setFormData, serviceList.wrmRealtimeSummary, axiosInstance]);  

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename={formData && formData.length > 1 ? 'web-report' : `${formData}-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="PlayStoreReport"
          className="download-table-xls-button"
          table="PlayStoreReport-table"
          filename={formData && formData.length > 1 ? 'playstore-report' : `${formData}-report`}
          sheet="tablexls"
        />
      </div>
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="table-to-xls">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              {
                exportclientReports && exportclientReports.data.map((item, i) => {
                  return (
                    <th>{item.report_value}</th>
                  )
                })
              }
            </tr>
          </thead>
          <tbody className="fs-10">
            {
              _.isArray(exportLists && exportLists.data) ?
                exportLists && exportLists.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      {
                        exportclientReports && exportclientReports.data.map((itemReport, i) => {
                          let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                          return (
                            _.isArray(reportTdData) ? (
                              <td>
                                {item[itemReport.report_key].toString()}
                              </td>
                            ) :
                              <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                          )
                        })
                      }
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="PlayStoreReport-table">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              {
                exportClientPlayStoreReports && exportClientPlayStoreReports.data.map((item) => {
                  return (
                    <th>{item.report_value}</th>
                  )
                })
              }
            </tr>
          </thead>
          <tbody className="fs-10">
            {
              _.isArray(playStoreExportResponse && playStoreExportResponse.data) ?
                playStoreExportResponse && playStoreExportResponse.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      {
                        exportClientPlayStoreReports && exportClientPlayStoreReports.data.map((itemReport, i) => {
                          let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                          return (
                            _.isArray(reportTdData) ? (
                              <td>
                                {item[itemReport.report_key].toString()}
                              </td>
                            ) :
                              <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                          )
                        })
                      }
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
      <div className={`card md-10 ${className}`} >
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1 ms-4'>
                {TagSummaryData && TagSummaryData.data && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total:
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3 ms-1'>
                      {TagSummaryData && TagSummaryData.data && TagSummaryData.data.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className='col-lg-6'>
              <div className='d-flex justify-content-end mt-4 me-4'>
                <SearchList setCredFilterParams={setCredFilterParams} setSearchData={setSearchData} TagSummaryData={TagSummaryData} setSelectShow = {setSelectShow} setFormDatas = {setFormData}/>
                {/* {
                  !_.isEmpty(formData) && Role !== 'Analyst' ? (
                    <>
                      <button
                        type='button'
                        className='btn btn-md btn-light-success btn-responsive pull-right w-150px'
                        onClick={(e) => exported(e)}
                        disabled={exportLoading}
                      >
                        {!exportLoading &&
                          <span className='indicator-label'>
                            <i className="bi bi-filetype-csv" />
                            Web Report
                          </span>
                        }
                        {exportLoading && (
                          <span className='indicator-progress text-success' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                      <button
                        type='button'
                        className='btn btn-sm btn-light-success btn-responsive font-5vw me-3 pull-right  ms-2'
                        onClick={(e) => playStorExported(e)}
                        disabled={playStoreExportLoading}
                      >
                        {!playStoreExportLoading &&
                          <span className='indicator-label'>
                            <i className="bi bi-filetype-csv" />
                            Play Store Report Export
                          </span>
                        }
                        {playStoreExportLoading && (
                          <span className='indicator-progress text-success' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    </>
                  )
                    : null
                } */}
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className='fw-bolder fs-7 text-gray-800'>
                <tr>
                {setCredFilterParams.clientId || selectShow ?
                  <th>
                    <div className="d-flex">
                      <span>Select all</span>
                    </div>
                    <div className='mt-4'>
                    {/* {selectShow ? */}
                    <div className="min-width-150px text-center form-check form-check-custom form-check-success form-check-solid">
                      <input
                        className="form-check-input cursor-pointer "
                        type="Checkbox"
                        onChange={(e) => handleCheckboxChange(e)}
                        name='checkNameSelectAll'
                      />
                    </div>
                      {/* : null} */}
                      </div>
                  </th> : null}
                  <th className="min-w-150px">
                    <div className="d-flex">
                      <span>Batch Uploaded Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("uploadedDate")}
                        >
                          <i
                            className={`bi ${sorting.uploadedDate
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-150px">
                    <div className="d-flex">
                      <span>Batch Delivery Date</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deliveryDate")}
                        >
                          <i
                            className={`bi ${sorting.deliveryDate
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Batch Name</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${sorting.tag
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Duplicate Cases</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("duplicate_counts")}
                        >
                          <i
                            className={`bi ${sorting.duplicate_counts
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Unique Cases</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("unique_counts")}
                        >
                          <i
                            className={`bi ${sorting.unique_counts
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Total Cases</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("totalCases")}
                        >
                          <i
                            className={`bi ${sorting.totalCases
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Case Approved</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("approvedCases")}
                        >
                          <i
                            className={`bi ${sorting.approvedCases
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Case Rejected</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("rejectedCases")}
                        >
                          <i
                            className={`bi ${sorting.rejectedCases
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Time Taken to Deliver</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("hoursConsumed")}
                        >
                          <i
                            className={`bi ${sorting.hoursConsumed
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Batch Type</span>
                      <div className="min-w-25px text-center">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("batchType")}
                        >
                          <i
                            className={`bi ${sorting.batchType
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-7'>
                {
                  !loading
                    ? (
                      Array.isArray(TagSummaryData &&
                        TagSummaryData.data && TagSummaryData.data.result)
                        ? (
                          TagSummaryData && TagSummaryData.data && TagSummaryData.data.result.map((item, i) => {

                            return (
                              <Fragment key={"IRM_" + i}>
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  {setCredFilterParams.clientId || selectShow  ?
                                    <td className="min-width-150px text-center form-check form-check-custom form-check-success form-check-solid">
                                      <input
                                        className="form-check-input cursor-pointer "
                                        type="Checkbox"
                                        value={item.tag}
                                        onChange={(e) => handleChange(e)}
                                        checked={formData && formData.includes(item.tag)}
                                        name='checkName'
                                      />
                                    </td> : null}
                                  <td>
                                    {
                                      item.uploadedDate ? item.uploadedDate : "--"
                                    }
                                  </td>
                                  <td>
                                    {
                                      item.resolvedDate ? item.resolvedDate : "--"
                                    }
                                  </td>
                                  <td>
                                    {item && item.tag ? item.tag : '--'}
                                  </td>
                                  <td>
                                    {
                                      _.includes(Role, 'Client User') ? (
                                        <span className={`badge  ${RISKSTATUS[item.status === 'HOLD' ? 'PROCESSING' : item.status]}`}>
                                          {item.status === 'HOLD' ? 'PROCESSING' : item.status}
                                        </span>
                                      ) : (
                                        <span className={`badge  ${RISKSTATUS[item.status]}`}>
                                          {item.status ? item.status : "--"}
                                        </span>
                                      )
                                    }
                                  </td>
                                  <td>
                                    {item && item.duplicate_counts ? item.duplicate_counts : '0'}
                                  </td>
                                  <td>
                                    {item && item.unique_counts ? item.unique_counts : '0'}
                                  </td>
                                  <td>
                                    {item && item.totalCases ? item.totalCases : '0'}
                                  </td>
                                  <td>
                                    {item && item.approvedCases && item.approvalRate ?`${item.approvedCases}(${Number(item.approvalRate).toFixed(2)}%)` : '0(0%)'}
                                  </td>
                                  <td>
                                    {item && item.rejectedCases && item.rejectionRate ?`${item.rejectedCases}(${Number(item.rejectionRate).toFixed(2)}%)` : '0(0%)'}
                                  </td>
                                  <td>
                                    {item && item.hoursConsumed ? item.hoursConsumed : '--'}
                                  </td>
                                  <td>
                                    {item && item.batchType ? item.batchType : "Live"}
                                  </td>
                                </tr>
                              </Fragment>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { WrmRealtimeSummaryStore, SummaryWebExportStore, UpdatetagstatusStore, TagSummaryIDStore, TagSummaryUpdateStore, clinetListStore, PostSendEmailStore, OGMStore, SummaryPlayExportStore, GetClientsStore } = state
  return {
    loading: WrmRealtimeSummaryStore && WrmRealtimeSummaryStore.loading ? WrmRealtimeSummaryStore.loading : false,
    TagSummaryData: WrmRealtimeSummaryStore && WrmRealtimeSummaryStore.WrmRealtimeSummaryData ? WrmRealtimeSummaryStore.WrmRealtimeSummaryData : {},
    exportLists: SummaryWebExportStore && SummaryWebExportStore.summaryWebExportData ? SummaryWebExportStore.summaryWebExportData : '',
    exportLoading: SummaryWebExportStore && SummaryWebExportStore.loading ? SummaryWebExportStore.loading : '',
    playStoreExportResponse: SummaryPlayExportStore && SummaryPlayExportStore.summaryPlayExportData ? SummaryPlayExportStore.summaryPlayExportData : '',
    playStoreExportLoading: SummaryPlayExportStore && SummaryPlayExportStore.loading ? SummaryPlayExportStore.loading : '',
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    UpdatetagstatusData: UpdatetagstatusStore && UpdatetagstatusStore.UpdatetagstatusData && UpdatetagstatusStore.UpdatetagstatusData.data ? UpdatetagstatusStore.UpdatetagstatusData.data : '',
    TagSummaryIdData: TagSummaryIDStore && TagSummaryIDStore.TagSummaryIdData ? TagSummaryIDStore.TagSummaryIdData : '',
    TagSummaryIdDataloading: TagSummaryIDStore && TagSummaryIDStore.loading ? TagSummaryIDStore.loading : false,
    TagSummaryupdateLoading: TagSummaryUpdateStore && TagSummaryUpdateStore.loading ? TagSummaryUpdateStore.loading : false,
    TagSummaryUpdateData: TagSummaryUpdateStore && TagSummaryUpdateStore.TagSummaryUpdateData ? TagSummaryUpdateStore.TagSummaryUpdateData : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    PostSendEmailData: PostSendEmailStore && PostSendEmailStore.PostSendEmailData ? PostSendEmailStore.PostSendEmailData : '',
    PostSendEmailDataloading: PostSendEmailStore && PostSendEmailStore.loading ? PostSendEmailStore.loading : false,
    OGMpostDAta: OGMStore && OGMStore.OGMpostDAta ? OGMStore.OGMpostDAta : '',
    OGMpostloading: OGMStore && OGMStore.loading ? OGMStore.loading : false,
    exportclientReports: state && state.webclientReportStore && state.webclientReportStore.exportclientReports,
    exportClientPlayStoreReports: state && state.exportClientPlayStoreReportStore && state.exportClientPlayStoreReportStore.exportClientPlayStoreReports,
    GetClientsRes: GetClientsStore && GetClientsStore.GetClientsRes ? GetClientsStore.GetClientsRes?.data : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  TagSummaryDispatch: (params) => dispatch(WrmRealtimeSummaryAction.WrmRealtimeSummary(params)),
  getExportDispatch: (params) => dispatch(WrmRealtimeSummaryExportAction.WrmRealtimeSummaryExport(params)),
  clearExportListDispatch: (data) => dispatch(WrmRealtimeSummaryExportAction.WrmRealtimeSummaryExportClear()),
  getPlayStoreExportDispatch: (data) => dispatch(SummaryPlaytoreExportAction.SummaryPlaytoreExport(data)),
  clearPlayStoreExportDispatch: (data) => dispatch(SummaryPlaytoreExportAction.SummaryPlaytoreExportClear(data)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
  UpdatetagstatusDispatch: (data) => dispatch(UpdatetagstatusAction.Updatetagstatus(data)),
  CleartagstatusDispatch: (data) => dispatch(UpdatetagstatusAction.UpdatetagstatusClear(data)),
  TagSummaryIdDispatch: (params) => dispatch(TagSummaryIdAction.TagSummaryID(params)),
  ClearIdDispatch: (params) => dispatch(TagSummaryIdAction.TagSummaryIdClear(params)),
  TagSummaryUpdateDispatch: (id, params) => dispatch(TagSummaryUpdateAction.TagSummaryUpdate(id, params)),
  ClearTagSummaryUpdateDispatch: (params) => dispatch(TagSummaryUpdateAction.TagSummaryUpdateClear(params)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  PostSendEmailDispatch: (data) => dispatch(PostSendEmailAction.PostSendEmail(data)),
  SendEmailClearDispatch: (data) => dispatch(PostSendEmailAction.PostSendEmailClear(data)),
  OGMpostDispatch: (data) => dispatch(OGMactions.OGMpost(data)),
  clearOGMpostDispatch: (data) => dispatch(OGMactions.clearOGMpost(data)),
  exportClientWebDispatch: (id, params) => dispatch(exportClientWebActions.exportClientWeb(id, params)),
  exportClientPlayStoreDispatch: (id, params) => dispatch(exportClientPlayStoreActions.exportClientPlayStore(id, params)),
  getClientsWrmDispatch: (data) => dispatch(GetClientsActions.getClientsWrm(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(WrmRealtimeSummary)
