import { USER_MANAGEMENT_ERROR, REGEX } from '../../utils/constants'

export const userRoleValidation = (values, setErrors) => {
  const errors = {}
  if (!values.role) {
    errors.role = USER_MANAGEMENT_ERROR.USER_ROLE_REQUIRED
  }
  if (!values.userTypeId) {
    errors.userTypeId = USER_MANAGEMENT_ERROR.USER_TYPE_REQUIRED
  }
  setErrors(errors)
  return errors
}
