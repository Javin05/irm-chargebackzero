import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { partnerActions } from '../../store/actions'
import { getUserPermissions } from '../../utils/helper'
import { Can } from '../../theme/layout/components/can'
import {
  STATUS_BADGES,
  SLICE_CHARACTERS,
  CREATE_PERMISSION,
  UPDATE_PERMISSION
} from '../../utils/constants'
import { TooltipOnHover } from '../../theme/layout/components/tooltipOnHover/TooltipOnHover'
import ReactPaginate from 'react-paginate'
import { Link, useLocation } from 'react-router-dom'

const Partners = (props) => {
  const {
    className,
    loadingGetPartner,
    getPartnerDispatch,
    getPartner,
    count
  } = props

  const url = useLocation().pathname
  const getUsersPermissions = getUserPermissions(url)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [limit, setLimit] = useState(25)
  const [sorting, setSorting] = useState({})

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getPartnerDispatch(params)
  }, [])

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: activePageNumber
    }
    getPartnerDispatch(params)
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getPartnerDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getPartnerDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getPartnerDispatch(params)
    }
  }
  const totalPages =
    count
      ? Math.ceil(parseInt(count) / limit)
      : 1

  return (
    <>
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6'>
              <div className='col-md-3 mt-1'>
                {count
                  ? (
                    <span className='text-muted fw-bold d-flex fs-5 mt-2'>
                      Total: &nbsp;{' '}
                      <span className='text-gray-700 fw-bolder text-hover-primary fs-5'>
                        {count}
                      </span>
                    </span>
                  )
                  : null}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='d-flex col-md-6 justify-content-end my-auto'>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <Link
                  to='/partner-onboarding'
                  className='btn btn-sm btn-light-primary me-3 font-5vw pull-right'
                >
                  {/* eslint-disable */}
                  <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                  Add Partner
                  {/* eslint-enable */}
                </Link>
              </Can>
            </div>
          </div>
          <div className='table-responsive'>
            <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={UPDATE_PERMISSION}
                  >
                    <th>
                      <div className='d-flex'>
                        <span>Action</span>
                      </div>
                    </th>
                  </Can>
                  <th>
                    <div className='d-flex'>
                      <span>Name</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('companyName')}
                        >
                          <i
                            className={`bi ${sorting.companyName
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Email</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientEmail')}
                        >
                          <i
                            className={`bi ${sorting.clientEmail
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Phone</span>
                      <div className='min-w-40px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientPhoneNumber')}
                        >
                          <i
                            className={`bi ${sorting.clientPhoneNumber
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Country</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientCountry')}
                        >
                          <i
                            className={`bi ${sorting.clientCountry
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>State</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientState')}
                        >
                          <i
                            className={`bi ${sorting.clientState
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>City</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientCity')}
                        >
                          <i
                            className={`bi ${sorting.clientCity
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Address</span>
                      <div className='min-w-75px text-end ellipsis'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientAddress')}
                        >
                          <i
                            className={`bi ${sorting.clientAddress
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Zip</span>
                      <div className='min-w-40px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('clientZip')}
                        >
                          <i
                            className={`bi ${sorting.clientZip
                              ? 'bi-arrow-up-circle-fill'
                              : 'bi-arrow-down-circle'
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex min-w-75px'>
                      <span>Partners Notes</span>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Status</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {!loadingGetPartner
                  ? (
                    getPartner && getPartner.length > 0
                      ? (
                        getPartner.map((partner, i) => {
                          return (
                            <tr
                              key={i}
                              style={
                                i === 0
                                  ? { borderColor: 'black' }
                                  : { borderColor: 'white' }
                              }
                            >
                              <Can
                                permissons={getUsersPermissions}
                                componentPermissions={UPDATE_PERMISSION}
                              >
                                <td className='text-center'>
                                  <Link
                                    to={`/partner-onboarding/update/${partner._id}`}
                                    className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px '
                                  >
                                    <KTSVG
                                      path='/media/icons/duotune/art/art005.svg'
                                      className='svg-icon-3'
                                    />
                                  </Link>
                                </td>
                              </Can>
                              <td>
                                {
                                  partner.partnerFirstName
                                    ? `${partner.partnerFirstName} ${partner.partnerLastName}`
                                    : '--'
                                }
                              </td>
                              <td>
                                {partner.partnerEmail && partner.partnerEmail ? partner.partnerEmail : '--'}
                              </td>
                              <td>
                                {partner.partnerPhoneNumber ? partner.partnerPhoneNumber : '--'}
                              </td>
                              <td>
                                {partner.partnerCountry ? partner.partnerCountry : '--'}
                              </td>
                              <td>
                                {partner.partnerState ? partner.partnerState : '--'}
                              </td>
                              <td>
                                {partner.partnerCity ? partner.partnerCity : '--'}
                              </td>
                              <td>
                                {partner.partnerAddress ? partner.partnerAddress : '--'}
                              </td>
                              <td>
                                {partner.partnerZip ? partner.partnerZip : '--'}
                              </td>
                              <td>
                                <TooltipOnHover
                                  string={partner.partnerNotes ? partner.partnerNotes : '--'}
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_35}
                                />
                              </td>
                              <td>
                                <span
                                  className={`badge ${STATUS_BADGES[partner.status]}`}
                                >
                                  {partner.status
                                    ? partner.status
                                    : '--'}
                                </span>
                              </td>
                            </tr>
                          )
                        })
                      )
                      : (
                        <tr className='text-center py-3'>
                          <td colSpan='100%'>No record(s) found</td>
                        </tr>
                      )
                  )
                  : (
                    <tr>
                      <td colSpan='100%' className='text-center'>
                        <div
                          className='spinner-border text-primary m-5'
                          role='status'
                        />
                      </td>
                    </tr>
                  )}
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { partnerStore } = state
  return {
    loadingGetPartner: partnerStore && partnerStore.loadingGetPartner ? partnerStore.loadingGetPartner : false,
    getPartner: partnerStore && partnerStore.getPartner ? partnerStore.getPartner : [],
    count: partnerStore && partnerStore.count ? partnerStore.count : 0,
    messageGetPartner: partnerStore && partnerStore.messageGetPartner ? partnerStore.messageGetPartner : '',
    statusGetPartner: partnerStore && partnerStore.statusGetPartner ? partnerStore.statusGetPartner : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPartnerDispatch: (data) => dispatch(partnerActions.getPartner(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Partners)