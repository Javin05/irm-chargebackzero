import _ from 'lodash'

export const setCompanyDetails = (data) => {
  if (!_.isEmpty(data)) {
    return {
      company: data.company,
      clientCountry: data.clientCountry,
      industry: data.industry && data.industry._id && data.industry._id,
      customerId: data.customerId,
      clientAddress: data.clientAddress,
      clientCity: data.clientCity,
      clientState: data.clientState,
      clientZip: data.clientZip,
      clientEmail: data.clientEmail,
      clientPhoneNumber: data.clientPhoneNumber,
      phoneNumberExtension: data.phoneNumberExtension,
      clientURL: data.clientURL,
      leadSource: data.leadSource,
      clientComplexity: data.clientComplexity,
      currency: data.currency,
      clientDescription: data.clientDescription,
      report_email: data.report_email,
      kycCreditLimit: data.kycCreditLimit,
      riskCreditLimit: data.riskCreditLimit,
      backend_api_key: data.backend_api_key,
      skip_category_validation: data.skip_category_validation ? data.skip_category_validation.toString() : data.skip_category_validation === false ? data.skip_category_validation.toString() : '',
      pma: data.pma ? data.pma.toString() : data.pma === false ? data.pma.toString() : '',
      batch_upload_email: data.batch_upload_email,
      logo: data.logo,
      blacklistCheck: data.blacklistCheck,
      baseCreate: data.baseCreate,
      accountsQ: data.accountsQ,
      kyc: data.kyc,
      wrm: data.wrm,
      wrmLimitPerDay: data.wrmLimitPerDay,
      wrmLimitPerWeek: data.wrmLimitPerWeek,
      emailSend: data.emailSend,
      ogmFrequency: data.ogmFrequency,
      categoryMerge: data.categoryMerge ? data.categoryMerge.toString() : data.categoryMerge === false ? data.categoryMerge.toString() : '',
      levelCategory: data.levelCategory ? data.levelCategory.toString() : data.levelCategory === false ? data.levelCategory.toString() : '',
      proposedCategory: data.proposedCategory,
      ogm_grouping: data.ogm_grouping,
      networkAnalysis: data.networkAnalysis,
      feedBackChat: data.feedBackChat,
      extraPolicyCheck: data.extraPolicyCheck,
      backendCache: data.backendCache ? data.backendCache.toString() : data.backendCache === false ? data.backendCache.toString() : '',
      kycPhoneVerification: data.kycPhoneVerification,
      kycEmailVerification: data.kycEmailVerification,
      wrmDuplicateUrlCheck: data.wrmDuplicateUrlCheck,
      edd_callback_report_status_url: data.edd_callback_report_status_url,
      edd_callback_report_url: data.edd_callback_report_url,
      edd_callback_status: data.edd_callback_status,
      edd_callback_report: data.edd_callback_report,
      edd_callback_report_api_key: data.edd_callback_report_api_key,
      wrm_callback_report_status_url: data.wrm_callback_report_status_url,
      wrm_callback_report_url: data.wrm_callback_report_url,
      wrm_callback_report_status: data.wrm_callback_report_status,
      wrm_callback_report: data.wrm_callback_report,
      withInBatchCheckDuplicate: data.withInBatchCheckDuplicate,
      wrm_website_days_check: data.wrm_website_days_check,
      wrm_callback_report_api_key: data.wrm_callback_report_api_key,
      ogm_callback_report_status_url: data.ogm_callback_report_status_url,
      ogm_callback_report_url: data.ogm_callback_report_url,
      ogm_callback_report_status: data.ogm_callback_report_status,
      ogm_callback_report: data.ogm_callback_report,
      ogm_callback_report_api_key: data.ogm_callback_report_api_key,
      ogm_auto_spilt_trigger: data.ogm_auto_spilt_trigger
    }
  }
}

export const getMerchantPayload = (data) => {
  if (!_.isEmpty(data)) {
    return {
      merchantName: data.merchantName,
      merchantCountry: data.country,
      industry: data.industry,
      merchantAddress: data.address,
      merchantCity: data.city,
      merchantState: data.state,
      merchantZip: data.zip,
      merchantEmail: data.email,
      merchantPhoneNumber: data.phoneNumber,
      phoneNumberExtension: data.phoneNumberExtension,
      externalId: data.externalId,
      companyDescription: data.companyDescription,
      mid: data.midNumber,
      descriptorName: data.descriptorName,
      descriptorId: data.descriptorId,
      doingBusinessAs: data.doingBusinessAs,
      acquirerBin: data.acquirerBin,
      caid: data.caid,
      storeName: data.storeName,
      storeType: data.storeType,
      signupUrl: data.signupUrl,
      tosUrl: data.tosUrl,
      checkoutUrl: data.checkoutUrl,
      returnPolicyUrl: data.returnPolicyUrl,
      tosPolicyScreenshot: data.tosPolicyScreenshot,
      refundPolicyScreenshot: data.refundPolicyScreenshot,
      checkoutScreenshot: data.checkoutScreenshot,
      shippingPolicyScreenshot: data.shippingPolicyScreenshot
    }
  }
}

export const setMerchantData = (data) => {
  if (!_.isEmpty(data)) {
    return {
      merchantName: data.merchantName,
      country: data.merchantCountry,
      industry: data.industry,
      address: data.merchantAddress,
      city: data.merchantCity,
      state: data.merchantState,
      zip: data.merchantZip,
      email: data.merchantEmail,
      phoneNumber: data.merchantPhoneNumber,
      phoneNumberExtension: data.phoneNumberExtension,
      externalId: data.externalId,
      companyDescription: data.companyDescription,
      midNumber: data.mid,
      descriptorName: data.descriptorName,
      descriptorId: data.descriptorId,
      doingBusinessAs: data.doingBusinessAs,
      acquirerBin: data.acquirerBin,
      caid: data.caid,
      storeName: data.storeName,
      storeType: data.storeType,
      signupUrl: data.signupUrl,
      tosUrl: data.tosUrl,
      checkoutUrl: data.checkoutUrl,
      returnPolicyUrl: data.returnPolicyUrl,
      tosPolicyScreenshot: data.tosPolicyScreenshot,
      refundPolicyScreenshot: data.refundPolicyScreenshot,
      checkoutScreenshot: data.checkoutScreenshot,
      shippingPolicyScreenshot: data.shippingPolicyScreenshot
    }
  }
}

export const getCompanyDetailsUpdatePayload = (data) => {
  if (!_.isEmpty(data)) {
    return {
      company: data.company,
      clientCountry: data.clientCountry,
      industry: data.industry,
      customerId: data.customerId,
      clientAddress: data.clientAddress,
      clientCity: data.clientCity,
      clientState: data.clientState,
      clientZip: data.clientZip,
      clientEmail: data.clientEmail,
      clientPhoneNumber: data.clientPhoneNumber,
      phoneNumberExtension: data.phoneNumberExtension,
      clientURL: data.clientURL,
      leadSource: data.leadSource,
      clientComplexity: data.clientComplexity,
      currency: data.currency,
      clientDescription: data.clientDescription,
      report_email: data.report_email,
      kycCreditLimit: data.kycCreditLimit,
      riskCreditLimit: data.riskCreditLimit,
      backend_api_key: data.backend_api_key,
      skip_category_validation: data.skip_category_validation,
      pma: data.pma,
      batch_upload_email: data.batch_upload_email,
      logo: data.logo,
      blacklistCheck: data.blacklistCheck,
      baseCreate: data.baseCreate,
      accountsQ: data.accountsQ,
      kyc: data.kyc,
      wrm: data.wrm,
      wrmLimitPerDay: data.wrmLimitPerDay,
      wrmLimitPerWeek: data.wrmLimitPerWeek,
      emailSend: data.emailSend,
      ogmFrequency: data.ogmFrequency,
      categoryMerge: data.categoryMerge,
      levelCategory: data.levelCategory,
      proposedCategory: data.proposedCategory,
      ogm_grouping: data.ogm_grouping,
      networkAnalysis: data.networkAnalysis,
      feedBackChat: data.feedBackChat,
      extraPolicyCheck: data.extraPolicyCheck,
      backendCache: data.backendCache,
      kycPhoneVerification: data.kycPhoneVerification,
      kycEmailVerification: data.kycEmailVerification,
      wrmDuplicateUrlCheck: data.wrmDuplicateUrlCheck,
      edd_callback_report_status_url: data.edd_callback_report_status_url,
      edd_callback_report_url: data.edd_callback_report_url,
      edd_callback_status: data.edd_callback_status,
      edd_callback_report: data.edd_callback_report,
      edd_callback_report_api_key: data.edd_callback_report_api_key,
      wrm_callback_report_status_url: data.wrm_callback_report_status_url,
      wrm_callback_report_url: data.wrm_callback_report_url,
      wrm_callback_report_status: data.wrm_callback_report_status,
      wrm_callback_report: data.wrm_callback_report,
      withInBatchCheckDuplicate: data.withInBatchCheckDuplicate,
      wrm_website_days_check: data.wrm_website_days_check,
      wrm_callback_report_api_key: data.wrm_callback_report_api_key,
      ogm_callback_report_status_url: data.ogm_callback_report_status_url,
      ogm_callback_report_url: data.ogm_callback_report_url,
      ogm_callback_report_status: data.ogm_callback_report_status,
      ogm_callback_report: data.ogm_callback_report,
      ogm_callback_report_api_key: data.ogm_callback_report_api_key,
      ogm_auto_spilt_trigger: data.ogm_auto_spilt_trigger
    }
  }
}

export const getUserDetailsPayload = (data) => {
  if (!_.isEmpty(data)) {
    return {
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      mobile: data.mobile,
      password: '',
      status: data.status,
      roleId: data.roleId && data.roleId._id && data.roleId._id
    }
  }
}

export const merchantPayload = (data, id) => {
  if (!_.isEmpty(data)) {
    return {
      merchantName: data.merchantName,
      merchantCountry: data.merchantCountry,
      industry: data.industry,
      merchantAddress: data.merchantAddress,
      merchantCity: data.merchantCity,
      merchantState: data.merchantState,
      merchantZip: data.merchantZip,
      merchantEmail: data.merchantEmail,
      merchantPhoneNumber: data.merchantPhoneNumber,
      phoneNumberExtension: data.phoneNumberExtension,
      externalId: data.externalId,
      company: data.company,
      clientId: id,
      companyDescription: data.companyDescription,
      mid: data.mid,
      descriptorName: data.descriptorName,
      descriptorId: data.descriptorId,
      doingBusinessAs: data.doingBusinessAs,
      acquirerBin: data.bin,
      caid: data.caId,
      storeName: data.storeName,
      storeType: data.storeType,
      signupUrl: data.signupUrl,
      tosUrl: data.tosUrl,
      checkoutUrl: data.checkoutUrl,
      returnPolicyUrl: data.returnPolicyUrl,
      tosPolicyScreenshot: data.tosPolicyScreenshot,
      refundPolicyScreenshot: data.refundPolicyScreenshot,
      checkoutScreenshot: data.checkoutScreenshot,
      shippingPolicyScreenshot: data.shippingPolicyScreenshot
    }
  }
}

export const getMIDCRMPayload = (data) => {
  if (!_.isEmpty(data)) {
    return {
      midNumber: data.midNumber,
      descriptorName: data.descriptorName,
      descriptorId: data.descriptorId,
      doingBusinessAs: data.doingBusinessAs,
      bin: data.bin,
      caId: data.caId,
      storeName: data.storeName,
      storeType: data.storeType,
      signupUrl: data.signupUrl,
      tosUrl: data.tosUrl,
      checkoutUrl: data.checkoutUrl,
      returnPolicyUrl: data.returnPolicyUrl,
      tosPolicyScreenshot: data.tosPolicyScreenshot,
      refundPolicyScreenshot: data.refundPolicyScreenshot,
      checkoutScreenshot: data.checkoutScreenshot,
      shippingPolicyScreenshot: data.shippingPolicyScreenshot
    }
  }
}

export const getProcessorPayload = (data) => {
  if (!_.isEmpty(data)) {
    const merchant = {
      merchantName: data.merchantName,
      merchantCountry: data.merchantCountry,
      industry: data.industry,
      merchantAddress: data.merchantAddress,
      merchantCity: data.merchantCity,
      merchantState: data.merchantState,
      merchantZip: data.merchantZip,
      merchantEmail: data.merchantEmail,
      merchantPhoneNumber: data.merchantPhoneNumber,
      phoneNumberExtension: data.phoneNumberExtension,
      externalId: data.externalId,
      companyDescription: data.companyDescription,
    }
    const mid = {
      mid: data.mid,
      descriptorName: data.descriptorName,
      descriptorId: data.descriptorId,
      doingBusinessAs: data.doingBusinessAs,
      acquirerBin: data.acquirerBin,
      caid: data.caid,
      storeName: data.storeName,
      storeType: data.storeType,
      signupUrl: data.signupUrl,
      tosUrl: data.tosUrl,
      checkoutUrl: data.checkoutUrl,
      returnPolicyUrl: data.returnPolicyUrl,
      tosPolicyScreenshot: data.tosPolicyScreenshot,
      refundPolicyScreenshot: data.refundPolicyScreenshot,
      checkoutScreenshot: data.checkoutScreenshot,
      shippingPolicyScreenshot: data.shippingPolicyScreenshot
    }
    return { merchant, mid }
  }
}