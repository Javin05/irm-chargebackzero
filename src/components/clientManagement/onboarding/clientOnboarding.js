import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import ShowFields from './ShowFields'
import { Stepper } from '../../../theme/layout/components/stepper/Stepper'
import CompanyDetails from './userDetails/companyDetails'
import UserInfo from './userDetails/userInfo'
import Users from './userDetails/Users'
import ScoreSetting from './userDetails/scoreSetting'
import { clientDetailsActions } from '../../../store/actions'
import Package from './userDetails/Package'
import Checkout from './userDetails/checkoutPage'
import { CRM_FORM } from '../../../utils/constants'
import { useLocation } from 'react-router-dom'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { Link } from "react-router-dom";
import ExportSetting from './userDetails/exportSetting'

function ClientOnboarding(props) {
  const { clientDetailsDispatch } = props
  const [completedSteps, setCompletedSteps] = useState([-1])
  const [activeStep, setActiveStep] = useState(0)
  const [array, setArray] = useState([])
  const [clientDetails, setClientDetails] = useState({})
  const [summary, setSummary] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [tab, setTab] = useState('COMPANY')
  const active = getLocalStorage(CRM_FORM.ACTIVE_STEP)

  const stepperArray = [
    {
      stepperLabel: 'Client Details',
      Optional: ''
    },
    {
      stepperLabel: 'Login Info',
      Optional: ''
    },
    {
      stepperLabel: 'Preview',
      Optional: ''
    }
  ]

  const stepperEditArray = [
    {
      stepperLabel: 'Client Details',
      Optional: ''
    },
    {
      stepperLabel: 'Users',
      Optional: ''
    },
    {
      stepperLabel: 'Packages',
      Optional: ''
    },
    {
      stepperLabel: 'Checkout',
      Optional: ''
    }
  ]

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update')
  const id = url && url[1]
  const urlName = pathName && pathName.split('update/')
  const clientId = urlName && urlName[1]

  const onClickNext = (currentId) => {
    const id = currentId - 1
    setActiveStep(currentId)
    if (completedSteps.length === currentId) {
      if (completedSteps && !completedSteps.includes(id)) {
        setCompletedSteps((values) => ([...values, id]))
        setLocalStorage(CRM_FORM.ACTIVE_STEP, currentId)
      }
    }
  }

  const goBack = (id) => {
    setActiveStep(id)
  }

  const getDescendants = (arr, step) => {
    const val = []
    arr.forEach((item, i) => {
      if (step > i) {
        val.push(i)
      }
    })
    return val
  }
  const d = getDescendants(array, active)
  const editstepper = getDescendants(array, array.length)

  useEffect(() => {
    if (active) {
      setActiveStep(Number(active))
      setCompletedSteps([-1, ...d])
    } else {
      setActiveStep(0)
      setCompletedSteps([-1])
    }
  }, [active])

  useEffect(() => {
    if (id) {
      setEditMode(true)
      setArray(stepperEditArray)
      clientDetailsDispatch(id)
    } else {
      setEditMode(false)
      setArray(stepperArray)
    }
  }, [id])

  useEffect(() => {
    if (editMode) {
      setActiveStep(0)
      setCompletedSteps([-1, ...editstepper])
    }
  }, [editMode])
  const showTabDetails = (type) => {
    switch (type) {
      case 'COMPANY':
        return <CompanyDetails clientId={clientId} />
      case 'USER':
        return <Users clientId={clientId} />
      case 'PACKAGE':
        return <Package clientId={clientId} />
      case 'SCORE':
        return <ScoreSetting clientId={clientId} module={"WRM"} />
      case 'EXPORT':
        return <ExportSetting clientId={clientId} module={"WRM"} />
      case 'WRMRT SCORE':
        return <ScoreSetting clientId={clientId} module={"WRMRT"} />
      case 'WRMRT EXPORT':
        return <ExportSetting clientId={clientId} module={"WRMRT"} />
    }
  }

  const handleTabs = (name) => {
    setTab(name)
  }
  return (
    <>
      {
        editMode ? (
          <>
            <div className='row'>
              <div className='col-md-12'>
                <div className='card card-nav-tabs mt-10'>
                  <div className='card-header card-header-primary'>
                    <div className='nav-tabs-navigation'>
                      <div className='nav-tabs-wrapper'>
                        <ul className='nav nav-tabs' data-tabs='tabs'>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link active' href='#companydetails' data-toggle='tab'
                              onClick={() => { handleTabs('COMPANY') }}
                            >
                              <i className='bi bi-building m-2 text-white' />
                              Company Details
                            </a>
                          </li>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#userinfo' data-toggle='tab'
                              onClick={() => { handleTabs('USER') }}
                            >
                              <i className='bi bi-person-circle m-2 text-white' />
                              Users
                            </a>
                          </li>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#SCORE' data-toggle='tab'
                              onClick={() => { handleTabs('SCORE') }}
                            >
                              <i className='bi bi-gear-fill m-2 text-white' />
                              Score Settings
                            </a>
                          </li>
                            <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#SCORE' data-toggle='tab'
                              onClick={() => { handleTabs('EXPORT') }}
                            >
                              <i className='bi bi-file-arrow-down-fill m-2 text-white' />
                              Export setting
                            </a>
                          </li>
                          <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#SCORE' data-toggle='tab'
                              onClick={() => { handleTabs('WRMRT SCORE') }}
                            >
                              <i className='bi bi-gear-fill m-2 text-white' />
                              Wrm Realtime Score Settings
                            </a>
                          </li>
                            <li className='client-nav-item'>
                            <a
                              className='nav-link' href='#SCORE' data-toggle='tab'
                              onClick={() => { handleTabs('WRMRT EXPORT') }}
                            >
                              <i className='bi bi-file-arrow-down-fill m-2 text-white' />
                              Wrm Realtime Export setting
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div>
                      <Link to='/client-management' className='btn btn-light-dark d-flex justify-content-start'>Back</Link>
                    </div>
                  </div>
                </div>
                <div className='card-body mt-4'>
                  <ShowFields>
                    {showTabDetails(tab)}
                  </ShowFields>
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className='row'>
              <div className='col-lg-11'>
                <Stepper
                  activeStep={activeStep}
                  setActiveStep={setActiveStep}
                  completedSteps={completedSteps}
                  stepperArr={array}
                  setCompletedSteps={setCompletedSteps}
                  onStepperClick={onClickNext}
                />
              </div>
              <div className='col-lg-1'>
                <Link to='/client-management' className='btn btn-light-dark d-flex justify-content-start'>Back</Link>
              </div>
            </div>
            <ShowFields>
              {activeStep === 0 ? <CompanyDetails onClickNext={onClickNext} goBack={goBack} setClientDetails={setClientDetails} setSummary={setSummary} /> : null}
              {activeStep === 1 ? <UserInfo onClickNext={onClickNext} goBack={goBack} setClientDetails={setClientDetails} setSummary={setSummary} /> : null}
              {activeStep === 2
                ? <Checkout
                  summary={summary}
                  onClickNext={onClickNext} goBack={goBack}
                  setClientDetails={setClientDetails} clientDetails={clientDetails}
                />
                : null}
            </ShowFields>
          </>
        )
      }
    </>
  )
}

const mapStateToProps = (state) => ({
  chargebacks: state && state.chargebackStore && state.chargebackStore.chargebacks,
  loading: state && state.chargebackStore && state.chargebackStore.loading,
  exports: state && state.exportStore && state.exportStore.exports ? state.exportStore.exports : []
})

const mapDispatchToProps = (dispatch) => ({
  clientDetailsDispatch: (data) => dispatch(clientDetailsActions.getClientDetails(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ClientOnboarding)