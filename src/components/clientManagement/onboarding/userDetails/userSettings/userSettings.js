import { useState } from 'react'
import { Stepper } from '../../../../../theme/layout/components/stepper/Stepper'
import ShowFields from '../../ShowFields'
import AlertIntelligence from './AlertIntelligence'
import ChargebackManagement from './ChargebackManagement'
import OrderIntelligence from './OrderIntelligence'

const UserSettings = (props) => {
  const [completedSteps, setCompletedSteps] = useState([])
  const [activeStep, setActiveStep] = useState(0)

  const stepperArray = [
    {
      stepperLabel: 'Alert Intelligence',
      Optional: ''
    },
    {
      stepperLabel: 'Chargeback Management',
      Optional: ''
    },
    {
      stepperLabel: 'Order Intelligence',
      Optional: ''
    }
  ]

  return (
    <>
      <div className='card'>
        <div className='card-body py-3'>
          <Stepper
            activeStep={activeStep}
            setActiveStep={setActiveStep}
            completedSteps={completedSteps}
            stepperArr={stepperArray}
            setCompletedSteps={setCompletedSteps}
          />
          <ShowFields>
            {activeStep === 0 ? <AlertIntelligence /> : null}
            {activeStep === 1 ? <ChargebackManagement /> : null}
            {activeStep === 2 ? <OrderIntelligence /> : null}
          </ShowFields>
        </div>
      </div>
    </>
  )
}

export default UserSettings