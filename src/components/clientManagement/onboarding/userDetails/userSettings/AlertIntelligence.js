import { useState } from 'react'
import ReactSelect from '../../../../../theme/layout/components/ReactSelect'
import color from '../../../../../utils/colors'

const AlertIntelligence = (props) => {
  const { loading } = props
  const [selectedClientOption, setSelectedClientOption] = useState('')
  const [clientOption] = useState()
  const [, setFormData] = useState({
    merchantName: '',
    country: '',
    industry: '',
    address: '',
    city: '',
    state: '',
    zip: '',
    email: '',
    phoneNumber: '',
    ext: '',
    url: '',
    companyDescription: ''
  })

  const [errors, setErrors] = useState({})

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const handleChangeClient = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedClientOption(selectedOption)
      setFormData((values) => ({
        ...values,
        companyName: selectedOption.value
      }))
      setErrors({ ...errors, companyName: '' })
    } else {
      setSelectedClientOption()
      setFormData((values) => ({ ...values, companyName: '' }))
    }
  }

  const handleSubmit = () => {}
  return (
    <>
      <div className='card-header bg-skyBlue py-10'>
        <div className='card-body'>
          <h2 className='mb-5'>Alert Intelligence</h2>
          <div className='form-group row mb-4'>
            <div className='col-lg-12 mb-3'>
              <div className='my-5'>
                <label className='form-check form-switch form-check-custom form-check-solid'>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    value='1'
                    checked='checked'
                  />
                  <span className='form-check-label fw-bold'>
                    Client-Managed Alerts
                  </span>
                </label>
              </div>
              <div className='my-5'>
                <label className='form-check form-switch form-check-custom form-check-solid'>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    value='1'
                    checked='checked'
                  />
                  <span className='form-check-label fw-bold'>
                    Duplicate Alert Credit
                  </span>
                </label>
              </div>
              <div className='my-5'>
                <label className='form-check form-switch form-check-custom form-check-solid'>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    value='1'
                    checked='checked'
                  />
                  <span className='form-check-label fw-bold'>
                    Send Alert Notifications
                  </span>
                </label>
              </div>
              <div className='my-5'>
                <label className='form-check form-switch form-check-custom form-check-solid'>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    value='1'
                    checked='checked'
                  />
                  <span className='form-check-label fw-bold'>RDR</span>
                </label>
              </div>
            </div>
          </div>
          <div className='form-group row mb-4'>
            <div className='col-lg-4 mb-3'>
              <label className='font-size-xs  font-weight-bold mb-2 form-label'>
                Dashboard Type:
              </label>
              <div className='col-lg-11'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='companyName'
                  className='basic-single'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeClient}
                  options={clientOption}
                  value={selectedClientOption}
                />
              </div>
              {errors && errors.companyName && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red;}'}</style>
                  {errors.companyName}
                </div>
              )}
            </div>
          </div>
          <div className='form-group row mb-4'>
            <div className='col-lg-12'>
              <button
                className='btn btn-info m-2'
                onClick={(event) => {
                  handleSubmit(event)
                }}
              >
                {loading
                  ? (
                    <span
                      className='spinner-border spinner-border-sm mx-3'
                      role='status'
                      aria-hidden='true'
                    />
                    )
                  : (
                      'Update'
                    )}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default AlertIntelligence