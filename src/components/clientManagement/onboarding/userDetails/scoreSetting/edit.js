import _ from 'lodash'

export const setStatusWeightage = (data) => {
  if (!_.isEmpty(data)) {
    return {
        riskLevel: data.riskLevel,
        weightage: data.weightage,
        decision: data.decision
    }
  }
}

export const setRiskyDomain = (data) => {
  if (!_.isEmpty(data)) {
    return {
        domainRegisterCompany: data.domainRegisterCompany,
        risk: data.risk
    }
  }
}

export const setGroupScore = (data) => {
  if (!_.isEmpty(data)) {
    return {
      groupName: data.groupName,
      weightage: data.weightage,
      highRisk: data.highRisk,
      highRiskScore: data.highRiskScore,
      totalHighRisk: data.totalHighRisk,
      totalHighRiskScore: data.totalHighRiskScore
    }
  }
}

export const setRiskScore = (data) => {
  if (!_.isEmpty(data)) {
    return {
      groupId: data.groupId,
      weightage: data.weightage,
      highRisk: data.highRisk,
      highRiskScore: data.highRiskScore,
      groupName: data.groupName,
      report: data.riskName,
      hardBlock: data.hardBlock,
      productLinkCheck: data.productLinkCheck
    }
  }
}