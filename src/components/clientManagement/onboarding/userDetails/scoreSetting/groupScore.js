import { useEffect, useState } from 'react'
import { GroupScoreValidation } from './validation'
import { useLocation } from 'react-router-dom'
import { KTSVG } from '../../../../../theme/helpers'
import { REGEX, STATUS_RESPONSE, RISKSTATUS, SWEET_ALERT_MSG } from '../../../../../utils/constants'
import {
  GroupScoreWeightAgeActions,
  GroupScoreWeightAgePostActions,
  GroupScoreWeightAgeEditActions,
  GroupScoreWeightAgeUpdateActions,
  GroupScoreWeightAgeDeleteActions
} from '../../../../../store/actions'
import { connect } from 'react-redux'
import _ from 'lodash'
import { confirmationAlert, warningAlert } from '../../../../../utils/alerts'
import ReactPaginate from 'react-paginate'
import Modal from 'react-bootstrap/Modal'
import clsx from 'clsx'
import { setGroupScore } from './edit'
import RiskScoreWeightAge from './riskScore'
import ReactSelect from "../../../../../theme/layout/components/ReactSelect"
import color from "../../../../../utils/colors"

const groupNameData = {
  "status": "ok",
  "data": [
    {
      "_id": "62a9a6e6417b8b0004aa564d",
      "name": "Web Security",
    },
    {
      "_id": "6273a20fb37bf400048ffdae",
      "name": "Online Website Reputation",
    },
    {
      "_id": "623ee53d301f7f812175e212",
      "name": "Website Content Checkup Domain Health",
    },
    {
      "_id": "623ee53d301f7f812175e213",
      "name": "Website Health",
    },
    {
      "_id": "623ee53d301f7f812175e214",
      "name": "Transparency Risk",
    },
    {
      "_id": "623ee53d301f7f812175e215",
      "name": "Web Content Risk",
    },
    {
      "_id": "623ee53d301f7f812175e216",
      "name": "Business Risk",
    }
  ],
  "count": 3
}

const GroupScoreWeightAge = (props) => {
  const {
    loading,
    GroupScoreWeightAge,
    getGroupScoreWeightAgeDispatch,
    postGroupScoreWeightAgeDispatch,
    GroupScoreWeightAgePost,
    GroupScoreWeightAgePostLoading,
    clearGroupScoreWeightAgePostDispatch,
    EditGroupScoreWeightAgeDispatch,
    GroupScoreWeightAgeEdit,
    clearGroupScoreWeightAgeEditDispatch,
    UpdateGroupScoreWeightAgeDispatch,
    GroupScoreWeightAgeUpdate,
    clearGroupScoreWeightAgeUpdateDispatch,
    DeleteGroupScoreWeightAgeDispatch,
    GroupScoreWeightAgeDelete,
    clearGroupScoreWeightAgeDeleteDispatch,
    module
  } = props

  const [isFormUpdated, setFormUpdated] = useState(false)
  const [formData, setFormData] = useState({
    groupName: '',
    weightage: '',
    highRisk: '',
    highRiskScore: '',
    totalHighRisk: '',
    totalHighRiskScore: '',
  })

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [show, setShow] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [limit] = useState(25)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const id = url && url[3]
  const [grouNameOption, setGroupNameOption] = useState()
  const [selectedGroupNameOption, setSelectedGroupNameOption] = useState('')

  useEffect(() => {
    const params = {
      clientId: id,
      limit: limit,
      page: activePageNumber
    }
    getGroupScoreWeightAgeDispatch(params, module)
    clearGroupScoreWeightAgeEditDispatch()
  }, [])

  const handleChange = (e) => {
    e.persist()
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleSubmit = () => {
    const errorMsg = GroupScoreValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      if (!editMode) {
        const params = {
          clientId: id,
          groupName: formData.groupName,
          weightage: formData.weightage,
          highRisk: formData.highRisk,
          highRiskScore: formData.highRiskScore,
          totalHighRisk: formData.totalHighRisk,
          totalHighRiskScore: formData.totalHighRiskScore
        }
        postGroupScoreWeightAgeDispatch(params, module)
      } else {
        const listId = GroupScoreWeightAgeEdit && GroupScoreWeightAgeEdit.data && GroupScoreWeightAgeEdit.data._id
        const params = {
          clientId: id,
          groupName: formData.groupName,
          weightage: formData.weightage,
          highRisk: formData.highRisk,
          highRiskScore: formData.highRiskScore,
          totalHighRisk: formData.totalHighRisk,
          totalHighRiskScore: formData.totalHighRiskScore
        }
        UpdateGroupScoreWeightAgeDispatch(listId, params, module)
      }
    }
  }

  const handleSorting = (name) => { }

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false)
        handleSubmit()
      }
    }
  }, [isFormUpdated])

  useEffect(() => {
    const groupName = getDefaultOptions(groupNameData && groupNameData.data)
    setGroupNameOption(groupName)
    if (!_.isEmpty(formData.groupName)) {
      const selOption = _.filter(groupName, function (x) { if (_.includes(formData.groupName._id, x.value)) { return x } })
      setSelectedGroupNameOption(selOption)
    }
  }, [groupNameData])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    if (GroupScoreWeightAgeEdit && GroupScoreWeightAgeEdit.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = setGroupScore(GroupScoreWeightAgeEdit && GroupScoreWeightAgeEdit.data)
      const editData = GroupScoreWeightAgeEdit && GroupScoreWeightAgeEdit.data
      if (editData && editData._id) {
        const groupName = getDefaultOptions(groupNameData && groupNameData.data)
        if (!_.isEmpty(editData.groupName)) {
          const selOption = _.filter(groupName, function (x) {
            if (_.includes(editData.groupName, x.label)) {
              return x
            }
          })
          setSelectedGroupNameOption(selOption)
        }
      }
      setFormData(data)
      setEditMode(true)
      setShow(true)
    } else if (GroupScoreWeightAgeEdit && GroupScoreWeightAgeEdit.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        GroupScoreWeightAgeEdit && GroupScoreWeightAgeEdit.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearGroupScoreWeightAgeEditDispatch()
    }
  }, [GroupScoreWeightAgeEdit])

  const onConfirm = () => {
    setShow(false)
    const params = {
      clientId: id
    }
    getGroupScoreWeightAgeDispatch(params, module)
  }

  useEffect(() => {
    if (GroupScoreWeightAgePost && GroupScoreWeightAgePost.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        GroupScoreWeightAgePost && GroupScoreWeightAgePost.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearGroupScoreWeightAgePostDispatch()
    } else if (GroupScoreWeightAgePost && GroupScoreWeightAgePost.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        GroupScoreWeightAgePost && GroupScoreWeightAgePost.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearGroupScoreWeightAgePostDispatch()
    }
  }, [GroupScoreWeightAgePost])

  useEffect(() => {
    if (GroupScoreWeightAgeUpdate && GroupScoreWeightAgeUpdate.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        GroupScoreWeightAgeUpdate && GroupScoreWeightAgeUpdate.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      setShow(false)
      clearGroupScoreWeightAgeUpdateDispatch()
      clearGroupScoreWeightAgeEditDispatch()
    } else if (GroupScoreWeightAgeUpdate && GroupScoreWeightAgeUpdate.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        GroupScoreWeightAgeUpdate && GroupScoreWeightAgeUpdate.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearGroupScoreWeightAgeUpdateDispatch()
    }
  }, [GroupScoreWeightAgeUpdate])

  useEffect(() => {
    if (GroupScoreWeightAgeDelete && GroupScoreWeightAgeDelete.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        GroupScoreWeightAgeDelete && GroupScoreWeightAgeDelete.message,
        'success',
        'Back to Page',
        'Ok',
        () => {
          onConfirm()
        },
        () => {
          onConfirm()
        }
      )
      clearGroupScoreWeightAgeDeleteDispatch()
    } else if (GroupScoreWeightAgeDelete && GroupScoreWeightAgeDelete.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        GroupScoreWeightAgeDelete && GroupScoreWeightAgeDelete.message,
        '',
        'Cancel',
        'Ok',
        () => { },
        () => { }
      )
      clearGroupScoreWeightAgeDeleteDispatch()
    }
  }, [GroupScoreWeightAgeDelete])

  const handleEdit = (id) => {
    EditGroupScoreWeightAgeDispatch(id, module)
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_LIST,
      'warning',
      'Yes',
      'No',
      () => { DeleteGroupScoreWeightAgeDispatch(id, module) },
      () => { { } }
    )
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: id
    }
    setActivePageNumber(pageNumber)
    getGroupScoreWeightAgeDispatch(params, module)
  }

  const clearPopup = () => {
    setShow(false)
    setFormData(values => ({
      groupName: '',
      weightage: '',
      highRisk: '',
      highRiskScore: '',
      totalHighRisk: '',
      totalHighRiskScore: '',
    }))
    setSelectedGroupNameOption('')
    clearGroupScoreWeightAgeEditDispatch()
  }

  const handleChangeGropuName = selectedOption => {
    if (selectedOption !== null) {
      setSelectedGroupNameOption(selectedOption)
      setFormData(values => ({ ...values, groupName: selectedOption.label }))
    } else {
      setFormData(values => ({ ...values, groupName: '' }))
    }
    setErrors({ ...errors, groupName: '' })
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  const totalPages =
    GroupScoreWeightAge && GroupScoreWeightAge.count
      ? Math.ceil(parseInt(GroupScoreWeightAge && GroupScoreWeightAge.count) / limit)
      : 1

  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Group Name :
                </label>
              </div>
              <div className='col-md-8'>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='groupName'
                  placeholder="Select..."
                  className="basic-single"
                  classNamePrefix="select"
                  handleChangeReactSelect={handleChangeGropuName}
                  options={grouNameOption}
                  value={selectedGroupNameOption}
                isDisabled={!grouNameOption}
                />
                {errors && errors.groupName && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.groupName}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Weight Age :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Weight Age'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.weightage && errors.weightage },
                    {
                      'is-valid': formData.weightage && !errors.weightage
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='weightage'
                  autoComplete='off'
                  value={formData.weightage || ''}
                  onKeyPress={(e) => {
                    if (
                      !REGEX.NUMERIC.test(e.key)
                    ) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.weightage && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.weightage}
                  </div>
                )}
              </div>
            </div>
            <div className='row mt-4'>
              <div className='col-lg-4 mb-3'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  High Risk :
                </label>
              </div>
              <div className='col-lg-8'>
                <div className="form-check form-check-custom form-check-solid mb-4">
                  <label className='d-flex flex-stack mb-5 cursor-pointer'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='Yes'
                        name='highRisk'
                        checked={formData.highRisk === 'Yes'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        Yes
                      </span>
                    </span>
                  </label>
                  <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='No'
                        name='highRisk'
                        checked={formData.highRisk === 'No'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        No
                      </span>
                    </span>
                  </label>
                </div>
                {errors && errors.highRisk && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.highRisk}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  HighRisk Score :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='HighRisk Score'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.highRiskScore && errors.highRiskScore },
                    {
                      'is-valid': formData.highRiskScore && !errors.highRiskScore
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='highRiskScore'
                  autoComplete='off'
                  value={formData.highRiskScore || ''}
                  onKeyPress={(e) => {
                    if (
                      !REGEX.NUMERIC.test(e.key)
                    ) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.highRiskScore && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.highRiskScore}
                  </div>
                )}
              </div>
            </div>
            <div className='row mt-4'>
              <div className='col-lg-4 mb-3'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Total High Risk :
                </label>
              </div>
              <div className='col-lg-8'>
                <div className="form-check form-check-custom form-check-solid mb-4">
                  <label className='d-flex flex-stack mb-5 cursor-pointer'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='Yes'
                        name='totalHighRisk'
                        checked={formData.totalHighRisk === 'Yes'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        Yes
                      </span>
                    </span>
                  </label>
                  <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                    <span className='form-check form-check-custom form-check-solid me-2'>
                      <input
                        className='form-check-input'
                        type='radio'
                        onChange={(e) => handleChange(e)}
                        value='No'
                        name='totalHighRisk'
                        checked={formData.totalHighRisk === 'No'}
                      />
                    </span>
                    <span className='d-flex flex-column'>
                      <span className='fs-7 text-muted'>
                        No
                      </span>
                    </span>
                  </label>
                </div>
                {errors && errors.totalHighRisk && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.totalHighRisk}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Total HighRisk Score :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Total HighRisk Score'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': formData.totalHighRiskScore && errors.totalHighRiskScore },
                    {
                      'is-valid': formData.totalHighRiskScore && !errors.totalHighRiskScore
                    }
                  )}
                  onChange={(e) => handleChange(e)}
                  type='text'
                  name='totalHighRiskScore'
                  autoComplete='off'
                  value={formData.totalHighRiskScore || ''}
                  onKeyPress={(e) => {
                    if (
                      !REGEX.NUMERIC.test(e.key)
                    ) {
                      e.preventDefault()
                    }
                  }}
                />
                {errors && errors.totalHighRiskScore && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.totalHighRiskScore}
                  </div>
                )}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4' />
              <div className='col-md-8'>
                <button
                  type='button'
                  className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                  onClick={(e) => handleSubmit(e)}
                  disabled={GroupScoreWeightAgePostLoading}
                >
                  {!GroupScoreWeightAgePostLoading && <span className='indicator-label'>Submit</span>}
                  {GroupScoreWeightAgePostLoading && (
                    <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                      Please wait...
                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className='card h-auto'>
        <div className='card-body py-3'>
          <div className='row'>
            <div className='col-md-6'>
              <h3 className='fs-2 m-4 d-flex justify-content-start'>Group Score Weightage</h3>
            </div>
            <div className='col-md-6'>
              <div className='d-flex justify-content-end m-4'>
                <button
                  className='btn btn-light-primary btn-sm'
                  onClick={() => setShow(true)}
                >
                  Add
                </button>
              </div>
            </div>
          </div>
          <div className='table-responsive'>
            <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className='d-flex'>
                      <span>GroupName</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('groupName')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>HighRisk</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('highRisk')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>HighRisk Score</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('highRiskScore')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Total High Risk</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('totalHighRisk')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Total High Risk Score</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('totalHighRiskScore')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Weightage</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('weightage')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Weightage percentage</span>
                      <div className='min-w-25px text-end'>
                        <div
                          className='cursor-pointer'
                          onClick={() => handleSorting('weightage_percentage')}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>status</span>
                    </div>
                  </th>
                  <th>
                    <div className='d-flex'>
                      <span>Action</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      _.isArray(GroupScoreWeightAge && GroupScoreWeightAge.result)
                        ? (
                          GroupScoreWeightAge && GroupScoreWeightAge.result.map((riskItem, _id) => {
                            return (
                              <tr
                                key={_id}
                                style={
                                  _id === 0
                                    ? { borderColor: 'black' }
                                    : { borderColor: 'white' }
                                }
                              >
                                <td>
                                  {riskItem.groupName ? riskItem.groupName : '--'}
                                </td>
                                <td>
                                  {riskItem.highRisk ? riskItem.highRisk : '--'}
                                </td>
                                <td>
                                  {riskItem.highRiskScore ? riskItem.highRiskScore : '--'}
                                </td>
                                <td>
                                  {riskItem.totalHighRisk ? riskItem.totalHighRisk : '--'}
                                </td>
                                <td>
                                  {riskItem.totalHighRiskScore ? riskItem.totalHighRiskScore : '--'}
                                </td>
                                <td>
                                  {riskItem.weightage ? riskItem.weightage : '--'}
                                </td>
                                <td>
                                  {riskItem.weightage_percentage ? riskItem.weightage_percentage+" %" : '--'}
                                </td>
                                <td>
                                  <span className={`badge ${RISKSTATUS[riskItem.status && riskItem.status]}`}>
                                    {riskItem.status ? riskItem.status : "--"}
                                  </span>
                                </td>
                                <td>
                                  <td className="pb-0 pt-5 text-start">
                                    <button
                                      className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4'
                                      onClick={() => handleEdit(riskItem._id)}
                                      title="Edit customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                    <button
                                      className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                      onClick={() => onDeleteItem(riskItem._id)}
                                      title="Delete customer"
                                    >
                                      <KTSVG
                                        path='/media/icons/duotune/general/gen027.svg'
                                        className='svg-icon-3'
                                      />
                                    </button>
                                  </td>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className='form-group row mb-4 mt-6'>
            <div className='col-lg-12 mb-4 align-items-end d-flex'>
              <div className='col-lg-12'>
                <ReactPaginate
                  nextLabel='Next >'
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel='< Prev'
                  pageClassName='page-item'
                  pageLinkClassName='page-link'
                  previousClassName='page-item'
                  previousLinkClassName='page-link'
                  nextClassName='page-item'
                  nextLinkClassName='page-link'
                  breakLabel='...'
                  breakClassName='page-item'
                  breakLinkClassName='page-link'
                  containerClassName='pagination'
                  activeClassName='active'
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <RiskScoreWeightAge module={module}/>
    </>
  )
}

const mapStateToProps = (state) => {
  const { GroupScoreWeightAgePostStore, GroupScoreWeightAgeEditStore, GroupScoreWeightAgeUpdateStore, GroupScoreWeightAgeDeleteStore } = state
  return {
    GroupScoreWeightAge: state && state.GroupScoreWeightAgegetStore && state.GroupScoreWeightAgegetStore.GroupScoreWeightAge ? state.GroupScoreWeightAgegetStore.GroupScoreWeightAge : [],
    loading: state && state.GroupScoreWeightAgegetStore && state.GroupScoreWeightAgegetStore.loading,
    GroupScoreWeightAgePost: GroupScoreWeightAgePostStore && GroupScoreWeightAgePostStore.GroupScoreWeightAgePost ? GroupScoreWeightAgePostStore.GroupScoreWeightAgePost : '',
    GroupScoreWeightAgePostLoading: GroupScoreWeightAgePostStore && GroupScoreWeightAgePostStore.loading ? GroupScoreWeightAgePostStore.loading : false,
    GroupScoreWeightAgeEdit: GroupScoreWeightAgeEditStore && GroupScoreWeightAgeEditStore.GroupScoreWeightAgeEdit ? GroupScoreWeightAgeEditStore.GroupScoreWeightAgeEdit : '',
    GroupScoreWeightAgeUpdate: GroupScoreWeightAgeUpdateStore && GroupScoreWeightAgeUpdateStore.GroupScoreWeightAgeUpdate ? GroupScoreWeightAgeUpdateStore.GroupScoreWeightAgeUpdate : '',
    GroupScoreWeightAgeDelete: GroupScoreWeightAgeDeleteStore && GroupScoreWeightAgeDeleteStore.GroupScoreWeightAgeDelete ? GroupScoreWeightAgeDeleteStore.GroupScoreWeightAgeDelete : '',
  }
}


const mapDispatchToProps = (dispatch) => ({
  getGroupScoreWeightAgeDispatch: (params, module) => dispatch(GroupScoreWeightAgeActions.getGroupScoreWeightAge(params, module)),
  postGroupScoreWeightAgeDispatch: (params, module) => dispatch(GroupScoreWeightAgePostActions.postGroupScoreWeightAge(params, module)),
  clearGroupScoreWeightAgePostDispatch: () => dispatch(GroupScoreWeightAgePostActions.clearGroupScoreWeightAgePost()),
  EditGroupScoreWeightAgeDispatch: (params, module) => dispatch(GroupScoreWeightAgeEditActions.EditGroupScoreWeightAge(params, module)),
  clearGroupScoreWeightAgeEditDispatch: () => dispatch(GroupScoreWeightAgeEditActions.clearGroupScoreWeightAgeEdit()),
  UpdateGroupScoreWeightAgeDispatch: (id, params, module) => dispatch(GroupScoreWeightAgeUpdateActions.UpdateGroupScoreWeightAge(id, params, module)),
  clearGroupScoreWeightAgeUpdateDispatch: (id, params) => dispatch(GroupScoreWeightAgeUpdateActions.clearGroupScoreWeightAgeUpdate(id, params)),
  DeleteGroupScoreWeightAgeDispatch: (params, module) => dispatch(GroupScoreWeightAgeDeleteActions.DeleteGroupScoreWeightAge(params, module)),
  clearGroupScoreWeightAgeDeleteDispatch: (params) => dispatch(GroupScoreWeightAgeDeleteActions.clearGroupScoreWeightAgeDelete(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(GroupScoreWeightAge)