export const StatusWeightageValidation = (values, setErrors) => {
  const errors = {}
  if (!values.riskLevel) {
    errors.riskLevel = "Risk Lvevel is Required"
  }
  if (!values.weightage) {
    errors.weightage = "Weight Cage is Required"
  }
  if (!values.decision) {
    errors.decision = "Decision is Required"
  }
  setErrors(errors)
  return errors
}

export const RiskyDomainValidation = (values, setErrors) => {
  const errors = {}
  if (!values.domainRegisterCompany) {
    errors.domainRegisterCompany = "Domain Register Company is Required"
  }
  if (!values.risk) {
    errors.risk = "Risk is Required"
  }
  setErrors(errors)
  return errors
}

export const GroupScoreValidation = (values, setErrors) => {
  const errors = {}
  if (!values.groupName) {
    errors.groupName = "Group Name is Required"
  }
  if (!values.weightage) {
    errors.weightage = "Weight Age is Required"
  }
  setErrors(errors)
  return errors
}

export const RiskScoreValidation = (values, setErrors) => {
  const errors = {}
  if (!values.groupId) {
    errors.groupId = "Group Id is Required"
  }
  if (!values.weightage) {
    errors.weightage = "Weight Age is Required"
  }
  setErrors(errors)
  return errors
}