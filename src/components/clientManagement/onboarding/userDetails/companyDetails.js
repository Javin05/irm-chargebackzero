import { useEffect, useState } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { companyValidation } from "./validation";
import ReactSelect from "../../../../theme/layout/components/ReactSelect";
import {
  CRM_FORM,
  REGEX,
  STATUS_RESPONSE,
  currencyOptions,
  FILE_FORMAT_IMAGES,
  DROPZONE_MESSAGES,
  API_URL,
} from "../../../../utils/constants";
import { setLocalStorage, getLocalStorage } from "../../../../utils/helper";
import { warningAlert, confirmAlert } from "../../../../utils/alerts";
import color from "../../../../utils/colors";
import { useLocation } from "react-router-dom";
import {
  setAddressDetails,
  getAdressValues,
  handleTrimWhiteSpace,
  handleTrimSpaceOnly,
} from "../functions/functions";
import {
  setCompanyDetails,
  getCompanyDetailsUpdatePayload,
} from "../functions/formData";
import {
  CountryActions,
  StateActions,
  CityActions,
  clientDetailsActions,
  editClientsActions,
  industryActions,
  addApiKeyActions,
  apiKeyActions,
  addMerchantUploadActions,
} from "../../../../store/actions";
import { KTSVG } from "../../../../theme/helpers";
import { Modal } from "../../../../theme/layout/components/modal";
import Dropzone, { useDropzone } from "react-dropzone";
import styled from "styled-components";

const StyledDiv = styled.div`
  .container {
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 14px;
    width: 100%;
    height: 46px;
    border-width: 2px;
    border-radius: 2px;
    border-style: dashed;
    background-color: #fafafa;
    color: #9ab6d9;
    outline: none;
    transition: border 0.24s ease-in-out;
  }
`;

const CompanyDetails = (props) => {
  const {
    getCountryDispatch,
    getCountrys,
    loadingCountry,
    getStateDispatch,
    getStates,
    loadingState,
    getCityDispatch,
    getCitys,
    loadingCity,
    clientDetailsDispatch,
    dataCD,
    statusCD,
    loadingClientDetails,
    editClientsDispatch,
    cleareditClientsDispatch,
    clearClientDetailsDispatch,
    onClickNext,
    setSummary,
    setClientDetails,
    statusECL,
    messageECL,
    loadingECL,
    getIndustryDispatch,
    getIndustry,
    addApiKeyDispatch,
    clearAddApiKeyDispatch,
    getApiKeyDispatch,
    getApiKey,
    loadingApi,
    statusAAK,
    messageAAK,
    loadingAAK,
    addMerchantUploadDispatch,
    clearaddMerchantUploadDispatch,
    dataAMUpload,
    loadingAMUpload,
    statusAMUpload,
  } = props;

  const { getRootProps } = useDropzone();
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update");
  const id = url && url[1];
  const [isFormUpdated, setFormUpdated] = useState(false);
  const [countryOption, setCountryOption] = useState();
  const [selectedCountryOption, setSelectedCountryOption] = useState("");
  const [stateOption, setStateOption] = useState();
  const [selectedStateOption, setSelectedStateOption] = useState("");
  const [cityOptions, setCityOptions] = useState();
  const [selectedCityOption, setSelectedCityOption] = useState("");
  const [selectedCurrencyOption, setSelectedCurrencyOption] = useState();
  const [selectedIndustryOption, setSelectedIndustryOption] = useState("");
  const [typingTimeout, setTypingTimeout] = useState(0);
  const [industryOption, setIndustryOption] = useState();
  const [errors, setErrors] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [showForm, setShowForm] = useState(true);
  const [genKeyExist, setGenKeyExist] = useState(false);
  const [show, setShow] = useState(false);
  const [showEye, setShowEye] = useState(false);
  const [formData, setFormData] = useState({
    company: "",
    clientCountry: "",
    industry: "",
    customerId: "",
    clientAddress: "",
    clientCity: "",
    clientState: "",
    clientZip: "",
    clientEmail: "",
    clientPhoneNumber: "",
    phoneNumberExtension: "",
    clientURL: "",
    leadSource: "",
    clientComplexity: "",
    currency: "",
    clientDescription: "",
    logo: "",
    report_email: '',
    kycCreditLimit: '',
    riskCreditLimit: '',
    skip_category_validation: 'true',
    pma: 'false',
    backend_api_key: '',
    batch_upload_email: '',
    blacklistCheck: 'YES',
    networkAnalysis: 'NO',
    baseCreate: 'WRM',
    wrm: 'NO',
    accountsQ: 'NO',
    kyc: 'NO',
    wrmLimitPerDay: "",
    wrmLimitPerWeek: "",
    emailSend:'NO',
    ogmFrequency:'RUN BASED ON BATCH DATE FREQUENCY',
    categoryMerge:'false',
    backendCache: 'true',
    kycPhoneVerification: 'NO',
    kycEmailVerification: 'NO',
    levelCategory:'false',
    proposedCategory: 'NO',
    ogm_grouping: 'NO',
    extraPolicyCheck: 'NO',
    fraud_tool_limit:"",
    feedBackChat: 'NO',
    wrmDuplicateUrlCheck: 'NO',
    edd_callback_report_status_url: "",
    edd_callback_report_url: "",
    edd_callback_status: 'NO',
    edd_callback_report: 'NO',
    edd_callback_report_api_key: '',
    wrm_callback_report_status_url: '',
    wrm_callback_report_url: '',
    wrm_callback_report_status: 'NO',
    wrm_callback_report: 'NO',
    withInBatchCheckDuplicate: 'NO',
    wrm_website_days_check: 'NO',
    wrm_callback_report_api_key: '',
    ogm_callback_report_status_url: '',
    ogm_callback_report_url: '',
    ogm_callback_report_status: 'NO',
    ogm_callback_report: 'NO',
    ogm_callback_report_api_key: '',
    ogm_auto_spilt_trigger: 'NO'
  });

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    getCountryDispatch(params);
    getStateDispatch(params);
    getCityDispatch(params);
    getIndustryDispatch(params);
  }, []);

  const handleChange = (e) => {
    e.persist();
    const { value, name } = e.target;
    !isFormUpdated && setFormUpdated(true);
    setFormData((values) => ({ ...values, [name]: value }));
    setErrors({ ...errors, [name]: "" });
  };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  };

  const handleChangeCurrency = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCurrencyOption(selectedOption);
      setFormData((values) => ({ ...values, currency: selectedOption.value }));
    } else {
      setSelectedCurrencyOption();
      setFormData((values) => ({ ...values, currency: "" }));
    }
    setErrors({ ...errors, currency: "" });
  };

  const handleChangeCountry = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          getStateDispatch({
            countryId: selectedOption.value,
            skipPagination: 'true'
          });
        }, 1500)
      );

      setSelectedCountryOption(selectedOption);
      setFormData((values) => ({
        ...values,
        clientCountry: selectedOption.value,
        clientState: "",
        clientCity: "",
      }));
      setSelectedStateOption();
      setSelectedCityOption();
    } else {
      setSelectedCountryOption();
      setSelectedStateOption();
      setSelectedCityOption();
      setFormData((values) => ({
        ...values,
        clientCountry: "",
        clientState: "",
        cityclientCity: "",
      }));
    }
    setErrors({ ...errors, clientCountry: "" });
  };

  const handleChangeState = (selectedOption) => {
    if (selectedOption !== null) {
      if (typingTimeout) {
        clearTimeout(typingTimeout);
      }
      setTypingTimeout(
        setTimeout(() => {
          getCityDispatch({
            stateId: selectedOption.value,
            skipPagination: 'true'
          });
        }, 2000)
      );
      setSelectedStateOption(selectedOption);
      setFormData((values) => ({
        ...values,
        clientState: selectedOption.value,
        clientCity: "",
      }));
      setSelectedCityOption();
    } else {
      setSelectedStateOption();
      setSelectedCityOption();
      setFormData((values) => ({ ...values, clientState: "", clientCity: "" }));
    }
    setErrors({ ...errors, clientState: "" });
  };

  const handleChangeCity = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption);
      setFormData((values) => ({
        ...values,
        clientCity: selectedOption.value,
      }));
    } else {
      setSelectedCityOption();
      setFormData((values) => ({ ...values, clientCity: "" }));
    }
    setErrors({ ...errors, clientCity: "" });
  };

  const handleChangeIndustry = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedIndustryOption(selectedOption);
      setFormData((values) => ({ ...values, industry: selectedOption.value }));
      setErrors({ ...errors, industry: "" });
    } else {
      setSelectedIndustryOption();
      setFormData((values) => ({ ...values, industry: "" }));
    }
  };

  useEffect(() => {
    const country = getDefaultOptions(getCountrys);
    setCountryOption(country);
  }, [getCountrys]);

  useEffect(() => {
    const state = getDefaultOptions(getStates);
    setStateOption(state);
  }, [getStates]);

  useEffect(() => {
    const city = getDefaultOptions(getCitys);
    setCityOptions(city);
  }, [getCitys]);

  useEffect(() => {
    const data = getDefaultIndustryOptions(getIndustry, "industryType");
    setIndustryOption(data);
  }, [getIndustry]);

  const getDefaultOptions = (rawData) => {
    const defaultOptions = [];
    for (const item in rawData) {
      defaultOptions.push({
        label: rawData[item].name,
        value: rawData[item]._id,
      });
    }
    return defaultOptions;
  };

  const getDefaultIndustryOptions = (data, name) => {
    const defaultOptions = [];
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ""}`,
          value: item._id,
        })
      );
      return defaultOptions;
    }
  };

  const getSelectedIndustryLabel = () => {
    const getIndustryLabel = getDefaultIndustryOptions(
      getIndustry,
      "industryType"
    );
    const selIndOption = _.filter(getIndustryLabel, function (x) {
      if (_.includes(formData.industry, x.value)) {
        return x;
      }
    });
    if (selIndOption.length > 0) {
      return selIndOption[0];
    }
  };

  const handleSubmit = () => {
    const errorMsg = companyValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        const gePayload = getCompanyDetailsUpdatePayload(formData);
        editClientsDispatch(id, gePayload);
      } else {
        onClickNext(1);
        const getCountryValue = getAdressValues(
          getCountrys,
          formData.clientCountry
        );
        const getStateValue = getAdressValues(getStates, formData.clientState);
        const gerCityValue = getAdressValues(getCitys, formData.clientCity);
        const getIndValue = getSelectedIndustryLabel();
        setSummary((values) => ({
          ...values,
          client: {
            country: getCountryValue && getCountryValue.label,
            state: getStateValue && getStateValue.label,
            city: gerCityValue && gerCityValue.label,
            industry: getIndValue && getIndValue.label,
          },
        }));
        setClientDetails((values) => ({ ...values, client: formData }));
        setLocalStorage(CRM_FORM.COMPANY_DETAILS, JSON.stringify(formData));
      }
    }
  };

  const onConfirm = () => {
    clientDetailsDispatch(id);
  };

  useEffect(() => {
    if (statusECL === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageECL,
        "success",
        "ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      cleareditClientsDispatch();
    } else if (statusECL === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert("Error", messageECL, "", "Ok");
    }
    cleareditClientsDispatch();
  }, [statusECL]);

  useEffect(() => {
    const localData = JSON.parse(getLocalStorage(CRM_FORM.COMPANY_DETAILS));
    if (!_.isEmpty(localData)) {
      if (!editMode) {
        setFormData(localData);
        if (localData.industry) {
          const data = getDefaultIndustryOptions(getIndustry, "industryType");
          const selOption = _.filter(data, function (x) {
            if (_.includes(localData.industry, x.value)) {
              return x;
            }
          });
          if (selOption.length > 0) {
            setSelectedIndustryOption(selOption);
          }
        }
        if (localData.currency) {
          const selOption = _.filter(currencyOptions, function (x) {
            if (_.includes(localData.currency, x.value)) {
              return x;
            }
          });
          setSelectedCurrencyOption(selOption);
        }
        setAddressDetails(
          getCountrys,
          getStates,
          getCitys,
          localData.clientCountry,
          localData.clientState,
          localData.clientCity,
          setSelectedCountryOption,
          setSelectedStateOption,
          setSelectedCityOption
        );
      }
    }
  }, []);

  useEffect(() => {
    return () => {
      if (isFormUpdated) {
        setFormUpdated(false);
      }
    };
  }, [isFormUpdated]);

  useEffect(() => {
    if (statusCD === STATUS_RESPONSE.SUCCESS_MSG) {
      if (dataCD && dataCD._id) {
        const data = setCompanyDetails(dataCD);
        setFormData(data);
        if (dataCD && dataCD.apiKeyisGenerated) {
          setGenKeyExist(dataCD && dataCD.apiKeyisGenerated);
        }
        if (dataCD.clientCountry) {
          const country = getDefaultOptions(getCountrys);
          const selOption = _.filter(country, function (x) {
            if (_.includes(dataCD.clientCountry, x.value)) {
              return x;
            }
          });
          if (selOption.length > 0) {
            setSelectedCountryOption(selOption);
          }
        }

        if (dataCD.clientState) {
          const state = getDefaultOptions(getStates);
          const selOption = _.filter(state, function (x) {
            if (_.includes(dataCD.clientState, x.value)) {
              return x;
            }
          });
          if (selOption.length > 0) {
            setSelectedStateOption(selOption);
          }
        }
        if (dataCD.clientCity) {
          const city = getDefaultOptions(getCitys);
          const selOption = _.filter(city, function (x) {
            if (_.includes(dataCD.clientCity, x.value)) {
              return x;
            }
          });
          if (selOption.length > 0) {
            setSelectedCityOption(selOption);
          }
        }
        if (dataCD.industry) {
          const data = getDefaultIndustryOptions(getIndustry, "industryType");
          const selOption = _.filter(data, function (x) {
            if (_.includes(dataCD.industry, x.value)) {
              return x;
            }
          });
          setSelectedIndustryOption(selOption);
        }
        if (dataCD.currency) {
          const selOption = _.filter(currencyOptions, function (x) {
            if (_.includes(dataCD.currency, x.value)) {
              return x;
            }
          });
          setSelectedCurrencyOption(selOption);
        }
        clearClientDetailsDispatch();
      } else if (statusCD === STATUS_RESPONSE.ERROR_MSG) {
        clearClientDetailsDispatch();
      }
    }
  }, [statusCD]);

  useEffect(() => {
    if (id) {
      setEditMode(true);
      clientDetailsDispatch(id);
      setShowForm(false);
    } else {
      setEditMode(false);
    }
  }, [id]);

  const onGenerateKey = () => {
    const currentClientId = id.replace("/", "");
    const payload = {
      clientId: currentClientId,
    };
    addApiKeyDispatch(payload);
  };

  useEffect(() => {
    if (statusAAK === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        messageAAK,
        "success",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      clearAddApiKeyDispatch();
    } else if (statusAAK === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert("Error", messageAAK, "", "Ok");
    }
    clearAddApiKeyDispatch();
  }, [statusAAK]);

  const onShowApiKey = () => {
    if (!showEye) {
      const currentClientId = id.replace("/", "");
      getApiKeyDispatch(currentClientId);
    }
  };

  const handleFileUpload = (files, name) => {
    const file = files && files[0] ? files[0] : []
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const fileType = file && file.type;
    const uploadedFileSize = file && file.size;
    isValidFileFormat = _.includes(FILE_FORMAT_IMAGES, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        const data = new FormData();
        data.append("type", "logo");
        data.append("file_to_upload", file);
        addMerchantUploadDispatch(data);
        setErrors((values) => ({ ...values, logo: "" }));
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.IMAGE_INVALID });
    }
  }

  useEffect(() => {
    if (statusAMUpload === STATUS_RESPONSE.SUCCESS_MSG) {
      if (dataAMUpload && dataAMUpload.path) {
        setFormData((values) => ({ ...values, logo: dataAMUpload.path }));
        clearaddMerchantUploadDispatch()
      }
    }
  }, [statusAMUpload])

  return (
    <>
      <Modal showModal={show} modalWidth={550}>
        <div>
          <div className="d-flex justify-content-between align-items-center">
            <h2 className="me-8 px-5">Show API Key</h2>
            <button
              type="button"
              className="btn btn-lg btn-icon btn-active-light-primary close"
              onClick={() => {
                setShow(false);
              }}
            >
              {/* {/ eslint-disable /} */}
              <KTSVG
                path="/media/icons/duotune/arrows/arr061.svg"
                className="svg-icon-1"
              />
              {/* {/ eslint-disable /} */}
            </button>
          </div>
        </div>
        <div>
          <div className="d-flex justify-content-center align-items-center">
            {showEye ? (
              loadingApi ? (
                <div className="d-flex justify-content-center py-5">
                  <div
                    className="spinner-border text-primary m-3"
                    role="status"
                  />
                </div>
              ) : (
                <>
                  <div className="text-gray-700 fw-bolder fs-6 pl-3">
                    {getApiKey.apiKey ? getApiKey.apiKey : "--"}
                  </div>
                </>
              )
            ) : (
              <>
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
                <i className="bi bi-asterisk text-dark fs-5 m-1" />
              </>
            )}
            <button
              className="btn btn-sm btn-white m-2 fa-pull-right"
              onClick={() => {
                setShowEye((val) => !val);
                onShowApiKey();
              }}
            >
              <i
                className={`bi bi-eye${showEye ? "" : "-slash"
                  } fs-5 fw-bolder text-primary`}
              />
            </button>
          </div>
        </div>
      </Modal>
      <div className="card-header bg-skyBlue py-10">
        <div className="card-body ms-4">
          {loadingClientDetails ? (
            <div className="d-flex justify-content-center py-5">
              <div className="spinner-border text-primary m-5" role="status" />
            </div>
          ) : (
            <>
              <div className="d-flex">
                <div className="col-lg-6">
                  <h2 className="mb-5">Company Details</h2>
                </div>
                {editMode && !showForm ? (
                  <div className="col-lg-6">
                    <div className="d-flex justify-content-end me-4">
                      {genKeyExist ? (
                        <>
                          <button
                            className="btn btn-sm btn-primary rounded-0 fw-bolder"
                            data-toggle="modal"
                            data-target="#searchModal"
                            onClick={() => {
                              setShow(true);
                              setShowEye(false);
                            }}
                          >
                            Show API Key
                          </button>
                        </>
                      ) : (
                        <button
                          className="btn btn-sm btn-dark rounded-0 fw-bolder"
                          data-toggle="tooltip"
                          data-placement="right"
                          title="Generate API Key"
                          disabled={loadingAAK}
                          onClick={() => {
                            onGenerateKey();
                          }}
                        >
                          {loadingAAK ? (
                            <span
                              className="spinner-border spinner-border-sm mx-3"
                              role="status"
                              aria-hidden="true"
                            />
                          ) : (
                            "Generate API Key"
                          )}
                          {/* Generate API Key &nbsp; */}
                        </button>
                      )}
                    </div>
                  </div>
                ) : null}
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Company Name:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="company"
                      type="text"
                      className="form-control"
                      placeholder="Company Name"
                      onChange={(e) => handleChange(e)}
                      value={formData.company || ""}
                      maxLength={42}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.company && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.company}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Customer ID (Any CRM):
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="customerId"
                      type="text"
                      className="form-control"
                      placeholder="Customer ID"
                      onChange={(e) => handleChange(e)}
                      value={formData.customerId || ""}
                      maxLength={42}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Industry:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      isClearable
                      styles={customStyles}
                      isMulti={false}
                      name="industry"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeIndustry}
                      options={industryOption}
                      value={selectedIndustryOption}
                    />
                  </div>
                  {errors && errors.industry && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.industry}
                    </div>
                  )}
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Country:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      isClearable
                      styles={customStyles}
                      isMulti={false}
                      name="clientCountry"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeCountry}
                      options={countryOption}
                      value={selectedCountryOption}
                      isLoading={loadingCountry}
                      isDisabled={loadingCity || loadingCountry}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    State:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      isClearable
                      styles={customStyles}
                      isMulti={false}
                      name="clientState"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeState}
                      options={stateOption}
                      value={selectedStateOption}
                      isLoading={loadingState}
                      isDisabled={loadingCity || loadingState}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    City:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      isClearable
                      styles={customStyles}
                      isMulti={false}
                      name="clientCity"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeCity}
                      options={cityOptions}
                      value={selectedCityOption}
                      isLoading={loadingCity}
                      isDisabled={loadingCity}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Address:
                  </label>
                  <div className="col-lg-11">
                    <textarea
                      autoComplete="off"
                      name="clientAddress"
                      type="text"
                      className="form-control"
                      placeholder="Address"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientAddress || ""}
                      maxLength={500}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.clientAddress && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.clientAddress}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Zip Code:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="clientZip"
                      type="text"
                      className="form-control"
                      placeholder="Zip"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientZip || ""}
                      maxLength={6}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.clientZip && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.clientZip}
                    </div>
                  )}
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Email:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="clientEmail"
                      type="text"
                      className="form-control"
                      placeholder="Email"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientEmail || ""}
                      maxLength={42}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.clientEmail && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.clientEmail}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Phone Number:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="clientPhoneNumber"
                      type="text"
                      className="form-control"
                      placeholder="Phone Number"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientPhoneNumber || ""}
                      maxLength={12}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.clientPhoneNumber && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.clientPhoneNumber}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Phone Extension:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="phoneNumberExtension"
                      type="text"
                      className="form-control"
                      placeholder="Phone Extension"
                      onChange={(e) => handleChange(e)}
                      value={formData.phoneNumberExtension || ""}
                      maxLength={10}
                      onBlur={(e) => handleTrimSpaceOnly(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!/[0-9+]/.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    URL:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="clientURL"
                      type="text"
                      className="form-control"
                      placeholder="URL"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientURL || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Lead Source:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="leadSource"
                      type="text"
                      className="form-control"
                      placeholder="Lead Source"
                      onChange={(e) => handleChange(e)}
                      value={formData.leadSource || ""}
                      maxLength={42}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Client Complexity:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="clientComplexity"
                      type="text"
                      className="form-control"
                      placeholder="Client Complexity"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientComplexity || ""}
                      maxLength={42}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="form-group row mb-6">
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Currency:
                  </label>
                  <div className="col-lg-11">
                    <ReactSelect
                      isClearable
                      styles={customStyles}
                      isMulti={false}
                      name="currency"
                      className="basic-single"
                      classNamePrefix="select"
                      handleChangeReactSelect={handleChangeCurrency}
                      options={currencyOptions}
                      value={selectedCurrencyOption}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Company Description:
                  </label>
                  <div className="col-lg-11">
                    <textarea
                      autoComplete="off"
                      name="clientDescription"
                      type="text"
                      className="form-control"
                      placeholder="Company Description"
                      onChange={(e) => handleChange(e)}
                      value={formData.clientDescription || ""}
                      maxLength={500}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Company Logo:
                  </label>
                  <div className="col-lg-10">
                    <StyledDiv {...getRootProps({ refKey: "innerRef" })}>
                      <Dropzone
                        onDrop={(acceptedFiles) => {
                          handleFileUpload(acceptedFiles, "file_to_upload", 'logo');
                        }}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <div className="container w-30rem">
                            <div {...getRootProps()}>
                              <input
                                {...getInputProps()}
                                name="logo"
                                id="file_to_upload"
                                accept=".png, .jpg, .jpeg"
                                multiple={false}
                              />
                              <p className="text-primary fw-bolder fs-6">
                                Click here to select files
                              </p>
                            </div>
                          </div>
                        )}
                      </Dropzone>
                    </StyledDiv>
                    {loadingAMUpload ? (
                      <div className="d-flex justify-content-center align-items-center py-3">
                        <div className="d-flex justify-content-center">
                          <span
                            className="spinner-grow spinner-grow-sm"
                            role="status"
                          >
                            <span className="visually-hidden" />
                          </span>
                          <span
                            className="spinner-grow spinner-grow-sm mx-1"
                            role="status"
                          >
                            <span className="visually-hidden" />
                          </span>
                        </div>
                        <div className="mx-2">Uploading</div>
                      </div>
                    ) : null}
                    {formData.logo ? (
                      <div className='ms-20'>
                        <img
                          className='mx-20 mt-2'
                          src={`${API_URL}/uploads/${formData.logo}`}
                          style={{ width: 120, height: 120 }}
                        />
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="form-group row mb-6">

                <div className="col-lg-12">
                  <h4 className="mb-2">
                    Email Details
                  </h4>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                    Report Email:
                  </label>
                  <div className="col-lg-11">
                    <textarea
                      autoComplete="off"
                      name="report_email"
                      type="text"
                      className="form-control"
                      placeholder="Report Email"
                      onChange={(e) => handleChange(e)}
                      value={formData.report_email || ""}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.report_email && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.report_email}
                    </div>
                  )}
                </div>
                {
                  editMode ? (
                    <>
                    <div className="col-lg-4 mb-3 mt-2">
                      <label className="font-size-xs  font-weight-bold mb-2 required form-label">
                        Batch Upload Initimate Email :
                      </label>
                      <div className="col-lg-11">
                        <input
                          autoComplete="off"
                          name="batch_upload_email"
                          type="text"
                          className="form-control"
                          placeholder="Batch Upload Initimate Email"
                          onChange={(e) => handleChange(e)}
                          value={formData.batch_upload_email || ""}
                          onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                          onKeyPress={(e) => {
                            if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                              e.preventDefault();
                            }
                          }}
                        />
                      </div>
                      {errors && errors.batch_upload_email && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red;}"}</style>
                          {errors.batch_upload_email}
                        </div>
                      )}
                    </div>
                    <div className="col-lg-4">
                      <label className="font-size-xs  font-weight-bold mb-2 form-label">
                        Email Notification:
                      </label>
                      <div className="col-lg-11">
                        <div className="form-check form-check-custom form-check-solid mt-4">
                          <label className='d-flex flex-stack mb-5 cursor-pointer'>
                            <span className='form-check form-check-custom form-check-solid me-2'>
                              <input
                                className='form-check-input'
                                type='radio'
                                onChange={(e) => handleChange(e)}
                                value='YES'
                                name='emailSend'
                                checked={formData.emailSend === 'YES'}
                              />
                            </span>
                            <span className='d-flex flex-column'>
                              <span className='fs-7 text-muted'>
                                Yes
                              </span>
                            </span>
                          </label>
                          <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                            <span className='form-check form-check-custom form-check-solid me-2'>
                              <input
                                className='form-check-input'
                                type='radio'
                                onChange={(e) => handleChange(e)}
                                value='NO'
                                name='emailSend'
                                checked={formData.emailSend === 'NO'}
                              />
                            </span>
                            <span className='d-flex flex-column'>
                              <span className='fs-7 text-muted'>
                                No
                              </span>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                    </>
                  ) : null
                }
                <div className="col-lg-12">
                  <h4 className="mb-2">
                    Credit Details
                  </h4>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Kyc Credit Limit:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="kycCreditLimit"
                      type="text"
                      className="form-control"
                      placeholder="Kyc Creadit Limit"
                      onChange={(e) => handleChange(e)}
                      value={formData.kycCreditLimit || ""}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.kycCreditLimit && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.kycCreditLimit}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Risk Credit Limit:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="riskCreditLimit"
                      type="text"
                      className="form-control"
                      placeholder="Risk Creadit Limit"
                      onChange={(e) => handleChange(e)}
                      value={formData.riskCreditLimit || ""}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.riskCreditLimit && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.riskCreditLimit}
                    </div>
                  )}
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    WRM Limit Per Day:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="wrmLimitPerDay"
                      type="text"
                      className="form-control"
                      placeholder="WRM Limit Per Day"
                      onChange={(e) => handleChange(e)}
                      value={formData.wrmLimitPerDay || ""}
                      maxLength={4}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    WRM Limit Per Week:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="wrmLimitPerWeek"
                      type="text"
                      className="form-control"
                      placeholder="WRM Limit Per Week"
                      onChange={(e) => handleChange(e)}
                      value={formData.wrmLimitPerWeek || ""}
                      maxLength={4}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="col-lg-12">
                  <h4 className="mb-2">
                    Backend API Key Details
                  </h4>
                </div>
                <div className="col-lg-4 mb-3">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Backend Api Key:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="backend_api_key"
                      type="text"
                      className="form-control"
                      placeholder="Backend Api Key"
                      onChange={(e) => handleChange(e)}
                      value={formData.backend_api_key || ""}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  {errors && errors.backend_api_key && (
                    <div className="rr mt-1">
                      <style>{".rr{color:red;}"}</style>
                      {errors.backend_api_key}
                    </div>
                  )}
                </div>
                <div className="col-lg-12">
                  <h4 className="mb-2">
                    Category Details
                  </h4>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Skip Category Validation:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='true'
                            name='skip_category_validation'
                            checked={formData.skip_category_validation === 'true'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='false'
                            name='skip_category_validation'
                            checked={formData.skip_category_validation === 'false'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Level Category:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='true'
                            name='levelCategory'
                            checked={formData.levelCategory === 'true'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='false'
                            name='levelCategory'
                            checked={formData.levelCategory === 'false'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Category Merge:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='true'
                            name='categoryMerge'
                            checked={formData.categoryMerge === 'true'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='false'
                            name='categoryMerge'
                            checked={formData.categoryMerge === 'false'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      Proposed Category:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='proposedCategory'
                              checked={formData.proposedCategory === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='proposedCategory'
                              checked={formData.proposedCategory === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                </div>
                <div className="col-lg-12">
                  <h4 className="mb-2">
                    WRM Details
                  </h4>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Blacklist Check:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='blacklistCheck'
                            checked={formData.blacklistCheck === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='blacklistCheck'
                            checked={formData.blacklistCheck === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    PMA:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='true'
                            name='pma'
                            checked={formData.pma === 'true'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='false'
                            name='pma'
                            checked={formData.pma === 'false'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      Extra Policy Check:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='extraPolicyCheck'
                              checked={formData.extraPolicyCheck === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='extraPolicyCheck'
                              checked={formData.extraPolicyCheck === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Network Analysis:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='networkAnalysis'
                            checked={formData.networkAnalysis === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='networkAnalysis'
                            checked={formData.networkAnalysis === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      WRM Duplicate Url Check:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='wrmDuplicateUrlCheck'
                              checked={formData.wrmDuplicateUrlCheck === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='wrmDuplicateUrlCheck'
                              checked={formData.wrmDuplicateUrlCheck === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      WRM Duplicate Url Check Within Batch:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='withInBatchCheckDuplicate'
                              checked={formData.withInBatchCheckDuplicate === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='withInBatchCheckDuplicate'
                              checked={formData.withInBatchCheckDuplicate === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      WRM Website Days Check:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='wrm_website_days_check'
                              checked={formData.wrm_website_days_check === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='wrm_website_days_check'
                              checked={formData.wrm_website_days_check === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    Backend Cache:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='true'
                            name='backendCache'
                            checked={formData.backendCache === 'true'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='false'
                            name='backendCache'
                            checked={formData.backendCache === 'false'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      FeedBack Chat:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='feedBackChat'
                              checked={formData.feedBackChat === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='feedBackChat'
                              checked={formData.feedBackChat === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                </div>
                <div className="col-lg-12">
                  <h4 className="mb-2">
                    OGM Details
                  </h4>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    OGM Base Report:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='WRM'
                            name='baseCreate'
                            checked={formData.baseCreate === 'WRM'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            WRM
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='OGM'
                            name='baseCreate'
                            checked={formData.baseCreate === 'OGM'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            OGM
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                {editMode ?
                  <>
                    <div className="col-lg-4">
                      <label className="font-size-xs  font-weight-bold mb-2 form-label">
                        OGM Frequency Run Setting:
                      </label>
                      <div className="col-lg-11">
                        <div className="form-check form-check-custom form-check-solid mt-4">
                          <label className='d-flex flex-stack mb-5 cursor-pointer'>
                            <span className='form-check form-check-custom form-check-solid me-2'>
                              <input
                                className='form-check-input'
                                type='radio'
                                onChange={(e) => handleChange(e)}
                                value='RUN BASED ON BATCH DATE FREQUENCY'
                                name='ogmFrequency'
                                checked={formData.ogmFrequency === 'RUN BASED ON BATCH DATE FREQUENCY'}
                              />
                            </span>
                            <span className='d-flex flex-column'>
                              <span className='fs-7 text-muted'>
                                RUN BASED ON BATCH DATE FREQUENCY
                              </span>
                            </span>
                          </label>
                          <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                            <span className='form-check form-check-custom form-check-solid me-2'>
                              <input
                                className='form-check-input'
                                type='radio'
                                onChange={(e) => handleChange(e)}
                                value='SAME DATE FREQUENCY'
                                name='ogmFrequency'
                                checked={formData.ogmFrequency === 'SAME DATE FREQUENCY'}
                              />
                            </span>
                            <span className='d-flex flex-column'>
                              <span className='fs-7 text-muted'>
                                SAME DATE FREQUENCY
                              </span>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </> : null}
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      OGM Grouping:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='ogm_grouping'
                              checked={formData.ogm_grouping === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='ogm_grouping'
                              checked={formData.ogm_grouping === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <h4 className="mb-2">
                      KYC Details
                    </h4>
                  </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    KYC Phone Verification:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='kycPhoneVerification'
                            checked={formData.kycPhoneVerification === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='kycPhoneVerification'
                            checked={formData.kycPhoneVerification === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    KYC Email Verification:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='kycEmailVerification'
                            checked={formData.kycEmailVerification === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='kycEmailVerification'
                            checked={formData.kycEmailVerification === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12">
                    <h4 className="mb-2">
                      Fraud Analysis Tool
                    </h4>
                </div>
                <div className="col-lg-4 mb-2">
                    <label className="font-size-xs font-weight-bold mb-2 form-label">
                      Fraud tool limit:
                    </label>
                    <div className="col-lg-11">
                      <input
                        autoComplete="off"
                        name="fraud_tool_limit"
                        type="text"
                        className="form-control"
                        placeholder="Fraud tool limit"
                        onChange={(e) => handleChange(e)}
                        value={formData.fraud_tool_limit || ""}
                        maxLength={4}
                        onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12">
                    <h4 className="mb-2">
                      WRM Callback Details
                    </h4>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                   WRM Callback Report API Key:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="wrm_callback_report_api_key"
                      type="text"
                      className="form-control"
                      placeholder="WRM Callback Report API Key"
                      onChange={(e) => handleChange(e)}
                      value={formData.wrm_callback_report_api_key || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      WRM Callback Report:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='wrm_callback_report'
                              checked={formData.wrm_callback_report === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='wrm_callback_report'
                              checked={formData.wrm_callback_report === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                   WRM Callback Report Url:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="wrm_callback_report_url"
                      type="text"
                      className="form-control"
                      placeholder="WRM Callback Report Url"
                      onChange={(e) => handleChange(e)}
                      value={formData.wrm_callback_report_url || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      WRM Callback Report Status:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='wrm_callback_report_status'
                              checked={formData.wrm_callback_report_status === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='wrm_callback_report_status'
                              checked={formData.wrm_callback_report_status === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                   WRM Callback Report Status Url:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="wrm_callback_report_status_url"
                      type="text"
                      className="form-control"
                      placeholder="WRM Callback Report Status Url"
                      onChange={(e) => handleChange(e)}
                      value={formData.wrm_callback_report_status_url || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-12">
                    <h4 className="mb-2">
                      OGM Callback Details
                    </h4>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    OGM Callback Report API Key:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="ogm_callback_report_api_key"
                      type="text"
                      className="form-control"
                      placeholder="OGM Callback Report API Key"
                      onChange={(e) => handleChange(e)}
                      value={formData.ogm_callback_report_api_key || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      OGM Auto Spilt Trigger:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='ogm_auto_spilt_trigger'
                              checked={formData.ogm_auto_spilt_trigger === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='ogm_auto_spilt_trigger'
                              checked={formData.ogm_auto_spilt_trigger === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      OGM Callback Report:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='ogm_callback_report'
                              checked={formData.ogm_callback_report === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='ogm_callback_report'
                              checked={formData.ogm_callback_report === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    OGM Callback Report Url:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="ogm_callback_report_url"
                      type="text"
                      className="form-control"
                      placeholder="OGM Callback Report Url"
                      onChange={(e) => handleChange(e)}
                      value={formData.ogm_callback_report_url || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      OGM Callback Report Status:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='ogm_callback_report_status'
                              checked={formData.ogm_callback_report_status === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='ogm_callback_report_status'
                              checked={formData.ogm_callback_report_status === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    OGM Callback Report Status Url:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="ogm_callback_report_status_url"
                      type="text"
                      className="form-control"
                      placeholder="OGM Callback Report Status Url"
                      onChange={(e) => handleChange(e)}
                      value={formData.ogm_callback_report_status_url || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-12">
                    <h4 className="mb-2">
                      EDD Callback Details
                    </h4>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                   EDD Callback Report API Key:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="edd_callback_report_api_key"
                      type="text"
                      className="form-control"
                      placeholder="EDD Callback Report API Key"
                      onChange={(e) => handleChange(e)}
                      value={formData.edd_callback_report_api_key || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      EDD Callback Report:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='edd_callback_report'
                              checked={formData.edd_callback_report === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='edd_callback_report'
                              checked={formData.edd_callback_report === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                   EDD Callback Report Url:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="edd_callback_report_url"
                      type="text"
                      className="form-control"
                      placeholder="EDD Callback Report Url"
                      onChange={(e) => handleChange(e)}
                      value={formData.edd_callback_report_url || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  <div className="col-lg-4">
                    <label className="font-size-xs  font-weight-bold mb-2 form-label">
                      EDD Callback Report Status:
                    </label>
                    <div className="col-lg-11">
                      <div className="form-check form-check-custom form-check-solid mt-4">
                        <label className='d-flex flex-stack mb-5 cursor-pointer'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='YES'
                              name='edd_callback_status'
                              checked={formData.edd_callback_status === 'YES'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              Yes
                            </span>
                          </span>
                        </label>
                        <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                          <span className='form-check form-check-custom form-check-solid me-2'>
                            <input
                              className='form-check-input'
                              type='radio'
                              onChange={(e) => handleChange(e)}
                              value='NO'
                              name='edd_callback_status'
                              checked={formData.edd_callback_status === 'NO'}
                            />
                          </span>
                          <span className='d-flex flex-column'>
                            <span className='fs-7 text-muted'>
                              No
                            </span>
                          </span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 mb-2">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    EDD Callback Report Status Url:
                  </label>
                  <div className="col-lg-11">
                    <input
                      autoComplete="off"
                      name="edd_callback_report_status_url"
                      type="text"
                      className="form-control"
                      placeholder="EDD Callback Report Status Url"
                      onChange={(e) => handleChange(e)}
                      value={formData.edd_callback_report_status_url || ""}
                      maxLength={500}
                      onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                      onKeyPress={(e) => {
                        if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                          e.preventDefault();
                        }
                      }}
                    />
                  </div>
                  </div>
                  {/* <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    WRM:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='wrm'
                            checked={formData.wrm === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='wrm'
                            checked={formData.wrm === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                  </div>
                  <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                  AccountsQ:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='accountsQ'
                            checked={formData.accountsQ === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='accountsQ'
                            checked={formData.accountsQ === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                  </div>
                  <div className="col-lg-4">
                  <label className="font-size-xs  font-weight-bold mb-2 form-label">
                    KYC:
                  </label>
                  <div className="col-lg-11">
                    <div className="form-check form-check-custom form-check-solid mt-4">
                      <label className='d-flex flex-stack mb-5 cursor-pointer'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='YES'
                            name='kyc'
                            checked={formData.kyc === 'YES'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            Yes
                          </span>
                        </span>
                      </label>
                      <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                        <span className='form-check form-check-custom form-check-solid me-2'>
                          <input
                            className='form-check-input'
                            type='radio'
                            onChange={(e) => handleChange(e)}
                            value='NO'
                            name='kyc'
                            checked={formData.kyc === 'NO'}
                          />
                        </span>
                        <span className='d-flex flex-column'>
                          <span className='fs-7 text-muted'>
                            No
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                  </div> */}
              </div>
            </>
          )}
        </div>
        {showForm ? (
          <div className="form-group row mb-4">
            <div className="col-lg-6" />
            <div className="col-lg-6">
              <div className="col-lg-11">
                <button
                  className="btn btn-orange m-2 fa-pull-right"
                  id="submitBtn"
                  onClick={(event) => {
                    handleSubmit(event);
                  }}
                >
                  Next
                </button>
              </div>
            </div>
          </div>
        ) : null}
        {editMode && !showForm ? (
          <div className="form-group row mb-4">
            <div className="col-lg-6" />
            <div className="col-lg-6">
              <div className="col-lg-11">
                <button
                  className="btn btn-blue m-2 fa-pull-right"
                  onClick={(event) => {
                    handleSubmit(event);
                  }}
                >
                  {loadingECL ? (
                    <span
                      className="spinner-border spinner-border-sm mx-3"
                      role="status"
                      aria-hidden="true"
                    />
                  ) : (
                    "Save"
                  )}
                </button>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  const { apiKeyStore } = state;
  return {
    getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists,
    loadingCountry: state && state.CountrylistStore && state.CountrylistStore.loading,
    getStates: state && state.StatelistStore && state.StatelistStore.Statelists,
    loadingState: state && state.StatelistStore && state.StatelistStore.loading,
    getCitys: state && state.CitylistStore && state.CitylistStore.Citylists,
    loadingCity: state && state.CitylistStore && state.CitylistStore.loading,
    getIndustry:state && state.industryStore && state.industryStore.getIndustry,
    dataCD: state && state.clientDetailsStore && state.clientDetailsStore.dataCD,
    statusCD: state && state.clientDetailsStore && state.clientDetailsStore.statusCD,
    messageCD: state && state.clientDetailsStore && state.clientDetailsStore.messageCD,
    loadingClientDetails: state && state.clientDetailsStore && state.clientDetailsStore.loadingClientDetails,
    dataECL: state && state.editClientStore && state.editClientStore.dataECL,
    statusECL: state && state.editClientStore && state.editClientStore.statusECL,
    messageECL: state && state.editClientStore && state.editClientStore.messageECL,
    loadingECL: state && state.editClientStore && state.editClientStore.loadingECL,
    loadingAAK: state && state.addApiKeyStore && state.addApiKeyStore.loadingAAK,
    statusAAK: state && state.addApiKeyStore && state.addApiKeyStore.statusAAK,
    messageAAK: state && state.addApiKeyStore && state.addApiKeyStore.messageAAK,
    dataAAK: state && state.addApiKeyStore && state.addApiKeyStore.dataAAK,
    dataCD: state && state.clientDetailsStore && state.clientDetailsStore.dataCD,
    getApiKey: apiKeyStore && apiKeyStore.getApiKey ? apiKeyStore.getApiKey : [],
    loadingApi: apiKeyStore && apiKeyStore.loading ? apiKeyStore.loading : false,
    loadingAMUpload: state && state.addMerchantUploadStore && state.addMerchantUploadStore.loadingAMUpload,
    dataAMUpload: state && state.addMerchantUploadStore && state.addMerchantUploadStore.dataAMUpload,
    statusAMUpload: state && state.addMerchantUploadStore && state.addMerchantUploadStore.statusAMUpload,
    messageAMUpload: state && state.addMerchantUploadStore && state.addMerchantUploadStore.messageAMUpload,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getCountryDispatch: () => dispatch(CountryActions.getCountrys()),
  CountryActions: (data) => dispatch(CountryActions.getCountrys(data)),
  getCountryDispatch: (params) => dispatch(CountryActions.getCountrylist(params)),
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  CityActions: (data) => dispatch(CityActions.getCitys(data)),
  getIndustryDispatch: (params) => dispatch(industryActions.getIndustry(params)),
  industryActions: (data) => dispatch(industryActions.getIndustry(data)),
  clientDetailsDispatch: (data) => dispatch(clientDetailsActions.getClientDetails(data)),
  clearClientDetailsDispatch: () => dispatch(clientDetailsActions.clearClientDetails()),
  editClientsDispatch: (id, data) => dispatch(editClientsActions.editClients(id, data)),
  cleareditClientsDispatch: () => dispatch(editClientsActions.cleareditClients()),
  addApiKeyDispatch: (data) => dispatch(addApiKeyActions.addApiKey(data)),
  clearAddApiKeyDispatch: () => dispatch(addApiKeyActions.clearaddApiKey()),
  getApiKeyDispatch: (id) => dispatch(apiKeyActions.getApiKey(id)),
  addMerchantUploadDispatch: (data) => dispatch(addMerchantUploadActions.addMerchantUpload(data)),
  clearaddMerchantUploadDispatch: () => dispatch(addMerchantUploadActions.clearaddMerchantUpload()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CompanyDetails);