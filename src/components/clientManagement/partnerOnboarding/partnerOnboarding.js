import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import ShowFields from './ShowFields'
import { Stepper } from '../../../theme/layout/components/stepper/Stepper'
import CompanyDetails from './userDetails/companyDetails'
import ClientForm from './userDetails/ClientForm'
import Checkout from './userDetails/checkoutPage'
import UserPartnerInfo from './userDetails/userPartnerDetails'
import UpdatePartnersUser from './userDetails/UpdatePartnersUser'
import { CRM_FORM } from '../../../utils/constants'
import { useLocation } from 'react-router-dom'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'

function PartnerOnboarding (props) {
  const [completedSteps, setCompletedSteps] = useState([-1])
  const [activeStep, setActiveStep] = useState(0)
  const [array, setArray] = useState([])
  const [partnerDetails, setPartnerDetails] = useState({})
  const [editMode, setEditMode] = useState(false)
  const active = getLocalStorage(CRM_FORM.ACTIVE_STEP)
  const [tab, setTab] = useState('PARTNER')
  const stepperArray = [
    {
      stepperLabel: 'Partner Details',
      Optional: ''
    },
    {
      stepperLabel: 'Choose Client',
      Optional: ''
    },
    {
      stepperLabel: 'User Info',
      Optional: ''
    },
    {
      stepperLabel: 'Summary',
      Optional: ''
    }
  ]

  const stepperEditArray = [
    {
      stepperLabel: 'Partner Details',
      Optional: ''
    },
    {
      stepperLabel: 'Choose Client',
      Optional: ''
    },
    {
      stepperLabel: 'User Info',
      Optional: ''
    },
    {
      stepperLabel: 'Summary',
      Optional: ''
    }
  ]

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('update')
  const id = url && url[1]

  const onClickNext = (currentId) => {
    const id = currentId - 1
    setActiveStep(currentId)
    if (completedSteps.length === currentId) {
      if (completedSteps && !completedSteps.includes(id)) {
        setCompletedSteps((values) => ([...values, id]))
        setLocalStorage(CRM_FORM.ACTIVE_STEP, currentId)
      }
    }
  }

  const goBack = (id) => {
    setActiveStep(id)
  }

  const getDescendants = (arr, step) => {
    const val = []
    arr.forEach((item, i) => {
      if (step > i) {
        val.push(i)
      }
    })
    return val
  }
  const d = getDescendants(array, active)
  const editstepper = getDescendants(array, array.length)

  const showTabDetails = (type) => {
    switch (type) {
      case 'PARTNER':
        return <CompanyDetails />
      case 'CLIENT':
        return <ClientForm />
      case 'USER':
        return <UpdatePartnersUser />
    }
  }

  useEffect(() => {
    if (active) {
      setActiveStep(Number(active))
      setCompletedSteps([-1, ...d])
    } else {
      setActiveStep(0)
      setCompletedSteps([-1])
    }
  }, [active])

  useEffect(() => {
    if (id) {
      setEditMode(true)
      setArray(stepperEditArray)
    } else {
      setEditMode(false)
      setArray(stepperArray)
    }
  }, [id])

  useEffect(() => {
    if (editMode) {
      setActiveStep(0)
      setCompletedSteps([-1, ...editstepper])
    }
  }, [editMode])

  const handleTabs = (name) => {
    setTab(name)
  }

  return (
    <>
      {
        editMode
          ? (
            <div className='row'>
            <div className='col-md-12'>
              <div className='card card-nav-tabs mt-10'>
                <div className='card-header card-header-primary'>
                  <div className='nav-tabs-navigation'>
                    <div className='nav-tabs-wrapper'>
                      <ul className='nav nav-tabs' data-tabs='tabs'>
                        <li className='client-nav-item'>
                          <a
                            className='nav-link active' href='#companydetails' data-toggle='tab'
                            onClick={() => { handleTabs('PARTNER') }}
                          >
                            <i className='bi bi-building m-2 text-white' />
                            Partner Details
                          </a>
                        </li>
                        <li className='client-nav-item'>
                          <a
                            className='nav-link' href='#merchantinfo' data-toggle='tab'
                            onClick={() => { handleTabs('CLIENT') }}
                          >
                            <i className='bi bi-person-plus-fill m-2 text-white' />
                            Client Details
                          </a>
                        </li>
                        <li className='client-nav-item'>
                          <a
                            className='nav-link' href='#userinfo' data-toggle='tab'
                            onClick={() => { handleTabs('USER') }}
                          >
                            <i className='bi bi-person-circle m-2 text-white' />
                            Users
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='card-body'>
              <ShowFields>
                {showTabDetails(tab)}
              </ShowFields>
            </div>
          </div>
            )
          : (
            <>
            <Stepper
              activeStep={activeStep}
              setActiveStep={setActiveStep}
              completedSteps={completedSteps}
              stepperArr={array}
              setCompletedSteps={setCompletedSteps}
              onStepperClick={onClickNext}
            />
            <ShowFields>
              {activeStep === 0 ? <CompanyDetails onClickNext={onClickNext} goBack={goBack} setPartnerDetails={setPartnerDetails} /> : null}
              {activeStep === 1 ? <ClientForm onClickNext={onClickNext} goBack={goBack} setPartnerDetails={setPartnerDetails} /> : null}
              {activeStep === 2 ? <UserPartnerInfo onClickNext={onClickNext} goBack={goBack} partnerDetails={partnerDetails} setPartnerDetails={setPartnerDetails} /> : null}
              {activeStep === 3
                ? <Checkout
                    onClickNext={onClickNext} goBack={goBack}
                    setPartnerDetails={setPartnerDetails} partnerDetails={partnerDetails}
                  />
                : null}
            </ShowFields>
            </>
            )
      }
    </>
  )
}

const mapStateToProps = (state) => ({
  chargebacks: state && state.chargebackStore && state.chargebackStore.chargebacks,
  loading: state && state.chargebackStore && state.chargebackStore.loading,
  exports: state && state.exportStore && state.exportStore.exports ? state.exportStore.exports : []
})

const mapDispatchToProps = (dispatch) => ({
  getChargebackDispatch: (params) => {}
})

export default connect(mapStateToProps, mapDispatchToProps)(PartnerOnboarding)