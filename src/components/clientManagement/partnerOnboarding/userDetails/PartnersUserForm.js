import { useEffect, useState } from "react";
import { editUserInfoValidation } from "./validation";
import {
  STATUS_RESPONSE,
  REGEX,
  SAVE_CURRENT
} from "../../../../utils/constants";
import { confirmationAlert, warningAlert } from "../../../../utils/alerts";
import { setPartnerUserDetailsPayload } from "../functions/formData";
import { getLocalStorage } from '../../../../utils/helper'
import {
  userActions,
  userGetDetailsActions,
  editUserActions,
  userRoleActions,
} from "../../../../store/actions";
import _ from "lodash";
import { connect } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";

const PartnersUserForm = (props) => {
  const {
    getUserDispatch,
    editUsersDispatch,
    getUserDetails,
    statusGUD,
    cleareditUserDispatch,
    gerUserRole,
    loadingGUD,
    getUserDetailsDispatch,
    clearUserDetailsDispatch,
    getUserRoleDispatch,
    loadingEUD,
    statusEUD,
    messageEUD
  } = props;
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    roleId: "",
  });
  const [errors, setErrors] = useState({});
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update/");
  const id = url && url[1];
  const history = useHistory()
  const currentPartnerId = getLocalStorage(SAVE_CURRENT.PARTNER_ID)

  const handleChange = (e) => {
    e.persist();
    setFormData((values) => ({ ...values, [e.target.name]: e.target.value }));
    setErrors({ ...errors, [e.target.name]: "" });
  };

  useEffect(() => {
    getUserRoleDispatch()
  }, []);

  useEffect(() => {
    if (id) {
      getUserDetailsDispatch(id)
    }
  }, [id])

  useEffect(() => {
    if (statusGUD === STATUS_RESPONSE.SUCCESS_MSG) {
      if (getUserDetails && getUserDetails._id) {
        const getDetailsData = setPartnerUserDetailsPayload(getUserDetails)
        setFormData(values => ({
          ...values,
          ...getDetailsData
        }))
      }
      clearUserDetailsDispatch();
    } else if (statusGUD === STATUS_RESPONSE.ERROR_MSG) {
      clearUserDetailsDispatch();
    }
  }, [statusGUD]);

  const onConfirm = () => {
    const params = {
      partnerId: id,
    };
    getUserDispatch(params);
    history.goBack()
  };

  useEffect(() => {
    if (statusEUD === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        "success",
        messageEUD,
        "success",
        "Back to User",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      cleareditUserDispatch();
    } else if (statusEUD === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        messageEUD,
        "",
        "Cancel",
        "Ok",
        () => { },
        () => { }
      );
      cleareditUserDispatch();
    }
  }, [statusEUD, messageEUD]);

  const handleSubmit = () => {
    const errorMsg = editUserInfoValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      const payload = {
        ...formData,
        partnerId: currentPartnerId
      };
      editUsersDispatch(id, payload);
    }
  }

  return (
    <>
      <div className="bg-skyBlue">
        <div className="card-header bg-skyBlue py-10">
          <div className="card-body">
            <div className="row">
              <div className="col-lg-6 my-auto">
                <h2 className="mb-5">User Info</h2>
              </div>
              <div className="col-lg-6">
                <div className="d-flex justify-content-end">
                  <button
                    className={`btn btn-darkRed
                      m-2 fa-pull-right`}
                      onClick={() => {
                        history.goBack()
                      }}
                  >
                    Back
                  </button>
                </div>
              </div>
            </div>
            <div className="card-header bg-skyBlue py-0">
              <div className="card-body p-0">
                <>
                  {loadingGUD ? (
                    <div className="d-flex justify-content-center py-5">
                      <div
                        className="spinner-border text-primary m-5"
                        role="status"
                      />
                    </div>
                  ) : (
                    <>
                      <div className="form-group row mb-4">
                        <div className="col-lg-12 mb-3">
                          <div className="row">
                            <div className="col-lg-2" />
                            <div className="col-lg-2">
                              <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                First Name:
                              </label>
                            </div>
                            <div className="col-lg-4">
                              <div className="input-group mb-5">
                                <span
                                  className="input-group-text"
                                  id="basic-addon3"
                                >
                                  <i className="bi bi-person-circle" />
                                </span>
                                <input
                                  name="firstName"
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  placeholder="First Name"
                                  aria-describedby="basic-addon3"
                                  onChange={(e) => handleChange(e)}
                                  value={formData.firstName || ""}
                                  autoComplete="off"
                                  maxLength={42}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_CHARS_SPACE.test(e.key)
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                              {errors && errors.firstName && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red;}"}</style>
                                  {errors.firstName}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row mb-4">
                        <div className="col-lg-12 mb-3">
                          <div className="row">
                            <div className="col-lg-2" />
                            <div className="col-lg-2">
                              <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                Last Name:
                              </label>
                            </div>
                            <div className="col-lg-4">
                              <div className="input-group mb-5">
                                <span
                                  className="input-group-text"
                                  id="basic-addon3"
                                >
                                  <i className="bi bi-person-circle" />
                                </span>
                                <input
                                  name="lastName"
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  placeholder="Last Name"
                                  aria-describedby="basic-addon3"
                                  onChange={(e) => handleChange(e)}
                                  value={formData.lastName || ""}
                                  autoComplete="off"
                                  maxLength={42}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_CHARS_SPACE.test(e.key)
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                              {errors && errors.lastName && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red;}"}</style>
                                  {errors.lastName}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row mb-4">
                        <div className="col-lg-12 mb-3">
                          <div className="row">
                            <div className="col-lg-2" />
                            <div className="col-lg-2">
                              <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                Email:
                              </label>
                            </div>
                            <div className="col-lg-4">
                              <div className="input-group mb-5">
                                <span
                                  className="input-group-text"
                                  id="basic-addon3"
                                >
                                  <i className="bi bi-envelope-fill" />
                                </span>
                                <input
                                  name="email"
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  placeholder="Email"
                                  aria-describedby="basic-addon3"
                                  onChange={(e) => handleChange(e)}
                                  value={formData.email || ""}
                                  autoComplete="off"
                                  maxLength={42}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(
                                        e.key
                                      )
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                              {errors && errors.email && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red;}"}</style>
                                  {errors.email}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row mb-4">
                        <div className="col-lg-12 mb-3">
                          <div className="row">
                            <div className="col-lg-2" />
                            <div className="col-lg-2">
                              <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                Password:
                              </label>
                            </div>
                            <div className="col-lg-4">
                              <div className="input-group mb-5">
                                <span
                                  className="input-group-text"
                                  id="basic-addon3"
                                >
                                  <i className="bi bi-lock-fill" />
                                </span>
                                <input
                                  name="password"
                                  type="password"
                                  className="form-control"
                                  id="basic-url"
                                  placeholder="Password"
                                  aria-describedby="basic-addon3"
                                  onChange={(e) => handleChange(e)}
                                  value={formData.password || ""}
                                  autoComplete="off"
                                  maxLength={42}
                                  onPaste={(e) => {
                                    e.preventDefault();
                                    return false;
                                  }}
                                  onCopy={(e) => {
                                    e.preventDefault();
                                    return false;
                                  }}
                                  onKeyPress={(e) => {
                                    if (
                                      !REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(
                                        e.key
                                      )
                                    ) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                              </div>
                              {errors && errors.password && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red;}"}</style>
                                  {errors.password}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row mb-4">
                        <div className="col-lg-12 mb-3">
                          <div className="row">
                            <div className="col-lg-2" />
                            <div className="col-lg-2">
                              <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                Role:
                              </label>
                            </div>
                            <div className="col-lg-4">
                              <div className="input-group mb-5">
                                <span
                                  className="input-group-text"
                                  id="basic-addon3"
                                >
                                  <i className="bi bi-envelope" />
                                </span>
                                <select
                                  name="roleId"
                                  className="form-select form-select-solid"
                                  data-control="select"
                                  data-placeholder="Select an option"
                                  data-allow-clear="true"
                                  onChange={(e) => handleChange(e)}
                                  value={formData.roleId || ""}
                                >
                                  <option value="">Select Role...</option>
                                  {gerUserRole &&
                                    gerUserRole.length > 0 &&
                                    gerUserRole.map((item, i) => (
                                      <option key={i} value={item._id}>
                                        {item.role}
                                      </option>
                                    ))}
                                </select>
                              </div>
                              {errors && errors.roleId && (
                                <div className="rr mt-1">
                                  <style>{".rr{color:red}"}</style>
                                  {errors.roleId}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group row mt-4">
                        <div className="col-lg-6" />
                        <div className="col-lg-6">
                          <div className="col-lg-12">
                            <button
                              className="btn btn-blue m-2 fa-pull-right"
                              onClick={() => {
                                handleSubmit();
                              }}
                            >
                              {loadingEUD ? (
                                <span
                                  className="spinner-border spinner-border-sm mx-3"
                                  role="status"
                                  aria-hidden="true"
                                />
                              ) : (
                                "Save"
                              )}
                            </button>
                          </div>
                        </div>
                      </div>
                    </>
                  )}
                </>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  const { userStore, userRoleStore, editUserStore } = state;
  return {
    getUserDetails: userStore && userStore.userGetDetails ? userStore.userGetDetails : [],
    statusGUD: userStore && userStore.statusGUD ? userStore.statusGUD : "",
    messagesGUD: userStore && userStore.messagesGUD ? userStore.messagesGUD : "",
    loading: userStore && userStore.loading ? userStore.loading : false,
    loadingGUD: userStore && userStore.loadingGUD ? userStore.loadingGUD : false,
    getUser: userStore && userStore.getUser ? userStore.getUser : [],
    gerUserRole: userRoleStore && userRoleStore.gerUserRole ? userRoleStore.gerUserRole : [],
    loadingEUD: editUserStore && editUserStore.loadingEUD ? editUserStore.loadingEUD : false,
    statusEUD: editUserStore && editUserStore.statusEUD ? editUserStore.statusEUD : "",
    messageEUD: editUserStore && editUserStore.messageEUD ? editUserStore.messageEUD : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getUserDetailsDispatch: (params) => dispatch(userGetDetailsActions.getUserDetails(params)),
  clearUserDetailsDispatch: () => dispatch(userGetDetailsActions.clearUserDetails()),
  getUserDispatch: (params) => dispatch(userActions.getUser(params)),
  editUsersDispatch: (id, params) => dispatch(editUserActions.editUser(id, params)),
  cleareditUserDispatch: () => dispatch(editUserActions.cleareditUser()),
  getUserRoleDispatch: (data) => dispatch(userRoleActions.getUserRole(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(PartnersUserForm);