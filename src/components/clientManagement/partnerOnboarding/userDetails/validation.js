import { USER_ERROR, CRM_ERROR, REGEX } from '../../../../utils/constants'

export const companyValidation = (values, setErrors) => {
  const errors = {}
  if (!values.partnerFirstName) {
    errors.partnerFirstName = USER_ERROR.PARTNER_FIRST_NAME_REQUIRED
  }
  if (!values.partnerLastName) {
    errors.partnerLastName = USER_ERROR.PARTNER_LAST_NAME_REQUIRED
  }
  if (!values.address) {
    errors.address = USER_ERROR.ADDRESS_REQUIRED
  }
  if (!values.email) {
    errors.email = USER_ERROR.EMAIL
  }
  if (values.email && !REGEX.EMAIL.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('@')
    const emailName = getEmailName && getEmailName[0]
    if(REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
   }
  if (!values.phone) {
    errors.phone = USER_ERROR.PHONE_NUMBER
  }
  if (values.phone && !REGEX.PHONE_NUMBER.test(values.phone)) {
    errors.phone = USER_ERROR.PHONE_NUMBER_INVALID
  }
  if (values.url && !REGEX.URL.test(values.url)) {
    errors.url = USER_ERROR.URL_VALID
  }
  if (!values.zip) {
    errors.zip = USER_ERROR.ZIPCODE_REQUIRED
  }
  if (!values.industry) {
    errors.industry = USER_ERROR.INDUSTRY_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const clientValidation = (values, setErrors) => {
  const errors = {}
  if (!values.clientFirstName) {
    errors.clientFirstName = USER_ERROR.CLIENT_FIRST_NAME_REQUIRED
  }
  if (!values.clientLastName) {
    errors.clientLastName = USER_ERROR.CLIENT_LAST_NAME_REQUIRED
  }
  if (!values.clientNotes) {
    errors.clientNotes = USER_ERROR.CLIENT_DESCRIPTION_REQUIRED
  }
  if (!values.company) {
    errors.company = USER_ERROR.COMPANYNAME
  }
  if (!values.clientAddress) {
    errors.clientAddress = USER_ERROR.ADDRESS_REQUIRED
  }
  if (!values.clientCity) {
    errors.clientCity = USER_ERROR.CITY_REQUIRED
  }
  if (!values.clientEmail) {
    errors.clientEmail = USER_ERROR.EMAIL
  }
  if (values.clientEmail && !REGEX.EMAIL.test(values.clientEmail)) {
    errors.clientEmail = USER_ERROR.EMAIL_INVALID
  }
  if (values.clientEmail) {
    const getEmailName = values.clientEmail.split('@')
    const emailName = getEmailName && getEmailName[0]
    if(REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.clientEmail = USER_ERROR.EMAIL_INVALID
    }
   }
  if (!values.clientPhoneNumber) {
    errors.clientPhoneNumber = USER_ERROR.PHONE_NUMBER
  }
  if (values.clientPhoneNumber && !REGEX.PHONE_NUMBER.test(values.clientPhoneNumber)) {
    errors.clientPhoneNumber = USER_ERROR.PHONE_NUMBER_INVALID
  }
  if (!values.clientState) {
    errors.clientState = USER_ERROR.STATE_REQUIRED
  }
  if (!values.clientZip) {
    errors.clientZip = USER_ERROR.ZIPCODE_REQUIRED
  }
  if (!values.clientCountry) {
    errors.clientCountry = USER_ERROR.COUNTRY_REQUIRED
  }
  if (!values.industry) {
    errors.industry = USER_ERROR.INDUSTRY_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const merchantValidation = (values, setErrors) => {
  const errors = {}
  if (!values.merchantName) {
    errors.merchantName = USER_ERROR.MERCHANT_NAME_REQUIRED
  }
  if (!values.address) {
    errors.address = USER_ERROR.ADDRESS_REQUIRED
  }
  if (!values.city) {
    errors.city = USER_ERROR.CITY_REQUIRED
  }
  if (!values.email) {
    errors.email = USER_ERROR.EMAIL
  }
  if (values.email && !REGEX.EMAIL.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('@')
    const emailName = getEmailName && getEmailName[0]
    if(REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
   }
  if (!values.phoneNumber) {
    errors.phoneNumber = USER_ERROR.PHONE_NUMBER
  }
  if (values.phoneNumber && !REGEX.PHONE.test(values.phoneNumber)) {
    errors.phoneNumber = USER_ERROR.PHONE_NUMBER_INVALID
  }
  if (!values.state) {
    errors.state = USER_ERROR.STATE_REQUIRED
  }
  if (!values.externalId) {
    errors.externalId = USER_ERROR.EXTERNAL_ID_REQUIRED
  }
  if (!values.ext) {
    errors.ext = USER_ERROR.EXISTING_PHONE_REQUIRED
  }
  if (!values.companyDescription) {
    errors.companyDescription = USER_ERROR.COMPANY_DESCRIPTION_REQUIRED
  }
  if (!values.zip) {
    errors.zip = USER_ERROR.ZIPCODE_REQUIRED
  }
  if (!values.country) {
    errors.country = USER_ERROR.COUNTRY_REQUIRED
  }
  if (!values.industry) {
    errors.industry = USER_ERROR.INDUSTRY_REQUIRED
  }
  if (!values.midNumber) {
    errors.midNumber = USER_ERROR.MIDNUMBER_REQUIRED
  }
  if (!values.descriptorName) {
    errors.descriptorName = USER_ERROR.DESCRIPTOR_NAME_REQUIRED
  }
  if (!values.descriptorId) {
    errors.descriptorId = USER_ERROR.DESCRIPTOR_ID_REQUIRED
  }
  if (!values.doingBusinessAs) {
    errors.doingBusinessAs = USER_ERROR.DOING_BUSINESS_AS_REQUIRED
  }
  if (!values.bin) {
    errors.bin = USER_ERROR.BIN_REQUIRED
  }
  if (!values.caid) {
    errors.caid = USER_ERROR.CAID_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const userValidation = (values, setErrors) => {
  const errors = {}
  if (!values.email) {
    errors.email = USER_ERROR.EMAIL_REQUIRED
  } else if (values.email && !/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('@')
    const emailName = getEmailName && getEmailName[0]
    if(REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
   }
  if (!values.password) {
    errors.password = USER_ERROR.PASSWORD_REQUIRED
  } else if (!REGEX.PASSWORD_MIN_MAX_LENGTH.test(values.password)) {
    errors.password = USER_ERROR.PASSWORD_MIN_MAX_LENGTH
  }
  if (!values.cPassword) {
    errors.cPassword = USER_ERROR.CONFIRM_PASSWORD_REQUIRED
  }
  if (values.password !== values.cPassword) {
    errors.cPassword = USER_ERROR.PASSWORD_SAME
  }
  setErrors(errors)
  return errors
}

export const editUserInfoValidation = (values, setErrors) => {
  const errors = {}
  if (!values.email) {
    errors.email = USER_ERROR.EMAIL_REQUIRED
  } else if (values.email && !/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('@')
    const emailName = getEmailName && getEmailName[0]
    if(REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
   }
  if (!values.password) {
    errors.password = USER_ERROR.PASSWORD_REQUIRED
  } else if (!REGEX.PASSWORD_MIN_MAX_LENGTH.test(values.password)) {
    errors.password = USER_ERROR.PASSWORD_MIN_MAX_LENGTH
  }
  if (!values.firstName) {
    errors.firstName = USER_ERROR.NAME_REQUIRED
  }
  if (!values.lastName) {
    errors.lastName = USER_ERROR.NAME_REQUIRED
  }
  if (!values.roleId) {
    errors.roleId = USER_ERROR.ROLE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const userInfoValidation = (values, setErrors) => {
  const errors = {}
  if (!values.email) {
    errors.email = USER_ERROR.EMAIL_REQUIRED
  } else if (values.email && !/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = USER_ERROR.EMAIL_INVALID
  }
  if (values.email) {
    const getEmailName = values.email.split('@')
    const emailName = getEmailName && getEmailName[0]
    if(REGEX.ALPHA_UPPER_CASE.test(emailName)) {
      errors.email = USER_ERROR.EMAIL_INVALID
    }
   }
  if (!values.password) {
    errors.password = USER_ERROR.PASSWORD_REQUIRED
  } else if (!REGEX.PASSWORD_MIN_MAX_LENGTH.test(values.password)) {
    errors.password = USER_ERROR.PASSWORD_MIN_MAX_LENGTH
  }
  if (!values.name) {
    errors.name = USER_ERROR.NAME_REQUIRED
  }
  if (!values.status) {
    errors.status = USER_ERROR.STATUS_REQUIRED
  }
  if (!values.role) {
    errors.role = USER_ERROR.ROLE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const integrationValidation = (values, setErrors) => {
  const errors = {}
  if (!values.companyName) {
    errors.companyName = USER_ERROR.COMPANYNAME
  }
  if (!values.processorType) {
    errors.processorType = CRM_ERROR.PROCESSOR_TYPE_REQUIRED
  }
  if (!values.paymentGateway) {
    errors.paymentGateway = CRM_ERROR.PAYMENT_GATE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const crmValidation = (values, setErrors) => {
  const errors = {}
  if (!values.crmId) {
    errors.crmId = USER_ERROR.COMPANYNAME
  }
  setErrors(errors)
  return errors
}

export const addClientValidation = (values, setErrors) => {
  const errors = {}
  if (!values.clientId) {
    errors.clientId = USER_ERROR.CLIENT_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const processorValidation = (values, setErrors) => {
  const errors = {}
  if (!values.processorType) {
    errors.processorType = CRM_ERROR.PROCESSOR_TYPE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const paymentValidation = (values, setErrors) => {
  const errors = {}
  if (!values.paymentGateway) {
    errors.paymentGateway = CRM_ERROR.PAYMENT_GATE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const addCrmValidation = (values, setErrors) => {
  const errors = {}
  if (!values.apiKey) {
    errors.apiKey = CRM_ERROR.API_KEY_REQUIRED
  }
  if (!values.apiSecretKey) {
    errors.apiSecretKey = CRM_ERROR.API_SECRET_REQUIRED
  }
  if (!values.apiEndPoint) {
    errors.apiEndPoint = CRM_ERROR.API_END_POINT_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const addGateValidation = (values, setErrors) => {
  const errors = {}
  if (!values.gatewayEndPoint) {
    errors.gatewayEndPoint = CRM_ERROR.API_KEY_REQUIRED
  }
  if (!values.gatewayApiKey) {
    errors.gatewayApiKey = CRM_ERROR.API_SECRET_REQUIRED
  }
  if (!values.gatewayApiSecretKey) {
    errors.gatewayApiSecretKey = CRM_ERROR.API_END_POINT_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const addSFTPValidation = (values, setErrors) => {
  const errors = {}
  if (!values.hostName) {
    errors.hostName = CRM_ERROR.HOST_NAME_REQUIRED
  }
  if (!values.portNumber) {
    errors.portNumber = CRM_ERROR.PORT_NUMBER_REQUIRED
  }
  if (!values.username) {
    errors.username = CRM_ERROR.USER_NAME_REQUIRED
  }
  if (!values.password) {
    errors.password = CRM_ERROR.PASSWORD_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const addFaxValidation = (values, setErrors) => {
  const errors = {}
  if (!values.faxNumber) {
    errors.faxNumber = CRM_ERROR.FAX_NUMBER_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const addEmailValidation = (values, setErrors) => {
  const errors = {}
  if (!values.email) {
    errors.email = CRM_ERROR.EMAIL_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const addApilValidation = (values, setErrors) => {
  const errors = {}
  if (!values.apiKey) {
    errors.apiKey = CRM_ERROR.API_REQUIRED
  }
  if (!values.apiSecretKey) {
    errors.apiSecretKey = CRM_ERROR.API_REQUIRED
  }
  if (!values.apiEndPoint) {
    errors.apiEndPoint = CRM_ERROR.API_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const packageValidation = (values, setErrors) => {
  const errors = {}
  if (!values.orderIntelligence && !values.preventionAlert && !values.chargebackManagement) {
    errors.orderIntelligence = 'Please Select at least one subscription!'
  }
  setErrors(errors)
  return errors
}