import React, { useEffect, useState } from 'react'
import { KTSVG } from '../../../../theme/helpers'
import { useLocation } from 'react-router-dom'
import {
  SLICE_CHARACTERS,
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  USER_ERROR,
  SET_STORAGE
} from "../../../../utils/constants";
import ReactSelect from "../../../../theme/layout/components/ReactSelect";
import { setLocalStorage, getLocalStorage } from "../../../../utils/helper";
import { TooltipOnHover } from "../../../../theme/layout/components/tooltipOnHover/TooltipOnHover";
import _ from "lodash";
import ReactPaginate from "react-paginate";
import { connect } from "react-redux";
import { Modal } from "../../../../theme/layout/components/modal";
import {
  warningAlert,
  confirmAlert,
  confirmationAlert
} from '../../../../utils/alerts'
import {
  partnerClientActions,
  clientGetDetailsActions,
  getPartnersClientActions,
  addPartnersClientAction
} from "../../../../store/actions";
import color from "../../../../utils/colors";
import { addClientValidation } from "../userDetails/validation";

const ClientForm = (props) => {
  const {
    goBack,
    setPartnerDetails,
    className,
    getClientDetailsDispatch,
    clearClientDetailsDispatch,
    getClientDispatch,
    getPartnersClientDispatch,
    clearPartnersClientDispatch,
    getPartnerClient,
    onClickNext,
    clientGetDetails,
    loadingGCLD,
    getUsersPartners,
    loadingGetUsersPartners,
    partnerClientData,
    addPartnersClientDispatch,
    messageAPClientErr,
    statusAPClient,
    messageAPClient,
    clearAddPartnersClient
  } = props
  const didMount = React.useRef(false)
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update/");
  const id = url && url[1];
  const [show, setShow] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [currentId, setCurrentId] = useState();
  const [errors, setErrors] = useState({});
  const [showTable, setShowTable] = useState(false);
  const [clientOption, setClientOption] = useState();
  const [selectedOption, setSelectedOption] = useState("");
  const [tableForm, setTableForm] = useState([]);
  const [addedClientIds, setAddedClientIds] = useState([]);
  const [formData, setFormData] = useState({
    clientId: "",
  });
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [limit, setLimit] = useState(25);

  const handleAddTable = () => {
    if (editMode) {
    const payload = {
      clientId: formData.clientId,
      partnerId: id
    } 
    addPartnersClientDispatch(payload, null)
  } else {
    setTableForm((values) => [...values, clientGetDetails]);
  }
    setAddedClientIds((values) => [...values, formData.clientId]);
    setShowTable(true)
    setCurrentId();
    setShow(false);
    clearClientDetailsDispatch();
    setSelectedOption();
  };

  const handleAddClick = () => {
    const errorMsg = addClientValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      getClientDetailsDispatch(currentId)
      setShow(true)
    }
  }

  const handleSubmit = () => {
    setPartnerDetails((values) => ({ ...values, clientId: addedClientIds }))
    setLocalStorage(SET_STORAGE.PARTNER_USER_DETAILS, JSON.stringify(tableForm));
    onClickNext(2);
  }

  const onConfirmDelete = (deleteId) => {
    if (editMode) {
      const payload = {
        clientId: deleteId,
        partnerId: id
      }
      const params= {
        remove:""
      }
      addPartnersClientDispatch(payload, params)
    } else {
        const delObj = _.forEach(tableForm, (obj) => {
          return obj._id !== deleteId ? obj : null
        });
      setTableForm(delObj)
    }
  }

  const onDeleteItem = (id) => {  
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_CLIENT,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmDelete(id)
      },
      () => { }
    )
  }

  const onPostConfirm = () => {
    if (editMode) {
      getPartnersClientDispatch(id)
    }
    getClientDispatch()
  };

  useEffect(() => {
    if (statusAPClient === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAPClient,
        'success',
        'ok',
        () => {
          onPostConfirm()
        },
        () => {
          onPostConfirm()
        }
      )
      clearAddPartnersClient()
    } else if (statusAPClient === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert('Error', messageAPClientErr, '', 'Ok')
    }
    clearAddPartnersClient()
  }, [statusAPClient])

  useEffect(() => {
    getClientDispatch()
    return () => {
      clearPartnersClientDispatch()
      setTableForm([])
    }
  }, [])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const onModalClose = () => {
    setShow(false);
    setCurrentId();
    setFormData({ clientId: "" });
    setAddedClientIds([]);
    setSelectedOption();
    clearClientDetailsDispatch();
  };

  const handleSelectChange = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedOption(selectedOption);
      setCurrentId(selectedOption.value);
      setFormData((values) => ({ ...values, clientId: selectedOption.value }));
      setErrors((values) => ({ ...values, clientId: "" }));
    } else {
      setCurrentId();
      setErrors((values) => ({ ...values, clientId: USER_ERROR.CLIENT_ALREADY }));
      setSelectedOption();
      setFormData((values) => ({ ...values, clientId: "" }));
    }
  }

  useEffect(() => {
    if (partnerClientData && partnerClientData.length > 0) {
      const data = getDefaultOptions(partnerClientData);
      setClientOption(data);
    }
  }, [partnerClientData]);

  useEffect(() => {
    if (getUsersPartners && getUsersPartners.length > 0) {
      setTableForm(getUsersPartners && getUsersPartners);
      setShowTable(true)
    }
  }, [getUsersPartners]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target;
    setLimit(value);
    if (editMode) {
    const params = {
      limit: value,
      page: activePageNumber
    }
    getPartnersClientDispatch(id)
    }
  };

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    if (editMode) {
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getPartnersClientDispatch(id)
    }
  }

  const getDefaultOptions = (data) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
        data.map((item) =>
          defaultOptions.push({
            label: `${item.company ? item.company : ''}`,
            value: item._id
          })
        )
      return defaultOptions
    }
  }

  useEffect(() => {
    if (id) {
      setEditMode(true);
      getPartnersClientDispatch(id)
    } else {
      setEditMode(false);
    }
  }, [id]);

  useEffect(() => {
    if (!didMount.current) { didMount.current = true }
  })

  const totalPages =
    getPartnerClient && getPartnerClient.count
      ? Math.ceil(parseInt(getPartnerClient && getPartnerClient.count) / limit)
      : 1

  return (
    <>
      <Modal showModal={show} modalWidth={850}>
        <div className='' id='crmModal'>
          <div className=''>
            <div className='p-5'>
              <div className='d-flex justify-content-between align-items-center'>
                <h2 className='me-8'>Client Details</h2>
                <button
                  type='button'
                  className='btn btn-lg btn-icon btn-active-light-primary close'
                  onClick={() => {
                    onModalClose()
                  }}
                >
                  {/* eslint-disable */}
                  <KTSVG
                    path="/media/icons/duotune/arrows/arr061.svg"
                    className="svg-icon-1"
                  />
                  {/* eslint-disable */}
                </button>
              </div>
              <div className="bg-light">
                <form className="container-fixed">
                  <div className="card-body">
                    {loadingGCLD ? (
                      <div className="d-flex justify-content-between align-items-center w-100">
                        <div
                          className="spinner-border text-primary mt-6 m-auto"
                          role="status"
                        />
                      </div>
                    ) : (
                      <div className="d-flex h-100 align-items-center">
                        <div className="w-100 rounded-3 bg-light bg-opacity-75 pt-6 px-10">
                          <div className="w-80 row">
                            <div className="col-lg-6 col-md-6">
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Company Name : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.company
                                        ? clientGetDetails.company : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Industry : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.industry ? clientGetDetails.industry : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Customer Id : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.customerId ? clientGetDetails.customerId : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Country : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientCountry &&
                                        clientGetDetails.clientCountry.name ?
                                        clientGetDetails.clientCountry.name : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  City : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientCity &&
                                        clientGetDetails.clientCity.name ?
                                        clientGetDetails.clientCity.name : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  State : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientState &&
                                        clientGetDetails.clientState.name ?
                                        clientGetDetails.clientState.name : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Zip Code : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientZip ? clientGetDetails.clientZip : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Address : &nbsp; &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientAddress ? clientGetDetails.clientAddress : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Email : &nbsp;
                                </span>
                                <div className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientEmail ? clientGetDetails.clientEmail : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </div>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Phone : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientPhoneNumber ? clientGetDetails.clientPhoneNumber : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  URL : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientURL ? clientGetDetails.clientURL : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Source : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.leadSource ? clientGetDetails.leadSource : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Client Complexity : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientComplexity ? clientGetDetails.clientComplexity : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Currency : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.currency ? clientGetDetails.currency : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                              <div className="d-flex align-items-center mb-5">
                                <span className="fw-bold fs-6 text-gray-800">
                                  Company Description : &nbsp;
                                </span>
                                <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                  <TooltipOnHover
                                    string={
                                      clientGetDetails &&
                                        clientGetDetails.clientDescription ? clientGetDetails.clientDescription : "--"
                                    }
                                    sliceUpTo={SLICE_CHARACTERS.UP_TO_40}
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="d-flex justify-content-end">
                    <button
                      type="button"
                      className="btn btn-sm btn-darkRed m-2 fa-pull-right"
                      onClick={() => {
                        onModalClose();
                      }}
                    >
                      Cancel
                    </button>
                    <button
                      type="button"
                      className="btn btn-sm btn-green m-2 fa-pull-right"
                      onClick={() => {
                        handleAddTable();
                      }}
                    >
                      Add
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <div className="card-header bg-skyBlue py-10">
        <div className="card-body">
          <h2 className="mb-5">Add Client</h2>
          <div className="form-group row mb-2">
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="d-flex justify-content-start my-auto">
                    <div className="col-lg-5 pr-3 me-3">
                      <ReactSelect
                        styles={customStyles}
                        isMulti={false}
                        name="clientId"
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleSelectChange}
                        options={clientOption}
                        value={selectedOption}
                      />
                      {errors && errors.clientId && (
                        <div className="rr mt-1">
                          <style>{".rr{color:red}"}</style>
                          {errors.clientId}
                        </div>
                      )}
                    </div>
                    <div>
                      <button
                        className="btn btn-green mb-3 py-2"
                        onClick={() => {
                          handleAddClick();
                        }}
                      >
                        Select
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {showTable ? (
            <>
              <div className="w-100 d-flex justify-content-start mt-2">
                <div className={`card ${className}`}>
                  <div className="card-body py-3">
                    <div className="d-flex justify-content-start col-md-4">
                      <div className="col-md-3 mt-1">
                        {tableForm && tableForm.length ? (
                          <span className="text-muted fw-bold d-flex fs-5 mt-2">
                            Total: &nbsp;{" "}
                            <span className="text-gray-700 fw-bolder text-hover-primary fs-5">
                              {tableForm.length}
                            </span>
                          </span>
                        ) : null}
                      </div>
                      <div className="col-md-7 d-flex">
                        <label className="col-form-label text-lg-start">
                          Record(s) per Page : &nbsp;{" "}
                        </label>
                        <div className="col-md-3">
                          <select
                            className="form-select w-6rem"
                            data-control="select"
                            data-placeholder="Select an option"
                            data-allow-clear="true"
                            onChange={(e) => handleRecordPerPage(e)}
                          >
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="75">75</option>
                            <option value="100">100</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="table-responsive">
                      <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                        <thead>
                          <tr className="fw-bold fs-6 text-gray-800">
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Action</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Company Name</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Email</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Phone</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Country</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>State</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>City</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Address</span>
                              </div>
                            </th>
                            <th className="min-w-200px text-start">
                              <div className="d-flex">
                                <span>Zip</span>
                              </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {loadingGetUsersPartners ? (
                            <tr>
                              <td colSpan="5" className="text-center">
                                <div
                                  className="spinner-border text-primary m-5"
                                  role="status"
                                />
                              </td>
                            </tr>
                          ) : tableForm && tableForm.length > 0 ? (
                            tableForm.map((item, i) => {
                              return (
                                <tr
                                  key={i}
                                  style={
                                    i === 0
                                      ? { borderColor: "black" }
                                      : { borderColor: "white" }
                                  }
                                >
                                  <td className="min-width-150px">
                                    <div>
                                      &nbsp;&nbsp;&nbsp;
                                      <button
                                        className="btn btn-icon btn-icon-danger btn-sm w-10px h-10px"
                                        onClick={() => onDeleteItem(item._id)}
                                      >
                                        {/* {/ eslint-disable /} */}
                                        <KTSVG
                                          path="/media/icons/duotune/general/gen027.svg"
                                          className="svg-icon-3"
                                        />
                                        {/* {/ eslint-enable /} */}
                                      </button>
                                    </div>
                                  </td>
                                  <td>
                                    {item.company && item.company
                                      ? item.company
                                      : "--"}
                                  </td>
                                  <td>
                                    {item.clientEmail ? item.clientEmail : "--"}
                                  </td>
                                  <td>
                                    {item.clientPhoneNumber
                                      ? item.clientPhoneNumber
                                      : "--"}
                                  </td>
                                  <td>
                                    {item.clientCountry &&
                                      item.clientCountry.name
                                      ? item.clientCountry.name
                                      : "--"}
                                  </td>
                                  <td>
                                    {item.clientState && item.clientState.name
                                      ? item.clientState.name
                                      : "--"}
                                  </td>
                                  <td>
                                    {item.clientCity && item.clientCity.name
                                      ? item.clientCity.name
                                      : "--"}
                                  </td>
                                  <td>
                                    <TooltipOnHover
                                      string={
                                        item.clientAddress
                                          ? item.clientAddress
                                          : "--"
                                      }
                                      sliceUpTo={SLICE_CHARACTERS.UP_TO_35}
                                    />
                                  </td>
                                  <td>
                                    {item.clientZip ? item.clientZip : "--"}
                                  </td>
                                </tr>
                              );
                            })
                          ) : (
                            <tr className="text-center py-3">
                              <td colSpan="100%">No record(s) found</td>
                            </tr>
                          )}
                        </tbody>
                      </table>
                      <div className="form-group row mb-4 mt-6">
                        <div className="col-lg-12 mb-4 align-items-end d-flex">
                          <div className="col-lg-12">
                            <ReactPaginate
                              nextLabel="Next >"
                              onPageChange={handlePageClick}
                              pageRangeDisplayed={3}
                              marginPagesDisplayed={2}
                              pageCount={totalPages}
                              previousLabel="< Prev"
                              pageClassName="page-item"
                              pageLinkClassName="page-link"
                              previousClassName="page-item"
                              previousLinkClassName="page-link"
                              nextClassName="page-item"
                              nextLinkClassName="page-link"
                              breakLabel="..."
                              breakClassName="page-item"
                              breakLinkClassName="page-link"
                              containerClassName="pagination"
                              activeClassName="active"
                              renderOnZeroPageCount={null}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : null}
          {errors && errors.crmForm && (
            <div className="rr mt-0 text-danger">
              <style>{".rr{color:red}"}</style>
              {errors.crmForm}
            </div>
          )}
        </div>
        {
          !editMode ? (
            <>
            <div className="form-group row mb-4">
              <div className="col-lg-6" />
              <div className="col-lg-6">
                <div className="col-lg-11">
                  <button
                    className="btn btn-orange m-2 fa-pull-right"
                    onClick={(event) => {
                      handleSubmit(event);
                    }}
                  >
                    Next
                  </button>
                  <button
                    className='btn btn-darkRed m-2 fa-pull-right'
                    onClick={() => {
                      goBack(0)
                    }}
                  >
                    Back
                  </button>
                </div>
              </div>
            </div>
            </>
          ) : null
        }
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loadingGCLD: state && state.clientDetailsStore && state.clientDetailsStore.loadingGCLD ? state.clientDetailsStore.loadingGCLD : false,
  clientGetDetails: state && state.clientDetailsStore && state.clientDetailsStore.clientGetDetails,
  getPartnerClient: state && state.clientStore && state.clientStore.getPartnerClient,
  loadingGetClient: state && state.clientStore && state.clientStore.loading,
  getUsersPartners: state && state.getPartnersUsersStore && state.getPartnersUsersStore.getUsersPartners,
  loadingGetUsersPartners: state && state.getPartnersUsersStore && state.getPartnersUsersStore.loadingGetUsersPartners,
  messageAPClientErr: state && state.addPartnerClientStore && state.addPartnerClientStore.messageAPClientErr,
  statusAPClient: state && state.addPartnerClientStore && state.addPartnerClientStore.statusAPClient,
  messageAPClient: state && state.addPartnerClientStore && state.addPartnerClientStore.messageAPClient,
  partnerClientData: state && state.partnerClientStore && state.partnerClientStore.partnerClientData,
});

const mapDispatchToProps = (dispatch) => ({
  getClientDetailsDispatch: (id) => dispatch(clientGetDetailsActions.getClientDetails(id)),
  clearClientDetailsDispatch: () => dispatch(clientGetDetailsActions.clearClientDetails()),
  getClientDispatch: (data) => dispatch(partnerClientActions.getPartnerClient(data)),
  clearClientDispatch: () => dispatch(partnerClientActions.clearPartnerClient()),
  getPartnersClientDispatch: (data) => dispatch(getPartnersClientActions.getDetails(data)),
  clearPartnersClientDispatch: () => dispatch(getPartnersClientActions.clear()),
  addPartnersClientDispatch: (data, params) => dispatch(addPartnersClientAction.addPartnersClient(data, params)),
  clearAddPartnersClient: () => dispatch(addPartnersClientAction.clearAddPartnersClient()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ClientForm);