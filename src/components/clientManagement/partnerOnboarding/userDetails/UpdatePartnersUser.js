import { useEffect, useState } from "react";
import { editUserInfoValidation } from "./validation";
import { KTSVG } from "../../../../theme/helpers";
import {
  STATUS_RESPONSE,
  REGEX,
  SLICE_CHARACTERS,
  SWEET_ALERT_MSG,
  SAVE_CURRENT
} from "../../../../utils/constants";
import { setLocalStorage } from '../../../../utils/helper'
import { confirmationAlert, warningAlert, confirmAlert } from "../../../../utils/alerts";
import { TooltipOnHover } from "../../../../theme/layout/components/tooltipOnHover/TooltipOnHover";
import ReactPaginate from "react-paginate";
import {
  userActions,
  editUserActions,
  userRoleActions,
  addUserActions,
  deleteUsersActions
} from "../../../../store/actions";
import _ from "lodash";
import { connect } from "react-redux";
import { Link, useLocation } from "react-router-dom";

const UpdatePartnersUser = (props) => {
  const {
    getUserDispatch,
    addUsersDispatch,
    deleteUsersDispatch,
    clearDeleteUserDispatch,
    clearaddUserDispatch,
    getUserDetails,
    statusGUD,
    statusEUD,
    messageEUD,
    cleareditUserDispatch,
    gerUserRole,
    loadingGUD,
    getUser,
    getUserRoleDispatch,
    loadingAUD,
    statusAUD,
    messageAUD,
    statusDeleteUsers,
    messageDeleteUsers
  } = props;
  const [userId, setUserId] = useState();
  const [showTable, setShowTable] = useState(true);
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [limit, setLimit] = useState(25);
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    roleId: "",
  });
  const [errors, setErrors] = useState({});
  const pathName = useLocation().pathname;
  const url = pathName && pathName.split("update/");
  const id = url && url[1];

  const handleChange = (e) => {
    e.persist();
    setFormData((values) => ({ ...values, [e.target.name]: e.target.value }));
    setErrors({ ...errors, [e.target.name]: "" });
  };

  useEffect(() => {
    const params = {
      partnerId: id,
    };
    getUserDispatch(params);
    getUserRoleDispatch()
  }, []);


  useEffect(() => {
    if (statusGUD === STATUS_RESPONSE.SUCCESS_MSG) {
      if (getUserDetails && getUserDetails._id) {
        setUserId(getUserDetails._id);
      }
    } else if (statusGUD === STATUS_RESPONSE.ERROR_MSG) {
    }
  }, [statusGUD]);

  const onAddConfirm = () => {
    const params = {
      partnerId: id,
    };
    getUserDispatch(params);
    setShowTable(true)
  };

  useEffect(() => {
    if (statusAUD === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        "success",
        messageAUD,
        "success",
        "Back to User",
        "Ok",
        () => {
          onAddConfirm();
        },
        () => {
          onAddConfirm();
        }
      );
      clearaddUserDispatch();
    } else if (statusAUD === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        messageAUD,
        "",
        "Cancel",
        "Ok",
        () => {},
        () => {}
      );
      clearaddUserDispatch();
    }
  }, [statusAUD, messageAUD]);

  const onDeleteConfirm = () => {
    const params = {
      partnerId: id,
    };
    getUserDispatch(params);
  };


  useEffect(() => {
    if (statusDeleteUsers === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageDeleteUsers,
        'success',
        'ok',
        () => {
          onDeleteConfirm()
        },
        () => {
          onDeleteConfirm()
        }
      )
      clearDeleteUserDispatch();
    } else if (statusDeleteUsers === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        messageDeleteUsers,
        "",
        "Cancel",
        "Ok",
        () => {},
        () => {}
      );
      clearDeleteUserDispatch();
    }
  }, [statusDeleteUsers, messageDeleteUsers]);

  const onConfirm = () => {
    const params = {
      partnerId: id,
    };
    getUserDispatch(params);
  };

  useEffect(() => {
    if (statusEUD === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        "success",
        messageEUD,
        "success",
        "Back to User",
        "Ok",
        () => {
          onConfirm();
        },
        () => {
          onConfirm();
        }
      );
      cleareditUserDispatch();
    } else if (statusEUD === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        messageEUD,
        "",
        "Cancel",
        "Ok",
        () => {},
        () => {}
      );
      cleareditUserDispatch();
    }
  }, [statusEUD, messageEUD]);

  const handleSubmit = () => {
    const errorMsg = editUserInfoValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      const payload = {
        ...formData,
        partnerId: id,
      };
      addUsersDispatch(payload)
    }
  };

  const handleRecordPerPage = (e) => {
    const { value } = e.target;
    setLimit(value);
    const params = {
      limit: value,
      page: activePageNumber,
    };
    getUserDispatch(params);
  };

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: pageNumber,
    };
    setActivePageNumber(pageNumber);
    getUserDispatch(params);
  };

  const onConfirmDelete = (id) => {
    deleteUsersDispatch(id)
  };

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_USER,
      "warning",
      "Yes",
      "No",
      () => {
        onConfirmDelete(id);
      },
      () => {}
    );
  };

  const totalPages =
    getUserDetails && getUserDetails.count
      ? Math.ceil(parseInt(getUserDetails && getUserDetails.count) / limit)
      : 1;

  return (
    <>
      <div className="bg-skyBlue">
        <div className="card-header bg-skyBlue py-10">
          <div className="card-body">
            <div className="row">
              <div className="col-lg-6 my-auto">
                <h2 className="mb-5">User Info</h2>
              </div>
              <div className="col-lg-6">
                <div className="d-flex justify-content-end">
                  <button
                    className={`btn btn-${
                      !showTable ? "darkRed" : "primary"
                    } m-2 fa-pull-right`}
                    onClick={() => {
                      setShowTable((val) => !val);
                    }}
                  >
                    {showTable ? "Add" : "Back"}
                  </button>
                </div>
              </div>
            </div>
            <div className="card-header bg-skyBlue py-0">
              <div className="card-body p-0">
                {showTable ? (
                  <>
                    <div className="w-100 d-flex justify-content-start mt-2">
                      <div className={`card`}>
                        <div className="card-body py-3">
                          <div className="d-flex justify-content-start col-md-4">
                            <div className="col-md-3 mt-1">
                              {getUserDetails && getUserDetails.length ? (
                                <span className="text-muted fw-bold d-flex fs-5 mt-2">
                                  Total: &nbsp;{" "}
                                  <span className="text-gray-700 fw-bolder text-hover-primary fs-5">
                                    {getUserDetails.length}
                                  </span>
                                </span>
                              ) : null}
                            </div>
                            <div className="col-md-7 d-flex">
                              <label className="col-form-label text-lg-start">
                                Record(s) per Page : &nbsp;{" "}
                              </label>
                              <div className="col-md-3">
                                <select
                                  className="form-select w-6rem"
                                  data-control="select"
                                  data-placeholder="Select an option"
                                  data-allow-clear="true"
                                  onChange={(e) => handleRecordPerPage(e)}
                                >
                                  <option value="25">25</option>
                                  <option value="50">50</option>
                                  <option value="75">75</option>
                                  <option value="100">100</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div className="table-responsive">
                            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                              <thead>
                                <tr className="fw-bold fs-6 text-gray-800">
                                  <th className="min-w-200px text-start">
                                    <div className="d-flex">
                                      <span>Action</span>
                                    </div>
                                  </th>
                                  <th className="min-w-200px text-start">
                                    <div className="d-flex">
                                      <span>First Name</span>
                                    </div>
                                  </th>
                                  <th className="min-w-200px text-start">
                                    <div className="d-flex">
                                      <span>Last Name</span>
                                    </div>
                                  </th>
                                  <th className="min-w-200px text-start">
                                    <div className="d-flex">
                                      <span>Email</span>
                                    </div>
                                  </th>
                                  <th className="min-w-200px text-start">
                                    <div className="d-flex">
                                      <span>Role</span>
                                    </div>
                                  </th>
                                  <th className="min-w-200px text-start">
                                    <div className="d-flex">
                                      <span>Status</span>
                                    </div>
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                {loadingGUD ? (
                                  <tr>
                                    <td colSpan="5" className="text-center">
                                      <div
                                        className="spinner-border text-primary m-5"
                                        role="status"
                                      />
                                    </td>
                                  </tr>
                                ) : getUser && getUser.result && getUser.result.length > 0 ? (
                                  getUser.result.map((item, i) => {
                                    return (
                                      <tr
                                        key={i}
                                        style={
                                          i === 0
                                            ? { borderColor: "black" }
                                            : { borderColor: "white" }
                                        }
                                      >
                                        <td className="min-width-150px">
                                          <div>
                                            <Link
                                              to={`/partners-user/update/${item._id}`}
                                              onClick={()=> {
                                                setLocalStorage(SAVE_CURRENT.PARTNER_ID, id)
                                              }}
                                              className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px "
                                            >
                                              <KTSVG
                                                path="/media/icons/duotune/art/art005.svg"
                                                className="svg-icon-3"
                                              />
                                            </Link>{" "}
                                            &nbsp;&nbsp;&nbsp;
                                            <button
                                              className="btn btn-icon btn-icon-danger btn-sm w-10px h-10px"
                                              onClick={() =>
                                                onDeleteItem(item._id)
                                              }
                                            >
                                              {/* {/ eslint-disable /} */}
                                              <KTSVG
                                                path="/media/icons/duotune/general/gen027.svg"
                                                className="svg-icon-3"
                                              />
                                              {/* {/ eslint-enable /} */}
                                            </button>
                                          </div>
                                        </td>
                                        <td>
                                          {item.firstName
                                            ? item.firstName
                                            : "--"}
                                        </td>
                                        <td>
                                          {item.lastName ? item.lastName : "--"}
                                        </td>
                                        <td>
                                        {item.email ? item.email : "--"}
                                        </td>
                                        <td>
                                          {item.roleId &&
                                          item.roleId.role
                                            ? item.roleId.role
                                            : "--"}
                                        </td>
                                        <td>
                                          <TooltipOnHover
                                            string={
                                              item.clientAddress
                                                ? item.clientAddress
                                                : "--"
                                            }
                                            sliceUpTo={
                                              SLICE_CHARACTERS.UP_TO_35
                                            }
                                          />
                                        </td>
                                        <td>
                                          {item.clientZip
                                            ? item.clientZip
                                            : "--"}
                                        </td>
                                      </tr>
                                    );
                                  })
                                ) : (
                                  <tr className="text-center py-3">
                                    <td colSpan="100%">No record(s) found</td>
                                  </tr>
                                )}
                              </tbody>
                            </table>
                            <div className="form-group row mb-4 mt-6">
                              <div className="col-lg-12 mb-4 align-items-end d-flex">
                                <div className="col-lg-12">
                                  <ReactPaginate
                                    nextLabel="Next >"
                                    onPageChange={handlePageClick}
                                    pageRangeDisplayed={3}
                                    marginPagesDisplayed={2}
                                    pageCount={totalPages}
                                    previousLabel="< Prev"
                                    pageClassName="page-item"
                                    pageLinkClassName="page-link"
                                    previousClassName="page-item"
                                    previousLinkClassName="page-link"
                                    nextClassName="page-item"
                                    nextLinkClassName="page-link"
                                    breakLabel="..."
                                    breakClassName="page-item"
                                    breakLinkClassName="page-link"
                                    containerClassName="pagination"
                                    activeClassName="active"
                                    renderOnZeroPageCount={null}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    {loadingGUD ? (
                      <div className="d-flex justify-content-center py-5">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </div>
                    ) : (
                      <>
                        <div className="form-group row mb-4">
                          <div className="col-lg-12 mb-3">
                            <div className="row">
                              <div className="col-lg-2" />
                              <div className="col-lg-2">
                                <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                  First Name:
                                </label>
                              </div>
                              <div className="col-lg-4">
                                <div className="input-group mb-5">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    <i className="bi bi-person-circle" />
                                  </span>
                                  <input
                                    name="firstName"
                                    type="text"
                                    className="form-control"
                                    id="basic-url"
                                    placeholder="First Name"
                                    aria-describedby="basic-addon3"
                                    onChange={(e) => handleChange(e)}
                                    value={formData.firstName || ""}
                                    autoComplete="off"
                                    maxLength={42}
                                    onKeyPress={(e) => {
                                      if (
                                        !REGEX.ALPHA_CHARS_SPACE.test(e.key)
                                      ) {
                                        e.preventDefault();
                                      }
                                    }}
                                  />
                                </div>
                                {errors && errors.firstName && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red;}"}</style>
                                    {errors.firstName}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group row mb-4">
                          <div className="col-lg-12 mb-3">
                            <div className="row">
                              <div className="col-lg-2" />
                              <div className="col-lg-2">
                                <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                  Last Name:
                                </label>
                              </div>
                              <div className="col-lg-4">
                                <div className="input-group mb-5">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    <i className="bi bi-person-circle" />
                                  </span>
                                  <input
                                    name="lastName"
                                    type="text"
                                    className="form-control"
                                    id="basic-url"
                                    placeholder="Last Name"
                                    aria-describedby="basic-addon3"
                                    onChange={(e) => handleChange(e)}
                                    value={formData.lastName || ""}
                                    autoComplete="off"
                                    maxLength={42}
                                    onKeyPress={(e) => {
                                      if (
                                        !REGEX.ALPHA_CHARS_SPACE.test(e.key)
                                      ) {
                                        e.preventDefault();
                                      }
                                    }}
                                  />
                                </div>
                                {errors && errors.lastName && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red;}"}</style>
                                    {errors.lastName}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group row mb-4">
                          <div className="col-lg-12 mb-3">
                            <div className="row">
                              <div className="col-lg-2" />
                              <div className="col-lg-2">
                                <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                  Email:
                                </label>
                              </div>
                              <div className="col-lg-4">
                                <div className="input-group mb-5">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    <i className="bi bi-envelope-fill" />
                                  </span>
                                  <input
                                    name="email"
                                    type="text"
                                    className="form-control"
                                    id="basic-url"
                                    placeholder="Email"
                                    aria-describedby="basic-addon3"
                                    onChange={(e) => handleChange(e)}
                                    value={formData.email || ""}
                                    autoComplete="off"
                                    maxLength={42}
                                    onKeyPress={(e) => {
                                      if (
                                        !REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(
                                          e.key
                                        )
                                      ) {
                                        e.preventDefault();
                                      }
                                    }}
                                  />
                                </div>
                                {errors && errors.email && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red;}"}</style>
                                    {errors.email}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group row mb-4">
                          <div className="col-lg-12 mb-3">
                            <div className="row">
                              <div className="col-lg-2" />
                              <div className="col-lg-2">
                                <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                  Password:
                                </label>
                              </div>
                              <div className="col-lg-4">
                                <div className="input-group mb-5">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    <i className="bi bi-lock-fill" />
                                  </span>
                                  <input
                                    name="password"
                                    type="password"
                                    className="form-control"
                                    id="basic-url"
                                    placeholder="Password"
                                    aria-describedby="basic-addon3"
                                    onChange={(e) => handleChange(e)}
                                    value={formData.password || ""}
                                    autoComplete="off"
                                    maxLength={42}
                                    onPaste={(e) => {
                                      e.preventDefault();
                                      return false;
                                    }}
                                    onCopy={(e) => {
                                      e.preventDefault();
                                      return false;
                                    }}
                                    onKeyPress={(e) => {
                                      if (
                                        !REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(
                                          e.key
                                        )
                                      ) {
                                        e.preventDefault();
                                      }
                                    }}
                                  />
                                </div>
                                {errors && errors.password && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red;}"}</style>
                                    {errors.password}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group row mb-4">
                          <div className="col-lg-12 mb-3">
                            <div className="row">
                              <div className="col-lg-2" />
                              <div className="col-lg-2">
                                <label className="font-size-xs font-weight-bold mb-2 required form-label mt-4">
                                  Role:
                                </label>
                              </div>
                              <div className="col-lg-4">
                                <div className="input-group mb-5">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    <i className="bi bi-envelope" />
                                  </span>
                                  <select
                                    name="roleId"
                                    className="form-select form-select-solid"
                                    data-control="select"
                                    data-placeholder="Select an option"
                                    data-allow-clear="true"
                                    onChange={(e) => handleChange(e)}
                                    value={formData.roleId || ""}
                                  >
                                    <option value="">Select Role...</option>
                                    {gerUserRole &&
                                      gerUserRole.length > 0 &&
                                      gerUserRole.map((item, i) => (
                                        <option key={i} value={item._id}>
                                          {item.role}
                                        </option>
                                      ))}
                                  </select>
                                </div>
                                {errors && errors.roleId && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.roleId}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group row mt-4">
                          <div className="col-lg-6" />
                          <div className="col-lg-6">
                            <div className="col-lg-12">
                              <button
                                className="btn btn-blue m-2 fa-pull-right"
                                onClick={() => {
                                  handleSubmit();
                                }}
                              >
                                {loadingAUD ? (
                                  <span
                                    className="spinner-border spinner-border-sm mx-3"
                                    role="status"
                                    aria-hidden="true"
                                  />
                                ) : (
                                  "Save"
                                )}
                              </button>
                            </div>
                          </div>
                        </div>
                      </>
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  const { userStore, userRoleStore, addUserStore, deleteUsersStore } = state;
  return {
    getUserDetails: userStore && userStore.userGetDetails ? userStore.userGetDetails : [],
    statusGUD: userStore && userStore.statusGUD ? userStore.statusGUD : "",
    messagesGUD: userStore && userStore.messagesGUD ? userStore.messagesGUD : "",
    loadingGUD: userStore && userStore.loadingGUD ? userStore.loadingGUD : false,
    getUser: userStore && userStore.getUser ? userStore.getUser : [],
    gerUserRole: userRoleStore && userRoleStore.gerUserRole ? userRoleStore.gerUserRole : [],
    loadingAUD: addUserStore && addUserStore.loadingAUD ? addUserStore.loadingAUD : false,
    statusAUD: addUserStore && addUserStore.statusAUD ? addUserStore.statusAUD : "",
    messageAUD: addUserStore && addUserStore.messageAUD ? addUserStore.messageAUD : "",
    loadingDeleteUsers: deleteUsersStore && deleteUsersStore.loadingDeleteUsers ? deleteUsersStore.loadingDeleteUsers : false,
    statusDeleteUsers: deleteUsersStore && deleteUsersStore.statusDeleteUsers ? deleteUsersStore.statusDeleteUsers : "",
    messageDeleteUsers: deleteUsersStore && deleteUsersStore.messageDeleteUsers ? deleteUsersStore.messageDeleteUsers : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getUserDispatch: (params) => dispatch(userActions.getUser(params)),
  editUsersDispatch: (id, params) => dispatch(editUserActions.editUser(id, params)),
  cleareditUserDispatch: () => dispatch(editUserActions.cleareditUser()),
  getUserRoleDispatch: (data) => dispatch(userRoleActions.getUserRole(data)),
  addUsersDispatch: (payload) =>
  dispatch(addUserActions.addUser(payload)),
  clearaddUserDispatch: () => dispatch(addUserActions.clearaddUser()),
  deleteUsersDispatch: (id) =>
  dispatch(deleteUsersActions.delete(id)),
  clearDeleteUserDispatch: () => dispatch(deleteUsersActions.clear())
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePartnersUser);