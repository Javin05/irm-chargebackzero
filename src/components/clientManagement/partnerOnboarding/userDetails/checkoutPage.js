import { useEffect } from "react";
import {
  STATUS_RESPONSE,
  SLICE_CHARACTERS,
  SET_STORAGE
} from "../../../../utils/constants";
import { useHistory } from "react-router-dom";
import { removeLocalStorage } from "../../../../utils/helper";
import { connect } from "react-redux";
import { addPartnerActions } from "../../../../store/actions";
import { warningAlert, confirmAlert } from '../../../../utils/alerts'
import { TooltipOnHover } from "../../../../theme/layout/components/tooltipOnHover/TooltipOnHover";

const Checkout = (props) => {
  const {
    addPartnerActionsDispatch,
    clearPartnerActionsDispatch,
    statusAddPartners,
    messageAddPartners,
    partnerDetails,
    goBack,
    loadingAddPartners
  } = props;
  const history = useHistory();
  const getPayload = {
    company: partnerDetails && partnerDetails.partner && partnerDetails.partner.companyName,
    partnerFirstName: partnerDetails && partnerDetails.partner && partnerDetails.partner.partnerFirstName,
    partnerLastName: partnerDetails && partnerDetails.partner && partnerDetails.partner.partnerLastName,
    partnerAddress: partnerDetails && partnerDetails.partner && partnerDetails.partner.address,
    partnerCity: partnerDetails && partnerDetails.partner && partnerDetails.partner.city,
    partnerState: partnerDetails && partnerDetails.partner && partnerDetails.partner.state,
    partnerZip: partnerDetails && partnerDetails.partner && partnerDetails.partner.zip,
    partnerCountry: partnerDetails && partnerDetails.partner && partnerDetails.partner.country,
    partnerEmail: partnerDetails && partnerDetails.partner && partnerDetails.partner.email,
    password: partnerDetails && partnerDetails.user && partnerDetails.user.password,
    partnerPhoneNumber: partnerDetails && partnerDetails.partner && partnerDetails.partner.phone,
    partnerNotes: partnerDetails && partnerDetails.partner && partnerDetails.partner.clientNotes,
    clientId: partnerDetails && partnerDetails.clientId && partnerDetails.clientId
  }

  const removeFormStorage = () => {
    removeLocalStorage(SET_STORAGE.PARTNER_DETAILS);
    removeLocalStorage(SET_STORAGE.PARTNER_USER_DETAILS);
  };

  const onConfirm = () => {
    removeFormStorage()
    history.push('/partner-management')
  }

  useEffect(() => {
    if (statusAddPartners === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        'Success',
        messageAddPartners,
        'success',
        'ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearPartnerActionsDispatch()
    } else if (statusAddPartners === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        messageAddPartners,
        '',
        'Ok'
      )
    }
    clearPartnerActionsDispatch()
  }, [statusAddPartners])

  const handleSave = () => {
    addPartnerActionsDispatch(getPayload);
    removeFormStorage()
  };

  return (
    <>
      <div className="card-header bg-skyBlue">
        <div className="card-body">
          <h2 className="mb-2">Summary</h2>
          <div className="card-header bg-skyBlue">
            <div className="card-body">
              <div className="row g-4">
                <div className="col-xl-4">
                  <div className="d-flex h-100 align-items-center">
                    <div className="w-100 rounded-3 bg-light bg-opacity-75 py-12 px-10">
                    <div className="mb-7">
                      <h2 className="text-gray-700 text-center mb-5">
                        Partner Details
                      </h2>
                    </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="w-80 mb-10">
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Partner Name: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px textstart">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.partnerFirstName ?
                                      `${partnerDetails.partner.partnerFirstName} ${partnerDetails.partner.partnerLastName}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Industry: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.industry ?
                                      `${partnerDetails.partner.industry}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Country: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.country ?
                                      `${partnerDetails.partner.country}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                State: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.state ?
                                      `${partnerDetails.partner.state}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Address: &nbsp; &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.address ?
                                      `${partnerDetails.partner.address}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_25}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Company Description: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.clientNotes ?
                                      `${partnerDetails.partner.clientNotes}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_25}
                                />
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="mt-14 mb-4">
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Email: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.email ?
                                      `${partnerDetails.partner.email}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Phone: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.phone ?
                                      `${partnerDetails.partner.phone}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                City: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.city ?
                                      `${partnerDetails.partner.city}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Zip Code: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                <TooltipOnHover
                                  string={
                                    partnerDetails &&
                                      partnerDetails.partner &&
                                      partnerDetails.partner.zip ?
                                      `${partnerDetails.partner.zip}`
                                      : "--"
                                  }
                                  sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                                />
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-4">
                  <div className="h-100 align-items-center">
                    <div className="w-100 h-100 rounded-3 bg-light bg-opacity-75 py-12 px-10">
                      <div className="mb-7">
                        <h2 className="text-gray-700 text-center mb-5">
                          Client Details:
                        </h2>
                      </div>
                      {
                        partnerDetails && partnerDetails.clientId && partnerDetails.clientId.map((item, i) => (
                          <div className="w-80 mb-10" key={i}>
                            <div className="d-flex align-items-center mb-5">
                              <span className="fw-bold fs-6 text-gray-800">
                                Client Details: &nbsp;
                              </span>
                              <span className="fw-bolder text-muted fs-6 w-120px text-start">
                                {item}
                              </span>
                            </div>
                          </div>
                        ))
                      }
                    </div>
                  </div>
                </div>
                <div className="col-xl-4">
                  <div className="h-100 align-items-center">
                    <div className="w-100 h-100 rounded-3 bg-light bg-opacity-75 py-12 px-10">
                      <div className="mb-7">
                        <h2 className="text-gray-700 text-center mb-5">
                          Login Info:
                        </h2>
                      </div>
                      <div className='w-80 mb-10'>
                        <div className='d-flex align-items-center mb-5'>
                          <span className="fw-bold fs-6 text-gray-800">
                            First Name: &nbsp;
                          </span>
                          <span className="fw-bolder text-muted fs-6 w-120px textstart">
                            <TooltipOnHover
                              string={
                                partnerDetails &&
                                  partnerDetails.user &&
                                  partnerDetails.user.firstName ?
                                  `${partnerDetails.user.firstName}`
                                  : "--"
                              }
                              sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                            />
                          </span>
                        </div>
                      </div>
                      <div className='w-80 mb-10'>
                        <div className='d-flex align-items-center mb-5'>
                          <span className='fw-bold fs-6 text-gray-800'>
                            Last Name: &nbsp;
                          </span>
                          <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                            <TooltipOnHover
                              string={
                                partnerDetails &&
                                  partnerDetails.user &&
                                  partnerDetails.user.lastName ?
                                  `${partnerDetails.user.lastName}`
                                  : "--"
                              }
                              sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                            />
                          </span>
                        </div>
                      </div>
                      <div className='w-80 mb-10'>
                        <div className='d-flex align-items-center mb-5'>
                          <span className='fw-bold fs-6 text-gray-800'>
                            Email : &nbsp;
                          </span>
                          <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                            <TooltipOnHover
                              string={
                                partnerDetails &&
                                  partnerDetails.user &&
                                  partnerDetails.user.email ?
                                  `${partnerDetails.user.email}`
                                  : "--"
                              }
                              sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                            />
                          </span>
                        </div>
                      </div>
                      <div className='w-80 mb-10'>
                        <div className='d-flex align-items-center mb-5'>
                          <span className='fw-bold fs-6 text-gray-800'>
                            Role : &nbsp;
                          </span>
                          <span className='fw-bolder text-muted fs-6 w-120px text-start'>
                            <TooltipOnHover
                              string={
                                partnerDetails &&
                                  partnerDetails.user &&
                                  partnerDetails.user.roleId ?
                                  `${partnerDetails.user.roleId}`
                                  : "--"
                              }
                              sliceUpTo={SLICE_CHARACTERS.UP_TO_15}
                            />
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="form-group row mb-2 mt-5">
            <div className="col-lg-6" />
            <div className="col-lg-6">
              <div className="col-lg-12">
                <button
                  className="btn btn-success m-2 fa-pull-right"
                  onClick={() => {
                    handleSave();
                  }}
                >
                  {loadingAddPartners ? (
                    <span
                      className="spinner-border spinner-border-sm mx-3"
                      role="status"
                      aria-hidden="true"
                    />
                  ) : (
                    "Create Partner"
                  )}
                </button>
                <button
                  className="btn btn-darkRed m-2 fa-pull-right"
                  onClick={() => {
                    goBack(2);
                  }}
                >
                  Back
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  loadingAddPartners: state && state.addPartnerstore && state.addPartnerstore.loadingAddPartners,
  statusAddPartners: state && state.addPartnerstore && state.addPartnerstore.statusAddPartners,
  messageAddPartners: state && state.addPartnerstore && state.addPartnerstore.messageAddPartners,
  addPartners: state && state.addPartnerstore && state.addPartnerstore.addPartners,
});

const mapDispatchToProps = (dispatch) => ({
  addPartnerActionsDispatch: (data) => dispatch(addPartnerActions.add(data)),
  clearPartnerActionsDispatch: () => dispatch(addPartnerActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);