import _ from 'lodash'

export const setPartnerCompanyDetails = (data) => {
  if (!_.isEmpty(data)) {
    return {
      partnerFirstName: data.partnerFirstName,
      partnerLastName: data.partnerLastName,
      industry: data.industry,
      companyName: data.company,
      country: data.partnerCountry,
      address: data.partnerAddress,
      city: data.partnerCity,
      state: data.partnerState,
      zip: data.partnerZip,
      email: data.partnerEmail,
      phone: data.partnerPhoneNumber,
      partnerNotes: data.partnerNotes
    }
  }
}

export const getPartnerDetailsPayload = (data) => {
  if (!_.isEmpty(data)) {
    return {
      partnerFirstName: data.partnerFirstName,
      partnerLastName: data.partnerLastName,
      industry: data.industry,
      companyName: data.companyName,
      country: data.partnerCountry,
      address: data.partnerAddress,
      city: data.partnerCity,
      state: data.partnerState,
      zip: data.partnerZip,
      email: data.partnerEmail,
      phone: data.partnerPhoneNumber,
      partnerNotes: data.partnerNotes
    }
  }
}

export const setPartnerUserDetailsPayload = (data) => {
  if (!_.isEmpty(data)) {
    return {
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      password: data.password,
      roleId: data.roleId && data.roleId._id,
    }
  }
}