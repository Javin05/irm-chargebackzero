import { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../../theme/helpers'
import color from '../../../utils/colors'
import _ from 'lodash'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import { REGEX, currencyOptions } from '../../../utils/constants'
import {
  CountryActions,
  StateActions,
  CityActions,
  clientActions,
  industryActions
} from '../../../store/actions'

function SearchList(props) {
  const {
    getCountryDispatch,
    getCountrys,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
    getClientDispatch,
    getIndustryDispatch,
    getIndustry,
    setSearchParams,
    activePageNumber,
    setActivePageNumber,
    limit
  } = props

  const [, setShow] = useState(false)
  const [countryOption, setCountryOption] = useState()
  const [selectedCountryOption, setSelectedCountryOption] = useState('')
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [selectedCurrencyOption, setSelectedCurrencyOption] = useState()
  const [selectedIndustryOption, setSelectedIndustryOption] = useState()
  const [industryOption, setIndustryOption] = useState()
  const [formData, setFormData] = useState({
    company: '',
    clientCountry: '',
    industry: '',
    customerId: '',
    clientCity: '',
    clientState: '',
    clientZip: '',
    clientEmail: '',
    clientPhoneNumber: '',
    leadSource: '',
    currency: ''
  })

  useEffect(() => {
    getCountryDispatch()
    getIndustryDispatch()
  }, [])
  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : ''
    })
  }

  const getDefaultIndustryOptions = (data, name) => {
    const defaultOptions = []
    if (!_.isEmpty(data)) {
      data.map((item) =>
        defaultOptions.push({
          label: `${item[name] ? item[name] : ''}`,
          value: item._id
        })
      )
      return defaultOptions
    }
  }

  const handleChangeCountry = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCountryOption(selectedOption)
      setFormData((values) => ({
        ...values,
        clientCountry: selectedOption.value,
        clientState: '',
        clientCity: ''
      }))
      setSelectedStateOption()
      setSelectedCityOption()
      getStateDispatch({ countryId: selectedOption.value })
    } else {
      setSelectedCountryOption()
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData((values) => ({
        ...values,
        clientCountry: '',
        clientState: '',
        cityclientCity: ''
      }))
    }
  }

  const handleChangeState = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedStateOption(selectedOption)
      setFormData((values) => ({
        ...values,
        clientState: selectedOption.value,
        clientCity: ''
      }))
      setSelectedCityOption()
      getCityDispatch({ stateId: selectedOption.value })
    } else {
      setSelectedStateOption()
      setSelectedCityOption()
      setFormData((values) => ({ ...values, clientState: '', clientCity: '' }))
    }
  }

  const handleChangeCity = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCityOption(selectedOption)
      setFormData((values) => ({ ...values, clientCity: selectedOption.value }))
    } else {
      setSelectedCityOption()
      setFormData((values) => ({ ...values, clientCity: '' }))
    }
  }

  const handleChangeIndustry = selectedOption => {
    if (selectedOption !== null) {
      setSelectedIndustryOption(selectedOption)
      setFormData(values => ({ ...values, industry: selectedOption.value }))
    } else {
      setSelectedIndustryOption()
      setFormData(values => ({ ...values, industry: '' }))
    }
  }

  const handleChangeCurrency = (selectedOption) => {
    if (selectedOption !== null) {
      setSelectedCurrencyOption(selectedOption)
      setFormData((values) => ({ ...values, currency: selectedOption.value }))
    } else {
      setSelectedCurrencyOption()
      setFormData((values) => ({ ...values, currency: '' }))
    }
  }

  useEffect(() => {
    const country = getDefaultOptions(getCountrys)
    setCountryOption(country)
  }, [getCountrys])

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
  }, [getStates])

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
  }, [getCitys])

  useEffect(() => {
    const data = getDefaultIndustryOptions(getIndustry, 'industryType')
    setIndustryOption(data)
  }, [getIndustry])

  const resetState = () => {
    setFormData({
      company: '',
      clientCountry: '',
      industry: '',
      customerId: '',
      clientCity: '',
      clientState: '',
      clientZip: '',
      clientEmail: '',
      clientPhoneNumber: '',
      leadSource: '',
      currency: ''
    })
    setSelectedCountryOption()
    setSelectedStateOption()
    setSelectedCityOption()
    setSelectedIndustryOption()
    setSelectedCurrencyOption()
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({
        label: rawData[item].name,
        value: rawData[item]._id
      })
    }
    return defaultOptions
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    if (name === 'amount' || name === 'ARN' || name === 'cardNumber' || name === 'transactionAmount' || name === 'creditCardNumber') {
      if (REGEX.NUMERIC.test(value)) {
        setFormData({ ...formData, [name]: value })
      } else {
        setFormData({ ...formData, [name]: '' })
      }
    } else {
      setFormData({ ...formData, [e.target.name]: e.target.value })
    }
  }

  const handleSearch = () => {
    const params = {
      limit: limit,
      page: 1
    }
    setActivePageNumber(1)
    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key] !== '') {
        params[key] = formData[key]
      }
    }
    const result = _.omit(params, ['limit', 'page']);
    setSearchParams(_.pickBy(result))
    setShow(false)
    getClientDispatch(params)
    resetState()
  }

  const handleReset = () => {
    resetState()
    setSearchParams()
    const params = {
      limit: limit,
      page: activePageNumber,
    }
    getClientDispatch(params)
  }

  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, ' ')
      setState((values) => ({ ...values, [name]: getData.trim() }))
    } else {
      setState((values) => ({ ...values, [name]: '' }))
    }
  }

  return (
    <>
      <div className='d-flex bd-highlight'>
        <button
          type='button'
          className='btn btn-sm btn-light-primary font-5vw pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          <span className="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
              <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor"></rect>
              <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
            </svg>
          </span>
          Search
        </button>
      </div>
      <div
        className='modal fade'
        id='searchModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1000px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Search</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => {
                  setShow(false)
                  resetState()
                }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header bg-lightBlue'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs  font-weight-bold mb-2 form-label'>
                          Company Name:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            autoComplete='off'
                            name='company'
                            type='text'
                            className='form-control'
                            placeholder='Company Name'
                            onChange={(e) => handleChange(e)}
                            value={formData.company || ''}
                            maxLength={42}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            onKeyPress={(e) => {
                              if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-2'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Customer ID (Any CRM):
                        </label>
                        <div className='col-lg-11'>
                          <input
                            autoComplete='off'
                            name='customerId'
                            type='text'
                            className='form-control'
                            placeholder='Customer ID'
                            onChange={(e) => handleChange(e)}
                            value={formData.customerId || ''}
                            maxLength={42}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            onKeyPress={(e) => {
                              if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Industry:
                        </label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='industry'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeIndustry}
                            options={industryOption}
                            value={selectedIndustryOption}
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Country:
                        </label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='clientCountry'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeCountry}
                            options={countryOption}
                            value={selectedCountryOption}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          State:
                        </label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='clientState'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeState}
                            options={stateOption}
                            value={selectedStateOption}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          City:
                        </label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='clientCity'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeCity}
                            options={cityOptions}
                            value={selectedCityOption}
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Zip Code:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            autoComplete='off'
                            name='clientZip'
                            type='text'
                            className='form-control'
                            placeholder='Zip'
                            onChange={(e) => handleChange(e)}
                            value={formData.clientZip || ''}
                            maxLength={6}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            onKeyPress={(e) => {
                              if (!/[0-9]/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Email:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            autoComplete='off'
                            name='clientEmail'
                            type='text'
                            className='form-control'
                            placeholder='Email'
                            onChange={(e) => handleChange(e)}
                            value={formData.clientEmail || ''}
                            maxLength={42}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            onKeyPress={(e) => {
                              if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-2'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Phone Number:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            autoComplete='off'
                            name='clientPhoneNumber'
                            type='text'
                            className='form-control'
                            placeholder='Phone Number'
                            onChange={(e) => handleChange(e)}
                            value={formData.clientPhoneNumber || ''}
                            maxLength={12}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            onKeyPress={(e) => {
                              if (!REGEX.NUMERIC_CHARS.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2  form-label'>
                          Lead Source:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            autoComplete='off'
                            name='leadSource'
                            type='text'
                            className='form-control'
                            placeholder='Lead Source'
                            onChange={(e) => handleChange(e)}
                            value={formData.leadSource || ''}
                            maxLength={42}
                            onBlur={e => handleTrimWhiteSpace(e, setFormData)}
                            onKeyPress={(e) => {
                              if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                        </div>
                      </div>
                      <div className='col-lg-4 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-2 form-label'>
                          Currency:
                        </label>
                        <div className='col-lg-11'>
                          <ReactSelect
                            styles={customStyles}
                            isMulti={false}
                            name='currency'
                            className='basic-single'
                            classNamePrefix='select'
                            handleChangeReactSelect={handleChangeCurrency}
                            options={currencyOptions}
                            value={selectedCurrencyOption}
                          />
                        </div>
                      </div>
                    </div>
                    <div className='form-group row mt-4'>
                      <div className='col-lg-6' />
                      <div className='col-lg-6'>
                        <div className='col-lg-12'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                            onClick={() => handleSearch()}
                            data-dismiss='modal'
                          >
                            Search
                          </button>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                            data-dismiss='modal'
                            onClick={() => handleReset()}
                          >
                            Reset
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div >
    </>
  )
}

const mapStateToProps = state => ({
  getCountrys: state && state.CountrylistStore && state.CountrylistStore.Countrylists,
  loading: state && state.CountrylistStore && state.CountrylistStore.loading,
  getStates: state && state.StatelistStore && state.StatelistStore.Statelists,
  loading: state && state.StatelistStore && state.StatelistStore.loading,
  getCitys: state && state.CitylistStore && state.CitylistStore.Citylists,
  loading: state && state.CitylistStore && state.CitylistStore.loading,
  getIndustry: state && state.industryStore && state.industryStore.getIndustry,
})

const mapDispatchToProps = dispatch => ({
  getCountryDispatch: () => dispatch(CountryActions.getCountrylist()),
  CountryActions: (data) => dispatch(CountryActions.getCountrylist(data)),
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  StateActions: (data) => dispatch(StateActions.getStatelist(data)),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  CityActions: (data) => dispatch(CityActions.getCitylist(data)),
  getClientDispatch: (data) => dispatch(clientActions.getClient(data)),
  getIndustryDispatch: (params) => dispatch(industryActions.getIndustry(params)),
  clearIndustryDispatch: () => dispatch(industryActions.clearIndustry()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)