import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../../theme/helpers'
import _, { values } from 'lodash'
import { getWebAnalysisActions, riskManagementActions, ExportListActions } from '../../../store/actions'
import './styles.css';
import Modal from 'react-bootstrap/Modal'
import { searchValidation } from './validation'
import clsx from 'clsx'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper';
import FindRole from '../Role'

function SearchList(props) {
  const {
    getRiskManagementlistDispatch,
    Value,
    setexportShow
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [buil, setBUild] = useState(false);
  const [dataValue, setDataValue] = useState({});
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [manualFormData, setManualFormData] = useState({
    website: '',
    tag: '',
    riskStatus: '',
    reportStatus: ''
  })

  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const handleSearch = () => {
    const errorMsg = searchValidation(manualFormData, setError)
    if (_.isEmpty(errorMsg)) {
      const params = {
        website: manualFormData.website,
        tag: manualFormData.tag,
        riskStatus: manualFormData.riskStatus,
        reportStatus: manualFormData.reportStatus
      }
      setexportShow(true)
      setShow(false)
      getRiskManagementlistDispatch(params)
      setDataValue(params)
      setLocalStorage('ExportHide', JSON.stringify(
        !_.isEmpty(params && params) ? params : ''
      ))
      setLocalStorage('TAG', JSON.stringify(
        params
      ))
      setLocalStorage('WEBSITSEARCH', JSON.stringify(
        params
      ))
    }
  }

  const clearPopup = () => {
    setShow(false)
  }

  const tagSearch = JSON.parse(getLocalStorage('WEBSITSEARCH'))

  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
      website: tagSearch.website,
      tag: tagSearch.tag,
      riskStatus: tagSearch.riskStatus,
      reportStatus: tagSearch.reportStatus
    }))
  }, [])

  useEffect(() => {
    if (Value === true) {
      setManualFormData(values => ({
        ...values,
        website: '',
        tag: '',
        riskStatus: '',
        reportStatus: ''
      }))
    } else {
      setBUild(true)
    }
  }, [Value])

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search & Export
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Search Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Tag :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Tag'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.tag && error.tag },
                    {
                      'is-valid': manualFormData.tag && !error.tag
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type='text'
                  name='tag'
                  autoComplete='off'
                  value={manualFormData.tag || ''}
                />
                {error && error.tag && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.tag}
                  </div>
                )}
              </div>
            </div>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Website :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  placeholder='Website'
                  className={clsx(
                    'form-control form-control-lg form-control-solid',
                    { 'is-invalid': manualFormData.website && error.website },
                    {
                      'is-valid': manualFormData.website && !error.website
                    }
                  )}
                  onChange={(e) => handleChanges(e)}
                  type='text'
                  name='website'
                  autoComplete='off'
                  value={manualFormData.website || ''}
                />
                {error && error.website && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error.website}
                  </div>
                )}
              </div>
            </div>
            <FindRole
              role={Role}
            >
              <div className="row mb-8">
                <div className='col-md-4'>
                  <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Status :
                  </label>
                </div>
                <div className='col-md-8'>
                  <select
                    name='riskStatus'
                    className='form-select form-select-solid'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleChanges(e)}
                    value={manualFormData.riskStatus || ''}
                  >
                    <option value=''>Select...</option>
                    <option value='APPROVED'>APPROVED</option>
                    <option value='REJECTED'>REJECTED</option>
                    <option value='MANUAL REVIEW'>MANUAL REVIEW</option>
                    <option value='PENDING'>PENDING</option>
                    <option value='PROCESSING'>PROCESSING</option>
                  </select>
                </div>
              </div>
            </FindRole>
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Report Status :
                </label>
              </div>
              <div className='col-md-8'>
                <select
                  name='reportStatus'
                  className='form-select form-select-solid'
                  data-control='select'
                  data-placeholder='Select an option'
                  data-allow-clear='true'
                  onChange={(e) => handleChanges(e)}
                  value={manualFormData.reportStatus || ''}
                >
                  <option value=''>Select...</option>
                  <option value='REJECTED'>REJECTED</option>
                  <option value='PENDING'>PENDING</option>
                  <option value='DATA PROCESSING'>DATA PROCESSING</option>
                  <option value='DATA CAPTURED'>DATA CAPTURED</option>
                  <option value='PREPARING REPORT'>PREPARING REPORT</option>
                  <option value='COMPLETED'>COMPLETED</option>
                  <option value='TAG PROCESSING'>TAG PROCESSING</option>
                  <option value='TAG CAPTURED'>TAG CAPTURED</option>
                  <option value='WAITING FOR REPORT'>WAITING FOR REPORT</option>
                </select>
              </div>
            </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  className='btn btn-light-primary m-1 mt-8 font-5vw '
                  onClick={handleSearch}>
                  Search
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    clinetListStore
  } = state
  return {
    getRiskManagementlist: state && state.riskManagementlistStore && state.riskManagementlistStore.getRiskManagementlist,
    loading: state && state.riskManagementlistStore && state.riskManagementlistStore.loading,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
  }
}

const mapDispatchToProps = dispatch => ({
  getWebAnalysisDispatch: (params) => dispatch(getWebAnalysisActions.getgetWebAnalysislist(params)),
  getRiskManagementlistDispatch: (params) => dispatch(riskManagementActions.getRiskManagementlist(params)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)