import { useState } from "react";
import ReactApexChart from 'react-apexcharts'

const DonutChart = (props) => {
    const {chartData} = props

    const [caseSummary, setCaseSummary] = useState({
        options: {
            chart: {
                toolbar: {
                    show: true
                }
            },
            colors : chartData?.colors,
            labels: chartData?.labels,            
            dataLabels: {
                enabled: true,
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false
                    }
                }
            }],
            legend: {
                position: "right",
                offsetY: 0,
                height: 200
            }
        },
        series: chartData?.series,
    })

    return (
        <ReactApexChart
            options={caseSummary.options}
            series={caseSummary.series}
            type="donut"
            width="100%"
            height="250px"
        />
    );
}

export default DonutChart;
