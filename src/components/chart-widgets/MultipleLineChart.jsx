import ApexCharts from "react-apexcharts";

const MultipleLineChart = (props) => {
    const { chartData } = props
    const chart = {
        series: chartData?.series,
        options: {
            colors : chartData?.colors,
            chart: {
                height: 350,
                type: 'line',
                zoom: {
                  enabled: false
                },
                toolbar: {
                    show: true,
                    tools: {
                        download: true,
                        selection: false,
                        zoom: false,
                        zoomin: false,
                        zoomout: false,
                        pan: false,
                        reset : false,
                    },
                },
            },
            stroke: {
                width: [3, 3, 3, 4, 4, 4],
                curve: 'straight',
            },
            legend: {
                tooltipHoverFormatter: function(val, opts) {
                  return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
                }
            },
            dataLabels: {
                enabled: true,
            },
            markers: {
                size: 0,
                hover: {
                  sizeOffset: 6
                }
            },
            labels: chartData?.labels,
        }
    };
    return (
        <ApexCharts
            options={chart.options}
            series={chart.series}
            type="line"
            height={240}
        />
    );
};

export default MultipleLineChart;