import React, { useState, useEffect } from "react";
import { GoogleMap, StreetViewPanorama } from "@react-google-maps/api";

function StreetMap(props) {
    const { mapData, type } = props
    const [viewDefault, setViewDefault] = useState("phone") // phone/personal/ip
    const mapContainerStyle = {
        height: "450px",
        width: "100%"
    }

    useEffect(() => {
        if (type === "BUSINESS" && mapData !== null) {
            let businessAddressLocation ={
                lat:parseFloat(mapData && mapData.businessAddressLocation?.lat),
                lng:parseFloat(mapData && mapData.businessAddressLocation?.long)
            }
            setViewDefault(businessAddressLocation)
          }else if(mapData !== null && mapData !== undefined){
            let individualAddressLocation ={
              lat:parseFloat(mapData && mapData.individualAddressLocation ? mapData.individualAddressLocation.lat : mapData.lattitude),
              lng:parseFloat(mapData && mapData.individualAddressLocation? mapData.individualAddressLocation.long : mapData.longitude   )
          }
          setViewDefault(individualAddressLocation)
          }
    }, [mapData])

    const viewDefaults = { lat: 37.7749, lng: -122.4194 }
    return (
        <GoogleMap
            id="googlemap-streetview"
            mapContainerStyle={mapContainerStyle}
            zoom={17}
            // center={viewDefaults}
            // onLoad={() => console.log("Loaded GoogleMap")}
            >
                <StreetViewPanorama
                    position={viewDefault}
                    enableCloseButton={false}
                    linksControl={false}
                    addressControl={true}
                    visible={true}
                    motionTracking={true}
                    motionTrackingControl={true}
                    // onLoad={() => console.log("Loaded StreetViewPanorama")}
                />
        </GoogleMap>
    );
}

export default StreetMap;