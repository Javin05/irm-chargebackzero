import React, { useEffect, useState } from 'react';

const Data = [
    {
      "MCC": 742,
      "Description": "Veterinary Services",
      "mccDescription": "Merchants classified with this MCC are licensed practitioners of veterinary medicine, dentistry, or surgery. This MCC includes medical practitioners of pets (e.g., dogs, cats), livestock (e.g., cattle, horses, sheep, hogs, goats, poultry), and large or exotic animals.",
      "mccIncluded": "Animal Doctors, Hospitals Clinics � Pet\nHospitals � Pet Pet Clinics\nServices � Veterinary"
    },
    {
      "MCC": 763,
      "Description": "Agricultural Co-operatives",
      "mccDescription": "Merchants classified with this MCC provide farm management services or engage in farming operations. Also mccIncluded are associations and cooperatives that provide assistance and services to farmers in farm management. These services include, but are not limited to financial assistance, management or complete maintenance of crops, soil preparation, planting and cultivating, aerial dusting and spraying, disease and insect control, weed control, and harvesting crops.",
      "mccIncluded": "Farm Management Services\nOrchards Vineyards"
    },
    {
      "MCC": 780,
      "Description": "Landscaping and Horticultural Services",
      "mccDescription": "Merchants classified with this MCC provide landscape planning and design services. This MCC also  includes Merchants that offer a variety of lawn and garden services (e.g., planting, fertilizing, mowing, mulching, seeding, spraying, sod laying).",
      "mccIncluded": "Gardening Services Horticulture Services Services � Gardening Services � Horticulture Services � Landscaping"
    },
    {
      "MCC": 1520,
      "Description": "General Contractors � Residential and Commercial",
      "mccDescription": "Merchants classified with this MCC are general contractors primarily engaged in construction of residential and/or commercial buildings. This MCC includes contractors that perform new construction as well as remodeling, repair, additions, or alterations.",
      "mccIncluded": "Building Contractors � Residential, Commercial\nConstruction Companies\nContractors � General Contractors � Residential, Commercial"
    },
    {
      "MCC": 1711,
      "Description": "Heating, Plumbing, and Air Conditioning Contractors",
      "mccDescription": "Merchants classified with this MCC are contractors that provide heating, plumbing and air conditioning services. This MCC includes contractors that perform air system balancing and testing, drainage system installation, furnace repair, irrigation system installation, refrigeration and freezer work, sewer hookups and connections, solar heating, sprinkler system installation and water pump installation and servicing.",
      "mccIncluded": "Air Conditioning Contractors Contractors � Air\nConditioning\nContractors � Heating \nContractors � Plumbing Heating Contractors Plumbing Contractors"
    },
    {
      "MCC": 1731,
      "Description": "Electrical Contractors",
      "mccDescription": "Merchants classified with this MCC are contractors that provide electrical work such as fire alarm installation, sound equipment installation, telecommunications equipment installation, cable/internet installation, and telephone and telephone equipment installation.",
      "mccIncluded": "Contractors � Electrical"
    },
    {
      "MCC": 1740,
      "Description": "Masonry, Stonework, Tile Setting, Plastering and Insulation Contractors",
      "mccDescription": "Merchants classified with this MCC are contractors that perform masonry work, stone setting and other stone work, tile setting, plain and ornamental plastering, and insulation. This MCC also includes contractors that perform bricklaying, ceramic and marble work, mosaic work, acoustical improvements, and drywall construction.",
      "mccIncluded": "Contractors � Insulation Contractors � Masonry Contractors � Plastering Contractors � Stonework Contractors � Tile Setting Insulation Contractors Plastering Contractors Stonework Contractors Tile Setting Contractors"
    },
    {
      "MCC": 1750,
      "Description": "Carpentry Contractors",
      "mccDescription": "Merchants classified with this MCC are contractors that perform carpentry work. This MCC includes contractors that do cabinet work at the construction site, framing, trim and finish work and window and door installation.",
      "mccIncluded": "Contractors � Carpentry"
    },
    {
      "MCC": 1761,
      "Description": "Roofing, Siding, and Sheet Metal Work Contractors",
      "mccDescription": "Merchants classified with this MCC are contractors that install roofing and/or siding and do sheet metal work. This MCC includes contractors that perform architectural sheet metal work, ceilings and skylight installation, duct and gutter installation, and roof spraying, painting or coating.",
      "mccIncluded": "Contractors � Roofing\nContractors � Sheet Metal Work\nContractors � Siding\nSheet Metal Work Contractors\nSiding Contractors"
    },
    {
      "MCC": 1771,
      "Description": "Concrete Work Contractors",
      "mccDescription": "Merchants classified with this MCC are contractors that perform concrete, cement or asphalt work. This MCC includes contractors that construct private driveways and walks of all materials. Also included in this category are contractors that pour concrete for foundations and perform grouting work and concrete patio and sidewalk construction.",
      "mccIncluded": "Asphalt Contractors Cement Contractors\nContractors � Concrete Work"
    },
    {
      "MCC": 1799,
      "Description": "Special Trade Contractors (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are special trade contractors that perform construction work not covered with a more specific MCC. This MCC includes contractors that perform jobs such as awning installation, bathtub refinishing, fence construction,  fire escape installation, house moving, home window replacement, garage door installation, ornamental metal work, swimming pool construction, glasswork, well drilling, wallpaper services, and waterproofing.",
      "mccIncluded": "Contractors � Decorating Contractors � Demolition Services\nContractors � Glasswork\nContractors � Painting, Home and Building\nContractors � Paper Hanging\nContractors � Special Trade Contractors Contractors � Welding\nContractors � Well Drilling\nDecorating Contractors Decorators � Interior Demolition Services Garage Door Installation Glasswork Contractors Interior Decorators Window Replacement\nMiscellaneous Special Contractors\nPainting Contractors � Home, Building\nPaper Hanging Contractors\nServices � Demolition Wallpaper Hangers Welding Contractors Well Drilling Contractors"
    },
    {
      "MCC": 2741,
      "Description": "Miscellaneous Publishing and Printing",
      "mccDescription": "Merchants classified with this MCC engage in business-to-business or wholesale printing, publishing and/or bookbinding activity. This includes Merchants that print or publish maps and atlases, business newsletters, directories, sheet music, paper patterns, technical manuals and papers, telephone directories and yearbooks.",
      "mccIncluded": "Bookbinding Services Printing Services Publishing Services Services � Bookbinding\nServices � Publishing, Printing"
    },
    {
      "MCC": 2791,
      "Description": "Typesetting, Plate Making and Related Services",
      "mccDescription": "Merchants classified with this MCC perform business- to-business or wholesale typesetting for the printing trade and those that make plates for printing purposes. These Merchants are involved in advertisement typesetting, phototypesetting, computer-controlled typesetting, and color separations. This MCC also includes Merchants that make positives and negatives from which offset lithographic plates are made.  These establishments do not print from the plates they make but prepare them only for use by other Merchants. This MCC also includes Merchants that provide engraving or embossing services for printing purposes, such as engraving on wood, rubber, copper, or steel, or photoengraving.",
      "mccIncluded": "Plate Making Services Services � Plate Making Services � Typesetting"
    },
    {
      "MCC": 2842,
      "Description": "Specialty Cleaning, Polishing and Sanitation Preparations",
      "mccDescription": "Merchants classified with this MCC engage in the business-to-business or wholesale manufacture of polishes for wood, metal, and other materials as well as cleaning solutions, industrial and household disinfectants, and other sanitation preparations. Also mccIncluded are Merchants that sell non- personal deodorants, waxes and dressings for fabricated leather and other materials, wax removers, dry cleaning fluid and chemicals, rust and stain removers, wallpaper cleaners, and window cleaning solutions.",
      "mccIncluded": "Cleaning Preparations Materials � Cleaning, Polishing, Sanitation\nPolishing Preparations Sanitation Preparations"
    },
    {
      "MCC": 4011,
      "Description": "Railroads",
      "mccDescription": "Merchants classified with this MCC are railroads engaged in freight transport operations.",
      "mccIncluded": "Freight � Rail, Train Transportation\nTrains � Freight Transportation Transportation � Railroads,\nFreight"
    },
    {
      "MCC": 4111,
      "Description": "Local and Suburban Commuter Passenger Transportation, Including Ferries",
      "mccDescription": "Merchants classified with this MCC provide local and suburban mass passenger transportation over regular routes and on regular schedules. Included in this category is railway commuter transportation. Not included in this category are taxicabs, limousines, and bus lines.",
      "mccIncluded": "Commuter Transportation � Ferries\nCommuter Transportation � Railways\nFerries\nTransportation � Commuter, Local, Suburban Commuter Passenger Transportation\nTransportation � Commuter, Railways\nTransportation � Ferries"
    },
    {
      "MCC": 4112,
      "Description": "Passenger Railways",
      "mccDescription": "Merchants classified with this MCC are railroads that primarily provide long distance passenger transportation.  This may or may not include overnight accommodations on the train during long distance travel.",
      "mccIncluded": "Railways � Passenger Trains � Passenger\nTransportation � Passenger Railways"
    },
    {
      "MCC": 4119,
      "Description": "Ambulance Services",
      "mccDescription": "Merchants classified with this MCC provide emergency vehicle transportation with trained personnel. These Merchants may or may not provide emergency medical treatment enroute to a hospital or medical provider.",
      "mccIncluded": "Air Ambulance Services Emergency Vehicle Services\nServices � Air Ambulance Services � Ambulance\nServices � Emergency Vehicles\nTransportation � Ambulance Services"
    },
    {
      "MCC": 4121,
      "Description": "Taxicabs and Limousines",
      "mccDescription": "Merchants classified with this MCC provide transportation services by automobile and do not operate on a regular schedule or established route. This MCC includes ride-share Merchants.",
      "mccIncluded": "Cabs, Taxicabs, Limousines Limousines\nServices � Taxicabs, Limousines\nTransportation � Taxicabs, Limousines"
    },
    {
      "MCC": 4131,
      "Description": "Bus Lines",
      "mccDescription": "Merchants classified with this MCC provide passenger bus transportation services on a regular schedule over predetermined routes. Not included are charter and tour bus operators.",
      "mccIncluded": "Coach Lines Transportation � Bus Lines"
    },
    {
      "MCC": 4214,
      "Description": "Motor Freight Carriers and Trucking � Local and Long Distance, Moving and Storage Companies, and Local Delivery Services",
      "mccDescription": "Merchants classified with this MCC provide local or long-distance trucking services and may or may not also provide storage services.",
      "mccIncluded": "Delivery Services � Local Freight Carriers, Trucking and Storage Local Delivery Service Long Distance Trucking Services\nMotor Freight Carriers � Local, Long Distance Trucking\nMoving Companies Moving, Storage Companies\nServices � Delivery, Local Services � Freight Carriers, Trucking and Storage\nServices � Trucking, Local and Long Distance Storage, Moving Companies � Local, Long Distance\nTrucking � Local, Long Distance"
    },
    {
      "MCC": 4215,
      "Description": "Courier Services � Air and Ground, and Freight Forwarders",
      "mccDescription": "Merchants classified with this MCC deliver individually addressed letters, parcels, and packages, but excludes the Postal Services � Government Only (MCC 9402).",
      "mccIncluded": "Air or Ground Courier Services\nFreight Forwarders, Courier Services\nGround or Air Courier Services\nServices � Courier, Air or Ground\nServices � Freight Forwarders, Courier Services"
    },
    {
      "MCC": 4225,
      "Description": "Public Warehousing and Storage � Farm Products, Refrigerated Goods, Household Goods, and Storage",
      "mccDescription": "Merchants classified with this MCC provide storage and warehousing for farm products, refrigerated storage for perishable products, and storage for household goods and furniture.",
      "mccIncluded": "Warehousing, Storage � Public Warehousing"
    },
    {
      "MCC": 4411,
      "Description": "Steamship and Cruise Lines",
      "mccDescription": "Merchants classified with this MCC provide passenger transportation on the open seas or inland waters for the purpose of vacation or pleasure. These Merchants typically offer food, entertainment, and cabin accommodations included in the fare and operate pre-defined and advertised routes.",
      "mccIncluded": "Ships � Cruise Lines"
    },
    {
      "MCC": 4457,
      "Description": "Boat Rentals and Leasing",
      "mccDescription": "Merchants classified with this MCC primarily provide boat rental and leasing services for fishing boats, non- crew houseboats, sail boats, power boats, jet skis, and yachts.",
      "mccIncluded": "Jet Skis � Rental, Leases\nPowerboats � Rental, Leases\nRentals � Boats Rentals � Jet Skis\nRentals � Yachts, Non-Crew\nSailboats � Rentals, Leases Yacht Rentals � Non-Crew"
    },
    {
      "MCC": 4468,
      "Description": "Marinas, Marine Service, and Supplies",
      "mccDescription": "Merchants classified with this MCC operate marinas and may rent boat slips and store boats.  Other services may include boat cleaning, incidental boat repair, and the retail sale of marine supplies.\nMerchants located at a marina that sell fuel for consumer use in boats that do not have a separate Merchant agreement from the marina are included in this category.",
      "mccIncluded": "Harbors\nServices � Marine, Marina\nSupplies � Marinas, Marine Service, Supplies\nYacht Harbors"
    },
    {
      "MCC": 4511,
      "Description": "Airlines and Air Carriers (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are airlines and air carriers that do not have their own specific MCC.",
      "mccIncluded": ""
    },
    {
      "MCC": 4582,
      "Description": "Airports, Flying Fields, and Airport Terminals",
      "mccDescription": "Merchants classified with this MCC operate and maintain airports and flying fields. This includes Merchants that provide aircraft cleaning and janitorial services, aircraft servicing and repair, aircraft storage at airports, and airport hangar rental. Merchants located within the airport terminal that sell food, newspapers, and/or gift or souvenir items must be classified with the appropriate MCC for that business.",
      "mccIncluded": "Flying Fields Terminals � Airports"
    },
    {
      "MCC": 4722,
      "Description": "Travel Agencies and Tour Operators",
      "mccDescription": "Merchants classified with this MCC provide travel information and booking services. They act as agents on behalf of travelers in booking and ticketing air, land, or sea transportation and/or accommodation.\nThey also arrange and assemble tours for sale through a travel agent or directly to the consumer. Bus charters and tour bus operators are mccIncluded.",
      "mccIncluded": "Charter Buses\nPackage Tour Operators Tour Buses\nTour Operators Travel Packages Travel Wholesalers"
    },
    {
      "MCC": 4723,
      "Description": "Package Tour Operators � Germany Only",
      "mccDescription": "Merchants classified with this MCC are package tour operators in Germany.",
      "mccIncluded": ""
    },
    {
      "MCC": 4784,
      "Description": "Tolls and Bridge Fees",
      "mccDescription": "Merchants classified with this MCC collect fees associated with toll roads, highways, and bridges.",
      "mccIncluded": "Bridge, Toll Fees Fees � Bridge, Toll"
    },
    {
      "MCC": 4789,
      "Description": "Transportation Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide passenger transportation services that are not classified with a more specific MCC. This MCC includes Merchants that provide transportation via horse-drawn cabs and carriages, bicycle-taxis, aerial tramways, airport shuttles, and cable cars. Ferry service, bus transportation, cruise lines, passenger railways, and taxi and limousine service Merchants are not included in this category.",
      "mccIncluded": "Airport Shuttle Transportation\nMiscellaneous Transport Services\nServices � Transportation Shuttle Transportation"
    },
    {
      "MCC": 4812,
      "Description": "Telecommunication Equipment and Telephone Sales",
      "mccDescription": "Merchants classified with this MCC sell telecommunications equipment such as telephones, fax machines, pagers, mobile phones, and other equipment related to telecommunications.",
      "mccIncluded": "Cellular Telephone Equipment\nEquipment � Telecommunications Pagers, Paging Equipment\nTelephone Equipment"
    },
    {
      "MCC": 4814,
      "Description": "Telecommunication Services, including Local and Long-Distance Calls, Credit Card Calls, Calls Through Use of Magnetic- Stripe-Reading Telephones, and Fax Services",
      "mccDescription": "Merchants classified with this MCC provide telecommunications services including local and long- distance telephone calls, calls placed through the use of magnetic stripe-reading telephones, and fax services. Merchants that provide periodic (e.g., monthly) billing of telephone calls should be classified with this MCC.",
      "mccIncluded": "Cellular Telephone Service Faxing Services\nLocal Telecommunication Service\nLong Distance Telecommunication Service\nPrepaid Calling Cards � Telecommunication Service"
    },
    {
      "MCC": 4816,
      "Description": "Computer Network/Information Services",
      "mccDescription": "Merchants classified with this MCC provide internet access. This MCC includes all Merchants that provide computer network and information services and internet service providers.",
      "mccIncluded": "Electronic Bulletin Board Access\nInformation Services � via Computer Networks\nInternet Payment Facilitators\nWebsite Management/ Maintenance"
    },
    {
      "MCC": 4821,
      "Description": "Telegraph Services",
      "mccDescription": "Merchants classified with this MCC that provide telegraph and other nonverbal message communications services such as telegrams.",
      "mccIncluded": "Cablegrams\nServices � Cablegrams Services � Telegraph"
    },
    {
      "MCC": 4829,
      "Description": "Money Transfer",
      "mccDescription": "Merchants classified with this MCC allow customers to transfer funds via an electronic funds transfer / wire transfer / remittance to a named entity (both card- present and card-absent locations including on the premises of the Merchant and third-party agents such as casinos, truck stops, or check-cashing storefronts) or via an original credit transaction (e.g., via Visa Direct or Person to Person [P2P] payments).\nA Merchant that transfers non-fiat currency such as cryptocurrency must include all required data in the authorization request and clearing record.",
      "mccIncluded": "Wire Transfer"
    },
    {
      "MCC": 4899,
      "Description": "Cable, Satellite and Other Pay Television/Radio/Streaming Services",
      "mccDescription": "Merchants classified with this MCC provide the connection and ongoing delivery of television/radio/streaming content on a subscription or fee basis.",
      "mccIncluded": "Pay Radio Services Pay Television Services Cable\nSatellite Services Services � Cable and Other Pay Television\nServices\nServices � Pay Radio and Satellite Services"
    },
    {
      "MCC": 4900,
      "Description": "Utilities � Electric, Gas, Water, and Sanitary",
      "mccDescription": "Merchants classified with this MCC provide the ongoing generation, transmission, and/or distribution of electric or gas power or other utility services. This MCC also includes Merchants that provide water supply system services and Merchants primarily engaged in the collection and disposal of refuse.",
      "mccIncluded": "Electric Utilities Garbage Collectors Gas Utilities\nPublic Utilities Sanitary Utilities\nServices � Electric Utilities Services � Gas Utilities\nServices � Sanitary Utilities\nServices � Waste Management\nServices � Water Utilities Water Utilities"
    },
    {
      "MCC": 5013,
      "Description": "Motor Vehicle Supplies and New Parts",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of motor vehicle accessories, tools, equipment and new parts, including testing equipment, service station pumps and other equipment, glass, batteries, and engines.",
      "mccIncluded": "Automobile � Motor Vehicle Supplies, New Parts\nParts � Motor Vehicle Supplies, New Parts\nSupplies � Motor Vehicle Supplies, New Parts\nVehicle Supplies, New Parts"
    },
    {
      "MCC": 5021,
      "Description": "Office and Commercial Furniture",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale manufacturers and/or distributors of office and commercial furniture.",
      "mccIncluded": "Commercial Furniture Furniture � Office, Commercial"
    },
    {
      "MCC": 5039,
      "Description": "Construction Materials (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of construction materials and parts that are not classified with a more specific MCC.",
      "mccIncluded": "Materials � Construction Supplies � Construction Materials"
    },
    {
      "MCC": 5044,
      "Description": "Photographic, Photocopy, Microfilm Equipment and Supplies",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of office and photographic equipment such as film, cash registers, photocopy machines, microfilm machines, vaults and safes, typewriters, fax machines, adding machines, and mailing machines.",
      "mccIncluded": "Copy Services � Photographic, Photocopy, Microfilm Supplies\nEquipment � Photographic, Photocopy, Microfilm Equipment, Supplies\nMicrofilm Supplies Photocopy Supplies\nSupplies � Photographic, Photocopy, Microfilm Equipment, Supplies"
    },
    {
      "MCC": 5045,
      "Description": "Computers and Computer Peripheral Equipment and Software",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of computer hardware, software and related equipment.",
      "mccIncluded": "Equipment � Computers, Computer Peripheral Equipment, Software\nHardware � Computer Software � Computer\nSupplies � Computers, Computer Peripheral Equipment, Software"
    },
    {
      "MCC": 5046,
      "Description": "Commercial Equipment (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of commercial machines and equipment that are not classified with a more specific MCC.",
      "mccIncluded": "Appliance Wholesaler Commercial Cooking, Food Service Equipment\nEquipment � Commercial Food Service Equipment � Commercial Cooking Equipment\nRestaurant Equipment � Commercial Cooking, Food Service Equipment\nSupplies � Commercial Equipment"
    },
    {
      "MCC": 5047,
      "Description": "Medical, Dental, Ophthalmic and Hospital Equipment and Supplies",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of laboratory, surgical, dental, or orthopedic equipment, patient monitoring equipment, wheelchairs, and other medical instruments, industrial safety devices, hospital equipment and hospital furniture, such as beds.",
      "mccIncluded": "Dental Equipment, Supplies Equipment � Medical, Dental, Ophthalmic,\nOrthopedic, Hospital\nEquipment, Supplies Hospital Equipment, Supplies\nOrthodontia Supplies Safety Equipment Supplies � Dental Supplies � Hospital Supplies � Medical Supplies � Orthopedic X-Ray Equipment"
    },
    {
      "MCC": 5051,
      "Description": "Metal Service Centers and Offices",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale marketers of semi-finished metal products, excluding precious metals.  These Merchants may sell metal piping and tubing, wire screening and bale ties, nails, ingots, aluminum bars, rails and accessories, metal or galvanized sheets, strip metal, iron rods, and wire rope or cable.",
      "mccIncluded": ""
    },
    {
      "MCC": 5065,
      "Description": "Electrical Parts and Equipment",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of electrical parts and communications equipment, such as electronic coils, capacitors, electronic condensers, diodes, semiconductor devices, public address equipment, and electric signs.",
      "mccIncluded": "Communications Equipment\nEquipment � Electrical Parts, Equipment Parts � Electrical, Equipment\nSupplies � Electrical Parts, Equipment"
    },
    {
      "MCC": 5072,
      "Description": "Hardware, Equipment and Supplies",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of general hardware and cutlery such as bolts, nuts, rivets, screws, fasteners, hand tools, locks, washers, tacks, staples, nails, hand saws, saw blades, and power hand tools.",
      "mccIncluded": "Equipment � Hardware, Equipment, Supplies\nHand Tools\nSupplies Hardware, Equipment, Supplies"
    },
    {
      "MCC": 5074,
      "Description": "Plumbing and Heating Equipment and Supplies",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of hydraulic plumbing and heating equipment and supplies. This MCC does not include wholesalers of commercial refrigeration equipment or supplies or appliances such as electric or gas ranges and commercial ovens.",
      "mccIncluded": "Equipment � Heating Equipment, Supplies Equipment � Plumbing\nEquipment, Supplies\nEquipment � Water Conditioning, Purification, Softening\nHeating Equipment, Supplies\nSupplies � Heating Equipment, Supplies Supplies � Plumbing\nEquipment, Supplies"
    },
    {
      "MCC": 5085,
      "Description": "Industrial Supplies (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of industrial supplies that are not classified with a more specific MCC.",
      "mccIncluded": "Supplies � Industrial"
    },
    {
      "MCC": 5094,
      "Description": "Precious Stones and Metals, Watches and Jewelry",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of jewelry, precious stones and metals. This MCC also includes wholesalers of costume jewelry, watches and watch parts, clocks, silverware, diamonds and gemstones, precious metals, pearls, and trophies.",
      "mccIncluded": "Gemstones, Precious Stones, Metals, Watches, Jewelry\nJewelry � Fine Watches"
    },
    {
      "MCC": 5099,
      "Description": "Durable Goods (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of durable goods that are not classified with a more specific MCC.",
      "mccIncluded": "Fire Extinguishers Firearms, Ammunition Gas Lighting Fixtures\nGoods � Durable Gravestones\nLuggage (Business to Business)\nMonuments Musical Instruments\nRough Timber Products Signs"
    },
    {
      "MCC": 5111,
      "Description": "Stationery, Office Supplies, Printing and Writing Paper",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of stationery and office supplies and printing and writing paper.",
      "mccIncluded": "Business Supplies � Stationery, Office, Printing, Writing Supplies\nOffice Supplies\nPaper � Writing, Printing, Stationery, Office Supplies\nPrinting Supplies\nSupplies � Stationery, Office Supplies, Printing, and Writing Paper"
    },
    {
      "MCC": 5122,
      "Description": "Drugs, Drug Proprietaries, and Druggist Sundries",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of prescription drugs, proprietary drugs, vitamins, druggists� sundries and toiletries, antiseptics, bandages, pharmaceuticals, and biological or related products.",
      "mccIncluded": "Cosmetic Distributors\nDistributors � Drugs, Druggists� Sundries, Toiletries\nDruggist Supplies Perfume Distributors Pharmaceuticals\nSupplies � Drugs, Drug Proprietaries, Druggist Sundries, Vitamins"
    },
    {
      "MCC": 5131,
      "Description": "Piece Goods, Notions, and Other Dry Goods",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of piece goods, dry goods, and notions/haberdashery.",
      "mccIncluded": "Fabric Goods Hair Accessories Notions\nSewing Accessories Supplies � Millinery Textiles, Textile Binding"
    },
    {
      "MCC": 5137,
      "Description": "Men�s, Women�s, and Children�s Uniforms and Commercial Clothing",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of commercial work clothing and uniforms. This MCC includes wholesale distributors of graduation caps and gowns, athletic and recreational uniforms, and school uniforms, but not shoes.",
      "mccIncluded": "Children�s Uniforms\nCommercial Clothing � Men�s, Women�s, Children�s\nMen�s Uniforms Professional Uniforms\nUniforms � Men�s, Women�s, Children�s\nWomen�s Uniforms"
    },
    {
      "MCC": 5139,
      "Description": "Commercial Footwear",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of commercial shoes and boots including athletic footwear.",
      "mccIncluded": "Footwear � Commercial Shoes, Shoe Accessories\n� Commercial"
    },
    {
      "MCC": 5169,
      "Description": "Chemicals and Allied Products (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of chemicals and allied products that are not classified with a more specific MCC.",
      "mccIncluded": "Detergents\nSupplies � Chemicals, Allied Products"
    },
    {
      "MCC": 5172,
      "Description": "Petroleum and Petroleum Products",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of petroleum and petroleum products, butane gas, crude oil, fuel oil, gasoline, kerosene, lubricating oils and greases, aircraft fueling services, and naphtha.",
      "mccIncluded": "Fuel Products, Services Services � Fueling"
    },
    {
      "MCC": 5192,
      "Description": "Books, Periodicals and Newspapers",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of books, periodicals, magazines, journals, and newspapers, including newspaper agencies.",
      "mccIncluded": "Journals Magazines\nNewspapers, Newspaper Agencies\nPeriodicals"
    },
    {
      "MCC": 5193,
      "Description": "Florists Supplies, Nursery Stock and Flowers",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of flowers, nursery stock and florists� supplies, fresh and artificial flowers, and potted plants.",
      "mccIncluded": "Gardening Supplies Nursery Stock Plants\nSupplies � Florist Supplies, Nursery Stock, Flowers"
    },
    {
      "MCC": 5198,
      "Description": "Paints, Varnishes and Supplies",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of paints, varnishes, wallpaper, and supplies, including colors and pigments, enamels, lacquers, paint brushes, paint pans, sandpaper, drop cloths, shellac, rollers, and sprayers.",
      "mccIncluded": "Supplies � Paints, Varnishes, Supplies\nVarnishes, Paints, Supplies"
    },
    {
      "MCC": 5199,
      "Description": "Nondurable Goods (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale distributors of nondurable goods that are not classified with a more specific MCC, such as food products, art goods, clothes hangers, foam rubber, ice, crude rubber, sponges, textile bags, burlap, canvas products, leather, and baskets.",
      "mccIncluded": "Goods � Nondurable"
    },
    {
      "MCC": 5200,
      "Description": "Home Supply Warehouse Stores",
      "mccDescription": "Merchants classified with this MCC sell of a wide range of home products such as wallpaper, paint, lumber, garden supplies, electrical equipment and supplies and home decorating items, as well as sinks, cabinets and doors.",
      "mccIncluded": "Warehouse Stores � Home Supply"
    },
    {
      "MCC": 5211,
      "Description": "Lumber and Building Materials Stores",
      "mccDescription": "Merchants classified with this MCC sell of one or more of the following products: lumber, unfinished wood products, lighting materials, concrete, sand, gravel, general building or electrical supplies, bricks, fencing, pipe, fiberglass, or molding. This MCC includes building centers that offer their products to contractors rather than to the general public and does not include home supply store chains that are frequented by the general public.",
      "mccIncluded": "Building Materials Construction Materials\nMaterials � Lumber, Building\nRoofing Materials\nSupplies � Lumber, Building Materials"
    },
    {
      "MCC": 5231,
      "Description": "Glass, Paint, and Wallpaper Stores",
      "mccDescription": "Merchants classified with this MCC sell glass, paint and painting supplies, wallpaper and hanging supplies, and other wall covering materials.",
      "mccIncluded": "Paint, Painting Supplies Supplies � Glass\nSupplies � Paint, Painting Supplies\nSupplies � Wallpaper, Supplies\nWallpaper, Wallcovering Supplies Store"
    },
    {
      "MCC": 5251,
      "Description": "Hardware Stores",
      "mccDescription": "Merchants classified with this MCC sell a full range of hardware supplies for purchase by the general public.",
      "mccIncluded": "Electrical Supplies Hand Tools\nLighting Fixtures, Supplies Plumbing Supplies\nPower Tools\nSupplies � Hardware Stores\nTools"
    },
    {
      "MCC": 5261,
      "Description": "Nurseries and Lawn and Garden Supply Stores",
      "mccDescription": "Merchants classified with this MCC sell nursery stock, trees, shrubs, other plants, seeds, bulbs, mulches, soil conditioners, fertilizers, pesticides, garden tools, and other garden supplies.",
      "mccIncluded": "Gardening Supplies Greenhouses\nLawn, Garden Supply Plant Shops\nSupplies � Nurseries, Lawn, Garden Supply Stores"
    },
    {
      "MCC": 5262,
      "Description": "Marketplaces",
      "mccDescription": "Entities classified with this MCC are online marketplaces that accept Visa and bring together cardholders and retailers selling a range of goods or services that may otherwise qualify for different MCCs, on a single e- commerce website or mobile application, under a single brand used to identify itself to cardholders.  Marketplaces with a single line of goods or services must use the MCC that most accurately describes that line of business.\nThese Marketplaces must meet all Visa qualification requirements and be registered with Visa as a Marketplace.",
      "mccIncluded": ""
    },
    {
      "MCC": 5271,
      "Description": "Mobile Home Dealers",
      "mccDescription": "Merchants classified with this MCC sell new and used mobile homes and related parts, accessories, and equipment.",
      "mccIncluded": "Dealers � Mobile Home, Parts, Equipment\nEquipment � Mobile Home Parts"
    },
    {
      "MCC": 5300,
      "Description": "Wholesale Clubs",
      "mccDescription": "Merchants classified with this MCC sell a full range of goods (often in bulk) and services in warehouse-style stores. These Merchants may or may not have membership requirements.",
      "mccIncluded": "Club Stores � Wholesale Clubs\nDiscount Goods � Wholesale Clubs Warehouse Retail �\nWholesale Clubs"
    },
    {
      "MCC": 5309,
      "Description": "Duty Free Stores",
      "mccDescription": "Merchants classified with this MCC sell a variety of gift and import items that are typically exempt from customs fees, such as liquor, perfume, and jewelry.",
      "mccIncluded": ""
    },
    {
      "MCC": 5310,
      "Description": "Discount Stores",
      "mccDescription": "Merchants classified with this MCC sell a general line of merchandise and offer it at a discounted rate.",
      "mccIncluded": ""
    },
    {
      "MCC": 5311,
      "Description": "Department Stores",
      "mccDescription": "Merchants classified with this MCC sell a general line of apparel, home furnishings, furniture, electronics, cosmetics, housewares, and major household appliances. These stores have departments that usually have their own separate check-out counters.",
      "mccIncluded": ""
    },
    {
      "MCC": 5331,
      "Description": "Variety Stores",
      "mccDescription": "Merchants classified with this MCC sell a limited selection of various products in a lower price range. Generally, these Merchants sell the same type of merchandise and operate in the same manner as a discount store, but on a much smaller scale.",
      "mccIncluded": ""
    },
    {
      "MCC": 5399,
      "Description": "Miscellaneous General Merchandise",
      "mccDescription": "Merchants classified with this MCC sell a general line of merchandise. Generally, these Merchants sell the same type of merchandise and operate in the same manner as a department store, but on a much smaller scale.",
      "mccIncluded": "General Merchandise � Miscellaneous"
    },
    {
      "MCC": 5411,
      "Description": "Grocery Stores and Supermarkets",
      "mccDescription": "Merchants classified with this MCC sell a complete, full line of food merchandise for home consumption. Most of the food merchandise is perishable, and perishables* must represent at least 45% of Merchant�s total monthly sales volume, measured in local currency. In addition, they also sell canned, frozen, prepackaged, and dry foods, a limited selection of housewares, cleaning and polishing products, personal hygiene products, cosmetics, greeting cards, books, magazines, household items, and dry goods. These Merchants may also operate specialized departments such as an in-store deli counter, meat counter, pharmacy, or floral department.\n* Perishable goods include the following:\n\n?    Bakery � in store (service)\n?    Bread and baked goods � fresh\n?    Dairy � butter, margarine, cottage cheese, sour cream, toppings, dough products, eggs, juices/drinks (refrigerated), milk, pudding, desserts, snacks, spreads, dips, and yogurt\n?    Deli � in store (service)\n?    Deli � refrigerated (self service)\n?    Frozen foods � baked goods, breakfast foods, desserts, fruits, toppings, frozen novelties, ice, ice cream, juices, drinks, meal starters, pizza, snacks, hors d�oeuvres, prepared foods, unprepared meat, poultry, seafood, vegetables\n?    Meat, fish, poultry � fresh\n?    Packaged meat � deli\n?    Fresh produce\n?    Floral",
      "mccIncluded": "Food Stores � Grocery Stores, Supermarkets Supermarkets"
    },
    {
      "MCC": 5422,
      "Description": "Freezer and Locker Meat Provisioners",
      "mccDescription": "Merchants classified with this MCC sell fresh, frozen, or cured meats, fish, shellfish, and other seafoods. This MCC also includes bulk retail sellers of meat for freezer storage and home freezer plans.",
      "mccIncluded": "Butcher Shops\nFish, Seafood Markets Frozen Meats, Seafood\nMeat Markets, Meat Lockers\nSeafood Markets"
    },
    {
      "MCC": 5441,
      "Description": "Candy, Nut, and Confectionery Stores",
      "mccDescription": "Merchants classified with this MCC are retail sellers of candy, chocolate, nuts, dried fruits,\npopcorn and other confections.",
      "mccIncluded": "Chocolate Shops Confectionery Shops Dried Fruit Shops Nut Shops\nPopcorn Stands"
    },
    {
      "MCC": 5451,
      "Description": "Dairy Products Stores",
      "mccDescription": "Merchants classified with this MCC sell over-the- counter packaged butter, cheese, ice cream, milk, and other dairy products.",
      "mccIncluded": "Cheese Shops"
    },
    {
      "MCC": 5462,
      "Description": "Bakeries",
      "mccDescription": "Merchants classified with this MCC sell (and may make) bakery goods.",
      "mccIncluded": "Bagel Shops Cake Shops Cookie Stores Doughnut Shops Pastry Shops Pie Shops Wedding Cakes"
    },
    {
      "MCC": 5499,
      "Description": "Miscellaneous Food Stores � Convenience Stores and Specialty Markets",
      "mccDescription": "Merchants classified with this MCC sell specialty foods not classified with a more specific MCC.",
      "mccIncluded": "Convenience Stores Delicatessens\nFood Stores � Convenience\nFruit Markets Gourmet Food Stores Health Food Stores Mini Markets\nPoultry Shops Produce Markets\nSpecialty Food Markets Vegetable Markets Vitamin Stores\nMeal Preparation Kits"
    },
    {
      "MCC": 5511,
      "Description": "Car and Truck Dealers (New and Used) Sales, Service, Repairs, Parts, and Leasing",
      "mccDescription": "Merchants classified with this MCC sell and/or lease new and used automobiles, trucks, pickups, and vans and may also conduct repair work and sell replacement parts and accessories.",
      "mccIncluded": "Automobile/Truck Dealers (New/Used) Sales, Service, Repairs, Parts, Leasing\nCar Dealers (New/Used) Dealers � Automobile/ Truck (New/Used), Sales,\nService, Repairs,\nParts, Leasing Leasing � Automobile, Truck\nMotor Vehicle Dealers\n(New/Used)\nRepairs � Automobile/ Truck Dealers (New/ Used) Sales, Service, Repairs, Parts, Leasing\nTruck Dealers (New/Used)"
    },
    {
      "MCC": 5521,
      "Description": "Car and Truck Dealers (Used Only) Sales, Service, Repairs, Parts, and Leasing",
      "mccDescription": "Merchants classified with this MCC sell and/or lease only used automobiles, trucks, pickups and vans and may also conduct repair work and sell replacement parts and accessories.",
      "mccIncluded": "Antique Automobiles Automobiles/Trucks (Used Only)\nCar Dealers (Used Only) Dealers � Antique Automobiles\nDealers � Automobile/ Truck (Used Only)"
    },
    {
      "MCC": 5532,
      "Description": "Automotive Tire Stores",
      "mccDescription": "Merchants classified with this MCC sell automotive and truck tires and related parts and may also conduct tire installation and repair services.",
      "mccIncluded": "Car Tires � New Only Repairs � Automotive Tire Stores\nServices � Repairs/ Automotive Tire Stores Tire Stores"
    },
    {
      "MCC": 5533,
      "Description": "Automotive Parts and Accessories Stores",
      "mccDescription": "Merchants classified with this MCC sell automobile parts, equipment, and accessories.",
      "mccIncluded": "Accessories � Automotive Automobile � Parts, Accessories Stores\nCar Parts, Accessories Stores\nMotor Vehicle Supplies, Parts\nParts, Automotive Supplies � Automotive\nParts, Accessories  Stores"
    },
    {
      "MCC": 5541,
      "Description": "Service Stations (With or without Ancillary Services)",
      "mccDescription": "Merchants classified with this MCC are retail sellers of engine fuel These Merchants may or may not also have a convenience store, car wash, or automotive repair shop on the premises, but must sell fuel for consumer use. Excluded from this category code are Automated Fuel Dispensers, MCC 5542",
      "mccIncluded": "Filling Stations � Automotive Gasoline Gas Stations (With or\nWithout Ancillary Services)\nGasoline � Service Stations Marina Service Stations\nPetrol � Service Stations Truck Stops � Gasoline Service"
    },
    {
      "MCC": 5542,
      "Description": "Automated Fuel Dispensers",
      "mccDescription": "Merchants classified with this MCC are retail sellers of engine fuel through the use of an Automated Fuel Dispenser (AFD).",
      "mccIncluded": "Cardholder-Activated\nFuel Dispensers Fuel Dispensers � Automated\nGas Pumps (Automated Fuel Dispensers)\nFuel � Self-Service Terminals\nPetrol Dispensers � Automated Petrol � Self- Service Terminals\nSelf-Service Terminals � Fuel\nSelf-Service Terminals � Petrol"
    },
    {
      "MCC": 5551,
      "Description": "Boat Dealers",
      "mccDescription": "Merchants classified with this MCC sell new and used motorboats, sailboats, and other watercraft, marine supplies, and outboard motors.",
      "mccIncluded": "Dealers � Boat Motorboat Dealers Outboard Motor Dealers Powerboat Dealers Sailboat Dealers"
    },
    {
      "MCC": 5552,
      "Description": "Electric Vehicle Charging",
      "mccDescription": "Merchants classified with this MCC sell electricity for the purpose of fueling an automobile",
      "mccIncluded": ""
    },
    {
      "MCC": 5561,
      "Description": "Camper, Recreational and Utility Trailer Dealers",
      "mccDescription": "Merchants classified with this MCC sell new and used campers, recreational and utility trailers, and related parts and accessories.",
      "mccIncluded": "Accessories � Recreational Vehicles\nCamper Dealers\nDealers � Recreational, Utility Trailers, Camper Dealers\nParts, Recreational Vehicles\nRV, Camper Dealers Recreational Vehicle Dealers, Parts, Accessories\nTrailers � Utility Utility Trailers"
    },
    {
      "MCC": 5571,
      "Description": "Motorcycle Shops and Dealers",
      "mccDescription": "Merchants classified with this MCC sell new and used motorcycles, motor scooters, mopeds and related parts, equipment, clothing, and accessories.",
      "mccIncluded": "Dealers � Motorcycle, Moped, Motor Scooters Moped Dealers, Parts, Accessories\nMotor Scooter Dealers, Parts, Accessories Supplies � Motorcycle\nParts, Accessories"
    },
    {
      "MCC": 5592,
      "Description": "Motor Homes Dealers",
      "mccDescription": "Merchants classified with this MCC sell new and used motor homes and related parts and\naccessories.",
      "mccIncluded": "Dealers � Motor Homes"
    },
    {
      "MCC": 5598,
      "Description": "Snowmobile Dealers",
      "mccDescription": "Merchants classified with this MCC sell new and used snowmobiles and related parts and accessories.",
      "mccIncluded": "Dealers � Snowmobiles"
    },
    {
      "MCC": 5599,
      "Description": "Miscellaneous Automotive, Aircraft, and Farm Equipment Dealers (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC sell new and used aircraft and/or automotive, farm, and recreational vehicles, parts, equipment and supplies not classified with a more specific MCC.",
      "mccIncluded": "Airplane, Aircraft Dealers All-Terrain Vehicle Dealers\nAutomotive Dealers � Miscellaneous Dealers � Airplane, Aircraft\nDealers � Golf Carts Dealers � Miscellaneous Automotive Dealers\nPlane Dealers � Aircraft"
    },
    {
      "MCC": 5611,
      "Description": "Men�s and Boys� Clothing and Accessories Stores",
      "mccDescription": "Merchants classified with this MCC sell men�s and boys� ready-to-wear clothing and accessories.",
      "mccIncluded": "Accessories � Men�s, Boys� Clothing\nApparel � Men�s, Boys� Clothing\nBoys� Clothing, Accessories Clothing � Men�s, Boys� Clothing, Accessory\nStores\nHat Shops � Men�s Necktie Stores\nTie Shops � Men�s Clothing"
    },
    {
      "MCC": 5621,
      "Description": "Women�s Ready-To-Wear Stores ..",
      "mccDescription": "Merchants classified with this MCC sell a general line of women�s ready-to-wear and special-event clothing.",
      "mccIncluded": "Apparel � Women�s Ready- To-Wear Stores\nBridal Shops Clothing � Women�s\nReady-To-Wear Stores\nCoat Stores � Women�s Dress Shops\nMaternity Stores Outerwear Stores � Women�s"
    },
    {
      "MCC": 5631,
      "Description": "Women�s Accessory and Specialty Shops",
      "mccDescription": "Merchants classified with this MCC sell women�s clothing accessories, including handbags, hats, costume jewelry, scarves, belts, hair accessories, lingerie, and hosiery.",
      "mccIncluded": "Accessories � Women�s Accessory, Specialty Shops\nApparel � Women�s Accessory, Specialty Shops\nClothing � Women�s Accessory, Specialty Shops\nCostume Jewelry Shops Handbag Stores\nHat Shops � Women�s Jewelry � Costume Lingerie Stores"
    },
    {
      "MCC": 5641,
      "Description": "Children�s and Infants� Wear Stores",
      "mccDescription": "Merchants classified with this MCC sell children�s and infants� clothing and accessories.",
      "mccIncluded": "Apparel � Children�s, Infants� Wear\nBaby Clothing Clothing � Children�s, Infants� Wear Stores\nInfants� Clothing"
    },
    {
      "MCC": 5651,
      "Description": "Family Clothing Stores",
      "mccDescription": "Merchants classified with this MCC sell general lines of men�s, women�s, and children�s clothing and accessories without specializing in sales for a particular gender or age group.",
      "mccIncluded": "Apparel � Family Clothing Stores\nClothing � Family Clothing Stores\nJeans, Denim Clothing Stores"
    },
    {
      "MCC": 5655,
      "Description": "Sports and Riding Apparel Stores",
      "mccDescription": "Merchants classified with this MCC sell athletic, sports, and riding apparel and active wear.",
      "mccIncluded": "Active Wear � Sports Apparel\nApparel � Sports, Riding Apparel Stores\nAthletic Apparel Stores Clothing � Sports Apparel, Riding Apparel Stores\nEquestrian Apparel Riding Apparel � Sports Apparel"
    },
    {
      "MCC": 5661,
      "Description": "Shoe Stores",
      "mccDescription": "Merchants classified with this MCC sell men�s, women�s, and/or children�s footwear (including athletic footwear) and accessories.",
      "mccIncluded": "Athletic Shoe Stores Boot Shops Footwear Stores Western Boot Shops"
    },
    {
      "MCC": 5681,
      "Description": "Furriers and Fur Shops",
      "mccDescription": "Merchants classified with this MCC sell a variety of items made of animal fur, including coats, jackets, hats, and gloves.",
      "mccIncluded": "Apparel � Furriers, Fur Shops\nClothing � Furriers, Fur Shops\nCoat Stores � Furriers"
    },
    {
      "MCC": 5691,
      "Description": "Men�s and Women�s Clothing Stores",
      "mccDescription": "Merchants classified with this MCC sell men�s and women�s clothing and accessories but not infants� and children�s clothing.",
      "mccIncluded": "Apparel � Men�s Clothing Stores\nClothing � Women�s Clothing Stores"
    },
    {
      "MCC": 5697,
      "Description": "Tailors, Seamstresses, Mending, and Alterations",
      "mccDescription": "Merchants classified with this MCC create and sell custom-made clothing and perform alterations, mending, and garment repair or restoration.",
      "mccIncluded": "Alterations"
    },
    {
      "MCC": 5698,
      "Description": "Wig and Toupee Stores",
      "mccDescription": "Merchants classified with this MCC sell permanently attached or temporary toupees, wigs, hair pieces, and hair extensions or provide hair weaving services.",
      "mccIncluded": "Hair Pieces, Extensions Hair Replacement � Non-Surgical\nToupee Stores"
    },
    {
      "MCC": 5699,
      "Description": "Miscellaneous Apparel and Accessory Shops",
      "mccDescription": "Merchants classified with this MCC sell specialized lines of apparel (excluding fur) and accessories that are not classified with a more specific MCC.",
      "mccIncluded": "Clothing � Formal Wear Clothing � Miscellaneous Apparel, Accessory Shops\nSwim Wear Shop T-Shirt Stores"
    },
    {
      "MCC": 5712,
      "Description": "Furniture, Home Furnishings, and Equipment Stores, Except Appliances",
      "mccDescription": "Merchants classified with this MCC sell home furnishings and a wide variety of finished furniture, products and accessories for the home.",
      "mccIncluded": "Bedding, Mattress Stores Custom Made Furniture Equipment Stores Home Furnishings, Furniture\nMade-to-Order Furniture Mattress Stores  Outdoor Furnishings\nPatio, Porch Furnishings Stores"
    },
    {
      "MCC": 5713,
      "Description": "Floor Covering Stores",
      "mccDescription": "Merchants classified with this MCC are retail sellers of floor coverings such as carpet, area rugs, floor tile, linoleum, stone, wood, and brick, and may or may not also perform installation services.",
      "mccIncluded": "Carpet, Rug Stores Linoleum Stores Rug Stores\nTile Stores"
    },
    {
      "MCC": 5714,
      "Description": "Drapery, Window Covering, and Upholstery Stores",
      "mccDescription": "Merchants classified with this MCC sell draperies, curtains, venetian blinds, window shades, and upholstery materials.",
      "mccIncluded": "Curtain Stores\nUpholstery Materials Stores Window-covering Stores"
    },
    {
      "MCC": 5718,
      "Description": "Fireplace, Fireplace Screens and Accessories Stores",
      "mccDescription": "Merchants classified with this MCC sell pre-made fireplaces, wood stoves, and fireplace parts and accessories.",
      "mccIncluded": "Hearth, Accessories Stores\nHeating � Fireplace, Accessories Stores Stoves � Fireplaces,\nAccessories Stores\nWoodburning Stoves, Accessories Stores"
    },
    {
      "MCC": 5719,
      "Description": "Miscellaneous Home Furnishing Specialty Stores",
      "mccDescription": "Merchants classified with this MCC sell miscellaneous home furnishings such as housewares, cutlery, bedding and linen, lamps and shades, mirrors and pictures, pottery, and ceramic items.",
      "mccIncluded": "Accessories � Home Furnishings\nBed, Bath Shops Cookware Stores Cutlery � Kitchen\nHome Furnishing Specialty Stores � Miscellaneous\nHousewares Stores Kitchenware Stores Lamp, Lighting Shops Linen Shops\nPottery Stores"
    },
    {
      "MCC": 5722,
      "Description": "Household Appliance Stores",
      "mccDescription": "Merchants classified with this MCC sell household appliances such as gas or electric ranges and stoves, ovens, refrigerators, dishwashers, water heaters, washers and dryers, garbage disposals, sewing machines, self-contained air conditioning units, and vacuum cleaners, and may or may not also conduct repair work.",
      "mccIncluded": "Sewing Machine Stores Vacuum Cleaner Stores"
    },
    {
      "MCC": 5732,
      "Description": "Electronics Stores",
      "mccDescription": "Merchants classified with this MCC sell a wide variety of electronic goods such as televisions, radios, cameras, computers, tablets, and stereo components and may also sell electronic parts and or accessories for the repair, construction or assembly of electronic equipment and/or also conduct repair work.",
      "mccIncluded": "Camcorders (And Other Electronics)\nComputer Hardware\n(And Other Electronics) Electronic Parts Electronic Repair Shops High Fidelity Equipment Personal Computers Radios\nStereos, Receivers, CD Equipment\nTelevision Stores VCR�s\nVideo Cameras (And Other Electronics)"
    },
    {
      "MCC": 5733,
      "Description": "Music Stores � Musical Instruments, Pianos, and Sheet Music",
      "mccDescription": "Merchants classified with this MCC sell musical instruments, sheet music, keyboards, instruction books, pianos, guitars and related supplies and equipment and may also offer music instruction.",
      "mccIncluded": "Guitar Stores Instruments � Musical Pianos"
    },
    {
      "MCC": 5734,
      "Description": "Computer Software Stores",
      "mccDescription": "Merchants classified with this MCC sell computer software programs for business and personal use and may also sell or lease computer hardware or other related products. Merchants that sell applications for mobile devices should use MCC 5817.",
      "mccIncluded": ""
    },
    {
      "MCC": 5735,
      "Description": "Record Stores",
      "mccDescription": "Merchants classified with this MCC sell records, compact discs (CDs), and cassettes, and may also rent video tapes.",
      "mccIncluded": "CD, Cassette, Record Stores\nCompact Disc, Cassette, Record Stores\nMusic Stores � Prerecorded Records, CDs, Cassettes\nRecord, CD, Cassette Stores\nUsed Record/CD Stores"
    },
    {
      "MCC": 5811,
      "Description": "Caterers",
      "mccDescription": "Merchants classified with this MCC contract with consumers to prepare and deliver (typically to a specified location) food and drinks for immediate consumption and may also provide clean-up services, tables, serving equipment, decorations, and personnel for on-site serving and cleanup.",
      "mccIncluded": "Food Preparation � Catering"
    },
    {
      "MCC": 5812,
      "Description": "Eating Places and Restaurants",
      "mccDescription": "Merchants classified with this MCC prepare food and drinks for immediate consumption, typically on the Merchant�s premises.  These Merchants typically provide table service and may include alcoholic beverages on the menu. This MCC includes food delivery for Merchants qualifying for this MCC.",
      "mccIncluded": "Cafes, Restaurants Cafeterias\nDiners � Eating Establishments\nRestaurants Soda Fountains"
    },
    {
      "MCC": 5813,
      "Description": "Drinking Places (Alcoholic Beverages) � Bars, Taverns, Nightclubs, Cocktail Lounges, and Discotheques",
      "mccDescription": "Merchants classified with this MCC sell alcoholic beverages such as wine, beer, mixed drinks, and other liquors and beverages for consumption on the premises.",
      "mccIncluded": "Drinking Establishments Beer Parlors\nBreweries, Bars Comedy Clubs Lounges\nMicro-breweries Pubs\nSaloons Tap Rooms Wine Bars"
    },
    {
      "MCC": 5814,
      "Description": "Fast Food Restaurants",
      "mccDescription": "Merchants classified with this MCC sell prepared food and beverages for consumption either on the premises or packaged for carry-out.  Orders are typically made and paid for at a counter, kiosk or drive-through window. This MCC includes food delivery for Merchants qualifying for this MCC.",
      "mccIncluded": "Quick Service Restaurants Vending Machines � Food Sandwich Shops Ice Cream Shops Coffee Shops Yogurt Shops Pretzel Stands"
    },
    {
      "MCC": 5815,
      "Description": "Digital Goods Media � Books, Movies, Digital artwork/images, Music",
      "mccDescription": "Merchants classified with this MCC sell books, movies, digital artwork/images or music that is delivered in electronic format.\nMerchants offering online streaming of digital media content on a subscription basis should use MCC 4899",
      "mccIncluded": ""
    },
    {
      "MCC": 5816,
      "Description": "Digital Goods � Games",
      "mccDescription": "Merchants classified with this MCC sell games (including games of skill but excluding games of chance) that are delivered in electronic format.",
      "mccIncluded": "Fantasy Sports (in jurisdictions where they not considered gambling)"
    },
    {
      "MCC": 5817,
      "Description": "Digital Goods � Applications (Excludes Games)",
      "mccDescription": "Merchants classified with this MCC sell applications or software (excludes games) that are delivered in electronic format.",
      "mccIncluded": ""
    },
    {
      "MCC": 5818,
      "Description": "Digital Goods � Large Digital Goods Merchant",
      "mccDescription": "All regions (excluding Europe):\nMerchants classified with this MCC are large-volume Merchants that sell digital goods (goods delivered via electronic format) with a minimum threshold of  25 million transactions annually.\nEurope region only:\nMerchants classified with this MCC are digital goods Merchants that are categorized as PCI DSS Level 1, as they process over 6 million Visa transactions annually.",
      "mccIncluded": ""
    },
    {
      "MCC": 5912,
      "Description": "Drug Stores and Pharmacies",
      "mccDescription": "Merchants classified with this MCC sell prescription and proprietary drugs, nonprescription (over thecounter) medicines, vitamins, cannabis (where legal to do so) and may also sell related items such as cosmetics, toiletries, tobacco, heating pads, back supports, novelty merchandise, greeting cards, and a limited supply of food items.",
      "mccIncluded": "Medicine � Prescription, Pharmacies Pharmacies\nPrescription Medication Toiletries"
    },
    {
      "MCC": 5921,
      "Description": "Package Stores � Beer, Wine, and Liquor",
      "mccDescription": "Merchants classified with this MCC sell packaged alcoholic beverages such as beer, wine, and liquor for consumption off the premises and may also sell a limited variety of food and snack items, newspapers and magazines, and a limited selection of toiletries and over-the-counter medicines.",
      "mccIncluded": "Alcohol � Liquor Stores"
    },
    {
      "MCC": 5931,
      "Description": "Used Merchandise and Secondhand Stores",
      "mccDescription": "Merchants classified with this MCC sell used merchandise or secondhand goods such as accessories, shoes and clothing, furniture, books, bicycles, musical instruments, sewing machines, electronic equipment, appliances, and other household items. Typically, the items for sale have been donated or are on consignment.",
      "mccIncluded": "Consignment Shops Secondhand Stores Thrift Stores Vintage Clothing"
    },
    {
      "MCC": 5932,
      "Description": "Antique Shops � Sales, Repairs, and Restoration Services",
      "mccDescription": "Merchants classified with this MCC sell antiques such as furniture, jewelry, cameras and photographic equipment, tools, artwork, books, appliances and other household items. This MCC does not include retail sellers of antique automobiles.",
      "mccIncluded": "Services � Antique Restoration"
    },
    {
      "MCC": 5933,
      "Description": "Pawn Shops",
      "mccDescription": "Merchants classified with this MCC lend money in exchange for personal property left with the Merchants as security.",
      "mccIncluded": ""
    },
    {
      "MCC": 5935,
      "Description": "Wrecking and Salvage Yards",
      "mccDescription": "Merchants classified with this MCC provide wrecking and salvaging services.",
      "mccIncluded": "Junk Yards"
    },
    {
      "MCC": 5937,
      "Description": "Antique Reproductions",
      "mccDescription": "Merchants classified with this MCC sell antique reproductions or facsimiles of clothing, furniture, rugs, paintings and artwork, mirrors, glassware, jewelry, and other household and personal items and may also provide antique photograph reproduction and restoration services.",
      "mccIncluded": ""
    },
    {
      "MCC": 5940,
      "Description": "Bicycle Shops � Sales and Service",
      "mccDescription": "Merchants classified with this MCC sell bicycles, bicycle parts, and accessories (including a limited variety of related sporting apparel) and may also conduct repair work.",
      "mccIncluded": "Mountain Bikes Service � Bicycles"
    },
    {
      "MCC": 5941,
      "Description": "Sporting Goods Stores",
      "mccDescription": "Merchants classified with this MCC sell sporting goods, sports equipment, camping supplies, and related parts and accessories, and may also rent or repair equipment.",
      "mccIncluded": "Backpacking Equipment Bait, Tackle Stores\nBilliard Tables � Sales Camping Equipment\nCutlery � Sporting Knives Exercise Equipment\nFishing � Equipment, Bait, Tackle\nGolf Equipment Gun Shops Hiking Equipment\nHunting Equipment, Supplies\nIn-Line Skates Shops Pool Tables � Sales Rollerblade Shops\nScuba, Skin Diving Equipment\nSkateboard Shops Ski Shops\nSkin Diving, Scuba Equipment\nSurfboards, Accessories Trekking Equipment\nWindsurf Boards, Accessories"
    },
    {
      "MCC": 5942,
      "Description": "Book Stores",
      "mccDescription": "Merchants classified with this MCC sell new and/or used books, journals, magazines, textbooks, maps and atlases, audio books, and calendars.",
      "mccIncluded": "Paperbacks � Book Stores Textbooks"
    },
    {
      "MCC": 5943,
      "Description": "Stationery Stores, Office and School Supply Stores",
      "mccDescription": "Merchants classified with this MCC are retail sellers of paper goods used in offices and schools, office and school supplies,, and small office equipment and furniture.",
      "mccIncluded": "Office Supply Stores School Supply Store"
    },
    {
      "MCC": 5944,
      "Description": "Jewelry Stores, Watches, Clocks, and Silverware Stores",
      "mccDescription": "Merchants classified with this MCC sell fine jewelry, watches, clocks, and sterling silver and silver-plated flatware and accessories, and may also repair fine jewelry.",
      "mccIncluded": "Clock Shop Diamond Stores\nGems, Precious Metals, Jewelry\nJewelry � Fine\nPrecious Gems, Metals, Jewelry\nSilversmiths Silverware Stores Watch Shops"
    },
    {
      "MCC": 5945,
      "Description": "Hobby, Toy, and Game Shops",
      "mccDescription": "Merchants classified with this MCC sell toys and games and may also sell a limited selection of do-it- yourself craft or hobby kits.",
      "mccIncluded": "Games Stores Hobby Shops Toy Stores\nToy, Game Shops"
    },
    {
      "MCC": 5946,
      "Description": "Camera and Photographic Supply Stores",
      "mccDescription": "Merchants classified with this MCC sell cameras, film, video equipment, printers, and other photographic equipment and supplies, and may provide photo/video processing services.",
      "mccIncluded": "Camcorders, Photographic Equipment\nEquipment � Photographic Supply Stores\nPhotography Supply Stores Supplies � Photographic\nVideo Cameras, Photographic Equipment"
    },
    {
      "MCC": 5947,
      "Description": "Gift, Card, Novelty and Souvenir Shops",
      "mccDescription": "Merchants classified with this MCC sell gifts and novelty items, greeting cards, balloons, souvenirs, holiday decorations, stationery, wrapping paper and bows, photo albums, and a limited selection of office supplies.",
      "mccIncluded": "Memorabilia, Souvenir Shops\nNovelty Shops\nSouvenir, Memorabilia Shops"
    },
    {
      "MCC": 5948,
      "Description": "Luggage and Leather Goods Stores",
      "mccDescription": "Merchants classified with this MCC are retail sellers of luggage, trunks, briefcases, leather backpacks, wallets, purses, gloves, and other leather goods, and may also sell a limited selection of leather apparel.",
      "mccIncluded": ""
    },
    {
      "MCC": 5949,
      "Description": "Sewing, Needlework, Fabric and Piece Goods Stores",
      "mccDescription": "Merchants classified with this MCC are retail sellers of fabric, patterns, yarn and other needlework/sewing supplies, and may offer sewing classes or instruction.",
      "mccIncluded": "Fabric Stores Knitting Shops Needlework Shops\nSupplies � Sewing, Fabric, Notions, Needlework, Piece Goods\nYarn Shops"
    },
    {
      "MCC": 5950,
      "Description": "Glassware/Crystal Stores",
      "mccDescription": "Merchants classified with this MCC sell glassware, china and crystal tableware, and glass, china or crystal gift items.",
      "mccIncluded": "China, Crystal Stores Crystal Stores\nCut Glass, Crystal"
    },
    {
      "MCC": 5960,
      "Description": "Direct Marketing � Insurance Services",
      "mccDescription": "Merchants classified with this MCC sell insurance via direct mail, billing statement stuffers, and magazine or television ads, all of which include either a toll-free telephone number or an address to which prospective customers may respond.",
      "mccIncluded": "Automobile Insurance � Sales (Direct Marketing) Insurance Services �\nDirect Marketing\nServices � Insurance, Direct Marketing"
    },
    {
      "MCC": 5962,
      "Description": "Direct Marketing � Travel-Related Arrangement Services",
      "mccDescription": "Merchants classified with this MCC sell travel-related services (including discount travel clubs or newsletters) via outbound telemarketing calls, mailings, magazine or television ads, or other direct marketing methods that include either a toll-free telephone number or an address to which prospective customers may respond.",
      "mccIncluded": "Services � Direct Marketing Travel- Related Arrangement Services\nTelemarketing Travel- Related Arrangement Services (Excluding Travel Agencies)\nTravel Clubs (Direct Marketing)\nTravel-Related Arrangement Services- Direct Marketing"
    },
    {
      "MCC": 5963,
      "Description": "Door-To-Door Sales",
      "mccDescription": "Merchants classified with this MCC sell a variety of products by means of house-to-house visits.",
      "mccIncluded": ""
    },
    {
      "MCC": 5964,
      "Description": "Direct Marketing � Catalog Merchant",
      "mccDescription": "Merchants classified with this MCC sell a variety of goods via catalogs and offer no means by which customers may purchase products in a face-to-face manner.",
      "mccIncluded": ""
    },
    {
      "MCC": 5965,
      "Description": "Direct Marketing � Combination Catalog and Retail Merchant",
      "mccDescription": "Merchants classified with this MCC sell a variety of goods via catalog and one or more retail outlets. MCC 5965 is used for goods sold via catalog, while the MCC relevant to what is sold in the retail outlets is used for goods sold in the retail outlets.",
      "mccIncluded": ""
    },
    {
      "MCC": 5966,
      "Description": "Direct Marketing � Outbound Telemarketing Merchant",
      "mccDescription": "Merchants classified with this MCC sell a variety of products using outbound telemarketing methods, where the Merchant initiates contact with prospective buyers via telephone or mailing (other than a catalog) that instructs the cardholder to contact the Merchant.\nThis MCC excludes charitable organizations that conduct outbound telemarketing.",
      "mccIncluded": ""
    },
    {
      "MCC": 5967,
      "Description": "Direct Marketing � Inbound Teleservices Merchant",
      "mccDescription": "Merchants classified with this MCC sell information or content services (or goods sold through the service) that customers access via telephone or fax or over the internet.",
      "mccIncluded": "Adult Content Audiotext Merchants Videotext Merchants\nDigital Content Merchants"
    },
    {
      "MCC": 5968,
      "Description": "Direct Marketing � Continuity/Subscription Merchant",
      "mccDescription": "Merchants classified with this MCC sell subscription products for delivery (such as book clubs, collectibles, magazines/newspapers cosmetics, food, clothing, �nutraceuticals�) via �direct� mail, email. apps, or salespeople. Merchants not participating in direct marketing activities that happen to offer recurring purchase transactions should use the MCC that best describes their business instead. For example, Merchants providing the ongoing delivery of television/radio/streaming content on a subscription basis must use MCC 4899\nQualified Merchants that �directly� sell subscriptions for digital goods or content may use MCC 5815, 5816, 5817, or 5818.",
      "mccIncluded": "Magazine Subscription\n(Direct Mail Only)\nDirect Mail Merchants"
    },
    {
      "MCC": 5969,
      "Description": "Direct Marketing � Other Direct Marketers (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC sell products or services through a variety of direct response methods that include an order form or an address and/or telephone number the customer may use to place an order by mail, telephone, or fax and that are not classified with a more specific MCC.",
      "mccIncluded": "Classified Ads � Newspaper (Via Direct Marketing)\nTicket Agencies � Direct Marketing"
    },
    {
      "MCC": 5970,
      "Description": "Artist�s Supply and Craft Shops",
      "mccDescription": "Merchants classified with this MCC sell art and craft materials, equipment and supplies used to create other items.",
      "mccIncluded": "Supplies � Art, Crafts"
    },
    {
      "MCC": 5971,
      "Description": "Art Dealers and Galleries",
      "mccDescription": "Merchants classified with this MCC sell artwork such as paintings, photographs, or sculpture.",
      "mccIncluded": ""
    },
    {
      "MCC": 5972,
      "Description": "Stamp and Coin Stores",
      "mccDescription": "Merchants classified with this MCC sell postage stamps and coins, and related accessories for the purpose of stamp collecting, coin collecting or investment.",
      "mccIncluded": "Numismatic Supplies Philatelic Supplies Stamp Collecting\nSupplies � Numismatic, Philatelic"
    },
    {
      "MCC": 5973,
      "Description": "Religious Goods Stores",
      "mccDescription": "Merchants classified with this MCC sell religious goods such as figurines, cards, devotion supplies, pictures, books, and decorative items.",
      "mccIncluded": ""
    },
    {
      "MCC": 5975,
      "Description": "Hearing Aids � Sales, Service, and Supply",
      "mccDescription": "Merchants classified with this MCC sell hearing aids and related supplies and may also conduct repair work.",
      "mccIncluded": "Repairs � Hearing Aids, Sales, Service, Supply, Stores\nService � Hearing Aids, Supplies\nSupplies � Hearing Aids, Service, Supplies"
    },
    {
      "MCC": 5976,
      "Description": "Orthopedic Goods � Prosthetic Devices",
      "mccDescription": "Merchants classified with this MCC sell a variety of prosthetic devices and orthopedic goods and accessories and may also conduct repair work.",
      "mccIncluded": "Orthotics, Prosthetic, Devices\nProsthetic Devices"
    },
    {
      "MCC": 5977,
      "Description": "Cosmetic Stores",
      "mccDescription": "Merchants classified with this MCC sell cosmetics.",
      "mccIncluded": "Make-Up Stores"
    },
    {
      "MCC": 5978,
      "Description": "Typewriter Stores � Sales, Rentals, and Service",
      "mccDescription": "Merchants classified with this MCC sell, rent, or lease typewriters, may also sell a limited selection of office supplies and equipment, and may also conduct repair work.",
      "mccIncluded": ""
    },
    {
      "MCC": 5983,
      "Description": "Fuel Dealers � Fuel Oil, Wood, Coal, and Liquefied Petroleum",
      "mccDescription": "Merchants classified with this MCC sell fuel oil, wood, coal, liquefied petroleum, aviation fuel, heating oil or gas propane for consumer use, and may also sell fuel for consumer use in automobiles.",
      "mccIncluded": "Aviation Fuel Coal Dealers\nDealers � Fuel Dealers, Fuel Oil, Wood, Coal, Liquefied Petroleum\nHome Heating Fuel Dealers Liquefied Petroleum Dealers\nOil Dealers Propane Dealers Wood Dealers"
    },
    {
      "MCC": 5992,
      "Description": "Florists",
      "mccDescription": "Merchants classified with this MCC sell cut flowers, flower arrangements, and potted plants, and may also sell novelty items.",
      "mccIncluded": "Balloon Bouquets Flower Shops Plant Store"
    },
    {
      "MCC": 5993,
      "Description": "Cigar Stores and Stands",
      "mccDescription": "Merchants classified with this MCC sell tobacco, cigarettes, electronic cigarettes, cigars, pipes, and smokers� supplies.",
      "mccIncluded": "Pipe Shops Smoke Shops Tobacco Shops"
    },
    {
      "MCC": 5994,
      "Description": "News Dealers and Newsstands",
      "mccDescription": "Merchants classified with this MCC sell newspapers, magazines, journals, and other periodicals.",
      "mccIncluded": "Dealers � Newspapers Magazines (Newsstands) Newspapers\nPeriodicals (Newsstands)"
    },
    {
      "MCC": 5995,
      "Description": "Pet Shops, Pet Foods and Supplies Stores",
      "mccDescription": "Merchants classified with this MCC sell domestic pets, pet food, and pet supplies.",
      "mccIncluded": "Animals"
    },
    {
      "MCC": 5996,
      "Description": "Swimming Pools � Sales and Service",
      "mccDescription": "Merchants classified with this MCC sell home swimming pools, spas, hot tubs, whirlpool baths, and supplies, and may also provide installation and/or repair services.",
      "mccIncluded": "Hot Tubs, Spas � Sales, Service, Supplies\nServices � Hot Tubs, Spas, Sales, Service, Supplies Services � Swimming\nPools, Sales, Service,\nSupplies\nSupplies � Hot Tubs, Spas Supplies � Swimming Pools"
    },
    {
      "MCC": 5997,
      "Description": "Electric Razor Stores � Sales and Service",
      "mccDescription": "Merchants classified with this MCC sell electric razors and may also provide repair services.",
      "mccIncluded": ""
    },
    {
      "MCC": 5998,
      "Description": "Tent and Awning Shops",
      "mccDescription": "Merchants classified with this MCC sell tents and awnings for home or business use and may also provide repair services.",
      "mccIncluded": "Awnings � Residential, Commercial"
    },
    {
      "MCC": 5999,
      "Description": "Miscellaneous and Specialty Retail Shops",
      "mccDescription": "Merchants classified with this MCC sell unique or specialized products that are not classified with a more specific MCC.",
      "mccIncluded": "Ammunition Stores Atlas, Map Stores Beauty Supply Stores\nBottled and Distilled Water Dealers Collectibles,\nMemorabilia Stores-\nSports and Hobby Dealers � Ice Firearms, Ammunition\nStores\nFireworks Stores Frame Shops � Photograph, Poster\nGravestones, Monuments � Sales, Installation\nHeadstones, Monuments Ice Dealers\nMagic Shops Map, Atlas Stores Monuments, Gravestones � Sales, Installation\nParty Supply Stores Picture Frames, Framing Shops\nSpecialty Retail Stores � Miscellaneous Sunglasses Stores\nSupplies � Beauty Supply Stores\nSupplies � Party Tombstones � Sales, Installation\nTrophy Sales"
    },
    {
      "MCC": 6010,
      "Description": "Financial Institutions � Manual Cash Disbursements",
      "mccDescription": "This MCC must be used for face-to-face cash disbursements.",
      "mccIncluded": "Banks � Financial Institutions � Manual Cash Disbursements\nCash � Banks, Credit Unions, Financial Institutions\nCredit Unions � Financial Institutions � Manual Cash Disbursements\nDeposits � Financial Institutions\nManual Cash Disbursements Savings and Loans �\nFinancial Institutions �\nManual Cash Disbursements Travelers Cheques\n(Financial Institutions)\nCash"
    },
    {
      "MCC": 6011,
      "Description": "Financial Institutions � Automated Cash Disbursements",
      "mccDescription": "This MCC must be used only for cash disbursements at automated teller machines (ATMs) owned, leased, controlled, or sponsored by banks, savings and loans, thrifts, and credit unions.",
      "mccIncluded": "ATM Cash � Disbursements Cash - ATM"
    },
    {
      "MCC": 6012,
      "Description": "Financial Institutions � Merchandise, Services, and Debt Repayment",
      "mccDescription": "This MCC must be used by Financial Institutions for the purchase of merchandise or services or the repayment of loans & debts.  �Financial Institution� includes banks, savings and loans, thrifts, and credit unions. For example: the purchase of checks, other financial products, or promotional merchandise, deposits, the funding of an account, the purchase or reload of a stored value card, the purchase of foreign currency, non-fiat currency (for example: cryptocurrency), money orders (a negotiable paper- based remittance � not a Money Transfer), travelers cheques, and loan fees or financial counseling service fees. This MCC must also be used for the repayment of a debt, loan, or credit card balance by a cardholder to the financial institution.\nA Financial Institution that sells non-fiat currency such as cryptocurrency must include all required data in the authorization request and clearing record.",
      "mccIncluded": "Account Funding � Financial Institution Banks � Financial\nInstitutions �\nMerchandise, Services Credit Unions � Financial\nInstitutions �\nMerchandise, Services Deposits � Financial\nInstitutions\nMerchandise, Services � Financial Institutions\nMortgage Companies � Financial Institutions\nPurchase of Foreign currency, non-fiat currency (for example, cryptocurrency), money orders (Not Money Transfer) and Travelers Cheques � Financial Institution\nLoan Payments � Financial Institution\nSavings and Loans � Financial Institutions � Merchandise, Services\nServices � Banks, Credit Unions, Financial Institutions (Non-Cash)\nStored Value Card"
    },
    {
      "MCC": 6051,
      "Description": "Non-Financial Institutions � Foreign Currency, Non-Fiat Currency (for example: Cryptocurrency), Money Orders (Not Money Transfer), Account Funding (not Stored Value Load), Travelers Cheques, and Debt Repayment",
      "mccDescription": "This MCC must be used for the funding of an account (excluding prepaid card loads), the purchase of foreign currency, non-fiat currency (for example: cryptocurrency), money orders, or travelers cheques that occurs at non-Financial Institutions such as currency exchanges or money order (a negotiable paper-based remittance � not a Money Transfer) Merchants.\nThis MCC must also be used for the repayment of a loan or debt if the entity that holds the debt is not a financial institution.\nA Merchant that sells non-fiat currency such as cryptocurrency must include all required data in the authorization request and clearing record. All other transactions at the same Merchant location must use the appropriate MCC for those transactions.",
      "mccIncluded": "Account Funding �\n(Non-Financial Institutions) Currency � Non-Fiat (for example: Cryptocurrency)\nCurrency � Foreign (Non- Financial Institutions) Exchange � Foreign\nCurrency (Non-Financial\nInstitutions)\nForeign Currency (Non- Financial Institutions) Lease Payments\nLoan Payments\nMoney � Foreign Currency\n(Non-Financial Institutions) Money Orders � Not Money Transfer (Non-Financial\nInstitutions)\nMortgage Companies\n� (Non-Financial Institutions)\nServices � Money Order, Traveler�s Cheques, Foreign Exchange (Non- Financial Institutions)\nTraveler�s Cheques\n(Financial Institutions)\nTraveler�s Cheques\n(Non-Financial Institutions)"
    },
    {
      "MCC": 6211,
      "Description": "Security Brokers/Dealers",
      "mccDescription": "Merchants classified with this MCC are licensed, in all jurisdictions they sell into to buy, sell and broker securities, stocks, bonds, commodities, and mutual funds.",
      "mccIncluded": "Bond Dealers Brokers � Securities,\nMutual Funds, Stocks,\nCommodities, Bonds Commodity Dealers\nDealers � Securities, Mutual Funds, Stocks, Commodities, Bonds\nForeign Currency Purchase Licensed Foreign Exchange Trading\nBinary Options Investment Firms � Dealers, Brokers\nMutual Funds Brokers\nStockbrokers"
    },
    {
      "MCC": 6300,
      "Description": "Insurance Sales, Underwriting, and Premiums",
      "mccDescription": "Merchants classified with this MCC sell personal or business insurance policies such as automobile, life, health, hospital, medical, and dental insurance, homeowners� and renters� insurance, real estate title insurance, pet health insurance and flood, fire, or earthquake insurance.",
      "mccIncluded": "Automobile Insurance � Sales (Non-Direct Marketing)\nHealth Insurance � Sales Homeowners Insurance � Sales\nLife Insurance � Sales Medical Insurance � Sales Product Warranties"
    },
    {
      "MCC": 6513,
      "Description": "Real Estate Agents and Managers",
      "mccDescription": "This MCC must be used for the payment of management fees, rental commissions, rental payments, and other payments at real estate agents, brokers, and managers engaged in the rental and management of residential and commercial properties.  This MCC should also be used for payments of deposits for real estate purchases Short term rentals (of less than one month) must be included in MCC 7011 Lodging � Hotels, Motels, Resorts, Central Reservation Services (Not Elsewhere Classified) or, if appropriate, the Merchant�s MCC.\nThis MCC does not include mortgage loan payments and real estate purchases. These transactions must be identified with a more appropriate MCC such as MCC 6012 Financial Institutions � Merchandise and Services; MCC 1520 General Contractors; MCC 8111 Legal Services and Attorneys.",
      "mccIncluded": "Apartment Rentals Commercial Property Rentals\nHomeowner/Condo Association Fees/Dues\nHousing Rentals Property Rentals\nReal Estate Agents Brokers, Managers � Rentals\nRental Services � Residential and Commercial Properties\nResidential Rentals Service � Residential and Commercial Properties\nRentals"
    },
    {
      "MCC": 6540,
      "Description": "Non-Financial Institutions � Stored Value Card Purchase/Load",
      "mccDescription": "This MCC must be used by Merchants whose primary business is the sale of and/or any subsequent reloads of stored value cards/accounts (including Visa Prepaid Cards) occurring at non-financial institutions. This MCC is not applicable for use with Staged Digital Wallet Operators (SDWO).",
      "mccIncluded": "Account Funding � (Non- Financial Institutions) Stored Value Card/Load\n(Non-Financial Institutions)\nStored Value Wallet/Load\n(Non-Financial Institutions)"
    },
    {
      "MCC": 7011,
      "Description": "Lodging � Hotels, Motels, Resorts, Central Reservation Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are lodging establishments or central reservation services that do not have their own specific MCC.\nMCC 7011 may include, bed and breakfast establishments, resorts, cabins, cottages, hostels, short-term property rentals less than 30 days in length, and local inns.",
      "mccIncluded": "Bed & Breakfast Establishments Central Reservation\nServices\nHotels (Not Listed in T&E Section)\nInns (Not Listed in T&E Section)\nLodging Establishments (Not Listed in T&E Section)\nMotels (Not Listed in T&E Section)\nResorts (Not Listed in T&E Section) Services � Central\nReservation, Lodging"
    },
    {
      "MCC": 7012,
      "Description": "Timeshares",
      "mccDescription": "Merchants classified with this MCC sell, lease, and rent timeshares and arrange timeshare condominium exchanges.",
      "mccIncluded": ""
    },
    {
      "MCC": 7032,
      "Description": "Sporting and Recreational Camps",
      "mccDescription": "Merchants classified with this MCC operate children�s camps, recreational camps, and sporting camps.",
      "mccIncluded": "Boys� Camps\nCamps � Boys�, Girls� Camps � Sporting, Recreational Children�s Camps Girls� Camps\nRecreational, Sporting Camps\nNudist Camps Dude Ranches"
    },
    {
      "MCC": 7033,
      "Description": "Trailer Parks and Campgrounds",
      "mccDescription": "Merchants classified with this MCC provide overnight or short-term camp sites for recreational vehicles, trailers, campers, or tents.",
      "mccIncluded": ""
    },
    {
      "MCC": 7210,
      "Description": "Laundry, Cleaning, and Garment Services",
      "mccDescription": "Merchants classified with this MCC operate steam or other types of laundries or laundry services.",
      "mccIncluded": "Diaper Services Garment Services\nServices � Laundry, Cleaning, Garment Services"
    },
    {
      "MCC": 7211,
      "Description": "Laundries � Family and Commercial",
      "mccDescription": "Merchants classified with this MCC provide coin- operated or similar self-service washers and dryers for general consumer use and may also offer laundry services.",
      "mccIncluded": "Laundromats"
    },
    {
      "MCC": 7216,
      "Description": "Dry Cleaners",
      "mccDescription": "Merchants classified with this MCC perform dry cleaning services for individuals or businesses and may offer limited alteration services.",
      "mccIncluded": "Services � Dry Cleaning"
    },
    {
      "MCC": 7217,
      "Description": "Carpet and Upholstery Cleaning",
      "mccDescription": "Merchants classified with this MCC clean carpets, drapes, and upholstery.",
      "mccIncluded": "Drapery Cleaning"
    },
    {
      "MCC": 7221,
      "Description": "Photographic Studios",
      "mccDescription": "Merchants classified with this MCC perform still or video portrait photography for the general public.",
      "mccIncluded": "Photographers � Wedding Portrait Studios\nWedding Photographers"
    },
    {
      "MCC": 7230,
      "Description": "Beauty and Barber Shops",
      "mccDescription": "Merchants classified with this MCC provide personal hair care services such as hair cutting, styling, and coloring, and may also perform manicures and pedicures and sell a limited selection of hair care products.",
      "mccIncluded": "Beauty Salons Fingernail Salons Hair Cutting Hair Salon Hair Styling Hairdressers\nMake-up Studios Manicurists\nNail Salons\nSalons � Hair, Beauty Salons � Nail"
    },
    {
      "MCC": 7251,
      "Description": "Shoe Repair Shops, Shoeshine Parlors, and Hat Cleaning Shops",
      "mccDescription": "Merchants classified with this MCC repair footwear and provide shoe and hat cleaning and maintenance services.",
      "mccIncluded": "Hat Cleaning Shops Shoeshine Shops, Stands\nRepairs � Shoe Repair Shops"
    },
    {
      "MCC": 7261,
      "Description": "Funeral Services and Crematories",
      "mccDescription": "Merchants classified with this MCC are mortuaries, crematories, and funeral homes that provide burial preparation services, conduct funerals, and provide cremation services.",
      "mccIncluded": "Mortuaries Morticians Undertakers"
    },
    {
      "MCC": 7273,
      "Description": "Dating Services",
      "mccDescription": "Merchants classified with this MCC provide dating and escort services, including online services.",
      "mccIncluded": "Escort Services\nServices � Dating, Escort Online Dating Services"
    },
    {
      "MCC": 7276,
      "Description": "Tax Preparation Services",
      "mccDescription": "Merchants classified with this MCC provide income tax return preparation services but do not provide auditing, accounting, or bookkeeping services.",
      "mccIncluded": "Services � Tax Preparation"
    },
    {
      "MCC": 7277,
      "Description": "Counseling Services � Debt, Marriage, and Personal",
      "mccDescription": "Merchants classified with this MCC provide a variety of personal counseling services, such as debt and financial counseling, marriage counseling, family counseling, and alcohol and drug abuse counseling.",
      "mccIncluded": "Alcohol, Drug Abuse Counseling\nCredit Counseling Debt Counseling\nDrug, Alcohol Abuse Counseling\nFamily Counseling Financial Counseling Service\nMarriage Counseling Personal Counseling\nServices � Counseling � Debt, Marriage, Personal Services � Financial Counseling"
    },
    {
      "MCC": 7278,
      "Description": "Buying and Shopping Services and Clubs",
      "mccDescription": "Merchants classified with this MCC act as an intermediary to facilitate buying and shopping for individuals or businesses on a fee basis.",
      "mccIncluded": "Personal Shopper Services � Buying/Shopping\nServices, Clubs\nShopping Services"
    },
    {
      "MCC": 7296,
      "Description": "Clothing Rental � Costumes, Uniforms, Formal Wear",
      "mccDescription": "Merchants classified with this MCC rent costumes, uniforms, formal wear, and other types of apparel and related accessories.",
      "mccIncluded": "Apparel � Clothing Rental\n� Costumes, Formal Wear, Uniforms\nCostume Rental Formal Wear Rental Rentals � Clothing �\nCostumes, Formal Wear,\nUniforms\nRentals � Costume Rentals � Tuxedo, Formal Wear\nRentals � Uniforms Tuxedo, Formal Wear Rentals\nUniforms Rental"
    },
    {
      "MCC": 7297,
      "Description": "Massage Parlors",
      "mccDescription": "Merchants classified with this MCC specialize in providing therapeutic massage services and may also provide other personal services such as spa treatments and rent saunas or hot tubs for customer use.",
      "mccIncluded": ""
    },
    {
      "MCC": 7298,
      "Description": "Health and Beauty Spas",
      "mccDescription": "Merchants classified with this MCC provide a variety of personal or therapeutic services such as facials, massages, mud baths, herbal wraps, tanning treatments, whirlpools, steam baths, and hair and makeup styling, and instructional classes.",
      "mccIncluded": "Tanning Salons"
    },
    {
      "MCC": 7299,
      "Description": "Miscellaneous Personal Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide personal services that are not classified with a more specific MCC.",
      "mccIncluded": "Animal Boarding Services Animal Breeders\nAnimal Grooming Services\nAnimal Training\nApartment Rental Services Boarding Services � Animal\nBody Piercing, Tattoo Parlors\nBreeders � Animals Dog Grooming Services Dog Training\nHorse Boarding Services Kennels � Animal\nPet Grooming Services\nServices � Animal Boarding Services � Stand-Alone Tipping/Gratuities\nServices � Miscellaneous Other Services � Miscellaneous\nPersonal Services � Pet Grooming\nServices � Water Filtration, Purification,\nSoftening Treatments Stables � Animal\nTattoo, Body Piercing Parlors\nTaxidermists Wedding Chapels / Planning"
    },
    {
      "MCC": 7311,
      "Description": "Advertising Services",
      "mccDescription": "Merchants classified with this MCC prepare advertising (writing copy, artwork, graphics, and other creative work) and place such advertising and/or samples in periodicals, newspapers, radio and television, on the internet, in the sky, or other advertising media for clients on a contract or fee basis.",
      "mccIncluded": "Ad Agencies\nAgencies � Advertising Classified Ads � Newspaper\nServices � Advertising"
    },
    {
      "MCC": 7321,
      "Description": "Consumer Credit Reporting Agencies",
      "mccDescription": "Merchants classified with this MCC provide consumer credit reporting services such as credit investigation, credit clearinghouses, and other related services.\nFor Merchants primarily engaged in collecting debts in default owed to others or that were originally owed to others, use MCC 7322",
      "mccIncluded": ""
    },
    {
      "MCC": 7322,
      "Description": "Collection Agencies",
      "mccDescription": "Merchants classified with the MCC collect payments of overdue receivables under contract or collect overdue receivables that they have purchased from a third party.",
      "mccIncluded": ""
    },
    {
      "MCC": 7333,
      "Description": "Commercial Photography, Art, and Graphics",
      "mccDescription": "Merchants classified with this MCC provide commercial art or graphic design services for advertising agencies, publishers, and other businesses.",
      "mccIncluded": "Commercial Artists Film Production Graphics � Commercial Silk Screening"
    },
    {
      "MCC": 7338,
      "Description": "Quick Copy, Reproduction, and Blueprinting Services",
      "mccDescription": "Merchants classified with this MCC reproduce and bind text, drawings, plans, maps, or other copy, by blueprinting, photocopying, or other methods of duplication.",
      "mccIncluded": "Photocopying Services Reproduction Services\nServices � Quick Copy, Reproduction"
    },
    {
      "MCC": 7339,
      "Description": "Stenographic and Secretarial Support",
      "mccDescription": "Merchants classified with this MCC provide a range of stenographic and secretarial services such as word processing, typing, editing, letter writing, proofreading, resume writing, and court reporting services.",
      "mccIncluded": "Services � Word Processing\nWord Processing Services"
    },
    {
      "MCC": 7342,
      "Description": "Exterminating and Disinfecting Services",
      "mccDescription": "Merchants classified with this MCC provide pest control, extermination, fumigation, and disinfection services.",
      "mccIncluded": "Exterminators Pest Control"
    },
    {
      "MCC": 7349,
      "Description": "Cleaning, Maintenance, and Janitorial Services",
      "mccDescription": "Merchants classified with this MCC provide cleaning and maintenance services.",
      "mccIncluded": "House Cleaning Services Housekeeping Services Maintenance Services Services � Housekeeping"
    },
    {
      "MCC": 7361,
      "Description": "Employment Agencies and Temporary Help Services",
      "mccDescription": "Merchants classified with this MCC provide employment services for those seeing employees or employment.",
      "mccIncluded": "Temporary Employment Agencies"
    },
    {
      "MCC": 7372,
      "Description": "Computer Programming, Data Processing, and Integrated Systems Design Services",
      "mccDescription": "Merchants classified with this MCC provide computer programming services, systems design, website design, data entry, and data processing services.",
      "mccIncluded": "Computer Software Design Systems Design � Computer"
    },
    {
      "MCC": 7375,
      "Description": "Information Retrieval Services",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale provisioners of online or database information retrieval services.",
      "mccIncluded": ""
    },
    {
      "MCC": 7379,
      "Description": "Computer Maintenance, Repair and Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide computer- related services such as computer consulting, database development, computer maintenance and repair, conversion services, and computer requirements analysis.",
      "mccIncluded": ""
    },
    {
      "MCC": 7392,
      "Description": "Management, Consulting, and Public Relations Services",
      "mccDescription": "Merchants classified with this MCC perform a variety of activities for private, nonprofit, and public organizations, such as strategic and organizational planning, financial planning and budgeting, developing marketing objectives, information systems planning, development of human resource policies, and public relations services.",
      "mccIncluded": "Consultants � Management Consultants � Marketing\nConsultants � Public Relations\nConsulting, Management Services\nManagement Consultants Marketing Consultants Public Relations Services\nServices � Management, Consulting, Public Relations"
    },
    {
      "MCC": 7393,
      "Description": "Detective Agencies, Protective Services, and Security Services, including Armored Cars, and Guard Dogs",
      "mccDescription": "Merchants classified with this MCC provide security devices/systems and services such as security guard, protective, and armored car services and investigative services.",
      "mccIncluded": "Alarm Systems, Services Armored Cars\nBurglar Alarm Companies\n� Home/Business Security\nFire Alarm Companies � Home/Business Security Guard Dogs\nHome Security Systems Private Investigators Protective Services\nSecurity Services, Protective Services, Detective Agencies\nSecurity Systems Services � Armored Car\nServices � Detective Agencies\nServices � Guard Dog Services � Protective Services � Security\nSurveillance Systems, Services"
    },
    {
      "MCC": 7394,
      "Description": "Equipment, Tool, Furniture, and Appliance Rental and Leasing",
      "mccDescription": "Merchants classified with this MCC rent and lease equipment, tools, furniture, appliances, and office equipment, excluding typewriters.",
      "mccIncluded": "Appliance Rental Furniture Rental\nLeasing Services � Equipment\nRentals � Appliance Rentals � Equipment Rentals � Furniture Rentals � Tools\nServices � Equipment Rental, Leasing"
    },
    {
      "MCC": 7395,
      "Description": "Photofinishing Laboratories and Photo Developing",
      "mccDescription": "Merchants classified with this MCC develop film, make photographic prints and enlargements, and provide custom services, and may also sell a limited selection of photographic supplies and accessories.",
      "mccIncluded": "Film Developing"
    },
    {
      "MCC": 7399,
      "Description": "Business Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide business services that are not typically considered to be �professions� and are not classified with a more specific MCC.",
      "mccIncluded": "Answering Services � Telephone\nConference Management Services\nConvention Bureaus\nEvent Planning and Management\nKeys � Locksmith Language, Translation Services\nLocksmith\nMail, Packing Stores, Services\nMeeting Planning Services Message, Paging Services Packing, Mailing Services\nPaging, Message Service Centers\nPostal Services � Non- Government Publishing Companies\nSeminar Planning Services Services � Telephone Answering\nServices � Trade Show Arrangement\nServices � Translation, Language\nTelephone Answering Services\nTourist Information Bureaus Trade Show Arrangement Services\nTranslation Services Video-Conferencing Services"
    },
    {
      "MCC": 7512,
      "Description": "Automobile Rental Agency",
      "mccDescription": "Merchants classified with this MCC are car rental agencies that do not have designated unique MCCs. This also includes car-sharing schemes (known as �car-clubs� in the UK)",
      "mccIncluded": "Car Rentals"
    },
    {
      "MCC": 7513,
      "Description": "Truck and Utility Trailer Rentals",
      "mccDescription": "Merchants classified with this MCC provide short-term rental or long-term leasing of trucks, vans, or utility trailers.",
      "mccIncluded": "Leasing Services � Trucks, Utility Trailers"
    },
    {
      "MCC": 7519,
      "Description": "Motor Home and Recreational Vehicle Rentals",
      "mccDescription": "Merchants classified with this MCC rent motor homes, RVs, campers, tent trailers, and other recreational vehicles on daily, short-term, or extended-term basis.",
      "mccIncluded": ""
    },
    {
      "MCC": 7523,
      "Description": "Parking Lots, Parking Meters and Garages",
      "mccDescription": "Merchants classified with this MCC provide temporary parking services for automobiles, usually on an hourly, daily, or monthly contract or fee basis.",
      "mccIncluded": "Parking Garages"
    },
    {
      "MCC": 7531,
      "Description": "Automotive Body Repair Shops",
      "mccDescription": "Merchants classified with this MCC perform automotive body repair work and may paint automobiles in conjunction with body repair work.",
      "mccIncluded": "Car Body Repair Shops Auto Body Shops"
    },
    {
      "MCC": 7534,
      "Description": "Tire Retreading and Repair Shops",
      "mccDescription": "Merchants classified with this MCC sell, install, repair, or retread old tires.",
      "mccIncluded": "Car Tires � Retreads Repairs � Tire Retreading, Repair Shops"
    },
    {
      "MCC": 7535,
      "Description": "Automotive Paint Shops",
      "mccDescription": "Merchants classified with this MCC paint, refinish, and restore automobiles.",
      "mccIncluded": "Car Paint Shops"
    },
    {
      "MCC": 7538,
      "Description": "Automotive Service Shops (Non-Dealer)",
      "mccDescription": "Merchants classified with this MCC conduct automotive repairs and general or specific services.",
      "mccIncluded": "Automotive Glass Repair/ Replacement\nCar Service Shops\nLube Stations � Quick Stop Muffler Shops\nOil Changers � Lube Stations\nRepairs � Automotive Service Shops\nTune-Up Shops � Automotive Wheel Alignment,\nBalancing, Repair Service\n� Automotive"
    },
    {
      "MCC": 7542,
      "Description": "Car Washes",
      "mccDescription": "Merchants classified with this MCC wash, wax, and polish automobiles or provide self-service facilities.",
      "mccIncluded": "Auto Detailing Automotive Cleaning, Waxing\nCleaning, Polishing � Automotive"
    },
    {
      "MCC": 7549,
      "Description": "Towing Services",
      "mccDescription": "Merchants classified with this MCC provide vehicle towing services.",
      "mccIncluded": ""
    },
    {
      "MCC": 7622,
      "Description": "Electronics Repair Shops",
      "mccDescription": "Merchants classified with this MCC repair electronics (excluding appliances) such as radios, televisions, stereo equipment, and computers.",
      "mccIncluded": "Radio, Television, Stereo, Electronics Repair Shops\nRepairs � Radio, Television, Stereo, Electronics\nServices � Television, Radio, Stereo, Electronics\nStereo Repair Shops Television Repair Shops"
    },
    {
      "MCC": 7623,
      "Description": "Air Conditioning and Refrigeration Repair Shops",
      "mccDescription": "Merchants classified with this MCC service and repair household and commercial air conditioning and refrigeration systems.",
      "mccIncluded": "Refrigeration Repair Shops Repairs � Air Conditioning, Refrigeration Repair\nShops"
    },
    {
      "MCC": 7629,
      "Description": "Electrical and Small Appliance Repair Shops",
      "mccDescription": "Merchants classified with this MCC service and repair household and commercial electrical components, small appliances (such as microwaves, toaster ovens, and toasters), and electrical office machines, excluding typewriters.",
      "mccIncluded": "Repairs � Office Machines"
    },
    {
      "MCC": 7631,
      "Description": "Watch, Clock and Jewelry Repair",
      "mccDescription": "Merchants classified with this MCC clean and repair all types of watches, clocks, and fine jewelry.",
      "mccIncluded": ""
    },
    {
      "MCC": 7641,
      "Description": "Furniture � Reupholstery, Repair, and Refinishing",
      "mccDescription": "Merchants classified with this MCC reupholster, repair, refinish, and restore furniture.",
      "mccIncluded": "Furniture Repair, Reupholstery\nRefinishing � Furniture Repair, Reupholstery Repairs � Reupholstery, Furniture Repair, Furniture Refinishing Services � Reupholstery,\nFurniture Repair,\nFurniture Refinishing"
    },
    {
      "MCC": 7692,
      "Description": "Welding Services",
      "mccDescription": "Merchants classified with this MCC perform welding services and repair.",
      "mccIncluded": "Services � Welding Repair"
    },
    {
      "MCC": 7699,
      "Description": "Miscellaneous Repair Shops and Related Services",
      "mccDescription": "Merchants classified with this MCC provide service and repair work and are not classified with a more specific MCC.",
      "mccIncluded": "Chimney Cleaning Service Furnace Cleaning Service Machine Shops\nServices � Chimney Cleaning\nServices � Furnace Cleaning\nServices � Large Appliance Repair\nServices � Bicycle Repair Services � Orthopedic Device Repair"
    },
    {
      "MCC": 7800,
      "Description": "Government-Owned Lotteries (US Region only)",
      "mccDescription": "Merchants classified with this MCC are government- owned lotteries in the US Region that are registered with Visa to sell lottery tickets.",
      "mccIncluded": ""
    },
    {
      "MCC": 7801,
      "Description": "Government Licensed On-Line Casinos (On-Line Gambling) (US Region only)",
      "mccDescription": "Merchants classified with this MCC are licensed by a US government entity (and/or sanctioned and regulated by applicable legislation) and registered with Visa to sell virtual gambling chips or offer online gambling.",
      "mccIncluded": ""
    },
    {
      "MCC": 7802,
      "Description": "Government-Licensed Horse/Dog Racing (US Region only)",
      "mccDescription": "Merchants classified with this MCC are licensed by a US government entity (and/or sanctioned and regulated by applicable legislation) and registered with Visa to sell on- or off-track wagers at horse- or dog-racing facilities.",
      "mccIncluded": ""
    },
    {
      "MCC": 7829,
      "Description": "Motion Picture and Video Tape Production and Distribution",
      "mccDescription": "Merchants classified with this MCC are business-to- business or wholesale producers and distributors of educational and industrial films and videos for exhibition and sale.",
      "mccIncluded": "Distributor � Motion Picture, Video Tape Production, Distribution\nEducational, Training Film Production\nFilm Production, Distribution\nMotion Picture, Video Tape Production, Distribution\nTV Commercial Production Training, Educational Film Production\nVideo Tape, Motion Picture Production, Distribution"
    },
    {
      "MCC": 7832,
      "Description": "Motion Picture Theaters",
      "mccDescription": "Merchants classified with this MCC operate movie theaters and sell tickets and refreshments or sell movie tickets online.",
      "mccIncluded": "Cinemas Entertainment � Movie Theaters\nFilms � Movie Theaters Movie Theaters"
    },
    {
      "MCC": 7841,
      "Description": "DVD/Video Tape Rental Stores",
      "mccDescription": "Merchants classified with this MCC rent DVDs/video tapes for personal use and may sell a limited selection of used videos/DVDs and refreshments.",
      "mccIncluded": "Rentals � DVD/Video Tape Stores"
    },
    {
      "MCC": 7911,
      "Description": "Dance Halls, Studios and Schools",
      "mccDescription": "Merchants classified with this MCC operate dance studios, schools, and public dance halls or ballrooms.",
      "mccIncluded": "Ballet Schools\nBallroom Dance Instruction Dance Schools\nSchools � Ballroom Dance Instruction\nStudios � Dance"
    },
    {
      "MCC": 7922,
      "Description": "Ticket Agencies and Theatrical Producers (Except Motion Pictures)",
      "mccDescription": "Merchants classified with this MCC operate live theatrical productions (such as road companies and summer theater groups) and services associated with theatrical productions and concerts (such as casting agencies, booking agencies, scenery, lighting, and other equipment services, and theatrical ticket agencies).",
      "mccIncluded": "Agencies � Ticket, Theatrical Producers\nPerforming Arts Companies � Theatrical Producers � Theatrical\nProduction Agencies � Theatrical\nTicket Agencies"
    },
    {
      "MCC": 7929,
      "Description": "Bands, Orchestras, and Miscellaneous Entertainers (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide entertainment other than theatrical productions (such as musicians, bands, orchestras, and other types of entertainers such as comedians and magicians).",
      "mccIncluded": "DJs\nDance Bands Magicians Music Bands Musicians"
    },
    {
      "MCC": 7932,
      "Description": "Billiard and Pool Establishments",
      "mccDescription": "Merchants classified with this MCC operate establishments that rent the use of pool or billiard tables and other games for entertainment.",
      "mccIncluded": "Billiard Parlors Snooker Parlors"
    },
    {
      "MCC": 7933,
      "Description": "Bowling Alleys",
      "mccDescription": "Merchants classified with this MCC operate bowling alleys for entertainment.",
      "mccIncluded": ""
    },
    {
      "MCC": 7941,
      "Description": "Commercial Sports, Professional Sports Clubs, Athletic Fields, and Sports Promoters",
      "mccDescription": "Merchants classified with this MCC operate and promote semiprofessional and professional athletic clubs (such as baseball, basketball, football, hockey, soccer), promote amateur and professional athletic events, manage individual athletes, and manage sports arenas and stadiums.",
      "mccIncluded": "Arenas � Sports Athletic Fields\nAutomotive Race Tracks\n� Non-Participatory\nClubs � Professional Sports Professional Sports Clubs\nPromoters � Sporting Events\nRace Tracks � Entrance Fee, Non-Betting Transactions\nSports Promoters\nStadiums"
    },
    {
      "MCC": 7991,
      "Description": "Tourist Attractions and Exhibits",
      "mccDescription": "Merchants classified with this MCC operate a variety of tourist attractions and exhibits for entertainment, such as botanical gardens, craft shows, museums, and wineries.",
      "mccIncluded": "Arboretums Art Museums\nBotanical Gardens Exhibits Expositions\nGardens � Botanical Museums\nWineries"
    },
    {
      "MCC": 7992,
      "Description": "Public Golf Courses",
      "mccDescription": "Merchants classified with this MCC operate public golf courses.",
      "mccIncluded": ""
    },
    {
      "MCC": 7993,
      "Description": "Video Amusement Game Supplies",
      "mccDescription": "Merchants classified with this MCC sell coin-operated amusement machines such as video game machines, jukeboxes, pinball machines, slot machines, mechanical games, instant photograph booths, and similar types of amusement equipment and game supplies. Merchants that operate video and game arcades for public use must be classified with MCC 7994 � Video Game Arcades/Establishments.",
      "mccIncluded": ""
    },
    {
      "MCC": 7994,
      "Description": "Video Game Arcades/Establishments",
      "mccDescription": "Merchants classified with this MCC operate arcades, amusement machines (such as video game machines, jukeboxes, pinball machines, mechanical games, instant photograph booths, and similar types of amusement equipment).",
      "mccIncluded": "Social casinos not offering any prizes of monetary value"
    },
    {
      "MCC": 7995,
      "Description": "Betting, including Lottery Tickets, Casino Gaming Chips, Off-Track Betting, Wagers at Race Tracks and games of chance to win prizes of monetary value",
      "mccDescription": "Merchants classified with this MCC operate games of chance to win prizes of monetary value or betting establishments.\nMerchants that are unable to distinguish a betting transaction from other transactions, must identify all transactions with MCC 7995.",
      "mccIncluded": "Betting � Casino Gaming Chips\nBetting � Lottery Tickets Betting � Off-Track Betting � Wagers\nBingo Parlors Raffle Tickets Unlicensed Foreign\nExchange Trading or Binary\nOptions\nSweepstakes"
    },
    {
      "MCC": 7996,
      "Description": "Amusement Parks, Circuses, Carnivals, and Fortune Tellers",
      "mccDescription": "Merchants classified with this MCC operate circuses, carnivals, and amusement and theme parks that offer mechanical rides, snacks and food stands, games, animal exhibits, and entertainment, as well as fortune- telling, astrology, mystic, and tarot card-reading services.",
      "mccIncluded": "County Fairs Fairs Mystics\nTarot Readings State Fairs"
    },
    {
      "MCC": 7997,
      "Description": "Membership Clubs (Sports, Recreation, Athletic), Country Clubs, and Private Golf Courses",
      "mccDescription": "Merchants classified with this MCC operate sports and recreation facilities that require membership such as exercise, athletic and health clubs, country clubs, private golf courses, boating and yacht clubs, swimming clubs, tennis clubs, bowling leagues, riding clubs, shooting and gun clubs, and racquetball clubs, and may also provide spa services",
      "mccIncluded": "Athletic Clubs � Physical Fitness\nBoating Clubs Clubs � Boating Clubs � Gun\nClubs � Racquetball Clubs � Sailing Clubs � Sports Clubs � Tennis Clubs � Yacht Country Clubs Exercise Clubs Fitness Clubs\nGolf Courses � Private Gun Clubs\nHealth Clubs\nPrivate Golf Courses Racquetball Clubs Sailing Clubs\nSports Clubs Tennis Clubs Yacht Clubs"
    },
    {
      "MCC": 7998,
      "Description": "Aquariums, Seaquariums, Dolphinariums, and Zoos",
      "mccDescription": "Merchants classified with this MCC operate zoos or sea life parks for entertainment and education for the general public.",
      "mccIncluded": "Sea Life Parks"
    },
    {
      "MCC": 7999,
      "Description": "Recreation Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide a wide variety of recreation and entertainment services that require active physical participation and that are not classified with a more specific MCC.",
      "mccIncluded": "Aircraft Rentals Ballooning Batting Cages\nBicycle Rentals including electric scooters Fishing/Hunting Licenses\nGames Instruction Golf � Driving Ranges Horseback Riding Instruction � Games Miniature Golf Motorcycle Rentals Parachuting\nPools � Public Swimming Public Swimming Pools\nRecreational Services � Aircraft Rentals\nRecreational Services � Driving Ranges Recreational Services �\nMiniature Golf\nRecreational Services � Public Swimming Pools Recreational Services �\nSki Slopes\nRecreational Services � Sport, Games Instruction\nRentals � Aircraft Resorts � Ski\nRoller Skating Rinks Services � Recreation Shooting Ranges\nSki Slopes Sky Diving\nSports Instruction Swimming Pools � Public"
    },
    {
      "MCC": 8011,
      "Description": "Doctors and Physicians (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are licensed physicians engaged in a practice of general or specialized medicine and surgery that is not classified with a more specific MCC.",
      "mccIncluded": "Cosmetic Surgeons Dermatologists Gynecologists\nMedical Professionals � Doctors\nNeurologists Obstetricians Orthopedists Pediatricians Physicians Plastic Surgeons Psychiatrists Radiologists Surgeons"
    },
    {
      "MCC": 8021,
      "Description": "Dentists and Orthodontists",
      "mccDescription": "Merchants classified with this MCC are licensed physicians that practice general or specialized dentistry such as dental surgery.",
      "mccIncluded": ""
    },
    {
      "MCC": 8031,
      "Description": "Osteopaths",
      "mccDescription": "Merchants classified with this MCC provide therapy involving the non-surgical manipulation and adjustment of bone structures.",
      "mccIncluded": ""
    },
    {
      "MCC": 8041,
      "Description": "Chiropractors",
      "mccDescription": "Merchants classified with this MCC provide therapy in which the spinal column and limbs are manipulated or adjusted.",
      "mccIncluded": ""
    },
    {
      "MCC": 8042,
      "Description": "Optometrists and Ophthalmologists",
      "mccDescription": "Merchants classified with this MCC are licensed physicians that examine and treat eye defects with corrective eyeglasses or contacts or surgical techniques, treat eye diseases, and may fill contact or eyeglass prescriptions and sell eyeglass frames.",
      "mccIncluded": "Eye Doctors"
    },
    {
      "MCC": 8043,
      "Description": "Opticians, Optical Goods, and Eyeglasses",
      "mccDescription": "Merchants classified with this MCC perform eye examinations, write prescriptions, make and sell eyeglasses, and sell other optical goods.",
      "mccIncluded": "Eyeglass Stores"
    },
    {
      "MCC": 8049,
      "Description": "Podiatrists and Chiropodists",
      "mccDescription": "Merchants classified with this MCC are licensed physicians specializing in diagnosing and treating foot ailments.",
      "mccIncluded": "Doctors � Chiropodists, Podiatrists\nFoot Doctors\nMedical Professionals � Podiatrists\nPodiatrists"
    },
    {
      "MCC": 8050,
      "Description": "Nursing, Home Healthcare and Personal Care Facilities",
      "mccDescription": "Merchants classified with this MCC provide inpatient nursing or health-related personal care services and operate convalescent/nursing homes, hospice facilities, offer care in home/residential environment and other long-term care facilities that generally do not offer surgery.",
      "mccIncluded": "Convalescent Homes Elder Housing Hospice Facilities Nursing Homes\nRest Homes Elder Care\nSkilled and Unskilled Home Care"
    },
    {
      "MCC": 8062,
      "Description": "Hospitals",
      "mccDescription": "Merchants classified with this MCC are hospitals that provide diagnostic services, extensive medical treatment, surgery, continuous nursing services, mental and psychiatric care, and other hospital services.",
      "mccIncluded": ""
    },
    {
      "MCC": 8071,
      "Description": "Medical and Dental Laboratories",
      "mccDescription": "Merchants classified with this MCC provide medical and dental laboratory services.",
      "mccIncluded": ""
    },
    {
      "MCC": 8099,
      "Description": "Medical Services and Health Practitioners (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are medical professionals that are not classified with a more specific MCC.",
      "mccIncluded": "Blood Banks  Chemical Dependency Treatment Centers\nFertility Clinics\nHair Replacements � Surgical\nHealth Practitioners, Medical Services\nHearing Testing Services Massage � Therapeutic\nMedical Professionals � Medical Services, Health Practitioners\nMental Health Practitioners Physical Therapists\nPsychiatrists Psychologists\nServices � Medical Services, Health Practitioners\nSports Medicine Clinics\nTherapists"
    },
    {
      "MCC": 8111,
      "Description": "Legal Services and Attorneys",
      "mccDescription": "Merchants classified with this MCC are headed by members of the bar and provide legal advice and services.",
      "mccIncluded": "Attorneys Law Offices Lawyers"
    },
    {
      "MCC": 8211,
      "Description": "Elementary and Secondary Schools",
      "mccDescription": "Merchants classified with this MCC are elementary and secondary schools that provide academic instruction.",
      "mccIncluded": "Grade Schools Grammar Schools High Schools Middle Schools\nSchools � Kindergartens"
    },
    {
      "MCC": 8220,
      "Description": "Colleges, Universities, Professional Schools, and Junior Colleges",
      "mccDescription": "Merchants classified with this MCC are community and junior colleges, universities, theological seminaries and professional schools (such as dental, law, engineering, medical) that provide academic courses and grant academic degrees.",
      "mccIncluded": "Community Colleges Graduate Schools"
    },
    {
      "MCC": 8241,
      "Description": "Correspondence Schools",
      "mccDescription": "Merchants classified with this MCC are correspondence schools that offer educational and instruction through the mail/internet by sending lessons and examinations to the student.",
      "mccIncluded": ""
    },
    {
      "MCC": 8244,
      "Description": "Business and Secretarial Schools",
      "mccDescription": "Merchants classified with this MCC are business and secretarial schools that offer general business training such as, office procedures, word processing, stenographic, and other clerical skills.",
      "mccIncluded": "Computer Training"
    },
    {
      "MCC": 8249,
      "Description": "Vocational and Trade Schools",
      "mccDescription": "Merchants classified with this MCC offer training and instruction in specialized professions such as welding, mechanics, carpentry, real estate, and truck driving.",
      "mccIncluded": "Technical Institutes"
    },
    {
      "MCC": 8299,
      "Description": "Schools and Educational Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC offer educational courses and services that are not classified with a more specific MCC.",
      "mccIncluded": "Art Schools Automobile Driving Instruction\nAviation Instruction Cooking Schools Culinary Instruction\nDriver Education Class, School\nFlying Instruction\nJob Training Services Karate Schools Modeling Schools Schools � Art\nSchools � Automobile Driving Instruction Schools � Cooking\nSchools � Flying Instruction Schools � Karate\nSchools � Modeling Services � Job Training\nServices � Schools, Educational Services Traffic School"
    },
    {
      "MCC": 8351,
      "Description": "Child Care Services",
      "mccDescription": "Merchants classified with this MCC provide care for infants or children.",
      "mccIncluded": "Babysitting Services Children � Day Care Services\nDay Care Services Nannies\nNursery Schools Preschool Centers Day Care Services"
    },
    {
      "MCC": 8398,
      "Description": "Charitable Social Service Organizations",
      "mccDescription": "Merchants classified with this MCC are non-political fund-raising organizations engaged in soliciting charitable donations/contributions on behalf of organizations engaged in social welfare services, or social service organizations engaged in social welfare services.\nThis MCC also includes crowd funding Merchants that accept donations on behalf of individuals raising money for various causes without receiving any perceived or actual financial benefit for doing so.",
      "mccIncluded": "Fund-Raising Organizations\n� Non-Political Organizations � Non- Political\nPublic Radio, Television Crowd Funding Merchants"
    },
    {
      "MCC": 8641,
      "Description": "Civic, Social, and Fraternal Associations",
      "mccDescription": "Merchants classified with this MCC are associations engaged in civic, social, or fraternal activities.",
      "mccIncluded": "Alumni/Alumnae Associations\nAssociations � Civic, Social, Fraternal Associations\nChambers of Commerce Fraternal Associations\nFund-Raising � Political Police Athletic Leagues (PAL)\nSocial Associations, Clubs Veterans� Organizations"
    },
    {
      "MCC": 8651,
      "Description": "Political Organizations",
      "mccDescription": "Merchants classified with this MCC are membership organizations that promote the interests of or raise funds for a national, state, or local political party or candidate.",
      "mccIncluded": "Political Fundraising"
    },
    {
      "MCC": 8661,
      "Description": "Religious Organizations",
      "mccDescription": "Merchants classified with this MCC are religious organizations that provide worship services, religious training or study, or religious activities.",
      "mccIncluded": "Chapels Churches\nMosques Synagogues Temples"
    },
    {
      "MCC": 8675,
      "Description": "Automobile Associations",
      "mccDescription": "Merchants classified with this MCC are automobile associations and clubs that provide a variety of travel- related services to its members.",
      "mccIncluded": "Automobile Club"
    },
    {
      "MCC": 8699,
      "Description": "Membership Organizations (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are organizations or associations that are not classified with a more specific MCC.",
      "mccIncluded": "Art Clubs Historical Clubs Labor Unions\nOrganizations � Membership Organizations\nPoetry Clubs\nProfessional Organizations Tenant Associations\nCondominium Associations HOAs\nUnions � Labor"
    },
    {
      "MCC": 8734,
      "Description": "Testing Laboratories (Non-Medical Testing)",
      "mccDescription": "Merchants classified with this MCC provide testing services (such as automobile testing, calibration and certification, food testing, forensic testing, product and pollution testing, and industrial X-ray inspection services).",
      "mccIncluded": "Food Testing Services Forensic Laboratories Pollution Testing \nProduct Testing Laboratories \nVeterinary Testing Laboratories"
    },
    {
      "MCC": 8911,
      "Description": "Architectural, Engineering, and Surveying Services",
      "mccDescription": "Merchants classified with this MCC provide professional architectural, home and building design, boat design, and engineering, land, water, and aerial surveying services.",
      "mccIncluded": ""
    },
    {
      "MCC": 8931,
      "Description": "Accounting, Auditing, and Bookkeeping Services",
      "mccDescription": "Merchants classified with this MCC provide accounting, bookkeeping, billing and payroll services, and other related auditing services, and may also provide income tax preparation services.",
      "mccIncluded": ""
    },
    {
      "MCC": 8999,
      "Description": "Professional Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC provide professional services that are not classified with a more specific MCC.",
      "mccIncluded": "Appraisers Auction Houses\nBrokers � Mortgage, Loan Court Stenographer Financial Planners Graphic Designers\nGuest Speakers, Lecturers Lecturers\nMarket Research Firms Mortgage Brokers Property Appraisers Public Speakers\nReal Estate Appraisers Research Firms"
    },
    {
      "MCC": 9211,
      "Description": "Court Costs, Including Alimony and Child Support",
      "mccDescription": "Merchants classified with this MCC are local, state, and federal courts of law that administer and process court fees, alimony, and child support payments.",
      "mccIncluded": ""
    },
    {
      "MCC": 9222,
      "Description": "Fines",
      "mccDescription": "Merchants classified with this MCC are government entities that administer and process local, state, and federal fines and penalties, motor vehicle violations, and community- or property-assessed fines.",
      "mccIncluded": "Community Assessed Penalties\nMotor Vehicle Violations"
    },
    {
      "MCC": 9223,
      "Description": "Bail and Bond Payments",
      "mccDescription": "Merchants classified with this MCC post bail to the court system as security for the appearance of the arraigned individual.",
      "mccIncluded": ""
    },
    {
      "MCC": 9311,
      "Description": "Tax Payments",
      "mccDescription": "Merchants classified with this MCC are local, state, and federal entities that engage in financial administration and taxation including collection of taxes and penalties and custody and disbursement of funds.",
      "mccIncluded": "Customs Bureaus"
    },
    {
      "MCC": 9399,
      "Description": "Government Services (Not Elsewhere Classified)",
      "mccDescription": "Merchants classified with this MCC are government entities that provide general support services to government and communities are not classified with a more specific MCC.",
      "mccIncluded": "DMV\nDepartment of Motor Vehicles\nFire Departments Libraries\nParks and Recreation Police Departments"
    },
    {
      "MCC": 9402,
      "Description": "Postal Services � Government Only",
      "mccDescription": "Merchants classified with this MCC are government postal offices that provide a variety of services such as accepting and processing packages and mail for delivery and selling postage stamps.",
      "mccIncluded": ""
    },
    {
      "MCC": 9405,
      "Description": "U.S. Federal Government Agencies or Departments",
      "mccDescription": "Merchants classified with this MCC process U.S. federal government transactions using an acquiring BIN associated with the Intra-Government Transfers (IGOTS) program.",
      "mccIncluded": ""
    },
    {
      "MCC": 9406,
      "Description": "Government-Owned Lotteries (Non-U.S. region)",
      "mccDescription": "Merchants classified with this MCC are government- owned lotteries that sell lottery tickets",
      "mccIncluded": ""
    },
    {
      "MCC": 9702,
      "Description": "Emergency Services (GCAS) (Visa use only)",
      "mccDescription": "This MCC is used only by Visa to enable members and processors to identify emergency transactions from the Global Customer Assistance Services (GCAS).",
      "mccIncluded": ""
    },
    {
      "MCC": 9950,
      "Description": "Intra-Company Purchases",
      "mccDescription": "Merchants classified with this MCC process purchasing card transactions representing internal unit-to-unit transfers of goods and services.",
      "mccIncluded": ""
    }
  ]

const MCCCodeList = () => {
  const [search,setSearch] =useState("")
  const handleSearch = (e)=>{
    setSearch(e.target.value)
  }
    return (  
      <>
      <div className="input-group rounded sticky-sm-top">
        <input type="search" className="form-control rounded" placeholder="Search" name="search" value={search} onChange={(e)=>handleSearch(e)} aria-label="Search" aria-describedby="search-addon" />
        <span className="input-group-text border-0" id="search-addon">
          <i className="fas fa-search"></i>
        </span>
      </div>
      <div className="table-responsive" >
      <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
        <thead className='fw-bolder fs-8 text-gray-800'>
          <tr>
            <th>
              <div className="d-flex">
                <span>MCC Code</span>
              </div>
            </th>
             <th>
              <div className="d-flex">
                <span>Description</span>
              </div>
            </th>
            <th>
              <div className="d-flex">
                <span>MCC Descrption</span>
              </div>
            </th>
            <th>
              <div className="d-flex">
                <span>mccIncluded</span>
              </div>
            </th>         
          </tr>
        </thead>
        <tbody className='fs-8'>
     {Data.filter(item=>{
      if(!search)return true
      if(item.Description.toLowerCase().includes(search.toLocaleLowerCase())||item.mccIncluded.toLowerCase().includes(search.toLocaleLowerCase())||item.mccDescription.toLowerCase().includes(search.toLocaleLowerCase())||item.MCC.toString().toLowerCase().includes(search)){
        return true
      }
     }).map((item, i) => {
                      return (
                        <tr
                          key={"reef_" + i}
                          style={
                            i === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                    <td className="ellipsis">
                            <>
                              {item.MCC}
                            </>
                          </td>
                         
                          <td  style={{minWidth:300,maxWidth:1000,wordWrap:"break-word"}}>
                            <>
                              {item.Description}
                            </>
                          </td>
                          <td  style={{minWidth:300,maxWidth:1000,wordWrap:"break-word"}}>
                            <>
                              {item.mccDescription}
                            </>
                          </td>
                          <td style={{minWidth:300,maxWidth:1000,wordWrap:"break-word"}}>
                            <>
                              {item.mccIncluded}
                            </>
                          </td>
                        </tr>
                      )
                    })}
        </tbody>
      </table>
    </div>
    </>
    );
}
 
export default MCCCodeList;