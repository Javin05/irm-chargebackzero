import React, { useEffect, useState } from 'react'
import { Link, useLocation, useHistory } from 'react-router-dom'
import _ from 'lodash'
import { KTSVG } from '../../theme/helpers'
import { userTypesActions, deleteUsertypeActions, userTypeDetailsActions } from '../../store/actions'
import { connect } from 'react-redux'
import { getUserPermissions } from '../../utils/helper'
import { Can } from '../../theme/layout/components/can'
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  DELETE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from '../../utils/constants'
import { warningAlert, confirmationAlert } from '../../utils/alerts'
import AddUsersType from './userType/add/AddUserType'
const UsersType = (props) => {
  const {
    className,
    getUserTypeDispatch,
    userTypeData,
    loadingGetUType,
    deleteUsertypeMessage,
    deleteUsertypeStatus,
    deleteUsertypeDispatch,
    clearDeleteUsertypeDispatch,
    getUsertypeDetailsDispatch
  } = props

  const url = useLocation().pathname
  const getUsersPermissions = getUserPermissions(url)
  const [show, setShow] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [limit] = useState(25)

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1,
      tag:"IRM"
    }
    getUserTypeDispatch(params)
  }, [])

  // useEffect(() => {
  //   const params = {
  //     limit: limit,
  //     page: 1
  //   }
  //   getUserTypeDispatch(params)
  // }, [limit])

  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_USER_TYPE,
      'warning',
      'Yes',
      'No',
      () => {
        deleteUsertypeDispatch(id)
      },
      () => { }
    )
  }

  const onConfirm = () => { }

  useEffect(() => {
    if (deleteUsertypeStatus === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'Success',
        deleteUsertypeMessage,
        'success',
        'Ok',
        'Cancel',
        () => { onConfirm() },
        () => { }
      )
      clearDeleteUsertypeDispatch()
    } else if (deleteUsertypeStatus === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        deleteUsertypeMessage,
        'error',
        'Ok',
        () => { onConfirm() },
        () => { onConfirm() }
      )
      clearDeleteUsertypeDispatch()
    }
  }, [deleteUsertypeStatus])

  return (
    <>
      <AddUsersType setShow={setShow} show={show} editMode={editMode} />
      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px - 2'>
            <div className='d-flex justify-content-start col-md-6' />
            <div className='d-flex col-md-6 justify-content-end my-auto'>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <div className='my-auto me-3 mt-4'>
                  <button
                    type='button'
                    className='btn btn-sm btn-light-primary font-5vw pull-right'
                    data-toggle='modal'
                    data-target='#addModal'
                    onClick={() => {
                      setShow(true)
                      setEditMode(false)
                    }}
                  >
                    {/* eslint-disable */}
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    {/* eslint-disable */}
                    Add User Type
                  </button>
                </div>
              </Can>
            </div>
          </div>
          <div className='table-responsive'>
            <table className='table table-hover table-rounded table-striped border gs-2 mt-6'>
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={UPDATE_DELETE_PERMISSION}
                  >
                    <th>
                      <div className='d-flex'>
                        <span>Action</span>
                      </div>
                    </th>
                  </Can>
                  <th>
                    <div className='d-flex'>
                      <span>User Type</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loadingGetUType
                    ? (
                      userTypeData &&
                        userTypeData.length > 0
                        ? (
                          userTypeData.map((item, i) => {
                            return (
                              <tr
                                key={i}
                                style={
                                  i === 0
                                    ? { borderColor: 'black' }
                                    : { borderColor: 'white' }
                                }
                              >
                                <Can
                                  permissons={getUsersPermissions}
                                  componentPermissions={UPDATE_DELETE_PERMISSION}
                                >
                                  <td className='pb-0 pt-3 text-start'>
                                    <div className='my-auto d-flex'>
                                      <Can
                                        permissons={getUsersPermissions}
                                        componentPermissions={UPDATE_PERMISSION}
                                      >
                                        <button
                                          className='btn btn-icon btn-bg-light btn-active-color-warning btn-icon-warning btn-sm'
                                          onClick={() => {
                                            setShow(true)
                                            setEditMode(true)
                                            getUsertypeDetailsDispatch(item._id)
                                          }}
                                        >
                                          {/* eslint-disable */}
                                          <KTSVG
                                            path="/media/icons/duotune/art/art005.svg"
                                            className="svg-icon-3"
                                          />
                                          {/* eslint-enable */}
                                        </button>
                                      </Can>
                                      <Can
                                        permissons={getUsersPermissions}
                                        componentPermissions={DELETE_PERMISSION}
                                      >
                                        <button
                                          className='btn btn-icon btn-bg-light btn-active-color-danger btn-icon-danger btn-sm ms-2'
                                          onClick={() => {
                                            onDeleteItem(item._id)
                                          }}
                                        >
                                          {/* eslint-disable */}
                                          <KTSVG
                                            path="/media/icons/duotune/general/gen027.svg"
                                            className="svg-icon-3"
                                          />
                                          {/* eslint-enable */}
                                        </button>
                                      </Can>
                                    </div>
                                  </td>
                                </Can>
                                <td className='ellipsis'>
                                  {item && item.userType
                                    ? _.startCase(item.userType)
                                    : '--'}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    : (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div className='spinner-border text-primary m-5' role='status' />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { usertypeStore, usertypeDeleteStore } = state
  return {
    loadingGetUType:
      usertypeStore && usertypeStore.loadingGetUType ? usertypeStore.loadingGetUType : false,
    userTypeData:
      usertypeStore && usertypeStore.userTypeData ? usertypeStore.userTypeData : {},
    count:
      usertypeStore && usertypeStore.count ? usertypeStore.count : 0,
    deleteUsertypeStatus:
      usertypeDeleteStore && usertypeDeleteStore.deleteUsertypeStatus ? usertypeDeleteStore.deleteUsertypeStatus : '',
    deleteUsertypeMessage:
      usertypeDeleteStore && usertypeDeleteStore.deleteUsertypeMessage ? usertypeDeleteStore.deleteUsertypeMessage : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getUserTypeDispatch: (params) => dispatch(userTypesActions.getUserType(params)),
  deleteUsertypeDispatch: (params) => dispatch(deleteUsertypeActions.deleteUsertype(params)),
  getUsertypeDetailsDispatch: (data) =>
    dispatch(userTypeDetailsActions.getUsertypeDetails(data)),
  clearUsertypeDetailsDispatch: () =>
    dispatch(userTypeDetailsActions.clearUsertypeDetails()),
  clearDeleteUsertypeDispatch: () => dispatch(deleteUsertypeActions.clearDeleteUsertype())
})

export default connect(mapStateToProps, mapDispatchToProps)(UsersType)
