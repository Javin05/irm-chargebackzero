import { USER_MANAGEMENT_ERROR } from '../../utils/constants'

export const userRoleValidation = (values, setErrors) => {
  const errors = {}
  if (!values.role) {
    errors.role = USER_MANAGEMENT_ERROR.USER_ROLE_REQUIRED
  }
  if (!values.userType) {
    errors.userType = USER_MANAGEMENT_ERROR.USER_TYPE_REQUIRED
  }
  setErrors(errors)
  return errors
}

export const userTypeValidation = (values, setErrors) => {
  const errors = {}
  if (!values.userType) {
    errors.userType = USER_MANAGEMENT_ERROR.USER_TYPE_REQUIRED
  }
  setErrors(errors)
  return errors
}
