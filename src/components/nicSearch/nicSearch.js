import moment from 'moment'
import React, { useEffect, useState } from 'react'
import Modal from 'react-bootstrap/Modal'
import ReactPaginate from 'react-paginate'
import { connect } from 'react-redux'
import { SendFeedBackactions, GetClientsActions, clientIdLIstActions, NicSearchactions } from '../../store/actions'
import { toAbsoluteUrl } from '../../theme/helpers'
import ReactSelect from '../../theme/layout/components/ReactSelect'
import { warningAlert } from '../../utils/alerts'
import { STATUS_RESPONSE } from '../../utils/constants'
import color from "../../utils/colors"
import { getLocalStorage } from '../../utils/helper'

const NicSearch = (props) => {
  const {
    loading,
    clientIdDispatch,
    clinetIdLists,
    nicSearchPost,
    nicSearchResponse,
    clearNicSearch
  } = props
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const [search, setSearch] = useState('')
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
  }, [])

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
    }
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const searchNic = () => {
    const payload = {
      clientId: SelectedAsigneesOption ? SelectedAsigneesOption && SelectedAsigneesOption.value : '',
      nic_code: search.trim()
    }
    nicSearchPost(payload)
  }

  useEffect(() => {
    return () => {
      setSearch('')
      setSelectedAsigneesOption('')
    }
  }, [])

  return (
    <>
      {
        Role === 'Admin' ? (
          <>
            <div className="d-flex align-items-center fs-5 fw-bold mb-2">
              Client
            </div>
            <div className='col-md-4 mb-4'>
              <ReactSelect
                styles={customStyles}
                isMulti={false}
                name='AppUserId'
                className='select2'
                classNamePrefix='select'
                handleChangeReactSelect={handleChangeAsignees}
                options={AsigneesOption}
                value={SelectedAsigneesOption}
                isDisabled={!AsigneesOption}
              />
            </div>
          </>
        ) : (
          null
        )
      }
      <div className='row mb-4 justify-content-between align-items-center'>
        <div className='col-md-7'>
          <input
            type="search"
            className="form-control rounded p-3 search-weight"
            placeholder="Search" name="search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            aria-label="Search"
            aria-describedby="search-addon" />
        </div>
        <div className='col-md-5 d-flex'>
          <button
            type="submit"
            className="btn btn-sm btn-primary"
            onClick={(event) => {
              searchNic(event)
            }}
            disabled={loading}
          >
            {!loading && (
              <span className="indicator-label">
                <i className="fas fa-search"></i>
                Search
              </span>
            )}
            {loading && (
              <span
                className="indicator-progress"
                style={{ display: "block" }}
              >
                Please wait...
                <span className="spinner-border spinner-border-sm align-middle ms-2" />
              </span>
            )}
          </button>
          <button
            className="btn btn-sm btn-secondary ms-3"
            onClick={() => {
              setSearch('')
              setSelectedAsigneesOption('')
              clearNicSearch()
            }}
            disabled={loading}
          >
            Reset
          </button>
        </div>
      </div>
      <div className={`card mt-2`}>
        <div className='card-body'>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped gs-2">
              <thead className='fw-bolder fs-8 text-gray-800'>
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Class</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Sub class</span>
                    </div>
                  </th>
                  <th className="min-w-100px text-center">
                    <div className="d-flex">
                      <span>Description</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className='fs-8'>
                {
                  !loading
                    ? (
                      nicSearchResponse &&
                        nicSearchResponse.data
                        ? (
                          nicSearchResponse.data && nicSearchResponse.data.data[0] && nicSearchResponse.data.data[0].nic_code_check && nicSearchResponse.data.data[0].nic_code_check.result && nicSearchResponse.data.data[0].nic_code_check.result.map((nicSearch, i) => {
                            return (
                              <tr
                                key={"reef_" + i}
                                style={
                                  i === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="ellipsis">
                                  <span>
                                    {nicSearch.class ? nicSearch.class : "--"}
                                  </span>
                                </td>
                                <td className="ellipsis">
                                  {
                                    nicSearch.subclass ? nicSearch.subclass : "--"
                                  }
                                </td>
                                <td className="ellipsis">
                                  {nicSearch.description ? nicSearch.description : "--"}
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { NicSearchStore, clinetListStore } = state
  return {
    nicSearchResponse: NicSearchStore && NicSearchStore.nicSearchData ? NicSearchStore.nicSearchData : {},
    loading: NicSearchStore && NicSearchStore.loading ? NicSearchStore.loading : false,
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  nicSearchPost: (data) => dispatch(NicSearchactions.nicSearchPost(data)),
  clearNicSearch: () => dispatch(NicSearchactions.clearNicSearchPost()),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(NicSearch)