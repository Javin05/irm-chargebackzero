import React, { useContext } from "react"
import Map from "../../components/merchant/subComponent/map"
// import Map from "srccomponentsmerchantsubComponentmap.js"
import Accordion from "react-bootstrap/Accordion"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import Card from "react-bootstrap/Card"
import { Row, Col } from "react-bootstrap-v5"
import _ from "lodash"
import MapGoogle from "../maps/MapGoogle";
import StreetMap from "../maps/StreetMap";
import { KycContext } from '../kycDashboard/StaticComponent'

export default function Address(props) {
  const {
    DistanceRes
  } = props

  const userdata = useContext(KycContext)
  const DistanceMapData = userdata && userdata.businessAddress
  const viweLat = DistanceMapData && DistanceMapData.lattitude
  const viweLng = DistanceMapData && DistanceMapData.longitude

  let allMarkers = []
  if (DistanceMapData) {
    allMarkers.push({
      lat: DistanceMapData && DistanceMapData.lattitude,
      lng: DistanceMapData && DistanceMapData.longitude,
      area: "BUSINESS ADDRESS"
    })
  }

  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <h2> Address </h2>{" "}
          <i className="fa fa-times-circle-o" aria-hidden="true"></i>
        </Accordion.Header>
        <Accordion.Body>
          <Card>
            <div className="separator separator-dashed" />

            <Card.Body>
              <div className="row">
                <div className="d-flex justify-content-end mt-4">
                  <span
                    className={`badge badge-sm ${userdata && userdata.businessAddress && userdata.businessAddress.addressVerification === true ? 'badge-success' : 'badge-danger'}`}
                  >
                    {
                      userdata && userdata.businessAddress && userdata.businessAddress.addressVerification === true ? 'Verified' : 'Unverified'
                    }
                  </span>
                </div>
                <div className="col">
                  <>
                    <table className="table table-striped">
                      <tbody>
                        <tr>
                          <th className="fs-4 fw-bold">Company Name</th>
                          <td>{userdata.businessName ? userdata.businessName : '--'}</td>
                        </tr>
                        <tr>
                          <th className="fs-4 fw-bold">Address</th>
                          <td>{userdata.businessAddress && userdata.businessAddress.address ? userdata.businessAddress.address : '--'}</td>
                        </tr>
                        <tr>
                          <th className="fs-4 fw-bold">State</th>
                          <td>{userdata.businessAddress && userdata.businessAddress.state ? userdata.businessAddress.state : '--'}</td>                        </tr><tr>
                          <th className="fs-4 fw-bold">City</th>
                          <td>{userdata.businessAddress && userdata.businessAddress.city ? userdata.businessAddress.city : '--'}</td>
                        </tr><tr>
                          <th className="fs-4 fw-bold">Pincode</th>
                          <td>{userdata.businessAddress && userdata.businessAddress.pincode ? userdata.businessAddress.pincode : '--'}</td>
                        </tr>
                      </tbody>
                    </table>
                  </>
                  {
                    _.isNumber(viweLat && viweLng) ? (
                      <span className="fw-bolder text-muted fs-6 w-120px mr-2 mt-4 mb-4">
                        ADDRESS VERFICATION
                      </span>
                    ) : (
                      null
                    )
                  }
                </div>
                {
                  _.isNumber(viweLat && viweLng) ? (
                    <div className="row">
                      <div className='col-lg-6'>
                        <MapGoogle mapMarkers={allMarkers} viweLat={viweLat} viweLng={viweLng} />
                      </div>
                      <div className="col-lg-6">
                        <div
                        >
                          {<StreetMap mapData={DistanceMapData} />}
                        </div>
                      </div>
                    </div>
                  ) : null
                }
              </div>
            </Card.Body>
          </Card>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  )
}
