import React, { useState, useCallback, useEffect, useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import Modal from 'react-bootstrap/Modal'
import _ from 'lodash'
import { KycContext } from '../kycDashboard/StaticComponent';
import { warningAlert } from "../../utils/alerts"
import { Can } from "../../theme/layout/components/can"
import { UPDATE_PERMISSION } from "../../utils/constants"
import { getUserPermissions } from '../../utils/helper'

export default function AdditionalDocument(props) {

  const userdata = useContext(KycContext)
  const { objectId, UpdateKycDispatch, FullKycResData, FullKycResDataLoading } = props
  const [isViewerOpen, setIsViewerOpen] = useState(false)
  const [editShow, setEditShow] = useState(false)
  const [formData, setFormData] = useState({})
  const [errors, setErrors] = useState({})
  const pathName = '/KYC'
  const getUsersPermissions = getUserPermissions(pathName, true)

  const handleChange = (e, currentIndex) => {
    setErrors({ ...errors, [e.target.name]: '' })
    setFormData((prevData) => {
      return {
        ...prevData,
        items: prevData.items.map((obj, i) =>
          i === currentIndex ? { ...obj, [e.target.name]: e.target.value } : obj
        )
      }
    })
  }

  const handleAddRole = () => {
    setFormData((prevData) => {
      if (prevData.items.length < 10) {
        return {
          ...prevData,
          items: [
            ...prevData.items,
            { label: '', value: '' }
          ]
        }
      } else {
        warningAlert(
          'error',
          "Maximum limit reached. Cannot add more items.",
          '',
          'Try again',
          '',
          () => { }
        )
        return prevData
      }
    })
  }

  const handleRemoveClick = (currentIndex) => {
    setFormData((prevData) => ({
      ...prevData,
      items: prevData.items.filter((_, i) => i !== currentIndex)
    }))
  }

  const handleSubmit = () => {
    if (_.isEmpty(formData.items[0].label) || _.isEmpty(formData.items[0].value)) {
      warningAlert(
        'error',
        "Please enter at least one item",
        '',
        'Try again',
        '',
        () => { }
      )
    } else {
      setErrors({ items: '' })
      const params = {
        additionalFields: (_.isArray(formData.items)) ? formData.items.filter(item => item.label.trim() !== '' || item.value.trim() !== '') : []

      }
      UpdateKycDispatch(objectId, params)
    }
  }

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === "ok") {
      setEditShow(true)
      setFormData((prevData) => ({
        ...prevData,
        items: [{ label: '', value: '' }]
      }))
      setIsViewerOpen(false)
    }

  }, [FullKycResData])

  useEffect(() => {
    if (userdata && userdata.additionalFields && !_.isEmpty(userdata.additionalFields) && !_.isEmpty(userdata.additionalFields[0].label) && !_.isEmpty(userdata.additionalFields[0].value)) {
      setEditShow(true)
      setFormData((prevData) => ({
        ...prevData,
        items: userdata && userdata.additionalFields.map((item) => ({
          label: item.label,
          value: item.value
        }))
      }))
    } else {
      setEditShow(false)
    }
  }, [userdata])

  useEffect(() => {
    if (!editShow) {
      setFormData({
        items: [{
          label: '',
          value: ''
        }]
      })
    }
  }, [editShow])

  const handleEdit = () => {
    setIsViewerOpen(true)
    setFormData((prevData) => ({
      ...prevData,
      items: userdata && userdata.additionalFields.map((item) => ({
        label: item.label,
        value: item.value
      }))
    }))
  }

  return (
    <>
      <Modal
        show={isViewerOpen}
        size="lg"
        centered
        onHide={() => setIsViewerOpen(false)}>
        <Modal.Header
          style={{ backgroundColor: '#5e6278' }}
          closeButton={() => setIsViewerOpen(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Additional Info
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-1" />
            <div className="col-lg-10">
              <div className="card card-custom overlay overflow-hidden">
                <div className="card-body p-0">
                  {
                    formData.items && formData.items.map((item, i) => {
                      return (
                        // <>
                          <div className="row" key={i}>
                            <div className="col-lg-8">
                              <div className="row">
                                <div className="col-lg-6">
                                  <label className='col-form-label col-md-8  text-lg-start'>Label:</label>
                                  <input onChange={(e) => handleChange(e, i)} type="text" name="label" className="form-control form-control-solid"
                                    placeholder="Label"
                                    value={item.label || ''}
                                  />
                                </div>
                                <div className="col-lg-6">
                                  <label className='col-form-label col-md-8  text-lg-start'>Value:</label>
                                  <input onChange={(e) => handleChange(e, i)} type="text" name="value" className="form-control form-control-solid"
                                    placeholder="value"
                                    value={item.value || ''}
                                  />
                                </div>
                                {errors && errors.items && (
                                  <div className="rr mt-1">
                                    <style>{".rr{color:red}"}</style>
                                    {errors.items}
                                  </div>
                                )}
                              </div>
                            </div>
                            <div className="col-lg-4">
                              {
                                i === 0 ? (
                                  null
                                ) : (
                                  <div className='col-md-5 mx-5 d-flex'>
                                    <button
                                      type='button'
                                      className='btn btn-sm btn-danger mt-14 text-lg-start mr-5 me-2'
                                      onClick={() => handleRemoveClick(i)}
                                    >
                                      <span className='indicator-label'>Remove</span>
                                    </button>
                                  </div>
                                )
                              }
                            </div>
                          </div>
                        // </>
                      )
                    })
                  }
                  <div>
                    <div className="row">
                      <div className="col-lg-6 d-flex justify-content-start">
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary mt-14 text-lg-start'
                          onClick={() => handleAddRole()}
                        >
                          <span className='indicator-label'>Add</span>
                        </button>
                      </div>
                      <div className="col-lg-6 d-flex justify-content-end">
                        <button
                          type='button'
                          className='btn btn-sm btn-light-primary mt-14 text-lg-start'
                          onClick={() => handleSubmit()}
                        >
                          {FullKycResDataLoading ? <span className='spinner-border spinner-border-sm align-middle text-primary ms-2' /> :
                            <span className='indicator-label'>Submit</span>}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-1" />
          </div>
        </Modal.Body>
      </Modal>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <h2>Additional Info</h2>{" "}
            <i className="fa fa-times-circle-o" aria-hidden="true"></i>
          </Accordion.Header>
          <Accordion.Body>
            <Card>
              <Card.Body>
                <div className="row mt-4"
                >
                  <Can
                    permissons={getUsersPermissions}
                    componentPermissions={UPDATE_PERMISSION}
                  >
                    <div className="col-lg-12"
                    >
                      <div className="d-flex justify-content-end">
                        <button className="btn btn-sm btn-light-dark"
                          onClick={() => { editShow ? handleEdit() : setIsViewerOpen(true) }}
                        >
                          {
                            editShow ? "Edit" : "Add"
                          }
                        </button>
                      </div>
                    </div>
                  </Can>
                  <div className="col-lg-6 ms-4"
                  >
                    <div className="card mt-4"
                    >
                      <div className="card-body p-0">
                        {
                          userdata && userdata.additionalFields && userdata.additionalFields.map((item, i) => {
                            return (
                              <div className="row mt-4" key={i}
                              >
                                <div className="col-lg-4 fs-5">
                                  <span className="fs-4 fw-bolder me-2">
                                    {
                                      !_.isEmpty(item && item.label) ?
                                        `${item && item.label ? item.label : ''} :`
                                        : ''
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 fs-5">
                                  {
                                    item && item.value
                                  }
                                </div>
                              </div>
                            )
                          })
                        }
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3" />
                </div>
              </Card.Body>
            </Card>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  )
}
