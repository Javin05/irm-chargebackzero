import React, { useEffect, useState, useCallback } from "react"
import { toAbsoluteUrl } from "../../theme/helpers"
import { connect } from "react-redux"
import {
  DasboardAPCAction,
  DasboardAPCtwoAction,
  DashboardDropdownAction
} from "../../store/actions"
import { Link, useLocation } from "react-router-dom"
import Accordion from "react-bootstrap/Accordion"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import _, { values } from 'lodash'
import { Modal } from "../../theme/layout/components/modal/Modal"
import { KTSVG } from "../../theme/helpers"
import AllPages from './all-pages'

function CrossVerify(props) {
  const {
    UserDetails,
    DashboardPANres,
    DashboardAPCDispatch,
    DashboardAPCloading,
    DashboardAPCres,
    APCsecondloading,
    DashboardAPCTwores,
    DashboardAPCTwoDispatch,
    DashboardDropdownDispatch,
    DashBoardDropsownRes,
    clearDashboardAPCTwoDispatch,
    clearDashboardAPCDispatch
  } = props
  const [formData, setFormData] = useState({
    First: '',
    Second: ''
  })
  const [firstvalue, setFirstValue] = useState('')
  const [secondValue, setSecondValue] = useState()
  const [FirstOption, setFirstOption] = useState()
  const [selectedFirstOption, setSelectedFirstOption] = useState('')
  const [SecondOption, setSecondOption] = useState()
  const [selectedSecondOption, setSelectedSecondOption] = useState('')
  const [isViewerOpen, setIsViewerOpen] = useState(false)
  const [currentImage, setCurrentImage] = useState('')
  const [firstDataValue, setFirstDataValue] = useState()
  const [firstDataMrz, setFirstDataMrz] = useState()

  const url = useLocation().pathname
  const fields = url && url.split("/")
  const id = fields && fields[3]

  useEffect(() => {
    DashboardDropdownDispatch(id)
  }, [])

  const firstData = DashboardAPCres && DashboardAPCres.data
  const secondData = DashboardAPCTwores && DashboardAPCTwores.data
  let firstOcr = []
  let firstmrz = []
  let secondOcr = []
  let secondMrz = []

  const ocrLabel = !_.isEmpty(firstData && firstData.ocrFields) ? Object.keys(firstData && firstData.ocrFields) : '--'
  const ocrValue = !_.isEmpty(firstData && firstData.ocrFields) ? Object.values(firstData && firstData.ocrFields) : '--'

  const mrzLabel = !_.isEmpty(firstData && firstData.mrzFields) ? Object.keys(firstData && firstData.mrzFields) : '--'
  const mrzValue = !_.isEmpty(firstData && firstData.mrzFields) ? Object.values(firstData && firstData.mrzFields) : '--'

  const ocrLabel1 = !_.isEmpty(secondData && secondData.ocrFields) ? Object.keys(secondData && secondData.ocrFields) : '--'
  const ocrValue1 = !_.isEmpty(secondData && secondData.ocrFields) ? Object.values(secondData && secondData.ocrFields) : '--'

  const mrzLabel1 = !_.isEmpty(secondData && secondData.mrzFields) ? Object.keys(secondData && secondData.mrzFields) : '--'
  const mrzValue1 = !_.isEmpty(secondData && secondData.mrzFields) ? Object.values(secondData && secondData.mrzFields) : '--'

  _.isArray(ocrLabel) && ocrLabel.forEach((label, index) => {
    const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    firstOcr.push({ label: formattedLabel, value: ocrValue[index] })
  })

  _.isArray(mrzLabel) && mrzLabel.forEach((label, index) => {
    const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    firstmrz.push({ label: formattedLabel, value: mrzValue[index] })
  })

  _.isArray(ocrLabel1) && ocrLabel1.forEach((label, index) => {
    const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    secondOcr.push({ label: formattedLabel, value: ocrValue1[index] })
  })
  _.isArray(mrzLabel1) && mrzLabel1.forEach((label, index) => {
    const formattedLabel = label.replaceAll("_", " ").toLowerCase().split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    secondMrz.push({ label: formattedLabel, value: mrzValue1[index] })
  })

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      if (rawData && rawData[item].updatedDocument === "Yes") {
        if (!_.isUndefined(rawData[item].documentName)) {
          defaultOptions.push({ label: rawData[item].documentName, value: rawData[item]._id })
        }
      }
    }
    return _.sortBy(defaultOptions, 'label')
  }

  useEffect(() => {
    const FirstDropDown = DashBoardDropsownRes && DashBoardDropsownRes.data ? DashBoardDropsownRes.data : ''
    if (FirstDropDown) {
      const First = getDefaultOptions(FirstDropDown)
      setFirstOption(First)
      if (!_.isEmpty(formData.First)) {
        const selOption = _.filter(First, function (x) { if (_.includes(formData.First._id, x.value)) { return x } })
        setSelectedFirstOption(selOption)
      }
    }
  }, [DashBoardDropsownRes])

  const handleChangeFirst = selectedOption => {
    if (selectedOption !== null) {
      setSelectedFirstOption(selectedOption)
      setFormData(values => ({ ...values, First: selectedOption.label }))
      if (selectedOption.value) {
        setFirstValue(selectedOption.value)
        const params = {
          name: selectedOption.value
        }
        DashboardAPCDispatch(selectedOption.value, params)
      }
    } else {
      setSelectedFirstOption()
      clearDashboardAPCDispatch()
      setFormData(values => ({ ...values, First: '' }))
    }
  }

  useEffect(() => {
    const SecondDropDown = DashBoardDropsownRes && DashBoardDropsownRes.data ? DashBoardDropsownRes.data : ''
    if (DashBoardDropsownRes) {
      const Second = getDefaultOptions(SecondDropDown)
      setSecondOption(Second)
      if (!_.isEmpty(formData.Second)) {
        const selOption = _.filter(Second, function (x) { if (_.includes(formData.Second._id, x.value)) { return x } })
        setSelectedSecondOption(selOption)
      }
    }
  }, [DashBoardDropsownRes])

  const handleChangeSecond = selectedOption => {
    if (selectedOption !== null) {
      setSelectedSecondOption(selectedOption)
      setFormData(values => ({ ...values, Second: selectedOption.label }))
      if (selectedOption.value) {
        setSecondValue(selectedOption.value)
        const params = {
          name: selectedOption.value
        }
        DashboardAPCTwoDispatch(selectedOption.value, params)
      }
    } else {
      setSelectedSecondOption()
      clearDashboardAPCTwoDispatch()
      setFormData(values => ({ ...values, Second: '' }))
    }
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index)
    setIsViewerOpen(true)
  }, [])

  const downloadFiles = (data, fileName) => {
    const link = document.createElement("a")
    const url = window.URL || window.webkitURL
    const revokeUrlAfterSec = 1000000
    link.href = data
    link.target = "_blank"
    link.download = fileName
    document.body.append(link)
    link.click()
    link.remove()
    setTimeout(() => url.revokeObjectURL(link.download), revokeUrlAfterSec)
  }

  return (
    <>
      <Modal showModal={isViewerOpen} modalWidth={1000}>
        <div
          className="modal-dialog modal-dialog-centered"
        >
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="me-8">
              </h2>
              <button
                type="button"
                className="btn btn-lg btn-icon btn-active-light-danger close me-2 mt-2"
                data-dismiss="modal"
                onClick={() => {
                  setIsViewerOpen(false)
                }}
              >
                {/* eslint-disable */}
                <KTSVG
                  path="/media/icons/duotune/arrows/arr061.svg"
                  className="svg-icon-1"
                />
                {/* eslint-disable */}
              </button>
            </div>
            <div className="container-fixed">
              <div className="card-header">
                <div className="card-body mt-4 mb-8">
                  <div className="row">
                    <div className="col-lg-1" />
                    <div className="col-lg-10">
                      <div className="card card-custom overlay overflow-hidden">
                        <div className="card-body p-0">
                          <div className="overlay-wrapper">
                            <img
                              src={currentImage}
                              alt=""
                              className="w-100 rounded"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-1" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <h2> Cross Verification</h2>{" "}
            <i className="fa fa-times-circle-o" aria-hidden="true"></i>
          </Accordion.Header>
          <Accordion.Body>
            <div>
              <div className="card">
                <div className="card-body mt-8 mb-8">
                  <div className="row">
                    <div className="col-lg-5">
                      <ReactSelect
                        isClearable
                        styles={customStyles}
                        isMulti={false}
                        name='First'
                        placeholder="Select..."
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeFirst}
                        options={FirstOption}
                        value={selectedFirstOption}
                        isDisabled={!FirstOption}
                      />
                      {
                        DashboardAPCloading ? (
                          <div className='d-flex justify-content-center'>
                            <div
                              className='spinner-border text-primary m-5 mt-8'
                              role='status'
                            />
                          </div>
                        ) : (
                          DashboardAPCres && !_.isEmpty(DashboardAPCres.data) ? (
                            <>
                              <div className="row mt-4"
                                style={{
                                  backgroundColor: 'aliceblue',
                                  borderRadius: '10px'
                                }}
                              >
                                <div className="col-lg-11 ms-5">
                                  <div className="card card-custom overlay overflow-hidden card-img mt-4 mb-4">
                                    <div className="card-body p-0">
                                      <>
                                        <div className="overlay-wrapper">
                                          <img
                                            src={
                                              DashboardAPCres.data &&
                                                DashboardAPCres.data.uploadDocument ? DashboardAPCres.data.uploadDocument :
                                                toAbsoluteUrl('/media/logos/no-image.png')
                                            }
                                            className="w-100 rounded"
                                          />
                                        </div>
                                        <div className="overlay-layer bg-dark bg-opacity-10">
                                          <a className="btn btn-light-primary btn-shadow"
                                            onClick={() => openImageViewer(
                                              DashboardAPCres.data &&
                                              DashboardAPCres.data.uploadDocument)}
                                          >
                                            View
                                          </a>
                                        </div>
                                      </>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              {
                                !_.isEmpty(DashboardAPCres && DashboardAPCres.data && DashboardAPCres.data.ocrFields) ? (
                                  <div className="row">
                                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                      <h4>OCR Field</h4>
                                      {
                                        _.isArray(firstOcr) ? firstOcr.map((data) => {
                                          return (
                                            <div className="row">
                                              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div className="fs-6 fw-bold mb-2 w-100">
                                                  {
                                                    data && data.label ? `${data.label} :` : '--'
                                                  }
                                                </div>
                                              </div>
                                              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                  {
                                                    data && data.value ? data.value.toString() : '--'
                                                  }
                                                </div>
                                              </div>
                                            </div>
                                          )
                                        }) : null
                                      }
                                    </div>
                                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                      <h4>MRZ Field</h4>
                                      {
                                        _.isArray(firstmrz) ? firstmrz.map((data) => {
                                          return (
                                            <div className="row">
                                              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div className="fs-6 fw-bold mb-2 w-100">
                                                  {
                                                    data && data.label ? `${data.label} :` : '--'
                                                  }
                                                </div>
                                              </div>
                                              <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                  {
                                                    data && data.value ? data.value.toString() : '--'
                                                  }
                                                </div>
                                              </div>
                                            </div>
                                          )
                                        }) : null
                                      }
                                    </div>
                                  </div>
                                ) : (
                                  null
                                )
                              }
                            </>
                          ) : (
                            DashboardAPCres && DashboardAPCres.status === 'error' ?
                              <div className="text-muted fs-4 mt-4"> No Records Found</div> : null
                          )
                        )
                      }
                    </div>
                    <div className="col-lg-2" />
                    <div className="col-lg-5">
                      <ReactSelect
                        isClearable
                        styles={customStyles}
                        isMulti={false}
                        name='Second'
                        placeholder="Select..."
                        className="basic-single"
                        classNamePrefix="select"
                        handleChangeReactSelect={handleChangeSecond}
                        options={SecondOption}
                        value={selectedSecondOption}
                        isDisabled={!SecondOption}
                      />

                      {
                        APCsecondloading ? (
                          <div className='d-flex justify-content-center'>
                            <div
                              className='spinner-border text-primary m-5 mt-8'
                              role='status'
                            />
                          </div>
                        ) : (

                          DashboardAPCTwores && !_.isEmpty(DashboardAPCTwores.data) ? (
                            <>
                              <div className="row mt-4"
                                style={{
                                  backgroundColor: 'aliceblue',
                                  borderRadius: '10px'
                                }}
                              >
                                <div className="col-lg-11 ms-5">
                                  <div className="card card-custom overlay overflow-hidden card-img mt-4 mb-4">
                                    <div className="card-body p-0">
                                      <>
                                        <div className="overlay-wrapper">
                                          <img
                                            src={
                                              DashboardAPCTwores.data &&
                                                DashboardAPCTwores.data.uploadDocument ? DashboardAPCTwores.data.uploadDocument :
                                                toAbsoluteUrl('/media/logos/no-image.png')
                                            }
                                            className="w-100 rounded"
                                          />
                                        </div>
                                        <div className="overlay-layer bg-dark bg-opacity-10">
                                          <a className="btn btn-light-primary btn-shadow"
                                            onClick={() => openImageViewer(
                                              DashboardAPCTwores.data &&
                                              DashboardAPCTwores.data.uploadDocument)}
                                          >
                                            View
                                          </a>
                                        </div>
                                      </>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              {
                                !_.isEmpty(DashboardAPCTwores && DashboardAPCTwores.data && DashboardAPCTwores.data.mrzFields) ? (
                                  <div className="row">
                                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                    <h4>OCR Field</h4>
                                    {
                                      _.isArray(secondOcr) ? secondOcr.map((data) => {
                                        return (
                                          <div className="row">
                                            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                              <div className="fs-6 fw-bold mb-2 w-100">
                                                {
                                                  data && data.label ? `${data.label} :` : '--'
                                                }
                                              </div>
                                            </div>
                                            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                              <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                {
                                                  data && data.value ? data.value.toString() : '--'
                                                }
                                              </div>
                                            </div>
                                          </div>
                                        )
                                      }) : null
                                    }
                                  </div>
                                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-4">
                                    <h4>MRZ Field</h4>
                                    {
                                      _.isArray(secondMrz) ? secondMrz.map((data) => {
                                        return (
                                          <div className="row">
                                            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                              <div className="fs-6 fw-bold mb-2 w-100">
                                                {
                                                  data && data.label ? `${data.label} :` : '--'
                                                }
                                              </div>
                                            </div>
                                            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                              <div className="fs-6 fw-bold mb-2 w-100 text-muted">
                                                {
                                                  data && data.value ? data.value.toString() : '--'
                                                }
                                              </div>
                                            </div>
                                          </div>
                                        )
                                      }) : null
                                    }
                                  </div>
                                </div>
                                ) : (
                                  null
                                )
                              }
                            </>
                          ) : DashboardAPCTwores && DashboardAPCTwores.status === 'error' ?
                            <div className="text-muted fs-4 mt-4"> No Records Found</div> : null
                        )
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  )
}

const mapStateToProps = (state) => {
  const { DashBoardAPCStore, DashBoardAPCTwoStore, DashboardDropdownStore } = state
  return {
    DashboardAPCres: DashBoardAPCStore && DashBoardAPCStore.DashboardAPCres ? DashBoardAPCStore.DashboardAPCres : {},
    DashboardAPCloading: DashBoardAPCStore && DashBoardAPCStore.loading ? DashBoardAPCStore.loading : false,
    APCsecondloading: DashBoardAPCTwoStore && DashBoardAPCTwoStore.loading ? DashBoardAPCTwoStore.loading : false,
    DashboardAPCTwores: DashBoardAPCTwoStore && DashBoardAPCTwoStore.DashboardAPCTwores ? DashBoardAPCTwoStore.DashboardAPCTwores : {},
    DashBoardDropsownRes: DashboardDropdownStore && DashboardDropdownStore.DashBoardDropsownRes ? DashboardDropdownStore.DashBoardDropsownRes : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  DashboardAPCDispatch: (id, params) => dispatch(DasboardAPCAction.DashboardAPC(id, params)),
  clearDashboardAPCDispatch: (id, params) => dispatch(DasboardAPCAction.clearDashboardAPC(id, params)),
  DashboardAPCTwoDispatch: (id, params) => dispatch(DasboardAPCtwoAction.DashboardAPCTwo(id, params)),
  clearDashboardAPCTwoDispatch: (id, params) => dispatch(DasboardAPCtwoAction.clearDashboardAPCTwo(id, params)),
  DashboardDropdownDispatch: (id) => dispatch(DashboardDropdownAction.DashboardDropdown(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(CrossVerify)