
import React, { useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import moment from 'moment'
import { KycContext } from '../kycDashboard/StaticComponent'

export default function BusinessData() {
  const userdata = useContext(KycContext)
  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <h2> Business Details</h2>{" "}
          <i className="fa fa-times-circle-o" aria-hidden="true"></i>
        </Accordion.Header>
        <Accordion.Body>
          <Card>
            <Card.Body>
              <div
                className="row block_div"
              >
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  {/* <legend className="float-none w-auto fs-3 p-2 h6 font-weight-600 ms-2 mt-4">
                    Business Information
                  </legend> */}
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Name :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.businessName ? userdata.businessName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Type :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.entityName ? userdata.entityName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Cateogry :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.businessCategory ? userdata.businessCategory : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Sub Cateogry :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.businessSubCategory ? userdata.businessSubCategory : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Website :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.website ? userdata.website : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-2">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Address :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.businessAddress && userdata.businessAddress.address ? userdata.businessAddress.address : '--'},{userdata && userdata.businessAddress && userdata.businessAddress.city},{userdata && userdata.businessAddress && userdata.businessAddress.state},{userdata && userdata.businessAddress && userdata.businessAddress.pincode},
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Business Description :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.businessDescription ? userdata.businessDescription : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Business Commencement Date :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.businessStartDate ? moment(userdata && userdata.businessStartDate ? userdata.businessStartDate : "--").format('DD/MM/YYYY') : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Number of years in business :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <span className="fw-bolder fs-6">
                        {userdata && userdata.noOfYearsInBusiness ? userdata.noOfYearsInBusiness : '--'}
                        (commenced at : {userdata && userdata.commenceDate ? moment(userdata && userdata.commenceDate ?userdata.commenceDate:'--').format('DD/MM/YYYY') : '--'})
                      </span>
                    </div>
                  </div>

                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Annual Turnover Range :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.annualTurnOverRange ? userdata.annualTurnOverRange : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Actual Annual Turnover :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.actualAnnualTurnOver ? `${userdata.actualAnnualTurnOver}${userdata.actualAnnualTurnOverRange}` : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Expected Monthly Card Turnover :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.exceptedMonthlyCardturnOver ? `${userdata.exceptedMonthlyCardturnOver}${userdata.exceptedMonthlyCardturnOverRange}` : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row mb-4">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Expected Monthly Transaction :
                      </div>
                    </div>
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <div className="fw-bolder fs-6">
                        {userdata && userdata.exceptedMonthlyTransaction ? `${userdata.exceptedMonthlyTransaction}${userdata.exceptedMonthlyTransactionRange}` : '--'}
                      </div>
                    </div>
                  </div>


                </div>
              </div>
            </Card.Body>
          </Card>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
