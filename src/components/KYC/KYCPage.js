import React, { useEffect, useState } from "react"
import { Link, useLocation } from "react-router-dom"
import _, { values } from "lodash"
import { KTSVG } from "../../theme/helpers"
import { KYCActions, clientCredFilterActions, kycStatusAction, ExportReportValueAction } from "../../store/actions"
import { connect } from "react-redux"
import SearchList from "./searchList"
import ReactPaginate from "react-paginate"
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { unsetLocalStorage, getLocalStorage } from "../../utils/helper"
import {
  KYC_STATUS,
  KYC_TYPE_STATUS,
  SET_FILTER,
  KYC_SCORE_STATUS,
  CREATE_PERMISSION,
  UPDATE_PERMISSION,
  UPDATE_DELETE_PERMISSION
} from "../../utils/constants"
import KYCAdd from "./Add"
import KYCminiAdd from "./mini-KYC/Add"
import EditKYC from "./editKyc/edit"
import moment from "moment"
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'
import Status from './ARH'
import { warningAlert } from "../../utils/alerts"

function KYCList(props) {
  const {
    getKYClistDispatch,
    getKYCUserDetailsDispatch,
    className,
    KYClists,
    loading,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction,
    kycStatusDispatch,
    kycStatusRes,
    kycStatusloading,
    kycExportReportDispatch,
    kycExportReportRes,
    clearKycExportReportDispatch,
    kycExportReportResloading
  } = props
  const location = useLocation()
  const didMount = React.useRef(false)
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({})
  const [exportButton, setExportButton] = useState(false)
  const [activePageNumber, setActivePageNumber] = useState(1)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("/")
  const currentRoute = url && url[1]
  const getUsersPermissions = getUserPermissions(pathName, true)
  const [routeShow, setRouteShow] = useState()
  const [show, setShow] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [activeKyc, setActiveKyc] = useState({})
  const [userId, setuserId] = useState('')
  const [completedSteps, setCompletedSteps] = useState([-1])
  const [formData, setFormData] = useState([])
  const statusData = kycStatusRes && kycStatusRes.data
  const [sorting, setSorting] = useState({
    deviceID: false,
    riskid: false,
    phone: false,
    email: false,
    ipAddress: false,
    address: false,
    status: false,
    firstName: false,
    lastName: false,
    companyName: false,
    comapanyEmail: false,
    clientId: false
  })

  useEffect(() => {
    if (location.state) {
      const data = location.state
      const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
      const credBasedParams = {
        clientId: ClinetId ? ClinetId : ''
      }
      const params = {
        limit: limit,
        page: 1,
        ...credBasedParams,
        ...searchParams,
      }
      Object.assign(params, data)
      const pickByParams = _.pickBy(params);
      getKYClistDispatch(pickByParams)
      kycStatusDispatch(credBasedParams)
    } else {
      const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
      const credBasedParams = {
        clientId: ClinetId ? ClinetId : ''
      }
      const params = {
        limit: limit,
        page: 1,
        ...credBasedParams,
        ...searchParams,
      }
      const pickByParams = _.pickBy(params);
      getKYClistDispatch(pickByParams)
      kycStatusDispatch(credBasedParams)
    }
  }, [location.state])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      setActivePageNumber(1);
      const credBasedParams = {
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      }
      const params = {
        limit: limit,
        page: 1,
        ...searchParams,
        ...credBasedParams,
      };
      const pickByParams = _.pickBy(params);
      getKYClistDispatch(pickByParams)
      kycStatusDispatch(pickByParams)
      setFilterFunctionDispatch(false);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue ? credBasedClientValue : ''
    }
    const params = {
      limit: value,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params);
    getKYClistDispatch(pickByParams)
    kycStatusDispatch(pickByParams)
    setActivePageNumber(1);
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const data = location.state
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue ? credBasedClientValue : ''
    }
    const params = {
      ...searchParams,
      limit: limit,
      page: pageNumber,
      ...credBasedParams,
    }
    Object.assign(params, data)
    const pickByParams = _.pickBy(params);
    setActivePageNumber(pageNumber)
    getKYClistDispatch(pickByParams)
    kycStatusDispatch(credBasedParams)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "DESC",
      }
      getKYClistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "ASC",
      }
      getKYClistDispatch(params)
    }
  }

  const totalPages =
    KYClists && KYClists.count
      ? Math.ceil(parseInt(KYClists && KYClists.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = "/merchant-login"
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  const ActiveFullKyc = (name, id, businessType) => {
    // setActiveKyc(name,id)
    const params = {
      kycName: name,
      kycId: id,
      organizationName: businessType
    }
    setActiveKyc((values) => ({ ...values, kycData: params }))
  }

  const handleChange = (e) => {
    e.persist()
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value])
    } else if (e.target.checked === false) {
      let freshArray = formData.filter(val => val !== e.target.value)
      setFormData([...freshArray])
    }
  }

  useEffect(() => {
    if (kycExportReportRes && kycExportReportRes.status === 'ok') {
      if (Array.isArray(kycExportReportRes && kycExportReportRes.data)) {
        const closeXlsx = document.getElementById('kycCsvReport')
        closeXlsx.click()
        clearKycExportReportDispatch()
      } else if (kycExportReportRes && kycExportReportRes.status === 'error') {
        warningAlert(
          'error',
          kycExportReportRes && kycExportReportRes.message,
          '',
          'Try again',
          '',
          () => { }
        )
        clearKycExportReportDispatch()
      }
    } else if (kycExportReportRes && kycExportReportRes.status === 'error') {
      warningAlert(
        'error',
        kycExportReportRes && kycExportReportRes.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearKycExportReportDispatch()
    }
  }, [kycExportReportRes])

  const handleExportReport = () => {
    delete searchParams.limit
    delete searchParams.page
    kycExportReportDispatch(searchParams)
  }

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-target='#kycCsvReport'
      >
        <ReactHTMLTableToExcel
          id="kycCsvReport"
          className="download-table-xls-button"
          table="kycCsvModel"
          filename="Kyc-report"
          sheet="tablexls"
        />
      </div>
      <EditKYC editMode={editMode} userId={userId} setEditMode={setEditMode} setuserId={setuserId} />
      {
        editMode === false ?
          <KYCminiAdd setShow={setShow} show={show} editMode={editMode} activeKyc={activeKyc} setActiveKyc={setActiveKyc} />
          : null
      }
      <div className={`card ${className}`}>
        <div className="card-body py-3  mt-8 mb-8">
          <div className="d-flex  px - 2">
            <div className="d-flex justify-content-between align-items-center col-md-5">
              <div className="col-md-3 ms-2">
                {KYClists &&
                  KYClists?.count && (
                    <span className="text-muted fw-bold d-flex fs-3">
                      Total: {" "}
                      <span className="text-gray-700 fw-bolder text-hover-primary fs-3 ms-1">
                        {KYClists && KYClists.count}
                      </span>
                    </span>
                  )}
              </div>
              <div className='col-md-7 d-flex align-items-center'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="d-flex col-md-7 align-items-center justify-content-end">
            <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
              <div className="me-2">
                {
                  !_.isEmpty(formData) ? (
                    <Status formData={formData} setFormData={setFormData} activePageNumber={activePageNumber} limit={limit}/>
                  ) : null
                }
              </div>
              {exportButton ?
                <div className="">
                  <button
                    type='button'
                    className='btn btn-sm btn-light-success btn-responsive font-5vw me-3 pull-right ms-2'
                    onClick={handleExportReport}
                    disabled={kycExportReportResloading}
                  >
                    {!kycExportReportResloading && (
                      <span className="indicator-label">
                        <i className="bi bi-filetype-csv" />
                        Export Report
                      </span>
                    )}
                    {kycExportReportResloading && (
                      <span
                        className="indicator-progress"
                        style={{ display: "block" }}
                      >
                        Please wait...
                        <span className="spinner-border spinner-border-sm align-middle ms-2" />
                      </span>
                    )}
                  </button>
                </div> : null}
                </Can>
              <div className="">
                <SearchList setSearchParams={setSearchParams} limit={limit} activePageNumber={activePageNumber} setExportButton={setExportButton} />
              </div>
              <Can
                permissons={getUsersPermissions}
                componentPermissions={CREATE_PERMISSION}
              >
                <div className=" me-3">
                  <button
                    type="button"
                    className="btn btn-sm btn-light-primary font-5vw pull-right"
                    data-toggle="modal"
                    data-target="#addModal"
                    onClick={() => {
                      setShow(true)
                      setEditMode(false)
                    }}
                  >
                    <KTSVG path="/media/icons/duotune/arrows/arr087.svg" />
                    Add Merchant
                  </button>
                </div>
              </Can>
            </div>
          </div>

          {
            !kycStatusloading ? (
              <div className='row'>
                <div className='col-md-4 col-lg-4 col-sm-4 ms-4'>
                  <div className='row'>
                    <div className='text-black-700 fw-bolder'>
                      STATUS
                    </div>
                    <div className='col-md-12 mt-4'>
                      <span className='fw-bold mt-2 fs-8 me-4 text-warning'>
                        PENDING
                        <span className=' text-hover-primary'>
                          - {statusData && statusData.pendingCount ? statusData.pendingCount : '0'}
                        </span>
                      </span>
                      <span className='fw-bold mt-2 fs-8 me-4 text-success'>
                        APPROVED
                        <span className=' text-hover-primary'>
                          - {statusData && statusData.approvedCount ? statusData.approvedCount : '0'}
                        </span>
                      </span>
                      <span className='text-danger fw-bold mt-2 fs-8 me-4'>
                        REJECTED
                        <span className='text-hover-primary'>
                          - {statusData && statusData.rejectCount ? statusData.rejectCount : '0'}
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className='col-md-12 d-flex justify-content-center'>
                <div
                  className='spinner-border text-success'
                  role='status'
                />
              </div>
            )
          }
        </div>
      </div>
      {!loading ? (
        !_.isEmpty(KYClists.data) ? (
          KYClists &&
          KYClists.data.map((item, i) => {
            const indiaDate = moment.utc(item && item.createdAt).local()
            const todayDate = moment()
            const duration = moment.duration(todayDate.diff(indiaDate))
            const hours = Math.floor(duration.asHours());
            const minutes = duration.minutes();
            return (
              <div className="card mt-6" key={i}>
                <div className="card-body">
                  <div className="row mt-4">
                    <div className="col-lg-8 col-md-8 col-sm-8">
                      <div className='row'>
                        <div className="col-lg-2 col-md-2 col-sm-2 ms-3">
                          <div className="text-muted fs-8 fw-bolder">
                            Action
                          </div>
                          <div className="fs-8 fw-bolder min-width-150px text-center form-check form-check-custom form-check-success form-check-solid mb-2">
                            <input
                              className="form-check-input cursor-pointer "
                              type="Checkbox"
                              value={item._id}
                              onChange={(e) => handleChange(e)}
                              checked={formData && formData.includes(item._id)}
                              name='checkName'
                            />
                          </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-3">
                          <div className="text-muted fs-8 fw-bolder">
                            Business Name
                          </div>
                          <div className="fs-8 fw-bolder">
                            {item.businessName
                              ? item.businessName
                              : "--"}
                          </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-3">
                          <div className="text-muted fs-8 fw-bolder">
                            Phone
                          </div>
                          <div className="fs-8 fw-bolder">
                            {item && item.phoneNumber && item.phoneNumber.number
                              ? item.phoneNumber.number
                              : "--"}
                          </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-3">
                          <div className="text-muted fs-8 fw-bolder">
                            Email
                          </div>
                          <div className="fs-8 fw-bolder">
                            {item && item.emailId && item.emailId.emailId
                              ? item.emailId.emailId
                              : "--"}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-4">
                      <div className="row">
                        <div className="col-lg-3">
                          <div className="text-muted fs-8 fw-bolder">
                            Case#
                          </div>
                          <div className="fs-8 fw-bolder ellipsis">
                            <Link to={`/static-summary/update/${item._id}`}>
                              KYC{item.kycId ? item.kycId : "--"}
                            </Link>
                          </div>
                        </div>
                        {/* <div className="col-lg-3">
                            <div className="text-muted fs-8 fw-bolder">
                              Client
                            </div>
                            <div className="fs-8 fw-bolder">
                              {item && item.clientId && item.clientId.company ? item.clientId.company : '--'}
                            </div>
                          </div> */}
                        <div className="col-lg-3">
                          <span
                            className={`ms-2 badge ${KYC_TYPE_STATUS[item && item.businessType]}`}
                          >
                            {item.businessType}
                          </span>
                        </div>
                        <div className="col-lg-4">
                          <div className="text-muted fs-8 fw-bolder">
                            Created
                          </div>
                          <div className="fs-8 fw-bolder">
                            {moment(
                              item.createdAt ? item.createdAt : "--"
                            ).format("DD/MM/YYYY")}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="separator separator-dashed border-secondary" />
                  <div className="row mx-1">
                    <div className="col-sm-6 col-md-6 col-lg-6 mt-4 mb-4">
                      <span className="fs-5 fw-bolder">
                        eKYC
                      </span>
                      {
                        item && item.phoneNumberVerification === "YES" &&
                          <span className="ms-4 fs-7">
                            <i className={`ms-2 me-1 ${KYC_SCORE_STATUS[item && item.phoneNumber && item.phoneNumber.verified]}`}
                              style={{
                                backgroundColor: "transparent",
                                fontSize: "0.95rem"
                              }}
                            />
                            Phone
                          </span>
                      }
                      {
                        item && item.emailIdVerification === "YES" &&
                          <span className="ms-4 fs-7">
                            <i className={`ms-2 me-1 ${KYC_SCORE_STATUS[item && item.emailId && item.emailId.verified]}`}
                              style={{
                                backgroundColor: "transparent",
                                fontSize: "0.95rem"
                              }}
                            />
                            Email
                          </span>
                      }
                      <span className="ms-4 fs-7">
                        {
                          item && item.documentVerification === true ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Document
                      </span>
                      <span className="ms-4 fs-7">
                        {
                          item && item.businessAddress && item.businessAddress.addressVerification === true ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Business Address
                      </span>
                      <span className="ms-4 fs-7">
                        {
                          item && item.bankVerificationStatus === "APPROVED" ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Bank Details
                      </span>
                    </div>

                    <div className="col-lg-1 col-md-1 col-sm-1 mt-2">
                      <div className="text-muted fs-8 fw-bolder">
                        EnqueueTime
                      </div>
                      <div className="fs-8 fw-bolder">
                        {item && item.enqueueHours ? !_.isEmpty(item.enqueueHours) ? item.enqueueHours : `${hours}:${minutes}` : `${hours}:${minutes}`}
                      </div>
                    </div>
                    <div className="col-lg-1 col-md-1 col-sm-1 mt-2">
                      <div className="text-muted fs-8 fw-bolder">
                        Priority
                      </div>
                      <div className="fs-8 fw-bolder">
                        {item && item.priority ? item.priority : "--"}
                      </div>
                    </div>

                    <div className="col-lg-2 col-md-2 col-sm-2">
                      <Can
                        permissons={getUsersPermissions}
                        componentPermissions={UPDATE_PERMISSION}
                      >
                        <button
                          className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px mt-4'
                          title="Edit Data"
                          onClick={() => {
                            setEditMode(true)
                            setuserId(item && item._id)
                          }}
                        >
                          <KTSVG
                            path='/media/icons/duotune/art/art005.svg'
                            className='svg-icon-3'
                          />
                        </button>
                      </Can>
                    </div>
                    <div className="col-lg-2">
                      <div
                        className={`card card-custom card-stretch gutter-b p-4 w-100 ${KYC_STATUS[item && item.kycStatus]
                          }`}
                        style={{ borderRadius: "0px" }}
                      >
                        <div className="card-body d-flex align-items-end pt-0">
                          <div className="d-flex align-items-center flex-column w-100">
                            <span className="fs-6 fw-bolder">
                              {item && item.kycStatus}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )
          })
        ) : (
          <div className="card">
            <div className="card-body">
              <div className="text-center py-3">
                <div colSpan="100%">No record(s) found</div>
              </div>
            </div>
          </div>
        )
      ) : (
        <div>
          <div colSpan="100%" className="text-center mt-10">
            <div className=" spinner-border text-primary m-5" role="status" />
          </div>
        </div>
      )}
      <div className="form-group row mb-4 mt-6">
        <div className="col-lg-12 mb-4 align-items-end d-flex">
          <div className="col-lg-12">
            <ReactPaginate
              nextLabel="Next >"
              onPageChange={handlePageClick}
              pageRangeDisplayed={3}
              marginPagesDisplayed={2}
              pageCount={totalPages}
              forcePage={activePageNumber - 1}
              previousLabel="< Prev"
              pageClassName="page-item"
              pageLinkClassName="page-link"
              previousClassName="page-item"
              previousLinkClassName="page-link"
              nextClassName="page-item"
              nextLinkClassName="page-link"
              breakLabel="..."
              breakClassName="page-item"
              breakLinkClassName="page-link"
              containerClassName="pagination"
              activeClassName="active"
              renderOnZeroPageCount={null}
            />
          </div>
        </div>
      </div>
      {/* csv Report */}
      <div className="table-responsive" style={{
        display: "none"
      }}>
        <table className="table" id="kycCsvModel">
          <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Client Name</th>
              <th>Case Id</th>
              <th>Phone Number</th>
              <th>Country Code</th>
              <th>Phone Number Verified</th>
              <th>Email Id</th>
              <th>Email Id Verified</th>
              <th>Entity Name</th>
              <th>Queue Name</th>
              <th>Business Name</th>
              <th>Primary Contact Name</th>
              <th>Business Category</th>
              <th>Business Description</th>
              <th>Website</th>
              <th>Alternate Phone Number</th>
              <th>Alternate EmailId</th>
              <th>Bank Account Number</th>
              <th>IFSC Code</th>
              <th>Account Holder Name</th>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Pin Code</th>
              <th>Kyc Status</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(kycExportReportRes && kycExportReportRes.data) ?
                kycExportReportRes && kycExportReportRes.data.map((item, it) => {
                  return (
                    <tr key={it}>
                      <td>
                        {item.clientName}
                      </td>
                      <td>
                        {item && item.caseId ? item.caseId : '--'}
                      </td>
                      <td>
                        {item && item.phoneNumber ? item.phoneNumber : '--'}
                      </td>
                      <td>
                        {item && item.countryCode ? item.countryCode : '--'}
                      </td>
                      <td>
                        {item && item.phoneNumberVerified ? item.phoneNumberVerified : '--'}
                      </td>
                      <td>
                        {item && item.emailId ? item.emailId : '--'}
                      </td>
                      <td>
                        {item && item.emailIdVerified ? item.emailIdVerified : '--'}
                      </td>
                      <td>
                        {item && item.entityName ? item.entityName : '--'}
                      </td>
                      <td>
                        {item && item.queueName ? item.queueName : '--'}
                      </td>
                      <td>
                        {item && item.businessName ? item.businessName : '--'}
                      </td>
                      <td>
                        {item && item.primaryContactName ? item.primaryContactName : '--'}
                      </td>
                      <td>
                        {item && item.businessCategory ? item.businessCategory : '--'}
                      </td>
                      <td>
                        {item && item.businessDescription ? item.businessDescription : '--'}
                      </td>
                      <td>
                        {item && item.website ? item.website : '--'}
                      </td>
                      <td>
                        {item && item.alternatePhoneNumber ? item.alternatePhoneNumber : '--'}
                      </td>
                      <td>
                        {item && item.alternateEmailId ? item.alternateEmailId : '--'}
                      </td>
                      <td>
                        {item && item.bankAccountNumber ? item.bankAccountNumber : '--'}
                      </td>
                      <td>
                        {item && item.ifscCode ? item.ifscCode : '--'}
                      </td>
                      <td>
                        {item && item.accountHolderName ? item.accountHolderName : '--'}
                      </td>
                      <td>
                        {item && item.address ? item.address : '--'}
                      </td>
                      <td>
                        {item && item.city ? item.city : '--'}
                      </td>
                      <td>
                        {item && item.state ? item.state : '--'}
                      </td>
                      <td>
                        {item && item.pinCode ? item.pinCode : '--'}
                      </td>
                      <td>
                        {item && item.kycStatus ? item.kycStatus : '--'}
                      </td>
                    </tr>
                  )
                })
                : null
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { KYClistStore, kycStatusStore, ExportReportValueTypeStore } = state
  return {
    KYClists:
      KYClistStore && KYClistStore.KYClists ? KYClistStore.KYClists : "",
    KYCUserDetails:
      KYClistStore && KYClistStore.KYCuser ? KYClistStore.KYCuser : "",
    loading:
      KYClistStore && KYClistStore.loading ? KYClistStore.loading : false,
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
    kycStatusRes:
      kycStatusStore && kycStatusStore.kycStatusRes ? kycStatusStore.kycStatusRes : "",
    kycStatusloading:
      kycStatusStore && kycStatusStore.loading ? kycStatusStore.loading : false,
    kycExportReportRes:
      ExportReportValueTypeStore && ExportReportValueTypeStore.exportData ? ExportReportValueTypeStore.exportData : "",
    kycExportReportResloading:
      ExportReportValueTypeStore && ExportReportValueTypeStore.loading ? ExportReportValueTypeStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  getKYCUserDetailsDispatch: (params) => dispatch(KYCActions.getKYCUserDetails(params)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data)),
  kycStatusDispatch: (data) => dispatch(kycStatusAction.kycStatus(data)),
  kycExportReportDispatch: (params) => dispatch(ExportReportValueAction.ExportReportValue(params)),
  clearKycExportReportDispatch: () => dispatch(ExportReportValueAction.clearExportReportValue()),
})

export default connect(mapStateToProps, mapDispatchToProps)(KYCList)
