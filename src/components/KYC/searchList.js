import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { KYCActions, clientIdLIstActions } from '../../store/actions'
import { getLocalStorage } from "../../utils/helper"
import { SET_FILTER } from "../../utils/constants"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"

function SearchList(props) {
  const { getKYClistDispatch, setSearchParams, activePageNumber, limit, clinetIdLists, clientIdDispatch, setExportButton } = props
  const [, setShow] = useState(false)
  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)

  const [formData, setFormData] = useState({
    clientId: ClinetId ? ClinetId : '',
    phone: '',
    personalEmail: '',
    address: '',
    status: '',
    riskId: ''
  })
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')

  useEffect(() => {
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params)
  }, [])

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSearch = () => {
    setShow(false)
    const params = {
      ...formData,
      limit: limit,
      page: activePageNumber
    }
    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key] !== '') {
        params[key] = formData[key]
        setExportButton(true)
      }
    }
    getKYClistDispatch(params)
    setSearchParams(params)
  }

  const handleReset = () => {
    setFormData({
      deviceID: '',
      phone: '',
      personalEmail: '',
      ipAddress: '',
      address: '',
      status: '',
      riskId: ''
    })
    setSelectedAsigneesOption('')
    setExportButton(false)
    const params = {
      limit: limit,
      page: activePageNumber,
      clientId: !_.isEmpty(credBasedClientValue) ? credBasedClientValue : ''
    }
    getKYClistDispatch(params)
  }

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>
      <div
        className='modal fade'
        id='searchModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1000px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Search</h2>
              <button type="button"
                data-repeater-delete=""
                className="btn btn-sm btn-icon btn-light-danger"
                data-dismiss='modal'
                onClick={() => { setShow(false) }}
              >
                <span className="svg-icon svg-icon-2">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor"></rect>
                    <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor"></rect>
                  </svg>
                </span>
              </button>
            </div>
            <div className='modal-body bg-lightBlue'>
              <form className='container-fixed'>
                <div className='card-header'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      {
                        Role === 'Admin' ? (
                          !credBasedClientValue ?
                            <div className='col-lg-3 mb-3'>
                              <label className='font-size-xs font-weight-bold mb-3  form-label'>
                                Client :
                              </label>
                              <div className='col-lg-11'>
                                <ReactSelect
                                  styles={customStyles}
                                  isMulti={false}
                                  name='AppUserId'
                                  className='select2'
                                  classNamePrefix='select'
                                  handleChangeReactSelect={handleChangeAsignees}
                                  options={AsigneesOption}
                                  value={SelectedAsigneesOption}
                                  isDisabled={!AsigneesOption}
                                />
                              </div>
                            </div>
                            : null
                        ) : (
                          null
                        )
                      }
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Case Id:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='riskId'
                            type='text'
                            className='form-control'
                            placeholder='case Id'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.riskId || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Phone:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='phone'
                            type='text'
                            className='form-control'
                            placeholder='Phone'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            maxLength={10}
                            value={formData.phone || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Email:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='personalEmail'
                            type='text'
                            className='form-control'
                            placeholder='Email'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.personalEmail || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Status:
                        </label>
                        <div className='col-lg-11'>
                          <select
                            name='status'
                            className='form-select form-select-solid'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e)}
                            value={formData.status || ''}
                          >
                            <option value=''>Select...</option>
                            <option value='PENDING'>PENDING</option>
                            <option value='APPROVED'>APPROVED</option>
                            <option value='REJECTED'>REJECTED</option>
                            <option value='HOLD'>HOLD</option>
                          </select>
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                              onClick={() => handleSearch()}
                              data-dismiss='modal'
                            >
                              Search
                            </button>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                              onClick={() => handleReset()}
                            >
                              Reset
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => ({
  getRiskManagementlist: state && state.riskManagementlistStore && state.riskManagementlistStore.getRiskManagementlist,
  loading: state && state.riskManagementlistStore && state.riskManagementlistStore.loading,
  clinetIdLists: state && state.clinetListStore && state.clinetListStore.clinetIdLists ? state.clinetListStore.clinetIdLists : "",
})

const mapDispatchToProps = dispatch => ({
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  clearClientIdDispatch: () => dispatch(clientIdLIstActions.clearclientIdLIst()),

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)