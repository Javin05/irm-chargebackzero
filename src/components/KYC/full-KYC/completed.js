import React, { useEffect } from "react";
import {
  KYCActions,
  FullKycValueAction
} from '../../../store/actions'
import { connect } from 'react-redux'
import { STATUS_RESPONSE } from '../../../utils/constants'

function FullKycCompletedPage(props) {
  const {
    setKycShow,
    setShow,
    getKYClistDispatch,
    setActiveStep,
    setCompletedSteps,
    setClientDetails,
    fullKycDispatch,
    kycAllDataSaved,
    FullKycResData,
    clearFullKycValueDispatch,
    clearClientsWrmDispatch
  } = props
  
  useEffect(() => {
    const params = {
      kycSubmitStatus : 'FULL',
      responseType : "CREATED"
    }
    fullKycDispatch(kycAllDataSaved && kycAllDataSaved.id, params)
    
  },[])

  const Finsh = () => {
    getKYClistDispatch()
  }
  
  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  return (
    <>
      <div>
        <div className="card card-flush"
          style={{
            backgroundColor: '#1a1d31'
          }}
        >
          <div className="card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"
          >
            <div className="d-flex justify-content-center">
              <i className="bi bi-check-circle-fill"
                style={{ color: '#208f20', backgroundColor: 'transparent', fontSize: '2.75rem' }}
              />
            </div>
            <div className="fs-1hx fw-bold text-white text-center mb-10 mt-5">
              Your Details Submitted Successfully
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center mt-4">
        <a className="btn btn-light-dark"
          onClick={() => { 
            setShow(false)
            setCompletedSteps([-1])
            setActiveStep(0)
            setKycShow(false)
            setClientDetails(null)
            clearClientsWrmDispatch()
            Finsh()
          }}
          >Back to List</a>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => {
  const { FullKycValueStore } = state
  return {
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(FullKycCompletedPage)
