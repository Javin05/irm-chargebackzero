
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businesDetailValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { KYC_FORM, REGEX, STATUS_RESPONSE, ORGANIZATIONS_TITLE } from '../../../utils/constants'
import _ from 'lodash'
import {
  CityActions,
  KYCcinAction,
  KYCgstinAction,
  UENAction
} from '../../../store/actions'
import { warningAlert } from "../../../utils/alerts"
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"

const CountryData =
{
  "status": "ok",
  "data": [
    {
      "_id": "62512ff29569c045ec71c83e",
      "updatedAt": "2022-04-09T07:04:21.541Z",
      "createdAt": "2022-04-09T07:04:21.541Z",
      "statecode": "4120",
      "name": "India",
      "status": "ACTIVE"
    },
    {
      "_id": "62512ff29569c045ec71c83e",
      "updatedAt": "2022-04-09T07:04:21.541Z",
      "createdAt": "2022-04-09T07:04:21.541Z",
      "statecode": "4120",
      "name": "Singapore",
      "status": "ACTIVE"
    },
  ]
}

function GstinAndCin(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    KYCbusinessCinLoading,
    KYCcinResponse,
    KycCinVerifyDispatch,
    KYCgstinResponse,
    KycGstinVerifyDispatch,
    clearKYCcinVerifyDispatch,
    clearKYCgstinVerifyDispatch,
    setFullKycDetails,
    UENDispatch,
    UENResponse,
    clearUENDispatch,
    UENLoading
  } = props

  const [errors, setErrors] = useState({})
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [GstinCheked, setGstinCheked] = useState()
  const [Verified, setVerified] = useState(false)
  const [gstinVerified, setGstinVerified] = useState(false)
  const [UenVerified, setUenVerified] = useState(false)
  const [showNext, setShowNext] = useState(false)
  const [Data, setData] = useState(true)
  const [countryOption, setcountryOption] = useState("India")
  const [selectedcountryOption, setSelectedcountryOption] = useState('')
  const [formData, setFormData] = useState({
    gstNumber: '',
    cin: '',
    noGstin: '',
    LLPIN: '',
    country: 'India',
    uenNumber: ''
  })

  const handleChange = (e) => {
    e.persist()
    const { value, name, checked } = e.target
    setGstinCheked(checked)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const CinVerify = () => {


    setLocalStorage('CINandGSTIN', JSON.stringify(formData))
    setData(formData)
    setFullKycDetails((values) => ({ ...values, gstinData: formData }))
    onClickNext(3)

    // const errors = {}
    // if (organization === 'Proprietorship' ||
    //   organization === 'Partnership' ||
    //   organization === 'Trust' ||
    //   organization === 'Society'
    // ) {
    //   if (_.isEmpty(formData.gstNumber) && GstinCheked === false) {
    //     errors.gstNumber = 'gstNumber Is Required'
    //   }
    //   if (_.isEmpty(errors)) {
    //     const data = {
    //       gstNumber: formData.gstNumber,
    //     }
    //     KycGstinVerifyDispatch(data)
    //     setLocalStorage('CINandGSTIN', JSON.stringify(formData))
    //     setData(formData)
    //     setFullKycDetails((values) => ({ ...values, gstinData: formData }))
    //   }
    // } else if (organization === 'LLP') {
    //   if (_.isEmpty(formData.gstNumber) && GstinCheked === false) {
    //     errors.gstNumber = 'gstNumber Is Required'
    //   }
    //   if (_.isEmpty(formData.LLPIN)) {
    //     errors.LLPIN = 'LLPIN Is Required'
    //   }
    //   if (_.isEmpty(errors)) {
    //     const data = {
    //       gstNumber: formData.gstNumber,
    //     }
    //     // KycGstinVerifyDispatch(data)
    //     setLocalStorage('CINandGSTIN', JSON.stringify(formData))
    //     setData(formData)
    //     setFullKycDetails((values) => ({ ...values, gstinData: formData }))
    //   }
    // }
    // else {
    //   if (_.isEmpty(formData.cin)) {
    //     errors.cin = 'CIN Is Required'
    //   } else if (formData.cin && !REGEX.CIN.test(formData.cin)) {
    //     errors.cin = 'CIN Number Is InValid'
    //   } if (_.isEmpty(formData.gstNumber) && GstinCheked === false) {
    //     errors.gstNumber = 'gstNumber Is Required'
    //   }
    //   if (_.isEmpty(errors)) {
    //     const data = {
    //       cin: formData.cin
    //     }
    //     KycCinVerifyDispatch(data)
    //     setLocalStorage('CINandGSTIN', JSON.stringify(formData))
    //     setData(formData)
    //     setFullKycDetails((values) => ({ ...values, gstinData: formData }))
    //   }
    // }
    // setErrors(errors)
  }

  const cinverify = JSON.parse(getLocalStorage('CINandGSTIN'))
  useEffect(() => {
    setFormData({
      cin: cinverify.cin,
      gstNumber: cinverify.gstNumber,
      LLPIN: cinverify.LLPIN,
    })
  }, [Data])

  useEffect(() => {
    if (KYCcinResponse && KYCcinResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setVerified(true)
      if (!_.isEmpty(formData.gstNumber) && GstinCheked === false) {
        const errors = {}
        if (_.isEmpty(formData.gstNumber)) {
          errors.gstNumber = 'GST Number Is Required'
        } else if (formData.gstNumber && !REGEX.GSTIN_REGEX.test(formData.gstNumber)) {
          errors.gstNumber = 'GST Number Number Is InValid'
        }
        if (_.isEmpty(errors)) {
          const data = {
            gstNumber: formData.gstNumber,
          }
          KycGstinVerifyDispatch(data)
        }
        setErrors(errors)
      } else {
        setShowNext(true)
      }
      clearKYCcinVerifyDispatch()
    } else if (KYCcinResponse && KYCcinResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KYCcinResponse && KYCcinResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCcinVerifyDispatch()
    }
  }, [KYCcinResponse])

  useEffect(() => {
    if (GstinCheked) {
      setFormData({
        gstNumber: '',
        cin: formData.cin
      })
    }
  }, [GstinCheked])

  useEffect(() => {
    if (KYCgstinResponse && KYCgstinResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setGstinVerified(true)
      clearKYCgstinVerifyDispatch()
      setShowNext(true)
    } else if (KYCgstinResponse && KYCgstinResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KYCgstinResponse && KYCgstinResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCgstinVerifyDispatch()
    }
  }, [KYCgstinResponse])

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  const countryName = CountryData && CountryData.data ? CountryData.data : '--'
  useEffect(() => {
    const country = getDefaultOptions(countryName)
    setcountryOption(country)
    if (!_.isEmpty(formData.country)) {
      const selOption = _.filter(country, function (x) { if (_.includes(formData.country._id, x.value)) { return x } })
      setSelectedcountryOption(selOption)
    }
  }, [countryName])

  const handleChangeCountry = selectedOption => {
    if (selectedOption !== null) {
      setSelectedcountryOption(selectedOption)
      setFormData(values => ({ ...values, country: selectedOption.label }))
    } else {
      setSelectedcountryOption()
      setFormData(values => ({ ...values, country: '' }))
    }
    setErrors({ ...errors, country: '' })
  }

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  const UENhandel = () => {
    const errors = {}
    if (_.isEmpty(formData.uenNumber)) {
      errors.uenNumber = 'UEN Is Required'
    }
    if (_.isEmpty(errors)) {
      const params = { ueNumber: formData.uenNumber }
      UENDispatch(params)
      setFullKycDetails((values) => ({ ...values, gstinData: formData }))
    }
    setErrors(errors)
  }

  useEffect(() => {
    if (UENResponse && UENResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShowNext(true)
      setUenVerified(true)
    } else if (UENResponse && UENResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        UENResponse && UENResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearUENDispatch()
    }
  }, [UENResponse])

  useEffect(() => {
    setUenVerified(false)
  }, [])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                {/* <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>{ORGANIZATIONS_TITLE[organization]}</span>
                  </label>
                </div> */}
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>We require this information for taxation and compliance.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Add your CIN and gstNumber'
                    />
                  </label>
                </div>
              </div>
              {
                (organization === 'Public Limited' ||
                  organization === 'Private Limited')
                  ? (
                    <>
                      <div className='row mb-4'>
                        <div className='col-lg-8'>
                          <input
                            type='text'
                            className='form-control form-control-lg form-control-solid mt-4'
                            name='cin'
                            placeholder='CIN'
                            onChange={(e) => handleChange(e)}
                            value={formData.cin || ''}
                            style={{ textTransform: 'uppercase' }}
                          />
                          {errors && errors.cin && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red;}'}</style>
                              {errors.cin}
                            </div>
                          )}
                        </div>
                        <div className='col-lg-4'>
                          {
                            Verified ? (
                              <button
                                className='btn btn-sm btn-success mt-4 me-2'
                              >
                                Verified
                                <i className="bi bi-person-check-fill ms-2" />
                              </button>
                            ) : (null)
                          }
                        </div>
                      </div>
                    </>
                  ) : (
                    organization === 'LLP' ? (
                      <>
                        <div className='row mb-4'>
                          <div className='col-lg-8'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid mt-4'
                              name='LLPIN'
                              placeholder='LLPIN'
                              onChange={(e) => handleChange(e)}
                              value={formData.LLPIN || ''}
                              style={{ textTransform: 'uppercase' }}
                              maxLength={8}
                            />
                            <div className='text-muted mt-2 fs-8'>EXAMPLE : AAB-2933</div>
                            {errors && errors.LLPIN && (
                              <div className='rr mt-1'>
                                <style>{'.rr{color:red;}'}</style>
                                {errors.LLPIN}
                              </div>
                            )}
                          </div>
                          {/* <div className='col-lg-4'>
                          {
                            Verified ? (
                              <button
                                className='btn btn-sm btn-success mt-4 me-2'
                              >
                                Verified
                                <i className="bi bi-person-check-fill ms-2" />
                              </button>
                            ) : (null)
                          }
                        </div> */}
                        </div>
                      </>

                    ) : (
                      null
                    )
                  )
              }

              <div className='row mb-4'>
                <div className='col-lg-6 mb-4 mt-4'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Country</span>
                  </label>
                  {/* <ReactSelect
                    isClearable
                    styles={customStyles}
                    isMulti={false}
                    name='country'
                    placeholder="Select..."
                    className="basic-single"
                    classNamePrefix="select"
                    handleChangeReactSelect={handleChangeCountry}
                    options={countryOption}
                    value={selectedcountryOption}
                    isDisabled={!countryOption}
                  /> */}

                  <div className='input mb-5 col-md-12'>
                    <select
                      name='country'
                      className='form-select form-select-solid'
                      data-control='select'
                      data-placeholder='Select an option'
                      data-allow-clear='true'
                      onChange={(e) => handleChange(e)}
                      value={formData.country || ''}
                    >
                      <option value='India'>India</option>
                    </select>
                  </div>
                </div>
                
                {errors && errors.country && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red}'}</style>
                      {errors.country}
                    </div>
                  )}
                {
                  formData.country === 'Singapore' ? (
                    <>
                      <div className='col-lg-8'>
                        <input
                          type='text'
                          className='form-control form-control-lg form-control-solid mt-4'
                          name='uenNumber'
                          placeholder='UEN'
                          onChange={(e) => handleChange(e)}
                          value={formData.uenNumber || ''}
                          style={{ textTransform: 'uppercase' }}
                          disabled={GstinCheked}
                        />
                        {errors && errors.uenNumber && (
                          <div className='rr mt-1'>
                            <style>{'.rr{color:red;}'}</style>
                            {errors.uenNumber}
                          </div>
                        )}
                      </div>
                      <div className='col-lg-4'>
                        {
                          UenVerified ? (
                            <button
                              className='btn btn-sm btn-success mt-4 me-2'
                            >
                              Verified
                              <i className="bi bi-person-check-fill ms-2" />
                            </button>
                          ) : (
                            <button
                              className='btn btn-sm btn-primary mt-4 me-2'
                              onClick={() => { UENhandel() }}
                              disabled={UENLoading}
                            >
                              {
                                UENLoading ? (
                                  <>
                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                      Please wait...
                                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                    </span>
                                  </>
                                ) : (
                                  <>
                                    <i className="bi bi-person-check-fill ms-2" />
                                    Verify
                                  </>
                                )
                              }
                            </button>
                          )
                        }
                      </div>
                    </>
                  ) : (
                    <> <div className='col-lg-8'>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid mt-4'
                        name='gstNumber'
                        placeholder='GSTIN'
                        onChange={(e) => handleChange(e)}
                        value={formData.gstNumber || ''}
                        style={{ textTransform: 'uppercase' }}
                        disabled={GstinCheked}
                      />
                      <div className="form-check form-check-custom form-check-solid form-check-sm mt-4">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          value="i dont have a gstNumber yet"
                          id="flexRadioLg"
                          // name='gstNumber'
                          onChange={(e) => handleChange(e)}
                          checked={GstinCheked}
                        />
                        <label className="form-check-label text-muted" for="flexRadioLg">
                          I dont have a gstNumber yet
                        </label>
                      </div>
                      {errors && errors.gstNumber && (
                        <div className='rr mt-1'>
                          <style>{'.rr{color:red;}'}</style>
                          {errors.gstNumber}
                        </div>
                      )}
                    </div>
                      <div className='col-lg-4'>
                        {
                          gstinVerified ? (
                            <button
                              className='btn btn-sm btn-success mt-4 me-2'
                            >
                              Verified
                              <i className="bi bi-person-check-fill ms-2" />
                            </button>
                          ) : (null)
                        }
                      </div>

                    </>
                  )
                }
              </div>
            </div>
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2'>
                  <button
                    onClick={() => { goBack(1) }}
                    type='button'
                    className='btn btn-sm btn-light-primary me-3'
                  >
                    <KTSVG
                      path='/media/icons/duotune/arrows/arr063.svg'
                      className='svg-icon-4 me-1'
                    />
                    Back
                  </button>
                </div>
                <div>
                  {
                    !showNext ? (
                      <button type='submit' className={`btn btn-sm  btn-light-primary`}
                        onClick={() => { CinVerify() }}
                        disabled={KYCbusinessCinLoading}
                      >
                        {!KYCbusinessCinLoading &&
                          <span className='indicator-label'>
                            <i className={`bi bi-person-check-fill`} />
                            Next
                          </span>
                        }
                        {KYCbusinessCinLoading && (
                          <span className='indicator-progress' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    ) :
                      <button type='submit' className={`btn btn-sm  btn-light-primary`}
                        onClick={() => { onClickNext(3) }}
                      >
                        Continue
                      </button>
                  }
                  <button type='submit' className='btn btn-sm btn-light-danger ms-2'
                    onClick={() => { onClickNext(3) }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Skip
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { KYCcinStore, KYCgstinStore, UENStore } = state

  return {
    KYCbusinessCinLoading: KYCcinStore && KYCcinStore.loading ? KYCcinStore.loading : false,
    KYCcinResponse: KYCcinStore && KYCcinStore.KycCIN ? KYCcinStore.KycCIN : {},
    KYCgstinResponse: KYCgstinStore && KYCgstinStore.Kycgstin ? KYCgstinStore.Kycgstin : {},
    UENResponse: UENStore && UENStore.UENres ? UENStore.UENres : {},
    UENLoading: UENStore && UENStore.loading ? UENStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycCinVerifyDispatch: (params) => dispatch(KYCcinAction.KYCcinVerify(params)),
  clearKYCcinVerifyDispatch: (params) => dispatch(KYCcinAction.clearKYCcinVerify(params)),
  KycGstinVerifyDispatch: (params) => dispatch(KYCgstinAction.KYCgstinVerify(params)),
  clearKYCgstinVerifyDispatch: (params) => dispatch(KYCgstinAction.clearKYCgstinVerify(params)),
  UENDispatch: (params) => dispatch(UENAction.UEN(params)),
  clearUENDispatch: (params) => dispatch(UENAction.clearUEN(params))
})

export default connect(mapStateToProps, mapDispatchToProps)(GstinAndCin)