
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businesDetailValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { DROPZONE_IMAGE_NAME_TYPES, REGEX, STATUS_RESPONSE, DROPZONE_MESSAGES, AADHAAR_BACK, AADHAAR_FRONT, IDPROFF, FRONT_TYPE_VERIFY } from '../../../utils/constants'
import _, { values } from 'lodash'
import {
  CityActions,
  KYCAdharNumberAction,
  AadhaarFrontAction,
  AadhaarBackAction,
  StateActions,
  PinCodeValueAction,
  FullKycValueAction
} from '../../../store/actions'
import { warningAlert } from "../../../utils/alerts"
import {
  FILE_FORMAT_TYPE,
  FILE_FORMAT_TYPE_DOCUMEN_IMAGE
} from "../../../constants/index"
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"

function Address(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    getStateDispatch,
    getStates,
    getCityDispatch,
    getCitys,
    clientDetails,
    KycAadharDispatch,
    AadharVerifyRes,
    Aadharloading,
    clearKYCAdhaarDispatch,
    setFullKycDetails,
    AadhaarFrontDispatch,
    AadhaarBackDispatch,
    AadharFrontValue,
    AadharBackValue,
    AadharFrontLoading,
    AadharBackLoading,
    ClearAadhaarBackDispatch,
    ClearAadhaarFrontDispatch,
    pincodeDataRes,
    getPincodeDispatch,
    fullKycDispatch,
    clearFullKycValueDispatch,
    FullKycResData,
    FullKycLoading,
    kycAllDataSaved
  } = props

  const hiddenFileInput = useRef(null)
  const hiddenFilesInput = useRef(null)
  const [errors, setErrors] = useState({})
  const [showAadhar, setShowAadhar] = useState(0)
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [stateOption, setStateOption] = useState()
  const [selectedStateOption, setSelectedStateOption] = useState('')
  const [cityOptions, setCityOptions] = useState()
  const [selectedCityOption, setSelectedCityOption] = useState('')
  const [Data, setData] = useState(true)
  const [frontendfileName, setFrontendfileName] = useState('Upload')
  const [backfileName, setbackfileName] = useState('Upload')
  const [showfrontAadhaar, setShowfrontAadhaar] = useState(false)
  const [showbackAadhaar, setShowbackAadhaar] = useState(false)
  const [pincode, setPincode] = useState('')
  const [formData, setFormData] = useState({
    Address: '',
    pinCode: '',
    state: '',
    city: '',
    // aadharNumber: '',
    // identityProof: '',
    // AadhaarFront: '',
    // AadhaarBack: '',
    // Aadhartype: '',
    // businessIpAddress: '',
    responseType : "CREATED"
  })

  useEffect(() => {
    const params = {
      skipPagination: 'true'
    }
    getStateDispatch(params)
  }, [])

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  setLocalStorage('AADHARTYPE', JSON.stringify(formData.identityProof ? formData.identityProof : 'null'))
  const organization = JSON.parse(getLocalStorage('ORGANIZATIONS'))
  const handleSubmit = () => {
    const errors = {}
    if (organization === 'Partnership') {
      if (_.isEmpty(formData.Address)) {
        errors.Address = 'Address Number Is Required'
      }
      if (_.isEmpty(formData.pinCode)) {
        errors.pinCode = 'pinCode Is Required'
      }
      if (_.isEmpty(formData.businessIpAddress)) {
        errors.businessIpAddress = 'Business Ip Address Is Required.'
      }
      if (_.isEmpty(formData.aadharNumber)) {
        errors.aadharNumber = 'Aadhar Number Is Required'
      }
      if (_.isEmpty(formData.identityProof)) {
        errors.identityProof = 'PLease Select Idetity Proof'
      }
      if (_.isEmpty(formData.AadhaarFront)) {
        errors.AadhaarFront = `${AADHAAR_FRONT[formData.identityProof]} Is Required`
      }
      if (_.isEmpty(formData.AadhaarBack)) {
        errors.AadhaarBack = `${AADHAAR_BACK[formData.identityProof]} Is Required`
      }
      if (_.isEmpty(errors)) {
        setLocalStorage('ADDRESSVERIFY', JSON.stringify(formData))
        setFullKycDetails((values) => ({ ...values, AddressData: formData }))
        setData(formData)
        const params = {
          aadharNumber: formData.aadharNumber
        }
        KycAadharDispatch(params)
      }
    } else {
      if (_.isEmpty(formData.Address)) {
        errors.Address = 'Address Is Required'
      }
      if (_.isEmpty(formData.pinCode)) {
        errors.pinCode = 'pinCode Is Required'
      }
      if (_.isEmpty(errors)) {
        fullKycDispatch(kycAllDataSaved && kycAllDataSaved.id, formData)
      }
    }
    setErrors(errors)
  }

  const getDefaultOptions = (rawData) => {
    const defaultOptions = []
    for (const item in rawData) {
      defaultOptions.push({ label: rawData[item].name, value: rawData[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    const state = getDefaultOptions(getStates)
    setStateOption(state)
    if (!_.isEmpty(formData.state)) {
      const selOption = _.filter(state, function (x) { if (_.includes(formData.state._id, x.value)) { return x } })
      setSelectedStateOption(selOption)
    }
  }, [getStates])

  useEffect(() => {
    const city = getDefaultOptions(getCitys)
    setCityOptions(city)
    if (!_.isEmpty(formData.city)) {
      const selOption = _.filter(city, function (x) { if (_.includes(formData.city._id, x.value)) { return x } })
      setSelectedCityOption(selOption)
    }
  }, [getCitys])

  const handlePincode = (e) => {
    setPincode(e.target.value.trim())
    setFormData({
      ...formData,
      pinCode: e.target.value.trim()
    })
  }

  useEffect(() => {
    if (pincode.length === 6) {
      const payload = {
        pincode: pincode
      }
      getPincodeDispatch(payload)
    } else if (pincode.length > 6) {
      const errors = {}
      errors.pincode = 'Pincode is InValid'
    }
  }, [pincode])

  const handleClick = (event) => {
    hiddenFileInput.current.click(event)
  }
  const handleBackClick = (event) => {
    hiddenFilesInput.current.click(event)
  }

  // useEffect(() => {
  // return () => {
  //   setFrontendfileName('Upload')
  //   setbackfileName('Upload')

  // }
  // },[])

  const customStyles = {
    control: (provided) => ({
      ...provided,
      alignItems: "baseline",
      background: "#fff",
      minHeight: "35px",
      border: "solid 0px",
      borderBottom: "solid 1px",
      boxShadow: "0 0 0 1px #fff",
      marginBottom: "0px",
      "&:hover": {
        border: "#fff",
        borderBottom: "solid 1px",
      },
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      color: "#cfc3c3",
    }),
    indicatorSeparator: (provided) => ({
      ...provided,
      width: "0px",
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
      borderColor: "#fff",
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "1em",
      color: "black",
      fontWeight: 200,
    })
  }

  useEffect(() => {
    return () => {
      setFormData({
        Address: '',
        pinCode: '',
        state: '',
        city: '',
        aadharNumber: '',
        identityProof: '',
        AadhaarFront: '',
        AadhaarBack: '',
        Aadhartype: '',
        businessIpAddress: ''
      })
    }
  }, [])

  useEffect(() => {
    if (pincodeDataRes && pincodeDataRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setFormData({
        ...formData,
        state: pincodeDataRes && pincodeDataRes.data && pincodeDataRes.data.state ? pincodeDataRes.data.state : '',
        city: pincodeDataRes && pincodeDataRes.data && pincodeDataRes.data.city ? pincodeDataRes.data.city : '',
      })
    } else if (pincodeDataRes && pincodeDataRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        pincodeDataRes && pincodeDataRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [pincodeDataRes])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      clearFullKycValueDispatch()
      onClickNext(6)
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  // "aadharNumber": "",
  //   "identityProof": "",
  //   "AadhaarFront": "",
  //   "AadhaarBack": "",
  //   "Aadhartype": "",

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Add your registered business address</span>
                  </label>
                </div>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text-muted fs-6 fw-bold'>We'll verify these details using your given business documents.</span>
                    <i
                      className='fas fa-exclamation-circle ms-2 fs-7'
                      data-bs-toggle='tooltip'
                      title='Add your pinCode and Address'
                    />
                  </label>
                </div>
              </div>
              <div className='row mb-4 col-lg-12 mt-4 mb-4'>
                <div>
                  <label className='d-flex align-items-center mb-2'>
                    <span className='text fs-4 fw-bolder required'>PinCode</span>
                  </label>
                </div>
                <div className='col-lg-6'>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mt-4'
                    name='pinCode'
                    placeholder='PinCode'
                    onChange={handlePincode}
                    value={pincode || ''}
                    maxLength={6}
                    onKeyPress={(e) => {
                      if (!REGEX.ALPHA_NUMERIC_CHARS_SPACE.test(e.key)) {
                        e.preventDefault()
                      }
                    }}
                  />
                  {errors && errors.pinCode && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red}'}</style>
                      {errors.pinCode}
                    </div>
                  )}
                </div>
              </div>
              <div className='row mb-4 col-lg-12 mt-4 mb-4'>
                <div className='col-lg-6'>
                  <div>
                    <label className='d-flex align-items-center mb-2'>
                      <span className='text-muted fs-4 fw-bolder required'>State</span>
                    </label>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-4'
                    name='state'
                    placeholder='State'
                    value={formData.state || ''}
                    disabled
                  />
                  {errors && errors.state && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.state}
                    </div>
                  )}
                </div>
                <div className='col-lg-6'>
                  <div>
                    <label className='d-flex align-items-center mb-2'>
                      <span className='text-muted fs-4 fw-bolder required'>City</span>
                    </label>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-4'
                    name='city'
                    placeholder='City'
                    value={formData.city || ''}
                    disabled
                  />
                  {errors && errors.city && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.city}
                    </div>
                  )}
                </div>
              </div>
              <div>
                <label className='d-flex align-items-center mb-2'>
                  <span className='text fs-4 fw-bolder required'>Address</span>
                </label>
              </div>
              <input
                type='text'
                className='form-control form-control-lg form-control-solid mt-4'
                name='Address'
                placeholder='Address'
                onChange={(e) => handleChange(e)}
                value={formData.Address || ''}
              />
              {errors && errors.Address && (
                <div className='rr mt-1'>
                  <style>{'.rr{color:red}'}</style>
                  {errors.Address}
                </div>
              )}
            </div>
            {
              organization === 'Partnership' ? (
                <>
                  <div className='fv-row mb-10'>
                    <div className='row mb-4'>
                      <div className='col-lg-12'>
                        <label className='d-flex align-items-center fw-bold mb-2'>
                          <span className='required fw-bold fs-4'>Add an identify Proof for {`${``}`}</span>
                        </label>
                      </div>
                      <div className='col-lg-12'>
                        <label className='d-flex align-items-center mb-2'>
                          <span className='text-muted fs-6 fw-bold'>We require this information for taxation and compliance.</span>
                          <i
                            className='fas fa-exclamation-circle ms-2 fs-7'
                            data-bs-toggle='tooltip'
                            title='AadharNumber'
                          />
                        </label>
                      </div>
                    </div>
                    <input
                      type='text'
                      className='form-control form-control-lg form-control-solid mt-4'
                      name='aadharNumber'
                      placeholder='Adhaar Number'
                      onChange={(e) => handleChange(e)}
                      value={formData.aadharNumber || ''}
                      onKeyPress={(e) => {
                        if (!REGEX.NUMERIC.test(e.key)) {
                          e.preventDefault()
                        }
                      }}
                    />
                    {errors && errors.aadharNumber && (
                      <div className='rr mt-1'>
                        <style>{'.rr{color:red}'}</style>
                        {errors.aadharNumber}
                      </div>
                    )}
                  </div>
                </>
              ) : (
                null
              )
            }
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10'>
                <div className='me-2' />
                <div className='d-flex'>
                  <button type='submit' className='btn btn-sm btn-primary me-3'
                    onClick={() => {
                      handleSubmit()
                    }}
                    disabled={FullKycLoading}
                  >
                    {!FullKycLoading &&
                      <span className='indicator-label'>
                        <i className='bi bi-person-fill' />
                        Proceed
                      </span>
                    }
                    {FullKycLoading && (
                      <span className='indicator-progress' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, AdharNumberStore, AAdhaarUploadStore, AAdhaarBackUploadStore, FullKycValueStore } = state

  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    AadharVerifyRes: AdharNumberStore && AdharNumberStore.AadharVerify ? AdharNumberStore.AadharVerify : {},
    Aadharloading: AdharNumberStore && AdharNumberStore.loading ? AdharNumberStore.loading : false,
    AadharFrontValue: AAdhaarUploadStore && AAdhaarUploadStore.AadharFrontValue ? AAdhaarUploadStore.AadharFrontValue : {},
    AadharFrontLoading: AAdhaarUploadStore && AAdhaarUploadStore.loading ? AAdhaarUploadStore.loading : false,
    AadharBackValue: AAdhaarBackUploadStore && AAdhaarBackUploadStore.AadharBackValue ? AAdhaarBackUploadStore.AadharBackValue : {},
    AadharBackLoading: AAdhaarBackUploadStore && AAdhaarBackUploadStore.loading ? AAdhaarBackUploadStore.loading : false,
    pincodeDataRes: state && state.PinCodeValueTypeStore && state.PinCodeValueTypeStore.pincodeData,
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getStateDispatch: (params) => dispatch(StateActions.getStatelist(params)),
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycAadharDispatch: (params) => dispatch(KYCAdharNumberAction.KYCAdhaarNumber(params)),
  clearKYCAdhaarDispatch: (params) => dispatch(KYCAdharNumberAction.clearKYCAdhaarNumber(params)),
  AadhaarFrontDispatch: (params) => dispatch(AadhaarFrontAction.AadhaarFrontValue(params)),
  AadhaarBackDispatch: (params) => dispatch(AadhaarBackAction.AadhaarBackValue(params)),
  ClearAadhaarBackDispatch: (params) => dispatch(AadhaarBackAction.clearAadhaarBackValue(params)),
  ClearAadhaarFrontDispatch: (params) => dispatch(AadhaarFrontAction.clearAadhaarFrontValue(params)),
  getPincodeDispatch: (payload) => dispatch(PinCodeValueAction.PinCodeValue(payload)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(Address)