
import React, { FC, useEffect, useRef, useState, useCallback } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  CommomFileAction,
  EntityValueAction,
  FullKycValueAction,
  categoryEntityAction
} from '../../../store/actions'
import { STATUS_RESPONSE, DROPZONE_MESSAGES, DOCUMENT_URL } from '../../../utils/constants'
import {
  RESTRICTED_FILE_FORMAT_TYPE, PDF
} from "../../../constants/index";
import { warningAlert } from "../../../utils/alerts"

function IdentityVerification(props) {
  const {
    onClickNext,
    setFullKycDetails,
    CommonFileDocument,
    ClearCommonFileDispatch,
    EntityValueDispatch,
    EntityValueData,
    CommonFileDispatch,
    CommonFileLoading,
    clientDetails,
    fullKycDispatch,
    clearFullKycValueDispatch,
    kycAllDataSaved,
    FullKycResData,
    FullKycLoading,
    categoryEntityDispatch,
    categoryEntityData
  } = props

  const hiddenFileInput = useRef(null)
  const addressFilesInput = useRef(null)
  const businessFilesInput = useRef(null)
  const categoryFilesInput = useRef(null)
  const merchantFilesInput = useRef(null)
  const vendorFilesInput = useRef(null)
  const additionalDocument1 = useRef(null)
  const additionalDocument2 = useRef(null)

  const [IndidualOption, setIndidualOption] = useState("India")
  const [selectedIndidualOption, setSelectedIndidualOption] = useState([])
  const [indualValue, setIndualValue] = useState([])

  const [errors, setErrors] = useState({})
  const [individuaFileName, setindividuaFileName] = useState('Upload')
  const [showIndidual, setshowIndidual] = useState(false)
  const [currentInputIndex, setCurrentInputIndex] = useState()
  const [fileIndex, setfileIndex] = useState()
  const [indualFormData, setIndualFormData] = useState([])
  const [indidualError, setIndidualError] = useState([])

  const [addressFileName, setaddressFileName] = useState('Upload')
  const [showAddress, setshowAddress] = useState(false);
  const [addressFormData, setAddressFormData] = useState([])
  const [addressIndex, setAddressIndex] = useState()
  const [addressfileIndex, setAddressfileIndex] = useState()


  const [businessFileName, setBusinessFileName] = useState('Upload')
  const [showbusiness, setshowBusiness] = useState(false);
  const [businessFormData, setBusinessFormData] = useState([])
  const [businessIndex, setBusinessIndex] = useState()
  const [businessfileIndex, setBusinessfileIndex] = useState()

  const [categoryFileName, setcategoryFileName] = useState('Upload')
  const [showcategory, setshowcategory] = useState(false);
  const [categoryFormData, setcategoryFormData] = useState([])
  const [categoryIndex, setcategoryIndex] = useState()
  const [categoryfileIndex, setcategoryfileIndex] = useState()

  const [merchantFileName, setmerchantFileName] = useState('Upload')
  const [showmerchant, setshowmerchant] = useState(false);
  const [merchantFormData, setmerchantFormData] = useState({
    merchantDeclarationFrom: '',
    vendorRegistrationForm: ''
  })
  const [additionalDocument1Data, setAdditionalDocument1Data] = useState({
    label: '',
    document: ''
  })
  const [additionalDocument1Show, setAdditionalDocument1Show] = useState(false);
  const [additionalDocument2Data, setAdditionalDocument2Data] = useState({
    label: '',
    document: ''
  })
  const [additionalDocument2Show, setAdditionalDocument2Show] = useState(false);
  const [vendorFileName, setvendorFileName] = useState('Upload')
  const [showvendor, setshowvendor] = useState(false);

  const EntityValue = EntityValueData && EntityValueData.data ? EntityValueData.data : ''

  useEffect(() => {
    const data = clientDetails && clientDetails.business && clientDetails.business
    const params = {
      entityId: data && data.entityId
    }
    // const params = {
    //   entityId: "6447ba270ead250f0852826b"
    // }

    EntityValueDispatch(params)
    const param = {
      category_id: data && data.category_id,
      subcategory_id: data && data.subcategory_id
    }

    categoryEntityDispatch(param)
  }, [])

  const IndidualHandleChange = (e, currentIndex) => {
    setCurrentInputIndex(currentIndex)
    const { name, value } = e.target
    const data = [...indualFormData]
    data[currentIndex][name] = value
    setIndualFormData(data)
  }

  const AddressHandleChange = (e, currentIndex) => {
    setAddressIndex(currentIndex)
    const { name, value } = e.target
    const data = [...addressFormData]
    data[currentIndex][name] = value
    setAddressFormData(data)
  }

  const BusinessHandleChange = (e, currentIndex) => {
    setBusinessIndex(currentIndex)
    const { name, value } = e.target
    const data = [...businessFormData]
    data[currentIndex][name] = value
    setBusinessFormData(data)
  }

  const categoryHandleChange = (e, currentIndex) => {
    setcategoryIndex(currentIndex)
    const { name, value } = e.target
    const data = [...categoryFormData]
    data[currentIndex][name] = value
    setcategoryFormData(data)
  }

  const handleAdditionalInformation = (e) => {
    const { name, value } = e.target
    if(name ==="additionalDocument1Label") {
      setAdditionalDocument1Data({...additionalDocument1Data, label:value})
    } else {
      setAdditionalDocument2Data({...additionalDocument2Data, label:value})
    }
  }

  useEffect(() => {
    if (EntityValue && !_.isEmpty(EntityValue.identityProof)) {
      const data = EntityValue && EntityValue.identityProof
      setIndualFormData(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
      setIndidualError(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
    if (EntityValue && !_.isEmpty(EntityValue.otherDocuments)) {
      const data = EntityValue && EntityValue.otherDocuments
      setAddressFormData(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
    if (EntityValue && !_.isEmpty(EntityValue.businessProof)) {
      const data = EntityValue && EntityValue.businessProof
      setBusinessFormData(data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
  }, [EntityValue])

  useEffect(() => {
    if (categoryEntityData && !_.isEmpty(categoryEntityData.data)) {
      setcategoryFormData(categoryEntityData && categoryEntityData.data.map(comment => ({
        documentName: '',
        documentNumber: '',
        uploadDocument: ''
      })))
    }
  }, [categoryEntityData])

  const handleSubmit = () => {
    // const errorMsg = identifyValidation(indualFormData, setIndidualError)
    // if (errorMsg) {
    //   onClickNext(5)
    // }
    setFullKycDetails((values) => ({
      ...values,
      indidual: indualFormData,
      adress: addressFormData,
      business: businessFormData,
      category: categoryFormData
    }))
    const data = {
      "documents": {
        "identityProof": indualFormData,
        "otherDocuments": addressFormData,
        "businessProof": businessFormData,
        "categoryProof": categoryFormData
      },
      merchantDeclarationFrom: merchantFormData && merchantFormData.merchantDeclarationFrom,
      vendorRegistrationForm: merchantFormData && merchantFormData.vendorRegistrationForm,
      additionalDocument1: additionalDocument1Data,
      additionalDocument2: additionalDocument2Data,
      responseType : "CREATED"
    }
    fullKycDispatch(kycAllDataSaved && kycAllDataSaved.id, data)
  }

  useEffect(() => {
    const typeData = CommonFileDocument && CommonFileDocument.data
    if (typeData && typeData.type === "individual" && CommonFileDocument && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      setErrors(false)
      setshowIndidual(true)
      setindividuaFileName('Uploaded')
      if (data) {
        setIndualFormData(list => list.map((item, i) =>
          i === fileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "Address" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAddressFormData(list => list.map((item, i) =>
          i === addressfileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      setshowAddress(true)
      setaddressFileName('Uploaded')
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "Business" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setBusinessFormData(list => list.map((item, i) =>
          i === businessfileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      setshowBusiness(true)
      setBusinessFileName('Uploaded')
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "category" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setcategoryFormData(list => list.map((item, i) =>
          i === categoryfileIndex
            ? {
              ...item,
              [`uploadDocument`]: data
            }
            : item
        ))
      }
      setshowcategory(true)
      setcategoryFileName('Uploaded')
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "merchant" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setmerchantFormData({
          ...merchantFormData,
          merchantDeclarationFrom: data
        })
      }
      setshowmerchant(true)
      setmerchantFileName('Uploaded')
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "vendor" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setmerchantFormData({
          ...merchantFormData,
          vendorRegistrationForm: data
        })
      }
      setshowvendor(true)
      setvendorFileName('Uploaded')
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "additionalDocument1" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAdditionalDocument1Data({
          ...additionalDocument1Data,
          document: data
        })
      }
      setAdditionalDocument1Show(true)
      ClearCommonFileDispatch()
    }
    else if (typeData && typeData.type === "additionalDocument2" && CommonFileDocument.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const data = CommonFileDocument && CommonFileDocument.data && CommonFileDocument.data.path
      if (data) {
        setAdditionalDocument2Data({
          ...additionalDocument2Data,
          document: data
        })
      }
      setAdditionalDocument2Show(true)
      ClearCommonFileDispatch()
    }
  }, [CommonFileDocument])

  const FileChangeHandler = (e) => {
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        if (name === 'individual') {
          const data = new FormData()
          data.append('type', 'individual')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'Address') {
          const data = new FormData()
          data.append('type', 'Address')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'Business') {
          const data = new FormData()
          data.append('type', 'Business')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'category') {
          const data = new FormData()
          data.append('type', 'category')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'merchant') {
          const data = new FormData()
          data.append('type', 'merchant')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'vendor') {
          const data = new FormData()
          data.append('type', 'vendor')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        }
      } else {
        setErrors({
          ...errors,
          [name]:
            warningAlert(
              'error',
              `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
              '',
              'Try again',
              '',
              () => { { } }
            )
        })
      }
    } else {
      setErrors({
        ...errors, [name]:
          warningAlert(
            'error',
            DROPZONE_MESSAGES.IMAGE_INVALID,
            '',
            'Try again',
            '',
            () => { { } }
          )
      })
    }
  }

  const handleAdditionalFileChange = (e) => {
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(PDF, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        if (name === 'additionalDocument1') {
          const data = new FormData()
          data.append('type', 'additionalDocument1')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        } else if (name === 'additionalDocument2') {
          const data = new FormData()
          data.append('type', 'additionalDocument2')
          data.append('file_to_upload', files)
          CommonFileDispatch(data)
        }
      } else {
        setErrors({
          ...errors,
          [name]:
            warningAlert(
              'error',
              `File size must below ${fileSize / 1048576} MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
              '',
              'Try again',
              '',
              () => { { } }
            )
        })
      }
    } else {
      setErrors({
        ...errors, [name]:
          warningAlert(
            'error',
            DROPZONE_MESSAGES.PDF_INVALID,
            '',
            'Try again',
            '',
            () => { { } }
          )
      })
    }
  }

  const handleClick = (event, i) => {
    hiddenFileInput.current.click(event)
    setfileIndex(i)
  }
  const AdresshandleClick = (event, i) => {
    addressFilesInput.current.click(event)
    setAddressfileIndex(i)
  }

  const categoryhandleClick = (event, i) => {
    categoryFilesInput.current.click(event)
    setcategoryfileIndex(i)
  }

  const merchanthandleClick = (event, i) => {
    merchantFilesInput.current.click(event)
  }

  const vendorhandleClick = (event, i) => {
    vendorFilesInput.current.click(event)
  }

  const handleAdditionalDocument1Click = (event, i) => {
    additionalDocument1.current.click(event)
  }

  const handleAdditionalDocument2Click = (event, i) => {
    additionalDocument2.current.click(event)
  }

  const BusinessClick = (event, i) => {
    businessFilesInput.current.click(event)
    setBusinessfileIndex(i)
  }

  const IndidualChange = (selectedOption, currentIndex) => {
    setCurrentInputIndex(currentIndex)
    setIndualValue(selectedOption)
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      const data = [...indualFormData]
      data[currentIndex]['documentName'] = selectedOption.value
      setIndualFormData(data)
    } else {
      setSelectedIndidualOption()
    }
    setErrors({ ...errors, country: '' })
  }

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      clearFullKycValueDispatch()
      onClickNext(5)
      setmerchantFileName("upload")
      setshowmerchant(false)
      setvendorFileName("upload")
      setshowvendor(false)
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [FullKycResData])


  return (
    <>
      <div className={`${CommonFileLoading ? 'opacity-25 text-primary m-5 text-center' : 'opacity-100'}`}>
        {CommonFileLoading && (
          <div>
            <span className="spinner-border text-primary opacity-100" role="status"></span>
            <span className="text-gray-800 fs-6 fw-semibold mt-5 ms-4 opacity-100">Loading...</span>
          </div>
        )}
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <div className='fv-row mb-10'>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='required fw-bold fs-4'>Upload your business documents</span>
                  </label>
                  <span className='required text-danger fw-bold fs-6'>As per RBI guidelines you are requested to upload both front and back side of documents</span>
                </div>
              </div>
              {
                _.isArray(EntityValue.identityProof) && EntityValue.identityProof.length ? (
                  <div className='row'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Identity & Address Proof
                      </label>
                    </div>
                    <div className='col-lg-4'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Document Number
                      </label>
                    </div>
                    <div className='col-lg-2'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                        Upload Doc
                      </label>
                    </div>
                  </div>
                ) : null
              }
              {
                EntityValue &&
                  _.isArray(EntityValue.identityProof) && EntityValue.identityProof.length ? (
                  EntityValue.identityProof.map((item, i) => {
                    const defaultValue = []
                    const businessProof = _.forEach(item.entity_document, function (o) {
                      defaultValue.push({
                        label: o,
                        value: o,
                        name: 'documentName'
                      })
                    })
                    return (
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-lg-6'>
                            {/* <Select
                              isClearable={currentInputIndex}
                              styles={customStyles}
                              name='country'
                              placeholder="Select..."
                              className="basic-single"
                              classNamePrefix="select"
                              onChange={(e) => {
                                IndidualChange(e, i)
                              }}
                              options={defaultValue}
                              defaultValue={() => {
                                if (currentInputIndex === i) {
                                  return selectedIndidualOption
                                } else if (_.isLength(indualFormData) > 0 ){
                                  return {
                                    value: indualFormData[i].documentName,
                                    label: indualFormData[i].documentName
                                  }
                                }
                              }}
                            /> */}
                            <select
                              name='documentName'
                              className='form-select form-select-solid mb-4'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => IndidualHandleChange(e, i)}
                            >
                              <option value=''>Select...</option>
                              {item &&
                                item.entity_document.length > 0 &&
                                item.entity_document.map((itemData, i) => (
                                  <option key={i}
                                    name='documentName'
                                    defaultValue={currentInputIndex === i ? indualFormData[i].documentName : itemData}
                                  >
                                    {itemData}
                                  </option>
                                ))}
                            </select>
                            {
                              _.isLength(indidualError) > 0 ?
                                indidualError && indidualError.documentName[i] && (
                                  <div className='rr mt-1'>
                                    <style>{'.rr{color:red;}'}</style>
                                    {indidualError.documentName[i]}
                                  </div>
                                )
                                : null}
                          </div>
                          <div className='col-lg-4'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid'
                              name='documentNumber'
                              placeholder='Document Number'
                              onChange={(e) => IndidualHandleChange(e, i)}
                            />

                            {/* {
                              indidualError.length > 0 ?
                                indidualError && indidualError.documentNumber[i] && (
                                  <div className='rr mt-1'>
                                    <style>{'.rr{color:red;}'}</style>
                                    {indidualError.documentNumber[i]}
                                  </div>
                                )
                                : null} */}
                          </div>
                          <div className='col-md-2'>
                            <input
                              type="file"
                              className="d-none"
                              name="individual"
                              id="individual"
                              multiple={false}
                              ref={hiddenFileInput}
                              accept="image/*"
                              onChange={(e) => {
                                FileChangeHandler(e)
                                e.target.value = null
                              }}
                            />
                            <button type="button"
                              className={`${showIndidual && !_.isEmpty(indualFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                              onClick={(e) => { handleClick(e, i) }}
                            // disabled={CommonFileLoading}
                            >

                              {
                                // CommonFileLoading[i]
                                //   ? (
                                //     <span
                                //       className='spinner-border spinner-border-sm mx-3'
                                //       role='status'
                                //       aria-hidden='true'
                                //     />
                                //   ) :
                                showIndidual && !_.isEmpty(indualFormData[i].uploadDocument) ?
                                  <>
                                    {
                                      individuaFileName
                                    }
                                  </>
                                  : "Upload"
                              }
                            </button>
                            {/* {indidualError && indidualError.uploadDocument[i] && (
                              <div className="rr mt-1">
                                <style>
                                  {
                                    ".rr{color:red}"
                                  }
                                </style>
                                {indidualError.uploadDocument[i]}
                              </div>
                            )} */}
                          </div>
                        </div>
                      </div>
                    )
                  })
                ) : null
              }
              {
                _.isArray(EntityValue.otherDocuments) && EntityValue.otherDocuments.length ? (
                  <div className='row'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Other Documents Proof
                      </label>
                    </div>
                    <div className='col-lg-4'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Document Number
                      </label>
                    </div>
                    <div className='col-lg-2'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                        Upload Doc
                      </label>
                    </div>
                  </div>
                ) : null
              }
              {
                EntityValue &&
                  _.isArray(EntityValue.otherDocuments) && EntityValue.otherDocuments.length ? (
                  EntityValue.otherDocuments.map((item, i) => {
                    return (
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-lg-6'>
                            <select
                              name='documentName'
                              className='form-select form-select-solid mb-4'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => AddressHandleChange(e, i)}
                            >
                              <option value=''>Select...</option>
                              {item &&
                                item.entity_document.length > 0 &&
                                item.entity_document.map((itemData, i) => (
                                  <option key={i}
                                    name='documentName'
                                    defaultValue={addressIndex === i ? addressFormData[i].documentName : itemData}
                                  >
                                    {itemData}
                                  </option>
                                ))}
                            </select>
                          </div>
                          <div className='col-lg-4'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid'
                              name='documentNumber'
                              placeholder='Document Number'
                              onChange={(e) => AddressHandleChange(e, i)}
                            />
                          </div>
                          <div className='col-md-2'>
                            <input
                              type="file"
                              className="d-none"
                              name="Address"
                              id="Address"
                              multiple={false}
                              ref={addressFilesInput}
                              accept="image/*"
                              onChange={(e) => {
                                FileChangeHandler(e)
                                e.target.value = null
                              }}
                            />
                            <button type="button"
                              className={`${showAddress && !_.isEmpty(addressFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                              onClick={(e) => { AdresshandleClick(e, i) }}
                            >
                              {
                                showAddress && !_.isEmpty(addressFormData[i].uploadDocument) ?
                                  <>
                                    {
                                      addressFileName
                                    }
                                  </>
                                  : "Upload"
                              }
                            </button>
                          </div>
                        </div>
                      </div>
                    )
                  })
                ) : null
              }

              {
                _.isArray(EntityValue.businessProof) && EntityValue.businessProof.length ? (
                  <div className='row'>
                    <div className='col-lg-6'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Business Proof
                      </label>
                    </div>
                    <div className='col-lg-4'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                        Document Number
                      </label>
                    </div>
                    <div className='col-lg-2'>
                      <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                        Upload Doc
                      </label>
                    </div>
                  </div>
                ) : null
              }
              {
                EntityValue &&
                  _.isArray(EntityValue.businessProof) && EntityValue.businessProof.length ? (
                  EntityValue.businessProof.map((item, i) => {
                    return (
                      <div className='fv-row'>
                        <div className='row'>
                          <div className='col-lg-6'>
                            <select
                              name='documentName'
                              className='form-select form-select-solid mb-4'
                              data-control='select'
                              data-placeholder='Select an option'
                              data-allow-clear='true'
                              onChange={(e) => BusinessHandleChange(e, i)}
                            >
                              <option value=''>Select...</option>
                              {item &&
                                item.entity_document.length > 0 &&
                                item.entity_document.map((itemData, i) => (
                                  <option key={i}
                                    name='documentName'
                                    defaultValue={businessIndex === i ? businessFormData[i].documentName : itemData}
                                  >
                                    {itemData}
                                  </option>
                                ))}
                            </select>
                          </div>
                          <div className='col-lg-4'>
                            <input
                              type='text'
                              className='form-control form-control-lg form-control-solid'
                              name='documentNumber'
                              placeholder='Document Number'
                              onChange={(e) => BusinessHandleChange(e, i)}
                            />
                          </div>
                          <div className='col-md-2'>
                            <input
                              type="file"
                              className="d-none"
                              name="Business"
                              id="Business"
                              multiple={false}
                              ref={businessFilesInput}
                              accept="image/*"
                              onChange={(e) => {
                                FileChangeHandler(e)
                                e.target.value = null
                              }}
                            />
                            <button type="button"
                              className={`${showbusiness && !_.isEmpty(businessFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                              onClick={(e) => { BusinessClick(e, i) }}
                            >
                              {
                                showbusiness && !_.isEmpty(businessFormData[i].uploadDocument) ?
                                  <>
                                    {
                                      businessFileName
                                    }
                                  </>
                                  : "Upload"
                              }
                            </button>
                          </div>
                        </div>
                      </div>
                    )
                  })
                ) : null
              }


              {/* categories */}

              <div className='mt-4'>
                {
                  _.isArray(categoryEntityData.data) && categoryEntityData.data.length ? (
                    <div className='row'>
                      <div className='col-lg-6'>
                        <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                          Category Proof
                        </label>
                      </div>
                      <div className='col-lg-4'>
                        <label className='d-flex align-items-center fw-bold mb-2 fs-6'>
                          Document Number
                        </label>
                      </div>
                      <div className='col-lg-2'>
                        <label className='d-flex align-items-center fw-bold mb-2 fs-7'>
                          Upload Doc
                        </label>
                      </div>
                    </div>
                  ) : null
                }
                {
                  categoryEntityData &&
                    _.isArray(categoryEntityData.data) && categoryEntityData.data.length ? (
                    categoryEntityData.data.map((item, i) => {
                      return (
                        <div className='fv-row'>
                          <div className='row'>
                            <div className='col-lg-6'>
                              <select
                                name='documentName'
                                className='form-select form-select-solid mb-4'
                                data-control='select'
                                data-placeholder='Select an option'
                                data-allow-clear='true'
                                onChange={(e) => categoryHandleChange(e, i)}
                              >
                                <option value=''>Select...</option>
                                {item &&
                                  item.entity_document.length > 0 &&
                                  item.entity_document.map((itemData, i) => (
                                    <option key={i}
                                      name='documentName'
                                      defaultValue={categoryIndex === i ? categoryFormData[i].documentName : itemData}
                                    >
                                      {itemData}
                                    </option>
                                  ))}
                              </select>
                            </div>

                            <div className='col-lg-4'>
                              <input
                                type='text'
                                className='form-control form-control-lg form-control-solid'
                                name='documentNumber'
                                placeholder='Document Number'
                                onChange={(e) => categoryHandleChange(e, i)}
                              />
                            </div>
                            <div className='col-md-2'>
                              <input
                                type="file"
                                className="d-none"
                                name="category"
                                id="category"
                                multiple={false}
                                ref={categoryFilesInput}
                                accept="image/*"
                                onChange={(e) => {
                                  FileChangeHandler(e)
                                  e.target.value = null
                                }}
                              />
                              <button type="button"
                                className={`${showcategory && !_.isEmpty(categoryFormData[i].uploadDocument) ? 'btn btn-success btn-sm' : 'btn btn-sm btn-light-primary'}`}
                                onClick={(e) => { categoryhandleClick(e, i) }}
                              >
                                {
                                  showcategory && !_.isEmpty(categoryFormData[i].uploadDocument) ?
                                    <>
                                      {
                                        categoryFileName
                                      }
                                    </>
                                    : "Upload"
                                }
                              </button>
                            </div>
                          </div>
                        </div>
                      )
                    })
                  ) : null
                }
              </div>
              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='fw-bold fs-4'>Merchant Declaration Form</span>
                  </label>
                  <div className='col-md-2'>
                    <input
                      type="file"
                      className="d-none"
                      name="merchant"
                      id="merchant"
                      multiple={false}
                      ref={merchantFilesInput}
                      accept="image/*"
                      onChange={(e) => {
                        FileChangeHandler(e)
                        e.target.value = null
                      }}
                    />
                    <button type="button"
                      className={`btn ${showmerchant ? 'btn-success btn-sm' : 'btn-sm btn-light-primary'}`}
                      onClick={(e) => { merchanthandleClick(e) }}
                    >
                      {
                        showmerchant ?
                          <>
                            {
                              merchantFileName
                            }
                          </>
                          : "Upload"
                      }
                    </button>
                  </div>
                </div>
              </div>

              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='fw-bold fs-4'>Vendor Registration Form</span>
                  </label>
                  <div className='col-md-2'>
                    <input
                      type="file"
                      className="d-none"
                      name="vendor"
                      id="vendor"
                      multiple={false}
                      ref={vendorFilesInput}
                      accept="image/*"
                      onChange={(e) => {
                        FileChangeHandler(e)
                        e.target.value = null
                      }}
                    />
                    <button type="button"
                      className={`btn ${showvendor ? 'btn-success btn-sm' : 'btn-sm btn-light-primary'}`}
                      onClick={(e) => { vendorhandleClick(e) }}
                    >
                      {
                        showvendor ?
                          <>
                            {
                              vendorFileName
                            }
                          </>
                          : "Upload"
                      }
                    </button>
                  </div>
                </div>
              </div>

              <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='fw-bold fs-4'>Other KYC Document </span>
                  </label>
                  <div className='row align-items-center'>
                    <div className='col-md-7'>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid'
                        name='additionalDocument1Label'
                        placeholder='Other KYC Document'
                        value={additionalDocument1Data.label||''}
                        onChange={(e) => handleAdditionalInformation(e)}
                      />
                    </div>
                    <div className='ms-2 col-md-2'>
                      <input
                        type="file"
                        className="d-none"
                        name="additionalDocument1"
                        id="additionalDocument1"
                        multiple={false}
                        ref={additionalDocument1}
                        accept=".pdf"
                        onChange={(e) => {
                          handleAdditionalFileChange(e)
                          e.target.value = null
                        }}
                      />
                      <button type="button"
                        className={`btn ${additionalDocument1Show ? 'btn-success btn-sm' : 'btn-sm btn-light-primary'}`}
                        onClick={(e) => { handleAdditionalDocument1Click(e) }}
                      >{additionalDocument1Show ? "Uploaded" : "Upload"}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className='row mb-4'>
                <div className='col-lg-12'>
                  <label className='d-flex align-items-center fw-bold mb-2'>
                    <span className='fw-bold fs-4'>Other KYC Document 2</span>
                  </label>
                  <div className='row align-items-center'>
                    <div className='col-md-7'>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid'
                        name='additionalDocument2Label'
                        placeholder='Additional Information'
                        value={additionalDocument2Data.label||''}
                        onChange={(e) => handleAdditionalInformation(e)}
                      />
                    </div>
                    <div className='col-md-2 ms-2'>
                      <input
                        type="file"
                        className="d-none"
                        name="additionalDocument2"
                        id="additionalDocument2"
                        multiple={false}
                        ref={additionalDocument2}
                        accept=".pdf"
                        onChange={(e) => {
                          handleAdditionalFileChange(e)
                          e.target.value = null
                        }}
                      />
                      <button type="button"
                        className={`btn ${additionalDocument2Show ? 'btn-success btn-sm' : 'btn-sm btn-light-primary'}`}
                        onClick={(e) => { handleAdditionalDocument2Click(e) }}
                      >
                        {additionalDocument2Show ? "Uploaded" : "Upload"}
                      </button>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>

            <div className='row'>
              <div className='d-flex justify-content-end pt-10'>
                <div className='me-2'>
                  <button type='submit' className='btn btn-sm btn-light-primary'
                    onClick={() => { handleSubmit() }}
                    disabled={FullKycLoading}
                  >
                    {!FullKycLoading &&
                      <span className='indicator-label'>
                        <i className='bi bi-person-fill' />
                        Proceed
                      </span>
                    }
                    {FullKycLoading && (
                      <span className='indicator-progress' style={{ display: 'block' }}>
                        Please wait...
                        <span className='spinner-border spinner-border-sm align-middle ms-2' />
                      </span>
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { CommonFileStore, EntityValueTypeStore, FullKycValueStore, categoryEntityStore } = state
  return {
    CommonFileDocument: CommonFileStore && CommonFileStore.commonFileRes ? CommonFileStore.commonFileRes : {},
    CommonFileLoading: CommonFileStore && CommonFileStore.loading ? CommonFileStore.loading : false,
    EntityValueData: EntityValueTypeStore && EntityValueTypeStore.EntityValue ? EntityValueTypeStore.EntityValue : {},
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
    categoryEntityData: categoryEntityStore && categoryEntityStore.categoryEntityData ? categoryEntityStore.categoryEntityData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  CommonFileDispatch: (params) => dispatch(CommomFileAction.CommonFile(params)),
  ClearCommonFileDispatch: (params) => dispatch(CommomFileAction.clearCommonFile(params)),
  EntityValueDispatch: (params) => dispatch(EntityValueAction.EntityValueData(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params)),
  categoryEntityDispatch: (params) => dispatch(categoryEntityAction.categoryEntity(params)),

})

export default connect(mapStateToProps, mapDispatchToProps)(IdentityVerification)