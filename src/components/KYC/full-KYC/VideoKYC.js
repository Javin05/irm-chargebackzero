import React, { useEffect, useRef, useCallback, useState, useMemo } from "react"
import Webcam from "react-webcam"
import ImageCapture from 'react-image-data-capture'
import { connect } from 'react-redux'
import {
  CityActions,
  CommomFileAction,
  IndidualPanUploadAction,
  form80UploadAction,
  form12UploadAction
} from '../../../store/actions'

const videoConstraints = {
  width: 300,
  height: 200,
  facingMode: "environment",
  facingMode: "user"
}

const Camera = (props) => {
  const {
    SetFrontSow,
    SetSecondShow,
    secondShow,
    SetProfileShow,
    ProfileShow,
    frontShow,
    onClickNext,
    form80UploadDispatch,
    setFullKycDetails
  } = props
  const webcamRef = useRef(null)
  const [url, setUrl] = useState(null)

  const capturePhoto = useCallback(async () => {
    const imageSrc = webcamRef.current.getScreenshot()
    setUrl(imageSrc)
    console.log('imageUrl', imageSrc)
    if (frontShow) {
      const data = new FormData()
      data.append("file_to_upload", imageSrc)
      setFullKycDetails((values) => ({ ...values, vedioKyc_Front: imageSrc }))
    }
    if (secondShow) {
      setFullKycDetails((values) => ({ ...values, vedioKyc_Back: imageSrc }))
    }
    if (ProfileShow) {
      setFullKycDetails((values) => ({ ...values, vedioKyc_Face: imageSrc }))
    } else (
      setFullKycDetails((values) => ({ ...values, vedioKyc_Face: imageSrc }))
    )
  }, [webcamRef])

  const onUserMedia = (e) => {
    console.log('webCam', e)
  }

  return (
    <>
      <div id="imageWrapper" />
      <canvas id="canvas" width="5" height="5" />
      {
        !url ? (
          <>
            <div className="d-flex justify-content-center mt-4">
              <Webcam
                ref={webcamRef}
                audio={false}
                screenshotFormat="image/jpeg"
                videoConstraints={videoConstraints}
                onUserMedia={onUserMedia}
                style={{
                  borderRadius: '20px',
                  backgroundSize: 'cover'
                }}
                className='image-input-wrapper'
              />
              <div>
                {/* <ImageCapture
                onCapture={onCapture}
                onError={onError}
                width={300}
                userMediaConfig={config}
              /> */}
              </div>
            </div>
            <div className="d-flex justify-content-center mt-4">
              <button
                className="btn btn-sm btn-light-primary"
                onClick={capturePhoto}
              >
                <i className="bi bi-camera-fill fs-2x" />
              </button>
            </div>
          </>
        ) : (
          <>
            <div className="row">
              <div className="col-lg-2" />
              <div className="col-lg-8 ms-4">
                <div className="card card-custom overlay overflow-hidden">
                  <div className="card-body p-0">
                    <div className="overlay-wrapper">
                      <img
                        src={url}
                        className="w-100 rounded"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-2" />
            </div>
            <div className="d-flex justify-content-center mt-4">
              <button
                className="btn btn-sm btn-light-danger me-4"
                onClick={() => setUrl(null)}
              >
                Re Capture
              </button>
              <button
                className="btn btn-sm btn-light-primary"
                onClick={() => {
                  if (frontShow) {
                    SetFrontSow(false)
                    SetSecondShow(true)
                  } else if (secondShow) {
                    SetFrontSow(false)
                    SetSecondShow(false)
                    SetProfileShow(true)
                  } else if (ProfileShow) {
                    SetFrontSow(false)
                    SetSecondShow(false)
                    SetProfileShow(false)
                    onClickNext(5)
                  } else {
                    onClickNext(8)
                  }
                }}
              >
                {
                  frontShow ? 'Next' : secondShow ? 'Next' : ProfileShow ? 'Finish' : 'Next'
                }
              </button>

            </div>
          </>
        )
      }
    </>
  )
}


const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, dropzoneStore, CommonFileStore, IndidualPanStore, Form80Store, Form12Store } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  form80UploadDispatch: (params) => dispatch(form80UploadAction.form80Upload(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Camera)
