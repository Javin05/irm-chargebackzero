import React, { useEffect, useState, useContext } from "react"
import Card from "react-bootstrap/Card"
import Accordion from "react-bootstrap/Accordion"
import { KYC_STATUS, RISKSTATUS, DASHBOARD_SCORE_COLOR, UPDATE_PERMISSION, STATUS_RESPONSE } from '../../utils/constants'
import Status from '../kycDashboard/ARH'
import _ from "lodash"
import { Can } from '../../theme/layout/components/can'
import { getUserPermissions } from '../../utils/helper'
import { KYCCommentActions, KYCCommentdeleteActions } from "../../store/actions"
import { useLocation } from 'react-router-dom'
import { connect } from "react-redux"
import { KTSVG } from "../../theme/helpers"
import { warningAlert, confirmationAlert, successAlert } from "../../utils/alerts"
import moment from "moment"
import KycCommentEdit from "./KycCommentEdit"
import { KycContext } from '../kycDashboard/StaticComponent'
import Table from 'react-bootstrap/Table';

function ProfileKycDasboard(props) {
  const {
    KycScoreres,
    getKYCCommentDispatch,
    KYCComments,
    loading,
    deleteKYCCommentDispatch,
    clearDeleteKYCCommentDispatch,
    DeleteKYCCommentData
  } = props
  const userdata = useContext(KycContext)
  const pathName = '/KYC'
  const getUsersPermissions = getUserPermissions(pathName, true)
  const kycpathId = useLocation().pathname
  const url = kycpathId && kycpathId.split("static-summary/update/")
  const id = url && url[1]
  const [editMode, setEditMode] = useState(false)
  const [userId, setUserId] = useState('')

  useEffect(() => {
    const params = {
      kycId: id
    }
    getKYCCommentDispatch(params)
  }, [id])

  const onDeleteItem = (id) => {
    confirmationAlert(
      "Are you sure want to delete this comment,",
      "",
      'warning',
      'Yes',
      'No',
      () => {
        deleteKYCCommentDispatch(id)
      },
      () => { { } }
    )
  }

  useEffect(() => {
    if (DeleteKYCCommentData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const params = {
        kycId: id
      }
      successAlert(
        DeleteKYCCommentData && DeleteKYCCommentData.message,
        'success'
      )
      getKYCCommentDispatch(params)
      clearDeleteKYCCommentDispatch()
    } else if (DeleteKYCCommentData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        DeleteKYCCommentData.message,
        '',
        'Ok'
      )
    }
    clearDeleteKYCCommentDispatch()
  }, [DeleteKYCCommentData])

  return (
    <div className="row">
      <div className="col-lg-12">
        <div className="card card-stretch card-bordered">
          <div
            className={`card card-custom card-stretch gutter-b p-4 w-100 ${KYC_STATUS[userdata && userdata.kycStatus]
              }`}
          >
            <h2 className="card-title text-white d-flex justify-content-center">{userdata && userdata.kycStatus}</h2>
          </div>
          <div className="card-body">
            <Can
              permissons={getUsersPermissions}
              componentPermissions={UPDATE_PERMISSION}
            >
              <Status UserDetails={userdata} getKYCCommentDispatch={getKYCCommentDispatch} />
            </Can>
            <div className="separator separator-dashed p-2" />
            {KYCComments && KYCComments.data && KYCComments.data.length > 0 ?
              <Accordion>
                <Accordion.Item
                  eventKey="0"
                >
                  <Accordion.Header>
                    <h4>Comments</h4>
                  </Accordion.Header>
                  <Accordion.Body>
                    <Table  responsive>
                      <thead>
                        <tr>
                          <th className="fs-6 fw-bold">Status</th>
                          <th className="fs-6 fw-bold">Comments</th>
                          <th className="fs-6 fw-bold">Updated By</th>
                          <th className="fs-6 fw-bold">Date</th>
                          <th className="fs-6 fw-bold text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          Array.isArray(KYCComments && KYCComments.data) ?
                            KYCComments && KYCComments.data.map((item, i) => {
                              return (
                                <tr
                                  style={{
                                    flexDirection: 'row',
                                    backgroundColor: i % 2 === 0 ? '#EBFCFF' : '#FAFAFA',
                                    paddingVertical: 5
                                  }}
                                  key={i}
                                >
                                  <td><span className={`badge ${RISKSTATUS[item.kycStatus]}`}>
                                    {item.kycStatus ? item.kycStatus : "--"}</span>
                                  </td>
                                  <td> {item.comments}</td>
                                  <td className="text-center">
                                    <p>{item.updatedUserName ? `${item.updatedUserName} (${item.updatedUserEmail})` : '--'}</p>
                                    <div>{item.updatedUserRole ? item.updatedUserRole : "--"}</div>
                                  </td>
                                  <td> {moment(item.createdAt).format('DD/MM/YYYY HH:mm:ss')}</td>
                                  <td className="text-center">
                                    {/* <div className="d-flex align-item-center justify-content-center"> */}
                                      <button
                                        className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px'
                                        title="Edit Data"
                                        onClick={() => {
                                          setEditMode(true)
                                          setUserId(item)
                                        }}
                                      >
                                        <KTSVG
                                          path='/media/icons/duotune/art/art005.svg'
                                          className='svg-icon-3'
                                        />
                                      </button>
                                      {/* <button
                                        className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-4'
                                        onClick={() => onDeleteItem(item._id)}
                                        title="Delete"
                                      >
                                        <KTSVG
                                          path='/media/icons/duotune/general/gen027.svg'
                                          className='svg-icon-3'
                                        />
                                      </button> */}
                                    {/* </div> */}
                                  </td>
                                </tr>
                              )
                            })
                            : (
                              <span className='text-dark fw-semibold fs-6 ms-4'>
                                No Records Found
                              </span>
                            )
                        }
                      </tbody>
                    </Table>
                    {/* <ul style={{ listStyleType: 'disc', paddingLeft: '1.5rem' }}>

                      {
                        KYCComments && Array.isArray(KYCComments.data) ? KYCComments.data.map(data =>
                          <div className="d-flex justify-content-between mb-2">
                            <div>
                              <div className="d-flex align-item-center">
                                <div>
                                  <li>{data.kycStatus}:&nbsp;</li>
                                </div>
                                <div>
                                  &nbsp;{data.comments}
                                </div>
                              </div>
                              <div>
                                Created Date:&nbsp; {moment(data.createdAt).format('DD/MM/YYYY')}
                              </div>
                            </div>
                            <div className="d-flex align-item-center">
                              <button
                                className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px'
                                title="Edit Data"
                                onClick={() => {
                                  setEditMode(true)
                                  setUserId(data)
                                }}
                              >
                                <KTSVG
                                  path='/media/icons/duotune/art/art005.svg'
                                  className='svg-icon-3'
                                />
                              </button>
                              <button
                                className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-4'
                                onClick={() => onDeleteItem(data._id)}
                                title="Delete"
                              >
                                <KTSVG
                                  path='/media/icons/duotune/general/gen027.svg'
                                  className='svg-icon-3'
                                />
                              </button>
                            </div>
                          </div>
                        ) : (
                          null
                        )
                      }
                    </ul> */}
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion> : null}
            <div className="separator separator-dashed" />
            <Accordion>
              <Accordion.Item
              // eventKey="1"
              >
                <Accordion.Header>
                  <h4>
                    {" "}
                    eKYC{" "}
                    <span
                      className={` ms-4 badge ${RISKSTATUS[userdata && userdata.status]}`}
                    >
                      {userdata && userdata.status}
                    </span>
                  </h4>{" "}
                </Accordion.Header>
                <Accordion.Body>
                  <Card>
                    <>
                      <div className="row">
                        <div className="col-lg-12">
                          {
                            KycScoreres &&
                              _.isArray(KycScoreres.data) ? (
                              KycScoreres.data.map((item, i) => {
                                return (
                                  <div key={i} className={`alert ${DASHBOARD_SCORE_COLOR[item && item.colour]} d-flex align-items-center`}>
                                    {
                                      item && item.header === 'Mobile Number' || item.header === 'Email' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'} -
                                              <span className="fw-bold fs-7 ms-1">
                                                {item && item.value ? item.value : '--'}
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Black Listed - {item && item.blackListed ? item.blackListed : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Verified - {item && item.verified ? item.verified : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }
                                    {
                                      item && item.header === 'Individual PAN Card' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'} -
                                              <span className="fw-bold fs-7 ms-1">
                                                {item && item.value ? item.value : '--'}
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              ValidPan - {item && item.validPan ? item.validPan : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Name Match - {item && item.nameMatch ? item.nameMatch : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }

                                    {
                                      item && item.header === 'Company PAN Card' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'} -
                                              <span className="fw-bold fs-7 ms-1">
                                                {item && item.value ? item.value : '--'}
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              ValidPan - {item && item.validPan ? item.validPan : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Name Match - {item && item.nameMatch ? item.nameMatch : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }
                                    {
                                      item && item.header === 'PAN Card of Authorized Signatory' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'} -
                                              <span className="fw-bold fs-7 ms-1">
                                                {item && item.value ? item.value : '--'}
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              ValidPan - {item && item.validPan ? item.validPan : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Name Match - {item && item.nameMatch ? item.nameMatch : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }
                                    {
                                      item && item.header === 'Aadhaar Card' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'} -
                                              <span className="fw-bold fs-7 ms-1">
                                                {item && item.value ? item.value : '--'}
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Valid Aadhar - {item && item.validAadhar ? item.validAadhar : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }
                                    {
                                      item && item.header === 'Document Submission' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'} -
                                              <span className="fw-bold fs-7 ms-1">
                                                {item && item.submission ? item.submission : '--'}
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Approval Document - {item && item.approvalDocument.toString() ? item.approvalDocument.toString() : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Rejected Document - {item && item.rejectedDocument.toString() ? item.rejectedDocument.toString() : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }
                                    {
                                      item && item.header === 'Bank Details' ? (
                                        <div className="row">
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-4 text-black mb-2">{item && item.header ? item.header : '--'}
                                              <span className="fw-bold fs-7 ms-1">
                                                ( Cheque OCR vs Bank Info )
                                              </span>
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              Account Verified - {item && item.accountVerified ? item.accountVerified : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              AccountName Match - {item && item.accountNameMatch ? item.accountNameMatch : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              AccountNumber Match - {item && item.accountNumberMatch ? item.accountNumberMatch : '--'}
                                            </div>
                                          </div>
                                          <div className="col-lg-12">
                                            <div className="fw-bold fs-7 mt-2">
                                              IFSC Code Match - {item && item.ifscCodeMatch ? item.ifscCodeMatch : '--'}
                                            </div>
                                          </div>
                                        </div>
                                      ) : null
                                    }
                                  </div>
                                )
                              })) : null
                          }
                        </div>
                      </div>
                    </>
                  </Card>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
            <KycCommentEdit editMode={editMode} setEditMode={setEditMode} userId={userId} setUserId={setUserId} id={id} />
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  const { KYCCommentlistStore, KYCCommentdeleteStore } = state
  return {
    KYCComments: KYCCommentlistStore && KYCCommentlistStore.KYCCommentlists ? KYCCommentlistStore.KYCCommentlists : '',
    loading: KYCCommentlistStore && KYCCommentlistStore.loading ? KYCCommentlistStore.loading : false,
    DeleteKYCCommentData: KYCCommentdeleteStore && KYCCommentdeleteStore.DeleteKYCCommentData ? KYCCommentdeleteStore.DeleteKYCCommentData : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYCCommentDispatch: (params) => dispatch(KYCCommentActions.getKYCCommentlist(params)),
  deleteKYCCommentDispatch: (params) => dispatch(KYCCommentdeleteActions.delete(params)),
  clearDeleteKYCCommentDispatch: (params) => dispatch(KYCCommentdeleteActions.clear(params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileKycDasboard)
