
import React, { useContext } from "react"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import { KYC_VERIFY, KYC_VERIFY_DATA } from "../../utils/constants"
import { KycContext } from '../kycDashboard/StaticComponent'

export default function Contacts() {
  const userdata = useContext(KycContext)
  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <h2> Contacts</h2>{" "}
          <i className="fa fa-times-circle-o" aria-hidden="true"></i>
        </Accordion.Header>
        <Accordion.Body>
          <Card>
            <Card.Body>
              <div
                className="row block_div"
              >
                <>
                  <table className="table table-striped">
                    <tbody>
                      <tr>
                        <th className="fs-4 fw-bold">Phone</th>
                        <td>
                          {userdata && userdata.phoneNumber
                            && userdata.phoneNumber.number ? userdata.phoneNumber.number : '--'}
                          <span
                            className={`ms-2 badge badge-sm ${KYC_VERIFY[userdata && userdata.phoneNumber
                              && userdata.phoneNumber.verified]}`}
                          >
                            {KYC_VERIFY_DATA[userdata && userdata.phoneNumber
                              && userdata.phoneNumber.verified]}
                          </span>
                        </td>
                      </tr>
                      <tr>
                        <th className="fs-4 fw-bold">Email</th>
                        <td>
                          {userdata && userdata.emailId
                            && userdata.emailId.emailId ? userdata.emailId.emailId : '--'}
                          <span
                            className={`ms-2 badge badge-sm ${KYC_VERIFY[userdata && userdata.emailId
                              && userdata.emailId.verified]}`}
                          >
                            {KYC_VERIFY_DATA[userdata && userdata.emailId
                              && userdata.emailId.verified]}
                          </span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </>
              </div>
            </Card.Body>
          </Card>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
