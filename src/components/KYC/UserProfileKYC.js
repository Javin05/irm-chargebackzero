import React, { useEffect, useState, useContext, useCallback } from "react"
import Tab from "react-bootstrap/Tab"
import Tabs from "react-bootstrap/Tabs"
import Card from "react-bootstrap/Card"
import ShowFields from "./ShowFields"
import DocumentView from "./DocumentView"
import Address from "./Address"
import Contacts from "./Contacts"
import ProfileKycDasboard from "./ProfileKycDasboard"
import { Link, useLocation, useParams } from "react-router-dom"
import _ from "lodash"
import { KTSVG } from "../../theme/helpers"
import { KYCUserAction, KycDocumentUpdateHistoryActions } from "../../store/actions"
import { connect } from "react-redux"
import { get } from "http"
import CrossVerify from './CrossVerify'
import { RISKSTATUS, KYC_SCORE_STATUS, STATUS_RESPONSE, FIND_FORMATE } from "../../utils/constants"
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts"
import BankVerifyData from './BankVerify'
import BusinessData from './BusinessDetailss'
import KycVideo from "./KycVideo"
import AdditionalDocument from "./AdditionalDocument"
import { KycContext } from '../kycDashboard/StaticComponent'
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal'
import moment from "moment"
import { Scrollbars } from 'react-custom-scrollbars';

function UserProfileKYC(props) {
  const {
    isLoading,
    DistanceRes,
    DashboardAadharRes,
    DashboardPANres,
    DashboardCINres,
    KycScoreres,
    VideoKycRes,
    UENdashboardRes,
    KycStatusres,
    getKYCUserDetailsDispatch,
    UpdateKycDispatch,
    FullKycResData,
    FullKycResDataLoading,
    getUpdateHistoryListDispatch,
    DocumentUpdateHistoryLoading,
    DocumentUpdateHistoryData,
    clearUpdateHistoryListDispatch
  } = props

  const userdata = useContext(KycContext)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const [documentHistoryShow, setDocumentHistoryShow] = useState(false)
  const [CurrentValue, setCurrentValue] = useState('')
  const [ViewerOpen, setViewerOpen] = useState(false)
  const [multipleList, setMultipleList] = useState(false)
  const [multipleListValue, setMultipleListValue] = useState('')
  const [header, setHeader] = useState('')

  useEffect(() => {
    if (KycStatusres && KycStatusres.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(KycStatusres && KycStatusres.message, "success")
      getKYCUserDetailsDispatch(id)
    } else if (KycStatusres && KycStatusres.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KycStatusres && KycStatusres.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KycStatusres])

  useEffect(() => {
    if (DocumentUpdateHistoryData && DocumentUpdateHistoryData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setDocumentHistoryShow(true)
    } else if (DocumentUpdateHistoryData && DocumentUpdateHistoryData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DocumentUpdateHistoryData && DocumentUpdateHistoryData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [DocumentUpdateHistoryData])


  const onKycDocumentHistory = () => {
    getUpdateHistoryListDispatch(id)
  }

  const clear = () => {
    setDocumentHistoryShow(false)
    clearUpdateHistoryListDispatch()
  }

  const openImageViewer = useCallback((index) => {
    setCurrentValue(index)
    setViewerOpen(true)
  }, [])

  const openMultipleList = (index, value) => {
    setMultipleList(true)
    setMultipleListValue(index)
    setHeader(value)
  }

  return (
    <>
      <Modal
        show={multipleList}
        size="lg"
        centered
        onHide={() => {setMultipleList(false)
        setMultipleListValue('')
        setHeader('')}}
        >
        <Modal.Header
          style={{
            backgroundColor: 'rgb(94, 98, 120)'
          }}
          closeButton={() => {setMultipleList(false)
            setMultipleListValue('')
            setHeader('')}}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {header}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-10">
              <div className="card card-custom overlay overflow-hidden">
                <div className="card-body p-0">
                  <div className="overlay-wrapper">
                    {multipleListValue && multipleListValue.map((item)=>
                      <div className="d-flex align-items-center">
                        <div>{item.label} :</div>
                        <div className="ms-2">{item.value}</div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={ViewerOpen}
        size="lg"
        centered
        onHide={() => setViewerOpen(false)}>
        <Modal.Header
          style={{
            backgroundColor: 'rgb(94, 98, 120)'
          }}
          closeButton={() => setViewerOpen(false)}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Image
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-1" />
            <div className="col-lg-10">
              <div className="card card-custom overlay overflow-hidden">
                <div className="card-body p-0">
                  <div className="overlay-wrapper">
                    <img
                      src={CurrentValue}
                      alt=""
                      className="w-100 rounded"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-1" />
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={documentHistoryShow}
        size="xl"
        centered
        onHide={() => clear()}
      >
        <Modal.Header
          style={{
            backgroundColor: 'rgb(94, 98, 120)'
          }}
          closeButton={() => clear()}>
          <Modal.Title
            style={{
              color: 'white',
            }}
          >
            Change Log
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="table-responsive">
            <div className="scroll h-650px">

              <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
                <thead>
                  <tr>
                    <th className="fs-6 fw-bold text-center">Name</th>
                    <th className="fs-6 fw-bold text-center">Previous Value</th>
                    <th className="fs-6 fw-bold text-center">Current Value</th>
                    <th className="fs-6 fw-bold text-center">Type</th>
                    <th className="fs-6 fw-bold text-center">Updated By</th>
                    <th className="fs-6 fw-bold text-center w-150px">Updated On</th>
                  </tr>
                </thead>
                <tbody
                >
                  {
                    Array.isArray(DocumentUpdateHistoryData && DocumentUpdateHistoryData.data) ?
                      DocumentUpdateHistoryData && DocumentUpdateHistoryData.data.map((item, i) => {
                        return (
                          <tr
                            style={{
                              flexDirection: 'row',
                              backgroundColor: i % 2 === 0 ? '#EBFCFF' : '#FAFAFA',
                              paddingVertical: 5
                            }}
                          >
                            <td className="text-center"> {item && item.updatedDatas && item.updatedDatas.name ? item.updatedDatas.name : '--'}</td>
                            <td className="text-center">
                              {item && item.updatedDatas && _.isArray(item.updatedDatas.previousValue) ?
                                <a
                                  className="btn btn-sm btn-light-primary btn-shadow"
                                  onClick={() => openMultipleList(item.updatedDatas.previousValue, "Previous Value")}
                                >
                                  View
                                </a> :
                                item && item.updatedDatas && item.updatedDatas.previousValue ? (
                                  item.updatedDatas.previousValue.startsWith('https://') &&
                                    FIND_FORMATE.some(format => item.updatedDatas.previousValue.endsWith(format)) ? (
                                    <a
                                      className="btn btn-sm btn-light-primary btn-shadow"
                                      onClick={() => openImageViewer(item.updatedDatas.previousValue)}
                                    >
                                      View
                                    </a>
                                  ) : 
                                  item.updatedDatas.previousValue.startsWith('https://') &&
                                  item.updatedDatas.previousValue.endsWith("pdf") ? (
                                    <a
                                      href={item.updatedDatas.previousValue}
                                      className="btn btn-sm btn-light-primary btn-shadow"
                                      target="_blank"
                                    >
                                      View
                                    </a>
                                  ) : (
                                    item.updatedDatas.previousValue
                                  )
                                ) : (
                                  '--'
                                )}
                            </td>
                            <td className="text-center">
                              {item && item.updatedDatas && _.isArray(item.updatedDatas.value) ?
                                <a
                                  className="btn btn-sm btn-light-primary btn-shadow"
                                  onClick={() => openMultipleList(item.updatedDatas.value, "Current Value")}
                                >
                                  View
                                </a>:
                                item && item.updatedDatas && item.updatedDatas.value ? (
                                  item.updatedDatas.value.startsWith('https://') &&
                                    FIND_FORMATE.some(format => item.updatedDatas.value.endsWith(format)) ? (
                                    <a
                                      className="btn btn-sm btn-light-primary btn-shadow"
                                      onClick={() => openImageViewer(item.updatedDatas.value)}
                                    >
                                      View
                                    </a>
                                  ): 
                                  item.updatedDatas.value.startsWith('https://') &&
                                  item.updatedDatas.value.endsWith("pdf") ? (
                                    <a
                                      href={item.updatedDatas.value}
                                      className="btn btn-sm btn-light-primary btn-shadow"
                                      target="_blank"
                                    >
                                      View
                                    </a>
                                  ) : (
                                    item.updatedDatas.value
                                  )
                                ) : (
                                  '--'
                                )}
                            </td>
                            <td className="text-center">
                              {item && item.type ? item.type : '--'}
                            </td>
                            <td className="text-center">
                              <div>{item && item.updatedUserName || item.updatedUserEmail ? `${item.updatedUserName} (${item.updatedUserEmail})` : '--'}</div>
                              <div>{item && item.updatedUserRole ? item.updatedUserRole : '--'}</div>
                            </td>
                            <td className="text-center"> {item && item.createdAt ? moment(item.createdAt).format('DD/MM/YYYY HH:mm:ss') : '--'}</td>
                          </tr>
                        )
                      })
                      : (
                        <span className='text-dark fw-semibold fs-6 ms-4 text-center'>
                          No Records Found
                        </span>
                      )
                  }
                </tbody>
              </table>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div className="row mx-1 bg-white">
        <div className="col-sm-12 col-md-12 col-lg-12">
          <div className="d-flex justify-content-end">
            <button
              className="btn btn-sm btn-light-dark px-2 ms-2 w-100px"
              onClick={() => onKycDocumentHistory()}
            >
              {
                DocumentUpdateHistoryLoading ? (
                  'please wait...'
                ) : 'Change Log'
              }
            </button>
          </div>
          <Card>
            {userdata &&
              typeof userdata === "object" &&
              typeof userdata !== "undefined" &&
              userdata ? (
              <div className="row">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                  <legend className="float-none w-auto fs-3 p-2 h6 font-weight-600 ms-2 mt-4">
                    Contact Information
                  </legend>
                  <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Primary Contact Name :
                      </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="fw-bolder fs-6 ">
                        {userdata && userdata.primaryContactName ? userdata.primaryContactName : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="ms-4 text-gray-600 fw-bold fs-5">
                        Phone Number :
                      </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="fw-bolder fs-6 ">
                        {userdata && userdata.phoneNumber && userdata.phoneNumber.number ? userdata.phoneNumber.number : '--'}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="ms-4 text-gray-600 fw-bold fs-5 ">
                        Email :
                      </div>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div className="fw-bolder fs-6 ">
                        {userdata && userdata.emailId && userdata.emailId.emailId ? userdata.emailId.emailId : '--'}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-4">
                  <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div className="row">
                      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div className="ms-4 text-gray-600 fw-bold fs-5">
                          KYC Id : <span className="text-black fw-bolder fs-6">#KYC{userdata && userdata.kycId}</span>
                        </div>
                      </div>
                      {/* <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <div className="fw-bolder fs-6">
                          #KYC{userdata && userdata.kycId}
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 mt-4 mb-4">
                      <span className="ms-4 fs-5 fw-bolder">
                        eKYC
                      </span>
                      {
                        userdata && userdata.emailIdVerification === 'YES' &&
                        <span className="ms-4 fs-7 fw-bolder">
                          <i className={` me-1 ${KYC_SCORE_STATUS[userdata && userdata.emailId && userdata.emailId.verified]}`} />
                          Email
                        </span>
                      }
                      {
                        userdata && userdata.phoneNumberVerification === 'YES' &&
                        <span className="ms-4 fs-7 fw-bolder">
                          <i className={`me-1 ${KYC_SCORE_STATUS[userdata && userdata.phoneNumber && userdata.phoneNumber.verified]}`} />
                          Phone
                        </span>
                      }
                      <span className="ms-4 fs-7 fw-bolder">
                        {
                          userdata && userdata.documentVerification === true ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Document
                      </span>
                      <span className="ms-4 fs-7 fw-bolder">
                        {
                          userdata && userdata.businessAddress && userdata.businessAddress.addressVerification === true ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Business Address
                      </span>
                      <span className="ms-4 fs-7 fw-bolder">
                        {
                          userdata && userdata.bankVerificationStatus === "APPROVED" ? (
                            <i className="bi bi-check-circle-fill ms-2 me-1"
                              style={{
                                color: "rgb(122 231 174)",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          ) : (
                            <i className="bi bi-x-circle-fill ms-2 me-1"
                              style={{
                                color: "red",
                                backgroundColor: "transparent",
                                fontSize: "0.95rem",
                              }}
                            />
                          )
                        }
                        Bank Details
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className="d-flex justify-content-center">
                <div className=" spinner-border text-primary m-5" role="status" />
              </div>
            )}
          </Card>
        </div>
      </div>
      <div className='row mt-8'>
        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
          <div className="row">
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <DocumentView
                DashboardAadharRes={DashboardAadharRes}
                DashboardPANres={DashboardPANres}
                DashboardCINres={DashboardCINres}
                UENdashboardRes={UENdashboardRes}
              />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <CrossVerify DashboardPANres={DashboardPANres} />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <Address DistanceRes={DistanceRes} />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <BankVerifyData />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <BusinessData />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <Contacts />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <KycVideo objectId={id} />
            </div>
            <div className='col-sm-12 col-md-12 col-lg-12'>
              <AdditionalDocument objectId={id} UpdateKycDispatch={UpdateKycDispatch} FullKycResData={FullKycResData} FullKycResDataLoading={FullKycResDataLoading} />
            </div>
          </div>
        </div>
        <div className="col-sm-4 col-md-4 col-lg-4">
          <ProfileKycDasboard KycScoreres={KycScoreres} />
        </div>
      </div>
      {/* ) : (
        <div className="d-flex justify-content-center py-5">
          <div className="spinner-border text-primary m-5" role="status" />
        </div>
      )} */}
    </>
  )
}

const mapStateToProps = (state) => {
  const { KycScoreStore, KycStatusStore, KycDocumentUpdateHistoryStore } = state

  return {
    KycScoreres: KycScoreStore && KycScoreStore.KycScoreres ? KycScoreStore.KycScoreres : {},
    KycStatusres: KycStatusStore && KycStatusStore.KycStatusres ? KycStatusStore.KycStatusres : {},
    DocumentUpdateHistoryLoading: KycDocumentUpdateHistoryStore && KycDocumentUpdateHistoryStore.loading ? KycDocumentUpdateHistoryStore.loading : false,
    DocumentUpdateHistoryData: KycDocumentUpdateHistoryStore && KycDocumentUpdateHistoryStore.KycDocumentUpdateHistoryData ? KycDocumentUpdateHistoryStore.KycDocumentUpdateHistoryData : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
  getUpdateHistoryListDispatch: (id) => dispatch(KycDocumentUpdateHistoryActions.getUpdateHistoryList(id)),
  clearUpdateHistoryListDispatch: () => dispatch(KycDocumentUpdateHistoryActions.clear())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileKYC)
