import React, { FC, useEffect, useRef, useState } from "react";
import { KTSVG } from "../../theme/helpers";
import { connect } from "react-redux";
import { DocumentVerifyValidation } from "./Validation";
import {
  setLocalStorage,
  getLocalStorage,
  removeLocalStorage,
} from "../../utils/helper";
import { KYC_FORM } from "../../utils/constants";
import _, { values } from "lodash";
import Captcha from "demos-react-captcha";
import {
  STATUS_RESPONSE,
  SWEET_ALERT_MSG,
  STATUS_BADGE,
  DROPZONE_MESSAGES,
  FILE_FORMAT_CB_DOCUMENT,
  DROPZONE_IMAGE_NAME_TYPES,
  DROPZONE_IMAGE_FLAG,
  FILE_FORMAT_TYPE,
} from "../../utils/constants";
import Dropzone from "../../utils/dropzone/Dropzone";
import { KYCAddAction, KYCActions } from "../../store/actions";
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts";

function DocumentVerification(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    clientDetails,
    KycPostDispatch,
    getKYCAdd,
    clearAddKYC,
    getKYClistDispatch,
  } = props;

  const [errors, setErrors] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [showForm, setShowForm] = useState(true);
  const [isFormUpdated, setFormUpdated] = useState(false);
  const hiddenFileInput = useRef(null);
  const [fileName, setFileName] = useState();
  const [defaultImages, setDefaultImages] = useState([]);
  const [dropzoneError, setDropzoneError] = useState();

  const [formData, setFormData] = useState({
    aadharVerification: "",
    captcha: "",
    aadharnotLinkedMyNumber: "",
    businessRegistrationProof: "",
    addressProof: "",
    aadharFront: "",
  });
  const clientvalue = {
    ...clientDetails,
  };
  const businessType = getLocalStorage("BUSINESSTYPE");

  const handleChange = (e) => {
    e.persist();
    const { value, name } = e.target;
    !isFormUpdated && setFormUpdated(true);
    setFormData((values) => ({ ...values, [name]: value }));
    setErrors({ ...errors, [name]: "" });
  };

  const onChange = (value) => {
    setFormData((values) => ({ ...values, captcha: value }));
  };

  const handleSubmit = () => {
    const errorMsg = DocumentVerifyValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      setClientDetails((values) => ({
        ...values,
        documentVerification: formData,
      }));
      KycPostDispatch(clientDetails);
      setLocalStorage(KYC_FORM.BANK_ACCOUNT, JSON.stringify(formData));
    }
  };

  const handleClick = (event) => {
    hiddenFileInput.current.click(event);
  };

  const handleNextClick = () => {
    const errorMsg = DocumentVerifyValidation(formData, setErrors);
    if (_.isEmpty(errorMsg)) {
      onClickNext(4);
    }
  };

  const clear = () => {
    setFormData({
      aadharVerification: "",
      captcha: "",
      aadharnotLinkedMyNumber: "",
      businessRegistrationProof: "",
      addressProof: "",
      aadharFront: "",
    });
  };

  useEffect(() => {
    if (getKYCAdd && getKYCAdd.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(getKYCAdd && getKYCAdd.message, "success");
      clearAddKYC();
      getKYClistDispatch();
      removeLocalStorage("CONTACT_DETAILS");
      removeLocalStorage("BUSINESS");
      removeLocalStorage("BUSINESS_DETAILS");
      removeLocalStorage("BANK_ACCOUNT");
      removeLocalStorage("VALIDNAME");
      removeLocalStorage("BUSINESSTYPE");
      removeLocalStorage("USER_MENU_DETAILS");
    } else if (getKYCAdd && getKYCAdd.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        "error",
        getKYCAdd && getKYCAdd.message,
        "",
        "Try again",
        "",
        () => {
          clear();
        },
        clearAddKYC()
      );
    }
  }, [getKYCAdd]);

  return (
    <>
      {businessType === "privatedLimited" ||
      businessType === "publicLimited" ||
      businessType === "LLPIN" ? (
        <div>
          <h3 className="mb-4 d-flex justify-content-center">
            Document Verification
          </h3>
          <div className="current" data-kt-stepper-element="content">
            <div className="w-100">
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">
                    Author Signatory's Aadhar Proof
                  </span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Aadhar Verification"
                  />
                </label>
                <select
                  className="form-select"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChange(e)}
                  value={formData.addressProof || ""}
                  name="addressProof"
                >
                  <option value="">Select</option>
                  <option value="Aadhar">Aadhar</option>
                  <option value="PanCard">PanCard</option>
                </select>
                {errors && errors.addressProof && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red;}"}</style>
                    {errors.addressProof}
                  </div>
                )}
              </div>
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Aadhar Front</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Aadhar Front"
                  />
                </label>
                <Dropzone
                  defaultImages={defaultImages}
                  type={DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS}
                  multiple
                  thumbnail={false}
                  maxFileSize="5000"
                  name={`${DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES.toLowerCase()}`}
                  flag={DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES}
                  formatType={FILE_FORMAT_TYPE}
                  showRemove
                  dropzoneError={dropzoneError}
                />
              </div>
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Aadhar Back</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Aadhar Back"
                  />
                </label>
                <Dropzone
                  defaultImages={defaultImages}
                  type={DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS}
                  multiple
                  thumbnail={false}
                  maxFileSize="5000"
                  name={`${DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES.toLowerCase()}`}
                  flag={DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES}
                  formatType={FILE_FORMAT_TYPE}
                  showRemove
                  dropzoneError={dropzoneError}
                />
              </div>
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Business Registration Proof</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Business Registration Proof"
                  />
                </label>
                <Dropzone
                  defaultImages={defaultImages}
                  type={DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS}
                  multiple
                  thumbnail={false}
                  maxFileSize="5000"
                  name={`${DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES.toLowerCase()}`}
                  flag={DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES}
                  formatType={FILE_FORMAT_TYPE}
                  showRemove
                  dropzoneError={dropzoneError}
                />
              </div>

              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Company PAN</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Company Pan"
                  />
                </label>
                <Dropzone
                  defaultImages={defaultImages}
                  type={DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS}
                  multiple
                  thumbnail={false}
                  maxFileSize="5000"
                  name={`${DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES.toLowerCase()}`}
                  flag={DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES}
                  formatType={FILE_FORMAT_TYPE}
                  showRemove
                  dropzoneError={dropzoneError}
                />
              </div>
            </div>
          </div>
        </div>
      ) : businessType === "proprietorShip" ? (
        <>
          <div className="current" data-kt-stepper-element="content">
            <div className="w-100">
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">
                    Aadhar Verification (via OTP)
                  </span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Aadhar Verification"
                  />
                </label>
                <input
                  type="text"
                  className="form-control form-control-lg form-control-solid"
                  name="aadharVerification"
                  placeholder="Aadhar Verification"
                  onChange={(e) => handleChange(e)}
                  value={formData.aadharVerification || ""}
                  maxLength={12}
                  onKeyPress={(e) => {
                    if (!/[0-9{1-3}.]/.test(e.key)) {
                      e.preventDefault();
                    }
                  }}
                />
                {errors && errors.aadharVerification && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red;}"}</style>
                    {errors.aadharVerification}
                  </div>
                )}
              </div>
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Captcha</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Enter the captcha shown above"
                  />
                </label>
                <Captcha
                  onChange={(e) => onChange(e)}
                  placeholder="Enter the captcha shown above"
                  name="captcha"
                  value={formData.captcha || ""}
                />
                {errors && errors.captcha && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red;}"}</style>
                    {errors.captcha}
                  </div>
                )}
              </div>
              <div className="fv-row mb-10">
                <div className="me-2">
                  <button
                    type="button"
                    className="btn btn-lg btn-light-primary me-3"
                  >
                    Submit & Get OTP
                  </button>
                </div>
              </div>
              <div className="form-check form-check-custom form-check-solid form-check-sm mb-4">
                <input
                  className="form-check-input"
                  type="checkbox"
                  value="aadharnotLinkedMyNumber"
                  id="flexRadioLg"
                  name="aadharnotLinkedMyNumber"
                  onChange={(e) => handleChange(e)}
                />
                <label className="form-check-label text-muted">
                  My Aadhar is not linked to my number
                </label>
              </div>

              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Business Registration Proof</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Business Registration Proof"
                  />
                </label>
                <select
                  className="form-select"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChange(e)}
                  value={formData.businessRegistrationProof || ""}
                  name="businessRegistrationProof"
                >
                  <option value="">Select</option>
                  <option value="MSME/Udyam/UdyogCertificate">
                    MSME/Udyam/UdyogCertificate
                  </option>
                  <option value="MSME/Udyam/UdyogCertificate">
                    MSME/Udyam/UdyogCertificate
                  </option>
                </select>
                {errors && errors.businessRegistrationProof && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red;}"}</style>
                    {errors.businessRegistrationProof}
                  </div>
                )}
              </div>
              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">MSME/Udyam/UdyogCertificate</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="MSME/Udyam/UdyogCertificate"
                  />
                </label>
                <Dropzone
                  defaultImages={defaultImages}
                  type={DROPZONE_IMAGE_NAME_TYPES.DOCUMENTS}
                  multiple
                  thumbnail={false}
                  maxFileSize="5000"
                  name={`${DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES.toLowerCase()}`}
                  flag={DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES}
                  formatType={FILE_FORMAT_TYPE}
                  showRemove
                  dropzoneError={dropzoneError}
                />
              </div>

              <div className="fv-row mb-10">
                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                  <span className="required">Personal Pan</span>
                  <i
                    className="fas fa-exclamation-circle ms-2 fs-7"
                    data-bs-toggle="tooltip"
                    title="Personal Pan"
                  />
                </label>
                <div className="mb-2">
                  <Dropzone
                    defaultImages={defaultImages}
                    type={DROPZONE_IMAGE_NAME_TYPES.IMAGE}
                    multiple
                    thumbnail={false}
                    maxFileSize="5000"
                    name={`${DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES.toLowerCase()}`}
                    flag={DROPZONE_IMAGE_FLAG.UPLOAD_DAILIES}
                    formatType={FILE_FORMAT_TYPE}
                    showRemove
                    dropzoneError={dropzoneError}
                  />
                </div>
                Upload scanned copy of personal PAN Card
              </div>
            </div>
          </div>
        </>
      ) : null}
      <div className="fv-row">
        <div className="d-flex flex-stack pt-10">
          <div className="me-2">
            <button
              onClick={() => {
                goBack(3);
              }}
              type="button"
              className="btn btn-sm btn-light-primary me-3"
            >
              <KTSVG
                path="/media/icons/duotune/arrows/arr063.svg"
                className="svg-icon-4 me-1"
              />
              Back
            </button>
          </div>
          <div className="mb-4">
            {showForm ? (
              <button
                type="submit"
                className="btn btn-sm btn-primary me-3"
                onClick={(event) => {
                  handleSubmit(event);
                }}
              >
                <span className="indicator-label">
                  <KTSVG
                    path="/media/icons/duotune/arrows/arr064.svg"
                    className="svg-icon-3 ms-2 me-0"
                  />
                </span>
                {loading ? (
                  <span
                    className="spinner-border spinner-border-sm mx-3"
                    role="status"
                    aria-hidden="true"
                  />
                ) : (
                  "Submit"
                )}
              </button>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { StatelistStore, KYCAddStore } = state;
  return {
    getStates:
      StatelistStore && StatelistStore.Statelists
        ? StatelistStore.Statelists
        : {},
    getKYCAdd: KYCAddStore && KYCAddStore.KYCAdd ? KYCAddStore.KYCAdd : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
  KycPostDispatch: (data) => dispatch(KYCAddAction.KYCAdd(data)),
  clearAddKYC: (data) => dispatch(KYCAddAction.clearKYC(data)),
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentVerification);
