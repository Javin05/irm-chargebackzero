
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { contactInfoValidation, phoneOtp } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { KYC_FORM, REGEX, STATUS_RESPONSE, USER_ERROR } from '../../../utils/constants'
import _, { isEmpty, values } from 'lodash'
import { KYCemailAction, KYCemailOtpAction, KYCAddALLAction, KYCActions, FullKycValueAction } from '../../../store/actions'
import Toastify from 'toastify-js'
import "toastify-js/src/toastify.css"
import { warningAlert } from "../../../utils/alerts"
import { setEmailData } from './formData'
import CryptoJS from "crypto-js"

function EmailInfo(props) {
  const {
    onClickNext,
    setClientDetails,
    emialVerifyloading,
    emialVerifyResponse,
    emialOtpVerifyloading,
    emialVerifyOtpResponse,
    EmailVerifyOtpDispatch,
    EmailVerifyDispatch,
    clearKYCemailOtpVerify,
    clearKYCemailVerify,
    kycAllDataSaved,
    UserDetails,
    clearFullKycValueDispatch,
    FullKycResData,
    FullKycLoading,
    fullKycDispatch,
    GetClientsRes
  } = props

  const [errors, setErrors] = useState({})
  const [error, setError] = useState({})
  const [showOtp, setShowOtp] = useState(false)
  const [formData, setFormData] = useState({
    contactEmail: '',
    alternateEmail:'',
    responseType : "UPDATED"
  })
  const [otpData, setOtpData] = useState({
    otp: '',
  })
  const [emailedit, setEmailedit] = useState(false)
  const [updateedit, setUpdateedit] = useState(false)

  const otpChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setOtpData((values) => ({ ...values, [name]: value }))
    setError({ ...error, [name]: '' })
  }

  const otpSubmit = () => {
    const error = {}
    if (_.isEmpty(otpData.otp)) {
      error.otp = "Email Otp is required"
    }
    setError(error)
    if (_.isEmpty(error)) {
      var digits = "0123456789";
      let OTP = "";
      let otp_length = 4
      for (let i = 0; i < otp_length; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
      }
      const cryptoForm = `${otpData.otp}-${formData.contactEmail}-${OTP}`
      const cryptoToken = CryptoJS.AES.encrypt(cryptoForm, 'I4M2OBW').toString()
      const data = {
        otp: CryptoJS.AES.encrypt(otpData.otp, 'I4M2OBW').toString(),
        emailId: formData.contactEmail,
        clientId: UserDetails && UserDetails.data && UserDetails.data.clientId && UserDetails.data.clientId._id,
        token: cryptoToken
      }
      EmailVerifyOtpDispatch(data)
    }
  }
  
  const emailVerify = () => {
    const errors = {}
    if (_.isEmpty(formData.contactEmail)) {
      errors.contactEmail = 'Email Is Required'
    } else if (formData.contactEmail && !REGEX.EMAIL.test(formData.contactEmail)) {
      errors.contactEmail = USER_ERROR.EMAIL_INVALID
    }
    setError(errors)
    if (_.isEmpty(errors)) {
      var digits = "0123456789";
      let OTP = "";
      let otp_length = 4
      for (let i = 0; i < otp_length; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
      }
      const cryptoForm = `${formData.contactEmail}-${OTP}`
      const cryptoToken = CryptoJS.AES.encrypt(cryptoForm, 'I4M2OBW').toString()
      const data = {
        emailId: formData.contactEmail,
        alternateEmailId: formData.alternateEmail,
        clientId: UserDetails && UserDetails.data && UserDetails.data.clientId && UserDetails.data.clientId._id, 
        token: cryptoToken
      }
      EmailVerifyDispatch(data)
      setClientDetails((values) => ({ ...values, Email: formData }))
    }
    setErrors(errors)
  }

  const ProccedData = () => {
    const errors = {}
    if (_.isEmpty(formData.contactEmail)) {
      errors.contactEmail = 'Email Is Required'
    } else if (formData.contactEmail && !REGEX.EMAIL.test(formData.contactEmail)) {
      errors.contactEmail = USER_ERROR.EMAIL_INVALID
    }
    setError(errors)
    if (_.isEmpty(errors)) {
      onClickNext(2)
      setClientDetails((values) => ({ ...values, Email: formData }))
    }
    setErrors(errors)
  }

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  useEffect(() => {
    if (emialVerifyResponse && emialVerifyResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      Toastify({
        text: "Otp Send Successfully In Your Email",
        duration: 4000,
        newWindow: true,
        close: true,
        gravity: "top",
        position: "right",
        stopOnFocus: true,
        offset: {
          x: 50,
          y: 10
        },
        className: "info"
      }).showToast()
      setShowOtp(true)
      clearKYCemailVerify()
    } else if (emialVerifyResponse && emialVerifyResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        emialVerifyResponse && emialVerifyResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCemailVerify()
    }
  }, [emialVerifyResponse])

  useEffect(() => {
    if (emialVerifyOtpResponse && emialVerifyOtpResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      if(!updateedit) {
        fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id, formData )
        clearKYCemailOtpVerify()
      } else {
        clearKYCemailOtpVerify()
        onClickNext(2)
      }
    } else if (emialVerifyOtpResponse && emialVerifyOtpResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        emialVerifyOtpResponse && emialVerifyOtpResponse.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearKYCemailOtpVerify()
    }
  }, [emialVerifyOtpResponse])

  useEffect(() => {
    if (UserDetails) {
      const data = setEmailData(UserDetails && UserDetails.data)
      setFormData(data)
    }
    if(UserDetails && UserDetails.data && UserDetails.data.emailId && UserDetails.data.emailId.emailId && !_.isEmpty(UserDetails.data.emailId.emailId)) {
      setEmailedit(true)
    } else {
      setEmailedit(false)
    }
  }, [UserDetails])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(2)
      clearFullKycValueDispatch()
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  // useEffect(() => {
  //   if(emailedit) {
  //     setUpdateedit(true)
  //   } else {
  //     setUpdateedit(false)
  //   }
  // },[emailedit])

  const onNextSubmit = () => {
    const params = {
      emailId: formData.contactEmail,
    }
    fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id, params)
  }

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            <>
              <div className='fv-row mb-10'>
                {
                  !showOtp ? (
                    <>
                      <div className='row mb-4'>
                        <div className='col-lg-12'>
                          <label className='d-flex align-items-center mb-2'>
                            <span className='text-dark fs-6 fw-bold'>Add your email address to receive account update</span>
                            <i
                              className='fas fa-exclamation-circle ms-2 fs-7'
                              data-bs-toggle='tooltip'
                              title='Email'
                            ></i>
                          </label>
                        </div>
                      </div>
                      <input
                        type='text'
                        className='form-control form-control-lg form-control-solid mb-4'
                        name='contactEmail'
                        placeholder='Contact Email'
                        onChange={(e) => handleChange(e)}
                        value={formData.contactEmail || ''}
                        disabled={emailedit}
                      />
                      {errors && errors.contactEmail && (
                        <div className='rr mt-1'>
                          <style>{'.rr{color:red;}'}</style>
                          {errors.contactEmail}
                        </div>
                      )}
                    </>
                  ) : (
                    <>
                      <div className='row'>
                        <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                          <span className='required'>
                            {
                              `Enter the OTP sent to `
                            }
                            <text className='text-danger'>
                              {formData.contactEmail}
                            </text>
                          </span>
                          <i
                            className='fas fa-exclamation-circle ms-2 fs-7'
                            data-bs-toggle='tooltip'
                            title='Otp'
                          ></i>
                        </label>
                        <div className='col-lg-6'>
                          <input
                            type='text'
                            className='form-control form-control-lg form-control-solid'
                            name='otp'
                            placeholder='OTP'
                            onChange={(e) => otpChange(e)}
                            value={otpData.otp || ''}
                            maxLength={4}
                            onKeyPress={(e) => {
                              if (!/^[0-9 .]+$/.test(e.key)) {
                                e.preventDefault()
                              }
                            }}
                          />
                          {error && error.otp && (
                            <div className='rr mt-1'>
                              <style>{'.rr{color:red;}'}</style>
                              {error.otp}
                            </div>
                          )}
                        </div>
                        <div className='col-lg-6'>
                          <button type='submit' className='btn btn-sm btn-light-primary'
                            onClick={() => { emailVerify() }}
                            disabled={emialVerifyloading}
                          >
                            {!emialVerifyloading &&
                              <span className='indicator-label'>
                                <i className='bi bi-person-fill' />
                                Resend OTP
                              </span>
                            }
                            {emialVerifyloading && (
                              <span className='indicator-progress' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                      </div>
                    </>
                  )
                }
              </div>
              <div className='fv-row mb-4'>
                <div className='d-flex align-items-center pt-10 justify-content-end'>
                  <div>
                   {
                      !showOtp ?
                        ( 
                            GetClientsRes && GetClientsRes.data && GetClientsRes.data.kycEmailVerification === "NO"
                              ? (
                                <button
                                  type="submit"
                                  className="btn btn-sm btn-light-primary ms-2"
                                  onClick={() => {
                                    onNextSubmit()
                                  }}
                                  disabled={FullKycLoading}
                                >
                                  {!FullKycLoading &&
                                    <span className='indicator-label'>
                                      <i className='bi bi-person-fill' />
                                      Next
                                    </span>
                                  }
                                  {FullKycLoading && (
                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                      Please wait...
                                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                    </span>
                                  )}
                                </button>
                              ) : (
                                <button type='submit' className='btn btn-sm btn-light-primary'
                                  onClick={() => { emailVerify() }}
                                  disabled={emialVerifyloading}
                                >
                                  {!emialVerifyloading &&
                                    <span className='indicator-label'>
                                      <i className='bi bi-person-fill' />
                                      Send OTP
                                    </span>
                                  }
                                  {emialVerifyloading && (
                                    <span className='indicator-progress' style={{ display: 'block' }}>
                                      Please wait...
                                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                    </span>
                                  )}
                                </button>
                              )
                          
                        ) : (
                          <button type='submit' className='btn btn-sm btn-primary'
                            onClick={(event) => {
                              otpSubmit(event)
                            }}
                          >
                            {!emialOtpVerifyloading &&
                              <span className='indicator-label'>
                                <i className='bi bi-person-check-fill' />
                                verify
                              </span>
                            }
                            {emialOtpVerifyloading && (
                              <span className='indicator-progress' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        )
                    }
                  </div>
                </div>
              </div>
            </>
          </div>
        </div>
      </div >
    </>
  )
}

const mapStateToProps = (state) => ({
  emialVerifyloading: state && state.EmailVerifyStore && state.EmailVerifyStore.loading,
  emialVerifyResponse: state && state.EmailVerifyStore && state.EmailVerifyStore.emialVerify,
  emialOtpVerifyloading: state && state.EmailVerifyOtpStore && state.EmailVerifyOtpStore.loading,
  emialVerifyOtpResponse: state && state.EmailVerifyOtpStore && state.EmailVerifyOtpStore.emialOtpVerify,
  MiniKycAllDataRes: state && state.KYCAddStore && state.KYCAddStore.KYCAddResponse,
  MiniKycAllDataResLoading: state && state.KYCAddStore && state.KYCAddStore.loading,
  FullKycResData: state && state.FullKycValueStore && state.FullKycValueStore.FullKycValue
})

const mapDispatchToProps = (dispatch) => ({
  EmailVerifyDispatch: (data) => dispatch(KYCemailAction.KYCemailVerify(data)),
  EmailVerifyOtpDispatch: (data) => dispatch(KYCemailOtpAction.KYCemailOtpVerify(data)),
  clearKYCemailOtpVerify: (data) => dispatch(KYCemailOtpAction.clearKYCemailOtpVerify(data)),
  clearKYCemailVerify: (data) => dispatch(KYCemailAction.clearKYCemailVerify(data)),
  AddAllKycDataDispatch: (id, data) => dispatch(KYCAddALLAction.KYCAddAllData(id, data)),
  getKYClistDispatch: (params) => dispatch(KYCActions.getKYClist(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(EmailInfo)