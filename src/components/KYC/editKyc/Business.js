
import React, { useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businessValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { DATE, REGEX, STATUS_RESPONSE } from '../../../utils/constants'
import _ from 'lodash'
import { EntityAction, CategoryValueAction, SubCategoryValueAction, PinCodeValueAction, FullKycValueAction } from "../../../store/actions"
import { setBusinessData } from './formData'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import { DateSelector } from '../../../theme/layout/components/DateSelector'
import { warningAlert } from "../../../utils/alerts"
import color from '../../../utils/colors'

function BusinessForm(props) {
  const {
    onClickNext,
    setSummary,
    setClientDetails,
    loading,
    goBack,
    setCompletedSteps,
    setActiveStep,
    getEntityDispatch,
    getEntityRes,
    UserDetails,
    setEntityValue,
    setEntityName,
    getCategoryDispatch,
    getCategory,
    getSubcategoryDispatch,
    getPincodeDispatch,
    getSubcategory,
    FullKycResData,
    fullKycDispatch,
    kycAllDataSaved,
    FullKycLoading,
    clearFullKycValueDispatch,
    pincodeDataRes,
    setcheckCategory,
    getKYCUserDetailsDispatch,
    userId
  } = props

  const [errors, setErrors] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [showForm, setShowForm] = useState(true)
  const [showWebsite, setShowWebsite] = useState(false)
  const [websiteCheked, setWebsiteCheked] = useState()
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [Data, setData] = useState(true)
  const [tabDefault, setTabdefault] = useState('');
  const [category, setCategory] = useState('');
  const [selectedCategory, setSelectedCategory] = useState('')
  const [subcategory, setSubcategory] = useState('');
  const [selectedSubcategory, setSelectedSubcategory] = useState('')
  const [pincode, setPincode] = useState('')
  const [update, setUpdate] = useState(false)
  const [formData, setFormData] = useState({
    businessCategory: '',
    businessSubCategory: '',
    website: '',
    entityId: '',
    brandName: '',
    businessName: '',
    primaryContactName: '',
    entityName: '',
    businessDescription: '',
    businessStartDate: '',
    annualTurnOverRange: '',
    actualAnnualTurnOver: '',
    actualAnnualTurnOverRange: '',
    exceptedMonthlyCardturnOver: '',
    exceptedMonthlyCardturnOverRange: '',
    exceptedMonthlyTransaction: '',
    exceptedMonthlyTransactionRange: '',
    subcategory_id: '',
    category_id: '',
    otherbusinessCategory: ''
  })

  const customStyles = {
    menuList: (provided) => ({
      ...provided,
      position: 'absolute',
      backgroundColor: '#FFFFFF',
      width: '100%',
      border: '1px solid hsl(0, 0%, 80%)',
      borderTop: 0
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : '',
    })
  }

  useEffect(() => {
    getEntityDispatch()
    getCategoryDispatch()
  }, [])

  const handleChange = (e) => {
    setUpdate(true)
    e.persist()
    const { value, name, checked } = e.target
    setWebsiteCheked(checked)
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }

  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.businessName)) {
      errors.businessName = 'Please Enter Business Name.'
    } if (_.isEmpty(formData.primaryContactName)) {
      errors.primaryContactName = 'Please Enter Primary Contact Name.'
    }
    if (_.isEmpty(formData.entityId)) {
      errors.entityId = 'Please Select entity Or Not Registered.'
    }
    if (_.isEmpty(formData.category_id)) {
      errors.category_id = 'Please Select Business Category.'
    }
    // if (_.isEmpty(formData.subcategory_id)) {
    //   errors.subcategory_id = 'Please Select Business Subcategory.'
    // }
    if (_.isEmpty(formData.annualTurnOverRange)) {
      errors.annualTurnOverRange = 'Please Select Annual Turnover Range.'
    }
    if (_.isEmpty(formData.website)) {
      errors.website = 'Website is required.'
    } else if (formData.website && !REGEX.WEBSITE_URLS.test(formData.website)) {
      errors.website = 'Website is InValid'
    }
    if (_.isEmpty(errors)) {
      const params = {
        ...formData,
        businessCategory: formData.businessCategory === "Other" ? formData.otherbusinessCategory : formData.businessCategory,
        responseType: "UPDATED"
      }
      fullKycDispatch(UserDetails && UserDetails.data && UserDetails.data._id, params)
      setData(formData)
      setClientDetails((values) => ({ ...values, business: formData }))
    }
    setErrors(errors)
  }

  const ToggleTab = (tab, name) => {
    setFormData((values) => ({ ...values, entityName: name }))
    setFormData((values) => ({ ...values, entityId: tab }))
    setTabdefault(tab)
    setEntityValue(tab)
    setEntityName(name)
  }

  const categoryNames = getCategory && getCategory.data
  useEffect(() => {
    const Asignees = getDefaultOption(categoryNames)
    setCategory(Asignees)
  }, [categoryNames])

  const getDefaultOption = (categoryNames) => {
    const defaultOptions = []
    for (const item in categoryNames) {
      defaultOptions.push({ label: categoryNames[item].category_value, value: categoryNames[item]._id })
    }
    return defaultOptions
  }

  const handleChangeCategory = selectedOption => {
    if (selectedOption !== null) {
      setUpdate(true)
      setSelectedCategory(selectedOption)
      setFormData(values => ({
        ...values,
        category_id: selectedOption.value,
        businessCategory: selectedOption.label,
      }))
      setcheckCategory(selectedOption.value)
      setErrors({ ...errors, category_id: "" })
      const params = {
        category_id: selectedOption.value
      }
      getSubcategoryDispatch(params)
    }
  }

  const subcategoryNames = getSubcategory && getSubcategory.data
  useEffect(() => {
    const subcategoryAsignees = getSubcategoryDefaultOption(subcategoryNames)
    setSubcategory(subcategoryAsignees)
  }, [subcategoryNames])

  const getSubcategoryDefaultOption = (subcategoryNames) => {
    const defaultSubcategoryOptions = []
    for (const item in subcategoryNames) {
      defaultSubcategoryOptions.push({ label: subcategoryNames[item].category_value, value: subcategoryNames[item]._id })
    }
    return defaultSubcategoryOptions
  }

  const handleChangeSubcategory = selectedOption => {
    if (selectedOption !== null) {
      setUpdate(true)
      setSelectedSubcategory(selectedOption)
      setFormData(values => ({
        ...values,
        subcategory_id: selectedOption.value,
        businessSubCategory: selectedOption.label
      }))
      setErrors({ ...errors, subcategory_id: "" })
    }
  }

  useEffect(() => {
    if (UserDetails) {
      const data = setBusinessData(UserDetails && UserDetails.data)
      const categoryList = getDefaultOption(categoryNames)
      const categoryId = UserDetails && UserDetails.data && UserDetails.data.category_id
      const params = {
        category_id: categoryId
      }
      getSubcategoryDispatch(params)
      if (categoryList && !_.isEmpty(categoryId)) {
        const selOption = _.find(categoryList, function (x) {
          if (_.includes(categoryId, x.value)) {
            return x
          }
        })
        setSelectedCategory(selOption)
        setFormData(values => ({ ...values, category_id: categoryId, }))
      }
      setFormData(data)
      setTabdefault(UserDetails && UserDetails.data && UserDetails.data.entityId)
    }
  }, [UserDetails, editMode, categoryNames])

  useEffect(() => {
    if (!_.isEmpty(subcategoryNames)) {
      const subCategoryList = getSubcategoryDefaultOption(subcategoryNames)
      const subCategoryId = UserDetails && UserDetails.data && UserDetails.data.subcategory_id
      if (subCategoryList && !_.isEmpty(subCategoryId)) {
        const selCatOption = _.find(subCategoryList, function (x) {
          if (_.includes(subCategoryId, x.value)) {
            return x
          }
        })
        setSelectedSubcategory(selCatOption)
        setFormData(values => ({ ...values, subcategory_id: subCategoryId, }))
      }
    }
  }, [UserDetails, subcategoryNames])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      onClickNext(3)
      clearFullKycValueDispatch()
      setPincode('')
      // setUpdate(false)
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG && update) {
      getKYCUserDetailsDispatch(userId)
    }
  }, [update, FullKycResData])

  return (
    <>
      <div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder required'>Business name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='businessName'
            placeholder='Business name'
            onChange={(e) => handleChange(e)}
            value={formData.businessName || ''}
            maxLength={50}
            onKeyPress={(e) => {
              const regex = REGEX.TEXT
              if (!regex.test(e.key) || formData.businessName >= 20) {
                e.preventDefault();
              }
            }}
          />
          {errors && errors.businessName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.businessName}
            </div>
          )}
        </div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder required'>Primary Contact Name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='primaryContactName'
            placeholder='Primary Contact Name'
            onChange={(e) => handleChange(e)}
            value={formData.primaryContactName || ''}
            maxLength={50}
            onKeyPress={(e) => {
              const regex = REGEX.TEXT
              if (!regex.test(e.key) || formData.primaryContactName >= 20) {
                e.preventDefault();
              }
            }}
          />
          {errors && errors.primaryContactName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.primaryContactName}
            </div>
          )}
        </div>
        <div>
          <div className='row mb-4'>
            <div className='col-lg-12'>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Trade/Brand Name</span>
              </label>
            </div>
          </div>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid mb-4'
            name='brandName'
            placeholder='Trade/Brand Name'
            onChange={(e) => handleChange(e)}
            value={formData.brandName || ''}
            maxLength={50}
          />
          {errors && errors.brandName && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.brandName}
            </div>
          )}
        </div>
        <div className='row mt-6'>
          <h3 className='mb-8 d-flex justify-content-center'>Pick only one that suits to your business </h3>
          <div className='col-sm-12 col-md-12 col-lg-12 mb-4' >
            {
              getEntityRes && getEntityRes.data && getEntityRes.data.map((item, i) => {
                return (
                  <a
                    onClick={() =>
                      ToggleTab(item && item._id, item && item.kyc_entity)
                    }
                    className={tabDefault === `${item && item._id}` ? "active btn btn-primary me-2 btn-sm mt-2" : "btn btn-outline btn-outline-primary me-2 btn-sm mt-2"}>
                    {
                      item && item.kyc_entity
                    }
                  </a>
                )
              })
            }
          </div>
          {errors && errors.entityId && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.entityId}
            </div>
          )}
        </div>
        <div className='fv-row mt-6'>
          <div className='col-lg-8'>
            <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
              <span className='required'>Business Category</span>
              <i
                className='fas fa-exclamation-circle ms-2 fs-7'
                data-bs-toggle='tooltip'
                title='City'
              ></i>
            </label>
            <ReactSelect
              styles={customStyles}
              isMulti={false}
              name='category_id'
              className='basic-single'
              classNamePrefix='select'
              handleChangeReactSelect={handleChangeCategory}
              options={category}
              value={selectedCategory}
            />
            {errors.category_id && (
              <div className='fv-plugins-message-container text-danger'>
                <span role='alert text-danger'>{errors.category_id}</span>
              </div>
            )}
          </div>
        </div>
        {
          formData && formData.businessCategory === "Other" ? (
            <div className='fv-row mt-6'>
              <div className='col-lg-8'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  name='otherbusinessCategory'
                  placeholder='Business Category'
                  onChange={(e) => handleChange(e)}
                  value={formData.otherbusinessCategory || ''}
                  maxLength={200}
                />
              </div>
            </div>
          ) : (
            <div className='fv-row mt-6'>
              <div className='col-lg-8'>
                <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                  <span className='required'>Business Subcategory</span>
                  <i
                    className='fas fa-exclamation-circle ms-2 fs-7'
                    data-bs-toggle='tooltip'
                    title='City'
                  ></i>
                </label>
                <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='businessSubCategory'
                  className='basic-single'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeSubcategory}
                  options={subcategory}
                  value={selectedSubcategory}
                  isDisabled={!selectedCategory}
                />
                {errors.businessSubCategory && (
                  <div className='fv-plugins-message-container text-danger'>
                    <span role='alert text-danger'>{errors.businessSubCategory}</span>
                  </div>
                )}
              </div>
            </div>
          )
        }
        <div className='fv-row mb-10 mt-8'>
          <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
            <span className='required'> Business Description</span>
            <i
              className='fas fa-exclamation-circle ms-2 fs-7'
              data-bs-toggle='tooltip'
              title='Business Description'
            />
          </label>
          <textarea
            type='text'
            className='form-control form-control-lg form-control-solid'
            name='businessDescription'
            placeholder='Business Description'
            onChange={(e) => handleChange(e)}
            value={formData.businessDescription || ''}
            maxLength={200}
          />
          {errors && errors.businessDescription && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.businessDescription}
            </div>
          )}
        </div>
        <div className='fv-row mb-10 mt-8'>
          <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
            <span>Business Commencement Date</span>
          </label>
          <DateSelector
            name='businessStartDate'
            placeholder='Business Commencement Date'
            className='form-control'
            selected={new Date(formData && !_.isEmpty(formData.businessStartDate)) ? formData.businessStartDate : ''}
            onChange={(date) => {
              setErrors({ ...errors, businessStartDate: '' })
              setFormData((values) => ({
                ...values,
                businessStartDate: date
              }))
            }}
            dateFormat={DATE.DATE_FOR_PICKER}
            isClearable={true}
            peek={true}
            monthDropdown={true}
            yearDropdown={true}
            showYear={true}
            maxDate={new Date()}
          />
          {errors && errors.businessStartDate && (
            <div className="rr mt-1">
              <style>{".rr{color:red}"}</style>
              {errors.businessStartDate}
            </div>
          )}
        </div>
        <div className='fv-row mb-10 mt-8'>
          <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
            <span className='required'>Annual Turnover Range</span>
          </label>
          <select
            name='annualTurnOverRange'
            className='form-select form-select-solid mb-4'
            data-control='select'
            data-placeholder='Select an option'
            data-allow-clear='true'
            onChange={(e) => handleChange(e)}
            value={formData.annualTurnOverRange}
          >
            <option value=''>Select...</option>
            <option value='> 1 Crore'>{'> 1 Crore...'}</option>
            <option value='> 50 Lakhs and < 1'>{'> 50 Lakhs and < 1'}</option>
            <option value='> 20 Lakhs and < 50 Lakhs'>{'> 20 Lakhs and < 50 Lakhs'}</option>
            <option value='< 20 Lakhs'>{'< 20 Lakhs'}</option>
          </select>
          {errors && errors.annualTurnOverRange && (
            <div className="rr mt-1">
              <style>{".rr{color:red}"}</style>
              {errors.annualTurnOverRange}
            </div>
          )}
        </div>
        <div className='row mb-10 mt-8 align-items-center justify-content-between'>
          <div className='col-lg-6'>
            <div>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Actual Annual Turnover</span>
              </label>
            </div>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid mb-4'
              name='actualAnnualTurnOver'
              placeholder='Actual Annual Turnover'
              onChange={(e) => handleChange(e)}
              value={formData.actualAnnualTurnOver || ''}
              maxLength={6}
              onKeyPress={(e) => {
                if (!REGEX.NUMERIC_SIX_DIGITS.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
          </div>
          <div className='col-lg-6 mt-8'>
            <select
              name='actualAnnualTurnOverRange'
              className='form-select form-select-solid mb-4'
              data-control='select'
              data-placeholder='Select an option'
              data-allow-clear='true'
              onChange={(e) => handleChange(e)}
              value={formData.actualAnnualTurnOverRange}
            >
              <option value=''>Select...</option>
              <option value='Crore'>Crore</option>
              <option value='Lakhs'>Lakhs</option>
            </select>
          </div>
          <div className='col-lg-6'>
            <div>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Expected Monthly Card Turnover</span>
              </label>
            </div>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid mb-4'
              name='exceptedMonthlyCardturnOver'
              placeholder='Expected Monthly Card Turnover'
              onChange={(e) => handleChange(e)}
              value={formData.exceptedMonthlyCardturnOver || ''}
              maxLength={6}
              onKeyPress={(e) => {
                if (!REGEX.NUMERIC_SIX_DIGITS.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
          </div>
          <div className='col-lg-6 mt-8'>
            <select
              name='exceptedMonthlyCardturnOverRange'
              className='form-select form-select-solid mb-4'
              data-control='select'
              data-placeholder='Select an option'
              data-allow-clear='true'
              onChange={(e) => handleChange(e)}
              value={formData.exceptedMonthlyCardturnOverRange}
            >
              <option value=''>Select...</option>
              <option value='Crore'>Crore</option>
              <option value='Lakhs'>Lakhs</option>
            </select>
          </div>
          <div className='col-lg-6'>
            <div>
              <label className='d-flex align-items-center mb-2'>
                <span className='text-dark fs-4 fw-bolder'>Expected Monthly Transaction</span>
              </label>
            </div>
            <input
              type='text'
              className='form-control form-control-lg form-control-solid mb-4'
              name='exceptedMonthlyTransaction'
              placeholder='Expected Monthly Transaction'
              onChange={(e) => handleChange(e)}
              value={formData.exceptedMonthlyTransaction || ''}
              maxLength={6}
              onKeyPress={(e) => {
                if (!REGEX.NUMERIC_SIX_DIGITS.test(e.key)) {
                  e.preventDefault()
                }
              }}
            />
          </div>
          <div className='col-lg-6 mt-8'>
            <select
              name='exceptedMonthlyTransactionRange'
              className='form-select form-select-solid mb-4'
              data-control='select'
              data-placeholder='Select an option'
              data-allow-clear='true'
              onChange={(e) => handleChange(e)}
              value={formData.exceptedMonthlyTransactionRange}
            >
              <option value=''>Select...</option>
              <option value='Lakhs'>Lakhs</option>
              <option value='Thousand'>Thousand</option>
            </select>
          </div>
        </div>
        <div className='fv-row mb-10 mt-8'>
          <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
            <span className='required'>Add your website link</span>
            <i
              className='fas fa-exclamation-circle ms-2 fs-7'
              data-bs-toggle='tooltip'
              title='Website'
            />
          </label>
          <input
            type='text'
            className='form-control form-control-lg form-control-solid'
            name='website'
            placeholder='Website'
            onChange={(e) => handleChange(e)}
            value={formData.website || ''}
          />
          {errors && errors.website && (
            <div className='rr mt-1'>
              <style>{'.rr{color:red;}'}</style>
              {errors.website}
            </div>
          )}
        </div>
        <div className='fv-row mt-4 mb-6'>
          <div className='d-flex justify-content-end'>
            <button type='submit' className='btn btn-sm btn-primary me-3'
              onClick={() => {
                handleSubmit()
              }}
              disabled={FullKycLoading}
            >
              {!FullKycLoading &&
                <span className='indicator-label'>
                  <i className='bi bi-person-fill' />
                  Proceed
                </span>
              }
              {FullKycLoading && (
                <span className='indicator-progress' style={{ display: 'block' }}>
                  Please wait...
                  <span className='spinner-border spinner-border-sm align-middle ms-2' />
                </span>
              )}
            </button>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = (state) => ({
  getCategory: state && state.CategoryValueTypeStore && state.CategoryValueTypeStore.CategoryValue ? state.CategoryValueTypeStore.CategoryValue : '',
  getEntityRes: state && state.EntityStore && state.EntityStore.EntityRes,
  getSubcategory: state && state.SubCategoryValueTypeStore && state.SubCategoryValueTypeStore.subCategoryValue ? state.SubCategoryValueTypeStore.subCategoryValue : '',
  FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
  FullKycResData: state && state.FullKycValueStore && state.FullKycValueStore.FullKycValue,
})

const mapDispatchToProps = (dispatch) => ({
  getEntityDispatch: () => dispatch(EntityAction.Entity()),
  getCategoryDispatch: () => dispatch(CategoryValueAction.CategoryValue()),
  getSubcategoryDispatch: (params) => dispatch(SubCategoryValueAction.SubCategoryValue(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(BusinessForm)