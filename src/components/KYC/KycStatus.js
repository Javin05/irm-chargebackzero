import React, { useState, useEffect, Fragment } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import {
  STATUS_RESPONSE,
  USER_ERROR,
  SWEET_ALERT_MSG,
} from "../../utils/constants"
import {
  DashVideoApproveAction,
  KYCUserAction
} from "../../store/actions"
import { useLocation } from "react-router-dom"
import {
  successAlert,
  warningAlert,
  confirmationAlert,
} from "../../utils/alerts"
import clsx from "clsx"
import Modal from 'react-bootstrap/Modal'

function KycStatus(props) {
  const {
    loading,
    DashVideoApproveDispatch,
    DashVideoApproveRes,
    clearDashVideoApproveDispatch,
    getKYCUserDetailsDispatch
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split("static-summary/update/")
  const id = url && url[1]
  const [show, setShow] = useState(false)
  const [rejectShow, setRejectShow] = useState(false)

  const [errors, setErrors] = useState({
    reason: "",
  })
  const [formData, setFormData] = useState({
    message: "",
  })
  const [rejectFormData, setRejectFormData] = useState({
    status: "REJECTED",
    reason: ""
  })
  const [approveFormData, setApproveFormData] = useState({
    status: "APPROVED",
    reason: "",
  })

  const approveSubmit = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      const params = {
        videoKycApprovalStatus: approveFormData.status,
        videoKycComments: approveFormData.reason,
      }
      DashVideoApproveDispatch(id, params)
    }
  }

  const onConfirmReject = () => {
      const params = {
        videoKycApprovalStatus: rejectFormData.status,
        videoKycComments: rejectFormData.reason,
        responseType:"UPDATED"
      }
    DashVideoApproveDispatch(id, params)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        "warning",
        "Yes",
        "No",
        () => {
          onConfirmReject()
        },
        () => { }
      )
    }
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: "" })
  }

  useEffect(() => {
    if (DashVideoApproveRes && DashVideoApproveRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setShow(false)
      setRejectShow(false)
      successAlert(
        DashVideoApproveRes && DashVideoApproveRes.message,
        'success'
      )
      getKYCUserDetailsDispatch(id)
      clearDashVideoApproveDispatch()
    } else if (DashVideoApproveRes && DashVideoApproveRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DashVideoApproveRes && DashVideoApproveRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearDashVideoApproveDispatch()
    }
  }, [DashVideoApproveRes])

  const clearPopup = () => {
    setShow(false)
    setRejectShow(false)
  }

  return (
    <>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Are You Sure Want to Approve ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Approved
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 ">
                      <textarea
                        name="reason"
                        type="text"
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid":
                              approveFormData.reason && errors.reason,
                          },
                          {
                            "is-valid":
                              approveFormData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => approveChange(e)}
                        autoComplete="off"
                        value={approveFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => approveSubmit()}
                          disabled={loading}
                        >
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <Modal
        show={rejectShow}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(36 36 92)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Are You Sure Want to Approve ?
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className='container-fixed'
            style={{
              backgroundColor: '#rgb(179 179 179)',
              borderRadius: '10px'
            }}
          >
            <div className="card-header">
              <div className="card-body">
                <div className="form-group row mb-4">
                  <div className="col-lg-12 col-md-12 col-sm-12 mb-3">
                    <label className="font-size-xs font-weight-bold mb-3 fs-4 form-label">
                      Reason For Reject :
                    </label>
                    <div className="col-lg-11 col-md-11 col-sm-11 mb-2">
                      <textarea
                        name="reason"
                        type="text"
                        className={clsx(
                          "form-control form-control-lg form-control-solid",
                          {
                            "is-invalid": formData.reason && errors.reason,
                          },
                          {
                            "is-valid": formData.reason && !errors.reason,
                          }
                        )}
                        placeholder="Message"
                        onChange={(e) => rejectChange(e)}
                        autoComplete="off"
                        value={rejectFormData.reason || ""}
                      />
                      {errors.reason && (
                        <div className="fv-plugins-message-container text-danger">
                          <span role="alert text-danger">
                            {errors.reason}
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="form-group row mb-4">
                    <div className="col-lg-6" />
                    <div className="col-lg-6">
                      <div className="col-lg-11">
                        <button
                          type="button"
                          className="btn btn-sm btn-light-primary m-2 fa-pull-right close"
                          onClick={() => rejectSubmit()}
                        >
                          <span className="indicator-label">
                            Submit
                          </span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      <div className="row">
        <div className="col-lg-11">
          <div className="card-toolbar d-flex mt-5">
            <>
              <ul className="nav">
                <li className="nav-item">
                  <a
                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success ms-2"
                    onClick={() => {
                      setShow(true)
                    }}
                  >
                    Approve
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                    onClick={() => {
                      setRejectShow(true)
                    }}
                  >
                    Reject
                  </a>
                </li>
              </ul>
            </>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const {
    DashVideoApproveStore
  } = state
  return {
    DashVideoApproveRes: DashVideoApproveStore && DashVideoApproveStore.DashVideoApproveRes ? DashVideoApproveStore.DashVideoApproveRes : {},
    loading: DashVideoApproveStore && DashVideoApproveStore.loading ? DashVideoApproveStore.loading : false,
  }
}

const mapDispatchToProps = (dispatch) => ({
  DashVideoApproveDispatch: (id, params) => dispatch(DashVideoApproveAction.DashVideoApproveDoc(id, params)),
  clearDashVideoApproveDispatch: (id) => dispatch(DashVideoApproveAction.clearVideoApproveDoc(id)),
  getKYCUserDetailsDispatch: (id) => dispatch(KYCUserAction.KYCUser_INIT(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(KycStatus)