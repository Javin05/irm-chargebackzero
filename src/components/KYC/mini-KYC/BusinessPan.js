
import React, { FC, useEffect, useRef, useState } from 'react'
import { KTSVG, toAbsoluteUrl } from '../../../theme/helpers'
import { connect } from 'react-redux'
import { businesDetailValidation } from './Validation'
import { setLocalStorage, getLocalStorage } from '../../../utils/helper'
import { KYC_FORM, REGEX, STATUS_RESPONSE } from '../../../utils/constants'
import _, { upperCase } from 'lodash'
import ReactSelect from '../../../theme/layout/components/ReactSelect'
import {
  CityActions,
  KYCpanAction,
  KYCcinAction,
  KYCgstinAction,
  KYCPersonalpanAction
} from '../../../store/actions'
import color from '../../../utils/colors'
import { warningAlert, confirmationAlert } from "../../../utils/alerts"

function BusinessPanForm(props) {
  const {
    onClickNext,
    setClientDetails,
    loading,
    goBack,
    clientDetails,
    KYCbusinessPanLoading,
    KycPanVerifyDispatch,
    KybusinessPanRes,
    clearKYCpanVerify
  } = props

  const [errors, setErrors] = useState({})
  const [isFormUpdated, setFormUpdated] = useState(false)
  const [businessPanVerify, setBusinessPanVerify] = useState(false)
  const [formData, setFormData] = useState({
    businessPanName: '',
    businessPan: ''
  })

  const validaName = clientDetails && clientDetails.contactInfo && clientDetails.contactInfo.contactName
  const businessType = clientDetails && clientDetails.business && clientDetails.business.businessType
  setLocalStorage('VALIDNAME', validaName)
  setLocalStorage('BUSINESSTYPE', businessType)

  const handleChange = (e) => {
    e.persist()
    const { value, name } = e.target
    !isFormUpdated && setFormUpdated(true)
    setFormData((values) => ({ ...values, [name]: value }))
    setErrors({ ...errors, [name]: '' })
  }


  const handleSubmit = () => {
    const errors = {}
    if (_.isEmpty(formData.businessPanName)) {
      errors.businessPanName = 'BusinessPanName Is Required'
    }
    if (_.isEmpty(errors)) {
      onClickNext(6)
      setClientDetails((values) => ({ ...values, businessPanData: formData }))
    }
    setErrors(errors)

    // const errorMsg = businesDetailValidation(formData, setErrors, validaName)
    // if (_.isEmpty(errorMsg)) {
    //   setClientDetails((values) => ({ ...values, businessDetails: formData }))
    //   // setSummary((values) => ({
    //   //   ...values,
    //   //   user: {
    //   //     roleId: getRolelabel && getRolelabel.label
    //   //   }
    //   // }))
    //   setLocalStorage(KYC_FORM.BUSINESS_DETAILS, JSON.stringify(formData))
    // }
  }

  const OnclickbusinessPan = () => {
    const errors = {}
    if (_.isEmpty(formData.businessPan)) {
      errors.businessPan = 'BusinessPan Is Required'
    } else if (formData.businessPan && !REGEX.PAN.test(formData.businessPan)) {
      errors.businessPan = 'BusinessPan Number Is InValid'
    }
    if (_.isEmpty(errors)) {
      const data = {
        businessPan: formData.businessPan
      }
      KycPanVerifyDispatch(data)
    }
    setErrors(errors)
  }

  const onConfirm = (data) => {
    setFormData({
      businessPanName: data,
      businessPan: formData.businessPan
    })
  }

  useEffect(() => {
    if (KybusinessPanRes && KybusinessPanRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      setBusinessPanVerify(true)
      confirmationAlert(
        'Confirm Your Business Name is',
        `${KybusinessPanRes && KybusinessPanRes.name}?`,
        'warning',
        'Yes,confirm',
        'No',
        () => { onConfirm(KybusinessPanRes && KybusinessPanRes.name) },
        () => { { } }
      )
      clearKYCpanVerify()
    } else if (KybusinessPanRes && KybusinessPanRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        KybusinessPanRes && KybusinessPanRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
    }
  }, [KybusinessPanRes])

  return (
    <>
      <div>
        <div className='current' data-kt-stepper-element='content'>
          <div className='w-100'>
            {
              !businessPanVerify ? (
                <div className='fv-row mb-10'>
                  <div className='row mb-4'>
                    <div className='col-lg-12'>
                      <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                        <span className='required fw-bold'>What's your business PAN number?</span>
                      </label>
                    </div>
                    <div className='col-lg-12'>
                      <label className='d-flex align-items-center mb-2'>
                        <span className='text-muted fs-6 fw-bold'>We require this to verify your identify</span>
                        <i
                          className='fas fa-exclamation-circle ms-2 fs-7'
                          data-bs-toggle='tooltip'
                          title='Individual Pan'
                        ></i>
                      </label>
                    </div>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid'
                    name='businessPan'
                    placeholder='Business PAN'
                    onChange={(e) => handleChange(e)}
                    value={formData.businessPan || ''}
                  />
                  {errors && errors.businessPan && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.businessPan}
                    </div>
                  )}
                </div>
              ) : (
                <div className='fv-row mb-10'>
                  <div className='row mb-4'>
                    <div className='col-lg-12'>
                      <label className='d-flex align-items-center fs-5 fw-bold mb-2'>
                        <span className='required fw-bold'>What's your brand name?</span>
                      </label>
                    </div>
                    <div className='col-lg-12'>
                      <label className='d-flex align-items-center mb-2'>
                        <span className='text-muted fs-6 fw-bold'>this is the business name that your customers recognize</span>
                        <i
                          className='fas fa-exclamation-circle ms-2 fs-7'
                          data-bs-toggle='tooltip'
                          title='Individual Pan'
                        ></i>
                      </label>
                    </div>
                  </div>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid'
                    name='businessPanName'
                    placeholder='BusinessPanName'
                    onChange={(e) => handleChange(e)}
                    value={formData.businessPanName || ''}
                    onKeyPress={(e) => {
                      if (!/^[a-zA-Z_ ]*$/.test(e.key)) {
                        e.preventDefault()
                      }
                    }}
                  />
                  {errors && errors.businessPanName && (
                    <div className='rr mt-1'>
                      <style>{'.rr{color:red;}'}</style>
                      {errors.businessPanName}
                    </div>
                  )}
                </div>

              )
            }
            <div className='fv-row'>
              <div className='d-flex flex-stack pt-10 justify-content-end'>
                <div className='me-2'>
                  {
                    !businessPanVerify ? (

                      <button type='submit' className={`btn btn-sm ${!businessPanVerify ? `btn-light-primary` : `btn-primary`}`}
                        onClick={() => { OnclickbusinessPan() }}
                        disabled={KYCbusinessPanLoading}
                      >
                        {!KYCbusinessPanLoading &&
                          <span className='indicator-label'>
                            <i className={`${!businessPanVerify ? `bi bi-person-check-fill` : `bi bi-next`}`} />
                            {!businessPanVerify ? `Continue` : `Verified`}
                          </span>
                        }
                        {KYCbusinessPanLoading && (
                          <span className='indicator-progress' style={{ display: 'block' }}>
                            Please wait...
                            <span className='spinner-border spinner-border-sm align-middle ms-2' />
                          </span>
                        )}
                      </button>
                    ) : (
                      <button type='submit' className={`btn btn-sm ${!businessPanVerify ? `btn-light-primary` : `btn-light-primary`}`}
                        onClick={() => { handleSubmit() }}
                      >
                        <KTSVG
                          path='/media/icons/duotune/arrows/arr064.svg'
                          className='svg-icon-3 ms-2 me-0'
                        />
                        Next
                      </button>
                    )
                  }
                  <button type='submit' className='btn btn-sm btn-light-danger ms-2'
                    onClick={() => { onClickNext(6) }}
                  >
                    <span className='indicator-label'>
                      <KTSVG
                        path='/media/icons/duotune/files/fil007.svg'
                        className='svg-icon-3 me-2'
                      />
                      Skip
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

const mapStateToProps = state => {
  const { StatelistStore, CitylistStore, KYCbusinessPanStore, KYCcinStore, KYCgstinStore, PersonalPanStore } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    getCitys: CitylistStore && CitylistStore.Citylists ? CitylistStore.Citylists : {},
    KYCbusinessPanLoading: KYCbusinessPanStore && KYCbusinessPanStore.loading ? KYCbusinessPanStore.loading : false,
    KybusinessPanRes: KYCbusinessPanStore && KYCbusinessPanStore.KybusinessPan ? KYCbusinessPanStore.KybusinessPan : {},
    KYCbusinessCinLoading: KYCcinStore && KYCcinStore.loading ? KYCcinStore.loading : false,
    KYCcinResponse: KYCcinStore && KYCcinStore.KycCIN ? KYCcinStore.KycCIN : {},
    KYCgstinLoading: KYCgstinStore && KYCgstinStore.loading ? KYCgstinStore.loading : false,
    KYCgstinResponse: KYCgstinStore && KYCgstinStore.Kycgstin ? KYCgstinStore.Kycgstin : {},
  }
}

const mapDispatchToProps = (dispatch) => ({
  getCityDispatch: (params) => dispatch(CityActions.getCitylist(params)),
  KycPanVerifyDispatch: (params) => dispatch(KYCpanAction.KYCpanVerify(params)),
  clearKYCpanVerify: (params) => dispatch(KYCpanAction.clearKYCpanVerify(params)),
  KycCinVerifyDispatch: (params) => dispatch(KYCcinAction.KYCcinVerify(params)),
  KycGstinVerifyDispatch: (params) => dispatch(KYCgstinAction.KYCgstinVerify(params)),
  PersonalPanDispatch: (params) => dispatch(KYCPersonalpanAction.KYCpersonalpanVerify(params))

})

export default connect(mapStateToProps, mapDispatchToProps)(BusinessPanForm)