import React, { useEffect, useState } from "react"
import { connect } from 'react-redux'
import {
  form80UploadAction,
  FullKycValueAction
} from '../../../store/actions'
import { Camera, FACING_MODES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import {
  STATUS_RESPONSE
} from '../../../utils/constants'
import { warningAlert } from "../../../utils/alerts"

const CameraPhoto = (props) => {
  const {
    onClickNext,
    setFullKycDetails,
    fullKycDispatch,
    clearFullKycValueDispatch,
    FullKycResData,
    FullKycLoading,
    kycAllDataSaved
  } = props
  const [url, setUrl] = useState(null)
  const [hasCameraAccess, setHasCameraAccess] = useState(false);

  const onUserMedia = (e) => {
    console.log('webCam', e)
  }


  const Capture = () => {
    const params = {
      videoKycImage: url,
      responseType : "CREATED"
    }
    fullKycDispatch(kycAllDataSaved && kycAllDataSaved.id, params)
  }

  useEffect(() => {
    if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      clearFullKycValueDispatch()
      onClickNext(7)
      setUrl(null)
    } else if (FullKycResData && FullKycResData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        FullKycResData && FullKycResData.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearFullKycValueDispatch()
    }
  }, [FullKycResData])


  useEffect(() => {
    checkCameraAccess();
  }, []);

  const checkCameraAccess = async () => {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({ video: true });
      setHasCameraAccess(true);
      stream.getTracks().forEach((track) => track.stop());
    } catch (error) {
      console.error('Camera access error:', error);
      setHasCameraAccess(false);
    }
  };

  return (
    <>
      {
        hasCameraAccess ? (
          <>
            <div id="imageWrapper" />
            <canvas id="canvas" width="5" height="5" />
            {
              !url ? (
                <>
                  <div className="d-flex justify-content-center mt-4 camera-style">
                    <Camera
                      idealFacingMode={FACING_MODES.ENVIRONMENT}
                      isImageMirror={false}
                      isFullScreen={true}
                      isMaxResolution={true}
                      style={{
                        borderRadius: '20px',
                        backgroundSize: 'cover'
                      }}
                      sizeFactor={1}
                      onUserMedia={onUserMedia}
                      onTakePhoto={(dataURI) => {
                        setUrl(dataURI);
                        setFullKycDetails((values) => ({ ...values, vedioKyc_Face: dataURI }))
                      }}
                    />
                  </div>
                </>
              ) : (
                <>
                  <div className="row">
                    <div className="col-lg-2" />
                    <div className="col-lg-8 ms-4">
                      <div className="card card-custom overlay overflow-hidden">
                        <div className="card-body p-0">
                          <div className="overlay-wrapper">
                            <img
                              src={url}
                              className="w-100 rounded"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-2" />
                  </div>
                  <div className="d-flex justify-content-center mt-4">
                    <button
                      className="btn btn-sm btn-light-danger me-4"
                      onClick={() => setUrl(null)}
                    >
                      Re Capture
                    </button>
                    <button type='submit' className={`btn btn-sm  btn-light-primary`}
                      onClick={() => { Capture() }}
                      disabled={FullKycLoading}
                    >
                      {!FullKycLoading &&
                        <span className='indicator-label'>
                          <i className={`bi bi-bank`} />
                          Next
                        </span>
                      }
                      {FullKycLoading && (
                        <span className='indicator-progress' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </>
              )
            }
          </>
        ) : (
            <div className="d-flex justify-content-center mt-4">
            <div className="text-dark mb-4">Your Camera is not accessible</div>
              <button
                className="btn btn-sm btn-light-dark me-4 ms-4"
                onClick={() => {
                  onClickNext(7)
                }}
              >
                Proceed
              </button>
            </div>
        )
      }

    </>
  )
}


const mapStateToProps = state => {
  const { StatelistStore, FullKycValueStore } = state
  return {
    getStates: StatelistStore && StatelistStore.Statelists ? StatelistStore.Statelists : {},
    FullKycResData: FullKycValueStore && FullKycValueStore.FullKycValue ? FullKycValueStore.FullKycValue : {},
    FullKycLoading: state && state.FullKycValueStore && state.FullKycValueStore.loading,
  }
}

const mapDispatchToProps = (dispatch) => ({
  form80UploadDispatch: (params) => dispatch(form80UploadAction.form80Upload(params)),
  fullKycDispatch: (id, params) => dispatch(FullKycValueAction.FullKycValue(id, params)),
  clearFullKycValueDispatch: (id, params) => dispatch(FullKycValueAction.clearFullKycValue(id, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(CameraPhoto)
