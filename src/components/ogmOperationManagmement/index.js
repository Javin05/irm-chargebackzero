import React, { useEffect, useState, useRef } from "react";
import _ from "lodash";
import {
  OgmOperationManagementActions,
  GetClientsActions,
  clientIdLIstActions,
  WRMOperatorsListActions,
} from "../../store/actions";
import SearchList from "./searchList";
import { connect } from "react-redux";
import AssignToUser from "./AssignToUser";
import ReactPaginate from "react-paginate";
import {
  getLocalStorage,
  removeLocalStorage,
  setLocalStorage,
} from "../../utils/helper";
import { SET_FILTER, STATUS_RESPONSE } from "../../utils/constants";
import moment from "moment";
import FindRole from "./Role";
import { useHistory } from "react-router-dom";
import CancelAssignment from "./CancelAssignment";
import { confirmAlert, warningAlert } from "../../utils/alerts";

function OgmOperationManagementList(props) {
  const {
    getOgmOperationManagementlistDispatch,
    className,
    OgmOperationManagement,
    loading,
    OgmOperationActionStatus,
    clientIdDispatch,
    clinetIdLists,
    getWRMOperatorsDispatch,
  } = props;

  const didMount = React.useRef(false);
  const [limit, setLimit] = useState(25);
  const [, setData] = useState({});
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [Value, setValue] = useState(false);
  const paginationSearch = JSON.parse(getLocalStorage("TAG"));
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  const [formData, setFormData] = React.useState([]);
  const [selectAllChecked, setSelectAllChecked] = useState(false);
  const [sorting, setSorting] = useState({
    clientId: false,
    taskId: false,
    caseId: false,
    workStatus: false,
    operationStatus: false,
    assignedFrom: false,
    assignedTo: false,
    tag: false,
    createdTime: false,
  });
  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER);
    removeLocalStorage("skip");
    setLocalStorage("skip", 1);
    const credBasedParams = {
      clientId: credBasedClientValue,
    };
    const params = {
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    };
    let clientParams = { limit: 50 };
    const value = {
      skipPagination: "true",
    };
    clientIdDispatch(value);
    const pickByParams = _.pickBy(params);
    getOgmOperationManagementlistDispatch(pickByParams);
    removeLocalStorage("TAG");
    getWRMOperatorsDispatch();
  }, [OgmOperationActionStatus]);

  const history = useHistory();
  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: pageNumber,
      tag: paginationSearch.tag ? paginationSearch.tag : "",
      clientId: paginationSearch.clientId ? paginationSearch.clientId : "",
      assignedTo: paginationSearch.assignedTo
        ? paginationSearch.assignedTo
        : "",
      operationStatus: paginationSearch.operationStatus
        ? paginationSearch.operationStatus
        : "",
      workStatus: paginationSearch.workStatus
        ? paginationSearch.workStatus
        : "",
    };
    setActivePageNumber(pageNumber);
    getOgmOperationManagementlistDispatch(params);
  };

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name];
      setSorting(sorting);
      setData({});
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "DESC",
      };
      getOgmOperationManagementlistDispatch(params);
    } else {
      const filter = _.mapValues(sorting, () => {
        return false;
      });
      filter[name] = !filter[name];
      setSorting(filter);
      setData({});
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: "ASC",
      };
      getOgmOperationManagementlistDispatch(params);
    }
  };

  const totalPages =
    OgmOperationManagement &&
    OgmOperationManagement.data &&
    OgmOperationManagement.data.count
      ? Math.ceil(
          parseInt(
            OgmOperationManagement &&
              OgmOperationManagement.data &&
              OgmOperationManagement.data.count
          ) / limit
        )
      : 1;

  const tagSearch = JSON.parse(getLocalStorage("WEBSITSEARCH"));

  useEffect(() => {
    return (
      setValue(true),
      setTimeout(() => {
        setValue(false);
      }, 1500)
    );
  }, []);

  const hadelRefresh = () => {
    getOgmOperationManagementlistDispatch(tagSearch);
  };

  const hadelReset = () => {
    const params = {
      limit: limit,
      page: 1,
      ...searchParams,
    };
    getOgmOperationManagementlistDispatch(params);
    removeLocalStorage("WEBSITSEARCH");
    setValue(true);
  };

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  });
  useEffect(() => {
    const data =
      OgmOperationManagement &&
      OgmOperationManagement.data &&
      OgmOperationManagement.data.result;
    if (!_.isEmpty(formData)) {
      if (formData && formData.length === data.length) {
        setSelectAllChecked(true);
      } else {
        setSelectAllChecked(false);
      }
    }
  }, [formData]);

  const handleChange = (e) => {
    e.persist();
    if (e.target.checked === true) {
      setFormData([...formData, e.target.value]);
    } else if (e.target.checked === false) {
      let freshArray = formData.filter((val) => val !== e.target.value);
      setFormData([...freshArray]);
    }
  };
  const handleChangeAll = (e) => {
    e.persist();
    if (e.target.checked === true) {
      let data = OgmOperationManagement.data.result.map((item) => item._id);
      setFormData(data);
    } else if (e.target.checked === false) {
      setFormData([]);
      setSelectAllChecked(false);
    }
  };
  useEffect(() => {
    if (OgmOperationActionStatus?.statusEUR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmAlert(
        "Success",
        OgmOperationActionStatus.messageEUR,
        "success",
        "Ok"
      );
    } else if (
      OgmOperationActionStatus?.statusEUR === STATUS_RESPONSE.ERROR_MSG
    ) {
      warningAlert(
        "Error",
        OgmOperationActionStatus.messageEUR,
        "error",
        "Close",
        "Ok",
        () => {}
      );
    }
  }, [OgmOperationActionStatus]);
  console.log(OgmOperationActionStatus);
  return (
    <>
      <div
        type="button"
        className="d-none"
        data-toggle="modal"
        id="reject-model"
        data-target="#categoryModal"
        onClick={() => {}}
      ></div>
      <div className={`card ${className}`}>
        <div className="card-body py-3">
          <div className="row">
            <div className="d-flex justify-content-start col-md-12 col-lg-12">
              <div className="col-md-6 mt-1 ms-2">
                {OgmOperationManagement &&
                  OgmOperationManagement.data &&
                  OgmOperationManagement.data.count && (
                    <span className="text-muted fw-bold d-flex fs-3 mt-2">
                      Total:
                      <span className="text-gray-700 fw-bolder text-hover-primary fs-3">
                        {OgmOperationManagement &&
                          OgmOperationManagement.data &&
                          OgmOperationManagement.data.count}
                      </span>
                    </span>
                  )}
              </div>
            </div>
            <div className="d-flex col-md-12 col-lg-12 justify-content-end my-auto mt-4">
              <ul className="nav nav-tabs nav-line-tabs fs-6">
                <li className="nav-item">
                  <SearchList
                    Value={Value}
                    clientList={
                      clinetIdLists &&
                      clinetIdLists.data &&
                      clinetIdLists.data.result
                        ? clinetIdLists.data.result
                        : []
                    }
                  />
                </li>
              </ul>
              {(Role === "Admin" || Role === "Supervisor") && (
                <>
                  <div className="my-auto">
                    <AssignToUser
                      cases={formData}
                      searchParams={searchParams}
                    />
                  </div>
                  <div className="my-auto">
                    <CancelAssignment
                      cases={formData}
                      searchParams={searchParams}
                    />
                  </div>
                </>
              )}

              <div className="my-auto">
                <button
                  onClick={() => hadelReset()}
                  type="button"
                  className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                >
                  Reset
                </button>
              </div>
              <div className="my-auto">
                <button
                  onClick={() => hadelRefresh()}
                  type="button"
                  className="btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right"
                >
                  Refresh
                </button>
              </div>
            </div>
            <div className="d-flex col-md-12 justify-content col-lg-12 my-auto mt-4">
              <div className="col-md-4 mt-1 ms-2">
                <span className="text-muted">
                  Supervisor Analyst Pending:
                  <span className="text-gray-700">
                    {OgmOperationManagement &&
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.countSupervisorAanalystPending
                      ? OgmOperationManagement.data
                          .countSupervisorAanalystPending
                      : 0}
                  </span>
                </span>
              </div>
              <div className="col-md-4 mt-1 ms-2">
                <span className="text-muted">
                  Supervisor QA Analyst Pending:
                  <span className="text-gray-700">
                    {OgmOperationManagement &&
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.countSupervisorQAAanalystPending
                      ? OgmOperationManagement.data
                          .countSupervisorQAAanalystPending
                      : 0}
                  </span>
                </span>
              </div>
              <div className="col-md-4 mt-1 ms-2">
                <span className="text-muted">
                  Analyst Pending:
                  <span className="text-gray-700">
                    {OgmOperationManagement &&
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.countAanalystInProgress
                      ? OgmOperationManagement.data.countAanalystInProgress
                      : 0}
                  </span>
                </span>
              </div>
            </div>
            <div className="d-flex col-md-12 justify-content col-lg-12 my-auto mt-4">
              <div className="col-md-4 mt-1 ms-2">
                <span className="text-muted">
                  Analyst in progress:
                  <span className="text-gray-700">
                    {OgmOperationManagement &&
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.countAanalystPending
                      ? OgmOperationManagement.data.countAanalystPending
                      : 0}
                  </span>
                </span>
              </div>
              <div className="col-md-4 mt-1 ms-2">
                <span className="text-muted">
                  QA Pending:
                  <span className="text-gray-700">
                    {OgmOperationManagement &&
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.countQAAanalystPending
                      ? OgmOperationManagement.data.countQAAanalystPending
                      : 0}
                  </span>
                </span>
              </div>
              <div className="col-md-4 mt-1 ms-2">
                <span className="text-muted">
                  QA in progress:
                  <span className="text-gray-700">
                    {OgmOperationManagement &&
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.countQAAanalystInProgress
                      ? OgmOperationManagement.data.countQAAanalystInProgress
                      : 0}
                  </span>
                </span>
              </div>
            </div>
          </div>

          <br />
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className="fw-bolder fs-8 text-gray-800">
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Select all</span>
                    </div>
                    <div className="mt-4">
                      <div className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                        <input
                          className="form-check-input cursor-pointer "
                          type="Checkbox"
                          onChange={(e) => handleChangeAll(e)}
                          name="checkNameSelectAll"
                          checked={selectAllChecked}
                        />
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Client name</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Task id</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("taskId")}
                        >
                          <i
                            className={`bi ${
                              sorting.taskId
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>

                  <th>
                    <div className="d-flex">
                      <span>Case ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("clietnId")}
                        >
                          <i
                            className={`bi ${
                              sorting.website
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Work status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("workStatus")}
                        >
                          <i
                            className={`bi ${
                              sorting.workStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Status</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("operationStatus")}
                        >
                          <i
                            className={`bi ${
                              sorting.operationStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>

                  <th>
                    <div className="d-flex">
                      <span>Assigned from</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("assignedFrom")}
                        >
                          <i
                            className={`bi ${
                              sorting.assignedFrom
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Assigned To</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${
                              sorting.ReportStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Duration(Min)</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${
                              sorting.ReportStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Tag</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("tag")}
                        >
                          <i
                            className={`bi ${
                              sorting.tag
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>created time</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          // onClick={() => handleSorting("ReportStatus")}
                        >
                          <i
                            className={`bi ${
                              sorting.ReportStatus
                                ? "bi-arrow-up-circle-fill"
                                : "bi-arrow-down-circle"
                            } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className="fs-8">
                {!loading ? (
                  OgmOperationManagement && OgmOperationManagement.data ? (
                    OgmOperationManagement.data &&
                    OgmOperationManagement.data.result.map(
                      (riskoperationmgmtlist, i) => {
                        return (
                          <tr
                            key={"reef_" + i}
                            style={
                              i === 0
                                ? { borderColor: "black" }
                                : { borderColor: "white" }
                            }
                          >
                            <td className="min-width-150px text-center form-check form-check-custom form-check-success form-check-solid">
                              <input
                                className="form-check-input cursor-pointer "
                                type="Checkbox"
                                value={riskoperationmgmtlist._id}
                                onChange={(e) => handleChange(e)}
                                // checked={formData.checkName === item.tag || ''}
                                name="checkName"
                              />
                            </td>
                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.clientId?.company
                                  ? riskoperationmgmtlist.clientId?.company
                                  : "--"}
                              </>
                            </td>
                            <td className="ellipsis">
                              <a
                                className="color-primary cursor-pointer"
                                onClick={() =>
                                  history.push(
                                    `/ogmmanagement/update/${riskoperationmgmtlist._id}`
                                  )
                                }
                                to={`/ogmmanagement/update/${riskoperationmgmtlist._id}`}
                              >
                                TIN
                                {riskoperationmgmtlist.ticketId
                                  ? riskoperationmgmtlist.ticketId
                                  : "--"}
                              </a>
                            </td>
                            <td className="ellipsis">
                              <a
                                className="color-primary cursor-pointer"
                                onClick={() =>
                                  window.open(
                                    `/risk-summary/update/${riskoperationmgmtlist._id}`,
                                    "_blank"
                                  )
                                }
                                to={`/risk-summary/update/${riskoperationmgmtlist._id}`}
                              >
                                OGM
                                {riskoperationmgmtlist.caseId
                                  ? riskoperationmgmtlist.caseId
                                  : "--"}
                              </a>
                            </td>
                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.workStatus
                                  ? riskoperationmgmtlist.workStatus
                                  : "--"}
                              </>
                            </td>
                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.operationStatus
                                  ? riskoperationmgmtlist.operationStatus
                                  : "--"}
                              </>
                            </td>

                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.assignedFrom?.firstName
                                  ? riskoperationmgmtlist.assignedFrom
                                      ?.firstName
                                  : "--"}
                              </>
                            </td>
                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.assignedTo?.firstName
                                  ? riskoperationmgmtlist.assignedTo?.firstName
                                  : "--"}
                              </>
                            </td>
                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.taskEndTime
                                  ? moment
                                      .duration(
                                        moment(
                                          riskoperationmgmtlist.taskEndTime
                                        ).diff(
                                          moment(
                                            riskoperationmgmtlist.taskStartTime
                                          )
                                        )
                                      )
                                      .asMinutes()
                                      .toFixed(2)
                                  : "--"}
                              </>
                            </td>
                            <td className="ellipsis">
                              {riskoperationmgmtlist.tag
                                ? riskoperationmgmtlist.tag
                                : "--"}
                            </td>
                            <td className="ellipsis">
                              <>
                                {riskoperationmgmtlist.caseStartTime
                                  ? moment(riskoperationmgmtlist.caseStartTime)
                                      .startOf("minute")
                                      .fromNow()
                                  : "--"}
                              </>
                            </td>
                          </tr>
                        );
                      }
                    )
                  ) : (
                    <tr className="text-center py-3">
                      <td colSpan="100%">No record(s) found</td>
                    </tr>
                  )
                ) : (
                  <tr>
                    <td colSpan="100%" className="text-center">
                      <div
                        className="spinner-border text-primary m-5"
                        role="status"
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { OgmOperationManagementStore, clinetListStore } = state;
  return {
    OgmOperationManagement:
      state &&
      state.OgmOperationManagementStore &&
      state.OgmOperationManagementStore.OgmOperationManagement,
    loading:
      OgmOperationManagementStore && OgmOperationManagementStore.loading
        ? OgmOperationManagementStore.loading
        : false,
    OgmOperationActionStatus:
      state &&
      state.OgmOperationManagementActionStore &&
      state.OgmOperationManagementActionStore,
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getOgmOperationManagementlistDispatch: (params) =>
    dispatch(OgmOperationManagementActions.getOgmOperationManagemnt(params)),
  clearClientsOgmDispatch: (data) =>
    dispatch(GetClientsActions.clearClientsWrm(data)),
  getClientsWrmDispatch: (data) =>
    dispatch(GetClientsActions.getClientsWrm(data)),
  clientIdDispatch: (data) =>
    dispatch(clientIdLIstActions.getclientIdList(data)),
  getWRMOperatorsDispatch: () =>
    dispatch(WRMOperatorsListActions.getWRMOperatorsList()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OgmOperationManagementList);