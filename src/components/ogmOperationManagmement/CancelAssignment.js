import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import {
 OgmOperatorActions
} from '../../store/actions'
// import './styles.css';
import Modal from 'react-bootstrap/Modal'

function CancelAssignment(props) {
  const {
    cases,
    ogmOperatorActionDispatch,
  } = props
  const [show, setShow] = useState(false)
  const [error, setError] = useState({});
  const [dataValue, setDataValue] = useState({});
  const [role,setRole] = useState("")
  const [manualFormData, setManualFormData] = useState({
     userId:"",
     name:""
  })

  
  const handleCancel = async() => {
     const params = {
      cases:cases,
      name:"cancel",
      assignedTo:manualFormData.userId,
    }
     await ogmOperatorActionDispatch(params)
    setShow(false)
    setDataValue(params)
  
  
  }

  const clearPopup = () => {
    setShow(false)
  }

  useEffect(() => {
    setManualFormData((values) => ({
      ...values,
    }))

  }, [show])

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          onClick={() => { setShow(true) }}
          disabled={cases.length===0}

        >
          {/* eslint-disable */}
          {/* <KTSVG path='/media/icons/duotune/general/gen021.svg' /> */}
          {/* eslint-disable */}
          
          Cancel Assignment
        </button>
      </div>

      <Modal
        show={show}
        size="lg"
        centered
        onHide={() => clearPopup()}>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Assign task
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
             <div>
                Cancel all selected cases from user
             </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  className='btn btn-light-primary m-1 mt-8 font-5vw '
                  onClick={()=>handleCancel()}>
                  Confirm
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => ({state})


const mapDispatchToProps = dispatch => ({
  ogmOperatorActionDispatch:(params)=>dispatch(OgmOperatorActions.ogmOperatorActionInit(params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CancelAssignment)