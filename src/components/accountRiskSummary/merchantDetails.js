import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import moment from 'moment'
import { useLocation } from 'react-router-dom'
import { merchantIdDetailsActions, GetAsigneeActions, AsiggnActions } from '../../store/actions'
import color from "../../utils/colors"
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import { STATUS_RESPONSE } from '../../utils/constants'
import _ from 'lodash'
import { getLocalStorage } from '../../utils/helper'

function MerchantDetails(props) {
  const {
    merchantSummary,
    getAssignee,
    getAsigneeslistDispatch,
    updateAssignDispatch,
    updateAssign,
    clearAsiggnDispatch,
  } = props

  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [formData, setFormData] = useState({
    assignedTo: ''
  })
  const [value, setValue] = useState()
  useEffect(() => {
    const params = {
      tag: "IRM"
    }
    getAsigneeslistDispatch(params)
  }, [])

  const StatusArray = {
    "PENDING": "badge-light-warning",
    "APPROVED": "badge-light-success",
    "REJECTED": "badge-light-danger",
    "HOLD": "badge-light-warning",
    "MANUAL REVIEW": "badge badge-orange text-black",
  }

  const AsigneesNames = getAssignee && getAssignee.data && getAssignee.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].firstName , value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const onConfirmupdate = (() => {
    updateAssignDispatch(currentId, formData)
  })

  const Assign = (() => {
    confirmationAlert(
      "Do You Want To",
      `Assign this Case #${merchantSummary && merchantSummary.riskId} to ${value}?`,
      'warning',
      'Yes',
      'No',
      () => { onConfirmupdate() },
      () => { }
    )
  })

  useEffect(() => {
    if (updateAssign && updateAssign.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        "Assigned Successfully",
        'success',
        )      
        clearAsiggnDispatch()
        setFormData({
          assignedTo:''
        })
    } else if (updateAssign && updateAssign.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        updateAssign.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearAsiggnDispatch()
    }
  }, [updateAssign])

  return (
    <>
      <div className='col-md-12 card card-xl-stretch mb-xl-8'>
        <div className='row border-0 p-5'>
          <div className='col-md-6'>
            <h3 className='card-title align-items-start flex-column '>
              <span className='d-flex align-items-center fw-boldest my-1 fs-2'>Case Details - Case ID #IRM-{merchantSummary && merchantSummary.riskId ? merchantSummary.riskId : '--'}
                <span className={`badge ml-2 ${merchantSummary && merchantSummary.riskStatus ? StatusArray[merchantSummary && merchantSummary.riskStatus] : ""}`} style={{ padding: "4px 12px", marginLeft: "10px", fontSize: "14px" }}>
                  {merchantSummary && merchantSummary.riskStatus ? merchantSummary.riskStatus : ""}
                </span>
              </span>
            </h3>
          </div>
          <div className='col-md-6'>
            <div className='row align-items-center justify-content-end'>
              <div className='col-xl-9 col-lg-8 col-md-7 col-sm-6'>
                <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2 text-end'>
                  Date Received:
                </div>
              </div>
              <div className='col-xl-3 col-lg-4 col-md-5 col-sm-6 text-end'>
                <div className='text-700 pl-3 fw-bold'>
                  {moment(merchantSummary && merchantSummary.createdAt ? merchantSummary.createdAt : "--").format('DD/MM/YYYY')}
                </div>
              </div>
            </div>
          </div>
          {
            !_.isEmpty(formData.assignedTo) ? (
              <span className='mt-4'>
              <button 
              class="btn btn-light-primary btn-sm"
              onClick={Assign}
              >
                Assign
                </button>
              </span>
            ):(
              null
            )
          }
        </div>
        <div className="separator separator-dashed my-3" />
        <div className="row row g-5 g-xl-8">
          <div className='col-sm-4 col-md-4 col-lg-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-body pt-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Queue:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.queueId && merchantSummary.queueId.queueName ? merchantSummary.queueId.queueName : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Name:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessName ? merchantSummary.businessName : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Phone:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessPhone ? merchantSummary.businessPhone : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Email:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessEmail ? merchantSummary.businessEmail : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Address:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessAddress ? merchantSummary.businessAddress : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business URL:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.website ? merchantSummary.website : '--'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-4 col-md-4 col-lg-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-body pt-0'>
                <div className='row mt-1'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Customer Name:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      <span>{merchantSummary && merchantSummary.firstName ? merchantSummary.firstName : '--'}</span>
                      <span className='ms-1'>{merchantSummary && merchantSummary.lastName ? merchantSummary.lastName : '--'}</span>
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Phone Number:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.phone ? merchantSummary.phone : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Email Address:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.personalEmail ? merchantSummary.personalEmail : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Customer Address:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.address ? merchantSummary.address : '--'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-sm-4 col-md-4 col-lg-4'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-body pt-0'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Average Monthly Volume:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.averageMonthlyVolume ? merchantSummary.averageMonthlyVolume : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Average Transaction Amount:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.gmvId ? merchantSummary.gmvId : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                    MCC Code:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.mccCode ? merchantSummary.mccCode : '--'}
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-gray-700 fw-bold mb-1 fs-5 pl-3 ml-2'>
                      Business Ownership Type:
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='text-700 pl-3 fw-bold'>
                      {merchantSummary && merchantSummary.businessOwnershipType && merchantSummary.businessOwnershipType.businessType ? merchantSummary.businessOwnershipType.businessType : '--'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { dashboardStore, riskManagementlistStore, editMerchantStore, postAsigneeStore, updateAssignStore } = state

  return {
    dashboardDetails:
      dashboardStore &&
        dashboardStore.dashboardDetails
        ? dashboardStore.dashboardDetails
        : {},
    riskmgmtlistdetails:
      riskManagementlistStore &&
        riskManagementlistStore.riskmgmtlists ?
        riskManagementlistStore.riskmgmtlists : null,
    merchantIddetails: editMerchantStore && editMerchantStore.merchantIddetail ? editMerchantStore.merchantIddetail : {},
    getAssignee: postAsigneeStore && postAsigneeStore.getAssignee ? postAsigneeStore.getAssignee : {},
    updateAssign: updateAssignStore && updateAssignStore.updateAssign ? updateAssignStore.updateAssign : {}
  }
}

const mapDispatchToProps = (dispatch) => ({
  merchantIdDetail: (id) => dispatch(merchantIdDetailsActions.getmerchantDetails(id)),
  getAsigneeslistDispatch: (params) => dispatch(GetAsigneeActions.GetAsignee(params)),
  updateAssignDispatch: (id,params) => dispatch(AsiggnActions.Asiggn(id,params)),
  clearAsiggnDispatch: () => dispatch(AsiggnActions.clearAsiggn())
})

export default connect(mapStateToProps, mapDispatchToProps)(MerchantDetails)