import { useEffect, useState } from 'react'
import Xarrow, {useXarrow} from 'react-xarrows';
import Icofont from 'react-icofont';
import './index.css'

const axios = require('axios');

function NetworkGraph() {
    const updateXarrow = useXarrow();
    const [path, pathType] = useState("straight"); //"smooth" | "grid" | "straight"
    const [childlineColor, setChildlineColor] = useState("#df9f81ee"); //"smooth" | "grid" | "straight"
    const [highlightColor, setHighlightColor] = useState("#de6a33"); //"smooth" | "grid" | "straight"
    const [highlight, setHighlight] = useState(null);
    const dummyData = [
        {
            "_id": "Phone_1",
            "source": "Phone",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "phone": "+919003521833",
            "personalEmail": "shunmugasundaram.p@chargebackzero.com",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "firstName": "dani",
            "lastName": "N",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-28T11:09:54.052Z",
            "updatedAt": "2022-03-28T11:09:54.052Z",
            "riskId": 100118
        },
        {
            "_id": "Phone_2",
            "source": "Phone",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "personalEmail": "ajithkumar.p@chargebackzero.com",
            "phone": "+919003521833",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "firstName": "javin",
            "lastName": "P",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-15T08:38:30.076Z",
            "updatedAt": "2022-03-15T08:38:30.076Z",
            "riskId": 100104
        },
        {
            "_id": "Phone_3",
            "source": "Phone",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "personalEmail": "ajithkumar.p@chargebackzero.com",
            "phone": "+919003521833",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "firstName": "javin",
            "lastName": "P",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-15T08:38:30.076Z",
            "updatedAt": "2022-03-15T08:38:30.076Z",
            "riskId": 100104
        },
        {
            "_id": "Phone_4",
            "source": "Phone",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "personalEmail": "ajithkumar.p@chargebackzero.com",
            "phone": "+919003521833",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "firstName": "javin",
            "lastName": "P",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-15T08:38:30.076Z",
            "updatedAt": "2022-03-15T08:38:30.076Z",
            "riskId": 100104
        },
        {
            "_id": "Email_1",
            "source": "Email",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "phone": "+919003521833",
            "personalEmail": "shunmugasundaram.p@chargebackzero.com",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "firstName": "Ajith",
            "lastName": "N",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-28T11:09:54.052Z",
            "updatedAt": "2022-03-28T11:09:54.052Z",
            "riskId": 100118
        },
        {
            "_id": "Email_2",
            "source": "Email",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "personalEmail": "shunmugasundaram.p@chargebackzero.com",
            "phone": "+919003521833",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "firstName": "Shunmugasundaram",
            "lastName": "P",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-15T08:38:30.076Z",
            "updatedAt": "2022-03-15T08:38:30.076Z",
            "riskId": 100104
        },
        {
            "_id": "Email_3",
            "source": "Email",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "phone": "+919003521833",
            "personalEmail": "shunmugasundaram.p@chargebackzero.com",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "firstName": "dani",
            "lastName": "N",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-28T11:09:54.052Z",
            "updatedAt": "2022-03-28T11:09:54.052Z",
            "riskId": 100118
        },
        {
            "_id": "Device_1",
            "source": "Device",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "phone": "+919003521833",
            "personalEmail": "shunmugasundaram.p@chargebackzero.com",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "firstName": "Ajith",
            "lastName": "N",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-28T11:09:54.052Z",
            "updatedAt": "2022-03-28T11:09:54.052Z",
            "riskId": 100118
        },
        {
            "_id": "Device_2",
            "source": "Device",
            "status": "ACTIVE",
            "riskStatus": "PENDING",
            "queueRefId": "6227261e71339114c1c3b512",
            "accountType": "BUSINESS",
            "queueStatus": "UN ASSIGNED",
            "personalEmail": "shunmugasundaram.p@chargebackzero.com",
            "phone": "+919003521833",
            "address": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "ipAddress": "49.37.218.122",
            "deviceID": "0006ad1b-9a28-4a2e-8c50-1273746d7ead",
            "firstName": "Shunmugasundaram",
            "lastName": "P",
            "companyName": "Chargebackzero",
            "businessEmail": "shunmugasundaram.p@chargebackzero.com",
            "businessPhone": "+919003521833",
            "businessAddress": "19/1, Old Agraharam Street, Tallakulam, Madurai 625002, Tamil Nadu",
            "website": "chargebackzero.com",
            "createdAt": "2022-03-15T08:38:30.076Z",
            "updatedAt": "2022-03-15T08:38:30.076Z",
            "riskId": 100104
        }
    ];

    const HighlightHandler = (e, lighter) =>{
        e.preventDefault();
        setHighlight(lighter);
        updateXarrow();
    }

    return (
        <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
                <div className='some-page-wrapper'>
                    <div className='graph-row'>
                        <div className='column'>
                            <span>suresh.mfilterit@shenll.com</span>
                            <div className="box main" id="main">
                                <Icofont icon="student-alt" style={{color:'black'}}  />
                            </div>
                        </div>
                        <div className='column'>
                            <span>Phone</span>
                            <div className="box" id="Phone"  onClick={(e)=>HighlightHandler(e, "Phone")}>
                                <Icofont icon="icofont-ui-touch-phone" style={{color:'black'}} />
                            </div>
                            <span>Ip</span>
                            <div className="box" id="Ip"  onClick={(e)=>HighlightHandler(e, "Ip")}>
                                <Icofont icon="icofont-eclipse" style={{color:'black'}} />
                            </div>
                            <span>Email</span>
                            <div className="box" id="Email"  onClick={(e)=>HighlightHandler(e, "Email")}>
                                <Icofont icon="icofont-email" style={{color:'black'}} />
                            </div>
                            <span>Website</span>
                            <div className="box" id="Website"  onClick={(e)=>HighlightHandler(e, "Website")}>
                                <Icofont icon="icofont-web" style={{color:'black'}} />
                            </div>
                            <span>Device</span>
                            <div className="box" id="Device"  onClick={(e)=>HighlightHandler(e, "Device")}>
                                <Icofont icon="icofont-computer" style={{color:'black'}} />
                            </div>
                        </div>
                        <div className='column mt-4'>
                            { dummyData && dummyData.map((dummy, index) => (
                                <>
                                    <span>{dummy.personalEmail}</span>
                                    <div key={"X_" + index} className={highlight===dummy.source? "box highlight" : "box"} id={dummy._id}>
                                        <span><Icofont icon="icofont-at" style={{color:'black'}}/></span>
                                    </div>
                                </>
                            ))}
                        </div>
                        <Xarrow start={'main'} end={'Phone'} path={path} color={childlineColor} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler(e, "Phone")}}/>
                        <Xarrow start={'main'} end={'Ip'} path={path} color={childlineColor} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler(e, "Ip")}}/>
                        <Xarrow start={'main'} end={'Email'} path={path} color={childlineColor} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler(e, "Email")}}/>
                        <Xarrow start={'main'} end={'Website'} path={path} color={childlineColor} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler(e, "Website")}}/>
                        <Xarrow start={'main'} end={'Device'} path={path} color={childlineColor} headSize={4} strokeWidth={2} passProps={{onClick: (e) => HighlightHandler(e, "Device")}}/>
                        { dummyData && dummyData.map((dummy, ind) => (
                            <Xarrow key={"Y_" + ind} start={dummy.source} end={dummy._id} path={path} color={ highlight === dummy.source ? highlightColor : childlineColor} headSize={4} strokeWidth={2} dashness={ highlight === dummy.source ? true : false } animateDrawing={true} animation={true}/>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default NetworkGraph;