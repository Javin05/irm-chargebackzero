import React, { Fragment } from 'react'
import { connect } from "react-redux";
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'
import { CHANGE_ICON } from '../../../utils/constants';

function BusinessPhone(props) {
  const {
    getRiskSummarys,
    dashboardDetails,
    BusinessPhoneData,
    BusinessPhoneDataValue,
  } = props;
  const viewData =
    getRiskSummarys && getRiskSummarys && getRiskSummarys.data
      ? getRiskSummarys.data
      : [];
  const getData = _.isArray(viewData) ? viewData.filter(o => (o ? o : '')) : ''
  const viewPhone =
    getData &&
      getData[0] &&
      getData[0].businessVerifyPhone &&
      getData[0].businessVerifyPhone.message.result &&
      getData[0].businessVerifyPhone.message.result
      ? getData[0].businessVerifyPhone.message.result
      : "--";
  const speedometetervalue =
    dashboardDetails && dashboardDetails?.data && dashboardDetails?.data
      ? dashboardDetails?.data
      : "--";
  const businessPhoneTotal =
    speedometetervalue && speedometetervalue?.businessPhoneTotalScore
      ? speedometetervalue?.businessPhoneTotalScore
      : "0";
  const businessPhonePositive =
    dashboardDetails && dashboardDetails?.data?.businessphonepositive
      ? dashboardDetails?.data?.businessphonepositive
      : "--";
  const businessPhoneNegative =
    dashboardDetails && dashboardDetails?.data?.businessphonenegative
      ? dashboardDetails?.data?.businessphonenegative
      : "--";
  const IDPhoneSize = Math.ceil(BusinessPhoneData && BusinessPhoneData.length / 3);
  const IDPhone1 = BusinessPhoneData && BusinessPhoneData.slice(0, IDPhoneSize);
  const IDPhone2 = BusinessPhoneData && BusinessPhoneData.slice(IDPhoneSize, IDPhoneSize * 2);
  const IDPhone3 = BusinessPhoneData && BusinessPhoneData.slice(IDPhoneSize * 2);

  return (
    <div>
      <div>
        <h1 className="d-flex justify-content-center mb-12">
          Business Phone
        </h1>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={businessPhoneTotal}
            text={`${businessPhoneTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {businessPhoneTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8 mt-8">
          <div className="col-lg-12">
            <h5 className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {!businessPhonePositive ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(businessPhonePositive) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    businessPhonePositive &&
                    typeof businessPhonePositive === "object" &&
                    businessPhonePositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {!businessPhoneNegative ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(businessPhoneNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    businessPhoneNegative &&
                    typeof businessPhonePositive === "object" &&
                    businessPhoneNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </h5>
          </div>
        </div>
        <div className="row">
          <div className="mb-8">
            <a href="#" className="d-flex justify-content-center fs-2">
              CONFIDENCE MATRIX
            </a>
          </div>
        </div>
        <div className="row g-5 g-xl-8 mb-8">
          <div className="col-xl-12">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="d-flex pt-3">
                  <span className="card-label fw-bolder text-dark">
                    Business Phone
                  </span>
                  <span className="ms-2 text-muted">
                    {BusinessPhoneDataValue}
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className='row g-5'>
                  <div className="col-xl-4">
                    <div className="card-body pt-0">
                      {
                        !IDPhone1
                          ? (
                            <div>
                              <div className='text-center'>
                                <div className='spinner-border text-primary m-5' role='status' />
                              </div>
                            </div>
                          )
                          : IDPhone1 && IDPhone1.map((item, i) => {
                            return (
                              <Fragment key={"FIX_3" + i}>
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                      {
                                        !_.isEmpty(item.info) ? (
                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                            className='tooltip'
                                          >
                                            {item.info}
                                          </Tooltip>}
                                            placement={"right"}
                                          >
                                            <span>
                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                            </span>
                                          </OverlayTrigger>
                                        ) : (
                                          null
                                        )
                                      }
                                    </div>
                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                  </div>
                                  <span className='py-1' title={item.message}>
                                    <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              </Fragment>
                            )
                          })
                      }
                    </div>
                  </div>
                  <div className="col-xl-4">
                    <div className="card-body pt-0">
                      {
                        !IDPhone2
                          ? (
                            <div>
                              <div className='text-center'>
                                <div className='spinner-border text-primary m-5' role='status' />
                              </div>
                            </div>
                          )
                          : IDPhone2 && IDPhone2.map((item, i) => {
                            return (
                              <Fragment key={"FIX_3" + i}>
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                      {
                                        !_.isEmpty(item.info) ? (
                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                            className='tooltip'
                                          >
                                            {item.info}
                                          </Tooltip>}
                                            placement={"right"}
                                          >
                                            <span>
                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                            </span>
                                          </OverlayTrigger>
                                        ) : (
                                          null
                                        )
                                      }
                                    </div>
                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                  </div>
                                  <span className='py-1' title={item.message}>
                                    <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              </Fragment>
                            )
                          })
                      }
                    </div>
                  </div>
                  <div className="col-xl-4">
                    <div className="card-body pt-0">
                      {
                        !IDPhone3
                          ? (
                            <div>
                              <div className='text-center'>
                                <div className='spinner-border text-primary m-5' role='status' />
                              </div>
                            </div>
                          )
                          : IDPhone3 && IDPhone3.map((item, i) => {
                            return (
                              <Fragment key={"FIX_3" + i}>
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                      {
                                        !_.isEmpty(item.info) ? (
                                          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                            className='tooltip'
                                          >
                                            {item.info}
                                          </Tooltip>}
                                            placement={"right"}
                                          >
                                            <span>
                                              <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                            </span>
                                          </OverlayTrigger>
                                        ) : (
                                          null
                                        )
                                      }
                                    </div>
                                    <span className='text-muted fw-bold d-block'>{item.value}</span>
                                  </div>
                                  <span className='py-1' title={item.message}>
                                    <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              </Fragment>
                            )
                          })
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  const { accountdashboardGetDetailsStore } = state;
  return {
    getRiskSummarys:
      state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    dashboardDetails:
      accountdashboardGetDetailsStore && accountdashboardGetDetailsStore.aaccountdashboardGetDetails
        ? accountdashboardGetDetailsStore.aaccountdashboardGetDetails
        : {},
  };
};

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(BusinessPhone);
