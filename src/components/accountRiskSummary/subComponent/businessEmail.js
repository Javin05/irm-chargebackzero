import React, { Fragment } from 'react'
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash";
import { CHANGE_ICON } from '../../../utils/constants'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'

function BusinessEmail(props) {
  const { dashboard, BusinessEmailData, BusinessEmailValue } = props;

  let businessEmailTotalScore =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "businessEmailTotalScore" in dashboard
      ? dashboard.businessEmailTotalScore
      : "--"; //Number
  let businessEmailPositive =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "businessemailpositive" in dashboard
      ? dashboard.businessemailpositive
      : null; //Array
  let businessEmailNegative =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "businessemailnegative" in dashboard
      ? dashboard.businessemailnegative
      : null; //Array

  const [IDEmail1, IDEmail2, IDEmail3] = [
    BusinessEmailData && BusinessEmailData.slice(0, Math.ceil(BusinessEmailData && BusinessEmailData.length / 3)),
    BusinessEmailData && BusinessEmailData.slice(Math.ceil(BusinessEmailData && BusinessEmailData.length / 3), Math.ceil((BusinessEmailData && BusinessEmailData.length / 3) * 2)),
    BusinessEmailData && BusinessEmailData.slice(Math.ceil((BusinessEmailData && BusinessEmailData.length / 3) * 2))
  ];

  return (
    <>
      <div className="row mt-8">
        <div className="col-lg-12">
          <h1 className="d-flex justify-content-center mb-4">Business Email</h1>
        </div>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={businessEmailTotalScore}
            text={`${businessEmailTotalScore}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {businessEmailTotalScore}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
      </div>
      <div className="row mb-8 mt-8">
        <div className="col-lg-12">
          <div className="row mb-4">
            <div className="col-lg-6">
              <h1 className="mb-4"> Positive Score Factors</h1>
              {!businessEmailPositive ? (
                <div>
                  <div className="text-center">
                    <div
                      className="spinner-border text-primary m-5"
                      role="status"
                    />
                  </div>
                </div>
              ) : _.isEmpty(businessEmailPositive) ? (
                <span className=" text-danger" role="status">
                  N/A
                </span>
              ) : (
                businessEmailPositive &&
                businessEmailPositive.map((item, i) => {
                  return (
                    <div className="mb-4" key={i}>
                      <div className="symbol symbol-45px me-4 mb-4">
                        <span className="symbol-label bg-success">
                          <span className="fs-5 text-white">
                            {item.riskscore}
                          </span>
                        </span>
                      </div>
                      <span className="fw-bold fs-5">{item.title}</span>
                    </div>
                  );
                })
              )}
            </div>
            <div className="col-lg-6">
              <h1 className="mb-4">Negative Score Factors</h1>
              {!businessEmailNegative ? (
                <div>
                  <div className="text-center">
                    <div
                      className="spinner-border text-primary m-5"
                      role="status"
                    />
                  </div>
                </div>
              ) : _.isEmpty(businessEmailNegative) ? (
                <span className=" text-danger" role="status">
                  N/A
                </span>
              ) : (
                businessEmailNegative &&
                businessEmailNegative.map((item, i) => {
                  return (
                    <div className="mb-4" key={i}>
                      <div className="symbol symbol-45px me-4 mb-4">
                        <span className="symbol-label bg-danger">
                          <span className="fs-5 text-white">
                            {item.riskscore}
                          </span>
                        </span>
                      </div>
                      <span className="fw-bold fs-5">{item.title}</span>
                    </div>
                  );
                })
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="mb-8">
          <a href="#" className="d-flex justify-content-center fs-2">
            CONFIDENCE MATRIX
          </a>
        </div>
      </div>
      <div className="row g-5 g-xl-8 mb-8">

        <div className="col-xl-12">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-header bg-col-bis border-0 ">
              <h3 className="d-flex pt-2">
                <span className="card-label fw-bolder text-dark">Business Email</span>
                <span className="ms-2 text-muted">{BusinessEmailValue}</span>
              </h3>
            </div>
            <div className='row g-5'>
              <div className="col-xl-4">
                <div className="card-body pt-0">
                  {
                    !IDEmail1
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDEmail1 && IDEmail1.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                              <div className='flex-grow-1 me-2'>
                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                  {
                                    !_.isEmpty(item.info) ? (
                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                        className='tooltip'
                                      >
                                        {item.info}
                                      </Tooltip>}
                                        placement={"right"}
                                      >
                                        <span>
                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                        </span>
                                      </OverlayTrigger>
                                    ) : (
                                      null
                                    )
                                  }
                                </div>
                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                              </div>
                              <span className='py-1' title={item.message}>
                                <i
                                  className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                  title={item.message}
                                />
                              </span>
                            </div>
                          </Fragment>
                        )
                      })
                  }
                </div>
              </div>
              <div className="col-xl-4">
                <div className="card-body pt-0">
                  {
                    !IDEmail2
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDEmail2 && IDEmail2.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                              <div className='flex-grow-1 me-2'>
                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                  {
                                    !_.isEmpty(item.info) ? (
                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                        className='tooltip'
                                      >
                                        {item.info}
                                      </Tooltip>}
                                        placement={"right"}
                                      >
                                        <span>
                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                        </span>
                                      </OverlayTrigger>
                                    ) : (
                                      null
                                    )
                                  }
                                </div>
                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                              </div>
                              <span className='py-1' title={item.message}>
                                <i
                                  className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                  title={item.message}
                                />
                              </span>
                            </div>
                          </Fragment>
                        )
                      })
                  }

                </div>
              </div>
              <div className="col-xl-4">
                <div className="card-body pt-0">
                  {
                    !IDEmail3
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDEmail3 && IDEmail3.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                              <div className='flex-grow-1 me-2'>
                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                  {
                                    !_.isEmpty(item.info) ? (
                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                        className='tooltip'
                                      >
                                        {item.info}
                                      </Tooltip>}
                                        placement={"right"}
                                      >
                                        <span>
                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                        </span>
                                      </OverlayTrigger>
                                    ) : (
                                      null
                                    )
                                  }
                                </div>
                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                              </div>
                              <span className='py-1' title={item.message}>
                                <i
                                  className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                  title={item.message}
                                />
                              </span>
                            </div>
                          </Fragment>
                        )
                      })
                  }

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default BusinessEmail;