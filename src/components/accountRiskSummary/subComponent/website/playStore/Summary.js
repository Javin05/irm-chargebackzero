import _ from 'lodash'

function PlayStoreSummarry(props) {
  const {
    playStoreData
  } = props
  
  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        {
       playStoreData && playStoreData.playstore_status === "App details not found" ?(
        
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Playstore Details
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Playstore Url Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.playstoreUrlName ? playStoreData.playstoreUrlName : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Playstore Status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.playstore_status ? playStoreData.playstore_status : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       ) :(
        <>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Data Collected
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    App Activity Details
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.app_activity_details ? playStoreData.app_activity_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    App Info And Performance
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_collected_app_info_and_performance_details ? playStoreData.data_collected_app_info_and_performance_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Contacts Details
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_collected_contacts_details ? playStoreData.data_collected_contacts_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Device Or Other Ids Details
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_collected_device_or_other_ids_details ? playStoreData.data_collected_device_or_other_ids_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Financial Info Details
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_collected_financial_info_details ? playStoreData.data_collected_financial_info_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Location
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_collected_location_details ? playStoreData.data_collected_location_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Personal Info
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_collected_personal_info_details ? playStoreData.data_collected_personal_info_details : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Data Shared
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    App Activity
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_shared_app_activity ? playStoreData.data_shared_app_activity : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Device Or Other Ids
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_shared_device_or_other_ids ? playStoreData.data_shared_device_or_other_ids : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Financial Info
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_shared_financial_info ? playStoreData.data_shared_financial_info : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Location
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_shared_location ? playStoreData.data_shared_location : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Personal Info
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.data_shared_personal_info ? playStoreData.data_shared_personal_info : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Security
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Data Is Encrypted In Transit
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.security_practices_data_is_encrypted_in_transit ? playStoreData.security_practices_data_is_encrypted_in_transit : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    You Can Request That Data Be Deleted
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.security_practices_you_can_request_that_data_be_deleted ? playStoreData.security_practices_you_can_request_that_data_be_deleted : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Developer
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Developer
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.developer ? playStoreData.developer : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                     Address
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.developer_address ? playStoreData.developer_address : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Email
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.developer_email ? playStoreData.developer_email : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    ID
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.developer_id ? playStoreData.developer_id : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Website
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.developer_website ? playStoreData.developer_website : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Permissions
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Permissions Other
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.permissions_Other ? playStoreData.permissions_Other : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Permissions Uncategorized
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.permissions_Uncategorized ? playStoreData.permissions_Uncategorized : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Permissions Wifi Connection Information
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.permissions_wifi_connection_information ? playStoreData.permissions_wifi_connection_information : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Playstore Details
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Playstore Url Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.playstoreUrlName ? playStoreData.playstoreUrlName : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Playstore Status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.playstore_status ? playStoreData.playstore_status : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                Review and Writting
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Reviews
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.reviews ? playStoreData.reviews : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Ratings
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.ratings ? playStoreData.ratings : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Comments
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.comments ? playStoreData.comments : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Content Review
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.content_rating ? playStoreData.content_rating : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Content Rating Description
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.content_rating_description ? playStoreData.content_rating_description : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="ms-4 border-0 pt-5">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                App Details
                </span>
              </h3>
            </div>
            <div className="separator separator-dashed my-3" />
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Genre
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.genre ? playStoreData.genre : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Genre Id
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.genre_id ? playStoreData.genre_id : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Price
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.price ? playStoreData.price : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Installs
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.installs ? playStoreData.installs : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Real Installs
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.real_installs ? playStoreData.real_installs : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Free
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.free ? playStoreData.free : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Currency
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.currency ? playStoreData.currency : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    Add Supported
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {playStoreData && playStoreData.ad_supported ? playStoreData.ad_supported : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </>
       )
        }
      </div>
    </>
  )
}

export default PlayStoreSummarry