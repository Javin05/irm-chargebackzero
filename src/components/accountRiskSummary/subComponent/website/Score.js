import _ from 'lodash'
import 'react-circular-progressbar/dist/styles.css'
import ReactSpeedometer from "react-d3-speedometer"
import { connect } from 'react-redux'
import { ExportListActions } from '../../../../store/actions'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { PMASTATUS } from '../../../../utils/constants'
import { PDFDownloadLink } from '@react-pdf/renderer';
import MyPdfComponent from '../pdf/PDFFile'

function Score(props) {
  const {
    websiteLink,
    getExportDispatch,
    exportLoading,
    websiteData,
    successVerifyDomain,
    domainRepetation,
    merchantIddetails,
    DashboardExportData,
    DashboardPmaLists,
  } = props
  const pmaData = DashboardPmaLists && DashboardPmaLists.data ? DashboardPmaLists.data : ''
  const pma = pmaData && pmaData.pma ? pmaData.pma : ''
  const pmaCheck = pmaData && pmaData.pmaCheck ? pmaData.pmaCheck : ''

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="simple"
          sheet="tablexls"
        />
      </div>
      <div className='row g-5 g-xl-8'>
        <div className='col-lg-12'>
          <div className="card card-xl-stretch">
            <div className="card-body pt-0">
              <div className='row mb-4'>
                <div className='col-lg-6'>
                  <span className='d-flex justify-content-center mt-4 fw-boldest my-1 fs-3'>
                    Website
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer'
                      onClick={() => window.open(websiteLink)}
                    >- {websiteLink}</a>
                  </span>
                </div>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-start mt-4 fw-boldest my-1 fs-3'>
                    Company Name
                    <a
                      className='text-hover-primary fs-4 ms-4 cursor-pointer text-capital'
                    >{DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : 'No Data'}</a>
                  </div>
                </div>
              </div>
              <div className="separator separator-dashed my-3" />
              <div className='row'>
                <div className='col-lg-6'>
                  <div className='d-flex justify-content-center'
                  >
                    <ReactSpeedometer
                      maxValue={100}
                      value={parseInt(DashboardExportData && DashboardExportData.riskScore)}
                      customSegmentStops={[0, 35, 75, 100]}
                      segmentColors={["limegreen", "gold", "tomato"]}
                      needleColor="red"
                      startColor="green"
                      segments={10}
                      endColor="blue"
                      className='pichart'
                      currentValueText="Over All Score: #{value}"
                      currentValuePlaceholderStyle={"#{value}"}
                    />
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div className='mt-2 d-flex'>
                    <PDFDownloadLink document={
                      <MyPdfComponent
                        DashboardExportData={DashboardExportData}
                        merchantIddetails={merchantIddetails}
                        DashboardPmaLists={DashboardPmaLists}
                        websiteData={websiteData}
                        successVerifyDomain={successVerifyDomain}
                        domainRepetation={domainRepetation}
                      />
                    } filename="FORM">
                      {({ loading }) => (loading ? <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2' >Loading Document...</button> : <button className='btn btn-outline btn-outline-dashed btn-outline-default w-300px me-2'> {!exportLoading &&
                        <span className='indicator-label'>
                          <img alt="" className="w-20px me-3" src="/media/svg/files/pdf.svg" />
                          PDF Report
                        </span>
                      }</button>)}
                    </PDFDownloadLink>
                  </div>
                </div>
              </div>
            </div>
            {
             merchantIddetails && merchantIddetails.data && merchantIddetails.data.clientId && merchantIddetails.data.clientId.pma === false ? (null) : (
                <div className='col-lg-12'>
                  <div className='fs-1 fw-boldest mt-4 d-flex justify-content-start ms-4'>
                    PMA
                  </div>
                  
                  {
                    pmaCheck && pmaCheck["PMA Flag"] === 'Yes' ? (
                      <>
                        <div className="separator separator-dashed my-3" />
                        <div class="table-responsive ms-4">
                          <table class="table"  style={{textAlign:"center"}}>
                            <thead>
                              <tr class="fw-bold fs-6 text-gray-800">
                                <th>Heavy Discounts</th>
                                <th>Login Credentials Are Required</th>
                                <th>Page Navigation Issue</th>
                                <th>Pricing is in Dollars</th>
                                <th>Pricing is not updated</th>
                                <th>Website Redirection</th>
                                <th>Website is not working</th>
                                <th>Online Booking Unavailable</th>
                                <th>Third party Utility Payment</th>
                                <th>License Category</th>
                                <th>Limited No of Products Listed</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Heavy Discounts']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Login Credentials are required']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Page Navigation Issue']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is in Dollars']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Pricing is not updated']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website Redirection']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Website is not working']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Online Booking Unavailable']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Third party Utility Payment']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['License Category']]} mt-4`} />
                                </td>
                                <td>
                                  <i className={`bi ${PMASTATUS[pma && pma['Limited No of Products Listed']]} mt-4`} />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </>
                    ) : (
                      
                      <div className='ml-5'>
                        <p>No violation detected</p>
                        <br/>
                      </div>
                    )
                  }
                  <div className="separator separator-dashed my-3" />
                </div>
              )
            }
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { exportlistStore } = state;
  return {
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : ''
  }
}

const mapDispatchToProps = (dispatch) => ({
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Score)