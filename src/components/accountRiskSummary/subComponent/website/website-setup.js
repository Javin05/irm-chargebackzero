import _ from 'lodash'
import moment from 'moment'
import { toAbsoluteUrl } from '../../../../theme/helpers'

function WebsiteSetup(props) {
  const {
    domainRepetation,
    DashboardExportData
  } = props

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Domain Info
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Registration Company
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Registered
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.domainRegistered ? DashboardExportData.domainRegistered : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Rank
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.domainRank ? DashboardExportData.domainRank : domainRepetation && domainRepetation.message && domainRepetation.message.messages ? domainRepetation.message.messages : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Registrant
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5 ellipsis me-2'>
                      Domain Registered Date
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        !_.isEmpty(DashboardExportData.domainRegistrationDate) ?
                          moment( DashboardExportData.domainRegistrationDate).format("DD-MM-YYYY")
                          : "No Data"
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5 ellipsis me-2'>
                      Domain Expiry Date
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                    {
                        !_.isEmpty(DashboardExportData.domainRegistrationExpiryDate) ?
                          moment( DashboardExportData.domainRegistrationExpiryDate).format("DD-MM-YYYY")
                          : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Name Servers
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Logo
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      <img
                        src={
                          DashboardExportData && DashboardExportData.logo
                            ? DashboardExportData.logo : 'No Data'
                        }
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        referrerpolicy="no-referrer"
                      />
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  SSL Check
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 ms-8'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5 ellipsis me-2'>
                      Ssl Certificate Check
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-gray-700'>
                      {
                        DashboardExportData && DashboardExportData.sslCertificateCheck ? DashboardExportData.sslCertificateCheck : "No Data"
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default WebsiteSetup