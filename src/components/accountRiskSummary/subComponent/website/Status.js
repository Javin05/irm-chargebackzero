import _ from 'lodash'

function Status(props) {

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8 ms-4' >
        <div className='col-lg-12'>
          <div className='row'>
            <div className='col-lg-12'>
              <div className="card-toolbar d-flex mt-5">
                <>
                  <ul className="nav">
                    <li className="nav-item">
                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-warning"
                        data-toggle='modal'
                        data-target='#approveModal'
                      >
                        Open
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-success"
                        data-toggle='modal'
                        data-target='#rejectModal'
                      >
                        Approve
                      </a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-light-danger"
                        data-toggle='modal'
                        data-target='#rejectModal'
                      >
                        Decline
                      </a>
                    </li>
                  </ul>
                </>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Status