import { Fragment } from 'react'
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import BusinessEmail from "./businessEmail";
import _ from "lodash";
import { CHANGE_ICON } from '../../../utils/constants'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'

function Email(props) {
  const { summary, dashboard, dashboardDetails } = props
  const IDEmail = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.email && dashboardDetails.dashboardData.email.data
  const IDEmailValue = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.email && dashboardDetails.dashboardData.email.value
  const BusinessEmailData = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.businessEmail && dashboardDetails.dashboardData.businessEmail.data
  const BusinessEmailValue = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.businessEmail && dashboardDetails.dashboardData.businessEmail.value

  let emailTotal =
    typeof dashboard !== "undefined" && "emailTotalScore" in dashboard
      ? dashboard.emailTotalScore
      : "--"; //Number
  let emailPositive =
    typeof dashboard !== "undefined" && "emailpositive" in dashboard
      ? dashboard.emailpositive
      : null; //Array
  let emailNegative =
    typeof dashboard !== "undefined" && "emailnegative" in dashboard
      ? dashboard.emailnegative
      : null; //Array

  const dashboardBusinesEmail = summary && summary.businessEmailFraudActivity && summary.businessEmailFraudActivity.check_email_fraud_activity && summary.businessEmailFraudActivity.check_email_fraud_activity.result && summary.businessEmailFraudActivity.check_email_fraud_activity.result.source_output
  const [IDEmail1, IDEmail2, IDEmail3] = [
    IDEmail && IDEmail.slice(0, Math.ceil(IDEmail && IDEmail.length / 3)),
    IDEmail && IDEmail.slice(Math.ceil(IDEmail && IDEmail.length / 3), Math.ceil((IDEmail && IDEmail.length / 3) * 2)),
    IDEmail && IDEmail.slice(Math.ceil((IDEmail && IDEmail.length / 3) * 2))
  ];

  return (
    <div>
      <div className="row mt-8">
        <div className="col-lg-12">
          <h1 className="d-flex justify-content-center mb-4">
            Individual Email
          </h1>
        </div>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={emailTotal}
            text={`${emailTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {emailTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
      </div>
      <div className="row mb-8 mt-8">
        <div className="col-lg-12">
          <div className="row mb-4">
            <div className="col-lg-6">
              <h1 className="mb-4">Positive Score Factors</h1>
              {!emailPositive ? (
                <div>
                  <div className="text-center">
                    <div
                      className="spinner-border text-primary m-5"
                      role="status"
                    />
                  </div>
                </div>
              ) : _.isEmpty(emailPositive) ? (
                <span className=" text-danger" role="status">
                  N/A
                </span>
              ) : (
                emailPositive &&
                emailPositive.map((item, i) => {
                  return (
                    <div className="mb-4" key={i}>
                      <div className="symbol symbol-45px me-4 mb-4">
                        <span className="symbol-label bg-success">
                          <span className="fs-5 text-white">
                            {item.riskscore}
                          </span>
                        </span>
                      </div>
                      <span className="fw-bold fs-5">{item.title}</span>
                    </div>
                  );
                })
              )}
            </div>
            <div className="col-lg-6">
              <h1 className="mb-4">Negative Score Factors</h1>
              {!emailNegative ? (
                <div>
                  <div className="text-center">
                    <div
                      className="spinner-border text-primary m-5"
                      role="status"
                    />
                  </div>
                </div>
              ) : _.isEmpty(emailNegative) ? (
                <span className=" text-danger" role="status">
                  N/A
                </span>
              ) : (
                emailNegative &&
                emailNegative.map((item, i) => {
                  return (
                    <div className="mb-4" key={i}>
                      <div className="symbol symbol-45px me-4 mb-4">
                        <span className="symbol-label bg-danger">
                          <span className="fs-5 text-white">
                            {item.riskscore}
                          </span>
                        </span>
                      </div>
                      <span className="fw-bold fs-5">{item.title}</span>
                    </div>
                  );
                })
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="mb-8">
          <a href="#" className="d-flex justify-content-center fs-2">
            CONFIDENCE MATRIX
          </a>
        </div>
      </div>
      <div className="row g-5 g-xl-8 mb-8">
        <div className="col-xl-12">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-header bg-col-bis border-0 ">
              <h3 className="d-flex pt-2">
                <span className="card-label fw-bolder text-dark">Email Dashboard</span>
                <span className="ms-2 text-muted">{IDEmailValue}</span>
              </h3>
            </div>
            <div className='row g-5'>
              <div className="col-xl-4">
                <div className="card-body pt-0">
                  {
                    !IDEmail1
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDEmail1 && IDEmail1.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                              <div className='flex-grow-1 me-2'>
                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                  {
                                    !_.isEmpty(item.info) ? (
                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                        className='tooltip'
                                      >
                                        {item.info}
                                      </Tooltip>}
                                        placement={"right"}
                                      >
                                        <span>
                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                        </span>
                                      </OverlayTrigger>
                                    ) : (
                                      null
                                    )
                                  }
                                </div>
                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                              </div>
                              <span className='py-1' title={item.message}>
                                <i
                                  className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                  title={item.message}
                                />
                              </span>
                            </div>
                          </Fragment>
                        )
                      })
                  }

                </div>
              </div>
              <div className="col-xl-4">
                <div className="card-body pt-0">
                  {
                    !IDEmail2
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDEmail2 && IDEmail2.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                              <div className='flex-grow-1 me-2'>
                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                  {
                                    !_.isEmpty(item.info) ? (
                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                        className='tooltip'
                                      >
                                        {item.info}
                                      </Tooltip>}
                                        placement={"right"}
                                      >
                                        <span>
                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                        </span>
                                      </OverlayTrigger>
                                    ) : (
                                      null
                                    )
                                  }
                                </div>
                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                              </div>
                              <span className='py-1' title={item.message}>
                                <i
                                  className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                  title={item.message}
                                />
                              </span>
                            </div>
                          </Fragment>
                        )
                      })
                  }
                </div>
              </div>
              <div className="col-xl-4">
                <div className="card-body pt-0">
                  {
                    !IDEmail3
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDEmail3 && IDEmail3.map((item, i) => {
                        return (
                          <Fragment key={"FIX_3" + i}>
                            <div className='d-flex align-items-center  rounded p-5 mb-0'>
                              <div className='flex-grow-1 me-2'>
                                <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                  {
                                    !_.isEmpty(item.info) ? (
                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                        className='tooltip'
                                      >
                                        {item.info}
                                      </Tooltip>}
                                        placement={"right"}
                                      >
                                        <span>
                                          <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                        </span>
                                      </OverlayTrigger>
                                    ) : (
                                      null
                                    )
                                  }
                                </div>
                                <span className='text-muted fw-bold d-block'>{item.value}</span>
                              </div>
                              <span className='py-1' title={item.message}>
                                <i
                                  className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                  title={item.message}
                                />
                              </span>
                            </div>
                          </Fragment>
                        )
                      })
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="col-xl-4">
          <div className="card card-xl-stretch mb-xl-8">
            <div className="card-header bg-col-bis border-0 ">
              <h3 className="card-title align-items-start flex-column ">
                <span className="card-label fw-bolder text-dark">
                  Personal Details
                </span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Email
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6">
                      {
                        dashboardIndidualEmail && dashboardIndidualEmail.advanced && dashboardIndidualEmail.advanced.sanitized_email
                          ? dashboardIndidualEmail.advanced.sanitized_email
                          : "--"
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Domain Age
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {
                        dashboardIndidualEmail && dashboardIndidualEmail.advanced && dashboardIndidualEmail.advanced.domain_age && dashboardIndidualEmail.advanced.domain_age.human
                          ? dashboardIndidualEmail.advanced.domain_age.human
                          : "--"
                      }
                    </span>
                  </div>
                </div>
              </div>

              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Suggested Domain
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualEmail && dashboardIndidualEmail.suggested_domain
                        ? dashboardIndidualEmail.suggested_domain
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>

              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      User Activity
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualEmail && dashboardIndidualEmail.user_activity
                        ? dashboardIndidualEmail.user_activity
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>

              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Spam Trap Score
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {
                        dashboardIndidualEmail && dashboardIndidualEmail.advanced && dashboardIndidualEmail.advanced.risk && dashboardIndidualEmail.advanced.risk.spam_trap_score
                          ? dashboardIndidualEmail.advanced.risk.spam_trap_score
                          : "0"
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Smtp Score
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {dashboardIndidualEmail && dashboardIndidualEmail.smtp_score
                        ? dashboardIndidualEmail.smtp_score
                        : "--"}
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center  rounded p-2 mb-0">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="text-gray-700 fw-bold fs-5 pl-3">
                      Blacklisted
                    </span>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <span className="ffw-bold text-bold fs-6  ">
                      {FINDBLACKLIST[summary && summary.blacklistedEmailCheck && summary.blacklistedEmailCheck.blacklisted_email_check && summary.blacklistedEmailCheck.blacklisted_email_check.email_number_status]}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> */}
      </div>
      <BusinessEmail summary={summary} dashboard={dashboard} BusinessEmailData={BusinessEmailData}
        dashboardBusinesEmail={dashboardBusinesEmail} BusinessEmailValue={BusinessEmailValue}
      />
    </div>
  );
}

export default Email;
