import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash";

function IpAddress(props) {
  const { summary, dashboard, dashboardDetails } = props;

  let ipAddressTotal =
    typeof dashboard !== "undefined" &&
      dashboard &&
      dashboard.data && dashboard.data.ipAddressTotalScore
      ? dashboard.data.ipAddressTotalScore
      : 0; //Number
  let ipAddressPositive =
    typeof dashboard !== "undefined" &&
      dashboard &&
      dashboard.data && dashboard.data.ipaddresspositive
      ? dashboard.data.ipaddresspositive
      : null; //Array
  let ipAddressNegative =
    typeof dashboard !== "undefined" &&
      dashboard &&
      dashboard.data && dashboard.data.ipaddressnegative
      ? dashboard.data.ipaddressnegative
      : null; //Array
  const Ipaddress = summary && summary.getIpLocation && summary.getIpLocation.get_ip_location && summary.getIpLocation.get_ip_location.message && summary.getIpLocation.get_ip_location.message.ip_location_data ? summary.getIpLocation.get_ip_location.message.ip_location_data : '--'

  return (
    <>
      <div className="container-fixed mt-8">
        <h1 className="d-flex justify-content-center mb-4">Ip Address</h1>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={ipAddressTotal}
            text={`${ipAddressTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {ipAddressTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <h1 className="mb-4">Positive Score Factors</h1>
            {_.isEmpty(ipAddressPositive) ? (
              <h1 className=" text-danger fs-4" role="status">
                N/A
              </h1>
            ) : (
              ipAddressPositive &&
              ipAddressPositive.map((item, i) => {
                return (
                  <div className="mb-4" key={i}>
                    <div className="symbol symbol-45px me-4 mb-4">
                      <span className="symbol-label bg-success">
                        <span className="fs-5 text-white">
                          {item.riskscore}
                        </span>
                      </span>
                    </div>
                    <span className="fw-bold fs-5">{item.title}</span>
                  </div>
                );
              })
            )}
          </div>
          <div className="col-lg-6">
            <h1 className="mb-4">Negative Score Factors</h1>
            {_.isEmpty(ipAddressNegative) ? (
              <span className=" text-danger" role="status">
                N/A
              </span>
            ) : (
              ipAddressNegative &&
              ipAddressNegative.map((item, i) => {
                return (
                  <div className="mb-4" key={i}>
                    <div className="symbol symbol-45px me-4 mb-4">
                      <span className="symbol-label bg-danger">
                        <span className="fs-5 text-white">
                          {item.riskscore}
                        </span>
                      </span>
                    </div>
                    <span className="fw-bold fs-5">{item.title}</span>
                  </div>
                );
              })
            )}
          </div>
        </div>
        <div className="row mt-8 mb-12">
          <div className="col-lg-4">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="d-flex pt-2">
                  <span className="card-label fw-bolder text-dark">
                    Ip Address
                  </span>
                  <span className="ms-2 text-muted">
                    {dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.ipAddress && dashboardDetails.data.dashboardData.ipAddress.value}
                  </span>
                </h3>
              </div>
              <div className="card-body pt-0">
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Country
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {Ipaddress && Ipaddress.country_name
                          ? Ipaddress.country_name
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        City
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {Ipaddress && Ipaddress.city_name? Ipaddress.city_name
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Country Code
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {Ipaddress && Ipaddress.country_code
                          ? Ipaddress.country_code
                          : "--"}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="align-items-center  rounded p-2 mb-0">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="text-gray-700 fw-bold fs-5 pl-3">
                        Isp
                      </span>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <span className="ffw-bold text-bold fs-6  ">
                        {
                          Ipaddress && Ipaddress.isp ? Ipaddress.isp
                            : "--"}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default IpAddress;