import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import 'react-circular-progressbar/dist/styles.css'
import { riskSummaryActions, matrixActions, matchAnalysisActions } from '../../../store/actions'
import _ from "lodash";
import MapGoogle from "../../maps/MapGoogle";
import StreetMap from "../../maps/StreetMap";
import { toAbsoluteUrl } from '../../../theme/helpers'

function CrossCheck(props) {
  const {
    merchantIddetails,
    matrixDetail,
    isLoaded,
    getMatchAnalysis,
    getMatchAnalysisDispatch,
    id
  } = props

  const viewData = matrixDetail && matrixDetail && matrixDetail.data ? matrixDetail.data : []
  const getData = viewData.filter(o => (o ? o : null))
  const splitData = getData && getData[0] ? getData[0] : '--'
  const individualAddress = splitData && splitData && splitData.address ? splitData.address : '--'
  const businessAddress = splitData && splitData && splitData.businessAddress ? splitData.businessAddress : '--'
  const merchant = merchantIddetails && merchantIddetails.data ? merchantIddetails.data : '--'

  let allMarkers = [];
  if (splitData && splitData.businessAddressLocation && splitData.businessPhoneLocation.lat && splitData.businessPhoneLocation.long) {
    allMarkers.push({
      lat: splitData.businessAddressLocation.lat,
      lng: splitData.businessAddressLocation.long,
      area: "BUSINESS ADDRESS"
    })
  } else {
    allMarkers.push({
      lat: splitData && splitData.businessPhoneLocation && splitData.businessPhoneLocation.lat,
      lng: splitData && splitData.businessPhoneLocation && splitData.businessPhoneLocation.long,
      area: "BUSINESS ADDRESS"
    })
  }
  if (splitData && splitData.individualAddressLocation && splitData.individualAddressLocation.lat && splitData.individualAddressLocation.long) {
    allMarkers.push({
      lat: splitData.individualAddressLocation.lat,
      lng: splitData.individualAddressLocation.long,
      area: "INDIVIDUALADDRESS"
    })
  } else {
    allMarkers.push({
      lat: splitData && splitData.phoneLocation && splitData.phoneLocation.lat,
      lng: splitData && splitData.phoneLocation && splitData.phoneLocation.long,
      area: "INDIVIDUALADDRESS"
    })
  }
  if (splitData && splitData.ipLocation && splitData.ipLocation.lat) {
    allMarkers.push({
      lat: splitData.ipLocation.lat,
      lng: splitData.ipLocation.long,
      area: "IP"
    })
  }
  if (splitData && splitData.businessAddressLocation && splitData.businessPhoneLocation.lat && splitData.businessPhoneLocation.long) {
    allMarkers.push({
      lat: splitData.businessAddressLocation.lat,
      lng: splitData.businessAddressLocation.long,
      area: "BUSINESS ADDRESS"
    })
  } else {
    allMarkers.push({
      lat: splitData && splitData.businessPhoneLocation && splitData.businessPhoneLocation.lat,
      lng: splitData && splitData.businessPhoneLocation && splitData.businessPhoneLocation.long,
      area: "BUSINESS ADDRESS"
    })
  }

  useEffect(() => {
    getMatchAnalysisDispatch(id)
  }, [])

  return (
    <>
      <div className='container-fixed'>
        <h1 className='d-flex justify-content-center mb-4'>Match Analysis</h1>
        <div className='row mt-8 mb-4'>
          <div className='col-lg-6 col-md-6 col-sm-6'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Individual
                  </span>
                </h3>
              </div>
              <div className='row mt-4 px-9'>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualName ? getMatchAnalysis.data.individualName : '--'}
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Phone Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualPhoneName ? getMatchAnalysis.data.individualPhoneName : "--"}
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Address Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualAddressName ? getMatchAnalysis.data.individualAddressName: '--'}
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Email Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualEmailName ? getMatchAnalysis.data.individualEmailName : '--'}
                  </span>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <div className='card-label fw-bolder text-dark'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualPhoneNameMatch && getMatchAnalysis.data.individualPhoneNameMatch.title}
                  </div>
                  <div className='card-label fw-bolder text-muted'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualPhoneNameMatch && getMatchAnalysis.data.individualPhoneNameMatch.value}
                  </div>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <div className='card-label fw-bolder text-dark'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualAddressNameMatch && getMatchAnalysis.data.individualAddressNameMatch.title}
                  </div>
                  <div className='card-label fw-bolder text-muted'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualAddressNameMatch && getMatchAnalysis.data.individualAddressNameMatch.value}
                  </div>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12 my-2'>
                  <div className='card-label fw-bolder text-dark'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualEmailNameMatch && getMatchAnalysis.data.individualEmailNameMatch.title}
                  </div>
                  <div className='card-label fw-bolder text-muted'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.individualEmailNameMatch && getMatchAnalysis.data.individualEmailNameMatch.value}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-lg-6 col-md-6 col-sm-6'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Business
                  </span>
                </h3>
              </div>
              <div className='row mt-4 px-9'>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessName ? getMatchAnalysis.data.businessName: '--'}
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Phone Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessPhoneName ? getMatchAnalysis.data.businessPhoneName : '--'}
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Address Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessAddressName ? getMatchAnalysis.data.businessAddressName : '--'}
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-dark '>
                    Email Name:
                  </span>
                </div>
                <div className='col-lg-6'>
                  <span className='card-label fw-bolder text-muted '>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessEmailName ? getMatchAnalysis.data.businessEmailName : '--'}
                  </span>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <div className='card-label fw-bolder text-dark'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessEmailNameMatch && getMatchAnalysis.data.businessEmailNameMatch.title}
                  </div>
                  <div className='card-label fw-bolder text-muted'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessEmailNameMatch && getMatchAnalysis.data.businessEmailNameMatch.value}
                  </div>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <div className='card-label fw-bolder text-dark'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessPhoneNameMatch && getMatchAnalysis.data.businessPhoneNameMatch.title}
                  </div>
                  <div className='card-label fw-bolder text-muted'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessPhoneNameMatch && getMatchAnalysis.data.businessPhoneNameMatch.value}
                  </div>
                </div>
                <div className='col-lg-12 col-md-12 col-sm-12 my-2'>
                  <div className='card-label fw-bolder text-dark'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessAddressNameMatch && getMatchAnalysis.data.businessAddressNameMatch.title}
                  </div>
                  <div className='card-label fw-bolder text-muted'>
                    {getMatchAnalysis && getMatchAnalysis.data && getMatchAnalysis.data.businessAddressNameMatch && getMatchAnalysis.data.businessAddressNameMatch.value}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <h1 className='d-flex justify-content-center mb-4'>Triangulation Metrics</h1>
        <div className='row mt-8 mb-12'>
          <div className='col-lg-6 col-md-6 col-sm-6'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Individual Address
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                  <span className='text-gray-700 fw-bold fs-5 pl-3'>
                    Customer Name
                  </span>
                  <span className='ffw-bold text-bold fs-6 ml-2'>
                    {merchant && merchant.firstName ? merchant.firstName : '--'} {merchant && merchant.lastName ? merchant.lastName : '--'}
                  </span>
                </div>
                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                  <span className='text-gray-700 fw-bold fs-5 pl-3'>
                    Address
                  </span>
                  <span className='ffw-bold text-bold fs-6 ml-2'>
                    {individualAddress && individualAddress ? individualAddress : '--'}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className='col-lg-6 col-md-6 col-sm-6'>
            <div className='card card-xl-stretch mb-xl-8'>
              <div className='card-header bg-col-bis border-0 '>
                <h3 className='card-title align-items-start flex-column '>
                  <span className='card-label fw-bolder text-dark'>
                    Business Address
                  </span>
                </h3>
              </div>
              <div className='card-body pt-0'>
                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                  <span className='text-gray-700 fw-bold fs-5 pl-3'>
                    Business Name
                  </span>
                  <span className='ffw-bold text-bold fs-6 ml-2'>
                    {merchant && merchant.businessName ? merchant.businessName : '--'}
                  </span>
                </div>
                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                  <span className='text-gray-700 fw-bold fs-5 pl-3'>
                    Address
                  </span>
                  <span className='ffw-bold text-bold fs-6 ml-2'>
                    {businessAddress && businessAddress ? businessAddress : '--'}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='row mt-8'>
          <div className='col-lg-6'>
            <div className='text mb-4 fs-4 fw-bolder'>INDIVIDUAL ADDRESS </div>
            {
              splitData && splitData.individualAddressLocation && _.isNumber(splitData.individualAddressLocation.lat) && _.isNumber(splitData.individualAddressLocation.long) ? (
                <div className='h-450px'>
                  {isLoaded ? <StreetMap mapData={splitData} type={"INDIVIDUAL"} /> : null}
                </div>
              ) : (
                <img
                  src={toAbsoluteUrl('/media/imageIcons/locationNotFound.png')}
                  className='map-img'
                />
              )
            }
          </div>
          <div className='col-lg-6'>
            <div className='text mb-4 fs-4 fw-bolder'>BUSINESS ADDRESS </div>
            {
              splitData && splitData.businessAddressLocation && _.isNumber(splitData.businessAddressLocation.long) && _.isNumber(splitData.businessAddressLocation.lat) ? (
                <div className='h-450px'>
                  {isLoaded ? <StreetMap mapData={splitData} type={"BUSINESS"} /> : null}
                </div>
              ) : (
                <img
                  src={toAbsoluteUrl('/media/imageIcons/locationNotFound.png')}
                  className='map-img'
                />
              )
            }
          </div>
          <div className='border-0 pt-8'>
            <h3 className='align-items-start flex-column '>
              <span className='fw-bolder text-dark'>
                Locations & Distances
              </span>
            </h3>
          </div>
          <div className='col-lg-6 mb-4'>
            {
              splitData && splitData.businessAddressLocation && _.isNumber(splitData.businessAddressLocation.long) && _.isNumber(splitData.businessAddressLocation.lat) || splitData && splitData.individualAddressLocation && _.isNumber(splitData.individualAddressLocation.lat) && _.isNumber(splitData.individualAddressLocation.long) ? (
                <div className='h-450px'>
                  {isLoaded ? <MapGoogle mapData={splitData} mapMarkers={allMarkers} /> : null}
                </div>
              ) : (
                <img
                  src={toAbsoluteUrl('/media/imageIcons/locationNotFound.png')}
                  className='map-img'
                />
              )
            }
            {/* </div> */}
          </div>
          <div className='col-lg-6'>
            <div className='card card-xl-stretch mb-xl-8 mb-4' >
              <div className='card-body pt-0'>
                <div className='row p-5'>
                  <h3 className='card-title align-items-start flex-column mb-0'>
                    <span className='card-label fw-bolder text-dark'>
                      Measuring from individual address :
                    </span>
                  </h3>
                  <span className='card-label fw-bolder text-muted py-2'>
                    {individualAddress && individualAddress ? individualAddress : '--'}
                  </span>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <h4 className='text-muted'>Individual</h4>
                    <div className='row mt-4'>
                      <div className='col-lg-1'>
                        <span className='svg-icon svg-icon-success me-2'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-telephone-fill' viewBox='0 0 16 16'>
                            <path fillRule='evenodd' d='M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-11 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Phone
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.individualAddressPhoneDistance ? splitData.individualAddressPhoneDistance : '--'}
                        </h5>
                      </div>
                      <div className='col-lg-1'>
                        <span className='me-2 svg-icon svg-icon-2 svg-icon-info'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                            <path opacity='0.3' d='M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z' fill='black' />
                            <path d='M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z' fill='black' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-10 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Ip Address
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.individualAddressIpAddressDistance ? splitData.individualAddressIpAddressDistance : '--'}
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <h4 className='ml-2 text-muted'>Business</h4>
                    <div className='row mt-4'>
                      <div className='col-lg-1 ml-2'>
                        <span className='svg-icon svg-icon-success me-2'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-telephone-fill' viewBox='0 0 16 16'>
                            <path fillRule='evenodd' d='M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-10 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Phone
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.individualAddressBusinessPhoneDistance ? splitData.individualAddressBusinessPhoneDistance : '--'}
                        </h5>
                      </div>
                      <div className='col-lg-1 ml-2'>
                        <span className='me-2 svg-icon svg-icon-2 svg-icon-info'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                            <path opacity='0.3' d='M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z' fill='black' />
                            <path d='M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z' fill='black' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-10 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Address
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.individualAddressBusinessAddressDistance ? splitData.individualAddressBusinessAddressDistance : '--'}
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='row px-5'>
                  <h3 className='card-title align-items-start flex-column mb-0'>
                    <span className='card-label fw-bolder text-dark'>
                      Measuring from business address :
                    </span>
                  </h3>
                  <span className='card-label fw-bolder text-muted py-2'>
                    {businessAddress && businessAddress ? businessAddress : '--'}
                  </span>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <h4 className='text-muted'>Individual</h4>
                    <div className='row mt-4'>
                      <div className='col-lg-1'>
                        <span className='svg-icon svg-icon-success me-2'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-telephone-fill' viewBox='0 0 16 16'>
                            <path fillRule='evenodd' d='M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-11 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Phone
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.individualPhoneBusinessAddressDistance ? splitData.individualPhoneBusinessAddressDistance : '--'}
                        </h5>
                      </div>
                      <div className='col-lg-1'>
                        <span className='me-2 svg-icon svg-icon-2 svg-icon-info'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                            <path opacity='0.3' d='M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z' fill='black' />
                            <path d='M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z' fill='black' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-10 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Ip Address
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.individualAddressBusinessAddressDistance ? splitData.individualAddressBusinessAddressDistance : '--'}
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <h4 className='ml-2 text-muted'>Business</h4>
                    <div className='row mt-4'>
                      <div className='col-lg-1 ml-2'>
                        <span className='svg-icon svg-icon-success me-2'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' className='bi bi-telephone-fill' viewBox='0 0 16 16'>
                            <path fillRule='evenodd' d='M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-10 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Phone
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.businessAddressBusinessPhoneDistance ? splitData.businessAddressBusinessPhoneDistance : '--'}
                        </h5>
                      </div>
                      <div className='col-lg-1 ml-2'>
                        <span className='me-2 svg-icon svg-icon-2 svg-icon-info'>
                          <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
                            <path opacity='0.3' d='M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z' fill='black' />
                            <path d='M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z' fill='black' />
                          </svg>
                        </span>
                      </div>
                      <div className='col-lg-10 mb-4'>
                        <span className='ml-2 card-label fw-bolder text-dark '>
                          Address
                        </span>
                        <h5 className='ml-2 card-label fw-bolder text-dark fs-7 text-muted'>
                          {splitData && splitData.businessAddressIPDistance ? splitData.businessAddressIPDistance : '--'}
                        </h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => {
  const { accountmerchantIdDetailsStore, accountmatrixStore, matchAnalysisStore } = state

  return {
    getRiskSummarys: state && state.riskSummaryStore && state.riskSummaryStore.getRiskSummarys,
    loading: state && state.riskSummaryStore && state.riskSummaryStore.loading,
    getMatchAnalysis: matchAnalysisStore && matchAnalysisStore.getMatchAnalysis,
    getMatchAnalysisLoading: matchAnalysisStore && matchAnalysisStore.loading,
    merchantIddetails: accountmerchantIdDetailsStore && accountmerchantIdDetailsStore.aaccountmerchantIdDetails ? accountmerchantIdDetailsStore.aaccountmerchantIdDetails : {},
    matrixDetail: accountmatrixStore && accountmatrixStore.accountmatrixData ? accountmatrixStore.accountmatrixData : {}
  }
}
const mapDispatchToProps = (dispatch) => ({
  getRiskSummaryDispatch: (id) => dispatch(riskSummaryActions.getRiskSummary(id)),
  getMatchAnalysisDispatch: (id) => dispatch(matchAnalysisActions.getMatchAnalysis(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CrossCheck)