import React, { Fragment } from 'react'
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import _ from "lodash";
import BusinessAddress from "./businessAddress";
import MapGoogle from "../../maps/MapGoogle";
import StreetMap from "../../maps/StreetMap";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'
import { toAbsoluteUrl } from '../../../theme/helpers'
import { CHANGE_ICON } from '../../../utils/constants';

function Address(props) {
  const { summary, dashboard, isLoaded, matrixDetail, dashboardDetails } = props;

  let addressTotal =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "addressTotalScore" in dashboard
      ? dashboard.addressTotalScore
      : 0; //Number
  let addressPositive =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "addresspositive" in dashboard
      ? dashboard.addresspositive
      : null; //Array
  let addressNegative =
    typeof dashboard !== "undefined" &&
      dashboard &&
      "addressnegative" in dashboard
      ? dashboard.addressnegative
      : null; //Array

  const IDAddress = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.address && dashboardDetails.dashboardData.address.data
  const IDAddressValue = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.address && dashboardDetails.dashboardData.address.value
  const BusinessAddressData = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.businessAddress && dashboardDetails.dashboardData.businessAddress.data
  const BusinessAddressValue = dashboardDetails && dashboardDetails.dashboardData && dashboardDetails.dashboardData.businessAddress && dashboardDetails.dashboardData.businessAddress.value

  let allMarkers = [];
  if (matrixDetail && matrixDetail.individualAddressLocation) {
    allMarkers.push({
      lat: matrixDetail.individualAddressLocation.lat,
      lng: matrixDetail.individualAddressLocation.long,
      area: "INDIVIDUALADDRESS"
    })
  }
  const [IDAddress1, IDAddress2, IDAddress3] = [
    IDAddress&&IDAddress.slice(0, Math.ceil(IDAddress&&IDAddress.length / 3)),
    IDAddress&&IDAddress.slice(Math.ceil(IDAddress&&IDAddress.length / 3), Math.ceil((IDAddress&&IDAddress.length / 3) * 2)),
    IDAddress&&IDAddress.slice(Math.ceil((IDAddress&&IDAddress.length / 3) * 2))
  ];

  return (
    <>
      <div className="container-fixed">
        <h1 className="d-flex justify-content-center mb-4 mt-8">
          Individual Address
        </h1>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={addressTotal}
            text={`${addressTotal}`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {addressTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8">
          <div className="col-lg-12">
            <div className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {!addressPositive ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(addressPositive) ? (
                    <span className=" text-danger " role="status">
                      N/A
                    </span>
                  ) : (
                    addressPositive &&
                    addressPositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {!addressNegative ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(addressNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    addressNegative &&
                    addressNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8 mb-12">
          <div className="col-xl-12">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="d-flex pt-2">
                  <span className="card-label fw-bolder text-dark">
                    Address
                  </span>
                  <span className="ms-2 text-muted">
                    {IDAddressValue}
                  </span>
                </h3>
              </div>
              <div className='row'>
                <div className='col-xl-4'>
                  <div className="card-body pt-0">
                    {
                      !IDAddress1
                        ? (
                          <div>
                            <div className='text-center'>
                              <div className='spinner-border text-primary m-5' role='status' />
                            </div>
                          </div>
                        )
                        : IDAddress1 && IDAddress1.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                          {
                                            !_.isEmpty(item.info) ? (
                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                className='tooltip'
                                              >
                                                {item.info}
                                              </Tooltip>}
                                                placement={"right"}
                                              >
                                                <span>
                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                </span>
                                              </OverlayTrigger>
                                            ) : (
                                              null
                                            )
                                          }
                                        </div>
                                        <span className='text-muted fw-bold d-block'>{item && item.value ? item.value : '--'}</span>
                                      </div>
                                      <span className='py-1' title={item.message}>
                                  <i
                                    className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                    title={item.message}
                                  />
                                </span>
                                    </div>
                            </Fragment>
                          )
                        })
                    }
                  </div>
                </div>
                <div className='col-xl-4'>
                  <div className="card-body pt-0">
                    {
                      !IDAddress2
                        ? (
                          <div>
                            <div className='text-center'>
                              <div className='spinner-border text-primary m-5' role='status' />
                            </div>
                          </div>
                        )
                        : IDAddress2 && IDAddress2.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                          {
                                            !_.isEmpty(item.info) ? (
                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                className='tooltip'
                                              >
                                                {item.info}
                                              </Tooltip>}
                                                placement={"right"}
                                              >
                                                <span>
                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                </span>
                                              </OverlayTrigger>
                                            ) : (
                                              null
                                            )
                                          }
                                        </div>
                                        <span className='text-muted fw-bold d-block'>{item && item.value ? item.value : '--'}</span>
                                      </div>
                                      <span className='py-1' title={item.message}>
                                  <i
                                    className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                    title={item.message}
                                  />
                                </span>
                                    </div>                                 
                            </Fragment>
                          )
                        })
                    }
                  </div>
                </div>
                <div className='col-xl-4'>
                  <div className="card-body pt-0">
                    {
                      !IDAddress3
                        ? (
                          <div>
                            <div className='text-center'>
                              <div className='spinner-border text-primary m-5' role='status' />
                            </div>
                          </div>
                        )
                        : IDAddress3 && IDAddress3.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                      <div className='flex-grow-1 me-2'>
                                        <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                          {
                                            !_.isEmpty(item.info) ? (
                                              <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                                className='tooltip'
                                              >
                                                {item.info}
                                              </Tooltip>}
                                                placement={"right"}
                                              >
                                                <span>
                                                  <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                                </span>
                                              </OverlayTrigger>
                                            ) : (
                                              null
                                            )
                                          }
                                        </div>
                                        <span className='text-muted fw-bold d-block'>{item && item.value ? item.value : '--'}</span>
                                      </div>
                                      <span className='py-1' title={item.message}>
                                  <i
                                    className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                    title={item.message}
                                  />
                                </span>
                                    </div>                                 
                            </Fragment>
                          )
                        })
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-8">
          <div className="col-lg-6">
            <div className="card w-744px h-450px">
              {
                matrixDetail && matrixDetail.individualAddressLocation && _.isNumber(matrixDetail.individualAddressLocation.lat) && _.isNumber(matrixDetail.individualAddressLocation.long) ? (
                  <div className='h-450px'>
                    {isLoaded ? <MapGoogle mapData={matrixDetail} mapMarkers={allMarkers} /> : null}
                  </div>
                ) : (
                  <img
                    src={toAbsoluteUrl('/media/imageIcons/locationNotFound.png')}
                    className='map-img'
                  />
                )
              }
            </div>
          </div>
          <div className="col-lg-6">
            <div
              style={{
                height: "450px",
                backgroundColor: "#eeeeee",
              }}
            >
              {
                matrixDetail && matrixDetail.individualAddressLocation && _.isNumber(matrixDetail.individualAddressLocation.lat) && _.isNumber(matrixDetail.individualAddressLocation.long) ? (
                  <div className='h-450px'>
                    {isLoaded ? <StreetMap mapData={matrixDetail} type={"INDIVIDUAL"} /> : null}
                  </div>
                ) : (
                  <img
                    src={toAbsoluteUrl('/media/imageIcons/locationNotFound.png')}
                    className='map-img'
                  />
                )
              }
            </div>
          </div>
        </div>
      </div>
      <div className="mt-10">
        <BusinessAddress
          summary={summary}
          dashboard={dashboard}
          splitData={matrixDetail}
          isLoaded={isLoaded}
          BusinessAddressData={BusinessAddressData}
          BusinessAddressValue={BusinessAddressValue}
        />
      </div>
    </>
  );
}

export default Address;
