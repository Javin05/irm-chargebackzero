import { Fragment, useRef } from 'react'
import { connect } from "react-redux";
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { CHANGE_ICON, DATE } from "../../../utils/constants";
import BusinessPhone from "./businessPhone";
import _ from "lodash";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip'

function Phone(props) {
  const {
    dashboardDetails,
    summary
  } = props
  const speedometetervalue =
    dashboardDetails && dashboardDetails.data && dashboardDetails.data
      ? dashboardDetails.data
      : "--";
  const phoneTotal =
    speedometetervalue && speedometetervalue.phoneTotalScore
      ? speedometetervalue.phoneTotalScore
      : "0";
  const phonePositive =
    dashboardDetails &&
      dashboardDetails.data &&
      dashboardDetails.data.phonepositive
      ? dashboardDetails.data.phonepositive
      : "--";
  const phoneNegative =
    dashboardDetails &&
      dashboardDetails?.data &&
      dashboardDetails.data.phonenegative
      ? dashboardDetails.data.phonenegative
      : "--";
  const dashboardBusinesPhone = summary && summary.businessVerifyPhoneV1 && summary.businessVerifyPhoneV1.verify_phone_v1
  const IDPhone = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.phone && dashboardDetails.data.dashboardData.phone.data
  const BusinessPhoneData = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessPhone && dashboardDetails.data.dashboardData.businessPhone.data
  const BusinessPhoneDataValue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessPhone && dashboardDetails.data.dashboardData.businessPhone.value
  const IDPhoneSize = Math.ceil(IDPhone && IDPhone.length / 3);
  const IDPhone1 = IDPhone && IDPhone.slice(0, IDPhoneSize);
  const IDPhone2 = IDPhone && IDPhone.slice(IDPhoneSize, IDPhoneSize * 2);
  const IDPhone3 = IDPhone && IDPhone.slice(IDPhoneSize * 2);

  return (
    <div>
      <div>
        <div className="row mt-8">
          <div className="col-lg-4" />
          <div className="col-lg-4">
            <h1 className="d-flex justify-content-center mb-4">
              Individual Phone
            </h1>
          </div>
        </div>
        <div className="d-flex justify-content-center mb-4">
          <CircularProgressbarWithChildren
            value={phoneTotal}
            text={`${phoneTotal}%`}
            strokeWidth={10}
            circleRatio={1}
            styles={buildStyles({
              rotation: 0.5,
              strokeLinecap: "butt",
              textColor: "mediumseagreen",
              pathColor: "#ed5555",
              trailColor: "mediumseagreen",
            })}
          >
            <div style={{ fontSize: 12, marginTop: 55 }}>
              <strong>Risk Score {phoneTotal}</strong>
            </div>
          </CircularProgressbarWithChildren>
        </div>
        <div className="row mb-8 mt-8">
          <div className="col-lg-12">
            <h5 className="mb-4">
              <div className="row">
                <div className="col-lg-6">
                  <h1 className="mb-4">Positive Score Factors</h1>
                  {!phonePositive ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(phonePositive) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    phonePositive &&
                    typeof phoneNegative === "object" &&
                    phonePositive.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-success">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
                <div className="col-lg-6">
                  <h1 className="mb-4">Negative Score Factors</h1>
                  {!phoneNegative ? (
                    <tr>
                      <td colSpan="6" className="text-center">
                        <div
                          className="spinner-border text-primary m-5"
                          role="status"
                        />
                      </td>
                    </tr>
                  ) : _.isEmpty(phoneNegative) ? (
                    <span className=" text-danger" role="status">
                      N/A
                    </span>
                  ) : (
                    phoneNegative &&
                    typeof phoneNegative === "object" &&
                    phoneNegative.map((item, i) => {
                      return (
                        <div className="mb-4" key={i}>
                          <div className="symbol symbol-45px me-4 mb-4">
                            <span className="symbol-label bg-danger">
                              <span className="fs-5 text-white">
                                {item.riskscore}
                              </span>
                            </span>
                          </div>
                          <span className="fw-bold fs-5">{item.title}</span>
                        </div>
                      );
                    })
                  )}
                </div>
              </div>
            </h5>
          </div>
        </div>
        <div className="mb-4" style={{ backgroundColor: "orangered" }}>
        </div>
        <div className="row">
          <div className="mb-8">
            <a href="#" className="d-flex justify-content-center fs-2">
              CONFIDENCE MATRIX
            </a>
          </div>
        </div>
        <div className="row g-5 g-xl-8 mb-8">
          <div className="col-xl-12">
            <div className="card card-xl-stretch mb-xl-8">
              <div className="card-header bg-col-bis border-0 ">
                <h3 className="d-flex pt-3">
                  <span className="card-label fw-bolder text-dark">
                    Phone
                  </span>
                  <span className="ms-2 text-muted">
                    {dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.phone && dashboardDetails.data.dashboardData.phone.value}
                  </span>
                </h3>
              </div>
              <div className='row g-5'>
                <div className="col-xl-4">
                  <div className="card-body pt-0">
                    {
                      !IDPhone1
                        ? (
                          <div>
                            <div className='text-center'>
                              <div className='spinner-border text-primary m-5' role='status' />
                            </div>
                          </div>
                        )
                        : IDPhone1 && IDPhone1.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                              <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                <div className='flex-grow-1 me-2'>
                                  <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                    {
                                      !_.isEmpty(item.info) ? (
                                        <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                          className='tooltip'
                                        >
                                          {item.info}
                                        </Tooltip>}
                                          placement={"right"}
                                        >
                                          <span>
                                            <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                          </span>
                                        </OverlayTrigger>
                                      ) : (
                                        null
                                      )
                                    }
                                  </div>
                                  <span className='text-muted fw-bold d-block'>{item.value}</span>
                                </div>
                                <span className='py-1' title={item.message}>
                                  <i
                                    className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                    title={item.message}
                                  />
                                </span>
                              </div>
                            </Fragment>
                          )
                        })
                    }
                  </div>
                </div>
                <div className="col-xl-4">
                  <div className="card-body pt-0">
                    {
                      !IDPhone2
                        ? (
                          <div>
                            <div className='text-center'>
                              <div className='spinner-border text-primary m-5' role='status' />
                            </div>
                          </div>
                        )
                        : IDPhone2 && IDPhone2.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                              <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                <div className='flex-grow-1 me-2'>
                                  <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                    {
                                      !_.isEmpty(item.info) ? (
                                        <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                          className='tooltip'
                                        >
                                          {item.info}
                                        </Tooltip>}
                                          placement={"right"}
                                        >
                                          <span>
                                            <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                          </span>
                                        </OverlayTrigger>
                                      ) : (
                                        null
                                      )
                                    }
                                  </div>
                                  <span className='text-muted fw-bold d-block'>{item.value}</span>
                                </div>
                                <span className='py-1' title={item.message}>
                                  <i
                                    className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                    title={item.message}
                                  />
                                </span>
                              </div>
                            </Fragment>
                          )
                        })
                    }

                  </div>
                </div>
                <div className="col-xl-4">
                  <div className="card-body pt-0">
                    {
                      !IDPhone3
                        ? (
                          <div>
                            <div className='text-center'>
                              <div className='spinner-border text-primary m-5' role='status' />
                            </div>
                          </div>
                        )
                        : IDPhone3 && IDPhone3.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                              <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                <div className='flex-grow-1 me-2'>
                                  <div className='fw-bolder text-gray-800  fs-6'>{item.title}
                                    {
                                      !_.isEmpty(item.info) ? (
                                        <OverlayTrigger overlay={<Tooltip id="tooltip-disabled"
                                          className='tooltip'
                                        >
                                          {item.info}
                                        </Tooltip>}
                                          placement={"right"}
                                        >
                                          <span>
                                            <i class="bi bi-info-circle-fill text-dark text-hover-warning ms-2 fs-5" />
                                          </span>
                                        </OverlayTrigger>
                                      ) : (
                                        null
                                      )
                                    }
                                  </div>
                                  <span className='text-muted fw-bold d-block'>{item.value}</span>
                                </div>
                                <span className='py-1' title={item.message}>
                                  <i
                                    className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                    title={item.message}
                                  />
                                </span>
                              </div>
                            </Fragment>
                          )
                        })
                    }

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <BusinessPhone dashboardBusinesPhone={dashboardBusinesPhone} BusinessPhoneData={BusinessPhoneData} summary={summary} BusinessPhoneDataValue={BusinessPhoneDataValue} />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  const { accountdashboardGetDetailsStore } = state;
  return {
    dashboardDetails:
      accountdashboardGetDetailsStore && accountdashboardGetDetailsStore.aaccountdashboardGetDetails
        ? accountdashboardGetDetailsStore.aaccountdashboardGetDetails
        : {},
  };
};
const mapDispatchToProps = (dispatch) => ({
  // getRiskSummaryDispatch: (id) =>  dispatch(riskSummaryActions.getRiskSummary(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Phone);
  