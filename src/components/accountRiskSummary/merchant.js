import React, { useState, useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { useLocation } from 'react-router-dom'
import { STATUS_RESPONSE, CHANGE_ICON } from '../../utils/constants'
import {
  dashboardDetailsActions,
  riskSummaryActions,
  GetCategoryActions,
  accountmerchantIdDetailsActions
} from '../../store/actions'
import { KTSVG } from '../../theme/helpers'
import { riskManagementActions } from '../../store/actions'
import { AccountsQueueApproveActions } from '../../store/actions'
import { AccountRiskCommentdeleteActions, AccountRiskCommentActions } from '../../store/actions'
import CrossCheck from './subComponent/crossCheck'
import { USER_ERROR, SWEET_ALERT_MSG, BLACKLIST, SECRETKYEY } from '../../utils/constants'
import clsx from 'clsx'
import "react-circular-progressbar/dist/styles.css";
import ReactSpeedometer from "react-d3-speedometer"
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import moment from 'moment'
import AccountRiskCommentEdit from "./accountRiskCommentEdit";
import DeviceFingerprint from "node_js_ipqs_device_tracker";

function Merchant(props) {
  const {
    dashboardDetails,
    id,
    setOpenPhone,
    setOpenEmail,
    setOpenAddress,
    setOpenIpAddress,
    getRiskManagementlistDispatch,
    ApprovePost,
    approveResponceData,
    loading,
    openMap,
    openBusinessMap,
    setopenBusinessMap,
    setopenMap,
    getIdMerchantDispatch,
    clearApprove,
    matrixDetail,
    merchantSummary,
    isLoaded,
    GetCategroyDispatch,
    summary,
    AccountRisklists,
    DeleteAccountRiskData,
    getAccountRiskCommentDispatch,
    deleteAccountRiskCommentDispatch,
    clearDeleteAccountRiskCommentDispatch
  } = props

  const [approveActive, setApproveActive] = useState(false)
  const [rejectActive, setRejectActive] = useState(false)
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('risk-summary/update/')
  const currentId = url && url[1]
  const [rejectValue, setRejectValue] = useState()
  const [deviceFingerPrintData, setdeviceFingerPrintData] = useState()

  const [errors, setErrors] = useState({
    reason: '',
  })
  const [formData, setFormData] = useState({
    message: ''
  })
  const [holdFormData, setHoldFormData] = useState({
    riskStatus: 'HOLD',
    reason: ''
  })
  const [rejectFormData, setRejectFormData] = useState({
    riskStatus: 'REJECTED',
    reason: '',
    rejectType: '',
    rejectMoreValue: ''

  })

  const [approveFormData, setApproveFormData] = useState({
    riskStatus: 'APPROVED',
    reason: '',
  })

  const [editMode, setEditMode] = useState(false)
  const [userId, setUserId] = useState('')

  useEffect(() => {
    GetCategroyDispatch(currentId)
  }, [currentId])

  useEffect(() => {
    if (openMap) {
      const modalBtn = document.getElementById('modal-btn')
      modalBtn.click()
    }
  }, [openMap])

  useEffect(() => {
    if (openBusinessMap) {
      const modalBtn = document.getElementById('businesmodal-btn')
      modalBtn.click()
    }
  }, [openBusinessMap])

  useEffect(() => {
    const params = {
      accountsRiskId: currentId
    }
    getAccountRiskCommentDispatch(params)
  }, [currentId])

  const approveSubmit = () => {
    const errors = {}
    if (_.isEmpty(approveFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      ApprovePost(id, approveFormData)
    }
  }

  const onConfirmHold = () => {
    ApprovePost(id, holdFormData)
  }

  const holdSubmit = () => {
    const errors = {}
    if (_.isEmpty(holdFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.HOLD,
        'warning',
        'Yes',
        'No',
        () => { onConfirmHold() },
        () => { }
      )
    }
  }

  const onConfirmReject = () => {
    ApprovePost(id, rejectFormData)
  }

  const rejectSubmit = () => {
    const errors = {}
    if (_.isEmpty(rejectFormData.reason)) {
      errors.reason = USER_ERROR.REASON
    }
    setErrors(errors)
    if (_.isEmpty(errors)) {
      confirmationAlert(
        SWEET_ALERT_MSG.CONFIRMATION_TEXT,
        SWEET_ALERT_MSG.REJECT,
        'warning',
        'Yes',
        'No',
        () => { onConfirmReject() },
        () => { }
      )
    }
  }

  const handleChange = (e) => {
    setHoldFormData({ ...holdFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const rejectChange = (e) => {
    setRejectFormData({ ...rejectFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
    setRejectValue(e.target.value)
  }

  const approveChange = (e) => {
    setApproveFormData({ ...approveFormData, [e.target.name]: e.target.value })
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const onDeleteItem = (id) => {
    confirmationAlert(
      "Are you sure want to delete this comment,",
      "",
      'warning',
      'Yes',
      'No',
      () => {
        deleteAccountRiskCommentDispatch(id)
      },
      () => { { } }
    )
  }

  useEffect(() => {
    if (DeleteAccountRiskData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const params = {
        accountsRiskId: currentId
      }
      successAlert(
        DeleteAccountRiskData && DeleteAccountRiskData.message,
        'success'
      )
      getAccountRiskCommentDispatch(params)
      clearDeleteAccountRiskCommentDispatch()
    } else if (DeleteAccountRiskData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        DeleteAccountRiskData.message,
        '',
        'Ok'
      )
    }
    clearDeleteAccountRiskCommentDispatch()
  }, [DeleteAccountRiskData])

  useEffect(() => {
    if (approveResponceData && approveResponceData.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        approveResponceData && approveResponceData.message,
        'success',
      )
      const params = {
        accountsRiskId: currentId
      }
      getRiskManagementlistDispatch()
      getIdMerchantDispatch(id)
      getAccountRiskCommentDispatch(params)
      clearApprove()
    } else if (approveResponceData && approveResponceData.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'Error',
        approveResponceData.message,
        '',
        'Ok'
      )
    }
  }, [approveResponceData])

  useEffect(() => {
    if (approveResponceData && approveResponceData.message === 'Case Approved Successfully!') {
      const approvemodalBtn = document.getElementById('approve-model')
      approvemodalBtn.click()
      setApproveFormData({
        reason: "",
        riskStatus: 'APPROVED',
      })
    }
    if (approveResponceData && approveResponceData.message === 'Case Rejected Successfully!') {
      const rejectmodalBtn = document.getElementById('reject-model')
      rejectmodalBtn.click()
      setRejectFormData({
        reason: "",
        rejectType: "",
        rejectMoreValue: "",
        riskStatus: 'REJECTED',
      })
    }
  }, [approveResponceData])


  useEffect(() => {
    DeviceFingerprint.initializeScriptAsync(SECRETKYEY.FINGERPRINT)
      .then(() => {
        const callback = (result) => {
          localStorage.setItem("deviceData", result)
          setdeviceFingerPrintData(result)
          console.log("DeviceFingerprint success result : ", result)
        }
        DeviceFingerprint.AfterResult(callback)
        DeviceFingerprint.Init()
      })
      .catch((err) => {
        setdeviceFingerPrintData(err)
        console.log("DeviceFingerprint errpr result : ", err)
        localStorage.setItem("deviceData", err)
      })
  }, [])

  const IDPhone = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.phone && dashboardDetails.data.dashboardData.phone.data
  const IDAddress = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.address && dashboardDetails.data.dashboardData.address.data
  const IDEmail = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.email && dashboardDetails.data.dashboardData.email.data
  const IDNegative = dashboardDetails && dashboardDetails.data && dashboardDetails.data.negative
  const IDPositive = dashboardDetails && dashboardDetails.data && dashboardDetails.data.positive
  const Risk = dashboardDetails && dashboardDetails && dashboardDetails.risk_overview && dashboardDetails.risk_overview.risk_score
  const BusinessPhone = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessPhone && dashboardDetails.data.dashboardData.businessPhone.data
  const BusinessEmail = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessEmail && dashboardDetails.data.dashboardData.businessEmail.data
  const BusinessAddress = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.businessAddress && dashboardDetails.data.dashboardData.businessAddress.data
  const IpAddress = dashboardDetails && dashboardDetails.data && dashboardDetails.data.dashboardData && dashboardDetails.data.dashboardData.ipAddress && dashboardDetails.data.dashboardData.ipAddress.data
  const speedometetervalue = dashboardDetails && dashboardDetails.data && dashboardDetails.data.totalScore ? dashboardDetails.data.totalScore : '0'
  const viewData = matrixDetail && matrixDetail && matrixDetail.data ? matrixDetail.data : []
  const getData = viewData.filter(o => (o ? o : null))

  return (
    <>
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="hold-model"
        data-target='#holdModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="reject-model"
        data-target='#rejectModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="approve-model"
        data-target='#approveModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="modal-btn"
        data-target='#InmapModal'
        onClick={() => { }}
      />
      <button
        type='button'
        className='d-none'
        data-toggle='modal'
        id="businesmodal-btn"
        data-target='#mapModal'
        onClick={() => { }}
      />
      <div
        className='modal fade'
        id='InmapModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Individual Address</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => { setopenMap(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <div className='row mt-8'>
                <div className='col-lg-6'>
                  <div className='card w-504px h-450px'>
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='mapModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Business Address</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => { setopenBusinessMap(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <div className='row mt-8'>
                <div className='col-lg-6'>
                  <div className='card w-504px h-450px'>
                  </div>
                </div>
                <div className='col-lg-6'>
                  <div
                    style={{
                      width: "714px",
                      height: "450px",
                      backgroundColor: "#eeeeee"
                    }}
                  >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='holdModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header label-four'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                          Reason For Hold
                        </label>
                        <div className='col-lg-11 col-md-11 col-sm-11 '>
                          <textarea
                            name='reason'
                            type='text'
                            className={clsx(
                              'form-control form-control-lg form-control-solid',
                              { 'is-invalid': formData.reason && errors.reason },
                              {
                                'is-valid': formData.reason && !errors.reason
                              }
                            )}
                            placeholder='Message'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={holdFormData.reason || ''}
                          />
                          {errors.reason && (
                            <div className='fv-plugins-message-container text-danger'>
                              <span role='alert text-danger'>{errors.reason}</span>
                            </div>
                          )}
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                              onClick={() => holdSubmit()}
                              disabled={loading}
                            >
                              <span className='indicator-label'>Submit</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='approveModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Are You Sure Want to Approve This Users Risk Analytics Report ?</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                  Reason For Approved
                </label>
                <div className='col-lg-12 col-md-12 col-sm-12 '>
                  <textarea
                    name='reason'
                    type='text'
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': approveFormData.reason && errors.reason },
                      {
                        'is-valid': approveFormData.reason && !errors.reason
                      }
                    )}
                    placeholder='Message'
                    onChange={(e) => approveChange(e)}
                    autoComplete='off'
                    value={approveFormData.reason || ''}
                  />
                  {errors.reason && (
                    <div className='fv-plugins-message-container text-danger'>
                      <span role='alert text-danger'>{errors.reason}</span>
                    </div>
                  )}
                </div>
              </div>
              <div className='form-group row mb-4'>
                <div className='col-lg-6' />
                <div className='col-lg-6'>
                  <div className='col-lg-11'>
                    <button
                      type='button'
                      className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                      onClick={() => approveSubmit()}
                      disabled={loading}
                    >
                      <span className='indicator-label'>Submit</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='modal fade'
        id='rejectModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-500px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Status</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <div className='col-lg-12 col-md-12 col-sm-12 mb-3'>
                <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                  Reason For Reject :
                </label>
                <div className='col-lg-12 col-md-12 col-sm-12 mb-2'>
                  <textarea
                    name='reason'
                    type='text'
                    className={clsx(
                      'form-control form-control-lg form-control-solid',
                      { 'is-invalid': formData.reason && errors.reason },
                      {
                        'is-valid': formData.reason && !errors.reason
                      }
                    )}
                    placeholder='Message'
                    onChange={(e) => rejectChange(e)}
                    autoComplete='off'
                    value={rejectFormData.reason || ''}
                  />
                  {errors.reason && (
                    <div className='fv-plugins-message-container text-danger'>
                      <span role='alert text-danger'>{errors.reason}</span>
                    </div>
                  )}
                </div>
                <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label'>
                  Type :
                </label>
                <div className='col-lg-12 col-md-12 col-sm-12'>
                  <select
                    className='form-select form-select-solid'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => rejectChange(e)}
                    value={rejectFormData.rejectType || ''}
                    name='rejectType'
                  >
                    <option value=''>Select...</option>
                    <option value='Suspect Fraud'>Suspect Fraud</option>
                    <option value=' Fraud'>Fraud</option>
                    <option value='Spam'>Spam</option>
                    <option value='Fake'>Fake</option>
                    <option value='Funneling'>Funneling</option>
                    <option value='More'>More</option>
                  </select>
                  {
                    rejectFormData.rejectType === 'More' ? (
                      <>
                        <label className='font-size-xs font-weight-bold mb-3 fs-4 form-label mt-2'>
                          please enter your message :
                        </label>
                        <textarea
                          name='rejectMoreValue'
                          type='text'
                          className={clsx(
                            'form-control form-control-lg form-control-solid',
                            { 'is-invalid': formData.rejectMoreValue && errors.rejectMoreValue },
                            {
                              'is-valid': formData.rejectMoreValue && !errors.rejectMoreValue
                            }
                          )}
                          placeholder='Message'
                          onChange={(e) => rejectChange(e)}
                          autoComplete='off'
                          value={rejectFormData.rejectMoreValue || ''}
                        />
                      </>
                    ) : rejectValue === 'Spam' || rejectValue === 'Suspect Fraud' || rejectValue === 'Fraud' || rejectValue === 'Fake' || rejectValue === 'Funneling' ?
                      null
                      : null
                  }
                </div>
              </div>
              <div className='form-group row mb-4'>
                <div className='col-lg-6' />
                <div className='col-lg-6'>
                  <div className='col-lg-11'>
                    <button
                      type='button'
                      className='btn btn-sm btn-light-primary m-2 fa-pull-right close'
                      onClick={() => rejectSubmit()}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className=' col-md-12 card mb-5 mb-xl-10'>
        <div className='card-body pt-9 pb-0'>
          <div className='d-flex flex-wrap flex-sm-nowrap mb-3'>
            <div className='flex-grow-1'>
              <div className='d-flex justify-content-between align-items-start flex-wrap mb-2'>
                <div className='d-flex flex-column'>
                  <div className='d-flex align-items-center mb-2'>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-12'>
                    <div className="card-toolbar d-flex mt-5">
                      <>
                        <ul className="nav">
                          <li className="nav-item">
                            <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-success"
                              data-toggle='modal'
                              data-target='#approveModal'
                              onClick={() => { setApproveActive(true) }}
                            >
                              Approve
                            </a>
                          </li>
                          <li className="nav-item">
                            <a className="nav-link btn btn-sm fw-bolder px-4 me-1 btn-danger"
                              data-toggle='modal'
                              data-target='#rejectModal'
                              onClick={() => { setRejectActive(true) }}
                            >
                              Reject
                            </a>
                          </li>
                        </ul>
                      </>
                    </div>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='col-lg-3 col-md-3 col-sm-3 ps-8'>
                  <h2 className='mb-2 d-flex justify-content-start text-limegreen mb-5'>Positive Factors</h2>
                  {
                    !IDPositive
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDPositive && IDPositive.map((item, i) => {
                        return (
                          <span key={"S_" + i} className='d-flex justify-content-start'>
                            <i className='bi bi-check-circle-fill text-limegreen min-w-30px fsu ' />
                            <h6 className='fw-bold fs-6 '>{item.message}</h6>
                          </span>
                        )
                      }
                      )
                  }
                </div>
                <div className='col-lg-5 col-md-5 col-sm-5'>
                  <div className='d-flex justify-content-center mb-4'
                  >
                    <ReactSpeedometer
                      maxValue={100}
                      value={parseInt(speedometetervalue)}
                      customSegmentStops={[0, 35, 75, 100]}
                      segmentColors={["limegreen", "gold", "tomato"]}
                      needleColor="red"
                      startColor="green"
                      segments={10}
                      endColor="blue"
                      className='pichart'

                      currentValueText="Over All Score: #{value}"
                      currentValuePlaceholderStyle={"#{value}"}
                    />
                  </div>
                </div>
                <div className='col-lg-4 col-md-4 col-sm-4'>
                  <h2 className='mb-2 d-flex justify-content-start symbol-label text-darkorange mb-5'>Negative Factors</h2>
                  {
                    !IDNegative
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      : IDNegative && IDNegative.map((item, i) => {
                        return (
                          <span key={"E" + i} className='d-flex justify-content-start'>
                            <i className='bi bi-exclamation-triangle-fill text-darkorange min-w-30px fsu' />
                            <h5 className='text-darkorange fw-bold fs-6 '>{item.message}</h5>
                          </span>
                        )
                      }
                      )
                  }
                  {AccountRisklists && AccountRisklists.data && AccountRisklists.data.length > 0 ?
                    <h2 className='mb-2 d-flex justify-content-start symbol-label text-black mb-5'>Comments</h2>
                    : null
                  }
                  {
                    AccountRisklists && Array.isArray(AccountRisklists.data) > 0 ? AccountRisklists.data.map((item, i) => {
                      return (
                        <ul style={{ listStyleType: 'disc', paddingLeft: '1.5rem' }}>
                          <div className="d-flex justify-content-between mb-2">
                            <div>
                              <div className="d-flex align-item-center">
                                <div>
                                  <li>{item && item.accountsStatus}&nbsp;: </li>
                                </div>
                                <div>
                                  &nbsp;{item && item.comments}
                                </div>
                              </div>
                              <div>
                                Created Date&nbsp;: {moment(item && item.createdAt).format('DD/MM/YYYY')}
                              </div>
                            </div>
                            <div className="d-flex align-item-center px-4">
                              <button
                                className='btn btn-icon btn-icon-warning btn-sm w-10px h-10px'
                                title="Edit Data"
                                onClick={() => {
                                  setEditMode(true)
                                  setUserId(item)
                                }}
                              >
                                <KTSVG
                                  path='/media/icons/duotune/art/art005.svg'
                                  className='svg-icon-3'
                                />
                              </button>
                              <button
                                className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px ms-4'
                                onClick={() => onDeleteItem(item && item._id)}
                                title="Delete"
                              >
                                <KTSVG
                                  path='/media/icons/duotune/general/gen027.svg'
                                  className='svg-icon-3'
                                />
                              </button>
                            </div>
                          </div>
                        </ul>
                      )
                    }) : null}
                </div>
                <div className='row mb-0'>
                  <div className='col-lg-4 col-md-4 col-sm-4' />
                  <div className='col-lg-4 col-md-4 col-sm-4'>
                    <h1 className='d-flex justify-content-center  fw-boldest my-1 fs-2'>{Risk} </h1>
                  </div>
                  <div className='col-lg-4 col-md-4 col-sm-4' />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='row'>
        <div className='col-lg-6 col-md-6 col-sm-6'>
          <div className='mb-4'>
            <span className='text-muted mt-1 fw-bold fs-4 justify-content-start'>INDIVIDUAL</span>
            <h1 className='text-dark fw-bolder justify-content-start'>{merchantSummary ? merchantSummary.firstName + ' ' + merchantSummary.lastName : "--"}</h1>
          </div>
          <div className='row'>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 mb-2  bg-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-card'>Phone</span>
                    <span className='text-muted mt-1 fw-bold fs-7'>{merchantSummary ? merchantSummary.phone : "--"}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenPhone(true) }}>
                      View Phone Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDPhone
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IDPhone) ?
                        IDPhone && IDPhone.map((item, i) => {
                          return (
                            <Fragment key={"FIX_1" + i}>
                              {
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : item.value}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                    {
                                      item.title === "Leaked Online" && item.value === "true" ? (
                                        <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu'
                                          title={item.message}
                                        />
                                      ) : (
                                        item.title === "Fraud Score" ? (
                                          item.value < 75 ? (
                                            <i className='bi bi-check-circle-fill text-success min-w-30px fsu'
                                              title={item.message}
                                            />
                                          ) : (
                                            <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu'
                                              title={item.message}
                                            />
                                          )
                                        ) : (
                                          <i
                                            className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                            title={item.message}
                                          />
                                        )
                                      )
                                    }
                                  </span>
                                </div>
                              }
                            </Fragment>
                          )
                        }
                        )
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-card'>
                  <h3 className='card-title align-items-start flex-column '>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Ip Address
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7 ellipsis'>{deviceFingerPrintData && deviceFingerPrintData.ip_address ? deviceFingerPrintData.ip_address : ''}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenIpAddress(true) }}
                    >
                      View Ip address Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IpAddress
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IpAddress) ?
                        IpAddress && IpAddress.map((item, i) => {
                          return (
                            <Fragment key={"FIX_2" + i}>
                              {
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : (item.value)}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                    {
                                      item.title === "Spam Trap Score" ? (
                                        item.value < 75 ? (
                                          <i className='bi bi-check-circle-fill text-success min-w-30px fsu'
                                            title={item.message}
                                          />
                                        ) : (
                                          <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu'
                                            title={item.message}
                                          />
                                        )
                                      ) : (
                                        <i
                                          className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                          title={item.message}
                                        />
                                      )
                                    }
                                  </span>
                                </div>
                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0  bg-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-card'>
                      Email
                    </span>
                    <span className='text-muted fw-bold fs-7 ellipsis' style={{ width: '100px' }}>{merchantSummary ? merchantSummary.personalEmail : "--"}
                    </span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenEmail(true) }}
                    >
                      View Email Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDEmail
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IDEmail) ?
                        IDEmail && IDEmail.map((item, i) => {
                          return (
                            <Fragment key={"FIX_3" + i}>
                              {
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : item.value}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                    <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 mb-xl-5  bg-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Address
                    </span>
                    <a className='mt-1 fw-bold fs-7 cp text link-muted'
                      data-toggle='modal'
                      data-target='#InmapModal'
                    >
                      {merchantSummary ? merchantSummary.address : "--"}
                    </a>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenAddress(true) }}
                    >View Address Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !IDAddress
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(IDAddress) ?
                        IDAddress && IDAddress.map((item, i) => {
                          return (
                            <Fragment key={"FIX_5" + i}>
                              {
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : item.value}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                    <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-card'>
                  <h3 className='card-title align-items-start flex-column '>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Blacklisted Check
                    </span>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                    <div className='flex-grow-1 me-2'>
                      <div className='fw-bolder text-gray-800  fs-6'>Phone</div>
                      <span className='text-muted fw-bold d-block'>{_.isEmpty(summary && summary.blacklistedPhoneCheck && summary.blacklistedPhoneCheck.blacklist_phone_check && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0] && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check.status) ? '--' : summary && summary.blacklistedPhoneCheck && summary.blacklistedPhoneCheck.blacklist_phone_check && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0] && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check.status}</span>
                    </div>
                    <span className='svg-icon svg-icon-1 svg-icon-success'>
                      <i className={`${BLACKLIST[summary && summary.blacklistedPhoneCheck && summary.blacklistedPhoneCheck.blacklist_phone_check && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0] && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check && summary.blacklistedPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check.status]} fs-4`} />
                    </span>
                  </div>
                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                    <div className='flex-grow-1 me-2'>
                      <div className='fw-bolder text-gray-800  fs-6'>Email</div>
                      <span className='text-muted fw-bold d-block'>{_.isEmpty(summary && summary.blacklistedEmailCheck && summary.blacklistedEmailCheck.blacklisted_email_check && summary.blacklistedEmailCheck.blacklisted_email_check.email_number_status) ? '--' : summary && summary.blacklistedEmailCheck && summary.blacklistedEmailCheck.blacklisted_email_check && summary.blacklistedEmailCheck.blacklisted_email_check.email_number_status}</span>
                    </div>
                    <span className='svg-icon svg-icon-1 svg-icon-success'>
                      <i className={`${BLACKLIST[summary && summary.blacklistedEmailCheck && summary.blacklistedEmailCheck.blacklisted_email_check && summary.blacklistedEmailCheck.blacklisted_email_check.email_number_status]} fs-4`} />
                    </span>
                  </div>
                </div>
              </div>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-card'>
                  <h3 className='card-title align-items-start flex-column '>
                    <span className='card-label fw-bolder text-dark  bg-card'>
                      Device Fingerprints
                    </span>
                  </h3>
                </div>
                {_.isEmpty(deviceFingerPrintData) ? <h4 className=' mt-4 ms-4'>No Data Found </h4> :
                  <div className='card-body pt-0'>
                    <div className='d-flex align-items-center rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800 fs-6'>Browser</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.browser ? deviceFingerPrintData.browser : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.browser) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Device Id</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.device_id ? deviceFingerPrintData.device_id : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.device_id) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Country</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.country ? deviceFingerPrintData.country : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.country) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Fraud chance</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.fraud_chance ? deviceFingerPrintData.fraud_chance : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.fraud_chance) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Ip Address</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.ip_address ? deviceFingerPrintData.ip_address : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.ip_address) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>

                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Is crawler</div>
                        <span className='text-muted fw-bold d-block text-break'>{String(deviceFingerPrintData.is_crawler)}</span>
                      </div>
                      {deviceFingerPrintData && !deviceFingerPrintData.is_crawler ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>

                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Isp</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.isp ? deviceFingerPrintData.isp : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.isp) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Operating system</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.operating_system ? deviceFingerPrintData.operating_system : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.operating_system) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Proxy</div>
                        <span className='text-muted fw-bold d-block text-break'>{String(deviceFingerPrintData.proxy)}</span>
                      </div>
                      {deviceFingerPrintData && !deviceFingerPrintData.proxy ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Recent Abuse</div>
                        <span className='text-muted fw-bold d-block text-break'>{String(deviceFingerPrintData.recent_abuse)}</span>
                      </div>
                      {deviceFingerPrintData && !deviceFingerPrintData.recent_abuse ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Region</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.region ? deviceFingerPrintData.region : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.region) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>SSL Fingerprint</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.ssl_fingerprint ? deviceFingerPrintData.ssl_fingerprint : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.ssl_fingerprint) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Timezone</div>
                        <span className='text-muted fw-bold d-block text-break'>{deviceFingerPrintData && deviceFingerPrintData.timezone ? deviceFingerPrintData.timezone : ''}</span>
                      </div>
                      {deviceFingerPrintData && !_.isEmpty(deviceFingerPrintData.timezone) ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                    <div className='d-flex align-items-center  rounded p-5 mb-0'>
                      <div className='flex-grow-1 me-2'>
                        <div className='fw-bolder text-gray-800  fs-6'>Vpn</div>
                        <span className='text-muted fw-bold d-block text-break'>{String(deviceFingerPrintData.vpn)}</span>
                      </div>
                      {deviceFingerPrintData && !deviceFingerPrintData.vpn ? <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-check-circle-fill text-success' />
                      </span> : <span className='svg-icon svg-icon-1 svg-icon-success'>
                        <i className='fs-4 bi bi-x-circle-fill text-danger' />
                      </span>}
                    </div>
                  </div>}
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-6 col-md-6 col-sm-6'>
          <div className='mb-4'>
            <span className='text-muted mt-1 fw-bolder fs-4  justify-content-start'>BUSINESS</span>
            <h1 className='text-dark fw-bolder justify-content-start '>{merchantSummary ? merchantSummary.businessName : "--"} <i className={`${BLACKLIST[summary && summary.blacklistedMerchantCheck && summary.blacklistedMerchantCheck.blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.status]} fs-4`} /></h1>
          </div>
          <div className='row'>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8 '>
                <div className='card-header border-0 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Phone
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7'>{merchantSummary ? merchantSummary.businessPhone : "--"}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link '
                      onClick={() => { setOpenPhone(true) }}
                    >View Phone Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !BusinessPhone
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(BusinessPhone) ?
                        BusinessPhone && BusinessPhone.map((item, i) => {
                          return (
                            <Fragment key={"FIX_7" + i}>
                              {

                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : item.value}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                    {
                                      item.title === "Leaked Online" && item.value === "true" ? (
                                        <i className='bi bi-exclamation-triangle-fill text-warning min-w-30px fsu'
                                          title={item.message}
                                        />
                                      ) : (
                                        item.title === "Fraud Score" ? (
                                          item.value < 75 ? (
                                            <i className='bi bi-check-circle-fill text-success min-w-30px fsu'
                                              title={item.message}
                                            />
                                          ) : (
                                            <i className='bi bi-exclamation-triangle-fill text-danger min-w-30px fsu'
                                              title={item.message}
                                            />
                                          )
                                        ) : (
                                          <i
                                            className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                            title={item.message}
                                          />
                                        )
                                      )
                                    }
                                  </span>
                                </div>
                              }
                            </Fragment>
                          )
                        }
                        )
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-header border-0 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Email
                    </span>
                    <span className='text-muted mt-1 fw-bold fs-7 ellipsis' style={{ width: '100px' }}>{merchantSummary ? merchantSummary.businessEmail : "--"}</span>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenEmail(true) }}
                    >View Email Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !BusinessEmail
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(BusinessEmail) ?
                        BusinessEmail && BusinessEmail.map((item, i) => {
                          return (
                            <Fragment key={"FIX_8" + i}>
                              {
                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : item.value}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                     <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>
                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }

                </div>
              </div>
            </div>
            <div className='col-lg-4 col-md-4 col-sm-4'>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-header border-0 mb-xl-5 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Address
                    </span>
                    <a className='mt-1 fw-bold fs-7 cp text link-muted'
                      data-toggle='modal'
                      data-target='#mapModal'
                    >
                      {merchantSummary ? merchantSummary.businessAddress : "--"}
                    </a>
                    <a
                      className='mt-1 fw-bold fs-7 cp card-link'
                      onClick={() => { setOpenAddress(true) }}
                    >View Address Details
                    </a>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  {
                    !BusinessAddress
                      ? (
                        <div>
                          <div className='text-center'>
                            <div className='spinner-border text-primary m-5' role='status' />
                          </div>
                        </div>
                      )
                      :
                      !_.isEmpty(BusinessAddress) ?
                        BusinessAddress && BusinessAddress.map((item, i) => {
                          return (
                            <Fragment key={"FIX_9" + i}>
                              {

                                <div className='d-flex align-items-center  rounded p-5 mb-0'>
                                  <div className='flex-grow-1 me-2 w-75'>
                                    <div className='fw-bolder text-gray-800  fs-6'>{item.title}</div>
                                    <span className='text-muted fw-bold d-block'>{_.isEmpty(item.value) ? '--' : item.value}</span>
                                  </div>
                                  <span
                                    className={`fw-bolder py-1`}
                                    title={item.message}
                                  >
                                    <i
                                      className={`fw-bolder ${CHANGE_ICON[item && item.color]} min-w-30px fsu`}
                                      title={item.message}
                                    />
                                  </span>
                                </div>

                              }
                            </Fragment>
                          )
                        })
                        : <h4 className=' mt-4 ms-4'>No Data Found </h4>
                  }
                </div>
              </div>
              <div className='card card-xl-stretch mb-xl-8'>
                <div className='card-header border-0 mb-xl-5 bg-business-card'>
                  <h3 className='card-title align-items-start flex-column'>
                    <span className='card-label fw-bolder text-dark bg-business-card'>
                      Blacklisted Check
                    </span>
                  </h3>
                </div>
                <div className='card-body pt-0'>
                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                    <div className='flex-grow-1 me-2'>
                      <div className='fw-bolder text-gray-800  fs-6'>Merchant</div>
                      <span className='text-muted fw-bold d-block w-75'>{_.isEmpty(summary && summary.blacklistedMerchantCheck && summary.blacklistedMerchantCheck.blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0] && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0].blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0].blacklist_merchant_check.status) ? '--' : summary && summary.blacklistedMerchantCheck && summary.blacklistedMerchantCheck.blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0] && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0].blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0].blacklist_merchant_check.status}</span>
                    </div>
                    <span className='svg-icon svg-icon-1 svg-icon-success'>
                      <i className={`${BLACKLIST[summary && summary.blacklistedMerchantCheck && summary.blacklistedMerchantCheck.blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0] && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0].blacklist_merchant_check && summary.blacklistedMerchantCheck.blacklist_merchant_check.data[0].blacklist_merchant_check.status]} fs-4`} />
                    </span>
                  </div>
                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                    <div className='flex-grow-1 me-2'>
                      <div className='fw-bolder text-gray-800  fs-6'>Phone</div>
                      <span className='text-muted fw-bold d-block w-75'>{_.isEmpty(summary && summary.blacklistedBusinessPhoneCheck && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0] && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check.status) ? '--' : summary && summary.blacklistedBusinessPhoneCheck && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0] && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check.status}</span>
                    </div>
                    <i className={`${BLACKLIST[summary && summary.blacklistedBusinessPhoneCheck && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0] && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check && summary.blacklistedBusinessPhoneCheck.blacklist_phone_check.data[0].blacklist_phone_check.status]} fs-4`} />
                  </div>
                  <div className='d-flex align-items-center  rounded p-5 mb-0'>
                    <div className='flex-grow-1 me-2'>
                      <div className='fw-bolder text-gray-800  fs-6'>Email</div>
                      <span className='text-muted fw-bold d-block w-75'>{_.isEmpty(summary && summary.blacklistedBusinessEmailCheck && summary.blacklistedBusinessEmailCheck.blacklisted_email_check && summary.blacklistedBusinessEmailCheck.blacklisted_email_check.email_number_status) ? '--' : summary && summary.blacklistedBusinessEmailCheck && summary.blacklistedBusinessEmailCheck.blacklisted_email_check && summary.blacklistedBusinessEmailCheck.blacklisted_email_check.email_number_status}</span>
                    </div>
                    <span className='svg-icon svg-icon-1 svg-icon-success'>
                      <i className={`${BLACKLIST[summary && summary.blacklistedBusinessEmailCheck && summary.blacklistedBusinessEmailCheck.blacklisted_email_check && summary.blacklistedBusinessEmailCheck.blacklisted_email_check.email_number_status]} fs-4`} />
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <CrossCheck isLoaded={isLoaded} id={id} />
      <AccountRiskCommentEdit editMode={editMode} setEditMode={setEditMode} userId={userId} setUserId={setUserId} currentId={currentId} />

    </>
  )
}

const mapStateToProps = (state) => {
  const { riskManagementlistStore, AccountsQueueApproveStore, accountmerchantIdDetailsStore, MatrixStore, AccountRisklistStore, AccountRiskdeleteStore } = state
  return {
    riskmgmtlistdetails:
      riskManagementlistStore &&
        riskManagementlistStore.riskmgmtlists ?
        riskManagementlistStore.riskmgmtlists : {},
    approveResponceData:
      AccountsQueueApproveStore &&
        AccountsQueueApproveStore.approveResponce ?
        AccountsQueueApproveStore.approveResponce : {},
    loading:
      AccountsQueueApproveStore &&
        AccountsQueueApproveStore.loading ?
        AccountsQueueApproveStore.loading : false,
    merchantIddetails:
      accountmerchantIdDetailsStore &&
        accountmerchantIdDetailsStore.merchantIddetails ?
        accountmerchantIdDetailsStore.merchantIddetails : '',
    matrixDetail:
      MatrixStore &&
        MatrixStore.matrixDetail ?
        MatrixStore.matrixDetail : {},
    AccountRisklists: AccountRisklistStore && AccountRisklistStore.AccountRisklists ? AccountRisklistStore.AccountRisklists : '',
    AccountRiskloading: AccountRisklistStore && AccountRisklistStore.loading ? AccountRisklistStore.loading : false,
    DeleteAccountRiskData: AccountRiskdeleteStore && AccountRiskdeleteStore.DeleteAccountRiskData ? AccountRiskdeleteStore.DeleteAccountRiskData : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPrevAlertDetailsDispatch: (id) => dispatch(dashboardDetailsActions.getdashboardDetails(id)),
  getRiskManagementlistDispatch: () => dispatch(riskManagementActions.getRiskManagementlist()),
  ApprovePost: (id, params) => dispatch(AccountsQueueApproveActions.approve(id, params)),
  getRiskSummaryDispatch: (id) => dispatch(riskSummaryActions.getRiskSummary(id)),
  getIdMerchantDispatch: (id) => dispatch(accountmerchantIdDetailsActions.getaccountmerchantIdDetailsData(id)),
  clearApprove: () => dispatch(AccountsQueueApproveActions.clearApprove()),
  GetCategroyDispatch: (params) => dispatch(GetCategoryActions.GetCategroy(params)),
  getAccountRiskCommentDispatch: (params) => dispatch(AccountRiskCommentActions.getAccountRiskCommentlist(params)),
  deleteAccountRiskCommentDispatch: (params) => dispatch(AccountRiskCommentdeleteActions.delete(params)),
  clearDeleteAccountRiskCommentDispatch: () => dispatch(AccountRiskCommentdeleteActions.clear()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Merchant)