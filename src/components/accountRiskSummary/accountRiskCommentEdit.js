import { useState, useEffect } from "react"
import { connect } from "react-redux"
import _ from "lodash"
import Modal from 'react-bootstrap/Modal'
import clsx from "clsx"
import { warningAlert, successAlert } from "../../utils/alerts"
import { AccountRiskCommentActions, updateAccountRiskCommentActions } from "../../store/actions"
import { AccountRiskCommentEditValidation } from "./accountRiskCommentEditData"
import { setAccountRiskCommentData } from "./formData"
import { STATUS_RESPONSE } from '../../utils/constants'

function CommentEdit(props) {
    const {
        currentId,
        editMode,
        setEditMode,
        userId,
        setUserId,
        getAccountRiskCommentDispatch,
        UpdateAccountRiskComments,
        updateAccountRiskCommentDispatch,
        clearUpdateDispatch
    } = props
    const [formData, setFormData] = useState({
        accountsStatus: "",
        comments: "",
    })
    const [errors, setErrors] = useState({})

    const handleChanges = (e) => {
        e.preventDefault()
        setFormData((values) => ({
            ...values,
            [e.target.name]: e.target.value
        }))
        setErrors({ ...errors, [e.target.name]: '' })
    }

    const clearPopup = () => {
        setEditMode(false)
        setFormData({
            comments: "",
            accountsStatus: ""
        })
        setUserId('')
    }

    useEffect(() => {
        const data = setAccountRiskCommentData(userId)
        setFormData(data)
    }, [userId])

    const handleSubmit = () => {
        const errorMsg = AccountRiskCommentEditValidation(formData, setErrors)
        const updateValue = {
            comments: formData.comments,
            accountsStatus: formData.accountsStatus,
        }
        if (_.isEmpty(errorMsg)) {
            const editId = userId && userId._id
            updateAccountRiskCommentDispatch(editId, updateValue)
        }
    }

    useEffect(() => {
        if (UpdateAccountRiskComments.status === STATUS_RESPONSE.SUCCESS_MSG) {
            const params = {
                accountsRiskId: currentId
            }
            successAlert(
                UpdateAccountRiskComments && UpdateAccountRiskComments.message,
                'success'
            )
            getAccountRiskCommentDispatch(params)
            clearUpdateDispatch()
            clearPopup()
        } else if (UpdateAccountRiskComments.status === STATUS_RESPONSE.ERROR_MSG) {
            warningAlert(
                'Error',
                UpdateAccountRiskComments && UpdateAccountRiskComments.message,
                '',
                'Ok',
            )
            clearUpdateDispatch()
        }
    }, [UpdateAccountRiskComments])

    return (
        <>
            <Modal
                show={editMode}
                size="lg"
                centered
                onHide={() => clearPopup()}>
                <Modal.Header
                    style={{ backgroundColor: 'rgb(126 126 219)' }}
                    closeButton={() => clearPopup()}>
                    <Modal.Title
                        style={{
                            color: 'white'
                        }}
                    >
                        Update Comment
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="card card-custom card-stretch gutter-b p-8">
                        <div className="row mb-8">
                            <div className='col-md-4'>
                                <label className="font-size-xs font-weight-bold mb-3  form-label">
                                    Account Status :
                                </label>
                            </div>
                            <div className='col-md-8'>
                                <select
                                    name='accountsStatus'
                                    className='form-select form-select-solid'
                                    data-control='select'
                                    data-placeholder='Select an option'
                                    data-allow-clear='true'
                                    onChange={(e) => handleChanges(e)}
                                    value={formData && formData.accountsStatus || ''}
                                    disabled
                                >
                                    <option value=''>Select...</option>
                                    <option value='APPROVED'>APPROVED</option>
                                    <option value='REJECTED'>REJECTED</option>
                                </select>
                            </div>
                        </div>
                        <div className="row mb-8">
                            <div className='col-md-4'>
                                <label className="font-size-xs font-weight-bold mb-3 form-label">
                                    Comments :
                                </label>
                            </div>
                            <div className='col-md-8'>
                                <textarea
                                    name="comments"
                                    type="text"
                                    className={clsx(
                                        "form-control form-control-lg form-control-solid",
                                    )}
                                    placeholder="Comment"
                                    onChange={(e) => handleChanges(e)}
                                    autoComplete="off"
                                    value={formData && formData.comments || ""}
                                />
                                {errors && errors.comments && (
                                    <div className="rr mt-1">
                                        <style>{".rr{color:red}"}</style>
                                        {errors.comments}
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="row">
                            <div className='col-md-4'>
                            </div>
                            <div className='col-md-8'>
                                <button
                                    className='btn btn-light-primary m-1 mt-8 font-5vw '
                                    onClick={handleSubmit}
                                >
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )

}

const mapStateToProps = (state) => {
    const { updateAccountRiskStore } = state
    return {
        UpdateAccountRiskComments: updateAccountRiskStore && updateAccountRiskStore.saveupdateAccountRiskResponse ? updateAccountRiskStore.saveupdateAccountRiskResponse : ''
    }
}

const mapDispatchToProps = (dispatch) => ({
    getAccountRiskCommentDispatch: (params) => dispatch(AccountRiskCommentActions.getAccountRiskCommentlist(params)),
    updateAccountRiskCommentDispatch: (id, params) => dispatch(updateAccountRiskCommentActions.updateAccountRiskComment(id, params)),
    clearUpdateDispatch: () => dispatch(updateAccountRiskCommentActions.clearupdateAccountRiskComment()),
})

export default connect(mapStateToProps, mapDispatchToProps)(CommentEdit)