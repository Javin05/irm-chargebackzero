import { useEffect, useState } from "react";
import "../../styles/dashboard/posts.scss";
import axiosInstance from "../../services";
import serviceList from "../../services/serviceList";

function APIUsageBillingSummaryAWS(props) {
  const [source, setSource] = useState();

  useEffect(() => {
    getUrl();
  }, []);

  const getUrl = async () => {
    console.log("Inside getUrl", this);
    const url = serviceList.apiusagebillingsummary;
    axiosInstance.get(url).then((response) => {
      if (response.status === 200) {
        const src =
          response.data && response.data.data && response.data.data.EmbedUrl
            ? response.data.data.EmbedUrl
            : "";
        setSource(src);
      }
    });
  };

  return (
    <div className='card p-0' style={{ height: '100vh',marginTop:"-80px" }}>
      <iframe
        src={source}
        width='100%'
        height='100%'
      />
    </div>
  );
}

export default APIUsageBillingSummaryAWS;