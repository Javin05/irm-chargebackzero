import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { successAlert, warningAlert, confirmationAlert } from "../../utils/alerts"
import {
  PostPMAaction,
  WrmRiskManagementActions
} from '../../store/actions'
import { STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants'
import _ from 'lodash'
import Select from 'react-select'
import { setLocalStorage, getLocalStorage } from '../../utils/helper';

const PMAdropdown = [
  {
    "label": "Legal and Merchant DBA Name Mismatch",
    "value": "Legal and Merchant DBA Name Mismatch",
  },
  {
    "label": "Page Redirection",
    "value": "Page Redirection",
  },
  {
    "label": "Merchant DBA Name Mismatch",
    "value": "Merchant DBA Name Mismatch",
  },
  {
    "label": "NBFC License Category",
    "value": "NBFC License Category",
  },
  {
    "label": "FSSAI License Category",
    "value": "FSSAI License Category",
  },
  {
    "label": "IRDA License Category",
    "value": "IRDA License Category",
  },
  {
    "label": "Ayush License Category",
    "value": "Ayush License Category",
  },
  {
    "label": "Website is not working",
    "value": "Website is not working",
  },
  {
    "label": "Login Credentials are required",
    "value": "Login Credentials are required",
  },
  {
    "label": "Terms and Condition Policy Missing",
    "value": "Terms and Condition Policy Missing",
  },
  {
    "label": "Privacy Policy Missing",
    "value": "Privacy Policy Missing",
  },
  {
    "label": "Return Policy Missing",
    "value": "Return Policy Missing",
  },
  {
    "label": "Refund Policy Missing",
    "value": "Refund Policy Missing",
  },
  {
    "label": "Shipping Policy Missing",
    "value": "Shipping Policy Missing",
  },
  {
    "label": "About Us Policy Missing",
    "value": "About Us Policy Missing",
  },
  {
    "label": "Cancellation Policy Missing",
    "value": "Cancellation Policy Missing",
  },
  {
    "label": "Contact Us Policy Missing",
    "value": "Contact Us Policy Missing",
  },
  {
    "label": "Pricing is not updated",
    "value": "Pricing is not updated",
  },
  {
    "label": "Pricing is in Dollars",
    "value": "Pricing is in Dollars",
  },
  {
    "label": "Heavy Discounts",
    "value": "Heavy Discounts",

  },
  {
    "label": "Website Redirection",
    "value": "Website Redirection",
  },
  {
    "label": "Page Navigation Issue",
    "value": "Page Navigation Issue",
  },
  {
    "label": "No Data",
    "value": "No Data",
  },
  {
    "label": "Multiple Line of Businesses",
    "value": "Multiple Line of Businesses"
  },
  {
    "label": "Product/Service not available",
    "value": "Product/Service not available"
  },
  {
    "label":'Missing Policy Links',
    "value":'Missing Policy Links'
  },
  {
    "label": 'Mandatory Policy Exception',
    "value": 'Mandatory Policy Exception'
  },
  {
    "label": 'Banned Category',
    "value": 'Banned Category'
  },
  {
    "label": 'International Address mentioned',
    "value": 'International Address mentioned'
  },
  {
    "label": 'Legal Name Mismatch',
    "value": 'Legal Name Mismatch'
  },
  {
    "label": 'MCC Mismatch',
    "value": 'MCC Mismatch'
  },
  {
    "label": 'Business Address Mismatch',
    "value": 'Business Address Mismatch'
  },
  {
    "label": 'City Mismatch',
    "value": 'City Mismatch'
  },
  {
    "label": "Marketplace",
    "value": "Marketplace"
  },
  {
    "label": "Online Booking Unavailable",
    "value": "Online Booking Unavailable"
  },
  {
    "label": "Third party Utility Payment",
    "value": "Third party Utility Payment"
  },
  {
    "label": "License Category",
    "value": "License Category"
  },
  {
    "label": "Limited No of Products Listed",
    "value": "Limited No of Products Listed"
  },
  {
    "label": "VISA & immigration service",
    "value": "VISA & immigration service"
  }
]

function PMAcategory(props) {
  const {
    postPMAres,
    PostMAdispatchDispatch,
    ClearPostMAdispatch,
    blockListValue,
    manualCategorys,
    riskIdValueId,
    getWrmRiskManagementlistDispatch
  } = props
  const [show, setShow] = useState(false)
  const [formData, setFormData] = useState({
    PMA: '',
    type: 'whiteList',
    riskId: blockListValue ? blockListValue : '',
    status: 'APPROVED'
  })
 
  const onConfirmUpdate = ((value) => {
    if(!_.isEmpty(value)){
      const formValue = {
        pma: value,
        riskId: blockListValue,
      }
      PostMAdispatchDispatch(formValue)
    }else{
      const exportParams = JSON.parse(getLocalStorage('TAG'))
        const params = {
          tag: exportParams && exportParams.tag ? exportParams.tag : '',
          website: exportParams && exportParams.website ? exportParams.website : '',
          riskStatus: exportParams && exportParams.riskStatus ? exportParams.riskStatus : '',
          reportStatus: exportParams && exportParams.reportStatus ? exportParams.reportStatus : '',
          pma: exportParams && exportParams.pma ? exportParams.pma : '',
          createdAtFrom: exportParams && exportParams.createdAtFrom ? exportParams.createdAtFrom : '',
          createdAtTo: exportParams && exportParams.createdAtTo ? exportParams.createdAtTo : '',
          acquirer: exportParams && exportParams.acquirer ? exportParams.acquirer : '',
        }
        getWrmRiskManagementlistDispatch(params)
    }
  })

  useEffect(() => {
    if (postPMAres && postPMAres.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        postPMAres && postPMAres.message,
        'success'
      )
      ClearPostMAdispatch()
      setShow(false)
      setFormData({
        PMA: '',
      })
    } else if (postPMAres && postPMAres.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postPMAres && postPMAres.message,
        '',
        'Try again',
        '',
        () => { }
      )
      ClearPostMAdispatch()
    }
  }, [postPMAres])

 
  return (
    <>
      <div className='flex-column w-300px overflow-auto mh-80px' >
        {PMAdropdown !== null ? (
          <Select
            menuPortalTarget={document.body}
            styles={{ menuPortal: base => ({ ...base, zIndex: 9999, }) }}
            options={PMAdropdown}
            defaultValue={() => {
              if (manualCategorys !== undefined) {
                if (PMAdropdown.hasOwnProperty(manualCategorys)) {
                  let one = PMAdropdown && PMAdropdown.filter(b => b.value === manualCategorys);
                  return one[0]
                }
                else {
                  return manualCategorys.split(",").map(item=>{
                    return ({
                      value: item,
                      label:item? item:"No Data",
                    }                   
                    )
                  })
                }
              }
            }}
              onChange={(e) =>
              {
                if(e){
                  let value = !_.isEmpty(e)  ? e.map(value=>value.value).toString() : ''
                confirmationAlert(
                  SWEET_ALERT_MSG.CONFIRMATION_TEXT,
                  `Want to update the PMA ${value} to the Case #IRM${riskIdValueId}?`,
                  'warning',
                  'Yes',
                  'No',
                  () => { onConfirmUpdate(value?value:"No Data") },
                  () => { onConfirmUpdate() }
                )
                }
              
            }}
            loadingIndicator={true}
            className="basic-multi-select"
            classNamePrefix="select"
            isMulti={true}
          />
        ) : null}

      </div>
    </>
  )
}


const mapStateToProps = (state) => {
  const {
    BlockListTypeStore,
    PostPMAStore
  } = state

  return {
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType?.data : null,
    postPMAres: PostPMAStore && PostPMAStore.postPMAres ? PostPMAStore.postPMAres?.data : '',
    PMAloading: PostPMAStore && PostPMAStore.loading ? PostPMAStore.loading : '',
  }
}

const mapDispatchToProps = (dispatch) => ({
  PostMAdispatchDispatch: (params) => dispatch(PostPMAaction.PostPMA(params)),
  ClearPostMAdispatch: () => dispatch(PostPMAaction.ClearPostPMA()),
  getWrmRiskManagementlistDispatch: (params) => dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params))
})


export default connect(mapStateToProps, mapDispatchToProps)(PMAcategory)