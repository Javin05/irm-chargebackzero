import React, { useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import _ from 'lodash'
import { connect } from 'react-redux'
import WrmRiskManagement from './analyzeRiskManangement'
import clsx from 'clsx'
import { setLocalStorage, removeLocalStorage, getLocalStorage } from '../../../utils/helper'
import { clientIdLIstActions, apiKeyActions, wrmSdkActions } from '../../../store/actions'

function SdkManagement(props) {
  const {
    getClientDispatch,
    getClient,
    loadingClient,
    getApiKeyDispatch,
    getWrmRiskManagementlistDispatch,
    getApiKey,
    clearApiKeyDispatch,
    loadingApi
  } = props

  const Query = useLocation();
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const [showSDK, SetShowSDK] = useState(false)

  const currentRoute = url && url[1]
  setLocalStorage("ROUTE", JSON.stringify(pathName))
  const findRoute =
    [
      "/wrm-riskmmanagement-sdk?apiKey=",
    ]
  const joindata = `${Query.pathname}${Query.search}`
  let result = joindata.slice(0, 32)
  const QueryData = `${Query.search}`
  let QueryResult = QueryData.slice(0, 8)
  const data = QueryResult === "?apiKey="
  setLocalStorage('SDKROUTE', QueryResult)
  let QueryValue = joindata.split('=')
  const currentKey = QueryValue && QueryValue[1]
  setLocalStorage('CURRENTKEY', currentKey)
  setLocalStorage('SDKAPI', currentKey)

  return (
    <>
      {

          !_.includes(findRoute, result) ? (
            <div className="container-fixed">
              <div
                id='kt_page_title'
                data-kt-swapper='true'
                data-kt-swapper-mode='prepend'
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className={clsx('page-title d-flex bg-skyBlue py-8 d-flex justify-content-center')}
              >
                <h1 className='d-flex justify-content-center text-dark fw-bolder my-1 fs-3 ms-10'>
                  This Page Couldn't Access Now
                </h1>
              </div>
            </div>
          ) : (
            <div className="container-fixed">
              <div
                id='kt_page_title'
                data-kt-swapper='true'
                data-kt-swapper-mode='prepend'
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className={clsx('page-title d-flex bg-skyBlue py-8')}
              >
                <h1 className='d-flex align-items-center text-dark fw-bolder my-1 fs-3 ms-10'>
                  Wrm Riskmanagement
                </h1>
              </div>
              <div className="mt-10">
                <WrmRiskManagement />
              </div>
            </div>
          )
      }
    </>
  )
}

const mapStateToProps = (state) => {
  const { } = state
  return {
    getApiKey: state && state.apiKeyStore && state.apiKeyStore.getApiKeyAllData && state.apiKeyStore.getApiKeyAllData.data,
  }
}

const mapDispatchToProps = (dispatch) => ({
  getApiKeyDispatch: (id) => dispatch(apiKeyActions.getApiKey(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SdkManagement)
