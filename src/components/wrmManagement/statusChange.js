import React, { useState, useEffect, Fragment } from "react"
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import { connect } from 'react-redux'
import _ from 'lodash'
import {
  updateStatusChangeActions,
} from '../../store/actions'
import Select from 'react-select'
import { successAlert, warningAlert } from "../../utils/alerts"
import { STATUS_RESPONSE } from '../../utils/constants'

const Statusdropdown = [
  {
    "label": "Apidata-Recapture",
    "value": "Apidata-Recapture",
  },
  {
    "label": "Regenerate-Report",
    "value": "Regenerate-Report",
  },
  {
    "label": "Regenerate-Riskscore",
    "value": "Regenerate-Riskscore",
  },
  {
    "label": "Apidata-ReScheduled",
    "value": "Apidata-ReScheduled",
  }
]

const StatusChange = (props) => {
  const {
    id,
    postStatusChangeDispatch,
    statusChangeResponse,
    clearStatusChange
  } = props
  const [show, setShow] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [status, setStatus] = useState('')
  const [error, setError] = useState('')

  useEffect(() => {
    if (statusChangeResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        statusChangeResponse && statusChangeResponse.message,
        'success'
      )
      clearStatusChange()
      setIsSubmitting(false)
      setShow(false)
      setStatus('')
    } else if (statusChangeResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        statusChangeResponse && statusChangeResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
      setIsSubmitting(false)
      clearStatusChange()
    }
  })

  const OnSubmit = () => {
    if (_.isEmpty(status)) {
      setError('Please update the Status')
    } else if (_.isEmpty(error)) {
      setIsSubmitting(true)
      const data = {
        updateStatus: status
      }
      postStatusChangeDispatch(id, data)
    }
  }

  return (
    <Fragment>
      <a className="btn btn-sm btn-light-dark fw-bolder px-4 me-1"
        onClick={() => { setShow(true) }}>
        Change Status
      </a>
      <Modal
        show={show}
        onHide={() => {
          setShow(false)
          //   clearValues()
        }}
        className="price-check-modal"
        size="md"
        centered>
        <Modal.Header closeButton className="position-relative">
          <Modal.Title>Change Status</Modal.Title>
        </Modal.Header>
        <Modal.Body className="pt-2">
          <Fragment>
            <div className="row my-8 align-items-center">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Change Status :
                </label>
              </div>
              <div className='col-md-8'>
                <Select
                  styles={{ menuPortal: base => ({ ...base, zIndex: 9999, }) }}
                  options={Statusdropdown}
                  onChange={(value) => {
                    if (_.isEmpty(value)) {
                      setError('Please update the Status')
                    } else {
                      setError('')
                      setStatus(value && value.value)
                    }
                  }}
                  loadingIndicator={true}
                  className="basic-select"
                  classNamePrefix="select"
                />
                {error && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {error}
                  </div>
                )}
              </div>
            </div>
          </Fragment>
          <Form.Group className="mb-5">
            <button
              className="btn btn-secondary mr-4 me-4"
              onClick={() => {
                setShow(false)
                setStatus('')
              }}>
              Cancel
            </button>
            <button
              onClick={OnSubmit}
              disabled={isSubmitting}
              className="btn btn-primary">
              {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
              Update
            </button>
          </Form.Group>
        </Modal.Body>
      </Modal>
    </Fragment>
  )
}

const mapStateToProps = state => {
  const {
    updateStatusChangeStore,
  } = state

  return {
    statusChangeResponse: updateStatusChangeStore && updateStatusChangeStore.updateStatusChangeResponce ? updateStatusChangeStore.updateStatusChangeResponce : '',
  }
}
const mapDispatchToProps = dispatch => ({
  postStatusChangeDispatch: (id, data) => dispatch(updateStatusChangeActions.updateStatusChange(id, data)),
  clearStatusChange: () => dispatch(updateStatusChangeActions.clearStatusChange())
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatusChange);