import React, { useEffect, useState, useRef } from 'react'
import {
  WebAnalysisActions,
  getWebAnalysisActions,
  ManualWebAnalysisActions,
  DeleteWebAnalysisActions,
  EditWebAnalysisActions,
  updateWebAnalysisActions,
  clientIdLIstActions,
  ExportListActions,
  WrmRiskManagementActions,
  PlayStoreExportActions,
  UpdateWebReportActions,
  CategoryStatusActions,
  ExportWRMActions,
  ExportPlaystoreActions,
  ExportAppstoreActions,
  exportClientWebActions,
  exportClientPlayStoreActions,
  AppStoreExportActions,
  exportClientAppStoreActions
} from '../../../store/actions'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import { KTSVG } from '../../../theme/helpers'
import { STATUS_RESPONSE, SWEET_ALERT_MSG, DROPZONE_MESSAGES, FILE_CSV_DOCUMENT, STATUS_BADGE } from '../../../utils/constants'
import { successAlert, warningAlert, confirmationAlert } from "../../../utils/alerts"
import './styles.css'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Modal from 'react-bootstrap/Modal'
import { userValidation, manualValidation, WebValidation } from './validation'
import { setWebAnalysisData } from './formData'
import clsx from 'clsx'
import SearchList from './searchList'
import ReactHTMLTableToExcel from "react-html-table-to-excel"
import { getLocalStorage, removeLocalStorage } from '../../../utils/helper'
import ReactSelect from "../../../theme/layout/components/ReactSelect"
import color from "../../../utils/colors"
import { CSVLink } from "react-csv"
import FindRole from '../Role'

function WebRiskAnalysis(props) {
  const {
    className,
    loading,
    getWebAnalysisDispatch,
    BlockListType,
    WebAnalysisDispatch,
    getWebAnalysis,
    postCSVWebAnalysis,
    clearImportDispatch,
    cleargetWebAnalysislistDispatch,
    postManualWebAnalysisDispatch,
    postManualWebAnalysis,
    clearManualWebAnalysisDispatch,
    DeleteWebAnalysisDispatch,
    deleteWebAnalysis,
    DeleteClearDispatch,
    EditWebAnalysisDispatch,
    EditWebAnalysis,
    UpdateWebAnalysisDispatch,
    EditclearWebAnalysis,
    UpdateClearDispatch,
    UpdateWebAnalysis,
    clientIdDispatch,
    clinetIdLists,
    getExportDispatch,
    exportLists,
    exportShow,
    postCSVLoading,
    getWrmRiskManagementlistDispatch,
    Value,
    setexportShow,
    setExportBtn,
    exportBtn,
    exportLoading,
    clearExportListDispatch,
    postManualWebLoading,
    showPma,
    getPlayStoreExportDispatch,
    playStoreExportLoading,
    playStoreExportResponse,
    appStoreExportLoading,
    appStoreExportResponse,
    clearPlayStoreExportDispatch,
    UpdateWebReportDispatch,
    ClearUpdateWebReportDispatch,
    UploadWebloading,
    UpdateWebReportRes,
    GetClientsRes,
    exportCategoryStatusLoading,
    exportCategoryStatusData,
    CategoryStatusDispatch,
    clearCategoryStatusDispatch,
    ExportWRMDispatch,
    exportWRMData,
    exportPlaystoreData,
    ExportPlaystoreDispatch,
    exportClientWebDispatch,
    exportclientReports,
    exportClientPlayStoreDispatch,
    exportClientPlayStoreReports,
    setCredFilterParams,
    setFilter,
    limit,
    WrmRiskManagement,
    setSearchParams,
    ExportAppstoreDispatch,
    ClearExportAppstoreDispatch,
    getAppStoreExportDispatch,
    clearAppStoreExportDispatch,
    exportClientAppStoreDispatch,
    exportClientAppStoreReports
  } = props

  const ClinetId = JSON.parse(getLocalStorage('CLIENTID'))
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))
  const ClientName = JSON.parse(getLocalStorage("CLIENTNAME"))
  const paginationSearch = JSON.parse(getLocalStorage('TAG'))
  const [, setData] = useState({})
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [errors, setErrors] = useState({})
  const [error, setError] = useState({})
  const [fileName, setFileName] = useState()
  const [show, setShow] = useState(false)
  const [Webshow, setWebShow] = useState(false)
  const [edit, setEdit] = useState(false)
  const [key, setKey] = useState('BulkUpload')
  const [editMode, setEditMode] = useState(false)
  const [currentId, setcurrentId] = useState()
  const [AsigneesOption, setAsignees] = useState()
  const [SelectedAsigneesOption, setSelectedAsigneesOption] = useState('')
  const [IndidualOption, setIndidualOption] = useState()
  const [SelectedIndidualOption, setSelectedIndidualOption] = useState('')
  const [reportShow, setReportShow] = useState(false)
  const [formData, setFormData] = useState({
    clientId: Role === 'Client User' ? ClinetId : '',
    tag: '',
    file: '',
    batchType: 'Live',
    lcheck: 'No'
  })
  const [clientId, setClientId] = useState({});
  const [manualFormData, setManualFormData] = useState({
    // clientId: Role === 'Client User' ? ClinetId : '',
    tag: '',
    // batchType: 'Live',
    website: '',
    emailId: '',
    phoneNumber: '',
    upi: '',
    legalName: '',
    acquirer: '',
    businessAddress: '',
    mccCode: '',
    mccDescription: '',
    billingDescriptor: '',
    merchantDBA: '',
    id1: '',
    masterCardICA: '',
    visaBIN: '',
    city: '',
    state: '',
    country: '',
    zip: '',
    mid: '',
    primaryMerchantContactName: '',
    productsServicesDescription: '',
  })
  const [webFormData, setWebFormData] = useState({
    file: '',
  })
  const [sorting, setSorting] = useState({
    tag: false
  })
  useEffect(() => {
    getWebAnalysisDispatch()
    const params = {
      skipPagination: 'true'
    }
    clientIdDispatch(params)
    removeLocalStorage('ExportHide')
  }, [])

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber
    }
    setActivePageNumber(pageNumber)
    getWebAnalysisDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getWebAnalysisDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getWebAnalysisDispatch(params)
    }
  }
  const handleChanges = (e) => {
    setManualFormData((manualFormData) => ({ ...manualFormData, [e.target.name]: e.target.value }))
    setError({ ...error, [e.target.name]: '' })
  }

  const hiddenFileInput = useRef(null)
  const handleChange = (e) => {
    setFormData((formData) => ({ ...formData, [e.target.name]: e.target.value }))
    setErrors({ ...errors, [e.target.name]: '' })
  }

  const handleFileChange = (e) => {
    e.preventDefault()
    const { name } = e.target
    let isValidFileFormat = true
    const maxFileSize = 5
    const files = e.target.files[0]
    const fileType = files && files.type
    const uploadedFileSize = files && files.size
    isValidFileFormat = _.includes(FILE_CSV_DOCUMENT, fileType)
    const fileSize = Number(maxFileSize) * 1024 * 1024
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setWebFormData((values) => ({
          ...values,
          file: files,
        }))
        setFormData((values) => ({
          ...values,
          file: files,
        }))
        setErrors((values) => ({ ...values, file: "" }))
        setFileName(files && files.name)
      } else {
        setErrors({
          ...errors,
          [name]: `File size must below ${fileSize / 1048576
            } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        })
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.CSV_INVALID })
    }
  }

  const handleClick = (event) => {
    hiddenFileInput.current.click(event)
  }

  const OnSubmit = () => {
    const errorMsg = userValidation(formData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("tag", formData.tag)
      data.append("clientId", formData.clientId)
      data.append("file", formData.file)
      data.append("batchType", formData.batchType)
      data.append("lcheck", formData.lcheck)
      WebAnalysisDispatch(data)
    }
  }

  const OnWebSubmit = () => {
    const errorMsg = WebValidation(webFormData, setErrors)
    if (_.isEmpty(errorMsg)) {
      const data = new FormData()
      data.append("tag", paginationSearch.tag ? paginationSearch.tag : '')
      data.append("file", webFormData.file)
      UpdateWebReportDispatch(data)
    }
  }

  const onConfirmUpdate = (currentId) => {
    UpdateWebAnalysisDispatch(currentId, manualFormData)
  }

  const handelSubmit = () => {
    const errorMsg = manualValidation(manualFormData, setError)
    if (_.isEmpty(errorMsg)) {
      if (editMode) {
        confirmationAlert(
          SWEET_ALERT_MSG.CONFIRMATION_TEXT,
          SWEET_ALERT_MSG.UPDATE_WEB,
          'warning',
          'Yes',
          'No',
          () => { onConfirmUpdate(currentId) },
          () => { }
        )
      } else {
        postManualWebAnalysisDispatch(manualFormData)
      }
    }
  }

  const clearPopup = () => {
    EditclearWebAnalysis()
    setSelectedIndidualOption('')
    setEditMode(false)
    setShow(false)
    setWebShow(false)
    setSelectedAsigneesOption('')
    setErrors({})
    setFileName('')
    setFormData(values => ({
      ...values,
      file: '',
      tag: '',
    }))
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
    setWebFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  const onConfirm = () => {
    setShow(false)
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    clearImportDispatch()
    setFormData(values => ({
      ...values,
      file: '',
      tag: '',
    }))
    setManualFormData(values => ({
      ...values,
      website: '',
      tag: ''
    }))
  }

  const clear = () => {
    setFileName(null)
    cleargetWebAnalysislistDispatch()
    clearImportDispatch()
    setManualFormData({
      website: '',
      tag: ''
    })
    setFormData(values => ({
      ...values,
      file: '',
      tag: ''
    }))
  }

  useEffect(() => {
    if (show === false) {
      setKey('BulkUpload')
    }
  }, [show])

  useEffect(() => {
    if (postCSVWebAnalysis && postCSVWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        'success',
        'Back to Web RisK Analysis',
        'Ok',
        () => { onConfirm() },
        () => { clear() },
        clearImportDispatch()
      )
      getWebAnalysisDispatch()
      cleargetWebAnalysislistDispatch()
      getWrmRiskManagementlistDispatch()
      setSelectedAsigneesOption('')
      setFileName('')
    } else if (postCSVWebAnalysis && postCSVWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postCSVWebAnalysis && postCSVWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      clearImportDispatch()
    }
  }, [postCSVWebAnalysis])
  useEffect(() => {
    if (postManualWebAnalysis && postManualWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        postManualWebAnalysis && postManualWebAnalysis.message,
        'success',
        'Back to Web RisK Analysis',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      getWebAnalysisDispatch()
      clearManualWebAnalysisDispatch()
      getWrmRiskManagementlistDispatch()
      setSelectedIndidualOption('')
      setFileName('')
    } else if (postManualWebAnalysis && postManualWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        postManualWebAnalysis && postManualWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      clearManualWebAnalysisDispatch()
    }
  }, [postManualWebAnalysis])

  const onConfirmDelete = (id) => {
    DeleteWebAnalysisDispatch(id)
  }
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_WEB,
      'warning',
      'Yes',
      'No',
      () => { onConfirmDelete(id) },
      () => { }
    )
  }

  useEffect(() => {
    if (deleteWebAnalysis && deleteWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        deleteWebAnalysis && deleteWebAnalysis.message,
        'success'
      )
      getWebAnalysisDispatch()
      DeleteClearDispatch()
    } else if (deleteWebAnalysis && deleteWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        deleteWebAnalysis && deleteWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      DeleteClearDispatch()
    }
  }, [deleteWebAnalysis])

  useEffect(() => {
    if (EditWebAnalysis && EditWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      const getByIdData = EditWebAnalysis && EditWebAnalysis.data
      const data = setWebAnalysisData(getByIdData)
      setManualFormData(data)
    }
  }, [EditWebAnalysis])

  useEffect(() => {
    if (UpdateWebAnalysis && UpdateWebAnalysis.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        // UpdateWebAnalysis && UpdateWebAnalysis.message,
        'Updated Successfully',
        'success'
      )
      getWebAnalysisDispatch()
      EditclearWebAnalysis()
      UpdateClearDispatch()
      setShow(false)
    } else if (UpdateWebAnalysis && UpdateWebAnalysis.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        UpdateWebAnalysis && UpdateWebAnalysis.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      DeleteClearDispatch()
    }
  }, [UpdateWebAnalysis])

  useEffect(() => {
    if (UpdateWebReportRes && UpdateWebReportRes.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        // UpdateWebReportRes && UpdateWebReportRes.message,
        'Updated Successfully',
        'success'
      )
      ClearUpdateWebReportDispatch()
      setWebFormData(values => ({
        ...values,
        file: '',
        tag: ''
      }))
      setWebShow(false)
    } else if (UpdateWebReportRes && UpdateWebReportRes.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        UpdateWebReportRes && UpdateWebReportRes.message,
        '',
        'Try again',
        '',
        () => { { } }
      )
      ClearUpdateWebReportDispatch()
    }
  }, [UpdateWebReportRes])

  const totalPages =
    BlockListType && BlockListType.count
      ? Math.ceil(parseInt(BlockListType && BlockListType.count) / limit)
      : 1

  const AsigneesNames = clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
  useEffect(() => {
    const Asignees = getDefaultOption(AsigneesNames)
    setAsignees(Asignees)
    setIndidualOption(Asignees)
  }, [AsigneesNames])

  const getDefaultOption = (AsigneesNames) => {
    const defaultOptions = []
    for (const item in AsigneesNames) {
      defaultOptions.push({ label: AsigneesNames[item].company, value: AsigneesNames[item]._id })
    }
    return defaultOptions
  }

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeAsignees = selectedOption => {
    if (selectedOption !== null) {
      setSelectedAsigneesOption(selectedOption)
      setFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const handleChangeIndidual = selectedOption => {
    if (selectedOption !== null) {
      setSelectedIndidualOption(selectedOption)
      setManualFormData(values => ({ ...values, clientId: selectedOption.value, }))
    }
  }

  const exportParams = JSON.parse(getLocalStorage('TAG'))
  // useEffect(() => {
  //   exportParams && exportParams.tag && ExportWRMDispatch(exportParams && exportParams.tag, "tag")
  //   exportParams && exportParams.tag && ExportPlaystoreDispatch(exportParams && exportParams.tag, "tag")
  // }, [exportParams.tag])

  const exported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      acquirer: exportParams && exportParams.acquirer,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      acquirer: exportParams && exportParams.acquirer,
      riskLevel: exportParams && exportParams.riskLevel,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : clientId.clientId ? clientId.clientId: '',
      batchType: exportParams && exportParams.batchType,
      type: exportParams && exportParams.type
    }
    // ExportWRMDispatch(exportParams && exportParams.tag, "tag")
    getExportDispatch(params)
  }

  const appStoreExported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      acquirer: exportParams && exportParams.acquirer,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      acquirer: exportParams && exportParams.acquirer,
      riskLevel: exportParams && exportParams.riskLevel,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : clientId.clientId ? clientId.clientId: '',
      batchType: exportParams && exportParams.batchType,
      type: exportParams && exportParams.type
    }
    // ExportAppstoreDispatch(exportParams && exportParams.tag, "tag")
    getAppStoreExportDispatch(params)
  }

  const playStorExported = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      riskLevel: exportParams && exportParams.riskLevel,
      acquirer: exportParams && exportParams.acquirer,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : clientId.clientId ? clientId.clientId: '',
      batchType: exportParams && exportParams.batchType,
      type: exportParams && exportParams.type
    }
    getPlayStoreExportDispatch(params)
    // ExportPlaystoreDispatch(exportParams && exportParams.tag, "tag")
  }
  useEffect(() => {
    if (GetClientsRes && !_.isEmpty(GetClientsRes && GetClientsRes._id)) {
      const params = {
        tag: exportParams && exportParams.tag,
        website: exportParams && exportParams.website,
        riskStatus: exportParams && exportParams.riskStatus,
        reportStatus: exportParams && exportParams.reportStatus,
        pma: exportParams && exportParams.pma,
        createdAtFrom: exportParams && exportParams.createdAtFrom,
        createdAtTo: exportParams && exportParams.createdAtTo,
        riskLevel: exportParams && exportParams.riskLevel,
        acquirer: exportParams && exportParams.acquirer,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : clientId.clientId ? clientId.clientId: '',
        batchType: exportParams && exportParams.batchType,
        type: exportParams && exportParams.type
      }
      exportClientWebDispatch(GetClientsRes && GetClientsRes._id, params)
      exportClientPlayStoreDispatch(GetClientsRes && GetClientsRes._id, params)
      exportClientAppStoreDispatch(GetClientsRes && GetClientsRes._id, params)
    }
  }, [GetClientsRes])

  useEffect(() => {
    if (exportLists && exportLists.data && exportLists.data.status === 'ok') {
      if (Array.isArray(exportLists && exportLists.data && exportLists.data.data)) {
        const closeXlsx = document.getElementById('bulkCsvReport')
        closeXlsx.click()
        clearExportListDispatch()
      } else if (_.isEmpty(exportLists && exportLists.data && exportLists.data.data)) {
        warningAlert(
          'error',
          exportLists && exportLists.data && exportLists.data.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearExportListDispatch()
      } else if (!_.isEmpty(exportLists && exportLists.data && exportLists.data.data && exportLists.data.data.file)) {
        const reportData = exportLists && exportLists.data && exportLists.data.data && exportLists.data.data.file
        const data = reportData
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        // link.target = "_blank"
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearExportListDispatch()
      }
    }
  }, [exportLists])

  useEffect(() => {
    if (playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.status === 'ok') {
      if (Array.isArray(playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.data)) {
        const closeXlsx = document.getElementById('PlayStoreReport')
        closeXlsx.click()
        clearPlayStoreExportDispatch()
      } else if (_.isEmpty(playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.data)) {
        warningAlert(
          'error',
          playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearPlayStoreExportDispatch()
      } else if (!_.isEmpty(playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.data && playStoreExportResponse.data.data.file)) {
        const data = playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.data && playStoreExportResponse.data.data.file
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearPlayStoreExportDispatch()
      }
    }
  }, [playStoreExportResponse])

  useEffect(() => {
    if (appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.status === 'ok') {
      if (Array.isArray(appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.data)) {
        const closeXlsx = document.getElementById('AppStoreReport')
        closeXlsx.click()
        clearAppStoreExportDispatch()
      } else if (_.isEmpty(appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.data)) {
        warningAlert(
          'error',
          appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearAppStoreExportDispatch()
      } else if (!_.isEmpty(appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.data && appStoreExportResponse.data.data.file)) {
        const data = appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.data && appStoreExportResponse.data.data.file
        const link = document.createElement("a")
        const url = window.URL || window.webkitURL
        const revokeUrlAfterSec = 1000000
        link.href = data
        document.body.append(link)
        link.click()
        link.remove()
        setTimeout(() => url.revokeObjectURL(link.href), revokeUrlAfterSec)
        clearAppStoreExportDispatch()
      }
    }
  }, [appStoreExportResponse])


  const data =
    [
      {
        "website": "https://payu.in/",
        "emailId": "nihalelton@gmail.com",
        "phoneNumber": 8838678689,
        "upi": "nihalelton@ybl",
        "legalName": "PayU",
        "acquirer": "Test",
        "businessAddress": "",
        "mccCode": "",
        "mccDescription": "",
        "billingDescriptor": "",
        "merchantDBA": "",
        "id1": "",
        "masterCardICA": "",
        "visaBIN": "",
        "city": "",
        "state": "",
        "country": "",
        "zip": "",
        "mid": "",
        "primaryMerchantContactName": "",
        "productsServicesDescription": "",
        "shopifyredirecturlinput":""
      },
      {
        "website": "http://enagic.co.in",
        "emailId": "sundar@gmail.com",
        "phoneNumber": 8003556833,
        "upi": "sundar@paytm",
        "legalName": "Enagic",
        "acquirer": "Test",
        "businessAddress": "",
        "mccCode": "",
        "mccDescription": "",
        "billingDescriptor": "",
        "merchantDBA": "",
        "id1": "",
        "masterCardICA": "",
        "visaBIN": "",
        "city": "",
        "state": "",
        "country": "",
        "zip": "",
        "mid": "",
        "primaryMerchantContactName": "",
        "productsServicesDescription": ""
      },
      {
        "website": "https://gigie.in/",
        "emailId": "shiyam.shenll@gmail.com",
        "phoneNumber": 7904566977,
        "upi": "shiyam@okaxis",
        "legalName": "Gigie",
        "acquirer": "Test",
        "businessAddress": "",
        "mccCode": "",
        "mccDescription": "",
        "billingDescriptor": "",
        "merchantDBA": "",
        "id1": "",
        "masterCardICA": "",
        "visaBIN": "",
        "city": "",
        "state": "",
        "country": "",
        "zip": "",
        "mid": "",
        "primaryMerchantContactName": "",
        "productsServicesDescription": ""
      },
      {
        "website": "https://gonuclei.com/",
        "emailId": "jagannathan.arumugam@mfilterit.com",
        "phoneNumber": 9884856553,
        "upi": "jagan@okaxis",
        "legalName": "Gonuclei",
        "acquirer": "Test",
        "businessAddress": "",
        "mccCode": "",
        "mccDescription": "",
        "billingDescriptor": "",
        "merchantDBA": "",
        "id1": "",
        "masterCardICA": "",
        "visaBIN": "",
        "city": "",
        "state": "",
        "country": "",
        "zip": "",
        "mid": "",
        "primaryMerchantContactName": "",
        "productsServicesDescription": ""
      },
      {
        "website": "https://curlvana.in/",
        "emailId": "u.deepika@chargebackzero.com",
        "phoneNumber": 9677056759,
        "upi": "deepika@ybl",
        "legalName": "Curlvana",
        "acquirer": "Test",
        "businessAddress": "",
        "mccCode": "",
        "mccDescription": "",
        "billingDescriptor": "",
        "merchantDBA": "",
        "id1": "",
        "masterCardICA": "",
        "visaBIN": "",
        "city": "",
        "state": "",
        "country": "",
        "zip": "",
        "mid": "",
        "primaryMerchantContactName": "",
        "productsServicesDescription": ""
      },
      {
        "website": "https://www.planify.in/",
        "emailId": "s.sweta@chargebackzero.com",
        "phoneNumber": 9790756603,
        "upi": "sweta@okaxis",
        "legalName": "Planify",
        "acquirer": "Test",
        "businessAddress": "",
        "mccCode": "",
        "mccDescription": "",
        "billingDescriptor": "",
        "merchantDBA": "",
        "id1": "",
        "masterCardICA": "",
        "visaBIN": "",
        "city": "",
        "state": "",
        "country": "",
        "zip": "",
        "mid": "",
        "primaryMerchantContactName": "",
        "productsServicesDescription": ""
      }
    ]

  const ClientShow = [
    "KYC User",
    "Client User",
    "Phonepe User"
  ]
  const AdminValidate = [
    "Analyst",
    "Admin"
  ]


  useEffect(() => {
    if (exportCategoryStatusData && exportCategoryStatusData.status === 'ok') {
      if (Array.isArray(exportCategoryStatusData && exportCategoryStatusData.data)) {
        const closeXlsx = document.getElementById('categoryCsvReport')
        closeXlsx.click()
        clearCategoryStatusDispatch()
      } else if (exportCategoryStatusData && exportCategoryStatusData.status === 'error') {
        warningAlert(
          'error',
          exportCategoryStatusData && exportCategoryStatusData.message,
          '',
          'Try again',
          '',
          () => { clear() }
        )
        clearCategoryStatusDispatch()
      }
    }
  }, [exportCategoryStatusData])

  const categoryStatus = () => {
    const params = {
      tag: exportParams && exportParams.tag,
      website: exportParams && exportParams.website,
      acquirer: exportParams && exportParams.acquirer,
      riskStatus: exportParams && exportParams.riskStatus,
      reportStatus: exportParams && exportParams.reportStatus,
      pma: exportParams && exportParams.pma,
      createdAtFrom: exportParams && exportParams.createdAtFrom,
      createdAtTo: exportParams && exportParams.createdAtTo,
      acquirer: exportParams && exportParams.acquirer,
      riskLevel: exportParams && exportParams.riskLevel,
      clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : clientId.clientId ? clientId.clientId: '',
      batchType: exportParams && exportParams.batchType,
      type: exportParams && exportParams.type
    }
    CategoryStatusDispatch(params)
  }

  return (
    <>
      <div
        type='button'
        className='d-none'
        data-target='#categoryCsvReport'
      >
        <ReactHTMLTableToExcel
          id="categoryCsvReport"
          className="download-table-xls-button"
          table="categoryCsvModel"
          filename={`${exportParams && exportParams.tag ? exportParams.tag : exportParams.website}-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="bulkCsvReport"
          className="download-table-xls-button"
          table="table-to-xls"
          filename={`${exportParams && exportParams.tag ? exportParams.tag : "IRM"}-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type='button'
        className='d-none'
        data-toggle='modal'
        data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="PlayStoreReport"
          className="download-table-xls-button"
          table="PlayStoreReport-table"
          filename={`${exportParams && exportParams.tag ? exportParams.tag : "IRM"}-report`}
          sheet="tablexls"
        />
      </div>
      <div
        type='button'
        className='d-none'
        // data-toggle='modal'
        // data-target='#categoryModal'
        onClick={() => { }}
      >
        <ReactHTMLTableToExcel
          id="AppStoreReport"
          className="download-table-xls-button"
          table="AppStoreReport-table"
          filename={`${exportParams && exportParams.tag ? exportParams.tag : "IRM"}-report`}
          sheet="tablexls"
        />
      </div>
      <div className={`card px-2 ${className} w-100`}>
        <div className='card-body py-3'>
          <div className='row align-items-center justify-content-end g-3'>
            <div className='col-3'>
              <SearchList setExportBtn={setExportBtn} Value={Value} setexportShow={setexportShow} setReportShow={setReportShow} setFilter={setFilter} limit={limit} setSearchParams={setSearchParams}  setClientId={setClientId} />
            </div>
            {
              Role !== 'Analyst' ? (
                <div className='col-3'>
                  <button
                    className='btn btn-sm btn-light-primary btn-responsive font-7vw pull-right w-100'
                    onClick={() => {
                      setShow(true)
                      setEditMode(false)
                    }}
                  >
                    <KTSVG path='/media/icons/duotune/arrows/arr087.svg' />
                    Add Case
                  </button>
                </div>
              ) :
                (
                  ''
                )
            }
            {
              exportShow && Role !== 'Analyst' ? (WrmRiskManagement &&
                WrmRiskManagement.data ?
                <>
                  {
                    reportShow ?
                      <>
                        <div className='col-3'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-success btn-responsive font-5vw w-100 pull-right'
                            onClick={(e) => exported(e)}
                            disabled={exportLoading}
                          >
                            {!exportLoading &&
                              <span className='indicator-label'>
                                <i className="bi bi-filetype-csv" />
                                Web Report Export
                              </span>
                            }
                            {exportLoading && (
                              <span className='indicator-progress text-success' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                        <div className='col-3'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-success btn-responsive font-5vw w-100 pull-right'
                            onClick={(e) => playStorExported(e)}
                            disabled={playStoreExportLoading}
                          >
                            {!playStoreExportLoading &&
                              <span className='indicator-label'>
                                <i className="bi bi-filetype-csv" />
                                Play Store Report Export
                              </span>
                            }
                            {playStoreExportLoading && (
                              <span className='indicator-progress text-success' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                        <div className='col-3'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-success btn-responsive font-5vw w-100 pull-right'
                            onClick={(e) => appStoreExported(e)}
                            disabled={appStoreExportLoading}
                          >
                            {!appStoreExportLoading &&
                              <span className='indicator-label'>
                                <i className="bi bi-filetype-csv" />
                                App Store Report Export
                              </span>
                            }
                            {appStoreExportLoading && (
                              <span className='indicator-progress text-success' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                      </>
                      : null
                  }
                  {
                    _.includes(AdminValidate, Role) ? (
                      <>
                        <div className='col-3'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-primary btn-responsive font-5vw w-100'
                            onClick={(e) => { setWebShow(true) }}
                          >
                            Upload Web Report
                          </button>
                        </div>
                        <div className='col-3'>
                          <button
                            type='button'
                            className='btn btn-sm btn-light-success btn-responsive font-5vw w-100'
                            onClick={(e) => categoryStatus(e)}
                            disabled={exportCategoryStatusLoading}
                          >
                            {!exportCategoryStatusLoading &&
                              <span className='indicator-label'>
                                {/* <i className="bi bi-filetype-csv" /> */}
                                Download Category Status
                              </span>
                            }
                            {exportCategoryStatusLoading && (
                              <span className='indicator-progress text-success' style={{ display: 'block' }}>
                                Please wait...
                                <span className='spinner-border spinner-border-sm align-middle ms-2' />
                              </span>
                            )}
                          </button>
                        </div>
                      </>
                    ) : null
                  }
                </> : null
              )
                : null
            }
          </div>
          {/* csv Report */}
          <div className="table-responsive" style={{
            display: "none"
          }}>
            <table className="table" id="categoryCsvModel">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  <th>Website</th>
                  <th>API Category</th>
                  <th>Level1 Category</th>
                  <th>Level2 Category</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                {
                  Array.isArray(exportCategoryStatusData && exportCategoryStatusData.data) ?
                    exportCategoryStatusData && exportCategoryStatusData.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          <td>
                            {item.website}
                          </td>
                          <td>
                            {item && item.apiCategory ? item.apiCategory : '--'}
                          </td>
                          <td>
                            {item && item.level1Category ? item.level1Category : '--'}
                          </td>
                          <td>
                            {item && item.level2Category ? item.level2Category : '--'}
                          </td>
                          <td>
                            {item && item.status ? item.status : '--'}
                          </td>
                        </tr>
                      )
                    })
                    : null
                }
              </tbody>
            </table>
          </div>
          <div className="table-responsive" style={{
            display: "none"
          }}>
            <table className="table" id="table-to-xls">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  {
                    exportclientReports && exportclientReports.data && exportclientReports.data.map((item, i) => {
                      return (
                        <th key={i}>{item.report_value}</th>
                      )
                    })
                  }
                </tr>
              </thead>
              <tbody className="fs-10">
                {
                  _.isArray(exportLists && exportLists.data && exportLists.data.data) ?
                    exportLists && exportLists.data && exportLists.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          {
                            exportclientReports && exportclientReports.data && exportclientReports.data.map((itemReport, i) => {
                              let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                              return (
                                _.isArray(reportTdData) ? (
                                  <td>
                                    {item[itemReport.report_key].toString()}
                                  </td>
                                ) :
                                  <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                              )
                            })
                          }
                        </tr>
                      )
                    })
                    : null
                }
              </tbody>
            </table>
          </div>
          <div className="table-responsive" style={{
            display: "none"
          }}>
            <table className="table" id="PlayStoreReport-table">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  {
                    exportClientPlayStoreReports && exportClientPlayStoreReports.data.map((item) => {
                      return (
                        <th>{item.report_value}</th>
                      )
                    })
                  }
                </tr>
              </thead>
              <tbody className="fs-10">
                {
                  _.isArray(playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.data) ?
                    playStoreExportResponse && playStoreExportResponse.data && playStoreExportResponse.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          {
                            exportClientPlayStoreReports && exportClientPlayStoreReports.data.map((itemReport, i) => {
                              let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                              return (
                                _.isArray(reportTdData) ? (
                                  <td key={i}>
                                    {item[itemReport.report_key].toString()}
                                  </td>
                                ) :
                                  <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                              )
                            })
                          }
                        </tr>
                      )
                    })
                    : null
                }
              </tbody>
            </table>
          </div>
          <div className="table-responsive" style={{
            display: "none"
          }}>
            <table className="table" id="AppStoreReport-table">
              <thead>
                <tr className="fw-bolder fs-6 text-gray-800">
                  {
                    exportClientAppStoreReports && exportClientAppStoreReports.data.map((item, i) => {
                      return (
                        <th keu={i}>{item.report_value}</th>
                      )
                    })
                  }
                </tr>
              </thead>
              <tbody className="fs-10">
                {
                  _.isArray(appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.data) ?
                    appStoreExportResponse && appStoreExportResponse.data && appStoreExportResponse.data.data.map((item, it) => {
                      return (
                        <tr key={it}>
                          {
                            exportClientAppStoreReports && exportClientAppStoreReports.data.map((itemReport, i) => {
                              let reportTdData = itemReport.report_key ? item[itemReport.report_key] : "No Data"
                              return (
                                _.isArray(reportTdData) ? (
                                  <td key={i}>
                                    {item[itemReport.report_key].toString()}
                                  </td>
                                ) :
                                  <td>{item[itemReport.report_key] ? item[itemReport.report_key] : "No Data"}</td>
                              )
                            })
                          }
                        </tr>
                      )
                    })
                    : null
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Modal
        show={Webshow}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            Upload Web
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="card card-custom card-stretch gutter-b p-8">
            <div className="row mb-8">
              <div className='col-md-4'>
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Web Upload Document :
                </label>
              </div>
              <div className='col-md-8'>
                <input
                  type="file"
                  className="d-none"
                  name="Uploadfile"
                  id="file"
                  multiple={true}
                  ref={hiddenFileInput}
                  onChange={(e) => {
                    handleFileChange(e)
                    e.target.value = null
                  }}
                />
                <button
                  type="button"
                  style={{
                    width: "100%",
                  }}
                  className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                  onClick={handleClick}
                >
                  <i className="bi bi-filetype-csv" />
                  Upload Document
                </button>
                {errors && errors.file && (
                  <div className="rr mt-1">
                    <style>{".rr{color:red}"}</style>
                    {errors.file}
                  </div>
                )}
                {fileName && fileName}
              </div>
            </div>
            <div className="row">
              <div className='col-md-4'>
              </div>
              <div className='col-md-8'>
                <button
                  type='button'
                  className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                  onClick={(e) => OnWebSubmit(e)}
                  disabled={UploadWebloading}
                >
                  {!UploadWebloading && <span className='indicator-label'>Submit</span>}
                  {UploadWebloading && (
                    <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                      Please wait...
                      <span className='spinner-border spinner-border-sm align-middle ms-2' />
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={show}
        size="lg"
        centered
        onHide={() =>
          clearPopup()
        }>
        <Modal.Header
          style={{ backgroundColor: 'rgb(126 126 219)' }}
          closeButton={() => clearPopup()}>
          <Modal.Title
            style={{
              color: 'white'
            }}
          >
            {editMode ? "Update" : "Add"} Web Case
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="ctab ftab"
          >
            <Tab eventKey="BulkUpload" title="BulkUpload">
              <div className="card card-custom card-stretch gutter-b p-8">
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Client :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name='AppUserId'
                          className='select2'
                          classNamePrefix='select'
                          handleChangeReactSelect={handleChangeAsignees}
                          options={AsigneesOption}
                          value={SelectedAsigneesOption}
                          isDisabled={!AsigneesOption}
                        />
                        {error && error.client && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.client}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    null
                  )
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': formData.tag && errors.tag },
                        {
                          'is-valid': formData.tag && !errors.tag
                        }
                      )}
                      onChange={(e) => handleChange(e)}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={formData.tag || ''}
                    />
                    {errors && errors.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.tag}
                      </div>
                    )}
                  </div>
                </div>
                {
                  Role === 'Admin' || Role === 'Analyst' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Batch Type:
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <select
                          name='batchType'
                          className='form-select form-select-solid'
                          data-control='select'
                          data-placeholder='Select an option'
                          data-allow-clear='true'
                          onChange={(e) => handleChange(e)}
                          value={formData.batchType || ''}
                        >
                          <option value='Live'>Live</option>
                          <option value='Test'>Test</option>
                        </select>
                      </div>
                    </div>
                  ) : null
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Upload Document :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      type="file"
                      className="d-none"
                      name="file"
                      id="file"
                      multiple={true}
                      ref={hiddenFileInput}
                      onChange={(e) => {
                        handleFileChange(e)
                        e.target.value = null
                      }}
                    />
                    <button
                      type="button"
                      style={{
                        width: "100%",
                      }}
                      className="btn btn-outline btn-outline-info btn-outline-primary me-2 mb-2"
                      onClick={handleClick}
                    >
                      <i className="bi bi-filetype-csv" />
                      Upload Document
                    </button>
                    {errors && errors.file && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {errors.file}
                      </div>
                    )}
                    {fileName && fileName}
                  </div>
                </div>
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3 required form-label">
                          Limited Check :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <div className="form-check form-check-custom form-check-solid mt-4">
                          <label className='d-flex flex-stack mb-5 cursor-pointer'>
                            <span className='form-check form-check-custom form-check-solid me-2'>
                              <input
                                className='form-check-input'
                                type='radio'
                                onChange={(e) => handleChange(e)}
                                value='Yes'
                                name='lcheck'
                                checked={formData.lcheck === 'Yes'}
                              />
                            </span>
                            <span className='d-flex flex-column'>
                              <span className='fs-7 text-muted'>
                                Yes
                              </span>
                            </span>
                          </label>
                          <label className='d-flex flex-stack mb-5 cursor-pointer ms-4'>
                            <span className='form-check form-check-custom form-check-solid me-2'>
                              <input
                                className='form-check-input'
                                type='radio'
                                onChange={(e) => handleChange(e)}
                                value='No'
                                name='lcheck'
                                checked={formData.lcheck === 'No'}
                              />
                            </span>
                            <span className='d-flex flex-column'>
                              <span className='fs-7 text-muted'>
                                No
                              </span>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                  ) : null
                }
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <CSVLink
                      data={data}
                      filename={"WRMSampleFileForImport.csv"}
                      className="btn btn-sm btn-light-success btn-responsive font-5vw"
                      target="_blank"
                    >
                      <i className="bi bi-filetype-csv" />
                      Download_Sample
                    </CSVLink>
                    <div className='fs-7 mt-4 text-danger'>* Website URL alone is a mandatory input</div>
                  </div>
                </div>
                <div className="row">
                  <div className='col-md-4'>
                  </div>
                  <div className='col-md-8'>
                    <button
                      type='button'
                      className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                      onClick={(e) => OnSubmit(e)}
                      disabled={postCSVLoading}
                    >
                      {!postCSVLoading && <span className='indicator-label'>Submit</span>}
                      {postCSVLoading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="Individual" title="Individual" style={{ height: '50vh', overflow: 'scroll' }}>
              <div className="card card-custom card-stretch gutter-b p-8">
                {
                  Role === 'Admin' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Client :
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <ReactSelect
                          styles={customStyles}
                          isMulti={false}
                          name='Indidual'
                          className='select2'
                          classNamePrefix='select'
                          handleChangeReactSelect={handleChangeIndidual}
                          options={IndidualOption}
                          value={SelectedIndidualOption}
                          isDisabled={!IndidualOption}
                        />
                        {error && error.client && (
                          <div className="rr mt-1">
                            <style>{".rr{color:red}"}</style>
                            {error.client}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    null
                  )
                }
                {/* <div>
                {
                  Role === 'Admin' || Role === 'Analyst' ? (
                    <div className="row mb-8">
                      <div className='col-md-4'>
                        <label className="font-size-xs font-weight-bold mb-3  form-label">
                          Batch Type:
                        </label>
                      </div>
                      <div className='col-md-8'>
                        <select
                          name='batchType'
                          className='form-select form-select-solid'
                          data-control='select'
                          data-placeholder='Select an option'
                          data-allow-clear='true'
                          onChange={(e) => handleChanges(e)}
                          value={manualFormData.batchType || ''}
                        >
                          <option value='Live'>Live</option>
                          <option value='Test'>Test</option>
                        </select>
                      </div>
                    </div>
                  ) : null
                }
              </div> */}

                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Tag :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Tag'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.tag && error.tag },
                        {
                          'is-valid': manualFormData.tag && !error.tag
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='tag'
                      autoComplete='off'
                      value={manualFormData.tag || ''}
                    />
                    {error && error.tag && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.tag}
                      </div>
                    )}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 required form-label">
                      Website :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Website'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.website && error.website },
                        {
                          'is-valid': manualFormData.website && !error.website
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='website'
                      autoComplete='off'
                      value={manualFormData.website || ''}
                    />
                    {error && error.website && (
                      <div className="rr mt-1">
                        <style>{".rr{color:red}"}</style>
                        {error.website}
                      </div>
                    )}
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Email Id :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Email ID'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.emailId && error.emailId },
                        {
                          'is-valid': manualFormData.emailId && !error.emailId
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='emailId'
                      autoComplete='off'
                      value={manualFormData.emailId || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Phone Number :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Phone Number'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.phoneNumber && error.phoneNumber },
                        {
                          'is-valid': manualFormData.phoneNumber && !error.phoneNumber
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      maxLength={10}
                      name='phoneNumber'
                      autoComplete='off'
                      value={manualFormData.phoneNumber || ''}
                      onKeyPress={(e) => {
                        if (!/^[0-9 .]+$/.test(e.key)) {
                          e.preventDefault()
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      UPI :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='UPI'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.upi && error.upi },
                        {
                          'is-valid': manualFormData.upi && !error.upi
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='upi'
                      autoComplete='off'
                      value={manualFormData.upi || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Legal Name :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Legal Name'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.legalName && error.legalName },
                        {
                          'is-valid': manualFormData.legalName && !error.legalName
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='legalName'
                      autoComplete='off'
                      value={manualFormData.legalName || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Acquirer :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Acquirer'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.acquirer && error.acquirer },
                        {
                          'is-valid': manualFormData.acquirer && !error.acquirer
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='acquirer'
                      autoComplete='off'
                      value={manualFormData.acquirer || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Business Address :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <textarea
                      placeholder='Business Address'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.businessAddress && error.businessAddress },
                        {
                          'is-valid': manualFormData.businessAddress && !error.businessAddress
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='businessAddress'
                      autoComplete='off'
                      value={manualFormData.businessAddress || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Mcc Code :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Mcc Code'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.mccCode && error.mccCode },
                        {
                          'is-valid': manualFormData.mccCode && !error.mccCode
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='mccCode'
                      autoComplete='off'
                      value={manualFormData.mccCode || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3 form-label">
                      Mcc Description :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Mcc Description'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.mccDescription && error.mccDescription },
                        {
                          'is-valid': manualFormData.mccDescription && !error.mccDescription
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='mccDescription'
                      autoComplete='off'
                      value={manualFormData.mccDescription || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Billing Descriptor :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Billing Descriptor'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.billingDescriptor && error.billingDescriptor },
                        {
                          'is-valid': manualFormData.billingDescriptor && !error.billingDescriptor
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='billingDescriptor'
                      autoComplete='off'
                      value={manualFormData.billingDescriptor || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Merchant DBA :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Merchant DBA'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.merchantDBA && error.merchantDBA },
                        {
                          'is-valid': manualFormData.merchantDBA && !error.merchantDBA
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='merchantDBA'
                      autoComplete='off'
                      value={manualFormData.merchantDBA || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      ID1 :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='ID1'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.id1 && error.id1 },
                        {
                          'is-valid': manualFormData.id1 && !error.id1
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='id1'
                      autoComplete='off'
                      value={manualFormData.id1 || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Master Card ICA :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Master Card ICA'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.masterCardICA && error.masterCardICA },
                        {
                          'is-valid': manualFormData.masterCardICA && !error.masterCardICA
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='masterCardICA'
                      autoComplete='off'
                      value={manualFormData.masterCardICA || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Visa BIN :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Visa BIN'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.visaBIN && error.visaBIN },
                        {
                          'is-valid': manualFormData.visaBIN && !error.visaBIN
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='visaBIN'
                      autoComplete='off'
                      value={manualFormData.visaBIN || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      City :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='City'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.city && error.city },
                        {
                          'is-valid': manualFormData.city && !error.city
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='city'
                      autoComplete='off'
                      value={manualFormData.city || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      State :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='State'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.state && error.state },
                        {
                          'is-valid': manualFormData.state && !error.state
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='state'
                      autoComplete='off'
                      value={manualFormData.state || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Country :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Country'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.country && error.country },
                        {
                          'is-valid': manualFormData.country && !error.country
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='country'
                      autoComplete='off'
                      value={manualFormData.country || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      ZIP :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='ZIP'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.zip && error.zip },
                        {
                          'is-valid': manualFormData.zip && !error.zip
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='zip'
                      autoComplete='off'
                      maxLength={6}
                      value={manualFormData.zip || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      MID :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='MID'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.mid && error.mid },
                        {
                          'is-valid': manualFormData.mid && !error.mid
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='mid'
                      autoComplete='off'
                      value={manualFormData.mid || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Primary Merchant ContactName :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Primary Merchant ContactName'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.primaryMerchantContactName && error.primaryMerchantContactName },
                        {
                          'is-valid': manualFormData.primaryMerchantContactName && !error.primaryMerchantContactName
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='primaryMerchantContactName'
                      autoComplete='off'
                      value={manualFormData.primaryMerchantContactName || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Products Services Description :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Products Services Description'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.productsServicesDescription && error.productsServicesDescription },
                        {
                          'is-valid': manualFormData.productsServicesDescription && !error.productsServicesDescription
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='productsServicesDescription'
                      autoComplete='off'
                      value={manualFormData.productsServicesDescription || ''}
                    />
                  </div>
                </div>
                <div className="row mb-8">
                  <div className='col-md-4'>
                    <label className="font-size-xs font-weight-bold mb-3  form-label">
                      Shopify Redirect URL :
                    </label>
                  </div>
                  <div className='col-md-8'>
                    <input
                      placeholder='Products Services Description'
                      className={clsx(
                        'form-control form-control-lg form-control-solid',
                        { 'is-invalid': manualFormData.shopifyredirecturlinput && error.shopifyredirecturlinput },
                        {
                          'is-valid': manualFormData.shopifyredirecturlinput && !error.shopifyredirecturlinput
                        }
                      )}
                      onChange={(e) => handleChanges(e)}
                      type='text'
                      name='shopifyredirecturlinput'
                      autoComplete='off'
                      value={manualFormData.shopifyredirecturlinput || ''}
                    />
                  </div>
                </div>







                <div className="row">
                  <div className='col-md-12 text-center'>
                    <button
                      className='btn btn-light-primary m-1 mt-8 font-5vw '
                      onClick={handelSubmit}
                      disabled={postManualWebLoading}
                    >
                      {!postManualWebLoading && <span className='indicator-label'>Submit</span>}
                      {postManualWebLoading && (
                        <span className='indicator-progress text-danger' style={{ display: 'block' }}>
                          Please wait...
                          <span className='spinner-border spinner-border-sm align-middle ms-2' />
                        </span>
                      )}
                    </button>
                  </div>
                </div>
              </div>
            </Tab>
          </Tabs>
        </Modal.Body>
      </Modal>
    </>
  )
}

const mapStateToProps = state => {
  const {
    rulesStore,
    BlockListlistStore,
    BlockListTypeStore,
    AddEmailToBlacklistKeysone,
    queueslistStore,
    BlockListUploadlistStore,
    BlockListEditlistStore,
    BlockListDeletelStore,
    BlockListUpdatelistStore,
    webclientReportStore,
    getWebAnalysisStore,
    WebAnalysisStore,
    ManualWebAnalysisStore,
    DeleteWebAnalysisStore,
    EditWebAnalysisStore,
    UpdateWebAnalysisStore,
    clinetListStore,
    exportlistStore,
    PlayStoreExportStore,
    UpdateWebReportManageStore,
    GetClientsStore,
    CategoryStatusManageStore,
    ExportPlaystoreStore,
    ExportWRMStore,
    AppStoreExportStore
  } = state

  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : '',
    BlockListlists: BlockListlistStore && BlockListlistStore.BlockListlists ? BlockListlistStore.BlockListlists : {},
    BlockListType: BlockListTypeStore && BlockListTypeStore.BlockListType ? BlockListTypeStore.BlockListType : {},
    BlockListUpdateSuccess: BlockListUpdatelistStore && BlockListUpdatelistStore.BlockListUpdatelists ? BlockListUpdatelistStore.BlockListUpdatelists : {},
    BlockEmailSuccess: AddEmailToBlacklistKeysone && AddEmailToBlacklistKeysone.emailSuccess ? AddEmailToBlacklistKeysone.emailSuccess : {},
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    BlocklistsUploads: BlockListUploadlistStore && BlockListUploadlistStore.BlocklistsUploads ? BlockListUploadlistStore.BlocklistsUploads : '',
    BlockListEditlists: BlockListEditlistStore && BlockListEditlistStore.BlockListEditlists ? BlockListEditlistStore.BlockListEditlists : '',
    DelteBlockList: BlockListDeletelStore && BlockListDeletelStore.DelteBlockList ? BlockListDeletelStore.DelteBlockList : '',

    loading: getWebAnalysisStore && getWebAnalysisStore.loading && getWebAnalysisStore.loading,
    getWebAnalysis: getWebAnalysisStore && getWebAnalysisStore.getWebAnalysis ? getWebAnalysisStore.getWebAnalysis : '',
    postCSVWebAnalysis: WebAnalysisStore && WebAnalysisStore.postCSVWebAnalysis
      ? WebAnalysisStore.postCSVWebAnalysis : '',
    postCSVLoading: WebAnalysisStore && WebAnalysisStore.loading ? WebAnalysisStore.loading : '',

    postManualWebAnalysis: ManualWebAnalysisStore && ManualWebAnalysisStore.postManualWebAnalysis ? ManualWebAnalysisStore.postManualWebAnalysis : '',
    postManualWebLoading: ManualWebAnalysisStore && ManualWebAnalysisStore.loading ? ManualWebAnalysisStore.loading : '',
    deleteWebAnalysis: DeleteWebAnalysisStore && DeleteWebAnalysisStore.deleteWebAnalysis ? DeleteWebAnalysisStore.deleteWebAnalysis : '',
    EditWebAnalysis: EditWebAnalysisStore && EditWebAnalysisStore.EditWebAnalysis ? EditWebAnalysisStore.EditWebAnalysis : '',
    UpdateWebAnalysis: UpdateWebAnalysisStore && UpdateWebAnalysisStore.UpdateWebAnalysis ? UpdateWebAnalysisStore.UpdateWebAnalysis : '',
    clinetIdLists: clinetListStore && clinetListStore.clinetIdLists ? clinetListStore.clinetIdLists : '',
    exportLists: exportlistStore && exportlistStore.exportLists ? exportlistStore.exportLists : '',
    exportLoading: exportlistStore && exportlistStore.loading ? exportlistStore.loading : '',
    playStoreExportResponse: PlayStoreExportStore && PlayStoreExportStore.PlayStoreExportResponse ? PlayStoreExportStore.PlayStoreExportResponse : '',
    playStoreExportLoading: PlayStoreExportStore && PlayStoreExportStore.loading ? PlayStoreExportStore.loading : '',
    appStoreExportResponse: AppStoreExportStore && AppStoreExportStore.AppStoreExportResponse ? AppStoreExportStore.AppStoreExportResponse : '',
    appStoreExportLoading: AppStoreExportStore && AppStoreExportStore.loading ? AppStoreExportStore.loading : '',
    UploadWebloading: UpdateWebReportManageStore && UpdateWebReportManageStore.loading && UpdateWebReportManageStore.loading,
    UpdateWebReportRes: UpdateWebReportManageStore && UpdateWebReportManageStore.UpdateWebReportRes && UpdateWebReportManageStore.UpdateWebReportRes,
    GetClientsRes: GetClientsStore && GetClientsStore.GetClientsRes && GetClientsStore.GetClientsRes.data ? GetClientsStore.GetClientsRes.data : {},
    exportCategoryStatusLoading: CategoryStatusManageStore && CategoryStatusManageStore.loading ? CategoryStatusManageStore.loading : '',
    exportCategoryStatusData: CategoryStatusManageStore && CategoryStatusManageStore.CategoryStatusData ? CategoryStatusManageStore.CategoryStatusData : '',
    exportWRMData: state.ExportWRMStore && ExportWRMStore.dataEAWL ? ExportWRMStore.dataEAWL : {},
    exportPlaystoreData: ExportPlaystoreStore && ExportPlaystoreStore.dataPSEL ? ExportPlaystoreStore.dataPSEL : {},
    exportclientReports: webclientReportStore && webclientReportStore.exportclientReports ? webclientReportStore.exportclientReports : {},
    exportClientPlayStoreReports: state && state.exportClientPlayStoreReportStore && state.exportClientPlayStoreReportStore.exportClientPlayStoreReports,
    exportClientAppStoreReports: state && state.exportClientAppStoreReportStore && state.exportClientAppStoreReportStore.exportClientAppStoreReports,
  }
}

const mapDispatchToProps = dispatch => ({
  WebAnalysisDispatch: (data) => dispatch(WebAnalysisActions.getWebAnalysis(data)),
  clearImportDispatch: (data) => dispatch(WebAnalysisActions.clearWebAnalysis(data)),
  getWebAnalysisDispatch: (data) => dispatch(getWebAnalysisActions.getgetWebAnalysislist(data)),
  cleargetWebAnalysislistDispatch: (data) => dispatch(getWebAnalysisActions.cleargetWebAnalysislist(data)),
  postManualWebAnalysisDispatch: (data) => dispatch(ManualWebAnalysisActions.postWebAnalysis(data)),
  clearManualWebAnalysisDispatch: (data) => dispatch(ManualWebAnalysisActions.clearManualWebAnalysis(data)),
  DeleteWebAnalysisDispatch: (data) => dispatch(DeleteWebAnalysisActions.deleteWebAnalysisDetails(data)),
  DeleteClearDispatch: (data) => dispatch(DeleteWebAnalysisActions.clearWebAnalysisDetails(data)),
  EditWebAnalysisDispatch: (id) => dispatch(EditWebAnalysisActions.EditWebAnalysisDetails(id)),
  EditclearWebAnalysis: () => dispatch(EditWebAnalysisActions.EditclearWebAnalysisDetails()),
  UpdateWebAnalysisDispatch: (data) => dispatch(updateWebAnalysisActions.updateWebAnalysisDetails(data)),
  UpdateClearDispatch: (data) => dispatch(updateWebAnalysisActions.updateclearWebAnalysisDetails(data)),
  clientIdDispatch: (data) => dispatch(clientIdLIstActions.getclientIdList(data)),
  getExportDispatch: (data) => dispatch(ExportListActions.getExportList(data)),
  getWrmRiskManagementlistDispatch: (params) => dispatch(WrmRiskManagementActions.getWrmRiskMangemnt(params)),
  clearExportListDispatch: (data) => dispatch(ExportListActions.clearExportList(data)),
  getPlayStoreExportDispatch: (data) => dispatch(PlayStoreExportActions.getPlayStoreExport(data)),
  getAppStoreExportDispatch: (data) => dispatch(AppStoreExportActions.getAppStoreExport(data)),
  clearAppStoreExportDispatch: (data) => dispatch(AppStoreExportActions.clearAppStoreExport(data)),
  clearPlayStoreExportDispatch: (data) => dispatch(PlayStoreExportActions.clearPlayStoreExport(data)),
  UpdateWebReportDispatch: (data) => dispatch(UpdateWebReportActions.getUpdateWebReport(data)),
  ClearUpdateWebReportDispatch: (data) => dispatch(UpdateWebReportActions.clearUpdateWebReport(data)),
  CategoryStatusDispatch: (params) => dispatch(CategoryStatusActions.getCategoryStatusManage(params)),
  clearCategoryStatusDispatch: () => dispatch(CategoryStatusActions.clearCategoryStatusManage()),
  ExportWRMDispatch: (id, query) => dispatch(ExportWRMActions.getExportWRM(id, query)),
  ClearExportWRMDispatch: () => dispatch(ExportWRMActions.clearExportWRM()),
  ExportPlaystoreDispatch: (id, query) => dispatch(ExportPlaystoreActions.getExportPlaystore(id, query)),
  ClearExportPlaystoreDispatch: () => dispatch(ExportPlaystoreActions.clearExportPlaystore()),
  ExportAppstoreDispatch: (id, query) => dispatch(ExportAppstoreActions.getExportAppstore(id, query)),
  ClearExportAppstoreDispatch: () => dispatch(ExportAppstoreActions.clearExportAppstore()),
  exportClientWebDispatch: (id, params) => dispatch(exportClientWebActions.exportClientWeb(id, params)),
  exportClientPlayStoreDispatch: (id, params) => dispatch(exportClientPlayStoreActions.exportClientPlayStore(id, params)),
  exportClientAppStoreDispatch: (id, params) => dispatch(exportClientAppStoreActions.exportClientAppStore(id, params)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WebRiskAnalysis)