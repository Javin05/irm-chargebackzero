import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  StreetViewPanorama,
} from 'react-google-maps';
import { compose, withProps } from 'recompose';

function StreetView (props) {
  const  {
    splitData
  } = props
  const [value, setValue] = useState({
    latitude : "49.2853171",
    lngtiude: "-123.1119202"
  })

  const latitude = splitData && splitData.businessAddressLocation && splitData.businessAddressLocation.lat
  const logititude = splitData && splitData.businessAddressLocation && splitData.businessAddressLocation.long

  const [positions, setpositions] = useState({
    lat: 24.9178,
    lng: 67.0972
  });

  const  mapvalue = {
    latitude : "49.2853171",
    lngtiude: "-123.1119202"
  }

    const StreetViewPanormaView = compose(
      withProps({
        googleMapURL:
          'https://maps.googleapis.com/maps/api/js?key=AIzaSyA45dz86V6IxsM_kv9QL86mpcPIG6PJKws&v=3.exp&libraries=geometry,drawing,places',
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `400px` }} />,
        mapElement: <div style={{ height: `100%` }} />,
        center: { lat: latitude , lng: logititude }
      }),
      withScriptjs,
      withGoogleMap
    )(props => (
      <GoogleMap defaultZoom={8} defaultCenter={props.center}>
        <StreetViewPanorama defaultPosition={props.center} visible />
      </GoogleMap>
    ));
    return(
      <div className="App">
      <StreetViewPanormaView />
    </div>
    )
}

export default StreetView