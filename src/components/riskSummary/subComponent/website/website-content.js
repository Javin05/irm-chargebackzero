import _ from 'lodash'
import { toAbsoluteUrl } from '../../../../theme/helpers'

function WebsiteContent(props) {
  const {
    DashboardExportData,
    merchantSummary
  } = props
  
  return (
    <>
      <div className='row g-5 g-xl-8 mb-8' >
        <div className='col-xl-4'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className="card-title align-items-start flex-column">
                <span className='card-label fw-bolder text-dark fs-3'>
                  Website Categorization
                </span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Registrar Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      <a
                        className='color-primary cursor-pointer'
                        onClick={() => window.open(DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'
                          ?
                          DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'
                          : 'No Data', "_blank")}
                      >
                        {
                          DashboardExportData && DashboardExportData.domainRegistrationCompany ? DashboardExportData.domainRegistrationCompany : 'No Data'
                        }
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Code
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.productCategoryCode
                          ? DashboardExportData.productCategoryCode : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Risk Classification
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.riskClassification
                          ? DashboardExportData.riskClassification : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Risk Level
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.riskLevel
                          ? DashboardExportData.riskLevel : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Categories
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.reason ? DashboardExportData.reason : 'No data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
                <>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Paid Up Capital
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            DashboardExportData && DashboardExportData.paidUpCapital ? DashboardExportData.paidUpCapital : 'No data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Google Analytics Id Relation
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            DashboardExportData && DashboardExportData.googleAnalyticsIdRelation ? DashboardExportData.googleAnalyticsIdRelation : 'No data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </>}
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Website Details
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        LegalName
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {
                          DashboardExportData && DashboardExportData.legalName
                            ? DashboardExportData.legalName : 'No Data'
                        }
                      </span>
                    </div>
                  </div>
                </div>}
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Logo
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      <img
                        src={DashboardExportData && DashboardExportData.logo ? DashboardExportData.logo : 'No Data'}
                        onError={(e) => { e.target.src = toAbsoluteUrl('/media/avatars/No_Image_Available.jpg') }}
                        style={{ width: '50px', height: '50px', objectFit: 'cover' }}
                      />
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                    {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? "Legal Name Scrapped" : "Organization Address - Transparency"}
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Domain Redirection
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Legal Name Match Percentage
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {merchantSummary && merchantSummary.report ? merchantSummary.report.legalNameMatchPercentage : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Products Service Description
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {merchantSummary && merchantSummary.report ? merchantSummary.report.productsServicesDescription : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Primary Merchant Contact Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {merchantSummary && merchantSummary.report ? merchantSummary.report.primaryMerchantContactName : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Card Scheme Compliance(BRAM/GBPP)
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Adult Content Monitoring
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.adultContentMonitoring ? DashboardExportData.adultContentMonitoring : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Predict Medicine
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.predictMedicine ? DashboardExportData.predictMedicine : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>}
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Merchant's Policies Url
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Terms & Condition
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.termsAndConditionPageUrl ? DashboardExportData.termsAndConditionPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Shipping Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.shippingPolicyPageUrl ? DashboardExportData.shippingPolicyPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Return Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.returnPolicyPageUrl ? DashboardExportData.returnPolicyPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Contact Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.contactUsPageUrl ? DashboardExportData.contactUsPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Privacy Policy
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.privacyPolicyPageUrl ? DashboardExportData.privacyPolicyPageUrl : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
              {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
                <>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          About Us
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {merchantSummary && merchantSummary.report ? merchantSummary.report.aboutUsPageUrl : 'No Data'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Cancellation Policy
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {merchantSummary && merchantSummary.report ? merchantSummary.report.cancellationPolicyPageUrl : 'No Data'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Refund Policy
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {merchantSummary && merchantSummary.report ? merchantSummary.report.refundPolicyPageUrl : 'No Data'}
                        </span>
                      </div>
                    </div>
                  </div>
                </>}
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Purchase Or Registration
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Id Submission prompt
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                    </span>
                  </div>
                </div>
                <div className='mt-6'>
                  <h4 className='d-flex justify-content-start'>
                    <span className='card-label fw-bolder text-dark fs-3'>
                      Website Is Accessible Without Login Prompt
                    </span>
                  </h4>
                  <div className='align-items-center rounded p-2 mb-0'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6 ps-1'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Website Is Accessible Without Login Prompt
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Web Analysis Status
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
                <div className='mt-4'>
                  <h4 className='d-flex justify-content-center '>
                    <span className='card-label fw-bolder text-dark fs-3'>
                      Page Activity Check
                    </span>
                  </h4>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Is Mining Happening?
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {DashboardExportData && DashboardExportData.pageActivityCheckMining ? DashboardExportData.pageActivityCheckMining : 'No Data'}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Any Untrusted Downloads?
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {DashboardExportData && DashboardExportData.pageActivityCheckUntrustedDownloads ? DashboardExportData.pageActivityCheckUntrustedDownloads : 'No Data'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              }
              <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Health Check
                  </span>
                </h4>
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Content Accessibilty
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageHealthCheckContentAccessibilty ? DashboardExportData.pageHealthCheckContentAccessibilty : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Page loading Time
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageHealthCheckPageLoadingTime ? DashboardExportData.pageHealthCheckPageLoadingTime : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Links Connectivity Check
                  </span>
                </h4>
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Status
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageAvailabilityCheckURLStatus ? DashboardExportData.pageAvailabilityCheckURLStatus : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Sucess Rate
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.pageLinksConnectivityCheckSuccessRate ? DashboardExportData.pageLinksConnectivityCheckSuccessRate : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className='mt-4'>
                <h4 className='d-flex justify-content-center '>
                  <span className='card-label fw-bolder text-dark fs-3'>
                    Page Redirection Check
                  </span>
                </h4>
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Website Redirection
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.websiteRedirection ? DashboardExportData.websiteRedirection : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
                <div className='align-items-center rounded p-2 mb-0 mx-2'>
                  <div className='row'>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-dark fw-bold mb-1 fs-5'>
                        Website Redirection URL
                      </span>
                    </div>
                    <div className='col-lg-6 col-md-6 col-sm-6'>
                      <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                        {DashboardExportData && DashboardExportData.websiteRedirectionURL ? DashboardExportData.websiteRedirectionURL : 'No Data'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Address
                </span>
              </h3>
            </div>
            <div className='card-body pt-0'>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Address Scrapped From Website
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.addressScrappedFromWebsite}
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Address From Google Map
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.addressFromGoogleMap}
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Matched Address From Google Map
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {DashboardExportData && DashboardExportData.matchedAddressFromGoogleMap}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Malware Risk
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Malware Risk
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.malwareRisk
                          ? DashboardExportData.malwareRisk : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Malware Present
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.malwarePresent
                          ? DashboardExportData.malwarePresent : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-xl-4'>
          <div className='card card-xl-stretch mb-xl-8'>
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className='card-title align-items-start flex-column '>
                <span className='card-label fw-bolder text-dark fs-3'>
                  Content Monitoring
                </span>
              </h3>
            </div>
            <div className='card-body pt-0 mb-2'>
              <div className='align-items-center rounded p-2 mb-0 mx-2'>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Currency
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.currenciesFoundOnWebsite
                          ? DashboardExportData.currenciesFoundOnWebsite : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Price Listing
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.productsPriceListedInWebsite
                          ? DashboardExportData.productsPriceListedInWebsite : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Unreasonable Price
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.websiteContainsUnreasonablePrice
                          ? DashboardExportData.websiteContainsUnreasonablePrice : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Accessibility
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.websiteIsAccessibleWithoutLoginPrompt
                          ? DashboardExportData.websiteIsAccessibleWithoutLoginPrompt : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Average Price
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.averageProductPrice
                          ? DashboardExportData.averageProductPrice : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Min Price Listed
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.minPriceListedInHomePage
                          ? DashboardExportData.minPriceListedInHomePage : 'No Data'
                      }
                    </span>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Max Price Listed
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        DashboardExportData && DashboardExportData.maxPriceListedInHomePage
                          ? DashboardExportData.maxPriceListedInHomePage : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {merchantSummary && merchantSummary.clientId && merchantSummary.clientId._id === "668be6c4a25a2cc85f5033fa" ? null :
          <>
            <div className='col-xl-4'>
              <div className="card card-xl-stretch mb-xl-8">
                <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
                  <h3 className="card-title align-items-start flex-column">
                    <span className='card-label fw-bolder text-dark fs-3'>
                      Policy Information Details
                    </span>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Drop Shipping Website
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.dropShippingWebsite : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          International Keywords In Shipping Page
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.internationalKeywordsInShippingPage : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Cod Keywords In Shipping Page
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.codKeywordsInShippingPage : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Delivery Timeline Shipping
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.deliveryTimelineShipping : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Delivery Timeline Shipping Keyword
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.deliveryTimelineShippingKeyword : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Indian Law Keywords In TermsPage
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.indianLawKeywordsInTermsPage : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Company Name In TermsPage
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.companyNameInTermsPage : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className='align-items-center rounded p-2 mb-0 mx-2'>
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Security Keywords In PrivacyPage
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.securityKeywordsInPrivacyPage : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-xl-4'>
              <div className="card card-xl-stretch mb-xl-8">
                <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
                  <h3 className="card-title align-items-start flex-column">
                    <span className='card-label fw-bolder text-dark fs-3'>
                      MCA Details
                    </span>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Mca Blacklisted Company And Cin
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.mcaBlacklistedCompanyAndCin : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Mca Blacklisted Status
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.mcaBlacklistedStatus : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-xl-4'>
              <div className="card card-xl-stretch mb-xl-8">
                <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
                  <h3 className="card-title align-items-start flex-column">
                    <span className='card-label fw-bolder text-dark fs-3'>
                      Transaction Laundering
                    </span>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Transaction laundering address matched uls
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.transaction_laundering_address_matched_uls : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Transaction laundering legalname matched uls
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.transaction_laundering_legalname_matched_uls : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Transaction laundering email matched uls
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.transaction_laundering_email_matched_uls : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Transaction laundering phone matched uls
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.transaction_laundering_phone_matched_uls : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-xl-4'>
              <div className="card card-xl-stretch mb-xl-8">
                <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
                  <h3 className="card-title align-items-start flex-column">
                    <span className='card-label fw-bolder text-dark fs-3'>
                      PMA Checks
                    </span>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          All Violation
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.allViolation : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-xl-4'>
              <div className="card card-xl-stretch mb-xl-8">
                <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
                  <h3 className="card-title align-items-start flex-column">
                    <span className='card-label fw-bolder text-dark fs-3'>
                      Mirror Pages
                    </span>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Mirror AboutUs Page
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report && merchantSummary.report.mirrorAboutUsPage && merchantSummary.report.mirrorAboutUsPage.length > 100 ? merchantSummary.report.mirrorAboutUsPage.substring(0, 100) + '...' : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Mirror Home Page
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report && merchantSummary.report.mirrorHomePage && merchantSummary.report.mirrorHomePage.length > 100 ? merchantSummary.report.mirrorHomePage.substring(0, 100) + '...' : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-xl-4'>
              <div className="card card-xl-stretch mb-xl-8">
                <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
                  <h3 className="card-title align-items-start flex-column">
                    <span className='card-label fw-bolder text-dark fs-3'>
                      BlackList Details
                    </span>
                  </h3>
                </div>
                <div className="card-body pt-0">
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Upi in blacklisted
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.upi_in_blacklisted : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Phone in blacklisted
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.phone_in_blacklisted : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Email in blacklisted
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.email_in_blacklisted : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="align-items-center rounded p-2 mb-0 mx-2">
                    <div className='row'>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-dark fw-bold mb-1 fs-5'>
                          Upi blacklisted website
                        </span>
                      </div>
                      <div className='col-lg-6 col-md-6 col-sm-6'>
                        <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                          {
                            merchantSummary && merchantSummary.report
                              ? merchantSummary.report.upi_blacklisted_website : 'No Data'
                          }
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        }
        <div className='col-xl-4'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='card-header web-tab-header pt-4 pb-3 border-0 mb-4'>
              <h3 className="card-title align-items-start flex-column">
                <span className='card-label fw-bolder text-dark fs-3'>
                  Linked Accounts
                </span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      No of accounts upi
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.no_of_accounts_upi : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      No of accounts phone
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.no_of_accounts_phone : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      No of accounts email
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.no_of_accounts_email : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Linked to rejected cases
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.linked_to_rejected_cases : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Linked cases
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.linked_cases : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      No accounts linked
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.no_accounts_linked : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Linked accounts
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.linked_accounts : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Is account linked
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.is_account_linked : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="align-items-center rounded p-2 mb-0 mx-2">
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Rejection reason
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold  text-gray-700 text-capital'>
                      {
                        merchantSummary && merchantSummary.report
                          ? merchantSummary.report.rejection_reason : 'No Data'
                      }
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default WebsiteContent