import React, { Fragment, useEffect, useState } from 'react'
import _, { isArray } from 'lodash'
import MapGoogle from "../../../maps/MapGoogle"
import StreetMap from "../../../maps/StreetMap"

function MerchantInfo(props) {
  const {
    matrixDetail,
    isLoaded,
    DashboardExportData,
    merchantIddetails
  } = props

  let allMarkers = []
  if (matrixDetail && matrixDetail.businessAddressLocation) {
    allMarkers.push({
      lat: matrixDetail.businessAddressLocation.lat,
      lng: matrixDetail.businessAddressLocation.long,
      area: "BUSINESS ADDRESS"
    })
  }

  return (
    <>
      <div className='row g-5 g-xl-8 mb-8'>
        <h1>Merchant Info</h1>
        <div className="card card-xl-stretch mb-xl-8 mx-3">
          <div className='border-0 pt-5'>
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label fw-bold fs-2 mb-1">Website</span>
            </h3>
          </div>
          <div className="card-body pt-0">
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr className="fw-bold fs-6 text-gray-800">
                    <th>Risk Score</th>
                    <th>Domain</th>
                    <th>Acquirer</th>
                    <th>Reason</th>
                    <th>MCC Scrapped</th>
                    <th>MCC Mismatch</th>
                    <th>Risk Classification</th>
                    <th>Website Working</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{DashboardExportData.riskScore}</td>
                    <td>
                      <a
                        className='color-primary cursor-pointer'
                        onClick={() => window.open(DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'
                          ?
                          DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'
                          : 'No Data', "_blank")}
                      >
                        {
                          DashboardExportData && DashboardExportData.webUrl ? DashboardExportData.webUrl : 'No Data'
                        }
                      </a>
                    </td>
                    <td>
                      {merchantIddetails && merchantIddetails.data && merchantIddetails.data.acquirer ? merchantIddetails.data.acquirer : ''}
                    </td>
                    <td>
                      {DashboardExportData && DashboardExportData.reason ? DashboardExportData.reason : 'No Data'}
                    </td>
                    <td>
                      {DashboardExportData && DashboardExportData.productCategoryCode ? DashboardExportData.productCategoryCode : 'No Data'}
                    </td>
                    <td>
                      {DashboardExportData && DashboardExportData.mccCodeMatch ? DashboardExportData.mccCodeMatch : 'No Data'}
                    </td>
                    <td>
                      {DashboardExportData && DashboardExportData.riskClassification ? DashboardExportData.riskClassification : 'No Data'}
                    </td>
                    <td>
                      {DashboardExportData && DashboardExportData.websiteWorking ? DashboardExportData.websiteWorking : 'No Data'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>  
        <div className='col-lg-6'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='ms-4 border-0 pt-5'>
              <h3 className="card-title align-items-start flex-column">
                <div className="symbol symbol-50px me-2">
                  <span className="symbol-label">
                    <i className="bi bi-exclamation-circle fs-2 text-danger h-40 align-self-center" alt="" />
                  </span>
                </div>
                <span className="card-label fw-bold fs-2 mb-1">Overall Risk</span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className="align-items-center  rounded p-2 mb-0 ms-4">
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Risk Score
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.riskScore ? DashboardExportData.riskScore :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Status
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.status ? DashboardExportData.status :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Business Risk
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.businessRisk ? DashboardExportData.businessRisk :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Web Content Risk
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.webContentRisk ? DashboardExportData.webContentRisk :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Transparency Risk
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.transparencyRisk ? DashboardExportData.transparencyRisk :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Website Health
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.websiteHealth ? DashboardExportData.websiteHealth :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Website Content Checkup Domain Health
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.websiteContentCheckupDomainHealth ? DashboardExportData.websiteContentCheckupDomainHealth :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Online Website Reputation
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.onlineWebsiteReputation ? DashboardExportData.onlineWebsiteReputation :'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Web Security
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.webSecurity ? DashboardExportData.webSecurity :'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-6'>
          <div className="card card-xl-stretch mb-xl-8">
            <div className='ms-4 border-0 pt-5'>
              <h3 className="card-title align-items-start flex-column">
                <div className="symbol symbol-50px me-2">
                  <span className="symbol-label">
                    <i className="bi bi-person-circle fs-2 text-danger h-40 align-self-center" alt="" />
                  </span>
                </div>
                <span className="card-label fw-bold fs-2 mb-1">Contact Details</span>
              </h3>
            </div>
            <div className="card-body pt-0">
              <div className='align-items-center rounded p-2 mb-0 ms-4'>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Email Id
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {
                        Array.isArray(DashboardExportData && DashboardExportData.contactDetailsEmail) ?
                        DashboardExportData && DashboardExportData.contactDetailsEmail.map((email) =>{
                          return(
                            <>{email}<br />
                            </>
                          )
                        })
                        :DashboardExportData && DashboardExportData.contactDetailsEmail
                      }
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Legal Name
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold text-capital'>
                      {DashboardExportData && DashboardExportData.legalNameScrapped ? DashboardExportData.legalNameScrapped : 'No Data'}
                    </span>
                  </div>
                </div>
                <div className='row mb-2 align-items-cente'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Telephone
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                    {Array.isArray(DashboardExportData && DashboardExportData.contactDetailsPhone) ?
                        DashboardExportData && DashboardExportData.contactDetailsPhone.map((Phone) =>{
                          return(
                            <>{Phone}<br />
                            </>
                          )
                        })
                        :DashboardExportData && DashboardExportData.contactDetailsPhone}
                    </span>
                  </div>
                </div>
                <div className="row mb-2">
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-dark fw-bold mb-1 fs-5'>
                      Business Address
                    </span>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <span className='text-muted fw-semibold'>
                      {DashboardExportData && DashboardExportData.businessAddress ? DashboardExportData.businessAddress : 'No Data'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-lg-12'>
          <div className="row mt-4">
            <div className="col-lg-6">
              <div className="card w-744px h-450px">
                {isLoaded ? <MapGoogle mapData={matrixDetail} mapMarkers={allMarkers} /> : null}
              </div>
            </div>
            <div className="col-lg-6">
              <div
                style={{
                  height: "450px",
                  backgroundColor: "#eeeeee",
                }}
              >
                {isLoaded ? <StreetMap mapData={matrixDetail} /> : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default MerchantInfo