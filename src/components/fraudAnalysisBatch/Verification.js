import React from 'react';
import Modal from "react-bootstrap/Modal";


const Verification = ({fraudToolResponseData,setShow,show,loading,formData,modalHeader,setFormData}) => {


  const onCloseHandle = ()=>{
    setShow(false)
    setFormData({...formData,documentLink:""})
  }
    const showResponse = (apiName) => {
        let close = () => {
          
          return (
            <div className="d-flex align-items-center justify-content-evenly ">
              <button
                className="btn btn-primary m-1 mt-8  font-5vw "
                onClick={() =>onCloseHandle()}
              >
                Close
              </button>
            </div>
          );
        };
        if (apiName === "Shop Lens") {
          return (
            <>
              {fraudToolResponseData?.ocr?.status ? (
                <>
                  <div className="d-flex justify-content-center">
                    <i
                      className="bi bi-check-circle-fill"
                      style={{
                        color: "#208f20",
                        backgroundColor: "transparent",
                        fontSize: "2.75rem",
                      }}
                    />
                  </div>
                  <div className="fs-1hx fw-bold text-blue text-center mb-10 mt-5">
                    Verification successfull!!
                  </div>
                </>
              ) : (
                <>
                  <div className="d-flex justify-content-center">
                    <i
                      className="bi bi-x-circle-fill"
                      style={{
                        color: "#FF474C",
                        backgroundColor: "transparent",
                        fontSize: "2.75rem",
                      }}
                    />
                  </div>
                  <div className="fs-1hx fw-bold text-green text-center mb-10 mt-5">
                    Verification failed!!
                  </div>
                </>
              )}
              <div className="row mt-10 mb-20 ">
                <div className="col-md-4">
                  <p>Shop name:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopName
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.shopName
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Shop name extracted:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopNameExtracted
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.shopNameExtracted
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Shop detected:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopDetected
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.shopDetected.toString()
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Shop owner name:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopOwnerName
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.shopOwnerName
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Shop owner name extracted:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.shopOwnerNameExtracted
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.shopOwnerNameExtracted
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>GST number:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.gstNumber
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.gstNumber
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Contact number:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.contactNumber
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.contactNumber
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Shop board Standee detected:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.boardStandeeDetected
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.boardStandeeDetected.toString()
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Shop Inventory detected:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.inventoryDetected
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.inventoryDetected.toString()
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>isMovable:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.isMovable
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.isMovable
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Address:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.address
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.address
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Address extracted:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.addressExtracted
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.addressExtracted
                      : "No data"}
                  </strong>
                </div>
                {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api?.mccDetails?.map(
                  (ele, index) => {
                    return (
                      <>
                        <div className="col-md-4">
                          <p>Description {index + 1}:</p>{" "}
                        </div>
                        <div className="col-md-8 ml-auto ">
                          <strong>{ele.description}</strong>
                        </div>
                        <div className="col-md-4">
                          <p>MCC Code {index + 1}:</p>{" "}
                        </div>
                        <div className="col-md-8 ml-auto ">
                          <strong>{ele.mccCode}</strong>
                        </div>
                      </>
                    );
                  }
                )}
                <div className="col-md-4">
                  <p>Status:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.status
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.status
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Date:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.datetime
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.datetime
                      : "No data"}
                  </strong>
                </div>
                <div className="col-md-4">
                  <p>Message:</p>{" "}
                </div>
                <div className="col-md-8 ml-auto ">
                  <strong>
                    {fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                      ?.message
                      ? fraudToolResponseData?.ocr?.data[0]?.shoplens360_api
                          ?.message
                      : "No data"}
                  </strong>
                </div>
              </div>
            </>
          );
        }
      };
    return (
        <>
      {/* {show && (
        <Modal
          show={show}
          size="lg"
          centered
          // onHide={() => clearPopup()}
        >
          <Modal.Header
            style={{ backgroundColor: "rgb(36 36 92)" }}
            // closeButton={() => clearPopup()}
          >
            <Modal.Title
              style={{
                color: "white",
              }}
            >
              {modalHeader} verification result
            </Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ minHeight: "400px" }}>
            <>
              <div className="card-body">
                <div className="row">
                  {loading ? (
                    <span
                      className="indicator-progress text-black"
                      style={{
                        display: "block",
                        fontSize: 30,
                        background: "white",
                        paddingLeft: 200,
                        paddingTop: 80,
                      }}
                    >
                      Please wait...
                      <span className="spinner-border spinner-border-sm align-middle ms-2" />
                    </span>
                  ) : (
                    <>
                    <img src={formData.documentLink} />
{                    showResponse(formData.api_name)
}                    </>
                  )}
                </div>
              </div>
            </>
          </Modal.Body>
        </Modal>
      )} */}
        </>
      );
}
 
export default Verification;