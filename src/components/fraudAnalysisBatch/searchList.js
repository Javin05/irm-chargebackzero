import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { FraudAnalysisSummaryListActions } from '../../store/actions'

function SearchList(props) {
  const { getFraudAnalysislistDispatch,  activePageNumber, limit } = props
  const [, setShow] = useState(false)

  const [formData, setFormData] = useState({
    api_name: '',
    fraud_summ_id: ''
  })

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleChangeFraudID = (e) => {
    const value = e.target.value;
    const lastFourDigits = value.slice(-4);
    setFormData({
        ...formData,
        fraud_summ_id: lastFourDigits
    });
};

  const handleSearch = () => {
    setShow(false)
    const params = {
      ...formData,
      limit: limit,
      page: activePageNumber
    }
    getFraudAnalysislistDispatch(params)
  }

  const handleReset = () => {
    setFormData({
      api_name: '',
      fraud_summ_id: ''
    })
    const params = {
      limit: limit,
      page: activePageNumber,
    }
    getFraudAnalysislistDispatch(params)
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>
      <div
        className='modal fade'
        id='searchModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-800px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Search</h2>
              <button type="button"
                data-repeater-delete=""
                className="btn btn-sm btn-icon btn-light-danger"
                data-dismiss='modal'
                onClick={() => { setShow(false) }}
              >
                <span className="svg-icon svg-icon-2">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor"></rect>
                    <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor"></rect>
                  </svg>
                </span>
              </button>
            </div>
            <div className='modal-body bg-lightBlue'>
              <form className='container-fixed'>
                <div className='card-header'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                    <div className='col-lg-6 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          API Name:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='api_name'
                            type='text'
                            className='form-control'
                            placeholder='API Name'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.api_name || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-6 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Fraud Summary ID:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='fraud_summ_id'
                            type='text'
                            className='form-control'
                            placeholder='Fraud Summary ID'
                            onChange={(e) => handleChangeFraudID(e)}
                            autoComplete='off'
                            value={formData.fraud_summ_id || ''}
                          />
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                              onClick={() => handleSearch()}
                              data-dismiss='modal'
                            >
                              Search
                            </button>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                              onClick={() => handleReset()}
                            >
                              Reset
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => ({
  getFraudAnalysislist: state && state.FraudAnalysisSummaryListStore && state.FraudAnalysisSummaryListStore.fraudAnalysisSummaryData,
  loading: state && state.FraudAnalysisSummaryListStore && state.FraudAnalysisSummaryListStore.loading,
})

const mapDispatchToProps = dispatch => ({
  getFraudAnalysislistDispatch: (params) => dispatch(FraudAnalysisSummaryListActions.request(params))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)