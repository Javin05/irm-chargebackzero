import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import {
  FraudAnalysisBulkUploadActions,
  FraudAnalysisSummaryListActions,
  clientIdLIstActions,
} from "../../store/actions";
import clsx from "clsx";
import { RESTRICTED_FILE_FORMAT_TYPE_ZIP } from "../../constants";
import _ from "lodash";
import { DROPZONE_MESSAGES, STATUS_RESPONSE } from "../../utils/constants";
import { FraudAnalysisFormValidation } from "./validator";
import { getLocalStorage } from "../../utils/helper";
import Modal from "react-bootstrap/Modal";
import Verification from "./Verification";
import { successAlert, warningAlert } from "../../utils/alerts";

let document = [
  {
    name: "Shop Lens",
    keyName: "shop_lens",
    category: "business",
  }
];
const category = [
  {
    name: "Business Due Diligence",
    key: "business",
  }
];
const FraudAnalysisBatch = ({
  clientIdDispatch,
  clinetIdLists,
  fraudToolfileDispatch,
  fraudToolResponseData,
  fraudToolUploadResponse,
  loading,
  getFraudToolListDispatch,
  clearFileUploadDispatch,
}) => {
  const [errors, setErrors] = useState({});
  const [showformg, setShowformg] = useState(false);
  const [fileIndex, setfileIndex] = useState();
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  const [disables, setDisables] = useState(false);
  const [modalHeader, setModalHeader] = useState("");
  const [show, setShow] = useState(false);
  const [docCategory, setDocCategory] = React.useState("");
  const [fileName, setFileName] = useState('');

  const [formData, setFormData] = useState({
    clientId: "",
    api_name: "",
    document: false,
    documentLink: "",
    tag: "",
  });
  const hiddenFileInput = useRef(null);
  useEffect(() => {
    const params = {
      skipPagination: "true",
    };
    clientIdDispatch(params);
  }, []);
  const handleChanges = (e) => {
      setFormData((manualFormData) => ({
        ...manualFormData,
        [e.target.name]: e.target.value,
      }));
    setErrors({ ...errors, [e.target.name]: "" });
  };
  const handleTrimWhiteSpace = (e, setState) => {
    const { name, value } = e.target;
    if (value && value.length > 1) {
      const getData = value.replace(/ +/g, " ");
      setState((values) => ({ ...values, [name]: getData.trim() }));
    } else {
      setState((values) => ({ ...values, [name]: "" }));
    }
  };
  let clientList =
    clinetIdLists && clinetIdLists.data && clinetIdLists.data.result
      ? clinetIdLists.data.result
      : [];
  const handleSubmit = () => {
    const errors = FraudAnalysisFormValidation(
      formData,
      setErrors,
      formData.api_name
    );
    if (_.isEmpty(errors)) {
      const data = new FormData();
      data.append('client_id', formData.clientId);
      data.append('api_key', formData.api_name);
      data.append('file', fileName);
      data.append('tag', formData.tag);
      fraudToolfileDispatch(data);
    }
    clearFileUploadDispatch();
  };
  const FileChangeHandler = (e) => {
    const { name } = e.target;
    let isValidFileFormat = true;
    const maxFileSize = 5;
    const files = e.target.files[0];
    const fileType = files && files.type;
    const uploadedFileSize = files && files.size;
    isValidFileFormat = _.includes(RESTRICTED_FILE_FORMAT_TYPE_ZIP, fileType);
    const fileSize = Number(maxFileSize) * 1024 * 1024;
    setDisables(true);
    if (isValidFileFormat) {
      if (uploadedFileSize < fileSize) {
        setFileName(files)
        setFormData({ ...formData, document: true });
        setErrors({ ...errors, [name]: "" });
      } else {
        // setDisables(false);
        setErrors({
          ...errors,
          [name]: `File size must below ${
            fileSize / 1048576
          } MB!. You file size is : ${Math.round(uploadedFileSize / 1024)}KB`,
        });
      }
    } else {
      setErrors({ ...errors, [name]: DROPZONE_MESSAGES.ZIP_INVALID });
      setShowformg(false);
    }
  };
  const handleClick = (event, i) => {
    hiddenFileInput.current.click(event);
    setfileIndex(i);
  };
  const handleRemoveImageClick = () => {
    setFormData({...formData, document: false})
    setFileName('');
    hiddenFileInput.current.value = null;
  };
  useEffect(() => {
    if (fraudToolUploadResponse && fraudToolUploadResponse.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        fraudToolUploadResponse && fraudToolUploadResponse.message,
        'success'
      )
      const params = {
        limit: 20,
        page: 1,
      };
      getFraudToolListDispatch(params)
      clearFileUploadDispatch()
      setFormData({
        clientId: "",
        api_name: "",
        document: false,
        documentLink: "",
        tag: "",
      })
      setFileName('');
      setDocCategory('');
    } else if (fraudToolUploadResponse && fraudToolUploadResponse.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        fraudToolUploadResponse && fraudToolUploadResponse.message,
        '',
        'Try again',
        '',
        () => { }
      )
     clearFileUploadDispatch()
    }
  }, [fraudToolUploadResponse])
  return (
    <>
      <div className={`card`}>
        <div className="card-body py-3 m-8">
          {Role === "Admin" && (
            <div className="row mb-8">
              <div className="col-md-3 required">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Client:
                </label>
              </div>
              <div className="col-md-6">
                <select
                  name="clientId"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={formData.clientId || ""}
                >
                  <option value="">Select...</option>
                  {clientList?.map((ele) => (
                    <option value={ele._id} key={ele._id}>
                      {ele.company}
                    </option>
                  ))}
                </select>
                {
                  <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                    <span role="alert text-danger">{errors.clientId}</span>
                  </div>
                }
              </div>
            </div>
          )}
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Category:
              </label>
            </div>
            <div className="col-md-6">
              <select
                name="operationStatus"
                className="form-select form-select-solid"
                data-control="select"
                data-placeholder="Select an option"
                data-allow-clear="true"
                onChange={(e) => setDocCategory(e.target.value)}
                value={docCategory || ""}
              >
                <option value="">Select...</option>
                {category.map((item) => (
                  <option value={item.key} key={item.key}>
                    {item.name}
                  </option>
                ))}
              </select>
              {
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">{errors.api_name}</span>
                </div>
              }
            </div>
          </div>

          {docCategory && (
            <div className="row mb-8">
              <div className="col-md-3 required">
                <label className="font-size-xs font-weight-bold mb-3  form-label">
                  Document:
                </label>
              </div>
              <div className="col-md-6">
                <select
                  name="api_name"
                  className="form-select form-select-solid"
                  data-control="select"
                  data-placeholder="Select an option"
                  data-allow-clear="true"
                  onChange={(e) => handleChanges(e)}
                  value={formData.api_name || ""}
                >
                  <option value="">Select...</option>
                  {document
                    .filter((item) => item.category === docCategory)
                    .map((item, index) => (
                      <option value={item.keyName} key={index}>
                        {item.name}
                      </option>
                    ))}
                </select>
                {
                  <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                    <span role="alert text-danger">{errors.api_name}</span>
                  </div>
                }
              </div>
            </div>
          )}
          {formData.api_name === "shop_lens" &&
           (
          <div className="row mb-8">
            <div className="col-md-3 required">
              <label className="font-size-xs font-weight-bold mb-3  form-label">
                Tag:
              </label>
            </div>
            <div className="col-md-6">
              <input
                placeholder="Tag"
                className={clsx(
                  "form-control form-control-lg form-control-solid",
                  {
                    "is-invalid":
                      formData.tag &&
                      errors.tag,
                  },
                  {
                    "is-valid":
                      formData.tag &&
                      !errors.tag,
                  }
                )}
                onChange={(e) => handleChanges(e)}
                onBlur={(e) => handleTrimWhiteSpace(e, setFormData)}
                type="text"
                name="tag"
                autoComplete="off"
                value={formData.tag || ""}
              />
            </div>
            {errors.tag && (
                <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                  <span role="alert text-danger">
                    {errors.tag}
                  </span>
                </div>
              )}
          </div>)}
          {formData.api_name === "shop_lens" &&
            (
              <div className="row mb-8">
                <div className="col-md-3 required">
                  <label className="font-size-xs font-weight-bold mb-3  form-label">
                    Shop Image:
                  </label>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-6 mb-3">
                  <div className="d-flex justify-content-start">
                    <input
                      type="file"
                      className="d-none"
                      name="document"
                      id="document"
                      accept=".zip"
                      multiple={false}
                      ref={hiddenFileInput}
                      onChange={(e) => {
                        FileChangeHandler(e);
                      }}
                    />{_.isEmpty(fileName && fileName.name) ?
                      <>
                        <a
                          className={
                            showformg
                              ? "btn btn-success btn-sm"
                              : "btn btn-sm btn-light-primary"
                          }
                          onClick={() => {
                            handleClick();
                          }}
                        >
                          {_.isEmpty(fileName && fileName.name) ? "Upload" : ""}
                        </a>
                      </> :
                      <>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div className="font-size-xs font-weight-bold text-success" style={{ marginRight: '8px' }}>
                            {fileName && fileName.name}
                          </div>
                          <button
                            type="button"
                            className="btn btn-sm btn-icon btn-light-danger"
                            onClick={handleRemoveImageClick}
                          >
                            <span className="svg-icon svg-icon-2">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor"></rect>
                                <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor"></rect>
                              </svg>
                            </span>
                          </button>
                        </div>
                      </>
                    }
                  </div>
                  {
                    <div className="fv-plugins-message-container text-danger mt-4 mb-4">
                      <span role="alert text-danger">{errors.document}</span>
                    </div>
                  }
                </div>
              </div>
            )}
          <div className="row mb-8">
            <div className="col-md-5"></div>
            <div className="col-md-2">
              <button
                className="btn btn-primary m-1 mt-8 font-5vw "
                onClick={handleSubmit}
              >
                Submit
              </button>
            </div>
          </div>
          <div className="col-md-4"></div>
        </div>
      </div>
      <Verification
        show={show}
        setShow={setShow}
        fraudToolResponseData={fraudToolResponseData}
        loading={loading}
        formData={formData}
        modalHeader={modalHeader}
        setFormData={setFormData}
      />
    </>
  );
};
const mapStateToProps = (state) => {
  const {
    clinetListStore,
    FraudAnalysisBulkUploadStore,
  } = state;
  return {
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
    fraudToolUploadResponse:
      FraudAnalysisBulkUploadStore && FraudAnalysisBulkUploadStore.FraudAnalysisBulkUploadRes ? FraudAnalysisBulkUploadStore.FraudAnalysisBulkUploadRes: "",
    loading: FraudAnalysisBulkUploadStore && FraudAnalysisBulkUploadStore.loading ? FraudAnalysisBulkUploadStore.loading: "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  clientIdDispatch: (data) =>
    dispatch(clientIdLIstActions.getclientIdList(data)),
  getFraudToolListDispatch: (params) =>
    dispatch(FraudAnalysisSummaryListActions.request(params)),
  fraudToolfileDispatch: (params) =>
    dispatch(FraudAnalysisBulkUploadActions.request(params)),
  clearFileUploadDispatch: () => dispatch(FraudAnalysisBulkUploadActions.clear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FraudAnalysisBatch);
