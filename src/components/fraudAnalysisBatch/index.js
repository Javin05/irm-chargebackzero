import React, { Fragment, useEffect, useState, } from "react";
import _ from "lodash";
import { FraudAnalysisExportActions, FraudAnalysisSummaryListActions } from "../../store/actions";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
import {
  getLocalStorage,
  removeLocalStorage,
} from "../../utils/helper";
import { SET_FILTER } from "../../utils/constants";
import { RISKSTATUS } from "../../utils/constants";
import Form from "./Form";
import moment from "moment";
import SearchList from "./searchList";
import { warningAlert } from "../../utils/alerts";
import ReactHTMLTableToExcel from "react-html-table-to-excel"

function FraudAnalysisBatchList(props) {
  const {
    className,
    getFraudToolSummaryListDispatch,
    getFraudAnalysisExportDispatch,
    clearExportDispatch,
    fraudToolSummaryList,
    loading,
    clinetIdLists,
    fraudToolAnaysisExport,
    exportLoading
  } = props;

  const didMount = React.useRef(false);
  const [limit, setLimit] = useState(20);
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1);
  const [Value, setValue] = useState(false);
  const paginationSearch = JSON.parse(getLocalStorage("WRM_OPS_SEARCH"));
  const Role = JSON.parse(getLocalStorage("ROLEDATA"));
  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER);
    const credBasedParams = {
      clientId: credBasedClientValue,
    };
    const params = {
      limit: limit,
      page: activePageNumber,
      ...credBasedParams,
      ...searchParams,
      ...paginationSearch,
    };
    const pickByParams = _.pickBy(params);
    getFraudToolSummaryListDispatch(pickByParams);
  }, [activePageNumber]);

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1;
    const params = {
      limit: limit,
      page: activePageNumber,
    };
    setActivePageNumber(pageNumber);
    getFraudToolSummaryListDispatch(params);
  };

  const totalPages =
    fraudToolSummaryList && fraudToolSummaryList.count
      ? Math.ceil(parseInt(fraudToolSummaryList && fraudToolSummaryList.count) / limit)
      : 1;


  useEffect(() => {
    return (
      setValue(true),
      setTimeout(() => {
        setValue(false);
      }, 1500)
    );
  }, []);


  const handleReset = () => {
    const params = {
      limit: limit,
      page: 1,
      ...searchParams,
    };
    getFraudToolSummaryListDispatch(params)
    setValue(true);
  };

  const handleExportReport = (id) => {
    getFraudAnalysisExportDispatch(id)
  }

  useEffect(() => {
    if (fraudToolAnaysisExport && fraudToolAnaysisExport.status === 'ok') {
      if (Array.isArray(fraudToolAnaysisExport && fraudToolAnaysisExport.data)) {
        const closeXlsx = document.getElementById('monitorExport')
        closeXlsx.click()
        clearExportDispatch()
      } else if (fraudToolAnaysisExport && fraudToolAnaysisExport.status === 'error') {
        warningAlert(
          'error',
          fraudToolAnaysisExport && fraudToolAnaysisExport.message,
          '',
          'Try again',
          '',
          () => { { } }
        )
        clearExportDispatch()
      }
    }
  }
    , [fraudToolAnaysisExport])

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    }
  });

  const exportReport =  fraudToolAnaysisExport && fraudToolAnaysisExport.data && fraudToolAnaysisExport.data.filter((item) => item.api_name === "Shop Lens" || item.api_name === "shop_lens")
  return (
    <>
    {exportReport ?
      <>
        <div
            type='button'
            className='d-none'
            data-target='#monitorExport'
          >
            <ReactHTMLTableToExcel
              id="monitorExport"
              className="download-table-xls-button"
              table="fraudAnalysisCsvModel"
              filename={`export-report`}
              sheet="tablexls"
            />
        </div>
        {/* csv Report */}
        <div className="table-responsive" style={{
          display: "none"
        }}>
          <table className="table" id="fraudAnalysisCsvModel">
            <thead>
            <tr className="fw-bolder fs-6 text-gray-800">
              <th>Tag</th>
              <th>Image Name</th>
              <th>Address</th>
              <th>Address Extracted</th>
              <th>Articles</th>
              <th>Board Standee Detected</th>
              <th>Contact Number</th>
              <th>Date Time</th>
              <th>GST Number</th>
              <th>Inventory Detected</th>
              <th>isMovable</th>
              <th>mccCode</th>
              <th>mccCode 1</th>
              <th>mccCodeDescription</th>
              <th>mccCodeDescription 1</th>
              <th>rankedIndustryDescriptions</th>
              <th>shopDetected</th>
              <th>shopName</th>
              <th>shopNameExtracted</th>
              <th>shopOwnerName</th>
              <th>shopOwnerNameExtracted</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(fraudToolAnaysisExport && fraudToolAnaysisExport.data) ?
                fraudToolAnaysisExport && fraudToolAnaysisExport.data.filter((item) => item.api_name = "Shop Lens")
                .map((item, it) => {
                  return (
                      <Fragment key={it}>
                        <tr>
                          <td>
                            {item.tag ? item.tag : 'No Data'}
                          </td>
                          <td>
                            {item.input_values && item.input_values.files ? item.input_values.files : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.address ? item.report_details.address : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.addressExtracted ? item.report_details.addressExtracted : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.articles ? item.report_details.articles : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.boardStandeeDetected ? item.report_details.boardStandeeDetected.toString() : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.contactNumber ? item.report_details.contactNumber : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.datetime ? item.report_details.datetime : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.gstNumber ? item.report_details.gstNumber : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.inventoryDetected ? item.report_details.inventoryDetected.toString() : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.isMovable ? item.report_details.isMovable : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.mccCode ? item.report_details.mccCode : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.mccCode1 ? item.report_details.mccCode1 : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.mccCodeDescription ? item.report_details.mccCodeDescription : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.mccCodeDescription1 ? item.report_details.mccCodeDescription1: 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.rankedIndustryDescriptions ? item.report_details.rankedIndustryDescriptions : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.shopDetected ? item.report_details.shopDetected.toString() : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.shopName ? item.report_details.shopName : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.shopNameExtracted ? item.report_details.shopNameExtracted : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.shopOwnerName ? item.report_details.shopOwnerName : 'No Data'}
                          </td>
                          <td>
                            {item && item.report_details && item.report_details.shopOwnerNameExtracted ? item.report_details.shopOwnerNameExtracted  : 'No Data'}
                          </td>
                        </tr>
                      </Fragment>
                  )
                })
                : null
            }
          </tbody>
          </table>
        </div>
      </> : null}
      <div className={`card ${className}`}>
        <div className="card-body py-3">
          <div className="row">
            <div className="d-flex justify-content-start col-md-12 col-lg-12">
              <div className="col-md-6 mt-1 ms-2">
                {fraudToolSummaryList && fraudToolSummaryList && fraudToolSummaryList.count && (
                  <span className="text-muted fw-bold d-flex fs-3 mt-2">
                    Total:
                    <span className="text-gray-700 fw-bolder text-hover-primary fs-3">
                      {fraudToolSummaryList && fraudToolSummaryList && fraudToolSummaryList.count}
                    </span>
                  </span>
                )}
              </div>
            </div>
            <div className="d-flex justify-content-center col-md-12 col-lg-12">
              <div className="col-md-8 mt-1 ms-2">
                <Form   />
              </div>
            </div>
            <div className="d-flex col-md-12 col-lg-12 justify-content-end my-auto mt-4">
              <ul className="nav nav-tabs nav-line-tabs fs-6">
                <li className="nav-item">
                  <SearchList Value={Value}
                    clientList={clinetIdLists && clinetIdLists.data && clinetIdLists.data.result ? clinetIdLists.data.result : []}
                  />
                </li>
              </ul>
            </div>
          </div>

          <br />
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead className="fw-bolder fs-8 text-gray-800">
                <tr>
                  <th>
                    <div className="d-flex">
                      <span>Action</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Fraud Summary Id</span>
                    </div>
                  </th>
                  {Role === "Admin" && (
                    <th>
                      <div className="d-flex">
                        <span>Client name</span>
                      </div>
                    </th>
                  )}

                  <th>
                    <div className="d-flex">
                      <span>API name</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Tag</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Completed status</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Export</span>
                    </div>
                  </th>
                  <th>
                    <div className="d-flex">
                      <span>Created at</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody className="fs-8">
                {!loading ? (
                  fraudToolSummaryList && fraudToolSummaryList.data ? (
                    fraudToolSummaryList.data && fraudToolSummaryList.data.result &&
                    fraudToolSummaryList.data.result.map((ele, i) => {
                      return (
                        <tr
                          key={"reef_" + i}
                          style={
                            i === 0
                              ? { borderColor: "black" }
                              : { borderColor: "white" }
                          }
                        >
                          <td className="min-width-150px text-center form-check form-check-custom form-check-primary form-check-solid">
                            <a
                              className="color-primary cursor-pointer"
                              // onClick={()=>handleSearch("","","","",riskoperationmgmtlist.caseId)}
                              onClick={() =>
                                window.open(
                                  `/fraudanalysis-batchupload/update/${ele._id}`,
                                  "_blank"
                                )
                              }
                              to={`/fraudanalysis-batchupload`}
                            >
                              <span>
                                <i
                                  style={{ color: "blue", fontSize: "20px" }}
                                  className="bi bi-eye-fill"
                                ></i>
                              </span>
                            </a>
                          </td>
                          <td className="ellipsis">
                            <>{ele.fraud_summ_id ? `${"FS-"}${ele.fraud_summ_id}`: "--"}</>
                          </td>
                          {Role === "Admin" && (
                            <td className="ellipsis">
                              <>
                                {ele.client_id ? ele.client_id.company : "--"}
                              </>
                            </td>
                          )}

                          <td className="ellipsis">
                            <>{ele.api_name ? ele.api_name : "--"}</>
                          </td>
                          <td className="ellipsis">
                            <>{ele.tag ? ele.tag : "--"}</>
                          </td>
                          <td className="ellipsis">
                            <>
                              <span
                                className={`badge ${
                                  RISKSTATUS[ele && ele.completed_status]
                                }`}
                              >
                                {ele.completed_status
                                  ? ele.completed_status
                                  : "--"}
                              </span>
                            </>
                            
                          </td>
                          <td className="ellipsis">
                            {ele.completed_status === "COMPLETED" && ele.api_name === "Shop Lens" ? (
                              <button className="btn btn-success btn-sm"
                                onClick={() => handleExportReport(ele._id)}
                                disabled={exportLoading}
                              >
                                {!exportLoading && 
                                  <span className='indicator-label'>
                                    <i className="bi bi-filetype-csv" />
                                    Download
                                  </span>
                                }
                                {exportLoading && (
                                  <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                            ) : ele.completed_status === "COMPLETED" && ele.api_name === "shop_lens" ? (
                              <button className="btn btn-success btn-sm"
                                onClick={() => handleExportReport(ele._id)}
                                disabled={exportLoading}
                              >
                                {!exportLoading && 
                                  <span className='indicator-label'>
                                    <i className="bi bi-filetype-csv" />
                                    Download
                                  </span>
                                }
                                {exportLoading && (
                                  <span className='indicator-progress text-primary' style={{ display: 'block' }}>
                                    Please wait...
                                    <span className='spinner-border spinner-border-sm align-middle ms-2' />
                                  </span>
                                )}
                              </button>
                            ) : (
                              <button className="btn btn-secondary btn-sm" disabled>
                                <span className='indicator-label'>
                                    <i className="bi bi-filetype-csv" />
                                    Download
                                  </span>
                              </button>
                            )}
                          </td>
                          <td className="ellipsis">
                          {ele.createdAt ? moment(ele.createdAt).startOf('minute').fromNow() : "--"}
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr className="text-center py-3">
                      <td colSpan="100%">
                        No record(s) found. Click below button to reset
                        <br />
                        {
                          <div className="my-auto">
                            <button
                              onClick={() => handleReset()}
                              type="button"
                              className="btn btn-lg btn-primary btn-responsive font-6vw me-3 pull-right"
                            >
                              Reset
                            </button>
                          </div>
                        }
                      </td>
                    </tr>
                  )
                ) : (
                  <tr>
                    <td colSpan="100%" className="text-center">
                      <div
                        className="spinner-border text-primary m-5"
                        role="status"
                      />
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={4}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const {
    clinetListStore,
    FraudAnalysisSummaryListStore,
    FraudAnalysisExportStore
  } = state;
  return {
    fraudToolSummaryList:
      FraudAnalysisSummaryListStore && FraudAnalysisSummaryListStore.fraudAnalysisSummaryData
        ? FraudAnalysisSummaryListStore.fraudAnalysisSummaryData
        : "",
    loading:
        FraudAnalysisSummaryListStore && FraudAnalysisSummaryListStore.loading
          ? FraudAnalysisSummaryListStore.loading
          : "",
    fraudToolAnaysisExport:
          FraudAnalysisExportStore && FraudAnalysisExportStore.fraudAnalysisExportData
            ? FraudAnalysisExportStore.fraudAnalysisExportData
            : "",
    exportLoading:
            FraudAnalysisExportStore && FraudAnalysisExportStore.loading
              ? FraudAnalysisExportStore.loading
              : "",
    
    clinetIdLists:
      clinetListStore && clinetListStore.clinetIdLists
        ? clinetListStore.clinetIdLists
        : "",
  };
};

const mapDispatchToProps = (dispatch) => ({
  getFraudToolSummaryListDispatch: (params) =>
    dispatch(FraudAnalysisSummaryListActions.request(params)),
  getFraudAnalysisExportDispatch: (params) =>
    dispatch(FraudAnalysisExportActions.request(params)),
  clearExportDispatch: (params) =>
    dispatch(FraudAnalysisExportActions.clear()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FraudAnalysisBatchList);
