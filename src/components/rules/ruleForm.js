import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Link, useLocation, useHistory } from 'react-router-dom'
import 'react-phone-input-2/lib/style.css'
import { inputFieldActions, rulesActions, RulesGetIdActions, updateRulesActions, queuesActions } from '../../store/actions'
import { userRoleValidation } from './validation'
import { setRuleData } from './formData'
import { STATUS_RESPONSE } from '../../utils/constants'
import { warningAlert, confirmationAlert } from "../../utils/alerts"
import ReactSelect from "../../theme/layout/components/ReactSelect"
import color from "../../utils/colors"
import { getLocalStorage } from '../../utils/helper'

function RuleForm (props) {
  const {
    postRulesListDispatch,
    getInputListDispatch,
    className,
    ruleGroup,
    inputfields,
    loading,
    addRules,
    PostclearRules,
    getRulesIdDispatch,
    clearRulesIdDetailsDispatch,
    loadingGR,
    RulesIdDetail,
    statusEA,
    loadingARG,
    loadingUR,
    updateRulesResponce,
    messageUR,
    statusUR,
    clearupdateRulesDispatch,
    updateRulesDispatch,
    queuesLists,
    getQueueslistDispatch,
    messageAR,
    statusAR
  } = props
  const pathName = useLocation().pathname
  const url = pathName && pathName.split('/')
  const currentId = url && url[3]
  const queue = url && url[2]
  const [limit, setLimit] = useState(25)
  const [logic, setLogic] = useState([]);
  const [editMode, setEditMode] = useState(false)
  const [theArray, setTheArray] = useState([]);
  const [errors, setErrors] = useState()
  const [SelectedQueueOption, setSelectedQueueOption] = useState('')
  const [QueueOption, setQueue] = useState()
  const [queueIdData, setQueueIdData] = useState()
  const [queueValue, setQueueValue] = useState()
  const [matchValue, setMatchValue] = useState()
  const [currentInputIndex, setCurrentInputIndex] = useState()

  const [inputValue, setinputValue] = useState(
    {
      inputvalueData: ''
    }
  )

  // const [inputValue, setinputValue] = useState(
  //   {
  //     customRules: [
  //       {
  //         ruleValue: '',
  //       }
  //     ]
  //   }
  // )

  const history = useHistory()
  const Role = JSON.parse(getLocalStorage("ROLEDATA"))

  const [formData, setFormData] = useState({
    ruleGroupName: '',
    status: 'INACTIVE',
    trigger:'POST VERIFICATION',
    condition: '',
    description: '',
    action: '',
    displayRuleLogic: '',
    queueId: queueIdData,
    customRules: [
      {
        ruleField: '',
        ruleOperator: '',
        ruleValue: '',
        notes: '',
        ruleMatchField: ''
      }
    ]
  })


  // const handleAddRole = (currentIndex) => {
  //   setFormData((values) => ({
  //     ...values,
  //     customRules: [
  //       ...formData.customRules,
  //       {
  //         ruleField: '',
  //         ruleOperator: '',
  //         ruleValue: '',
  //         notes: ''
  //       }
  //     ]
  //   }))
  // }

  useEffect(() => {
    if (currentId) {
      getRulesIdDispatch(currentId)
      setEditMode(true)
    } else {
      setEditMode(false)
    }
  }, [currentId])

  useEffect(() => {
    const params = {
      limit: limit,
      page: 1
    }
    getInputListDispatch(params)
    getQueueslistDispatch()
  }, [])
  // var tempRule;

  const handleAddRole = (currentIndex) => {
    setFormData((values) => ({
      ...values,
      customRules: [
        ...formData.customRules,
        {
          ruleField: '',
          ruleOperator: '',
          ruleValue: '',
          notes: ''
        }
      ]
    }))
  }


  // const inputhandleChange = (e, currentIndex) => {
  //   setpayloadData((values) => ({
  //     ...values,
  //     ...formData,
  //     customRules: formData.customRules.map((obj, i) =>
  //       i === currentIndex ?
  //         Object.assign(obj, {
  //           ...obj,
  //           // [e.target.name]: e.target.value,
  //           ruleValue: e.target.value
  //         })
  //         : obj
  //     )
  //   }))

  //   setinputValue({ ...inputValue, [e.target.name]: e.target.value })
  // }

  const handleChange = (e, currentIndex) => {
    setMatchValue(e.target.value)
    setCurrentInputIndex(currentIndex)
    setErrors({ ...errors, [e.target.name]: '' })
    if (
      e.target.name === 'ruleField' || e.target.name === "ruleOperator" ||
      e.target.name === 'ruleValue' || e.target.name === "ruleGroupName" ||
      e.target.name === "ruleMatchField"
    ) {
      setFormData((values) => ({
        ...values, [e.target.name]: e.target.value,
        customRules: formData.customRules.map((obj, i) =>
          i === currentIndex ?
            Object.assign(obj, {
              ...obj,
              [e.target.name]: e.target.value,
            })
            : obj
        )
      }))
    }
    else {
      setFormData({ ...formData, [e.target.name]: e.target.value })
    }
  }

  const rulesField = formData.ruleField ? formData.ruleField : ''
  const rulesOperator = formData.ruleOperator ? formData.ruleOperator : ''
  const rulesValue = formData.ruleValue ? formData.ruleValue : ''
  const rulescondition = formData.condition ? formData.condition : ''
  const rulessecondruleField = formData.secondruleField ? formData.secondruleField : ''
  const rulessecondruleOperator = formData.secondruleOperator ? formData.secondruleOperator : ''
  const rulessecondruleValue = formData.secondruleValue ? formData.secondruleValue : ''

  const tempRule = " WHERE(" + rulesField + " " + rulesOperator + " " + rulesValue +
    " " + rulescondition + " " + rulessecondruleField + " " + rulessecondruleOperator + " " + rulessecondruleValue + ")"

  const showRule = _.filter(formData.customRules, (x, i) => {
    return x
  })

  const handleReset = () => {
    setFormData({
      ruleGroupName: '',
      status: '',
      condition: '',
      description: '',
      action: '',
      displayRuleLogic: '',
      queueId:queueIdData,
      customRules: [
        {
          ruleField: '',
          ruleOperator: '',
          ruleValue: '',
          notes: ''
        }
      ]
    })
    const params = {
      limit: 25,
      page: 1
    }
  }

  const handleSubmit = (e) => {
   console.log(formData)
    // const formdataValue = {
    //   ...formData,
    //   customRules: formData.customRules && formData.customRules.map((obj, i) =>
    //     obj ?
    //       Object.assign(obj, {
    //         ruleValue: inputValue.inputvalueData,
    //       })
    //       : obj
    //     // console.log('oooooooooooooo',obj)
    //   )

    // }
    const errors = userRoleValidation(formData, setErrors)
    console.log(errors)
    if (_.isEmpty(errors)) {
      if (editMode) {
        updateRulesDispatch(currentId, formData)
      } else {
        postRulesListDispatch(formData)

      }

    }
  }

  const onConfirm = () => {
    PostclearRules()
    setFormData({
      ruleGroupName: '',
      status: '',
      condition: '',
      description: '',
      action: '',
      displayRuleLogic: '',
      customRules: [
        {
          ruleField: '',
          ruleOperator: '',
          ruleValue: '',
          notes: '',
        }
      ]
    })
    history.push('/rules')
  }
  const clear = () => {
    setFormData({
      ruleGroupName: '',
      status: '',
      condition: '',
      description: '',
      action: '',
      displayRuleLogic: '',
      trigger:'',
      customRules: [
        {
          ruleField: '',
          ruleOperator: '',
          ruleValue: '',
          notes: ''
        }
      ]
    })
  }

  useEffect(() => {
    if (statusEA === STATUS_RESPONSE.SUCCESS_MSG) {
      const setData = setRuleData(RulesIdDetail)
      if (RulesIdDetail && RulesIdDetail._id) {
        const queue = getDefaultOption(queueNames)
        if (!_.isEmpty(RulesIdDetail.queueId)) {
          const selOption = _.filter(queue, function (x) {
            if (_.includes(RulesIdDetail.queueId, x.value)) {
              setQueueValue(x.label)
              return x
            }
          })
          setSelectedQueueOption(selOption)
        }
      }

      setFormData(setData)
      clearRulesIdDetailsDispatch()
    } else {
      clearRulesIdDetailsDispatch()
    }
  }, [statusEA])

  useEffect(() => {
    if (statusAR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        messageAR,
        'success',
        'Back to Rules',
        'Ok',
        () => { onConfirm() },
        () => { clear() }
      )
      PostclearRules()
    } else if (statusAR === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        messageAR,
        '',
        'Try again',
        '',
        () => { clear() }
      )
      PostclearRules()
    }
  }, [statusAR, messageAR])

  const onConfirmEdit = () => {
    history.push('/rules')
    setFormData({
      ruleGroupName: '',
      status: '',
      condition: '',
      description: '',
      action: '',
      displayRuleLogic: '',
      customRules: [
        {
          ruleField: '',
          ruleOperator: '',
          ruleValue: '',
          notes: ''
        }
      ]
    })
  }

  useEffect(() => {
    if (statusUR === STATUS_RESPONSE.SUCCESS_MSG) {
      confirmationAlert(
        'success',
        messageUR,
        'success',
        'Back to Rules',
        'Ok',
        () => { onConfirmEdit() },
        () => { onConfirmEdit() }
      )
      clearupdateRulesDispatch()
    } else if (statusUR === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        messageUR,
        '',
        'Try again',
        '',
        () => { onConfirmEdit() }
      )
      clearupdateRulesDispatch()
    }
  }, [statusUR])


  const handleRemoveClick = currentIndex => {
    const removeObj = _.filter(formData.customRules, (x, i) => {
      if (currentIndex === i) { return null } else {
        return x
      }
    })
    setFormData((values) => ({
      ...values,
      customRules: removeObj
    }))
  }


  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  const handleChangeQueue = selectedOption => {
    if (selectedOption !== null) {
      setSelectedQueueOption(selectedOption)
      setFormData(values => ({ ...values, queueId: selectedOption.value }))
      setQueueIdData(selectedOption.value)
      setQueueValue(selectedOption.label)
    }
  }
  const queueNames = queuesLists && queuesLists.data
  useEffect(() => {
    const Queue = getDefaultOption(queueNames)
    setQueue(Queue)
    if (!_.isEmpty(formData.queueId)) {
      const selOption = _.filter(Queue, function (x) { if (_.includes(formData.queueId._id, x.value)) { return x } })
      setSelectedQueueOption(selOption)
    }
  }, [queueNames])

  const getDefaultOption = (queueNames) => {
    const defaultOptions = []
    for (const item in queueNames) {
      defaultOptions.push({ label: queueNames[item].queueName, value: queueNames[item]._id })
    }
    return defaultOptions
  }
  console.log(queueValue)
  return (
    <>
      <div style={{ backgroundColor: '#d4e3f6' }} className={`card ${className}`}>
        {
          loadingGR ? (
            <div className='d-flex justify-content-center py-5'>
              <div className='spinner-border text-primary m-5' role='status' />
            </div>
          ) : (
            <>
              <div className='row mt-10'>
                <div className='col-lg-4 '>
                   <div className='col-md-10 mx-10 '>
                   <label className='col-form-label col-md-8 text-lg-start required'> Select Queues:</label>
                      <ReactSelect
                      styles={customStyles}
                      isMulti={false}
                      name='AppUserId'
                      className='select2'
                      classNamePrefix='select'
                      handleChangeReactSelect={handleChangeQueue}
                      options={QueueOption}
                      value={SelectedQueueOption}
                      isDisabled={currentId ? true : false}
                    />
                  </div>
                </div> 
                <div className='col-lg-4 '>
                  <div className='col-md-10 mx-10 '>
                    <label className='col-form-label col-md-8 text-lg-start required'> Rule Name:</label>
                    <input
                      onChange={(e) => handleChange(e)}
                      value={formData.ruleGroupName || ''}
                      type="text" name="ruleGroupName"
                      className="form-control  form-control-solid"
                      placeholder="Rule name" />
                    {errors && errors.ruleGroupName && (
                      <div className='fv-plugins-message-container text-danger'>
                        <div className='fv-help-block'>
                          <span role='alert'>{errors && errors.ruleGroupName}</span>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className='col-lg-4 '>
                  <div className='col-md-10 mx-10  '>
                    <label className='col-form-label col-md-8  text-lg-start required'>Rule Description:</label>
                    <input onChange={(e) => handleChange(e)} type="text" name="description" className="form-control form-control-solid"
                      placeholder="Rule description"
                      value={formData.description || ''}
                    />
                    {errors && errors.description && (
                      <div className='fv-plugins-message-container text-danger'>
                        <div className='fv-help-block'>
                          <span role='alert'>{errors && errors.description}</span>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                {Role ==="Admin" && 
                <div className='col-lg-4 '>
                  <div className='col-md-10  mx-10'>
                    <label className='col-form-label col-md-4  text-lg-start required'>
                      Status :
                    </label>
                   
                    <div className='col-md-12 '>
                      <select
                        name="status"
                        value={formData.status || ''}
                        className='form-control form-control-solid form-select'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChange(e)}
                      >
                        <option value=''>Select</option>
                        <option value='ACTIVE'>ACTIVE</option>
                        <option value='INACTIVE'>INACTIVE</option>
                      </select>
                      {errors && errors.status && (
                        <div className='fv-plugins-message-container text-danger'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.status}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
               }
                <div className='col-lg-4 '>
                  <div className='col-md-10  mx-10'>
                    <label className='col-form-label col-md-4  text-lg-start required'>
                      Condition :
                    </label>
                    <div className='col-md-12 '>
                      <select
                        name="condition"
                        value={formData.condition || ''}
                        className='form-control form-control-solid form-select'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChange(e)}
                      ><option value=''>Select</option>
                        <option value='AND'>AND</option>
                        <option value='OR'>OR</option>
                      </select>
                      {errors && errors.condition && (
                        <div className='fv-plugins-message-container text-danger'>
                          <div className='fv-help-block'>
                            <span role='alert'>{errors && errors.condition}</span>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                      <div className='col-lg-4 '>
                  <div className='col-md-10  mx-10'>
                    <label className='col-form-label col-md-4  text-lg-start required'>
                      Trigger :
                    </label>
                    <div className='col-md-12 '>
                      <select
                        name="trigger"
                        value={formData.trigger || ''}
                        className='form-control form-control-solid form-select'
                        data-control='select'
                        data-placeholder='Select an option'
                        data-allow-clear='true'
                        onChange={(e) => handleChange(e)}
                      ><option value=''>Select</option>
                        <option value='POST VERIFICATION'>POST VERIFICATION</option>
                        <option value='PRE VERIFICATION'>PRE VERIFICATION</option>
                      </select>

                    </div>
                  </div>
                </div>
              </div>
              {
               formData.trigger ==="PRE VERIFICATION"?  formData.customRules && formData.customRules.map((item, i) => (
                  <div className='row mt-10' key={i}>
                    <div className='col-lg-3 '>
                      <div className='col-md-10 mx-10 '>
                        <label className='col-form-label col-md-8 text-lg-start'> Input Fields:</label>
                        <div className='col-md-12 '>
                          <select
                            name="ruleField"
                            value={item.ruleField || ''}
                            className='form-control form-control-solid form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e, i)}
                          >
                            {
                              queueValue === ('SUSPECT_TXN_Q') ? (
                                <>
                                  <option value=''>Select</option>
                                  <option value='Email'>Email</option>
                                  <option value='Phone'>Phone</option>
                                </>
                              ) :
                                queueValue === ('AML_Q') ?
                                  (
                                    <>
                                      <option value=''>Select</option>
                                      <option value='No of Transaction Days'>No of Transaction Days</option>
                                    </>
                                  ) :queueValue === ('MERCHANT_ONBOARDING_Q')?(
                                      <>
                                       <option value=''>Select</option>
                                      <option value='customer_phone'>Customer Phone</option>
                                      <option value='customer_email'>Customer Email</option>
                                      <option value='business_phone'>Business Phone</option>
                                      <option value='business_email'>Business Email</option>                                        
                                      </>
                                  ):queueValue === ('KYC_Q') ? (
                                    <>
                                      <option value=''>Select</option>
                                      <option value='mobile'>Mobile</option>
                                      <option value='email'>Email</option>                                   </>
                                  ):queueValue ===("WEB_RISK_MONITORING_Q")?
                                  <>
                                      <option value=''>Select</option>
                                      <option value='upi_handle'>UPI handle</option>                                    
                                      <option value='website'>website</option>                                      
                                      
                                  </>:queueValue===("ONGOING_MONITORING_Q")?<>
                                  <option value=''>Select</option>
                                  <option value='website'>website</option> 
                                  </>:
                                  (
                                    <>
                                      <option value=''>Select</option>
                                      <option value='Email'>Email</option>
                                      <option value='Phone'>Phone</option>
                                      <option value='Website'>Website</option>
                                    </>
                                  )
                            }
                          </select>
                          {errors && errors.ruleField && (
                            <div className='fv-plugins-message-container text-danger'>
                              <div className='fv-help-block'>
                                <span role='alert'>{errors && errors.ruleField}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className='col-lg-3 '>
                      <div className='col-md-10 mx-10 '>
                        <label className='col-form-label  col-md-8 text-lg-start'>
                          Operators :
                        </label>
                        <div className='col-lg-12 '>
                          <select
                            name="ruleOperator"
                            // value={item.ruleOperator || ''}
                            value={currentInputIndex === i ? item[i] && item[i].ruleOperator : item.ruleOperator}
                            className='form-control form-control-solid form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e, i)}
                          >
                            {queueValue === ('KYC_Q') ? (
                            <>
                           { item.ruleField === "email" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='ends_with'>ends with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>}
                            { item.ruleField === "mobile" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='starts_with'>start with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>
                             
                            ):queueValue === ('MERCHANT_ONBOARDING_Q') ? (
                            <>
                            { item.ruleField === "customer_phone" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='starts_with'>start with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            { (item.ruleField === "customer_email"||item.ruleField === "business_email" )&& 
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='ends_with'>ends with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            { item.ruleField === "business_phone" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='starts_with'>start with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            
                            
                            { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>
                             
                            ):queueValue ===("WEB_RISK_MONITORING_Q")?
                            <>
                             {(  item.ruleField === "upi_handle" ||item.ruleField === "website")&& 
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>                           
                            </>}
                             { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>
                            :queueValue===("ONGOING_MONITORING_Q")? 
                            <>
                             {item.ruleField === "website"&& 
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>                           
                            </>}
                            </> :
                            <>
                            <option value=''>Select</option>
                            <option value='Matches'>Matches</option>
                            <option value='Does not match'>Does not match</option>
                            <option value='Starts with'>Starts with</option>
                            <option value='Ends with'>Ends with</option>
                            <option value='Greater than or equal to'>Greater than or equal to</option>
                            <option value='Not equal to'>Not equal to</option>
                            <option value='Greater than'>Greater than</option>
                            <option value='Less than'>Less than</option>
                            <option value='Less than or equal to'>Less than or equal to </option>
                            </>
                            }
                            
                          </select>
                          {errors && errors.ruleOperator && (
                            <div className='fv-plugins-message-container text-danger'>
                              <div className='fv-help-block'>
                                <span role='alert'>{errors && errors.ruleOperator}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    {
                      (
                        formData && formData.customRules[i].ruleField === 'Billing Country' ||
                        formData && formData.customRules[i].ruleField === 'Shipping Country' ||
                        formData && formData.customRules[i].ruleField === 'AVS' ||
                        formData && formData.customRules[i].ruleField === 'BillingPhoneCountryCode' ||
                        formData && formData.customRules[i].ruleField === 'ShippingPhoneCountryCode') &&
                        (formData && formData.customRules[i].ruleOperator === 'Matches' || formData.customRules[i].ruleOperator === 'Does not match'
                        )
                        ? (
                          <div className='col-lg-3 '>
                            <div className='col-md-12 mx-10 '>
                              <label className='col-form-label  col-md-4 text-lg-start'>
                                DropDown :
                              </label>
                              <div className='col-lg-12 '>
                                <select
                                  name="ruleMatchField"
                                  value={currentInputIndex === i ? item[i] && item[i].ruleMatchField : item.ruleMatchField}
                                  // value={item.ruleMatchField || ''}
                                  className='form-control form-control-solid form-select'
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e, i)}
                                >
                                  <option value=''>Select</option>
                                  {
                                    (formData && formData.customRules[i].ruleField === 'Billing Country') ? (
                                      <>
                                        <option value='Does not match'>Shipping Address Country</option>
                                        <option value='IPAddressCountry'>IP Address Country</option>
                                      </>
                                    ) :
                                      (formData && formData.customRules[i].ruleField === 'Shipping Country') ? (
                                        <>
                                          <option value='Billing Address Country'>Billing Address Country</option>
                                          <option value='IPAddressCountry'>IP Address Country</option>
                                        </>
                                      ) : (formData && formData.customRules[i].ruleField === 'AVS') ? (
                                        <>
                                          <option value='A'>A</option>
                                          <option value='B'>B</option>
                                          <option value='C'>C</option>
                                        </>
                                      ) : (formData && formData.customRules[i].ruleField === 'BillingPhoneCountryCode') ? (
                                        <>
                                          <option value='Shipping Phone Country Code'>Shipping Phone Country Code</option>
                                          <option value='Enter a Phone Country Code'>Enter a Phone Country Code</option>
                                        </>
                                      ) :
                                        (formData && formData.customRules[i].ruleField === 'ShippingPhoneCountryCode') ? (
                                          <>
                                            <option value='Billng Phone Country Code'>Billng Phone Country Code</option>
                                            <option value='Enter a Phone Country Code'>Enter a Phone Country Code</option>
                                          </>
                                        ) : null
                                  }
                                </select>
                                {errors && errors.ruleMatchField && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <div className='fv-help-block'>
                                      <span role='alert'>{errors && errors.ruleMatchField}</span>
                                    </div>
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>

                        )
                        : (
                          null
                        )
                    }
                    {
                      (formData && formData.customRules[i].ruleMatchField === 'Shipping Phone Country Code' ||
                        formData && formData.customRules[i].ruleMatchField === 'Billng Phone Country Code') ? (
                        null
                      ) :
                        (
                          <div className='col-lg-3 '>
                      <div className='col-md-10 mx-10 '>
                        <label className='col-form-label  col-md-8 text-lg-start'>
                          Values :
                        </label>
                        <div className='col-lg-12 '>
                           { queueValue ==="KYC_Q" && item.ruleOperator == "starts_with" || queueValue ==="KYC_Q" && item.ruleOperator == "ends_with"|| queueValue ==="KYC_Q" && item.ruleOperator =="is_equal_to" ||queueValue === 'MERCHANT_ONBOARDING_Q' && item.ruleOperator == "starts_with" ||queueValue ==="MERCHANT_ONBOARDING_Q" && item.ruleOperator =="is_equal_to"|| queueValue ==="MERCHANT_ONBOARDING_Q" && item.ruleOperator == "ends_with" ?
                             
                           <input          
                            value={currentInputIndex === i ? item[i] && item[i].ruleValue : item.ruleValue}                            
                            className="form-control  form-control-solid"
                            placeholder="Rule value"
                            name="ruleValue" 
                            type="text"
                            onChange={(e) => handleChange(e,i)
                            
                          }
                          
                            /> :
                            
                            
                            <select
                            name="ruleValue"
                            // value={item.ruleOperator || ''}
                            value={currentInputIndex === i ? item[i] && item[i].ruleValue : item.ruleValue}
                            className='form-control form-control-solid form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e, i)}
                          >
                            {queueValue === ('KYC_Q') ? (
                            <>
                           { item.ruleOperator == "is_in" && 
                              <>
                            <option value=''>Select </option>
                            <option value='blacklist'>Blacklist </option>  
                            <option value='whitelist'>Whitelist</option>
                            </> }
                            { item.ruleOperator == "" && 
                              <>
                            <option value=''>Select </option>
                            </> }
                            </>
                            ):queueValue===("ONGOING_MONITORING_Q")?<> { item.ruleOperator == "is_in" && 
                              <>
                            <option value=''>Select </option>
                            <option value='blacklist'>Blacklist </option>  
                            <option value='whitelist'>Whitelist</option>
                            </> }
                            </>:queueValue === ('MERCHANT_ONBOARDING_Q')?(
                             <>
                            { item.ruleOperator == "is_in" && 
                              <>
                            <option value=''>Select </option>
                            <option value='blacklist'>blacklist </option>  
                            <option value='whitelist'>whitelist</option>
                            </> 
                            }
                            {(item.ruleField === ""||item.ruleOperator==="") &&
                           <option value=''>Select </option>
                            }
                            {(item.ruleField === ""||item.ruleOperator==="") &&
                           <option value=''>Select </option>
                            }
                             </>
                            ):queueValue ===("WEB_RISK_MONITORING_Q")?
                            <>
                            { (item.ruleField ==="website"||item.ruleField ==="upi_handle") && 
                            <>
                                <option value=''>Select</option>
                                <option value='blacklist'>blacklist</option>
                                <option value='whitelist'>whitelist</option>
                            </>
                            }
                             {(item.ruleField === ""||item.ruleOperator==="") &&
                              <option value=''>Select </option>
                            }
                            </>
                            :
                            <>
                            <option value=''>Select</option>
                            {/* <option value='Matches'>Matches</option>
                            <option value='Does not match'>Does not match</option>
                            <option value='Starts with'>Starts with</option>
                            <option value='Ends with'>Ends with</option>
                            <option value='Greater than or equal to'>Greater than or equal to</option>
                            <option value='Not equal to'>Not equal to</option>
                            <option value='Greater than'>Greater than</option>
                            <option value='Less than'>Less than</option>
                            <option value='Less than or equal to'>Less than or equal to </option> */}
                            </>
                            }
                            
                          </select>}
                          
                          {errors && errors.ruleValue && (
                            <div className='fv-plugins-message-container text-danger'>
                              <div className='fv-help-block'>
                                <span role='alert'>{errors && errors.ruleValue}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                        )
                    }
                    <div className='col-lg-3 '>
                      {
                        i === 0 ? (
                          <div className='col-md-10 mx-10 '>
                           {item.ruleValue.length > 0 &&
                           <button
                              type='button'
                              className='btn btn-sm btn-light-primary mt-14 text-lg-start '
                              onClick={() => handleAddRole(i)}
                            >
                              <span className='indicator-label'>Add Rule</span>
                            </button>
                            }
                          </div>
                        ) : (
                          <div className='col-md-5 mx-5 d-flex  '>
                            <button
                              type='button'
                              className='btn btn-sm btn-warning mt-14 text-lg-start mr-5'
                              onClick={() => handleRemoveClick(i)}
                            >
                              <span className='indicator-label'>Remove</span>
                            </button>
                              <button
                              type='button'
                              className='btn btn-sm btn-light-primary mt-14 text-lg-start '
                              onClick={() => handleAddRole(i)}
                            >
                              <span className='indicator-label'>Add</span>
                            </button>
                          </div>
                        )
                      }
                    </div>
                  </div>
                )): formData.customRules && formData.customRules.map((item, i) => (
                  <div className='row mt-10' key={i}>
                    <div className='col-lg-3 '>
                      <div className='col-md-10 mx-10 '>
                        <label className='col-form-label col-md-8 text-lg-start'> Input Fields:</label>
                        <div className='col-md-12 '>
                          <select
                            name="ruleField"
                            value={item.ruleField || ''}
                            className='form-control form-control-solid form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e, i)}
                          >
                            {
                              queueValue === ('SUSPECT_TXN_Q') ? (
                                <>
                                  <option value=''>Select</option>
                                  <option value='Email'>Email</option>
                                  <option value='Phone'>Phone</option>
                                  <option value='Ip Address'>Ip Address</option>
                                  <option value='Payment Method'>Payment Method</option>
                                  <option value='Billing Country'>Billing Country</option>
                                  <option value='Shipping Country'>Shipping Country</option>
                                  <option value='BillingPhoneCountryCode'>Billing Phone Country Code</option>
                                  <option value='ShippingPhoneCountryCode'>Shipping Phone Country Code</option>
                                  <option value='AVS'>AVS</option>
                                </>
                              ) :
                                queueValue === ('AML_Q') ?
                                  (
                                    <>
                                      <option value=''>Select</option>
                                      <option value='No of Transaction Days'>No of Transaction Days</option>
                                      <option value='Transaction Volume'>Transaction Volume</option>
                                      <option value='Transaction Amount'>Transaction Amount</option>
                                      <option value='Shipping Country'>Shipping Country</option>
                                      <option value='Billing Country'>Billing Country</option>
                                    </>
                                  ) :queueValue === ('MERCHANT_ONBOARDING_Q')?(
                                      <>
                                       <option value=''>Select</option>
                                      <option value='customer_name'>Customer Name</option>
                                      <option value='customer_phone'>Customer Phone</option>
                                      <option value='customer_email'>Customer Email</option>
                                      <option value='business_phone'>Business Phone</option>
                                      <option value='business_email'>Business Email</option>
                                      <option value='individual_address'>Individual Address</option>
                                      <option value='business_address'>Business Address(City/PIN/State)</option>
                                      <option value='type_of_ownership'>Type of ownership</option>
                                      <option value='business_or_legal_name'>Business/Legal Name</option>
                                      <option value='ip_address'>IP Adress</option> 
                                      <option value='business_sub_category'>Business Category</option>  
                                      <option value='average_transaction_amount'>Average Transaction Amount</option> 
                                      <option value='annual_turnover'>Annual Turnover</option>   
                                      <option value='business_age'>Business age</option>
                                      <option value='number_of_employees'>Number of Employees</option>                                         
                                      </>
                                  ):queueValue === ('KYC_Q') ? (
                                    <>
                                      <option value=''>Select</option>
                                      <option value='mobile'>Mobile</option>
                                      <option value='email'>Email</option>
                                      <option value='document_rejected_rate'>Document rejected rate</option>
                                      <option value='document_approval_rate'>Document approval rate</option>
                                      <option value='adharcard'>Adharcard</option>
                                      <option value='bank_account'>Bank Account</option>
                                      <option value='account_name_on_cancelled_cheque'>Account name on cancelled cheque</option>
                                      <option value='account_number_on_cancelled_cheque'>Account number on cancelled cheque</option>
                                      <option value='IFSC_code_on_cancelled_cheque'>IFSC code on cancelled cheque</option>
                                      <option value='PAN_card_authorized_signatory'>Pan card - Authorized signatory</option>
                                      <option value='passport'>Passport</option>
                                      <option value='certificate_of_incorporation'>Certificate of Incorporation</option>
                                      <option value='MOA_and_AOA'>MOA and AOA</option>
                                      <option value='company_PAN'>Company PAN</option>
                                      <option value='board_resolution'>Board resolution</option>
                                      <option value='GST_certification'>GST Certification</option>                                   </>
                                  ):queueValue ===("WEB_RISK_MONITORING_Q")?
                                  <>
                                      <option value=''>Select</option>
                                      {/* <option value='website'>Website</option> */}
                                      <option value='upi_handle'>UPI handle</option>
                                      <option value='MCC_code_input'>MCC Code Input</option>
                                      <option value='address_input'>Address Input</option>
                                      <option value='legal_name_input'>Legal Name Input</option>
                                      <option value='risk_classification'>Risk Classification</option>
                                      <option value='website_working'>Website Working?</option>
                                      <option value='suspicious_domain'>Suspicious Domain</option>
                                      <option value='malware_present'>Malware Present</option>
                                      <option value='malware_risk'>Malware Risk</option>
                                      <option value='domain_registration_date'>Domain Registration Date</option>
                                      <option value='domain_registration_expiry_day'>Domain Registration Expiry Date</option>
                                      <option value='ssl_certificate_check'>SSL Certificate Check</option>
                                      <option value='readiness'>Readiness</option>
                                      <option value='merchant_intelligence'>Merchant Intelligence</option>
                                      <option value='parked_domain'>Parked Domain</option>
                                      <option value='spamming'>Spamming</option>
                                      <option value='safety'>Safety</option>
                                      <option value='suspicious'>Suspicious</option>
                                      <option value='phishing'>Phishing</option>
                                      <option value='page_links_connectivity_check_success_rate'>Page Links Connectivity Check Success Rate</option>
                                      <option value='policy_compliance_status_contactus_page'>Policy Compliance Status Contactus Page</option>                                      
                                      <option value='contact_form_status'>Contact Form Status</option>
                                      <option value='privacy_policy_status'>Privacy Policy Status</option>
                                      <option value='return_policy_status'>Return Policy Status</option>
                                      <option value='shipping_policy_status'>Shipping Policy Status</option>                                      
                                      <option value='terms_and_condition_status'>Terms And Condition Status</option>
                                      <option value='currencies_found_on_website'>Currencies Found on Website</option>
                                      <option value='products_price_listed_in_website'>Products Price Listed In Website</option>                                      
                                      <option value='website_contains_unreasonable_price'>Website Contains Unreasonable Price</option>
                                      <option value='website_is_accessible_without_login_prompt'>Website Is Accessible Without Login Prompt</option>
                                      <option value='website_rediretion'>Website Redirection</option>                                      
                                      <option value='MCC'>MCC</option>                                      
                                      <option value='overall_score'>Overall score</option>                                      
                                      
                                  </>:queueValue===("ONGOING_MONITORING_Q")?<>
                                  <option value=''>Select</option>
                                  <option value='website_working'>Website working</option> 
                                  <option value='line_of_business'>Line of business</option> 
                                  <option value='risk_classification'>Risk classification</option> 
                                  <option value='MCC'>MCC</option> 
                                  <option value='drop_in_website_success_rate'>Drop in website sucess rate</option> 
                                  <option value='legal_name'>Legal name</option> 
                                  <option value='merchant_address'>Merchant Address</option> 
                                  <option value='website_redirection'>Website redirection</option> 
                                  <option value='page_loading_time'>Page loading time</option> 
                                  <option value='return_policy_url_violation'>Return policy URL violation</option>
                                  <option value='terms_and_condtion_url_violation'>Terms and condition URL violation</option>   
                                  <option value='privacy_policy_url_violation'>Privacy policy URL violation</option>   
                                  <option value='contact_us_url_vioation'>Contact us URL violation</option>   
                                  <option value='shipping_policy_url_violation'>Shipping policy url violation</option>   
                                  <option value='parked_domain'>Parked domain</option>   
                                  <option value='domain_expiry_risk'>Domain expiry risk</option>
                                  <option value='non_inr_pricing'>Non INR pricing</option>
                                  <option value='online_reputation_drop'>Online Reputation drop</option>
                                  <option value='heavy_discount'>Heavy discount</option> 
                                  <option value='contact_details_phone'>Contact details - Phone</option>
                                  <option value='contact_details_email'>Contact details - Email</option>         
                                  
                                  </>:
                                  (
                                    <>
                                      <option value=''>Select</option>
                                      <option value='Email'>Email</option>
                                      <option value='Phone'>Phone</option>
                                      <option value='Website'>Website</option>
                                      <option value='Individual Address'>Individual Address</option>
                                      <option value='Ip Address'>Ip Address</option>
                                      <option value='Divice Id'>Divice Id</option>
                                      <option value='Business Address'>Business Address</option>
                                    </>
                                  )
                            }
                          </select>
                          {errors && errors.ruleField && (
                            <div className='fv-plugins-message-container text-danger'>
                              <div className='fv-help-block'>
                                <span role='alert'>{errors && errors.ruleField}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className='col-lg-3 '>
                      <div className='col-md-10 mx-10 '>
                        <label className='col-form-label  col-md-8 text-lg-start'>
                          Operators :
                        </label>
                        <div className='col-lg-12 '>
                          <select
                            name="ruleOperator"
                            // value={item.ruleOperator || ''}
                            value={currentInputIndex === i ? item[i] && item[i].ruleOperator : item.ruleOperator}
                            className='form-control form-control-solid form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e, i)}
                          >
                            {queueValue === ('KYC_Q') ? (
                            <>
                           { item.ruleField === "email" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='is'>is</option>  
                            <option value='ends_with'>ends with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>}
                            { item.ruleField === "mobile" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='is'>is</option>  
                            <option value='starts_with'>start with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            { (item.ruleField === "document_rejected_rate" ||item.ruleField === "document_approval_rate") && 
                            <>
                            <option value=''>Select</option>
                            <option value='greater_than'>greater than</option>
                            <option value='less_than'>less than</option>                             
                            </>}
                            { (item.ruleField === "adharcard"||item.ruleField== "bank_account") && 
                            <>
                            <option value=''>Select</option>
                            <option value='is'>is</option>
                            </>}
                            { (item.ruleField ==="account_name_on_cancelled_cheque"||item.ruleField==="account_number_on_cancelled_cheque"||item.ruleField==="IFSC_code_on_cancelled_cheque")  && 
                            <>
                            <option value=''>Select</option>
                            <option value='match'>matches</option>
                            <option value='does_not_match'>does not match</option>                             
                            </>}
                            { (item.ruleField === "PAN_card_authorized_signatory"||item.ruleField== "passport" ||item.ruleField== "certificate_of_incorporation"||item.ruleField== "MOA_and_AOA"||item.ruleField== "company_PAN"||item.ruleField== "board_resolution"||item.ruleField== "GST_certification") && 
                            <>
                            <option value=''>Select</option>
                            <option value='is'>is</option>
                            </>}
                            { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>
                             
                            ):queueValue === ('MERCHANT_ONBOARDING_Q') ? (
                            <>
                           { (item.ruleField === "customer_name"||item.ruleField === "individual_address" ||item.ruleField === "business_address"||item.ruleField === "business_age"||item.ruleField === "number_of_employees"||item.ruleField === "annual_turnover"||item.ruleField === "average_transaction_amount"||item.ruleField === "business_or_legal_name"||item.ruleField === "business_sub_category")&&
                            <>
                            <option value=''>Select</option>
                            <option value='matches'>matches</option> 
                            <option value='does not match'>does not match</option>                                               
                            </>
                            }
                            { item.ruleField === "customer_phone" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='is'>is</option>  
                            <option value='starts_with'>start with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            { (item.ruleField === "customer_email"||item.ruleField === "business_email" )&& 
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='is'>is</option>  
                            <option value='ends_with'>ends with</option>
                            <option value='is_equal_to'>is equal to</option>                            
                            </>
                            }
                            { item.ruleField === "business_phone" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>
                            <option value='is'>is</option>  
                            <option value='starts_with'>start with</option>
                            <option value='is_equal_to'>is equal to</option>
                            <option value='matches'>matches</option> 
                            <option value='does_not_match'>does not match</option>                             
                            </>
                            }
                            { (item.ruleField ==="ip_address"||item.ruleField ==="type_of_ownership") && 
                            <>
                                <option value=''>Select</option>
                                <option value='is'>is</option>
                            </>
                            }
                            
                            { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>
                             
                            ):queueValue ===("WEB_RISK_MONITORING_Q")?
                            <>
                             {  item.ruleField === "upi_handle" &&
                            <>
                            <option value=''>Select</option>
                            <option value='is_in'>is in</option>                           
                            </>}
                            { (item.ruleField === "MCC_code_input"||item.ruleField === "legal_name_input"||item.ruleField === "address_input") &&
                            <>
                            <option value=''>Select</option>
                            <option value='does_not_match'>does not match</option>                           
                            </>}
                            { (item.ruleField === "risk_classification"||item.ruleField === "spamming" ||item.ruleField === "safety"||item.ruleField === "suspicious"||item.ruleField === "phishing"||item.ruleField === "parked_domain")&&
                            <>
                            <option value=''>Select</option>
                            <option value='matches'>matches</option>                           
                            </>}
                            {item.ruleField === "currencies_found_on_website" &&
                             <>
                            <option value=''>Select</option>
                            <option value='matches'>matches</option>  
                            <option value='does_not_match'>does not match</option>                         
                            </>
                             }
                            { (item.ruleField === "website_working"||item.ruleField === "website_rediretion"||item.ruleField === "website_is_accessible_without_login_prompt"||item.ruleField === "website_contains_unreasonable_price"||item.ruleField === "products_price_listed_in_website"||item.ruleField === "terms_and_condition_status"||item.ruleField === "shipping_policy_status"||item.ruleField === "return_policy_status"||item.ruleField === "privacy_policy_status"||item.ruleField === "contact_form_status"||item.ruleField === "policy_compliance_status_contactus_page"||item.ruleField === "readiness"||item.ruleField === "suspicious_domain"||item.ruleField === "malware_present"||item.ruleField === "malware_risk"||item.ruleField === "ssl_certificate_check"||item.ruleField === "policy_compliance_status_contactus_page"||item.ruleField === "contact_form_status"||item.ruleField === "privacy_policy_status"||item.ruleField === "return_policy_status"||item.ruleField === "shipping_policy_status"||item.ruleField === "terms_and_condition_status"||item.ruleField === "products_price_listed_in_website"||item.ruleField === "website_contains_unreasonable_price"||item.ruleField === "website_is_accessible_without_login_prompt"||item.ruleField === "website_rediretion")&&
                            <>
                            <option value=''>Select</option>
                            <option value='is'>is</option>                           
                            </>}
                            {(item.ruleField==="merchant_intelligence")&&
                              <>
                            <option value=''>Select</option>
                            <option value='is'>is</option>   
                            <option value='is_less_than'>is less than</option>                        
                            </>}
                            { item.ruleField === "page_links_connectivity_check_success_rate"&&
                            <>
                            <option value=''>Select</option>
                            <option value='is_less_than'>is less than</option>                           
                            </>
                            }
                            { item.ruleField === "MCC"&&
                            <>
                            <option value=''>Select</option>
                            <option value='equalto'>equalto</option>                           
                            </>
                            }
                            { item.ruleField === "overall_score"&&
                            <>
                            <option value=''>Select</option>
                            <option value='greater_than'>greater than</option>                           
                            </>
                            }
                            {
                              (item.ruleField === "domain_registration_date"||item.ruleField ==='domain_registration_expiry_day')&&
                              <>
                            <option value=''>Select</option>
                            <option value='is_within'>is within</option> 
                            </>
                            }
                             { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>:queueValue ===("ONGOING_MONITORING_Q")?
                            <>
                               { (item.ruleField ==="website_working" || item.ruleField=="parked_domain"||item.ruleField==="domain_expiry_risk"||item.ruleField==="non_inr_pricing"||item.ruleField==="heavy_discount"||item.ruleField==="website_redirection") && 
                            <>
                                <option value=''>Select</option>
                                <option value='equal_to'>equal to</option> 
                            </>}
                            
                              {item.ruleField ==="risk_classification" && 
                            <>
                                <option value=''>Select</option>
                                <option value='has_changed_from'>has changed from</option>
                            </>}
                              {(item.ruleField ==="MCC"||item.ruleField==="legal_name"||item.ruleField==="merchant_address"||item.ruleField ==="line_of_business"||item.ruleField ==="return_policy_url_violation"||item.ruleField ==="terms_and_condtion_url_violation"||item.ruleField ==="privacy_policy_url_violation"|| item.ruleField==="contact_us_url_vioation"||item.ruleField==="shipping_policy_url_violation"||item.ruleField==="contact_details_phone"||item.ruleField==="contact_details_email" )&& 
                            <>
                                <option value=''>Select</option>
                                <option value='has_changed_detected'>has changed detected</option>
                            </>}
                              {(item.ruleField ==="drop_in_website_success_rate"||item.ruleField ==="page_loading_time"||item.ruleField==="online_reputation_drop") && 
                            <>
                                <option value=''>Select</option>
                                <option value='is_greater_than'>is greater than</option>
                            </>}
                             { (item.ruleField ==="") && 
                            <>
                                <option value=''>Select</option>
                            </>}
                            </>
                            :
                            <>
                            <option value=''>Select</option>
                            <option value='Matches'>Matches</option>
                            <option value='Does not match'>Does not match</option>
                            <option value='Starts with'>Starts with</option>
                            <option value='Ends with'>Ends with</option>
                            <option value='Greater than or equal to'>Greater than or equal to</option>
                            <option value='Not equal to'>Not equal to</option>
                            <option value='Greater than'>Greater than</option>
                            <option value='Less than'>Less than</option>
                            <option value='Less than or equal to'>Less than or equal to </option>
                            </>
                            }
                            
                          </select>
                          {errors && errors.ruleOperator && (
                            <div className='fv-plugins-message-container text-danger'>
                              <div className='fv-help-block'>
                                <span role='alert'>{errors && errors.ruleOperator}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    {
                      (
                        formData && formData.customRules[i].ruleField === 'Billing Country' ||
                        formData && formData.customRules[i].ruleField === 'Shipping Country' ||
                        formData && formData.customRules[i].ruleField === 'AVS' ||
                        formData && formData.customRules[i].ruleField === 'BillingPhoneCountryCode' ||
                        formData && formData.customRules[i].ruleField === 'ShippingPhoneCountryCode') &&
                        (formData && formData.customRules[i].ruleOperator === 'Matches' || formData.customRules[i].ruleOperator === 'Does not match'
                        )
                        ? (
                          <div className='col-lg-3 '>
                            <div className='col-md-12 mx-10 '>
                              <label className='col-form-label  col-md-4 text-lg-start'>
                                DropDown :
                              </label>
                              <div className='col-lg-12 '>
                                <select
                                  name="ruleMatchField"
                                  value={currentInputIndex === i ? item[i] && item[i].ruleMatchField : item.ruleMatchField}
                                  // value={item.ruleMatchField || ''}
                                  className='form-control form-control-solid form-select'
                                  data-control='select'
                                  data-placeholder='Select an option'
                                  data-allow-clear='true'
                                  onChange={(e) => handleChange(e, i)}
                                >
                                  <option value=''>Select</option>
                                  {
                                    (formData && formData.customRules[i].ruleField === 'Billing Country') ? (
                                      <>
                                        <option value='Does not match'>Shipping Address Country</option>
                                        <option value='IPAddressCountry'>IP Address Country</option>
                                      </>
                                    ) :
                                      (formData && formData.customRules[i].ruleField === 'Shipping Country') ? (
                                        <>
                                          <option value='Billing Address Country'>Billing Address Country</option>
                                          <option value='IPAddressCountry'>IP Address Country</option>
                                        </>
                                      ) : (formData && formData.customRules[i].ruleField === 'AVS') ? (
                                        <>
                                          <option value='A'>A</option>
                                          <option value='B'>B</option>
                                          <option value='C'>C</option>
                                        </>
                                      ) : (formData && formData.customRules[i].ruleField === 'BillingPhoneCountryCode') ? (
                                        <>
                                          <option value='Shipping Phone Country Code'>Shipping Phone Country Code</option>
                                          <option value='Enter a Phone Country Code'>Enter a Phone Country Code</option>
                                        </>
                                      ) :
                                        (formData && formData.customRules[i].ruleField === 'ShippingPhoneCountryCode') ? (
                                          <>
                                            <option value='Billng Phone Country Code'>Billng Phone Country Code</option>
                                            <option value='Enter a Phone Country Code'>Enter a Phone Country Code</option>
                                          </>
                                        ) : null
                                  }
                                </select>
                                {errors && errors.ruleMatchField && (
                                  <div className='fv-plugins-message-container text-danger'>
                                    <div className='fv-help-block'>
                                      <span role='alert'>{errors && errors.ruleMatchField}</span>
                                    </div>
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>

                        )
                        : (
                          null
                        )
                    }
                    {
                      (formData && formData.customRules[i].ruleMatchField === 'Shipping Phone Country Code' ||
                        formData && formData.customRules[i].ruleMatchField === 'Billng Phone Country Code') ? (
                        null
                      ) :
                        (
                          <div className='col-lg-3 '>
                      <div className='col-md-10 mx-10 '>
                        <label className='col-form-label  col-md-8 text-lg-start'>
                          Values :
                        </label>
                        <div className='col-lg-12 '>
                           { queueValue ==="KYC_Q" && item.ruleOperator == "starts_with" || queueValue ==="KYC_Q" && item.ruleOperator == "ends_with"|| queueValue ==="KYC_Q" && item.ruleOperator =="is_equal_to" ||queueValue === 'MERCHANT_ONBOARDING_Q' && item.ruleOperator == "starts_with" ||queueValue ==="MERCHANT_ONBOARDING_Q" && item.ruleOperator =="is_equal_to"|| queueValue ==="MERCHANT_ONBOARDING_Q" && item.ruleOperator == "ends_with" ?
                             
                           <input          
                            value={currentInputIndex === i ? item[i] && item[i].ruleValue : item.ruleValue}                            
                            className="form-control  form-control-solid"
                            placeholder="Rule value"
                            name="ruleValue" 
                            type="text"
                            onChange={(e) => handleChange(e,i)
                            
                          }
                          
                            /> :
                            
                            
                            <select
                            name="ruleValue"
                            // value={item.ruleOperator || ''}
                            value={currentInputIndex === i ? item[i] && item[i].ruleValue : item.ruleValue}
                            className='form-control form-control-solid form-select'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e, i)}
                          >
                            {queueValue === ('KYC_Q') ? (
                            <>
                           { item.ruleOperator == "is_in" && 
                              <>
                            <option value=''>Select </option>
                            <option value='blacklist'>Blacklist </option>  
                            <option value='whitelist'>Whitelist</option>
                            </> }
                            { (item.ruleOperator == "greater_than"||item.ruleOperator == "less_than") &&
                              <>
                            <option value=''>Select </option>
                            <option value='20'>20</option>
                            <option value='40'>40</option>
                            <option value='60'>60</option>
                            <option value='80'>80</option>
                            <option value='100'>100</option>
                            </> }
                            {(item.ruleField==="adharcard"||item.ruleField=="bank_account"||item.ruleField==="mobile"||item.ruleField =="email")&&item.ruleOperator == "is" && 
                              <>
                            <option value=''>Select </option>
                            <option value='verified'>Verified </option>  
                            <option value='unverified'>Unverified</option>
                            </> }
                            {(item.ruleField==="account_name_on_cancelled_cheque"&&item.ruleOperator == "match"||item.ruleField==="account_name_on_cancelled_cheque"&&item.ruleOperator == "does_not_match") && 
                              <>
                            <option value=''>Select </option>
                            <option value='account_name'>Account Name provided by merchant</option>  
                            </> }
                            {(item.ruleField==="account_number_on_cancelled_cheque"&&item.ruleOperator == "match"||item.ruleField==="account_number_on_cancelled_cheque"&&item.ruleOperator == "does_not_match") && 
                              <>
                            <option value=''>Select </option>
                            <option value='account_number'>Account Number provided by merchant</option>  
                            </> }
                            {(item.ruleField==="IFSC_code_on_cancelled_cheque"&&item.ruleOperator == "match"||item.ruleField==="IFSC_code_on_cancelled_cheque"&&item.ruleOperator == "does_not_match") && 
                              <>
                            <option value=''>Select </option>
                            <option value='IFSC_code'>IFSC Code provided by merchant</option>  
                            </> }
                            { (item.ruleField ==="PAN_card_authorized_signatory"||item.ruleField ==="passport"||item.ruleField ==="certificate_of_incorporation"||item.ruleField ==="MOA_and_AOA"||item.ruleField ==="company_PAN"||item.ruleField ==="board_resolution"||item.ruleField ==="GST_certification")&&item.ruleOperator == "is" && 
                              <>
                            <option value=''>Select </option>
                            <option value='approved'>Approved </option>  
                            <option value='rejected'>Rejected</option>
                            </> }
                            { item.ruleOperator == "" && 
                              <>
                            <option value=''>Select </option>
                            </> }
                            </>
                            ):queueValue === ('MERCHANT_ONBOARDING_Q')?(
                             <>
                             { item.ruleField == "customer_name" && 
                              <>
                             <option value=''>Select </option>
                            <option value='name_on_phone'>name on phone </option>
                            <option value='name_on_email'>name on email </option>
                            <option value='name_on_individual_address'>name on individual address </option>
                            </> 
                            }
                            { item.ruleOperator == "is_in" && 
                              <>
                            <option value=''>Select </option>
                            <option value='blacklist'>blacklist </option>  
                            <option value='whitelist'>whitelist</option>
                            </> 
                            }
                            {(item.ruleField==="customer_phone"||item.ruleField==="customer_email"||item.ruleField==="business_phone"||item.ruleField==="business_email")&&item.ruleOperator == "is" && 
                              <>
                            <option value=''>Select </option>
                            <option value='verified'>Verified </option>  
                            <option value='unverified'>Unverified</option>
                            </> }
                            { item.ruleField === "business_phone" && (item.ruleOperator==="matches"||item.ruleOperator==="does_not_match")&& 
                              <>
                            <option value=''>Select </option>
                            <option value='business_address_country_code'>business address country code </option>  
                            <option value='ip_country_code'>ip country code</option>
                            </> 
                            }
                            {(item.ruleField === ""||item.ruleOperator==="") &&
                           <option value=''>Select </option>
                            }
                             { item.ruleField === "individual_address" && (item.ruleOperator==="matches"||item.ruleOperator==="does_not_match")&& 
                              <>
                            <option value=''>Select </option>
                            <option value='business_address'>business address </option>  
                            <option value='ip_address_country'>ip address country</option>
                            <option value='phone_country'>phone country</option>
                            </> 
                            }
                            { item.ruleField === "business_address" && (item.ruleOperator==="matches"||item.ruleOperator==="does_not_match")&& 
                              <>
                            <option value=''>Select </option>
                            <option value='ip_address_country'>ip address country</option>
                            <option value='phone_country'>phone country</option>
                            </> 
                            }
                            { item.ruleField === "ip_address" && 
                              <>
                            <option value=''>Select </option>
                            <option value='server_is_proxy'>server is proxy</option>
                            </> 
                            }
                            { item.ruleField === "type_of_ownership" && 
                              <>
                            <option value=''>Select </option>
                            <option value='individual'>Individual</option>
                            <option value='sole_proprietorship'>Sole Proprietorship</option>
                            <option value='partnership'>Partnership</option>
                            <option value='corporate'>Corporate</option>
                            <option value='non-profit_organization'>Non-profit Organization</option>
                            <option value='goverment_entity'>Goverment Entity</option>
                            </> 
                            }
                            { (item.ruleField ==="business_or_legal_name") && 
                            <>
                                <option value=''>Select</option>
                                <option value='customer_name'>customer name</option>
                            </>
                            }
                            { (item.ruleField ==="business_age") && 
                            <>
                                <option value=''>Select</option>
                                <option value='new_business'>new business</option>
                                <option value='less_than_six_months'>less than 6 months</option>
                                <option value='6-12_months'>6-12 months</option>
                                <option value='1-2_years'>1-2 years</option>
                                <option value='2-5_years'>2-5 years</option>
                                <option value='more_than_5_years'>more than 5 years</option>
                            </>
                            }
                            { (item.ruleField ==="number_of_employees") && 
                            <>
                                <option value=''>Select</option>
                                <option value='self_employeed'>self employeed</option>
                                <option value='1_5_employees'>1-5 employees</option>
                                <option value='6_10_employees'>6-10 employees</option>
                                <option value='11_15_employees'>11-25 employees</option>
                                <option value='25_50_employees'>25-50 employees</option>
                                <option value='50_plus_employees'>50+ employees</option>
                            </>
                            }
                            { (item.ruleField ==="number_of_employees") && 
                            <>
                                <option value=''>Select</option>
                                <option value='self_employeed'>self employeed</option>
                                <option value='1_5_employees'>1-5 employees</option>
                                <option value='6_10_employees'>6-10 employees</option>
                                <option value='11_15_employees'>11-25 employees</option>
                                <option value='25_50_employees'>25-50 employees</option>
                                <option value='50_plus_employees'>50+ employees</option>
                            </>
                            }
                            { (item.ruleField ==="annual_turnover") && 
                            <>
                                <option value=''>Select</option>
                                <option value='0_annual_turnover'>Started now</option>
                                <option value='0-10000_annual_turnover'>0-10000</option>
                                <option value='10000-100000_annual_turnover'>10000-100000</option>
                                <option value='100000-1000000_annual_turnover'>100000-1000000</option>
                                <option value='10lakh_1cr_annual_turnover'>10 lakh- 1 crore</option>
                                <option value='1cr_10cr_annual_turnover'>1 crore - 10 crore</option>
                                <option value='10cr_plus_annual_turnover'>10 crore ++</option>
                            </>
                            }
                            { (item.ruleField ==="average_transaction_amount") && 
                            <>
                                <option value=''>Select</option>
                                <option value='1_250'>1-250</option>
                                <option value='251_500'>251-500</option>
                                <option value='501_10000'>501-1000</option>
                                <option value='1001_2500'>1001-2500</option>
                                <option value='2501_5000'>2501-5000</option>
                                <option value='5001_plus'>5000 ++</option>
                            </>
                            }
                            {(item.ruleField === ""||item.ruleOperator==="") &&
                           <option value=''>Select </option>
                            }
                             </>
                            ):queueValue ===("WEB_RISK_MONITORING_Q")?
                            <>
                            { (item.ruleField ==="website"||item.ruleField ==="upi_handle") && 
                            <>
                                <option value=''>Select</option>
                                <option value='blacklist'>blacklist</option>
                                <option value='whitelist'>whitelist</option>
                            </>
                            }
                            { item.ruleField ==="MCC_code_input" && 
                            <>
                                <option value=''>Select</option>
                                <option value='MCC_code_scrapped'>MCC Code Scrapped </option>
                            </>
                            }
                            { item.ruleField ==="legal_name_input" && 
                            <>
                                <option value=''>Select</option>
                                <option value='legal_name_scrapped'>Legal Name Scrapped  </option>
                            </>
                            }
                            { item.ruleField ==="address_input" && 
                            <>
                                <option value=''>Select</option>
                                <option value='address_scrapped'>Address Scrapped </option>
                            </>
                            }
                            { item.ruleField ==="risk_classification" && 
                            <>
                                <option value=''>Select</option>
                                <option value='approved_high'>Approved-High </option>
                                <option value='restricted'>Restricted </option>
                                <option value='restricted_with_license'>Restricted with License </option>
                            </>
                            } 
                            { item.ruleField ==="website_working" && 
                            <>
                                <option value=''>Select</option>
                                <option value='no'>no </option>
                            </>
                            }
                            { (item.ruleField ==="website_contains_unreasonable_price"||item.ruleField ==="phishing"||item.ruleField==="suspicious") && 
                            <>
                                <option value=''>Select</option>
                                <option value='true'>true </option>
                            </>
                            }
                            { (item.ruleField ==="malware_present" ||item.ruleField ==="website_rediretion")&& 
                            <>
                                <option value=''>Select</option>
                                <option value='yes'>yes </option>
                            </>
                            }
                            { item.ruleField ==="website_is_accessible_without_login_prompt" && 
                            <>
                                <option value=''>Select</option>
                                <option value='login-registration_prompt'>login-registration prompt </option>
                                <option value='login-no_registration_prompt'>login-no-registration prompt </option>
                            </>
                            }
                            {
                              item.ruleField === "ssl_certificate_check"&&
                              <>
                                <option value=''>Select</option>
                                <option value='no'>no </option>
                              </>
                            }
                              {
                              item.ruleField === "overall_score"&&
                              <>
                                <option value=''>Select</option>
                               {[...Array(100)].map((x, i) =>
                                     <option value={i+1}>{i+1}</option>
                                  )}
                                
                              </>
                            }
                            {
                              item.ruleField === "MCC"&&
                              <>
                                <option value=''>Select</option>
                                {
                                  [5532,5732,5621,5631,5973,7996,7361,5661,4722,7230,5948,4722,4816,5045,4816,5044,5817,4812,8220,8211,8249,8211,8299,5732,4814,5817,5611,8043,5948,5122,5641,5655,5655,1520,1520,5399,5631,5699,5399,8021,7299,5499,5499,5814,5541,5947,7542,5992,5411,5719,7349,5722,5722,5812,5812,5812,5813,5813,5816,5816,7399,4215,4214,8398,5261,5732,5941,5713,5941,5941,5942,5960,4900,5942,5942,5942,5942,5942,8041,7996,7991,5712,5942,5942,5995,7911,7922,5993,5733,7273,8011,5122,8071,5942,5944,7998,5094,5967,5999,5999,5999,6012,6012,6051,6051,6211,6513,5994,5735,5977,6211,5732,5732,5732,7230,7210,5111,5945,7549,5521,7512,5511,7230,7333,7832,780,7230,4722,5815,7941,7941,5967,5967,7941,7941,7941,7995,8299,7538,7995,5047,7997,7997,5651,5943,7699,9399,7999,2741,5046,5065,5074,7999,1750,8011,5085,8351,8099,8220,5811,8111,8099,8999,5973,4121,9402,4821,5099,5172,5199,5300,5309,5451,5940,4789,8999,5970,8999,5972,8999,9311,4829,5933,8999,5931,8651,8999,7012,5262,9222,7623,7629,7321,5698,5169,5122,5462,7379,7631,5971,8299,9222,5499,5499].map(code =><option value={code}>{code} </option>)
                                }
                               
                              </>
                            }
                             {
                              (item.ruleField === "policy_compliance_status_contactus_page"||item.ruleField === "terms_and_condition_status"||item.ruleField === "contact_form_status"||item.ruleField === "privacy_policy_status"||item.ruleField === "return_policy_status"||item.ruleField === "shipping_policy_status")&&
                              <>
                                <option value=''>Select</option>
                                <option value='inaccesible '>inaccesible </option>
                              </>
                            }
                            {
                              item.ruleField === "page_links_connectivity_check_success_rate"&&
                               <>
                                <option value=''>Select</option>
                                <option value='50%'>50% </option>
                              </>
                            }
                             {
                              item.ruleField === "readiness"&&
                              <>
                                <option value=''>Select</option>
                                <option value='broken_links'>broken links </option>
                              </>
                            }
                            { item.ruleField ==="malware_risk" && 
                            <>
                                <option value=''>Select</option>
                                <option value='high'>high </option>
                            </>
                            }
                            { item.ruleField ==="safety" && 
                            <>
                                <option value=''>Select</option>
                                <option value='false'>false </option>
                            </>
                            }
                            { item.ruleField ==="currencies_found_on_website" && 
                            <>
                                <option value=''>Select</option>
                                <option value='rupee'>₹ </option>
                                 <option value='rupee_and_dollar'>₹$ </option>
                            </>
                            }
                            { (item.ruleField ==="parked_domain"||item.ruleField ==="spamming"||item.ruleField==="suspicious") && 
                            <>
                                <option value=''>Select</option>
                                <option value="true">true </option>
                            </>
                            }
                            { ((item.ruleField ==="merchant_intelligence" && item.ruleOperator==="is") ||item.ruleField ==="products_price_listed_in_website")&& 
                            <>
                                <option value=''>Select</option>
                                <option value='no'>No </option>
                            </>
                            } 
                              { item.ruleField ==="merchant_intelligence" && item.ruleOperator==="is_less_than" && 
                            <>
                                <option value=''>Select</option>
                                <option value='2.5'>2.5 </option>
                            </>
                            }                             
                            { (item.ruleField ==="domain_registration_date"||item.ruleField==="domain_registration_expiry_day") && 
                            <>
                                <option value=''>Select</option>
                                <option value='last_30_days'>Last 30 days </option>
                                <option value='last_60_days'>Last 60 days </option>
                                <option value='last_90_days'>Last 90 days </option>
                            </>
                            }                                                                
                            {(item.ruleField === ""||item.ruleOperator==="") &&
                              <option value=''>Select </option>
                            }
                            </>:queueValue ===("ONGOING_MONITORING_Q")?
                            <>
                            {item.ruleField === "website_working" &&
                              <>
                              <option value=''>Select </option>
                              <option value='no'>no </option>
                              </>
                            }
                            {(item.ruleField === "line_of_business"||item.ruleField==="MCC"||item.ruleField==="legal_name"||item.ruleField==="merchant_address"||item.ruleField==="website_redirection"||item.ruleField==="return_policy_url_violation"||item.ruleField==="terms_and_condtion_url_violation"||item.ruleField==="privacy_policy_url_violation"||item.ruleField==="contact_us_url_vioation"||item.ruleField==="shipping_policy_url_violation"||item.ruleField==="domain_expiry_risk"||item.ruleField==="non_inr_pricing"||item.ruleField==="contact_details_phone"||item.ruleField==="contact_details_email") &&
                              <>
                              <option value=''>Select </option>
                              <option value='yes'>yes </option>
                              </>
                            }
                             {item.ruleField === "risk_classification" &&
                              <>
                              <option value=''>Select </option>
                              <option value='low_to_high'>low to high </option>
                              <option value='low_to_restricted'>low to restricted </option>
                              <option value='medium_to_restricted'>medium to restricted </option>
                              <option value='high_to_restricted'>high to restricted </option>
                              <option value='medium_to_high'>medium to high </option>
                              </>
                            }
                           
                             {item.ruleField === "drop_in_website_success_rate" &&
                              <>
                              <option value=''>Select </option>
                              <option value='10'>10 </option>
                              </>
                            }
                             {item.ruleField === "page_loading_time" &&
                              <>
                              <option value=''>Select </option>
                              <option value='2000_ms'>2000 ms </option>
                              </>
                            }
                             {item.ruleField === "parked_domain" &&
                              <>
                              <option value=''>Select </option>
                              <option value='true'>true </option>
                              </>
                            }
                             {item.ruleField === "page_loading_time" &&
                              <>
                              <option value=''>Select </option>
                              <option value='2000_ms'>2000 ms </option>
                              </>
                            }
                             {(item.ruleField === ""||item.ruleOperator==="") &&
                              <option value=''>Select </option>
                            }
                            </>
                            :
                            <>
                            <option value=''>Select</option>
                            {/* <option value='Matches'>Matches</option>
                            <option value='Does not match'>Does not match</option>
                            <option value='Starts with'>Starts with</option>
                            <option value='Ends with'>Ends with</option>
                            <option value='Greater than or equal to'>Greater than or equal to</option>
                            <option value='Not equal to'>Not equal to</option>
                            <option value='Greater than'>Greater than</option>
                            <option value='Less than'>Less than</option>
                            <option value='Less than or equal to'>Less than or equal to </option> */}
                            </>
                            }
                            
                          </select>}
                          
                          {errors && errors.ruleValue && (
                            <div className='fv-plugins-message-container text-danger'>
                              <div className='fv-help-block'>
                                <span role='alert'>{errors && errors.ruleValue}</span>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                        )
                    }
                    <div className='col-lg-3 '>
                      {
                        i === 0 ? (
                          <div className='col-md-10 mx-10 '>
                           {item.ruleValue.length > 0 &&
                           <button
                              type='button'
                              className='btn btn-sm btn-light-primary mt-14 text-lg-start '
                              onClick={() => handleAddRole(i)}
                            >
                              <span className='indicator-label'>Add Rule</span>
                            </button>
                            }
                          </div>
                        ) : (
                          <div className='col-md-5 mx-5 d-flex  '>
                            <button
                              type='button'
                              className='btn btn-sm btn-warning mt-14 text-lg-start mr-5'
                              onClick={() => handleRemoveClick(i)}
                            >
                              <span className='indicator-label'>Remove</span>
                            </button>
                              <button
                              type='button'
                              className='btn btn-sm btn-light-primary mt-14 text-lg-start '
                              onClick={() => handleAddRole(i)}
                            >
                              <span className='indicator-label'>Add</span>
                            </button>
                          </div>
                        )
                      }
                    </div>
                  </div>
                ))
              }
              <div className='row mt-10'>
                <div className='col-lg-3 '>
                  <div className='mb-10'>
                    <div className='col-md-10 mx-10 '>
                      <label className='col-form-label col-md-4  text-lg-start required'>
                        Action :
                      </label>
                      <div className='col-lg-12 '>
                        <select
                          name="action"
                          className='form-control form-control-solid form-select'
                          data-control='select'
                          data-placeholder='Select an option'
                          data-allow-clear='true'
                          value={formData.action || ''}
                          onChange={(e) => handleChange(e)}
                        >
                          <option value=''>Select</option>
                          <option value='Accept Account'>Accept Account</option>
                          <option value='Reject Account'>Reject Account </option>
                          <option value='Manual Review'>Manual Review</option>
                        </select>
                        {errors && errors.action && (
                          <div className='fv-plugins-message-container text-danger'>
                            <div className='fv-help-block'>
                              <span role='alert'>{errors && errors.action}</span>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='row mt-10'>
                <div className='col-lg-12 '>
                  <div className='col-md-10 mx-10  '>
                    <label className='col-form-label col-md-4 text-lg-start'> Display Rule Logic:</label>
                    <div className='card card-xl-stretch mb-xl-8 '>
                      <div className='card-body pt-0 mt-4 mb-4'>
                        <div className='ml-2  text-gray-800 '>
                          WHERE(
                          {
                            showRule.map((item, i, arr) => {
                              return (
                                <span className='text-gray-800 ' key={i}>
                                  <span className='text-gray-800 me-2'>{item.ruleField}</span>
                                  <span className='text-gray-800 fs-4 me-2'>{item.ruleOperator}</span>
                                  <span className='text-gray-800 fs-4 me-2'>{item.ruleMatchField}</span>
                                  <span className='text-gray-800 me-2'>
                                    {`${item.ruleValue} ${arr.length - 1 === i ? '' : (
                                      i.length !== 0 ? rulescondition : ''
                                    )}`}
                                  </span>
                                </span>
                              )
                            }
                            )
                          }
                          )
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )
        }
        <div className='row mt-10'>
          <div className='col-lg-4' >
          </div>
          <div className='col-lg-4' >
          </div>
          <div className='col-lg-4 '>
            <button
              type='button'
              className='btn btn-sm btn-light-primary m-2 fa-pull-right'
              onClick={(e) => handleSubmit(e)}
              disabled={loadingUR || loadingARG}
            >
              {loadingUR || loadingARG ? (
                <span className='indicator-progress' style={{ display: 'block' }}>
                  Please wait...
                  <span className='spinner-border spinner-border-sm align-middle ms-2' />
                </span>
              ) : "Submit"
              }
            </button>
            <Link
              to='/rules'
              disabled={loadingUR || loadingARG||queueIdData===undefined}
              className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
            >
              Back To Rule List
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = state => {
  const {
    editRulesStore, inputFieldsStore, rulesStore, updateRulesStore, queueslistStore
  } = state
  return {
    addRules: rulesStore && rulesStore.addRules ? rulesStore.addRules : '',
    loadingARG: rulesStore && rulesStore.loadingARG ? rulesStore.loadingARG : false,
    // addRules: inputFieldsStore && inputFieldsStore.inputfields ? inputFieldsStore.inputfields : '',
    inputfields: state && state.inputFieldsStore && state.inputFieldsStore.inputfields,
    loadingGR: editRulesStore && editRulesStore.loadingGR ? editRulesStore.loadingGR : false,
    RulesIdDetail: editRulesStore && editRulesStore.RulesIdDetail ? editRulesStore.RulesIdDetail : {},
    statusEA: editRulesStore && editRulesStore.statusEA ? editRulesStore.statusEA : '',
    loadingUR: updateRulesStore && updateRulesStore.loadingUR ? updateRulesStore.loadingUR : false,
    updateRulesResponce: updateRulesStore && updateRulesStore.updateRulesResponce ? updateRulesStore.updateRulesResponce : {},
    messageUR: updateRulesStore && updateRulesStore.messageUR ? updateRulesStore.messageUR : '',
    statusUR: updateRulesStore && updateRulesStore.statusUR ? updateRulesStore.statusUR : '',
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    messageAR: rulesStore && rulesStore.messageAR ? rulesStore.messageAR : '',
    statusAR: rulesStore && rulesStore.statusAR ? rulesStore.statusAR : '',

  }
}

const mapDispatchToProps = dispatch => ({
  getInputListDispatch: (params) => dispatch(inputFieldActions.getInputFields(params)),
  postRulesListDispatch: (params) => dispatch(rulesActions.postRules(params)),
  PostclearRules: () => dispatch(rulesActions.PostclearRules()),
  getRulesIdDispatch: (params) => dispatch(RulesGetIdActions.getRulesIdDetails(params)),
  clearRulesIdDetailsDispatch: () => dispatch(RulesGetIdActions.clearRulesIdDetails()),
  updateRulesDispatch: (id, params) => dispatch(updateRulesActions.updateRules(id, params)),
  clearupdateRulesDispatch: () => dispatch(updateRulesActions.clearupdateRules()),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RuleForm)
