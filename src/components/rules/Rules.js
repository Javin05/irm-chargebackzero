import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { clientCredFilterActions, queuesActions, rulesActions } from '../../store/actions';
import { KTSVG } from '../../theme/helpers';
import { confirmationAlert, successAlert, warningAlert } from "../../utils/alerts";
import color from "../../utils/colors";
import { SET_FILTER, STATUS_BADGE, STATUS_RESPONSE, SWEET_ALERT_MSG } from '../../utils/constants';
import { getLocalStorage, unsetLocalStorage } from '../../utils/helper';

function Rules(props) {
  const {
    getRuleslistDispatch,
    deleteRulesListDispatch,
    className,
    rules,
    loading,
    DeleteRules,
    clearRulesDispatch,
    queuesLists,
    getQueueslistDispatch,
    setFilterFunctionDispatch,
    setCredFilterParams,
    setFilterFunction
  } = props
  const [limit, setLimit] = useState(25)
  const [, setData] = useState({})
  const [searchParams, setSearchParams] = useState({});
  const [activePageNumber, setActivePageNumber] = useState(1)
  const [SelectedQueueOption, setSelectedQueueOption] = useState('')
  const [QueueOption, setQueue] = useState()
  const [queueIDValue, setQueueIDValue] = useState()
  const [passIDValue, setPassIDValue] = useState()
  const queueNames = queuesLists && queuesLists.data
  const [queueid, setQueueid] = useState();
  //localStorage.getItem('dashboardKey') !== null ? localStorage.getItem('dashboardKey') : "624fc67fae69dc1e03f47ebd"
  const [formData, setFormData] = useState({
    queues: ''
  })

  const didMount = React.useRef(false)
  const [sorting, setSorting] = useState({
    ruleId: false,
    rulefield: false,
    ruleDescription: false,
    clientId: false
  })

  useEffect(() => {
    const credBasedClientValue = getLocalStorage(SET_FILTER.CLIENT_ID_FILTER)
    const credBasedParams = {
      clientId: credBasedClientValue
    }
    const params = {
      queueId: queueIDValue,
      limit: limit,
      page: 1,
      ...credBasedParams,
      ...searchParams,
    }
    const pickByParams = _.pickBy(params);
    getRuleslistDispatch(pickByParams)
    getQueueslistDispatch()
  }, [])

  useEffect(() => {
    if (didMount.current && setFilterFunction) {
      const currentFilterParams = setCredFilterParams.clientId
      setActivePageNumber(1);
      const params = {
        queueId: queueIDValue,
        limit: limit,
        page: 1,
        clientId: setCredFilterParams.clientId ? setCredFilterParams.clientId : ''
      };
      const pickByParams = _.pickBy(params);
      getRuleslistDispatch(pickByParams)
      getQueueslistDispatch()
      setFilterFunctionDispatch(false);
      setSearchParams(currentFilterParams);
    }
  }, [setFilterFunction, setCredFilterParams]);

  const handleRecordPerPage = (e) => {
    const { value } = e.target
    setLimit(value)
    const params = {
      limit: value,
      page: 1,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    getRuleslistDispatch(params)
    setActivePageNumber(1);
  }

  const handlePageClick = (event) => {
    const pageNumber = event.selected + 1
    const params = {
      limit: limit,
      page: pageNumber,
      clientId: setCredFilterParams.clientId
        ? setCredFilterParams.clientId
        : "",
    }
    setActivePageNumber(pageNumber)
    getRuleslistDispatch(params)
  }

  const handleSorting = (name) => {
    if (sorting && sorting[name]) {
      sorting[name] = !sorting[name]
      setSorting(sorting)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'DESC'
      }
      getRuleslistDispatch(params)
    } else {
      const filter = _.mapValues(sorting, () => {
        return false
      })
      filter[name] = !filter[name]
      setSorting(filter)
      setData({})
      const params = {
        ...searchParams,
        limit: limit,
        page: activePageNumber,
        sort: name,
        order: 'ASC'
      }
      getRuleslistDispatch(params)
    }
  }

  const totalPages =
    rules && rules.count
      ? Math.ceil(parseInt(rules && rules.count) / limit)
      : 1

  const logout = () => {
    unsetLocalStorage()
    window.location.href = '/merchant-login';
  };


  const onConfirmDelete = (id) => {
    deleteRulesListDispatch(id);
  }
  const onDeleteItem = (id) => {
    confirmationAlert(
      SWEET_ALERT_MSG.CONFIRMATION_TEXT,
      SWEET_ALERT_MSG.DELETE_RULES,
      'warning',
      'Yes',
      'No',
      () => { onConfirmDelete(id) },
      () => { }
    )
  }

  useEffect(() => {
    if (DeleteRules && DeleteRules.status === STATUS_RESPONSE.SUCCESS_MSG) {
      successAlert(
        DeleteRules && DeleteRules.message,
        'success'
      )
      clearRulesDispatch()
    } else if (DeleteRules && DeleteRules.status === STATUS_RESPONSE.ERROR_MSG) {
      warningAlert(
        'error',
        DeleteRules && DeleteRules.message,
        '',
        'Try again',
        '',
        () => { }
      )
      clearRulesDispatch()
    }
  }, [DeleteRules])

  useEffect(() => {
    const params = {
      queueId: queueid
    }
    getRuleslistDispatch(params)
  }, [queueid])

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      color: state.isSelected ? color.gray : color.black,
      background: state.isSelected ? color.white : "",
    }),
  }

  let options = [];
  if (queuesLists && queuesLists?.data) {
    queuesLists.data.forEach(element => {
      const obj = {
        'value': element._id,
        'label': element.queueName
      }
      options.push(obj);
    });
  }

  const chooseDashboardHandler = (e) => {
    // setDashboard(e.label)
    setQueueid(e.value)
    localStorage.setItem('dashboardKey', e.value);
    const params = {
      queueId: e && e.value,
      limit: limit,
      page: 1
    }
    getRuleslistDispatch(params)
  }

  const handleChangeQueue = selectedOption => {
    if (selectedOption !== null) {
      setSelectedQueueOption(selectedOption)
      setFormData(values => ({ ...values, queues: selectedOption.value }))
      setQueueIDValue(selectedOption && selectedOption.value)
      setPassIDValue(selectedOption && selectedOption.label)
      const params = {
        queueId: selectedOption && selectedOption.value,
        limit: limit,
        page: 1
      }
      getRuleslistDispatch(params)
      localStorage.setItem('dashboardKey', selectedOption && selectedOption.value);
    }
  }

  useEffect(() => {
    const Queue = getDefaultOption(queueNames)
    setQueue(Queue)
    if (!_.isEmpty(formData.queues)) {
      const selOption = _.filter(Queue, function (x) { if (_.includes(formData.queues._id, x.value)) { return x } })
      setSelectedQueueOption(selOption)
    }
  }, [queueNames])

  const getDefaultOption = (queueNames) => {
    const defaultOptions = []
    for (const item in queueNames) {
      defaultOptions.push({ label: queueNames[item].queueName, value: queueNames[item]._id })
    }
    return defaultOptions
  }

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true
    }
  })

  return (
    <>
      <div className={`card p-7 ${className}`}>
        <div className='card-body py-3'>
          <div className='d-flex  px-2'>
            <div className='d-flex justify-content-start col-md-5'>
              <div className='col-md-3 col-lg-3 col-sm-3 mt-1'>
                {rules && rules.count && (
                  <span className='text-muted fw-bold d-flex fs-3 mt-2'>
                    Total: &nbsp;{' '}
                    <span className='text-gray-700 fw-bolder text-hover-primary fs-3'>
                      {rules.count}
                    </span>
                  </span>
                )}
              </div>
              <div className='col-md-7 d-flex'>
                <label className='col-form-label text-lg-start'>
                  Record(s) per Page : &nbsp;{' '}
                </label>
                <div className='col-md-3 col-lg-3 col-sm-3'>
                  <select
                    className='form-select w-6rem'
                    data-control='select'
                    data-placeholder='Select an option'
                    data-allow-clear='true'
                    onChange={(e) => handleRecordPerPage(e)}
                  >
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                    <option value='75'>75</option>
                    <option value='100'>100</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='col-md-2 col-lg-2 col-sm-2 d-flex' />
            <div className='col-md-4 col-lg-4 col-sm-4 d-flex me-2'>
              <label className='col-form-label text-lg-start me-2'>
                Queues :
              </label>
              <div className='col-md-6 col-sm-6 '>

                {queueid !== null ? (
                  <Select
                    options={options}
                    className="react-c-select"
                    onChange={chooseDashboardHandler}
                    value={options.find(op => {
                      return op.value === queueid
                    })} />
                ) : (
                  <Select
                    options={options}
                    className="react-c-select"
                    onChange={chooseDashboardHandler} />
                )
                }


                {/* <ReactSelect
                  styles={customStyles}
                  isMulti={false}
                  name='AppUserId'
                  className='select2'
                  classNamePrefix='select'
                  handleChangeReactSelect={handleChangeQueue}
                  options={QueueOption}
                  value={SelectedQueueOption}
                  isDisabled={!QueueOption}
                /> */}
              </div>
            </div>
            <div className='d-flex col-md-3 col-lg-3 col-sm-3 my-auto'>
              <div className='my-auto me-3'>
                <Link
                  // to={`/rules-form/${passIDValue}`}
                  to={`/rule-form/add`}

                  className='btn btn-sm btn-light-primary btn-responsive font-5vw'
                >
                  <span className="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor"></rect>
                      <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor"></rect>
                    </svg>
                  </span>
                  Add Rule
                </Link>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table table-hover table-rounded table-striped border gs-2 mt-6">
              <thead>
                <tr className="fw-bold fs-6 text-gray-800">
                  <th className=" text-start">
                    <div className="d-flex">
                      <span>S.No</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("deviceID")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  {/* <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Rule ID</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ruleId")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>                   */}
                  <th>
                    <div className="d-flex">
                      <span>Client</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("clientId")}
                        >
                          <i
                            className={`bi ${sorting.queue
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Queue name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("queue")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Rule Group Name</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("ruleId")}
                        >
                          <i
                            className={`bi ${sorting.riskid
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Rule Action</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("status")}
                        >
                          <i
                            className={`bi ${sorting.status
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Rule Description</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("firstName")}
                        >
                          <i
                            className={`bi ${sorting.firstName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>
                  <th className="min-w-200px text-center">
                    <div className="d-flex">
                      <span>Status</span>
                    </div>
                  </th>
                  <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Action</span>
                      <div className="min-w-25px text-end">
                        <div
                          className="cursor-pointer"
                          onClick={() => handleSorting("lastName")}
                        >
                          <i
                            className={`bi ${sorting.lastName
                              ? "bi-arrow-up-circle-fill"
                              : "bi-arrow-down-circle"
                              } text-primary`}
                          />
                        </div>
                      </div>
                    </div>
                  </th>

                  {/* <th className="min-w-200px text-start">
                    <div className="d-flex">
                      <span>Edit</span>

                    </div>
                  </th> */}
                </tr>
              </thead>
              <tbody>
                {
                  !loading
                    ? (
                      rules &&
                        rules.data && rules.data.returnData.length !== 0
                        ? (
                          rules.data && rules.data.returnData.map((rule, id) => {
                            return (
                              <tr
                                key={id}
                                style={
                                  id === 0
                                    ? { borderColor: "black" }
                                    : { borderColor: "white" }
                                }
                              >
                                <td className="pb-0 pt-5  text-start">
                                  {id + 1}
                                </td>
                                {/* <td className="pb-0 pt-5  text-start">
                                  {rule.ruleGroupId && rule.ruleGroupId ? rule.ruleGroupId : "--"}
                                </td> */}
                                <td className="pb-0 pt-5  text-start">
                                  {rule?.clientId?.company
                                    ? rule.clientId?.company
                                    : "--"}
                                </td>
                                  <td className="pb-0 pt-5  text-start">
                                  {rule.queues && rule.queues[0] ? rule.queues[0].queueName : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {rule.ruleGroupName && rule.ruleGroupName ? rule.ruleGroupName : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {rule.action ? rule.action : "--"}
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  {rule.description ? rule.description : "--"}
                                </td>
                                {/* <td className="pb-0 pt-5  text-start">
                                  WHERE(
                                  {
                                    rule.customRules && rule.customRules.map((item, i) => {
                                      return (
                                        <>
                                          <div key={i}>
                                            <span className='text-gray-800 me-2'>{item.ruleField}</span>
                                            <span className='text-gray-800 fs-4 me-2'>{item.ruleOperator}</span>
                                            <span className='text-gray-800 me-2'>
                                              {`${item.ruleValue} ${i.length !== 0 ? rule.condition : ""}`}

                                            </span>
                                          </div>
                                        </>
                                      )
                                    })
                                  }
                                  )
                                </td> */}
                                <td className="pb-0 pt-5  text-start">
                                  <span
                                    className={`badge ${STATUS_BADGE[rule.status && rule.status]}`
                                    }>
                                    {rule.status ? rule.status : "--"}
                                  </span>
                                </td>
                                <td className="pb-0 pt-5  text-start">
                                  <div title="Edit customer"
                                    className=" text-start btn btn-icon btn-hover-primary btn-sm mx-3"
                                  >
                                    <Link
                                      to={`/rule/update/${rule._id}`}
                                      className="btn btn-icon btn-icon-warning btn-sm w-10px h-10px me-4" href="/client-onboarding/update/621c7764174b5b0004719991">
                                      <KTSVG
                                        path='/media/icons/duotune/art/art005.svg'
                                        className='svg-icon-3'
                                      />
                                    </Link>
                                  </div>
                                  <button
                                    className='btn btn-icon btn-icon-danger btn-sm w-10px h-10px'
                                    onClick={() => onDeleteItem(rule._id)}
                                    title="Delete customer"
                                  >
                                    {/* {/ eslint-disable /} */}
                                    <KTSVG
                                      path='/media/icons/duotune/general/gen027.svg'
                                      className='svg-icon-3'
                                    />
                                    {/* {/ eslint-enable /} */}
                                  </button>
                                </td>
                              </tr>
                            )
                          })
                        )
                        : (
                          <tr className='text-center py-3'>
                            <td colSpan='100%'>No record(s) found</td>
                          </tr>
                        )
                    )
                    :
                    (
                      <tr>
                        <td colSpan='100%' className='text-center'>
                          <div
                            className='spinner-border text-primary m-5'
                            role='status'
                          />
                        </td>
                      </tr>
                    )
                }
              </tbody>
            </table>
          </div>
          <div className="form-group row mb-4 mt-6">
            <div className="col-lg-12 mb-4 align-items-end d-flex">
              <div className="col-lg-12">
                <ReactPaginate
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  pageRangeDisplayed={3}
                  marginPagesDisplayed={2}
                  pageCount={totalPages}
                  forcePage={activePageNumber - 1}
                  previousLabel="< Prev"
                  pageClassName="page-item"
                  pageLinkClassName="page-link"
                  previousClassName="page-item"
                  previousLinkClassName="page-link"
                  nextClassName="page-item"
                  nextLinkClassName="page-link"
                  breakLabel="..."
                  breakClassName="page-item"
                  breakLinkClassName="page-link"
                  containerClassName="pagination"
                  activeClassName="active"
                  renderOnZeroPageCount={null}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}

const mapStateToProps = state => {
  const { rulesStore, queueslistStore } = state
  return {
    rules: state && state.rulesStore && state.rulesStore.rules,
    loading: state && state.rulesStore && state.rulesStore.loading,
    DeleteRules: rulesStore && rulesStore.DeleteRules ? rulesStore.DeleteRules : '',
    queuesLists: queueslistStore && queueslistStore.queueslists ? queueslistStore.queueslists : {},
    setFilterFunction: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setFilterFunction &&
      state.clientCrudFilterStore.setFilterFunction ? state.clientCrudFilterStore.setFilterFunction : false,
    setCredFilterParams: state && state.clientCrudFilterStore && state.clientCrudFilterStore.setCredFilterParams
      ? state.clientCrudFilterStore.setCredFilterParams : {},
  }
}
const mapDispatchToProps = dispatch => ({

  getRuleslistDispatch: (params) => dispatch(rulesActions.getRules(params)),
  deleteRulesListDispatch: (id) => dispatch(rulesActions.deleteRules(id)),
  clearRulesDispatch: () => dispatch(rulesActions.clearRules()),
  getQueueslistDispatch: (params) => dispatch(queuesActions.getqueueslist(params)),
  setFilterFunctionDispatch: (data) => dispatch(clientCredFilterActions.setFilterFunction(data))
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rules);